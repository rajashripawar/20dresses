package com.daogenerator;

import de.greenrobot.daogenerator.DaoGenerator;
import de.greenrobot.daogenerator.Entity;
import de.greenrobot.daogenerator.Property;
import de.greenrobot.daogenerator.Schema;

public class MyDaoGenerator {

    public static void main(String args[]) throws Exception {
        Schema schema = new Schema(3, "outletwise.com.twentydresses.model.database.greenbot");

        Entity shortlist = schema.addEntity("Shortlist");
        shortlist.addIdProperty();
        shortlist.addStringProperty("product_id").unique();
        shortlist.addStringProperty("product_img");
        shortlist.addStringProperty("product_price");
        shortlist.addStringProperty("product_discount");

        Entity user = schema.addEntity("User");
        user.addLongProperty("user_id").primaryKey();
        user.addStringProperty("user_email");
        user.addStringProperty("user_first_name");
        user.addStringProperty("user_last_name");
        user.addStringProperty("user_picture");
        user.addStringProperty("gender");
        user.addStringProperty("dob");
        user.addStringProperty("user_mobile");
        user.addBooleanProperty("mobile_verified");
        user.addStringProperty("user_background_image");
        user.addStringProperty("user_style");
        user.addStringProperty("facebook_id");
        user.addStringProperty("facebook_email");
        user.addStringProperty("cart_count");
        user.addStringProperty("user_registered");

        user.setHasKeepSections(true);

        Entity address = schema.addEntity("Address");
        address.addIntProperty("addressId").primaryKey();
        Property user_id = address.addLongProperty("user_id").getProperty();
        address.addStringProperty("streetName");
        address.addLongProperty("pinCode");
        user.addToMany(address, user_id);
        address.setHasKeepSections(true);

        Entity cart = schema.addEntity("Cart");
        cart.addIdProperty();
        cart.addStringProperty("cart_item_id");
        cart.addStringProperty("product_id");
        cart.addStringProperty("product_name");
        cart.addStringProperty("product_attr_id");
        cart.addStringProperty("product_qty");
        cart.addStringProperty("product_size");
        cart.addStringProperty("product_discount");
        cart.addStringProperty("product_mrp");
        cart.addStringProperty("product_img");
        cart.addStringProperty("product_max_qty");
        cart.addStringProperty("product_price");
        cart.addIntProperty("product_savings");
        cart.setHasKeepSections(true);

        new DaoGenerator().generateAll(schema, "../app/src/main/java");
    }
}
