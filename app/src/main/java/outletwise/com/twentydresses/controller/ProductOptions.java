package outletwise.com.twentydresses.controller;

import android.app.Activity;
import android.content.Context;
import android.view.View;
import android.widget.CompoundButton;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.ToggleButton;

import outletwise.com.twentydresses.R;
import outletwise.com.twentydresses.utilities.Constants;
import outletwise.com.twentydresses.view.activity.BaseActivity;
import outletwise.com.twentydresses.view.fragment.ProductsSortFragment;

/**
 * Created by User-PC on 06-08-2015.
 */
public class ProductOptions implements View.OnClickListener, CompoundButton.OnCheckedChangeListener {
    public static final int FILTER = 0;
    public static final int SORT = 1;
    public static final int SEARCH = 2;
    public static final int TOGGLE = 3;

    private Context context;
    private View rootView;
    private OptionClick optionClick;
    private ProductsSortFragment.Callback sortCallback;

    private TextView tvFilter;
    private TextView tvSort;
    private ToggleButton toggleButton;
    private ProductsSortFragment fragment;
    private int sortPos;
    private LinearLayout llFilter, llSort;


    public ProductOptions(Context context) {
        try {
            if (context == null)
                throw new NullPointerException("Context or rootView not initialized");

            this.context = context;
            this.rootView = ((Activity) context).findViewById(R.id.llOptions);
            init();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public ProductOptions(Context context, View rootView, String strType) {
        try {
            if (context == null)
                throw new NullPointerException("Context or rootView not initialized");

            this.context = context;
            this.rootView = rootView;
            init();
            rootView.findViewById(R.id.llTipText).setVisibility(View.GONE);
            TextView tvTipText = (TextView) rootView.findViewById(R.id.txtvw_tip_text);
            TextView tvTipCount = (TextView) rootView.findViewById(R.id.txtvw_tip_count);

            if (strType.equalsIgnoreCase(Constants.SALE)) {
                tvTipText.setText("Last few pieces at reduced price");
                tvTipCount.setVisibility(View.GONE);
                //  tvTipCount.setText(String.valueOf(Constants.TIP_TOTAL + " items"));
            } else if (strType.equalsIgnoreCase(Constants.NEW)) {
                tvTipText.setText("Just Arrived");
                tvTipCount.setVisibility(View.GONE);
                //  tvTipCount.setText(String.valueOf(Constants.TIP_TOTAL + " items"));
            }


        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public ProductOptions(Context context, String strType) {
        if (context == null)
            throw new NullPointerException("Context or rootView not initialized");

        this.context = context;
        this.rootView = ((Activity) context).findViewById(R.id.llOptions);
        rootView.findViewById(R.id.llTipText).setVisibility(View.VISIBLE);
        TextView tvTipText = (TextView) rootView.findViewById(R.id.txtvw_tip_text);
        TextView tvTipCount = (TextView) rootView.findViewById(R.id.txtvw_tip_count);
        init();

        if (strType.equalsIgnoreCase("ForYou")) {
            rootView.findViewById(R.id.llFilterSort).setVisibility(View.GONE);
            tvTipText.setText(Constants.TIP_NAME);
            tvTipCount.setText(String.valueOf(Constants.TIP_TOTAL + " items"));
        } else if (strType.equalsIgnoreCase("JustSold")) {
            rootView.findViewById(R.id.llFilterSort).setVisibility(View.GONE);
            rootView.findViewById(R.id.llTipText).setVisibility(View.GONE);
        } else
            rootView.findViewById(R.id.llFilterSort).setVisibility(View.VISIBLE);
    }

    public void setOptionClickListener(OptionClick optionClick) {
        this.optionClick = optionClick;
    }

    public void hideToggle() {
        rootView.findViewById(R.id.viewToggle).setVisibility(View.GONE);
        rootView.findViewById(R.id.llToggleButton).setVisibility(View.GONE);
        toggleButton.setVisibility(View.GONE);
    }

    void init() {
        tvFilter = (TextView) rootView.findViewById(R.id.tvFilter);
        tvSort = (TextView) rootView.findViewById(R.id.tvSort);
        toggleButton = (ToggleButton) rootView.findViewById(R.id.toggle);

        llFilter = (LinearLayout) rootView.findViewById(R.id.llFilter);
        llSort = (LinearLayout) rootView.findViewById(R.id.llSort);

        llFilter.setOnClickListener(this);
        llSort.setOnClickListener(this);

      /*  tvFilter.setOnClickListener(this);
        tvSort.setOnClickListener(this);*/
        toggleButton.setOnCheckedChangeListener(this);
    }

    public void setSortPos(int sortPos) {
        this.sortPos = sortPos;
    }

    @Override
    public void onClick(View v) {
        if (optionClick != null) {
            switch (v.getId()) {
               /* case R.id.tvFilter:
                    Constants.debug("Filter");
                    Option option = new Option();
                    option.setTag(FILTER);
                    optionClick.onOptionClick(option);
                    break;

                case R.id.tvSort:
                    Constants.debug("Sort");
                    fragment = new ProductsSortFragment();
                    fragment.show(((BaseActivity) context).getmFragmentManager(), ProductsSortFragment.class.getSimpleName());
                    fragment.setCallback(sortCallback);
                    fragment.setSortPos(sortPos);
                    break;*/

                case R.id.llFilter:
                    Constants.debug("Filter");
                    Option option = new Option();
                    option.setTag(FILTER);
                    optionClick.onOptionClick(option);
                    break;

                case R.id.llSort:
                    Constants.debug("Sort");
                    fragment = new ProductsSortFragment();
                    fragment.show(((BaseActivity) context).getmFragmentManager(), ProductsSortFragment.class.getSimpleName());
                    fragment.setCallback(sortCallback);
                    fragment.setSortPos(sortPos);
                    break;


            }
        }
    }

    @Override
    public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
        if (optionClick != null) {
            Option option = new Option();
            option.setTag(TOGGLE);
            option.setToggleState(isChecked);
            optionClick.onOptionClick(option);
        }
    }

    public void setSortCallback(ProductsSortFragment.Callback sortCallback) {
        this.sortCallback = sortCallback;
    }

    public interface OptionClick {
        void onOptionClick(Option tag);
    }

    public class Option {
        private int tag;
        private boolean toggleState;
        private String sortBy;

        public int getTag() {
            return tag;
        }

        public void setTag(int tag) {
            this.tag = tag;
        }

        public boolean isToggleState() {
            return toggleState;
        }

        public void setToggleState(boolean toggleState) {
            this.toggleState = toggleState;
        }

        public String getSortBy() {
            return sortBy;
        }

        public void setSortBy(String sortBy) {
            this.sortBy = sortBy;
        }
    }
}
