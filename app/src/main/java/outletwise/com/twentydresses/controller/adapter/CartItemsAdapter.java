package outletwise.com.twentydresses.controller.adapter;

import android.app.Activity;
import android.content.Context;
import android.graphics.Color;
import android.graphics.Paint;
import android.view.LayoutInflater;
import android.view.View;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.Spinner;
import android.widget.TextView;

import com.squareup.picasso.Picasso;

import java.util.ArrayList;
import java.util.List;

import outletwise.com.twentydresses.R;
import outletwise.com.twentydresses.model.database.greenbot.Cart;
import outletwise.com.twentydresses.utilities.Constants;

/**
 * Created by User-PC on 09-10-2015.
 */
public class CartItemsAdapter {
    private Context context;
    private ArrayList<Cart> cartItems;
    private ArrayList<Cart> outOfStockItems = new ArrayList<>();
    private LinearLayout linearLayout;
    private OnCartEvent onCartEvent;
    private int spinnerInitCount;

    public CartItemsAdapter(Context context, ArrayList<Cart> cartItems) {
        this.context = context;
        this.cartItems = cartItems;
        this.linearLayout = (LinearLayout) ((Activity) context).findViewById(R.id.llCartItems);
        addItems();
    }

    private void addItems() {
        spinnerInitCount = 0;
        for (final Cart cart : cartItems) {
            View cartLayout = LayoutInflater.from(context).inflate(
                    R.layout.layout_adapter_cart_item, null);

            TextView tvProductDiscountPrice = (TextView) cartLayout.findViewById(R.id.tvProductDiscountPrice);
            TextView tvProductPrice = (TextView) cartLayout.findViewById(R.id.tvProductPrice);

            tvProductPrice.setText(Constants.RUPEE +
                    Constants.formatAmount(cart.getProduct_mrp()));

            if (!cart.getProduct_discount().equalsIgnoreCase("0")) {
                tvProductDiscountPrice.setVisibility(View.VISIBLE);
                tvProductDiscountPrice.setText(Constants.RUPEE + Constants.getPriceWithDiscount(cart.getProduct_mrp(), cart.getProduct_discount()));
                tvProductPrice.setPaintFlags(tvProductPrice.getPaintFlags() | Paint.STRIKE_THRU_TEXT_FLAG);
                tvProductPrice.setTextColor(Color.parseColor("#D50000"));
            } else
                tvProductDiscountPrice.setVisibility(View.GONE);

            ((TextView) cartLayout.findViewById(R.id.tvProductName)).setText(cart.getProduct_name());

            if (cart.getProduct_size().equalsIgnoreCase("Default Size"))
                ((TextView) cartLayout.findViewById(R.id.tvProductSize)).setVisibility(View.GONE);
            else
                ((TextView) cartLayout.findViewById(R.id.tvProductSize)).setText("Size: " + cart.getProduct_size());


            if (cart.getProduct_savings() == 0) {
                ((TextView) cartLayout.findViewById(R.id.tvProductSaving)).setVisibility(View.GONE);
            } else
                ((TextView) cartLayout.findViewById(R.id.tvProductSaving)).setText("Savings: " + Constants.RUPEE +
                        Constants.formatAmount(cart.getProduct_savings()));

            ((TextView) cartLayout.findViewById(R.id.tvFinalProductPrice)).
                    setText(Constants.RUPEE + Constants.formatAmount(cart.getProduct_price()));

            ImageView imageView = (ImageView) cartLayout.findViewById(R.id.ivProductImage);
            Picasso.with(context).load(cart.getProduct_img()).into(imageView);

            cartLayout.findViewById(R.id.ivClose).setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    final View parent = (View) v.getParent();
                    Animation animation = AnimationUtils.loadAnimation(context, R.anim.abc_fade_out);
                    animation.setAnimationListener(new Animation.AnimationListener() {
                        @Override
                        public void onAnimationStart(Animation animation) {

                        }

                        @Override
                        public void onAnimationEnd(Animation animation) {
                            linearLayout.removeView(parent);
                            outOfStockItems.remove(parent.getTag());
                            if (onCartEvent != null)
                                onCartEvent.onItemRemoved((Cart) parent.getTag());
                        }

                        @Override
                        public void onAnimationRepeat(Animation animation) {

                        }
                    });
                    parent.startAnimation(animation);
                }
            });

            final Spinner spinner = (Spinner) cartLayout.findViewById(R.id.spProductQty);
            LinearLayout llSpinner = (LinearLayout) cartLayout.findViewById(R.id.llSpinner);

            /* If max qty of product is 0 then product is out of stock*/
            List<String> qty = Constants.getQtys(1,
                    Integer.parseInt(cart.getProduct_max_qty()));

            if (qty.size() == 0) {
                spinner.setVisibility(View.GONE);
                llSpinner.setVisibility(View.GONE);
                cartLayout.findViewById(R.id.rlOutOfStock).bringToFront();
                cartLayout.findViewById(R.id.rlOutOfStock).setVisibility(View.VISIBLE);
                outOfStockItems.add(cart);
            } else {
                ArrayAdapter<String> adapter = new ArrayAdapter<>(context, android.R.layout.simple_spinner_item, qty);
                adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
                spinner.setAdapter(adapter);
                spinner.setSelection(Integer.parseInt(cart.getProduct_qty()) - 1);
                spinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
                    @Override
                    public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                        Constants.debug(parent.getItemAtPosition(position).toString());
                        if (spinnerInitCount < cartItems.size()) {
                            spinnerInitCount++;
                        } else {
                            cart.setProduct_qty(parent.getItemAtPosition(position).toString());
                            onCartEvent.onItemQuantityChanged(cart);
                        }
                    }

                    @Override
                    public void onNothingSelected(AdapterView<?> parent) {

                    }
                });
            }

            if(llSpinner.getVisibility() == View.VISIBLE)
            {
                llSpinner.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        spinner.performClick();
                    }
                });
            }



            cartLayout.setTag(cart);
            linearLayout.addView(cartLayout);
            cartLayout.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if (onCartEvent != null)
                        onCartEvent.onItemClicked((Cart) v.getTag());
                }
            });
        }
    }

    public void setCartItems(ArrayList<Cart> cartItems) {
        this.cartItems = cartItems;
    }

    public ArrayList<Cart> getOutOfStockItems() {
        return outOfStockItems;
    }

    public void setOutOfStockItems() {
        if (outOfStockItems != null)
            outOfStockItems.clear();
    }

    private void removeViews() {
        linearLayout.removeAllViews();
    }

    public void notifyDataSetChanged() {
        setOutOfStockItems();
        removeViews();
        addItems();
    }

    public void setOnCartEvent(OnCartEvent onCartEvent) {
        this.onCartEvent = onCartEvent;
    }

    public interface OnCartEvent {
        void onItemRemoved(Cart cartItem);

        void onItemClicked(Cart cartItem);

        void onItemQuantityChanged(Cart cartItem);
    }
}
