package outletwise.com.twentydresses.controller;


import android.view.MotionEvent;
import android.view.View;

import outletwise.com.twentydresses.utilities.Constants;

/**
 * Created by User-PC on 27/07/2015.
 */

public class SwipeDetector implements View.OnTouchListener {

    private static final int TAP_DISTANCE = 10;
    private int min_distance = 100;
    private float downX, downY, upX, upY;
    private View v;
    boolean monitorAction = true;

    private onSwipeEvent swipeEventListener;


    public SwipeDetector(View v) {
        this.v = v;
        v.setOnTouchListener(this);
    }

    public void setOnSwipeListener(onSwipeEvent swipeEventListener) {
        this.swipeEventListener = swipeEventListener;
    }

    public void onTap() {
        if (swipeEventListener != null) {
            swipeEventListener.SwipeEventDetected(v, SwipeTypeEnum.TAP);
            monitorAction = false;
        } else
            Constants.error("please pass SwipeDetector.onSwipeEvent Interface instance");
    }

    public void onTopToBottomLeftSwipe() {
        if (swipeEventListener != null) {
            swipeEventListener.SwipeEventDetected(v, SwipeTypeEnum.TOP_TO_BOTTOM_LEFT);
            monitorAction = false;
        } else
            Constants.error("please pass SwipeDetector.onSwipeEvent Interface instance");
    }

    public void onTopToBottomRightSwipe() {
        if (swipeEventListener != null) {
            swipeEventListener.SwipeEventDetected(v, SwipeTypeEnum.TOP_TO_BOTTOM_RIGHT);
            monitorAction = false;
        } else
            Constants.error("please pass SwipeDetector.onSwipeEvent Interface instance");
    }

    public void onTopToBottomSwipe() {
        if (swipeEventListener != null) {
            swipeEventListener.SwipeEventDetected(v, SwipeTypeEnum.TOP_TO_BOTTOM);
            monitorAction = false;
        } else
            Constants.error("please pass SwipeDetector.onSwipeEvent Interface instance");
    }


    public boolean onTouch(View v, MotionEvent event) {
        switch (event.getAction()) {
            case MotionEvent.ACTION_DOWN: {
                downX = event.getX();
                downY = event.getY();
                return true;
            }
            case MotionEvent.ACTION_MOVE: {
                if (!monitorAction)
                    return false;

                upX = event.getX();
                upY = event.getY();

                float deltaX = downX - upX;
                float deltaY = downY - upY;

                //VERTICAL SCROLL
                if (Math.abs(deltaY) > min_distance) {
                    // top or down
                    if (deltaY < 0) {
                        if ((deltaX < 0)) {
                            this.onTopToBottomRightSwipe();
                            return true;
                        } else if (deltaX > 0) {
                            this.onTopToBottomLeftSwipe();
                            return true;
                        } else {
                            this.onTopToBottomSwipe();
                            return true;
                        }
                    }
                } else {
                    //not long enough swipe...
                    return false;
                }

                return true;

            }
            case MotionEvent.ACTION_UP:
                float deltaX = downX - upX;
                float deltaY = downY - upY;

                if (deltaX < TAP_DISTANCE && deltaY < TAP_DISTANCE) {
                    this.onTap();
                    return true;
                }

                monitorAction = true;
                return false;

            case MotionEvent.ACTION_CANCEL:
                monitorAction = true;
                return false;
        }

        return false;
    }

    public interface onSwipeEvent {
        public void SwipeEventDetected(View v, SwipeTypeEnum SwipeType);
    }

    public SwipeDetector setMinDistanceInPixels(int min_distance) {
        this.min_distance = min_distance;
        return this;
    }

    public enum SwipeTypeEnum {
        TOP_TO_BOTTOM, TOP_TO_BOTTOM_LEFT, TOP_TO_BOTTOM_RIGHT, TAP
    }

}
