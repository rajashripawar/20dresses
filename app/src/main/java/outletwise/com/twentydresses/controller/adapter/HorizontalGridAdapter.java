package outletwise.com.twentydresses.controller.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;

import java.util.ArrayList;
import java.util.List;

import outletwise.com.twentydresses.R;
import outletwise.com.twentydresses.utilities.Constants;

/**
 * Created by User-PC on 27/07/2015.
 */
public class HorizontalGridAdapter extends BaseRecyclerAdapter {
    private static final int imageWidth = 60;
    private static final int imageHeight = 80;
    private List<Integer> itemIds = new ArrayList<>();

    public HorizontalGridAdapter(Context context, ArrayList<Integer> imageViews) {
        super(context, imageViews);
    }

    private class ViewHolder extends BaseViewHolder {
        public View mView;
        public ImageView mImageView;

        public ViewHolder(View view) {
            super(view);
            mView = view;
            mImageView = (ImageView) view.findViewById(R.id.imageView);
        }

        @Override
        public void onClick(View v) {
            if (mItemClickListener != null) {
                mItemClickListener.onItemClick(v, getAdapterPosition());
            }
        }
    }

    @Override
    public BaseViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.layout_adapter_image, parent, false);
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(BaseViewHolder holder, final int position) {
        ViewHolder viewHolder = (ViewHolder) holder;
        DragDropAdapter.Tag tag = (DragDropAdapter.Tag) items.get(position);
        viewHolder.mImageView.setImageDrawable(Constants.resize(context, tag.resId, imageWidth, imageHeight));
        viewHolder.mView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (mItemClickListener != null) {
                    mItemClickListener.onItemClick(v, position);
                }
            }
        });
    }

    @Override
    public void addItem(Object item) {
        super.addItem(item);
        DragDropAdapter.Tag tag = (DragDropAdapter.Tag) item;
        itemIds.add(tag.position);
    }

    @Override
    public void removeItem(Object item) {
        super.removeItem(item);
        DragDropAdapter.Tag tag = (DragDropAdapter.Tag) item;
        itemIds.remove(tag.position);
    }

    public List<Integer> getItemIds() {
        return itemIds;
    }
}
