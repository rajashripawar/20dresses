package outletwise.com.twentydresses.controller.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.squareup.picasso.Picasso;

import java.util.ArrayList;
import java.util.List;

import outletwise.com.twentydresses.R;
import outletwise.com.twentydresses.model.Trends;

/**
 * Created by User-PC on 11-05-2016.
 */
public class TrendsAdapter extends BaseRecyclerAdapter {

    public TrendsAdapter(Context context, ArrayList<Trends.DataEntity> items) {
        super(context, items);
    }

    private class ViewHolder extends BaseViewHolder {

        public View mView;

        public ImageView ivProduct1;
        public ImageView ivProduct2;
        public ImageView ivProduct3;
        public ImageView ivBanner;
        public TextView tvProductCount;
        public TextView tvFooter;
        public List<ImageView> thumbnails = new ArrayList<>();

        public ViewHolder(View view) {
            super(view);
            mView = view;
            ivProduct1 = (ImageView) view.findViewById(R.id.ivProduct1);
            ivProduct2 = (ImageView) view.findViewById(R.id.ivProduct2);
            ivProduct3 = (ImageView) view.findViewById(R.id.ivProduct3);
            ivBanner = (ImageView) view.findViewById(R.id.ivBanner);
            thumbnails.add(ivProduct1);
            thumbnails.add(ivProduct2);
            thumbnails.add(ivProduct3);
            // thumbnails.add(ivProduct4);
            tvProductCount = (TextView) view.findViewById(R.id.tvProductCount);
            tvFooter = (TextView) view.findViewById(R.id.tvFooterText);
        }

        @Override
        public void onClick(View v) {
            if (mItemClickListener != null) {
                mItemClickListener.onItemClick(v, getAdapterPosition());
            }
        }
    }

    @Override
    public BaseViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.layout_adapter_trends, parent, false);
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(BaseViewHolder holder, int position) {

        ViewHolder viewHolder = (ViewHolder) holder;
        Trends.DataEntity data = (Trends.DataEntity) items.get(position);
        viewHolder.itemView.setTag(data);

        for (int i = 0; i < data.getThumbnails().size(); i++)
            Picasso.with(context).load(data.getThumbnails().get(i)).into(viewHolder.thumbnails.get(i));

        Picasso.with(context).load(data.getTrend_banner()).into(viewHolder.ivBanner);
        int totaltxtproductcount =  Integer.parseInt(data.getTotal())-3;
        viewHolder.tvProductCount.setText("+"+totaltxtproductcount);
        viewHolder.tvFooter.setText(data.getTrend_caption());
    }
}
