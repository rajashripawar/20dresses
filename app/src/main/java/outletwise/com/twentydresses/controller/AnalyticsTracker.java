package outletwise.com.twentydresses.controller;

import android.content.Context;

import com.clevertap.android.sdk.CleverTapAPI;
import com.clevertap.android.sdk.exceptions.InvalidEventNameException;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;

import outletwise.com.twentydresses.ApplicationUtil;
import outletwise.com.twentydresses.model.AddCartResponse;
import outletwise.com.twentydresses.model.PlaceOrder;
import outletwise.com.twentydresses.model.PlaceOrderResponse;
import outletwise.com.twentydresses.model.Product;
import outletwise.com.twentydresses.model.StyleProfile;
import outletwise.com.twentydresses.model.database.greenbot.User;
import semusi.activitysdk.ContextSdk;

/**
 * Created by User-PC on 30-03-2016.
 */
public class AnalyticsTracker {

    private static AnalyticsTracker mInstance = new AnalyticsTracker();

    public static AnalyticsTracker getInstance() {
        return mInstance;
    }


    private AnalyticsTracker() {
    }

    /*
    * Method to track category for CleverTap analytics
    * */
    public void trackCategory(String category) {
        HashMap<String, Object> catViewedAction = new HashMap<String, Object>();
        catViewedAction.put("Category Name", category);
        //CleverTap
        ApplicationUtil.getInstance().ct.event.push("Category viewed", catViewedAction);

        // appIce Analytics
        HashMap<String, String> catViewedAction1 = new HashMap<String, String>((HashMap) catViewedAction);
//        ContextSdk.tagEvent("Category viewed", catViewedAction1);

        //Mixpanel
        /*JSONObject obj = new JSONObject(catViewedAction);
        ApplicationUtil.getInstance().mMixpanel.track("Category viewed", obj);*/
    }


    /*
   * Method to track product view for CleverTap analytics
   * */
    public void trackProductView(Product product, String categoryName) {
        //CleverTap
        HashMap<String, Object> prodViewedAction = new HashMap<String, Object>();
        prodViewedAction.put("Category Id", product.getCategory_id());
        prodViewedAction.put("Category Name", categoryName);
        prodViewedAction.put("Product Name", product.getProduct_name());
        prodViewedAction.put("Price", product.getProduct_price());
        prodViewedAction.put("Internal_Source", categoryName);

        ApplicationUtil.getInstance().ct.event.push("Product viewed", prodViewedAction);

        // appIce Analytics
        HashMap<String, String> prodViewedAction1 = new HashMap<String, String>((HashMap) prodViewedAction);
//        ContextSdk.tagEvent("Product viewed", prodViewedAction1);


        //Mixpanel
   /*     JSONObject obj = new JSONObject(prodViewedAction);
        ApplicationUtil.getInstance().mMixpanel.track("Product viewed", obj);*/
    }


    /*
     * Method to track added to cart event in cleverTap
     */
    public void trackAddedToCart(AddCartResponse addCartResponse, String categoryName) {

        //CleverTap
        HashMap<String, Object> cartAddedAction = new HashMap<String, Object>();
        cartAddedAction.put("Product Id", addCartResponse.getProductDetails().getProduct_id());
        cartAddedAction.put("Product Name", addCartResponse.getProductDetails().getProduct_name());
        cartAddedAction.put("Category Name", categoryName);
        cartAddedAction.put("Price", addCartResponse.getProductDetails().getProduct_price());
        cartAddedAction.put("Quantity", addCartResponse.getProductDetails().getProduct_qty());
        cartAddedAction.put("Internal_Source", categoryName);

        ApplicationUtil.getInstance().ct.event.push("Added to cart", cartAddedAction);

        // appIce Analytics
        HashMap<String, String> cartAddedAction1 = new HashMap<String, String>((HashMap) cartAddedAction);
//        ContextSdk.tagEvent("Added to cart", cartAddedAction1);

        //Mixpanel
       /* JSONObject obj = new JSONObject(cartAddedAction);
        ApplicationUtil.getInstance().mMixpanel.track("Added to cart", obj);*/
    }

    /*
    * Method to track revenue
    */
    public void trackRevenue(PlaceOrderResponse placeOrderResponse, PlaceOrder placeOrder) {
        //CleverTap
        HashMap<String, Object> chargeDetails = new HashMap<String, Object>();
        chargeDetails.put("Amount", placeOrderResponse.getOrder().getOrder_total());
        chargeDetails.put("Payment Mode", placeOrder.getPayment_mode());
        chargeDetails.put("Charged ID", placeOrderResponse.getOrder_id());

        ArrayList<HashMap<String, Object>> items = new ArrayList<HashMap<String, Object>>();
        HashMap<String, Object> item;

        for (int i = 0; i < placeOrderResponse.getOrder_items().size(); i++) {
            item = new HashMap<String, Object>();
            item.put("Product Category", placeOrderResponse.getOrder_items().get(i).getProduct_category_name());
            item.put("Product Name", placeOrderResponse.getOrder_items().get(i).getProduct_name());
            item.put("Quantity", placeOrderResponse.getOrder_items().get(i).getQuantity());
            item.put("Product price", placeOrderResponse.getOrder_items().get(i).getProduct_mrp());
            item.put("Internal_Source", placeOrderResponse.getOrder_items().get(i).getProduct_category_name());
            items.add(item);
        }

        try {
            ApplicationUtil.getInstance().ct.event.push(CleverTapAPI.CHARGED_EVENT, chargeDetails, items);
        } catch (InvalidEventNameException e) {
            e.printStackTrace();
        }
        // appIce Analytics
        HashMap<String, Object> chargeDetails1 = new HashMap<String, Object>();
        chargeDetails1.put("Amount", placeOrderResponse.getOrder().getOrder_total());
        chargeDetails1.put("Payment Mode", placeOrder.getPayment_mode());
        chargeDetails1.put("Charged ID", placeOrderResponse.getOrder_id());

        JSONArray productList = new JSONArray();
        JSONObject productDetails;

        for (int i = 0; i < placeOrderResponse.getOrder_items().size(); i++) {
            productDetails = new JSONObject();

            try {
                productDetails.put("Product Category", placeOrderResponse.getOrder_items().get(i).getProduct_category_name());
                productDetails.put("Product Name", placeOrderResponse.getOrder_items().get(i).getProduct_name());
                productDetails.put("Quantity", placeOrderResponse.getOrder_items().get(i).getQuantity());
                productDetails.put("Product price", placeOrderResponse.getOrder_items().get(i).getProduct_mrp());
                productDetails.put("Internal_Source", placeOrderResponse.getOrder_items().get(i).getProduct_category_name());
            } catch (JSONException e) {
                e.printStackTrace();
            }
            productList.put(productDetails);
        }

        chargeDetails1.put("Order_details", productList);
        HashMap<String, String> purchaseDetails = new HashMap<String, String>((HashMap) chargeDetails1);
//        ContextSdk.tagEvent("Purchase", purchaseDetails);

        //Mixpanel
     /*   ApplicationUtil.getInstance().mMixpanel.getPeople().identify(PaymentOptionsActivity.ret);
        ApplicationUtil.getInstance().mMixpanel.getPeople().trackCharge(Double.parseDouble(String.valueOf(placeOrderResponse.getOrder().getOrder_total())), null);
        JSONObject obj = new JSONObject(chargeDetails1);
        ApplicationUtil.getInstance().mMixpanel.track("Purchase Item", obj);*/
    }


    /*
   * Method to track Sign up
   */
    public void trackSignUp(User user, String mode) {

        //CleverTap
        HashMap<String, Object> signUpAction = new HashMap<String, Object>();
        signUpAction.put("Mode of registration", mode);
        ApplicationUtil.getInstance().ct.event.push("Registration", signUpAction);

        // appIce Analytics
        HashMap<String, String> signUpAction1 = new HashMap<String, String>((HashMap) signUpAction);
//        ContextSdk.tagEvent("Registration", signUpAction1);
        //Mixpanel
      /*  JSONObject obj = new JSONObject(signUpAction);
        ApplicationUtil.getInstance().mMixpanel.track("Registration", obj);*/
    }


    /*
 * Method to track Quiz
 */
    public void trackQuiz(User user) {

        //CleverTap
        HashMap<String, Object> signUpAction = new HashMap<String, Object>();
        if (user != null && user.getUser_first_name() != null && user.getUser_last_name() != null)
            signUpAction.put("User Name", user.getUser_first_name() + " " + user.getUser_last_name());
        ApplicationUtil.getInstance().ct.event.push("Quiz Taken", signUpAction);

        // appIce Analytics
        HashMap<String, String> signUpAction1 = new HashMap<String, String>((HashMap) signUpAction);
//        ContextSdk.tagEvent("Quiz Taken", signUpAction1);

        //Mixpanel
      /*  JSONObject obj = new JSONObject(signUpAction);
        ApplicationUtil.getInstance().mMixpanel.track("Quiz Taken", obj);*/
    }


    /*
 * Method to track Sign In
 */
  /*  public void trackSignIn(User user, String mode) {

        //CleverTap
        HashMap<String, Object> signInAction = new HashMap<String, Object>();
        signInAction.put("Mode of login", mode);
        ApplicationUtil.getInstance().ct.event.push("Login", signInAction);

        // appIce Analytics
        HashMap<String, String> signInAction1 = new HashMap<String, String>((HashMap) signInAction);
        ContextSdk.tagEvent("Login", signInAction1);

        //Mixpanel
  *//*      JSONObject obj = new JSONObject(signInAction);
        ApplicationUtil.getInstance().mMixpanel.track("Login", obj);*//*
    }*/


    /*
    * Method to track Shortlist product
    * */

    public void trackShortlist(Product shortlist, String categoryName) {

        //CleverTap
        HashMap<String, Object> shortlistAddedAction = new HashMap<String, Object>();
        shortlistAddedAction.put("Shortlist Product Name", shortlist.getProduct_name());
        shortlistAddedAction.put("Shortlist Category Name", categoryName);
        shortlistAddedAction.put("Shortlist Price", shortlist.getProduct_price());
        ApplicationUtil.getInstance().ct.event.push("Added to shortlist", shortlistAddedAction);

        // appIce Analytics
        HashMap<String, String> shortlistAddedAction1 = new HashMap<String, String>((HashMap) shortlistAddedAction);
//        ContextSdk.tagEvent("Added to shortlist", shortlistAddedAction1);

        //Mixpanel
       /* JSONObject obj = new JSONObject(shortlistAddedAction);
        ApplicationUtil.getInstance().mMixpanel.track("Added to shortlist", obj);*/
    }

    public void trackForYouClicked(String tip_id, String tip_text) {

        //CleverTap
        HashMap<String, Object> forYouAction = new HashMap<String, Object>();
        forYouAction.put("tip_id", tip_id);
        forYouAction.put("tip_text", tip_text);
        ApplicationUtil.getInstance().ct.event.push("FY_Cluster_Clicked", forYouAction);

        // appIce Analytics
        HashMap<String, String> forYouAction1 = new HashMap<String, String>((HashMap) forYouAction);
//        ContextSdk.tagEvent("FY_Cluster_Clicked", forYouAction1);

        //Mixpanel
      /*  JSONObject obj = new JSONObject(forYouAction);
        ApplicationUtil.getInstance().mMixpanel.track("For You Clicked", obj);*/
    }

    /*
    * Google analytics
    * */
    public void sendDataToTwoTrackers(Map<String, String> params) {
        ApplicationUtil.getInstance().getDefaultTracker().send(params);
        ApplicationUtil.getInstance().getEcommerceTrackerTracker().send(params);
    }


    /*
    * Method to set profile attributes
    * */
    public void setRegProperties(StyleProfile styleProfile, User user, Context ctx) {

        //CleverTap
        HashMap<String, Object> profileUpdate = new HashMap<String, Object>();
        if (user != null && user.getUser_first_name() != null && user.getUser_last_name() != null) {
            profileUpdate.put("User Name", user.getUser_first_name() + " " + user.getUser_last_name());
//            ContextSdk.setCustomVariable("First Name", user.getUser_first_name(), ctx);
//            ContextSdk.setCustomVariable("Last Name", user.getUser_last_name(), ctx);
        }

        if (user != null && user.getUser_email() != null) {
            profileUpdate.put("User Email", user.getUser_email());
//            ContextSdk.setCustomVariable("User Email", user.getUser_email(), ctx);
        }


        if (user != null && user.getUser_mobile() != null) {
            profileUpdate.put("User Phone", user.getUser_mobile());
//            ContextSdk.setCustomVariable("User Phone", user.getUser_mobile(), ctx);
        }

        if (user != null && user.getDob() != null) {
            profileUpdate.put("User DOB", user.getDob());
//            ContextSdk.setCustomVariable("User DOB", user.getDob(), ctx);
        }

        if (user != null && user.getCart_count() != null) {
            profileUpdate.put("ItemsInCart", user.getCart_count());
//            ContextSdk.setCustomVariable("ItemsInCart", user.getCart_count(), ctx);
        }

        profileUpdate.put("Body shape", styleProfile.getUser_attr().getShape());
        profileUpdate.put("Top size", styleProfile.getUser_attr().getSize());
        profileUpdate.put("Bottoms size", styleProfile.getUser_attr().getBottoms_Size());
        profileUpdate.put("Height", styleProfile.getUser_attr().getHeight());
        profileUpdate.put("Shoes size", styleProfile.getUser_attr().getShoes_Size());
        profileUpdate.put("Face color", styleProfile.getUser_attr().getFace_Color());
        profileUpdate.put("User style", styleProfile.getUser_style());

        // Set custom properties for Appice
//        ContextSdk.setCustomVariable("Body shape", styleProfile.getUser_attr().getShape(), ctx);
//        ContextSdk.setCustomVariable("Top size", styleProfile.getUser_attr().getSize(), ctx);
//        ContextSdk.setCustomVariable("Bottoms size", styleProfile.getUser_attr().getBottoms_Size(), ctx);
//        ContextSdk.setCustomVariable("Height", styleProfile.getUser_attr().getHeight(), ctx);
//        ContextSdk.setCustomVariable("Shoes size", styleProfile.getUser_attr().getShoes_Size(), ctx);
//        ContextSdk.setCustomVariable("Face color", styleProfile.getUser_attr().getFace_Color(), ctx);
//        ContextSdk.setCustomVariable("User style", styleProfile.getUser_style(), ctx);

        ApplicationUtil.getInstance().ct.profile.push(profileUpdate);


        //Appice
        HashMap<String, String> profileUpdate1 = new HashMap<String, String>((HashMap) profileUpdate);
//        ContextSdk.tagEvent("Profile", profileUpdate1);

        //Mixpanel
       /* JSONObject userProf = new JSONObject();
        try {
            userProf.put("$Body_shape", styleProfile.getUser_attr().getShape());
            userProf.put("$Top_size", styleProfile.getUser_attr().getSize());
            userProf.put("$Bottoms_size", styleProfile.getUser_attr().getBottoms_Size());
            userProf.put("$Height", styleProfile.getUser_attr().getHeight());
            userProf.put("$Shoes_size", styleProfile.getUser_attr().getShoes_Size());
            userProf.put("$Face_color", styleProfile.getUser_attr().getFace_Color());
            userProf.put("$User_style", styleProfile.getUser_style());
        } catch (JSONException e) {
            e.printStackTrace();
        }
        ApplicationUtil.getInstance().mMixpanel.getPeople().set(userProf);*/

    }


    public void setRegProperties(User user) {

        // CleverTap
        HashMap<String, Object> profileUpdate = new HashMap<String, Object>();
        if (user != null && user.getUser_first_name() != null && user.getUser_last_name() != null)
            profileUpdate.put("Name", user.getUser_first_name() + " " + user.getUser_last_name());
        if (user != null && user.getUser_id() != null)
            profileUpdate.put("Identity", String.valueOf(user.getUser_id()));
        if (user != null && user.getUser_email() != null)
            profileUpdate.put("Email", user.getUser_email());
        if (user != null && user.getUser_mobile() != null && !user.getUser_mobile().equalsIgnoreCase("")) {
            try {
                profileUpdate.put("Phone", Long.parseLong(user.getUser_mobile()));
            } catch (NumberFormatException ex) {
                ex.printStackTrace();
            }

        }
        if (user != null && user.getGender() != null)
            profileUpdate.put("Gender", user.getGender());

        SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd");
        try {
            if (user != null && user.getDob() != null) {
                Date dob = dateFormat.parse(user.getDob());
                profileUpdate.put("DOB", dob);
            }
        } catch (ParseException e) {
            e.printStackTrace();
        }

        profileUpdate.put("MSG-email", true); // Enable email notifications
        profileUpdate.put("MSG-push", true); // Enable push notifications
        profileUpdate.put("MSG-sms", true);// Enable sms notifications
        ApplicationUtil.getInstance().ct.profile.push(profileUpdate);
        //Appice
        // ContextSdk.tagEvent("Profile", profileUpdate);


        //Mixpanel
      /*  ApplicationUtil.getInstance().mMixpanel.identify(BaseActivity.ret);
        JSONObject userProf = new JSONObject();
        try {
            userProf.put("$first_name", user.getUser_first_name());
            userProf.put("$last_name", user.getUser_last_name());
            userProf.put("$email", user.getUser_email());
        } catch (JSONException e) {
            e.printStackTrace();
        }
        ApplicationUtil.getInstance().mMixpanel.getPeople().set(userProf);


        JSONObject props = new JSONObject();
        try {
            props.put("User_id", user.getUser_id());
            props.put("Name", user.getUser_first_name() + " " + user.getUser_last_name());
            props.put("Email", user.getUser_email());
            props.put("Mobile", user.getUser_mobile());
            props.put("DOB", user.getDob());
            ApplicationUtil.getInstance().mMixpanel.registerSuperPropertiesOnce(props);
        } catch (JSONException e) {
            e.printStackTrace();
        }*/

    }


     /*
    * Method to track menu selected from drawer
    * */

    public void trackMenuSelected(String item_name) {

        //CleverTap
        HashMap<String, Object> shortlistAddedAction = new HashMap<String, Object>();
        shortlistAddedAction.put("Menu_item", item_name);
        ApplicationUtil.getInstance().ct.event.push("Menu selected", shortlistAddedAction);


        // appIce Analytics
        HashMap<String, String> forYouAction1 = new HashMap<String, String>((HashMap) shortlistAddedAction);
//        ContextSdk.tagEvent("Menu selected", forYouAction1);

    }


    /*
    * Method to track nav menu selected (Home,new,sale etc)
    * */

    public void trackNavMenuSelected(String section_name) {

        //CleverTap
        HashMap<String, Object> shortlistAddedAction = new HashMap<String, Object>();
        shortlistAddedAction.put("Section_Name", section_name);
        ApplicationUtil.getInstance().ct.event.push("Nav Item selected", shortlistAddedAction);


        // appIce Analytics
        HashMap<String, String> forYouAction1 = new HashMap<String, String>((HashMap) shortlistAddedAction);
//        ContextSdk.tagEvent("Nav Item selected", forYouAction1);

    }


    public void trackTrendsClicked(String trend_id, String trend_caption) {

        //CleverTap
        HashMap<String, Object> forYouAction = new HashMap<String, Object>();
        forYouAction.put("trend_id", trend_id);
        forYouAction.put("trend_caption", trend_caption);
        ApplicationUtil.getInstance().ct.event.push("TR_Cluster_Clicked", forYouAction);

        // appIce Analytics
        HashMap<String, String> forYouAction1 = new HashMap<String, String>((HashMap) forYouAction);
//        ContextSdk.tagEvent("TR_Cluster_Clicked", forYouAction1);

        //Mixpanel
      /*  JSONObject obj = new JSONObject(forYouAction);
        ApplicationUtil.getInstance().mMixpanel.track("For You Clicked", obj);*/
    }

    public void setPurchaseDate(String last_purchase_date, Context ctx) {

        //CleverTap
        HashMap<String, Object> profileUpdate = new HashMap<String, Object>();
        profileUpdate.put("last_purchase_date", last_purchase_date);

        // Set custom properties for Appice
//        ContextSdk.setCustomVariable("last_purchase_date", last_purchase_date, ctx);

        if ((last_purchase_date != null) && (!last_purchase_date.isEmpty()))
        ApplicationUtil.getInstance().ct.profile.push(profileUpdate);

        //Appice
        HashMap<String, String> profileUpdate1 = new HashMap<String, String>((HashMap) profileUpdate);
//        ContextSdk.tagEvent("Profile", profileUpdate1);
    }

    public void trackItemShared(Product product, String categoryName) {

        //CleverTap
        HashMap<String, Object> shortlistAddedAction = new HashMap<String, Object>();
        shortlistAddedAction.put("Product_ID", product.getProduct_id());
        shortlistAddedAction.put("Product_Name", product.getProduct_name());
        shortlistAddedAction.put("Category_Name", categoryName);
        ApplicationUtil.getInstance().ct.event.push("Item_Shared", shortlistAddedAction);

        // appIce Analytics
        HashMap<String, String> shortlistAddedAction1 = new HashMap<String, String>((HashMap) shortlistAddedAction);
//        ContextSdk.tagEvent("Item_Shared", shortlistAddedAction1);

    }
}
