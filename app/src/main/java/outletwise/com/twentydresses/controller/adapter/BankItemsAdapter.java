package outletwise.com.twentydresses.controller.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.CheckBox;
import android.widget.LinearLayout;
import android.widget.TextView;

import java.util.List;

import outletwise.com.twentydresses.R;
import outletwise.com.twentydresses.model.OrderReturnResponse.RefundEntity.BankDetailsEntity;

/**
 * Created by User-PC on 09-10-2015.
 */
public class BankItemsAdapter implements View.OnClickListener {
    private Context context;
    private List<BankDetailsEntity> bankDetailsList;
    private LinearLayout linearLayout;
    private BankChanged bankChanged;
    private CheckBox lastChecked;

    public BankItemsAdapter(Context context, List<BankDetailsEntity> bankDetailsList, LinearLayout linearLayout) {
        this.context = context;
        this.bankDetailsList = bankDetailsList;
        this.linearLayout = linearLayout;
        addItems();
    }

    public void addItems() {
        if (bankDetailsList != null) {
            for (BankDetailsEntity bank : bankDetailsList) {
                View bankDetailsLayout = LayoutInflater.from(context).inflate(
                        R.layout.layout_adapter_bank_details, null);
                ((TextView) bankDetailsLayout.findViewById(R.id.tvName)).setText(bank.getBank_account_name());
                ((TextView) bankDetailsLayout.findViewById(R.id.tvBankDetails)).setText(bank.toString());

                bankDetailsLayout.setTag(bank);
                linearLayout.addView(bankDetailsLayout);
                bankDetailsLayout.setOnClickListener(this);
            }
            lastChecked = (CheckBox) linearLayout.getChildAt(0).findViewById(R.id.checkBox);
            lastChecked.setChecked(true);
        }
    }

    private void removeViews() {
        linearLayout.removeAllViews();
    }

    public boolean isEmpty() {
        return linearLayout == null || linearLayout.getChildCount() == 0;
    }

    public boolean isListSizeChanged(int listSize) {
        return listSize != bankDetailsList.size();
    }

    public void notifyDataSetChanged() {
        removeViews();
        addItems();
    }

    public void setBankChanged(BankChanged bankChanged) {
        this.bankChanged = bankChanged;
    }

    public void clearCheck() {
        lastChecked.setChecked(false);
    }

    @Override
    public void onClick(View v) {
        if (bankChanged != null) {
            lastChecked.setChecked(false);
            lastChecked = ((CheckBox) v.findViewById(R.id.checkBox));
            lastChecked.setChecked(true);
            bankChanged.onBankChange((BankDetailsEntity) v.getTag());
        }
    }

    public interface BankChanged {
        void onBankChange(BankDetailsEntity bank);
    }
}
