package outletwise.com.twentydresses.controller.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import java.util.ArrayList;

import outletwise.com.twentydresses.R;
import outletwise.com.twentydresses.model.ProfileType;

/**
 * Created by User-PC on 25-08-2015.
 */
public class ProfileTypeAdapter extends BaseRecyclerAdapter {

    public ProfileTypeAdapter(Context context, ArrayList items) {
        super(context, items);
    }

    class ViewHolder extends BaseViewHolder {
        public View mView;
        public ImageView mImageIcon;
        public TextView mTextType;
        public TextView mTextTypeValue;

        public ViewHolder(View itemView) {
            super(itemView);
            mView = itemView;
            mImageIcon = (ImageView) itemView.findViewById(R.id.ivProfileType);
            mTextType = (TextView) itemView.findViewById(R.id.tvProfileType);
            mTextTypeValue = (TextView) itemView.findViewById(R.id.tvProfileTypeValue);
        }

        @Override
        public void onClick(View v) {
        }
    }

    @Override
    public BaseViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).
                inflate(R.layout.layout_profile_types, parent, false);
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(BaseViewHolder holder, int position) {
        ViewHolder viewHolder = (ViewHolder) holder;
        ProfileType profileType = (ProfileType) items.get(position);
        viewHolder.mImageIcon.setImageResource(profileType.getIvProfileIcon());
        viewHolder.mTextType.setText(profileType.getTvProfileType());
        viewHolder.mTextTypeValue.setText(profileType.getTvProfileTypeValue());
    }
}
