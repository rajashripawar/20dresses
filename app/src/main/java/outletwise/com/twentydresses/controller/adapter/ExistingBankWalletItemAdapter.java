package outletwise.com.twentydresses.controller.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.CheckBox;
import android.widget.LinearLayout;
import android.widget.TextView;

import java.util.List;

import outletwise.com.twentydresses.R;
import outletwise.com.twentydresses.model.WalletResponse;

/**
 * Created by User-PC on 24-02-2016.
 */
public class ExistingBankWalletItemAdapter implements View.OnClickListener {
    private Context context;
    private List<WalletResponse.RefundTypesEntity.BankDetailsEntity> bankDetailsList;
    private LinearLayout linearLayout;
    private BankChanged bankChanged;
    private CheckBox lastChecked;

    public ExistingBankWalletItemAdapter(Context context, List<WalletResponse.RefundTypesEntity.BankDetailsEntity> bankDetailsList, LinearLayout linearLayout) {
        this.context = context;
        this.bankDetailsList = bankDetailsList;
        this.linearLayout = linearLayout;
        addItems();
    }

    public void addItems() {
        if (bankDetailsList != null) {
            for (WalletResponse.RefundTypesEntity.BankDetailsEntity bank : bankDetailsList) {
                View bankDetailsLayout = LayoutInflater.from(context).inflate(
                        R.layout.layout_adapter_bank_details, null);
                ((TextView) bankDetailsLayout.findViewById(R.id.tvName)).setText("Account Name: " + bank.getBank_account_name());
                ((TextView) bankDetailsLayout.findViewById(R.id.tvBankDetails)).
                        setText("Bank Name: " + bank.getBank_name() + "\nAccount No.: " + bank.getBank_account_no() + "\nBranch: " + bank.getBank_branch_name() + "\nIFSC Code: " + bank.getBank_ifsc_code());

                bankDetailsLayout.setTag(bank);
                linearLayout.addView(bankDetailsLayout);

                bankDetailsLayout.findViewById(R.id.checkBox).setTag(bank);
                bankDetailsLayout.findViewById(R.id.checkBox).setOnClickListener(this);

                bankDetailsLayout.setOnClickListener(this);
            }
            lastChecked = (CheckBox) linearLayout.getChildAt(0).findViewById(R.id.checkBox);
            lastChecked.setChecked(true);
        }
    }

    private void removeViews() {
        linearLayout.removeAllViews();
    }

    public boolean isEmpty() {
        return linearLayout == null || linearLayout.getChildCount() == 0;
    }

    public boolean isListSizeChanged(int listSize) {
        return listSize != bankDetailsList.size();
    }

    public void notifyDataSetChanged() {
        removeViews();
        addItems();
    }

    public void setBankChanged(BankChanged bankChanged) {
        this.bankChanged = bankChanged;
    }

    public void clearCheck() {
        lastChecked.setChecked(false);
    }

    @Override
    public void onClick(View v) {
        if (bankChanged != null) {
            lastChecked.setChecked(false);
            lastChecked = ((CheckBox) v.findViewById(R.id.checkBox));
            lastChecked.setChecked(true);
            bankChanged.onBankChange((WalletResponse.RefundTypesEntity.BankDetailsEntity) v.getTag());
        }
    }

    public interface BankChanged {
        void onBankChange(WalletResponse.RefundTypesEntity.BankDetailsEntity bank);
    }
}
