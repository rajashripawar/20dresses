package outletwise.com.twentydresses.controller;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.telephony.SmsManager;
import android.telephony.SmsMessage;
import android.util.Log;

/**
 * Created by User-PC on 07-11-2015.
 */
public class SmsListener extends BroadcastReceiver {

    public static String MESSAGE = "message";

    // Get the object of SmsManager
    final SmsManager sms = SmsManager.getDefault();

    public void onReceive(Context context, Intent intent) {

        // Retrieves a map of extended data from the intent.
        final Bundle bundle = intent.getExtras();

        try {

            if (bundle != null) {
                final Object[] pdusObj = (Object[]) bundle.get("pdus");

                for (int i = 0; i < pdusObj.length; i++) {

                    SmsMessage currentMessage = SmsMessage.createFromPdu((byte[]) pdusObj[i]);
                    String phoneNumber = currentMessage.getDisplayOriginatingAddress();
                    String senderNum = phoneNumber;
                    String message = currentMessage.getDisplayMessageBody();

                       /* Intent intent1 = new Intent(VerifyOrderActivity.ACTION_RESP);
                        intent1.putExtra(MESSAGE, message);
                        context.sendBroadcast(intent1);*/
                    String otp = message.substring(message.indexOf(":") + 2, message.indexOf("to")).trim();

                    Intent service = new Intent(context, VerifyMobileService.class);
                    service.putExtra(VerifyMobileService.OTP, otp);
                    context.startService(service);
                    Log.i("SmsReceiver", "senderNum: " + senderNum + "; message: " + message);

                } // end for loop
            } // bundle is null

        } catch (Exception e) {
            Log.e("SmsReceiver", "Exception smsReceiver" + e);

        }
    }
}