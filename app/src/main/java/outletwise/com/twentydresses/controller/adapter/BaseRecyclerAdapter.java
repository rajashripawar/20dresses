package outletwise.com.twentydresses.controller.adapter;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.View;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by User-PC on 08-08-2015.
 */
public abstract class BaseRecyclerAdapter extends RecyclerView.Adapter<BaseRecyclerAdapter.BaseViewHolder> {
    public Context context;
    public ArrayList items;
    public OnItemClickListener mItemClickListener;

    public BaseRecyclerAdapter(Context context, ArrayList items) {
        this.context = context;
        this.items = items;
    }

    public static abstract class BaseViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {

        public BaseViewHolder(View itemView) {
            super(itemView);
            itemView.setOnClickListener(this);
        }
    }

    public void setOnItemClickListener(final OnItemClickListener mItemClickListener) {
        this.mItemClickListener = mItemClickListener;
    }

    public void addItem(Object item) {
        items.add(item);
        notifyItemInserted(items.size() - 1);
    }

    public void removeItem(Object item) {
        items.remove(item);
        notifyDataSetChanged();
    }

    public void removeItem(int position) {
        items.remove(position);
        notifyDataSetChanged();
    }

    @Override
    public int getItemCount() {
        return items.size();
    }

    public Object getItemAtPosition(int position) {
        return items.get(position);
    }

    public List getItems() {
        return items;
    }

    public interface OnItemClickListener {
        void onItemClick(View view, int position);
    }
}
