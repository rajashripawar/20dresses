package outletwise.com.twentydresses.controller.adapter;

import android.app.Activity;
import android.content.Context;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import java.util.List;

import outletwise.com.twentydresses.R;
import outletwise.com.twentydresses.model.Reason;

/**
 * Created by User-PC on 22-12-2015.
 */
public class ReasonsAdapter extends ArrayAdapter<Reason.ReasonsEntity> {
    private Context context;

    public ReasonsAdapter(Context context, int resource, List<Reason.ReasonsEntity> reasons) {
        super(context, resource, reasons);
        this.context = context;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        return getCustomView(position, convertView, parent);
    }

    @Override
    public View getDropDownView(int position, View convertView, ViewGroup parent) {
        return getCustomView(position, convertView, parent);
    }

    public View getCustomView(int position, View convertView, ViewGroup parent) {
        if (convertView == null)
            convertView = ((Activity) context).getLayoutInflater().inflate(R.layout.layout_adapter_reasons, parent, false);

        ((TextView) convertView.findViewById(R.id.text)).setText(getItem(position).getReason());
        convertView.setTag(getItem(position));
        return convertView;
    }
}