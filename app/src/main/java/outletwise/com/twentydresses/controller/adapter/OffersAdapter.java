package outletwise.com.twentydresses.controller.adapter;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.content.res.Resources;
import android.support.v7.widget.CardView;
import android.text.TextUtils;
import android.util.TypedValue;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.GridView;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.squareup.picasso.Picasso;

import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.Arrays;

import outletwise.com.twentydresses.R;
import outletwise.com.twentydresses.model.HomeBanner;
import outletwise.com.twentydresses.model.Offer;
import outletwise.com.twentydresses.model.Product;
import outletwise.com.twentydresses.utilities.Constants;
import outletwise.com.twentydresses.view.activity.ProductViewActivity;

/**
 * Created by User-PC on 02-12-2015.
 */
public class OffersAdapter extends BaseRecyclerAdapter {
    private Context mContext;
    private ArrayList<HomeBanner.TodayOfferBean> todayOfferBeanArrayList;
    public OnBannerClickListener mOnBannerClickListener;
    private ArrayList bannerItems;
    private ArrayList<Product> products;
    private static int temp = 0;

  /*  public OffersAdapter(Context context, ArrayList items) {
        super(context, items);
        this.mContext = context;
    }*/

    public OffersAdapter(Context context, HomeBanner bannerResponse, ArrayList<Product> products) {
        super(context, bannerResponse.getBanners());
        this.mContext = context;
        todayOfferBeanArrayList = bannerResponse.getToday_offer();
        bannerItems = bannerResponse.getBanners();
        this.products = products;

        temp = bannerItems.size() + 1;

    }

   /* @Override
    public void onClick(View v) {
        mOnBannerClickListener.onbannerClick((Integer) v.getTag() , String.valueOf(v.getId()));
    }*/

    class OffersHolder extends BaseViewHolder {

        ImageView banner, ivBanner1, ivBanner2;
        RelativeLayout rlBanner;
        LinearLayout ll_hori_banner;
        CardView cv_banner;
        GridView gridView;
        TextView tv_just_sold, tv_see_more;


        public OffersHolder(View itemView) {
            super(itemView);
            banner = (ImageView) itemView.findViewById(R.id.ivBanner);
            rlBanner = (RelativeLayout) itemView.findViewById(R.id.rl_banner);
            ll_hori_banner = (LinearLayout) itemView.findViewById(R.id.ll_hori_banner);
            ivBanner1 = (ImageView) itemView.findViewById(R.id.iv_banner1);
            ivBanner2 = (ImageView) itemView.findViewById(R.id.iv_banner2);
            cv_banner = (CardView) itemView.findViewById(R.id.cv_banner);
            gridView = (GridView) itemView.findViewById(R.id.gridview);
            tv_just_sold = (TextView) itemView.findViewById(R.id.tv_just_sold);
            tv_see_more = (TextView) itemView.findViewById(R.id.tv_see_more);
        }

        @Override
        public void onClick(View v) {
           /* if (mItemClickListener != null)
                mItemClickListener.onItemClick(v, getAdapterPosition());*/
            // mOnBannerClickListener.onbannerClick((String) v.getTag());
        }
    }

    @Override
    public BaseViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.layout_adapter_offers, parent, false);
        return new OffersHolder(view);
    }

    @Override
    public int getItemCount() {
        return bannerItems.size() + 1;
    }

    @Override
    public void onBindViewHolder(BaseViewHolder holder, final int position) {

        if (position != bannerItems.size() && position < temp) {

            ((OffersHolder) holder).gridView.setVisibility(View.GONE);
            ((OffersHolder) holder).tv_just_sold.setVisibility(View.GONE);
            HomeBanner.BannersBean bannersBeen = (HomeBanner.BannersBean) items.get(position);

            if (bannersBeen.getBanner_type().equalsIgnoreCase("Large")) {
                ((OffersHolder) holder).ll_hori_banner.setVisibility(View.GONE);
                ((OffersHolder) holder).banner.setVisibility(View.VISIBLE);
                ((OffersHolder) holder).cv_banner.setVisibility(View.VISIBLE);

                if (!TextUtils.isEmpty(bannersBeen.getData().get(0).getOffer_image()))
                    Picasso.with(context).load(bannersBeen.getData().get(0).getOffer_image()).into(((OffersHolder) holder).banner);

          /*  ((OffersHolder) holder).banner.setTag(position);*/
                ((OffersHolder) holder).banner.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        mOnBannerClickListener.OnBannerClick(position, 0);
                    }
                });
            }

            if (bannersBeen.getBanner_type().equalsIgnoreCase("Small")) {
                ((OffersHolder) holder).banner.setVisibility(View.GONE);
                ((OffersHolder) holder).cv_banner.setVisibility(View.GONE);
                ((OffersHolder) holder).ll_hori_banner.setVisibility(View.VISIBLE);

                if (!TextUtils.isEmpty(bannersBeen.getData().get(0).getOffer_image()))
                    Picasso.with(context).load(bannersBeen.getData().get(0).getOffer_image()).into(((OffersHolder) holder).ivBanner1);

                if (!TextUtils.isEmpty(bannersBeen.getData().get(1).getOffer_image()))
                    Picasso.with(context).load(bannersBeen.getData().get(1).getOffer_image()).into(((OffersHolder) holder).ivBanner2);

                ((OffersHolder) holder).ivBanner1.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        mOnBannerClickListener.OnBannerClick(position, 0);
                    }
                });
                ((OffersHolder) holder).ivBanner2.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        mOnBannerClickListener.OnBannerClick(position, 1);
                    }
                });

            }

        } else {
            JustSoldAdapter justSoldAdapter = new JustSoldAdapter(mContext, products);
            ((OffersHolder) holder).gridView.setAdapter(justSoldAdapter);
            ((OffersHolder) holder).gridView.setVisibility(View.VISIBLE);
            ((OffersHolder) holder).tv_just_sold.setVisibility(View.VISIBLE);

            ((OffersHolder) holder).tv_see_more.setVisibility(View.VISIBLE);

            int height = getGridHeight(products.size());
            height = height * 195;
            ViewGroup.LayoutParams layoutParams = ((OffersHolder) holder).gridView.getLayoutParams();
            layoutParams.height = convertDpToPixels(height, mContext);
            ((OffersHolder) holder).gridView.setLayoutParams(layoutParams);

            ((OffersHolder) holder).cv_banner.setVisibility(View.GONE);
            ((OffersHolder) holder).ll_hori_banner.setVisibility(View.GONE);


            ((OffersHolder) holder).gridView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
                @Override
                public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {
                   /* Product product = (Product) view.getTag();

                    Intent intent = new Intent(mActivity, ProductViewActivity.class);
                    intent.putExtra(Constants.PRODUCT_NAME, product);
                    intent.putExtra(Constants.CATEGORY, "");
                    startActivity(intent);*/

                    if (mOnBannerClickListener != null)
                        mOnBannerClickListener.OnGridItemClick(view);

                }

            });

            ((OffersHolder) holder).tv_see_more.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    if (mOnBannerClickListener != null)
                        mOnBannerClickListener.OnSeeMoreClick();


                }
            });
        }
    }

    protected int getGridHeight(int arrayListSize) {

        DecimalFormat df = new DecimalFormat("##.##");
        float a1 = arrayListSize / 3.0f;
        String a1Str = df.format(a1);
        System.out.println("a1Str:" + a1Str);
        int gridHeight = 0;

        if (a1Str.contains(".33")) {
            gridHeight = (int) a1 + 1;
        } else if (a1Str.contains(".67")) {
            gridHeight = (int) a1 + 1;
        } else {
            gridHeight = (int) a1;
        }
        return gridHeight;

                  /*  if (arrayListSoldOut.size() % 3 == 0)
                height = (arrayListSoldOut.size() / 3) * 200;
            else
                height = ((arrayListSoldOut.size() / 3) + 1) * 200;*/
    }

    public static int convertDpToPixels(float dp, Context context) {
        Resources resources = context.getResources();
        return (int) TypedValue.applyDimension(
                TypedValue.COMPLEX_UNIT_DIP,
                dp,
                resources.getDisplayMetrics()
        );
    }

    public void setOnBannerClickListener(final OnBannerClickListener mOnBannerClickListener) {
        this.mOnBannerClickListener = mOnBannerClickListener;
    }

    public interface OnBannerClickListener {
        void OnBannerClick(int position, int item_pos);

        void OnGridItemClick(View view);

        void OnSeeMoreClick();
    }
}
