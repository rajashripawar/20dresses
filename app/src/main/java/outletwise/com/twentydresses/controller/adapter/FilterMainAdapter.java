package outletwise.com.twentydresses.controller.adapter;

import android.content.Context;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import java.util.ArrayList;

import outletwise.com.twentydresses.R;
import outletwise.com.twentydresses.model.Category;
import outletwise.com.twentydresses.model.Filter;
import outletwise.com.twentydresses.utilities.Constants;
import outletwise.com.twentydresses.view.decoration.DividerItemDecoration;

/**
 * Created by User-PC on 30-11-2015.
 */
public class FilterMainAdapter extends BaseRecyclerAdapter {

    private RecyclerView rvDetail;
    private Filter filter;
    private Object subFilters;
    private FilterDetailAdapter adapter;
    private View selectedView;
    private int itemCount = 0;
    private Category.SubCat subCat;

    public FilterMainAdapter(Context context, Filter filter, Category.SubCat SubCat) {
        super(context, null);
        this.filter = filter;
        this.subCat = SubCat;

        if (subCat.getId().equalsIgnoreCase("1") && subCat.getName().equalsIgnoreCase(Constants.SALE) || subCat.getName().equalsIgnoreCase(Constants.NEW)) {
            if (filter.getCategories() != null && filter.getCategories().getValues().size() > 0)
                itemCount++;
        } else if (filter.getSizes() != null && filter.getSizes().getValues().size() > 0)
            itemCount++;

        if (filter.getColors() != null && filter.getColors().getValues().size() > 0)
            itemCount++;
        if (!subCat.getName().equalsIgnoreCase(Constants.NEW) && filter.getDiscounts() != null && filter.getDiscounts().getValues().size() > 0)
            itemCount++;

    }

    public ArrayList<String> getAllAppliedFilter_Color() {
        return adapter.getAllAppliedFilter_Color();
    }

    public String getParameterColor() {
        return adapter.getParameter_color();
    }


    public ArrayList<String> getAllAppliedFilter_Size() {
        return adapter.getAllAppliedFilter_Size();
    }

    public String getParameterSize() {
        return adapter.getParameter_size();
    }

    public String getParameterDiscount() {
        return adapter.getParameter_discount();
    }

    public String getAllAppliedFilter_Discount() {
        return adapter.getAllAppliedFilter_Discount();
    }

    public ArrayList<String> getAllAppliedFilter_Category() {
        return adapter.getAllAppliedFilter_Category();
    }

    public String getParameterCategory() {
        return adapter.getParameter_category();
    }

    public class FilterHolder extends BaseViewHolder {
        TextView tvText;

        public FilterHolder(View itemView) {
            super(itemView);
            tvText = (TextView) itemView.findViewById(R.id.tvText);
        }

        @Override
        public void onClick(View v) {

            if (subCat.getName().equalsIgnoreCase(Constants.NEW)) {
                switch (getAdapterPosition()) {
                    case 0:
                        subFilters = filter.getCategories();
                        break;
                    case 1:
                        subFilters = filter.getColors();
                        break;
                }
            } else if (subCat.getName().equalsIgnoreCase(Constants.SALE)) {
                switch (getAdapterPosition()) {
                    case 0:
                        subFilters = filter.getCategories();
                        break;
                    case 1:
                        subFilters = filter.getColors();
                        break;
                    case 2:
                        subFilters = filter.getDiscounts();
                        break;
                }
            } else {
                if (filter.getSizes().getValues().size() > 0) {
                    switch (getAdapterPosition()) {
                        case 0:
                            subFilters = filter.getColors();
                            break;
                        case 1:
                            subFilters = filter.getSizes();
                            break;
                        case 2:
                            subFilters = filter.getDiscounts();
                            break;
                    }
                } else {
                    switch (getAdapterPosition()) {
                        case 0:
                            subFilters = filter.getColors();
                            break;
                        case 1:
                            subFilters = filter.getDiscounts();
                            break;
                    }
                }
            }

            if (selectedView != null)
                selectedView.setSelected(false);
            selectedView = v;
            selectedView.setSelected(true);

            rvDetail.setLayoutManager(new LinearLayoutManager(context));
//          rvDetail.addItemDecoration(new DividerItemDecoration(context, DividerItemDecoration.VERTICAL_LIST));
            adapter.setSubFilter(subFilters);
            rvDetail.setAdapter(adapter);
            notifyDataSetChanged();
        }
    }

    @Override
    public BaseViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.layout_adapter_main_filter, parent, false);
        return new FilterHolder(view);
    }

    @Override
    public void onBindViewHolder(BaseViewHolder holder, int position) {

        if (subCat.getName().equalsIgnoreCase(Constants.NEW)) {
            switch (position) {
                case 0:
                    case_category(holder);
                    break;
                case 1:
                    case_color(holder);
                    break;
            }
        } else if (subCat.getName().equalsIgnoreCase(Constants.SALE)) {
            switch (position) {
                case 0:
                    case_category(holder);
                    break;
                case 1:
                    case_color(holder);
                    break;
                case 2:
                    case_discount(holder);
                    break;
            }
        } else {
            if (filter.getSizes().getValues().size() > 0) {
                switch (position) {
                    case 0:
                        case_color(holder);
                        break;
                    case 1:
                        case_size(holder);
                        break;
                    case 2:
                        case_discount(holder);
                        break;
                }
            } else {
                switch (position) {
                    case 0:
                        case_color(holder);
                        break;
                    case 1:
                        case_discount(holder);
                        break;
                }
            }
        }

    }


    private void case_discount(BaseViewHolder holder) {
        if (filter.getDiscounts().getValues().size() > 0) {
            if (filter.getDiscounts().getAppliedFiltersSize() > 0) {
                ((FilterHolder) holder).tvText.setText(filter.getDiscounts().getName()
                        + " (" + filter.getDiscounts().getAppliedFiltersSize() + ")");
                // ((FilterHolder) holder).tvText.setSelected(true);
            } else
                ((FilterHolder) holder).tvText.setText(filter.getDiscounts().getName());
        }
    }


    private void case_color(BaseViewHolder holder) {
        subFilters = filter.getColors();
        if (filter.getColors().getValues().size() > 0) {
            if (filter.getColors().getAppliedFiltersSize() > 0) {
                ((FilterHolder) holder).tvText.setText(filter.getColors().getName()
                        + " (" + filter.getColors().getAppliedFiltersSize() + ")");
                //  ((FilterHolder) holder).tvText.setSelected(true);
            } else
                ((FilterHolder) holder).tvText.setText(filter.getColors().getName());
        }

        if (adapter == null) {
            adapter = new FilterDetailAdapter(context, subFilters, subCat.getId(), rvDetail);
            rvDetail.setLayoutManager(new LinearLayoutManager(context));
            rvDetail.addItemDecoration(new DividerItemDecoration(context, DividerItemDecoration.VERTICAL_LIST));
            rvDetail.setAdapter(adapter);
        }
    }


    private void case_size(BaseViewHolder holder) {
        if (filter.getSizes().getValues().size() > 0) {
            if (filter.getSizes().getAppliedFiltersSize() > 0) {
                ((FilterHolder) holder).tvText.setText(filter.getSizes().getName()
                        + " (" + filter.getSizes().getAppliedFiltersSize() + ")");
                //  ((FilterHolder) holder).tvText.setSelected(true);
            } else
                ((FilterHolder) holder).tvText.setText(filter.getSizes().getName());
        }
    }


    private void case_category(BaseViewHolder holder) {

        subFilters = filter.getCategories();
        if (filter.getCategories().getValues().size() > 0) {
            if (filter.getCategories().getAppliedFiltersSize() > 0) {
                ((FilterHolder) holder).tvText.setText(filter.getCategories().getName()
                        + " (" + filter.getCategories().getAppliedFiltersSize() + ")");
                //    ((FilterHolder) holder).tvText.setSelected(true);
            } else
                ((FilterHolder) holder).tvText.setText(filter.getCategories().getName());
            if (adapter == null) {
                adapter = new FilterDetailAdapter(context, subFilters, subCat.getId(), rvDetail);
                rvDetail.setLayoutManager(new LinearLayoutManager(context));
                rvDetail.addItemDecoration(new DividerItemDecoration(context, DividerItemDecoration.VERTICAL_LIST));
                rvDetail.setAdapter(adapter);
            }
        }
    }

    @Override
    public int getItemCount() {
        return itemCount;
    }

    public void setRvDetail(RecyclerView rvDetail) {
        this.rvDetail = rvDetail;
    }
}
