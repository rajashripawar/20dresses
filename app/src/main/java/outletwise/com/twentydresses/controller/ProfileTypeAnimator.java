package outletwise.com.twentydresses.controller;

import android.view.View;
import android.view.animation.AccelerateDecelerateInterpolator;
import android.view.animation.Animation;
import android.view.animation.AnimationSet;
import android.view.animation.RotateAnimation;
import android.view.animation.ScaleAnimation;

import java.util.List;

/**
 * Created by User-PC on 27-08-2015.
 */
public class ProfileTypeAnimator implements View.OnClickListener {
   // private float[] angles = {90, 150, 210, 270, 300, 330};
    private float[] angles = {60, 120, 180, 240, 300, 360};
    private List<View> views;

    public ProfileTypeAnimator(List<View> views) {
        this.views = views;
    }

    public void animateViews() {
        for (int i = 0; i < angles.length; i++) {
            float angleDeg = angles[i];
            View v = views.get(i);
            float startScaleX, startScaleY, stopScaleX, stopScaleY, rotateAngle = 360.0f;
            float rotateAngleFrom = angleDeg, rotateAngleTo = angleDeg + rotateAngle;
            long animationDuration = 3000;
            startScaleX = 0.2f;
            startScaleY = 0.2f;
            stopScaleX = 0.5f + Math.min(((float) (int) v.getTag()) / 100, 0.60f);
            stopScaleY = 0.5f + Math.min(((float) (int) v.getTag()) / 100, 0.60f);
            if (i % 2 == 0) {
                rotateAngleFrom = rotateAngleTo;
                rotateAngleTo = angleDeg;
                v.bringToFront();
            }

            ScaleAnimation scaleAnimation = new ScaleAnimation(startScaleX, stopScaleX, startScaleY, stopScaleY,
                    Animation.RELATIVE_TO_SELF, 0.5f, Animation.RELATIVE_TO_SELF, 1);
            scaleAnimation.setInterpolator(new AccelerateDecelerateInterpolator());

            RotateAnimation rotateAnimation = new RotateAnimation(rotateAngleTo, rotateAngleFrom,
                    Animation.RELATIVE_TO_SELF, 0.5f, Animation.RELATIVE_TO_SELF, 1);
            rotateAnimation.setInterpolator(new AccelerateDecelerateInterpolator());

            AnimationSet set = new AnimationSet(true);
            set.addAnimation(scaleAnimation);
            set.addAnimation(rotateAnimation);
            set.setFillAfter(true);
            set.setDuration(animationDuration);
            v.startAnimation(set);
            v.setOnClickListener(this);
        }
    }


    @Override
    public void onClick(View v) {

    }
}
