package outletwise.com.twentydresses.controller.adapter;

import android.content.Context;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;
import android.support.v4.app.FragmentStatePagerAdapter;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.List;

import outletwise.com.twentydresses.R;

/**
 * Created by User-PC on 28-01-2016.
 */
public class HomeTabAdapter extends FragmentStatePagerAdapter {
    private final List<Fragment> mFragments = new ArrayList<>();
    private final List<String> mFragmentTitles = new ArrayList<>();
    private final List<TextView> tvTitles = new ArrayList<>();
    private TextView selectedTextView;

    private Context context;

    public HomeTabAdapter(Context context, FragmentManager fm) {
        super(fm);
        this.context = context;
    }

    public void addFragment(Fragment fragment, String title) {
        mFragments.add(fragment);
        mFragmentTitles.add(title);
    }

    @Override
    public Fragment getItem(int position) {

     /*   if(position == 2)
        {
            View v = LayoutInflater.from(context).inflate(R.layout.layout_home_tabs, null);
            v.
        }*/

        return mFragments.get(position);
    }

    @Override
    public int getCount() {
        return mFragments.size();
    }

    @Override
    public CharSequence getPageTitle(int position) {
        return mFragmentTitles.get(position);
    }

    public View getTabView(int position) {
        View v = LayoutInflater.from(context).inflate(R.layout.layout_home_tabs, null);
        TextView tvTitle = (TextView) v.findViewById(R.id.tvTitle);
        tvTitle.setText(getPageTitle(position));
        tvTitles.add(tvTitle);
        if (position == 0)
            setNewSelectedTextView(position);

        TextView tvBadge = (TextView) v.findViewById(R.id.tvTitleBadge);
//        tvBadge.setText("12"); //// TODO: 18-12-2015 Remove hardcoded values
        return v;
    }

    public void setNewSelectedTextView(int pos) {
        TextView selectedTextView = tvTitles.get(pos);
        if (this.selectedTextView != null)
        this.selectedTextView.setSelected(false);
        this.selectedTextView = selectedTextView;
        this.selectedTextView.setSelected(true);
    }

}
