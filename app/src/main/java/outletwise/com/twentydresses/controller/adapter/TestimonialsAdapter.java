package outletwise.com.twentydresses.controller.adapter;

import android.content.Context;
import android.content.Intent;
import android.text.Html;
import android.text.Spannable;
import android.text.SpannableString;
import android.text.style.ForegroundColorSpan;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

import com.squareup.picasso.Picasso;

import java.util.ArrayList;

import outletwise.com.twentydresses.R;
import outletwise.com.twentydresses.model.Testimonial;
import outletwise.com.twentydresses.model.TestimonialSocial;
import outletwise.com.twentydresses.utilities.Constants;
import outletwise.com.twentydresses.view.activity.ProductViewActivity;

/**
 * Created by User-PC on 17-11-2015.
 */
public class TestimonialsAdapter extends BaseRecyclerAdapter {
    String temp = " ";

    public TestimonialsAdapter(Context context, ArrayList items) {
        super(context, items);
        Log.e("","items size:::::"+items.size());
    }

    class TestimonialViewHolder extends BaseViewHolder {
        public TextView tvName;
        public TextView tvDate;
        public TextView tvLocation;
        public TextView tvFrom;
        public TextView tvTestimonialText;
        public ImageView ivProfile;
        public ImageView ivBigProfile;
        public Button btnviewproduct;

        public TestimonialViewHolder(View itemView) {
            super(itemView);

            tvName = (TextView) itemView.findViewById(R.id.tvName);
            tvDate = (TextView) itemView.findViewById(R.id.tvDate);
            tvLocation = (TextView) itemView.findViewById(R.id.tvLocation);
            tvFrom = (TextView) itemView.findViewById(R.id.tvFrom);

            tvTestimonialText = (TextView) itemView.findViewById(R.id.tvTestimonialText);
            ivProfile = (ImageView) itemView.findViewById(R.id.ivProfile);
            ivBigProfile = (ImageView) itemView.findViewById(R.id.ivbigProfile);
            btnviewproduct = (Button)itemView.findViewById(R.id.btnviewproduct);

        }

        @Override
        public void onClick(View v) {

        }
    }

    @Override
    public BaseViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.layout_adapter_testimonial, parent, false);
        return new TestimonialViewHolder(view);
    }

    @Override
    public void onBindViewHolder(BaseViewHolder holder, int position) {
        TestimonialViewHolder testimonialViewHolder = (TestimonialViewHolder) holder;
        TestimonialSocial.Testimonials testimonial = (TestimonialSocial.Testimonials) items.get(position);

        testimonialViewHolder.tvName.setText(testimonial.getName());
        testimonialViewHolder.tvLocation.setText(testimonial.getLocation());
        testimonialViewHolder.tvDate.setText(testimonial.getDate());
        testimonialViewHolder.tvFrom.setText("via "+testimonial.getTestimonial_via());

        String lefQuot = "\u201C";
        String rightQuot = "\u201D";
        String finalString = "";
        if (testimonial.getTestimonial() != null)
            finalString = lefQuot + Html.fromHtml(testimonial.getTestimonial()) + rightQuot;
        else
            finalString = lefQuot + " " + rightQuot;

        SpannableString sp = new SpannableString(finalString);
        sp.setSpan(new ForegroundColorSpan(context.getResources().getColor(R.color.color_text_primary)),
                1, finalString.length() - 1, Spannable.SPAN_INCLUSIVE_INCLUSIVE);

        /*   sp.setSpan(new RelativeSizeSpan(0.5f),
                1, finalString.length() - 1, Spannable.SPAN_INCLUSIVE_INCLUSIVE);*/

        testimonialViewHolder.tvTestimonialText.setText(sp);

        if (testimonial.getImage() != null)
        Picasso.with(context).load(testimonial.getImage()).into(testimonialViewHolder.ivProfile);
        if (testimonial.getPosted_image() != null){
            testimonialViewHolder.ivBigProfile.setVisibility(View.VISIBLE);
            Picasso.with(context).load(testimonial.getPosted_image()).into(testimonialViewHolder.ivBigProfile);
        }else {
            testimonialViewHolder.ivBigProfile.setVisibility(View.GONE);
        }

        if (testimonial.getProduct_id() != null && Integer.parseInt(testimonial.getProduct_id()) != 0){
            final String id =testimonial.getProduct_id();
            testimonialViewHolder.btnviewproduct.setVisibility(View.VISIBLE);
            testimonialViewHolder.btnviewproduct.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    Intent intent = new Intent(context, ProductViewActivity.class);
                    intent.putExtra(Constants.SINGLE_ITEM, ""+ id );
                    context.startActivity(intent);
                }
            });
        }else {
            testimonialViewHolder.btnviewproduct.setVisibility(View.GONE);
        }

    }
}
