package outletwise.com.twentydresses.controller;

import android.app.IntentService;
import android.app.NotificationManager;
import android.content.Intent;
import android.content.SharedPreferences;
import android.preference.PreferenceManager;
import android.support.v7.app.NotificationCompat;

import com.loopj.android.http.AsyncHttpClient;
import com.loopj.android.http.AsyncHttpResponseHandler;
import com.loopj.android.http.SyncHttpClient;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.UnsupportedEncodingException;

import cz.msebera.android.httpclient.Header;
import cz.msebera.android.httpclient.entity.StringEntity;
import outletwise.com.twentydresses.R;
import outletwise.com.twentydresses.utilities.Constants;
import outletwise.com.twentydresses.utilities.Urls;

/**
 * Created by User-PC on 08-01-2016.
 */
public class VerifyMobileService extends IntentService {
    private AsyncHttpClient aClient = new SyncHttpClient();
    private SharedPreferences preferences;
    public static String OTP = "otp";
    //   Context context;
    StringEntity entity;

    /**
     * Creates an IntentService.  Invoked by your subclass's constructor.
     */
    public VerifyMobileService() {
        super("Service");
    }

    @Override
    protected void onHandleIntent(Intent intent) {
        preferences = PreferenceManager.getDefaultSharedPreferences(this);
        // Constants.debug("Service started...");
        JSONObject jsonObject = new JSONObject();
        try {
            jsonObject.put(Constants.USER_ID, preferences.getLong(Constants.USER_ID, 0));
            jsonObject.put(Constants.AUTH_TOKEN, preferences.getString(Constants.KEY_TOKEN, ""));
            jsonObject.put(Constants.MOBILE, preferences.getString(Constants.MOBILE, ""));

            int order_id = preferences.getInt(Constants.ORDER_ID, 0);
            String otp = intent.getStringExtra(OTP);

            if (order_id != 0)
                jsonObject.put(Constants.ORDER_ID, order_id);
            jsonObject.put(Constants.OTP, otp);
        } catch (JSONException e) {
            e.printStackTrace();
        }


        try {
            entity = new StringEntity(jsonObject.toString());
        } catch (UnsupportedEncodingException e) {
            e.printStackTrace();
        }


        aClient.post(this, Urls.USER_VERIFY_MOBILE, entity, "application/json",
                new AsyncHttpResponseHandler() {

                    @Override
                    public void onSuccess(int statusCode, Header[] headers, byte[] responseBody) {
                        String s = new String(responseBody);
                        Constants.debug("..." + s);
                        showNotification();
                        stopSelf();
                    }

                    @Override
                    public void onFailure(int statusCode, Header[] headers, byte[] responseBody, Throwable error) {
                        String s = new String(responseBody);
                        Constants.debug("..." + s);
                        stopSelf();
                    }

                });

    }

    private void showNotification() {

        NotificationCompat.Builder builder = (NotificationCompat.Builder) new NotificationCompat.Builder(this)
                .setSmallIcon(R.mipmap.ic_launcher).setWhen(System.currentTimeMillis())
                .setAutoCancel(false)
                .setContentText("Mobile verified successfully..!!");

        NotificationManager mNotifyMgr =
                (NotificationManager) getSystemService(NOTIFICATION_SERVICE);
        //  Notification notif = builder.getNotification();

        mNotifyMgr.notify(1, builder.build());
    }


}
