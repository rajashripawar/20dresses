package outletwise.com.twentydresses.controller;

import java.util.ArrayList;

import outletwise.com.twentydresses.ApplicationUtil;
import outletwise.com.twentydresses.model.database.greenbot.Cart;
import outletwise.com.twentydresses.utilities.Constants;
import outletwise.com.twentydresses.utilities.database.DatabaseCallBack;
import outletwise.com.twentydresses.utilities.database.DatabaseOperation;

/**
 * Created by User-PC on 09-10-2015.
 */
public class CartOperations implements DatabaseCallBack {
    private static CartOperations cartOperations;
    private CartUpdation cartUpdation;
    private ArrayList<Cart> cartList = new ArrayList<>();

    private CartOperations() {
    }

    public static CartOperations getInstance() {
        if (cartOperations == null)
            cartOperations = new CartOperations();

        return cartOperations;
    }

    public void replaceItems(ArrayList<Cart> cartItems) {
        cartList = cartItems;
        new DatabaseOperation(this, ApplicationUtil.getInstance(), null,
                Constants.TAG_CART_REPLACE_ALL, Constants.Records.DELETE_ALL).execute();
    }

    public void addItems(ArrayList<Cart> cartItems) {
        new DatabaseOperation(this, ApplicationUtil.getInstance(), cartItems,
                Constants.TAG_CART_ADD, Constants.Records.INSERT_ALL).execute();
    }

    public void addItem(Cart cartItem) {
        new DatabaseOperation(this, ApplicationUtil.getInstance(), cartItem,
                Constants.TAG_CART_ADD, Constants.Records.INSERT).execute();
    }

    public void updateItem(Cart cartItem) {
        new DatabaseOperation(this, ApplicationUtil.getInstance(), cartItem,
                Constants.TAG_CART_UPDATE, Constants.Records.UPDATE).execute();
    }

    public void removeItem(String productId) {
        new DatabaseOperation(this, ApplicationUtil.getInstance(), productId,
                Constants.TAG_CART_DELETE, Constants.Records.DELETE).execute();
    }

    public void removeAll() {
        new DatabaseOperation(this, ApplicationUtil.getInstance(), null,
                Constants.TAG_CART_DELETE_ALL, Constants.Records.DELETE_ALL).execute();
    }

    public ArrayList<Cart> getCartList() {
        return cartList;
    }

    public void setCartList(ArrayList<Cart> cartList) {
        if (cartList != null)
            this.cartList = cartList;
    }

    public int getCartSize() {
        return cartList.size() == 0 ? Integer.MIN_VALUE : cartList.size();
    }

    public int getNoOfItems() {
        return cartList.size();
    }

    @Override
    public void onCompleteInsertion(int tag, String msg) {
        new DatabaseOperation(this, ApplicationUtil.getInstance(), null,
                Constants.TAG_CART_SELECT, Constants.Records.SELECT).execute();
    }

    @Override
    public void onCompleteUpdation(int tag, String msg) {
        new DatabaseOperation(this, ApplicationUtil.getInstance(), null,
                Constants.TAG_CART_UPDATE, Constants.Records.UPDATE).execute();
    }

    @Override
    public void onCompleteRetrieval(int tag, ArrayList obj, String msg) {
        cartList = obj;
        if (cartUpdation != null)
            cartUpdation.onCartUpdate();
    }

    @Override
    public void onCompleteDeletion(int tag, String msg) {
        switch (tag) {
            case Constants.TAG_CART_REPLACE_ALL:
                new DatabaseOperation(this, ApplicationUtil.getInstance(), cartList,
                        Constants.TAG_CART_ADD, Constants.Records.INSERT_ALL).execute();
                break;
            default:
                new DatabaseOperation(this, ApplicationUtil.getInstance(), null,
                        Constants.TAG_CART_SELECT, Constants.Records.SELECT).execute();
        }
    }

    @Override
    public void onDatabaseError(int tag, int status, String msg) {
        Constants.error(msg);
    }

    public void setCartUpdation(CartUpdation cartUpdation) {
        this.cartUpdation = cartUpdation;
    }

    public interface CartUpdation {
        void onCartUpdate();
    }
}
