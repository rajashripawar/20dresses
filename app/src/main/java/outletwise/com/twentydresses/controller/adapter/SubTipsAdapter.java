package outletwise.com.twentydresses.controller.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import java.util.ArrayList;

import outletwise.com.twentydresses.R;

/**
 * Created by User-PC on 28-09-2015.
 */
public class SubTipsAdapter extends BaseRecyclerAdapter {

    public SubTipsAdapter(Context context, ArrayList items) {
        super(context, items);
    }

    class TipsViewHolder extends BaseViewHolder {
        public TextView tvTipNo;
        public TextView tvExtraTip;


        public TipsViewHolder(View itemView) {
            super(itemView);
            tvExtraTip = (TextView) itemView.findViewById(R.id.tvExtraTip);
            tvTipNo = (TextView) itemView.findViewById(R.id.tvExtraTipNo);
        }

        @Override
        public void onClick(View v) {
        }
    }

    @Override
    public BaseViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.layout_adapter_tips, parent, false);
        return new TipsViewHolder(view);
    }

    @Override
    public void onBindViewHolder(BaseViewHolder holder, int position) {
        TipsViewHolder tipsViewHolder = (TipsViewHolder) holder;
        tipsViewHolder.itemView.setTag(items.get(position));

        tipsViewHolder.tvExtraTip.setText((String) items.get(position));
        tipsViewHolder.tvTipNo.setText("#" + (position + 1));
    }
}
