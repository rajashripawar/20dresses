package outletwise.com.twentydresses.controller.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.GridView;
import android.widget.ImageView;
import android.widget.TextView;

import com.squareup.picasso.Picasso;

import java.util.ArrayList;

import outletwise.com.twentydresses.R;
import outletwise.com.twentydresses.model.Product;
import outletwise.com.twentydresses.utilities.Constants;

public class JustSoldAdapter extends BaseAdapter {
    private Context mContext;
    private ArrayList<Product> mThumbIds;

    // Constructor
    public JustSoldAdapter(Context c, ArrayList<Product> arrayListSoldOut) {
        mContext = c;
        mThumbIds = arrayListSoldOut;
    }

    public int getCount() {
        return mThumbIds.size();
    }

    public Object getItem(int position) {
        return null;
    }

    public long getItemId(int position) {
        return 0;
    }

    // create a new ImageView for each item referenced by the Adapter
    public View getView(int position, View convertView, ViewGroup parent) {

        if (convertView == null) {
            convertView = LayoutInflater.from(mContext).inflate(R.layout.layout_just_sold, null);
        }

        ((TextView) convertView.findViewById(R.id.tv_product_name)).setText(mThumbIds.get(position).getProduct_name());
        ((TextView) convertView.findViewById(R.id.tv_product_price)).setText(Constants.RUPEE + " " + Constants.formatAmount(mThumbIds.get(position).getProduct_price()));

        ImageView ivProduct = (ImageView) convertView.findViewById(R.id.iv_sold_product);
        Picasso.with(mContext).load(mThumbIds.get(position).getProduct_img()).into(ivProduct);

        convertView.setTag(mThumbIds.get(position));
        return convertView;
    }


}