package outletwise.com.twentydresses.controller;

import java.util.Random;

import outletwise.com.twentydresses.R;
import outletwise.com.twentydresses.utilities.Constants;

/**
 * Created by User-PC on 17-08-2015.
 */
public class RandomBackground {

    private static final Random RANDOM = new Random();
    private static final int NO_OF_IMAGES = 3;

    public static int getRandomImage() {
        return getRandomImage(Constants.CATEGORY_DEFAULT);
    }

    public static int getRandomImage(int category) {
        switch (category){
            default:
            case Constants.CATEGORY_DEFAULT:
                return getRandomImageDefault();
        }
    }

    public static int getRandomImageDefault() {
        switch (RANDOM.nextInt(NO_OF_IMAGES)) {
            default:
            case 0:
                return R.drawable.signreg;
            case 1:
                return R.drawable.signreg;
            case 2:
                return R.drawable.signreg;
        }
    }
}
