package outletwise.com.twentydresses.controller.adapter;

import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.Spinner;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.List;

import mehdi.sakout.fancybuttons.FancyButton;
import outletwise.com.twentydresses.R;
import outletwise.com.twentydresses.model.Order;
import outletwise.com.twentydresses.model.Reason;
import outletwise.com.twentydresses.utilities.Constants;

/**
 * Created by User-PC on 04-11-2015.
 */
public class OrdersAdapter extends BaseRecyclerAdapter {

    private List<OrderItemsAdapter> orderItemsAdapterList = new ArrayList<>();
    private List<Reason.ReasonsEntity> reasons;
    private ReturnExchange returnExchange;
    private Reason.ReasonsEntity selectedReason;

    public OrdersAdapter(Context context, ArrayList<Order> items) {
        super(context, items);
        for (Order order : items) {
            OrderItemsAdapter adapter = new OrderItemsAdapter(context, order.getOrder_items());
            orderItemsAdapterList.add(adapter);
        }
    }


    class MyOrdersViewHolder extends BaseViewHolder {
        public TextView tvOrderNo;
        public FancyButton bConfirmed;
        public LinearLayout llOrders;
        public TextView tvOrderDetails;
        public TextView tvTrackStatus;
        public Button bCancel;
        public Button bReturnExchange;
        public Button bTrackOrder;
        public Spinner spReasons;

        public MyOrdersViewHolder(View itemView) {
            super(itemView);
            tvOrderNo = (TextView) itemView.findViewById(R.id.tvOrderNo);
            bConfirmed = (FancyButton) itemView.findViewById(R.id.bConfirmed);
            llOrders = (LinearLayout) itemView.findViewById(R.id.rvOrders);
            tvOrderDetails = (TextView) itemView.findViewById(R.id.tvOrderDetails);
            bCancel = (Button) itemView.findViewById(R.id.bCancel);
            bReturnExchange = (Button) itemView.findViewById(R.id.bReturnExchange);
            bTrackOrder = (Button) itemView.findViewById(R.id.bTrackOrder);
            spReasons = (Spinner) itemView.findViewById(R.id.spReasons1);
            tvTrackStatus = (TextView) itemView.findViewById(R.id.tvTrackStatus);
        }

        @Override
        public void onClick(View v) {
            if (mItemClickListener != null)
                mItemClickListener.onItemClick(v, getAdapterPosition());
        }
    }

    @Override
    public BaseViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.layout_order_item, parent, false);
        return new MyOrdersViewHolder(view);
    }

    @Override
    public void onBindViewHolder(BaseViewHolder holder, int position) {
        MyOrdersViewHolder viewHolder = (MyOrdersViewHolder) holder;
        final Order order = (Order) items.get(position);

        viewHolder.itemView.setTag(order);

        //  viewHolder.bConfirmed.setText(Constants.getOrderStatus(order.getOrder_status()));
        viewHolder.bConfirmed.setText(order.getOrder_status());
        viewHolder.tvOrderNo.setText("Order # " + order.getOrder_number());
        viewHolder.tvOrderDetails.setText("Placed on: " + order.getOrder_date() + " | Order Total: " + Constants.RUPEE +
                Constants.formatAmount(order.getOrder_total()));

        if (order.getTracking_url() != null) {
            viewHolder.bTrackOrder.setVisibility(View.VISIBLE);
            viewHolder.bTrackOrder.setText(order.getTrack_text());
            viewHolder.tvTrackStatus.setVisibility(View.VISIBLE);
            viewHolder.tvTrackStatus.setText("Status: " + order.getTrack_status());
        } else {
            viewHolder.bTrackOrder.setVisibility(View.GONE);
            viewHolder.bTrackOrder.setText(order.getTrack_text());
            viewHolder.tvTrackStatus.setVisibility(View.GONE);
            viewHolder.tvTrackStatus.setText("Status: " + order.getTrack_status());
        }

        if (order.isCancel_button()) {
            viewHolder.bCancel.setVisibility(View.VISIBLE);
            viewHolder.spReasons.setVisibility(View.GONE);
        } else {
            viewHolder.bCancel.setVisibility(View.GONE);
            viewHolder.spReasons.setVisibility(View.GONE);
        }

        if (order.isReturn_button())
            viewHolder.bReturnExchange.setVisibility(View.VISIBLE);
        else
            viewHolder.bReturnExchange.setVisibility(View.GONE);

        OrderItemsAdapter adapter = orderItemsAdapterList.get(position);
        if (adapter.isEmpty()) {
            adapter.addItems(viewHolder.llOrders);
        } else if (adapter.isListSizeChanged(order.getOrder_items().size())) {
            adapter.notifyDataSetChanged();
        } else {
            adapter.addItems(viewHolder.llOrders);
        }

        viewHolder.itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (returnExchange != null)
                    returnExchange.onItemClicked(order);
            }
        });

        // Onclick to horizontal image scrollview
        viewHolder.llOrders.setTag(order);
        viewHolder.llOrders.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (returnExchange != null)
                    returnExchange.onItemClicked(order);
            }
        });

        viewHolder.bReturnExchange.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (returnExchange != null)
                    returnExchange.onReturnExchange(order);
            }
        });

        viewHolder.bCancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (returnExchange != null) {
                   /* if (selectedReason == null)
                        ((BaseActivity) context).showToast("Please select reason");
                    else*/
                    returnExchange.onCancel(order, selectedReason);
                }
            }
        });


        viewHolder.bTrackOrder.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Uri uri = Uri.parse(order.getTracking_url());
                Intent intent = new Intent(Intent.ACTION_VIEW, uri);
                context.startActivity(intent);
            }
        });

        ReasonsAdapter reasonsAdapter = new ReasonsAdapter(context, R.layout.layout_adapter_reasons, reasons);
        viewHolder.spReasons.setAdapter(reasonsAdapter);
        viewHolder.spReasons.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                if (position == 0)
                    return;
                selectedReason = (Reason.ReasonsEntity) parent.getItemAtPosition(position);
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });
    }

    public void setOnItemClickListener(final OnItemClickListener mItemClickListener) {
        this.mItemClickListener = mItemClickListener;
    }

    public void setReturnExchange(ReturnExchange returnExchange) {
        this.returnExchange = returnExchange;
    }

    public void setReasons(List<Reason.ReasonsEntity> reasons) {
        Reason.ReasonsEntity reasonsEntity = new Reason.ReasonsEntity();
        reasonsEntity.setReason_id("-1");
        reasonsEntity.setReason("Select Reason");
        reasons.add(0, reasonsEntity);
        this.reasons = reasons;
    }


    public interface ReturnExchange {
        void onReturnExchange(Order order);

        void onCancel(Order order, Reason.ReasonsEntity reason);

        void onItemClicked(Order order);
    }
}
