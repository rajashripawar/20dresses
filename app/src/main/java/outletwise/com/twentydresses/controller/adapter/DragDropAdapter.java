package outletwise.com.twentydresses.controller.adapter;

import android.content.ClipData;
import android.content.Context;
import android.support.v4.view.PagerAdapter;
import android.support.v4.view.ViewPager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import outletwise.com.twentydresses.R;
import outletwise.com.twentydresses.controller.SwipeDetector;
import outletwise.com.twentydresses.utilities.Constants;

/**
 * Created by User-PC on 27/07/2015.
 */
public class DragDropAdapter extends PagerAdapter {
    private Context mContext;
    private LayoutInflater mLayoutInflater;
    private List<Integer> mResources = new ArrayList<>();
    private ArrayList<View> views = new ArrayList<>();
    private HashMap<Integer, Integer> mResourcesMap = new HashMap<>();

    public DragDropAdapter(Context context) {
        mContext = context;
        mLayoutInflater = (LayoutInflater) mContext.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        mResources.add(R.drawable.quiz_style_bohemian);
        mResources.add(R.drawable.quiz_style_causal);
        mResources.add(R.drawable.quiz_style_classic);
        mResources.add(R.drawable.quiz_style_edgy);
        mResources.add(R.drawable.quiz_style_feminne);
        mResources.add(R.drawable.quiz_style_ff);
        mResources.add(R.drawable.dumm_view);

        int[] style_ids = mContext.getResources().getIntArray(R.array.style_ids);
        int pos = 0;
        for (Integer mResource : mResources) {
            mResourcesMap.put(mResource, style_ids[pos++]);
        }
    }

    @Override
    public int getCount() {
        return mResources.size();
    }

    @Override
    public boolean isViewFromObject(View view, Object object) {
        return view == object;
    }

    @Override
    public Object instantiateItem(final ViewGroup container, final int position) {
        final View itemView = mLayoutInflater.inflate(R.layout.pager_image_view, container, false);
        views.add(itemView);

        final ImageView imageView = (ImageView) itemView.findViewById(R.id.imageView);
        imageView.setImageResource(mResources.get(position));
        Tag tag = new Tag();
        tag.position = mResourcesMap.get(mResources.get(position));
        tag.resId = mResources.get(position);
        imageView.setTag(tag);

        if (position == getCount() - 1) {
            itemView.setBackgroundColor(mContext.getResources().getColor(android.R.color.transparent));
        }

        if (position != getCount() - 1) {
            ((ImageView) itemView.findViewById(R.id.imageViewDummy)).setImageResource(mResources.get(position));
            new SwipeDetector(imageView).setOnSwipeListener(new SwipeDetector.onSwipeEvent() {
                @Override
                public void SwipeEventDetected(View v, SwipeDetector.SwipeTypeEnum swipeType) {

                    ClipData data;
                    View view = itemView.findViewById(R.id.rlShadow);
                    View.DragShadowBuilder shadowBuilder;

                    if (swipeType == SwipeDetector.SwipeTypeEnum.TOP_TO_BOTTOM_LEFT) {
                        data = ClipData.newPlainText(Constants.SWIPE_TYPE, SwipeDetector.SwipeTypeEnum.TOP_TO_BOTTOM_LEFT.toString());
                        ((TextView) view.findViewById(R.id.tvSetStyle)).setText(mContext.getString(R.string.text_may_be));
                        shadowBuilder = new View.DragShadowBuilder(view);
                    } else {
                        data = ClipData.newPlainText(Constants.SWIPE_TYPE, SwipeDetector.SwipeTypeEnum.TOP_TO_BOTTOM_RIGHT.toString());
                        ((TextView) view.findViewById(R.id.tvSetStyle)).setText(R.string.text_love_it);
                        shadowBuilder = new View.DragShadowBuilder(view);
                    }
                    imageView.startDrag(data, shadowBuilder, imageView, 0);
                }
            });
        }

        container.addView(itemView);

        return itemView;
    }

    @Override
    public int getItemPosition(Object object) {
        int index = views.indexOf(object);
        if (index == -1)
            return POSITION_NONE;
        else
            return index;
    }

    @Override
    public void destroyItem(ViewGroup container, int position, Object object) {
        container.removeView((RelativeLayout) object);
    }

    public int removeView(ViewPager pager, View v) {
        return removeView(pager, views.indexOf(v));
    }

    public int removeView(ViewPager pager, int position) {
        pager.setAdapter(null);
        views.remove(position);
        mResources.remove(position);
        pager.setAdapter(this);
        return position;
    }

    public void addView(View v, ViewPager pager) {
        views.add(pager.getCurrentItem(), v);
        Tag tag = (Tag) v.getTag();
        mResources.add(pager.getCurrentItem(), tag.resId);
        pager.setAdapter(null);
        pager.setAdapter(this);
    }

    public void reset(ViewPager pager) {
        pager.setAdapter(null);
        pager.setAdapter(this);
    }

    public View getView(int position) {
        return views.get(position);
    }


    class Tag {
        int resId;
        int position;
    }

}