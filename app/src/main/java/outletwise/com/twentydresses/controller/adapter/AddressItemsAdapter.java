package outletwise.com.twentydresses.controller.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.CheckBox;
import android.widget.LinearLayout;
import android.widget.TextView;

import java.util.List;

import outletwise.com.twentydresses.R;
import outletwise.com.twentydresses.model.Address;

/**
 * Created by User-PC on 09-10-2015.
 */
public class AddressItemsAdapter implements View.OnClickListener {
    private Context context;
    private List<Address> addressList;
    private LinearLayout linearLayout;
    private AddressChanged addressChanged;
    private CheckBox lastChecked;

    public AddressItemsAdapter(Context context, List<Address> addressList, LinearLayout linearLayout) {
        this.context = context;
        this.addressList = addressList;
        this.linearLayout = linearLayout;
        addItems();
    }

    public void addItems() {
        if (addressList != null && addressList.size() > 0) {
            for (Address address : addressList) {
                View addressLayout = LayoutInflater.from(context).inflate(
                        R.layout.layout_adapter_address, null);
                ((TextView) addressLayout.findViewById(R.id.tvName)).setText(address.getFirstname() + " " + address.getLastname());
                ((TextView) addressLayout.findViewById(R.id.tvAddress)).
                        setText(address.getAddress() + "\n" + address.getCity() + "\n" + address.getPincode() + "\n" + address.getState());
                ((TextView) addressLayout.findViewById(R.id.tvMobile)).setText("Mobile No. : " + address.getMobile());
                addressLayout.setOnClickListener(this);
                addressLayout.setTag(address);
                addressLayout.findViewById(R.id.iv_edit).setTag(address);
                addressLayout.findViewById(R.id.iv_edit).setOnClickListener(this);
                addressLayout.findViewById(R.id.iv_delete).setTag(address);
                addressLayout.findViewById(R.id.iv_delete).setOnClickListener(this);

                linearLayout.addView(addressLayout);
            }
            lastChecked = (CheckBox) linearLayout.getChildAt(0).findViewById(R.id.checkBox);

        } else {
            View addressLayout = LayoutInflater.from(context).inflate(
                    R.layout.layout_adapter_address, null);
            ((TextView) addressLayout.findViewById(R.id.tvName)).setText(context.getString(R.string.text_no_details));
        }
    }

    public void setAddressList(List<Address> addressList) {
        this.addressList = addressList;
    }

    private void removeViews() {
        linearLayout.removeAllViews();
    }

    public boolean isEmpty() {
        return linearLayout == null || linearLayout.getChildCount() == 0;
    }

    public boolean isListSizeChanged(int listSize) {
        return listSize != addressList.size();
    }

    public void notifyDataSetChanged() {
        removeViews();
        addItems();
    }

    public void setFirstAddressCheck(boolean b) {
        if (lastChecked != null)
            lastChecked.setChecked(b);
    }

    public void setAddressChanged(AddressChanged addressChanged) {
        this.addressChanged = addressChanged;
    }

    public void clearCheck() {
        if (lastChecked != null)
            lastChecked.setChecked(false);
    }

    @Override
    public void onClick(View v) {
        if (addressChanged != null) {
            if (v.getId() == R.id.iv_edit) {
                addressChanged.onEditAddress((Address) v.getTag());
            } else if (v.getId() == R.id.iv_delete) {
                addressChanged.onDeleteAddress((Address) v.getTag());
            } else {
                if (lastChecked != null)
                    lastChecked.setChecked(false);
                lastChecked = ((CheckBox) v.findViewById(R.id.checkBox));
                lastChecked.setChecked(true);
                addressChanged.onAddressChange((Address) v.getTag());
            }
        }
    }

    public interface AddressChanged {
        void onAddressChange(Address address);
        void onEditAddress(Address address);
        void onDeleteAddress(Address address);
    }
}
