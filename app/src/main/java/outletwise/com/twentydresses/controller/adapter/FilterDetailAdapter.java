package outletwise.com.twentydresses.controller.adapter;

import android.content.Context;
import android.graphics.Color;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import java.util.ArrayList;

import outletwise.com.twentydresses.R;
import outletwise.com.twentydresses.model.Filter;

/**
 * Created by User-PC on 30-11-2015.
 */

public class FilterDetailAdapter extends BaseRecyclerAdapter {

    private Object subFilter;
    public static ArrayList<String> allAppliedFilterColor = new ArrayList<>();
    public static ArrayList<String> allAppliedFilterCategory = new ArrayList<>();
    public static ArrayList<String> allAppliedFilterSize = new ArrayList<>();
    public static String allAppliedFilterDiscount;

    private String parameter_color;
    private String parameter_category;
    private String parameter_size;
    private String parameter_discount;
    private String strCategoryId;
    private View rootView;
    private RecyclerView rvDetail;
    private View previousDiscountCheck;
    private int previousDiscountPos = -1;

    public FilterDetailAdapter(Context context, Object subFilter, String strCategoryId, RecyclerView rvDetail) {
        super(context, null);
        this.subFilter = subFilter;
        this.strCategoryId = strCategoryId;
        this.rvDetail = rvDetail;
    }

    public class FilterHolder extends BaseViewHolder {
        TextView tvText;
        ImageView colorImage;
        ImageView check;
        TextView tvCount;

        public FilterHolder(View itemView) {
            super(itemView);
            rootView = itemView;
            tvText = (TextView) itemView.findViewById(R.id.tvText);
            colorImage = (ImageView) itemView.findViewById(R.id.color);
            check = (ImageView) itemView.findViewById(R.id.ivCheck);
            tvCount = (TextView) itemView.findViewById(R.id.tvCount);
        }

        @Override
        public void onClick(View v) {

            if (check.getVisibility() == View.VISIBLE) {
                check.setVisibility(View.GONE);
                if (subFilter instanceof Filter.CategoriesEntity) {
                    Filter.CategoriesEntity.ValuesEntity filter = (Filter.CategoriesEntity.ValuesEntity) v.getTag();
                    ((Filter.CategoriesEntity) subFilter).removeFilter(filter.getCategory());
                    allAppliedFilterCategory.remove(filter.getCategory());
                } else if (subFilter instanceof Filter.SizesEntity) {
                    Filter.SizesEntity.ValuesEntity filter = (Filter.SizesEntity.ValuesEntity) v.getTag();
                    ((Filter.SizesEntity) subFilter).removeFilter(filter.getAttr_id());
                    allAppliedFilterSize.remove(filter.getAttr_id());
                } else if (subFilter instanceof Filter.ColorsEntity) {
                    Filter.ColorsEntity.ValuesEntity filter = (Filter.ColorsEntity.ValuesEntity) v.getTag();
                    ((Filter.ColorsEntity) subFilter).removeFilter(filter.getColor_id());
                    allAppliedFilterColor.remove(filter.getColor_id());
                } else if (subFilter instanceof Filter.DiscountsEntity) {
                    previousDiscountCheck = null;
                    previousDiscountPos = -1;
//                    Filter.DiscountsEntity.ValuesEntity filter = (Filter.DiscountsEntity.ValuesEntity) v.getTag();
                    ((Filter.DiscountsEntity) subFilter).removeFilter();
                    allAppliedFilterDiscount = null;//remove(String.valueOf(filter.getSale()));
                }
            } else {
                check.setVisibility(View.VISIBLE);

                if (subFilter instanceof Filter.CategoriesEntity) {
                    Filter.CategoriesEntity.ValuesEntity filter = (Filter.CategoriesEntity.ValuesEntity) v.getTag();
                    ((Filter.CategoriesEntity) subFilter).addFilters(filter);

                    if (!allAppliedFilterCategory.contains(filter.getCategory()))
                        allAppliedFilterCategory.add(filter.getCategory());

                    parameter_category = ((Filter.CategoriesEntity) subFilter).getParameter();

                } else if (subFilter instanceof Filter.SizesEntity) {
                    Filter.SizesEntity.ValuesEntity filter = (Filter.SizesEntity.ValuesEntity) v.getTag();
                    ((Filter.SizesEntity) subFilter).addFilters(filter);

                    if (!allAppliedFilterSize.contains(filter.getAttr_id()))
                        allAppliedFilterSize.add(filter.getAttr_id());

                    parameter_size = ((Filter.SizesEntity) subFilter).getParameter();

                } else if (subFilter instanceof Filter.ColorsEntity) {
                    Filter.ColorsEntity.ValuesEntity filter = (Filter.ColorsEntity.ValuesEntity) v.getTag();
                    ((Filter.ColorsEntity) subFilter).addFilters(filter);

                    if (!allAppliedFilterColor.contains(filter.getColor_id()))
                        allAppliedFilterColor.add(filter.getColor_id());

                    parameter_color = ((Filter.ColorsEntity) subFilter).getParameter();

                } else if (subFilter instanceof Filter.DiscountsEntity) {
                    //toggle visibility of previous discount check
                    if (previousDiscountCheck != null)
                        previousDiscountCheck.setVisibility(View.GONE);
                    previousDiscountCheck = check;
                    previousDiscountPos = getAdapterPosition();

                    Filter.DiscountsEntity.ValuesEntity filter = (Filter.DiscountsEntity.ValuesEntity) v.getTag();
                    ((Filter.DiscountsEntity) subFilter).addFilters(filter);

                    allAppliedFilterDiscount = String.valueOf(filter.getSale());

                    parameter_discount = ((Filter.DiscountsEntity) subFilter).getParameter();

               /*     if (currentPosition != -1) {
                        View last = rvDetail.getChildAt(currentPosition); // the last one clicked
                       // last.findViewById(R.id.button1).setVisibility(View.GONE); // kill it
                        last.
                    }
                    v.findViewById(R.id.button1).setVisibility(View.VISIBLE);
                    current = position;*/

                }
            }
            notifyDataSetChanged();
        }


    }


    public ArrayList<String> getAllAppliedFilter_Color() {
        return allAppliedFilterColor;
    }

    public ArrayList<String> getAllAppliedFilter_Size() {
        return allAppliedFilterSize;
    }

    public String getAllAppliedFilter_Discount() {
        return allAppliedFilterDiscount;
    }

    public ArrayList<String> getAllAppliedFilter_Category() {
        return allAppliedFilterCategory;
    }

    public String getParameter_color() {
        return parameter_color;
    }

    public String getParameter_size() {
        return parameter_size;
    }

    public String getParameter_discount() {
        return parameter_discount;
    }

    public String getParameter_category() {
        return parameter_category;
    }

    @Override
    public BaseViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.layout_adapter_details_filter, parent, false);
        return new FilterHolder(view);
    }

    @Override
    public void onBindViewHolder(BaseViewHolder holder, int position) {

        FilterHolder filterHolder = (FilterHolder) holder;
        if (subFilter instanceof Filter.CategoriesEntity) {
            String count = " ("+((Filter.CategoriesEntity) subFilter).getValues().get(position).getCategory_count()+")";
            filterHolder.tvText.setText(((Filter.CategoriesEntity) subFilter).getValues().get(position).getCategory_name()+count);
           // filterHolder.tvCount.setText("("+((Filter.CategoriesEntity) subFilter).getValues().get(position).getCategory_count()+")");

            if (((Filter.CategoriesEntity) subFilter).getAppliedFilter(((Filter.CategoriesEntity) subFilter).getValues().get(position).getCategory()) != null)
                filterHolder.check.setVisibility(View.VISIBLE);
            else
                filterHolder.check.setVisibility(View.GONE);

            //For default category
            if (!strCategoryId.equalsIgnoreCase("1") && (((Filter.CategoriesEntity) subFilter).getValues().get(position).getCategory().equalsIgnoreCase(strCategoryId))) {
                filterHolder.check.setVisibility(View.VISIBLE);
                Filter.CategoriesEntity.ValuesEntity filter = ((Filter.CategoriesEntity) subFilter).getValues().get(position);
                ((Filter.CategoriesEntity) subFilter).addFilters(filter);
                allAppliedFilterCategory.add(filter.getCategory());
                parameter_category = ((Filter.CategoriesEntity) subFilter).getParameter();
            }

            filterHolder.itemView.setTag(((Filter.CategoriesEntity) subFilter).getValues().get(position));
        } else if (subFilter instanceof Filter.SizesEntity) {
            String count = " ("+((Filter.SizesEntity) subFilter).getValues().get(position).getAttr_count()+")";
            filterHolder.tvText.setText(((Filter.SizesEntity) subFilter).getValues().get(position).getAttr_value()+count);
            if (((Filter.SizesEntity) subFilter).getAppliedFilter(((Filter.SizesEntity) subFilter).getValues().get(position).getAttr_id()) != null)
                filterHolder.check.setVisibility(View.VISIBLE);
            else
                filterHolder.check.setVisibility(View.GONE);
            filterHolder.itemView.setTag(((Filter.SizesEntity) subFilter).getValues().get(position));

        } else if (subFilter instanceof Filter.ColorsEntity) {
            String count = " ("+((Filter.ColorsEntity) subFilter).getValues().get(position).getColor_count()+")";

            Filter.ColorsEntity.ValuesEntity color = ((Filter.ColorsEntity) subFilter).getValues().get(position);
            filterHolder.tvText.setText(color.getColor_name()+count);
            filterHolder.colorImage.setVisibility(View.VISIBLE);
            if (!color.getColor_name().equalsIgnoreCase("Multicolor"))
                filterHolder.colorImage.setColorFilter(Color.parseColor(color.getColor_code()));

            if (((Filter.ColorsEntity) subFilter).getAppliedFilter(((Filter.ColorsEntity) subFilter).getValues().get(position).getColor_id()) != null)
                filterHolder.check.setVisibility(View.VISIBLE);
            else
                filterHolder.check.setVisibility(View.GONE);

            filterHolder.itemView.setTag(((Filter.ColorsEntity) subFilter).getValues().get(position));
        } else if (subFilter instanceof Filter.DiscountsEntity) {
            String count = " ("+((Filter.DiscountsEntity) subFilter).getValues().get(position).getDiscount_count()+")";
            filterHolder.tvText.setText(((Filter.DiscountsEntity) subFilter).getValues().get(position).getDiscount()+count);

            if (previousDiscountPos == position)
                filterHolder.check.setVisibility(View.VISIBLE);
            else
                filterHolder.check.setVisibility(View.GONE);

            filterHolder.itemView.setTag(((Filter.DiscountsEntity) subFilter).getValues().get(position));
        }

    }

    @Override
    public int getItemCount() {
        if (subFilter instanceof Filter.CategoriesEntity) {
            return ((Filter.CategoriesEntity) subFilter).getValues().size();
        } else if (subFilter instanceof Filter.SizesEntity) {
            return ((Filter.SizesEntity) subFilter).getValues().size();
        } else if (subFilter instanceof Filter.ColorsEntity) {
            return ((Filter.ColorsEntity) subFilter).getValues().size();
        } else if (subFilter instanceof Filter.DiscountsEntity) {
            return ((Filter.DiscountsEntity) subFilter).getValues().size();
        } else
            return 0;
    }

    public void setSubFilter(Object subFilter) {
        this.subFilter = subFilter;
    }
}
