package outletwise.com.twentydresses.controller.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;

import com.squareup.picasso.Picasso;

import java.util.ArrayList;

import outletwise.com.twentydresses.R;

/**
 * Created by User-PC on 28-09-2015.
 */
public class TrendBannerAdapter extends BaseRecyclerAdapter {

    public TrendBannerAdapter(Context context, ArrayList items) {
        super(context, items);
    }

    class TrendBannerViewHolder extends BaseViewHolder {
        public ImageView ivBanner;


        public TrendBannerViewHolder(View itemView) {
            super(itemView);
            ivBanner = (ImageView) itemView.findViewById(R.id.ivBanner);
        }
        @Override
        public void onClick(View v) {
        }
    }

    @Override
    public BaseViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.layout_adapter_trend_banners, parent, false);
        return new TrendBannerViewHolder(view);
    }

    @Override
    public void onBindViewHolder(BaseViewHolder holder, int position) {
        TrendBannerViewHolder bannerViewHolder = (TrendBannerViewHolder) holder;
        bannerViewHolder.itemView.setTag(items.get(position));
        Picasso.with(context).load(String.valueOf(items.get(position))).into(bannerViewHolder.ivBanner);
    }
}
