package outletwise.com.twentydresses.controller.adapter;

/**
 * Created by User-PC on 04-05-2016.
 */

import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;

import java.util.ArrayList;
import java.util.List;


public class ReferTabAdapter extends FragmentPagerAdapter {
    private List<Fragment> fragmentsList = new ArrayList<>();

    public ReferTabAdapter(FragmentManager fm) {
        super(fm);
    }

    @Override
    public Fragment getItem(int position) {
        return fragmentsList.get(position);
    }

    @Override
    public int getCount() {
        return fragmentsList.size();
    }

    @Override
    public CharSequence getPageTitle(int position) {
        switch (position) {
            case 0:
                return "INVITE";
            case 1:
                return "FAQ";
//                return "REWARDS";
          /*  case 2:
                return "FAQ";*/
        }

        return null;
    }

    public void addFragments(List<Fragment> mFragments) {
        fragmentsList = mFragments;
    }
}