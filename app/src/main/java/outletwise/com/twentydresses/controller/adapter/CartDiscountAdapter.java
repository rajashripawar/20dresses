package outletwise.com.twentydresses.controller.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.TextView;

import java.util.ArrayList;

import outletwise.com.twentydresses.R;
import outletwise.com.twentydresses.model.SyncCart;
import outletwise.com.twentydresses.utilities.Constants;

/**
 * Created by User-PC on 06-05-2016.
 */
public class CartDiscountAdapter {

    private Context context;
    private ArrayList<SyncCart.CartValuesEntity.CartDiscountEntity> cartDiscountItems;
    private LinearLayout linearLayout;

    public CartDiscountAdapter(Context context) {
        this.context = context;
        //this.cartDiscountItems = cartDiscountItems;
     /*   this.linearLayout = (LinearLayout) ((Activity) context).findViewById(R.id.llCartDiscount);
        addItems();*/
    }


    private void addItems(LinearLayout linearLayout) {
        for (int i = 0; i < cartDiscountItems.size(); i++) {
            View cartDiscountLayout = LayoutInflater.from(context).inflate(
                    R.layout.layout_cart_discount, null);

            ((TextView) cartDiscountLayout.findViewById(R.id.tvDiscount)).setText(cartDiscountItems.get(i).getDiscount_name() + "(-):" +
                    cartDiscountItems.get(i).getDiscount_value());

            linearLayout.addView(cartDiscountLayout);
        }
    }

    private String getPaddedAmount(String amount, boolean rupeeSymbol) {
        String result = rupeeSymbol ? Constants.RUPEE : "" + Constants.formatAmount(amount);
        int pad = 16 - result.length();
        for (int i = 0; i < pad; i++) {
            result = " " + result;
        }
        return result;
    }

    private void removeViews(LinearLayout linearLayout) {
        if (linearLayout != null && linearLayout.getChildCount() > 0) {
            linearLayout.removeAllViews();
            Constants.debug("After remove " + linearLayout.getChildCount());
        } else {
            Constants.debug("In remove but not removed " + linearLayout.getChildCount());
        }
    }

    public void notifyDataSetChanged(LinearLayout linearLayout) {
        removeViews(linearLayout);
        addItems(linearLayout);
    }

    public void setDiscountList(ArrayList<SyncCart.CartValuesEntity.CartDiscountEntity> cartDiscount) {
        this.cartDiscountItems = cartDiscount;
    }
}
