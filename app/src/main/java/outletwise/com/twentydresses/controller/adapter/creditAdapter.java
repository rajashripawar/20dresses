package outletwise.com.twentydresses.controller.adapter;

import android.app.Activity;
import android.content.Context;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.TextView;

import java.util.ArrayList;

import outletwise.com.twentydresses.R;
import outletwise.com.twentydresses.model.CreditResponse;

public class creditAdapter {

    private final Context context;
    private ArrayList<CreditResponse.CreditSummaryEntity> list_Credit;
    private final LinearLayout linearLayout;

    public creditAdapter(Context context, ArrayList<CreditResponse.CreditSummaryEntity> list, LinearLayout llCredit) {

        this.context = context;
        list_Credit = list;
        this.linearLayout = llCredit;
        addViews();
    }

    private void addViews() {
       /* if ((linearLayout).getChildCount() > 0)
            (linearLayout).removeAllViews();*/
        View rootView1 = ((Activity) context).getLayoutInflater().inflate(R.layout.layout_adapter_credit, null);

        TextView tv1 = (TextView) rootView1.findViewById(R.id.tvCredit);
        tv1.setText("CREDITS");

        TextView tv2 = (TextView) rootView1.findViewById(R.id.tvDate);
        tv2.setText("EXPIRATION");

        TextView tv3 = (TextView) rootView1.findViewById(R.id.tvDesc);
        tv3.setText("DESCRIPTION");

        linearLayout.addView(rootView1);

        for (CreditResponse.CreditSummaryEntity list : list_Credit) {
            {
                View rootView = ((Activity) context).getLayoutInflater().inflate(R.layout.layout_adapter_credit, null);
                TextView tvCredit = (TextView) rootView.findViewById(R.id.tvCredit);
                tvCredit.setText("" + list.getCredits());

                TextView tvDate = (TextView) rootView.findViewById(R.id.tvDate);
                tvDate.setText("" + list.getExpire_date());

                TextView tvDesc = (TextView) rootView.findViewById(R.id.tvDesc);
                tvDesc.setText("" + list.getDescription());

                linearLayout.addView(rootView);
            }
        }
    }
}