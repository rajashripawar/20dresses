package outletwise.com.twentydresses.controller.adapter;

/**
 * Created by User-PC on 09-11-2015.
 */

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import com.citrus.sdk.payment.NetbankingOption;

import java.util.ArrayList;
import java.util.List;

import outletwise.com.twentydresses.R;

/*****
 * HomeTabAdapter class extends with ArrayAdapter
 ******/
public class NetBankingAdapter extends ArrayAdapter<NetbankingOption> {
    Context context;
    List<NetbankingOption> netBankingOptions;
    int resId;

    public NetBankingAdapter(Context context, int resource, List<NetbankingOption> objects) {
        super(context, resource, objects);
        this.context = context;
        this.netBankingOptions = new ArrayList<>();
        NetbankingOption defaultOption = new NetbankingOption("Select Bank", "Default");
        this.netBankingOptions.add(defaultOption);
        this.netBankingOptions.addAll(objects);
        this.resId = resource;
    }

    @Override
    public View getDropDownView(int position, View convertView,
                                ViewGroup parent) {
        return getCustomView(position, convertView, parent);
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        return getCustomView(position, convertView, parent);
    }

    public View getCustomView(int position, View convertView, ViewGroup parent) {
        if (convertView == null) {
            LayoutInflater inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            convertView = inflater.inflate(resId, parent, false);
        }
        TextView label = (TextView) convertView.findViewById(R.id.tvBankName);
        label.setText(netBankingOptions.get(position).getBankName());
//        if (position != 0)
//            label.setTextColor(context.getResources().getColor(R.color.color_accent));
//        else
//            label.setTextColor(context.getResources().getColor(R.color.color_text_primary));

        convertView.setTag(netBankingOptions.get(position));

        return convertView;
    }
}