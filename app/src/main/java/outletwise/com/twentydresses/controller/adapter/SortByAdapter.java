package outletwise.com.twentydresses.controller.adapter;

import android.content.Context;
import android.graphics.Typeface;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import java.util.ArrayList;

import outletwise.com.twentydresses.R;
import outletwise.com.twentydresses.utilities.Constants;

/**
 * Created by User-PC on 28-09-2015.
 */
public class SortByAdapter extends BaseRecyclerAdapter {
Context mcontext;
    public SortByAdapter(Context context, ArrayList<String> items) {
        super(context, items);
        this.mcontext = context;
    }

    private int selectedPos;

    class SortByViewHolder extends BaseViewHolder {
        TextView tvText;

        public SortByViewHolder(View itemView) {
            super(itemView);
            tvText = (TextView) itemView.findViewById(R.id.text);
        }

        @Override
        public void onClick(View v) {
            if (mItemClickListener != null && getAdapterPosition() != 0) {
                mItemClickListener.onItemClick(v, getAdapterPosition());
            }
        }
    }

    @Override
    public BaseViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.layout_adapter_sortby, parent, false);
        return new SortByViewHolder(view);
    }

    public void setSelectedPos(int selectedPos) {
        this.selectedPos = selectedPos;
    }

    @Override
    public void onBindViewHolder(BaseViewHolder holder, int position) {
        if (selectedPos == position && position != 0) {
            ((SortByViewHolder) holder).tvText.setSelected(true);
        }
        else {
            ((SortByViewHolder) holder).tvText.setSelected(false);
        }

        if (position == 0) {
            ((SortByViewHolder) holder).tvText.setTypeface(Typeface.DEFAULT_BOLD);
            ((SortByViewHolder) holder).tvText.setTextColor(mcontext.getResources().getColor(R.color.md_grey_700));

        }
        else {
            if (Constants.LATESTSORT == position){
                ((SortByViewHolder) holder).tvText.setTextColor(mcontext.getResources().getColor(R.color.color_accent));
                ((SortByViewHolder) holder).tvText.setTypeface(Typeface.DEFAULT);
            }else{

                ((SortByViewHolder) holder).tvText.setTypeface(Typeface.DEFAULT);
                ((SortByViewHolder) holder).tvText.setTextColor(mcontext.getResources().getColor(R.color.md_grey_700));
            }
        }
         ((SortByViewHolder) holder).tvText.setText((String) items.get(position));
        ((SortByViewHolder) holder).itemView.setTag(items.get(position));
    }
}
