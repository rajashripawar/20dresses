package outletwise.com.twentydresses.controller.adapter;

import android.content.Context;
import android.content.res.TypedArray;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseExpandableListAdapter;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.squareup.picasso.Picasso;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import outletwise.com.twentydresses.R;
import outletwise.com.twentydresses.model.Category;
import outletwise.com.twentydresses.model.database.greenbot.User;
import outletwise.com.twentydresses.utilities.Constants;

/**
 * Created by User-PC on 19-08-2015.
 */
public class DrawerAdapter extends BaseExpandableListAdapter {

    private Context mContext;
    private User user;
    private ArrayList<Category> categories = new ArrayList<>();
    //  private TypedArray drawerRes;
    private OnDrawerItemClick onDrawerItemClick;

    public DrawerAdapter(Context context) {
        this.mContext = context;
        // drawerRes = context.getResources().obtainTypedArray(R.array.drawer_images);
        //  List<String> tempItems = Arrays.asList(mContext.getResources().getStringArray(R.array.drawer_main_list));
        //  int i = 0;
      /*  for (String item : tempItems) {
            Category category = new Category();
            if (i == 0) // Position My Account
                category.setSubCatsString(Arrays.asList(mContext.getResources().getStringArray(R.array.drawer_my_account)));
            category.setId(i++ + "");
            category.setName(item);
            categories.add(category);
        }*/
    }

    public void setUser(User user) {
        this.user = user;
        notifyDataSetChanged();
    }

    public User getUser() {
        return user;
    }


    public void setCategories(ArrayList<Category> categories) {
        if (categories != null) {
            this.categories.addAll(categories);
            notifyDataSetChanged();
        }
    }

    public OnDrawerItemClick getOnDrawerItemClick() {
        return onDrawerItemClick;
    }

    @Override
    public Object getChild(int arg0, int arg1) {
        return arg1;
    }

    @Override
    public long getChildId(int groupPosition, int childPosition) {
        return childPosition;
    }

    @Override
    public View getChildView(int groupPosition, int childPosition,
                             boolean isLastChild, View convertView, ViewGroup parent) {
        if (groupPosition == 0)
            return null;
        if (convertView == null) {
            LayoutInflater inflater = (LayoutInflater) mContext
                    .getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            convertView = inflater.inflate(R.layout.layout_drawer_item, parent, false);
        }
        Category category = categories.get(groupPosition - 1);
        TextView lvFirst = (TextView) convertView.findViewById(R.id.tvDrawerItem);
        lvFirst.setTextSize(13);
        lvFirst.setText(category.getSubCats().get(childPosition).getName());
        convertView.findViewById(R.id.iv_menu_icon).setVisibility(View.GONE);

        RelativeLayout.LayoutParams params = null;
        if (android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.KITKAT) {
            params = new RelativeLayout.LayoutParams(new RelativeLayout.LayoutParams(RelativeLayout.LayoutParams.WRAP_CONTENT, RelativeLayout.LayoutParams.WRAP_CONTENT));
            params.setMargins(38, 0, 0, 0);
            lvFirst.setLayoutParams(params);
        }


        TextView tvNew = (TextView) convertView.findViewById(R.id.tvNew);
        if (groupPosition != 1) {
            tvNew.setTextSize(13);
            if (category.getSubCats().get(childPosition).getNew() != null && category.getSubCats().get(childPosition).getNew().equalsIgnoreCase("true"))
                tvNew.setVisibility(View.VISIBLE);
            else
                tvNew.setVisibility(View.GONE);
        } else
            tvNew.setVisibility(View.GONE);


        category.getSubCats().get(childPosition).setParent_page("" + category.getPage());
        category.getSubCats().get(childPosition).setChild_position("" + childPosition);
        convertView.setTag(category.getSubCats().get(childPosition));

        return convertView;
    }

    @Override
    public int getChildrenCount(int groupPosition) {
        return groupPosition == 0 ? 0 : categories.get(groupPosition - 1).getSubCats().size();
    }

    @Override
    public Object getGroup(int groupPosition) {
        return groupPosition;
    }

    @Override
    public int getGroupCount() {
        return categories.size() + 1;
    }

    @Override
    public long getGroupId(int groupPosition) {
        return groupPosition;
    }

    @Override
    public View getGroupView(int groupPosition, boolean isExpanded,
                             View convertView, ViewGroup parent) {
        LayoutInflater inflater = (LayoutInflater) this.mContext
                .getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        if (groupPosition == 0) {
            convertView = inflater.inflate(R.layout.layout_drawer_header, parent, false);
            //set user profile
            ImageView ivProfile = (ImageView) convertView.findViewById(R.id.ivProfile);

            if (user != null) {
                if (user.getUser_picture() != null && !user.getUser_picture().equalsIgnoreCase(""))
                    Picasso.with(mContext).load(user.getUser_picture()).into(ivProfile);

                ((TextView) convertView.findViewById(R.id.tvName)).
                        setText(user.getUser_first_name() + Constants.SPACE + user.getUser_last_name());

                if (!(user.getUser_style() == null))
                    ((TextView) convertView.findViewById(R.id.tvStyle)).setText(user.getUser_style());
                else
                    ((TextView) convertView.findViewById(R.id.tvStyle)).setText("");

                ImageView imgHeader = (ImageView) convertView.findViewById(R.id.imgHeader);
                if (user.getUser_background_image() != null && !user.getUser_background_image().equalsIgnoreCase(""))
                    Picasso.with(mContext).load(user.getUser_background_image()).into(imgHeader);
            }
            convertView.setTag(null);
            return convertView;
        } else if (categories.get(groupPosition - 1).isDivider())
            convertView = inflater.inflate(R.layout.layout_drawer_divider, parent, false);
        else
            convertView = inflater.inflate(R.layout.layout_drawer_item, parent, false);
        categories.get(groupPosition - 1).setPosition("" + groupPosition);
        convertView.setTag(categories.get(groupPosition - 1));

        if (convertView.getTag() != null && ((Category) convertView.getTag()).isDivider()) {
            return convertView;
        }

        TextView lvFirst = (TextView) convertView
                .findViewById(R.id.tvDrawerItem);
        lvFirst.setText(categories.get(groupPosition - 1).getName());
      /*  lvFirst.setCompoundDrawablesWithIntrinsicBounds(drawerRes.getDrawable(groupPosition - 1),
                null, null, null);
        lvFirst.setCompoundDrawablePadding(20);*/
        ImageView ivIcon = (ImageView) convertView.findViewById(R.id.iv_menu_icon);
        ivIcon.setVisibility(View.VISIBLE);
        Picasso.with(mContext).load(categories.get(groupPosition - 1).getIcon()).into(ivIcon);

        ImageView imageView = (ImageView) convertView.findViewById(R.id.ivDropDown);
        if (categories.get(groupPosition - 1).getSubCats().isEmpty()) {
            imageView.setVisibility(View.GONE);
        } else {
            imageView.setVisibility(View.VISIBLE);
            if (categories.size() > 0) {
                convertView.findViewById(R.id.pbWheel).setVisibility(View.GONE);
            } else {
                convertView.findViewById(R.id.pbWheel).setVisibility(View.VISIBLE);
            }
            if (isExpanded) {
                imageView.setImageResource(R.drawable.dropdownmenu_unselected);
            } else {
                imageView.setImageResource(R.drawable.dropdownmenu_selected);
            }
        }

        TextView tvNew = (TextView) convertView.findViewById(R.id.tvNew);
        if (groupPosition != 1) {
            tvNew.setTextSize(13);
            //tvNew.setBackgroundColor(mContext.getResources().getColor(R.color.md_pink_A700));
            if (categories.get(groupPosition - 1).getNewCat() != null && categories.get(groupPosition - 1).getNewCat().equalsIgnoreCase("true"))
                tvNew.setVisibility(View.VISIBLE);
            else
                tvNew.setVisibility(View.GONE);
        } else
            tvNew.setVisibility(View.GONE);
        return convertView;
    }

    @Override
    public boolean hasStableIds() {
        return true;
    }

    @Override
    public boolean isChildSelectable(int groupPosition, int childPosition) {
        return true;
    }

    public OnDrawerItemClick setOnDrawerItemClick(OnDrawerItemClick onDrawerItemClick) {
        this.onDrawerItemClick = onDrawerItemClick;
        return onDrawerItemClick;
    }

    public interface OnDrawerItemClick {
        void onChildItemClick(Object tag);
    }
}