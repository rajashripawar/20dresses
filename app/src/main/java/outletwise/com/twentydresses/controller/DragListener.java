package outletwise.com.twentydresses.controller;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.DragEvent;
import android.view.View;

import outletwise.com.twentydresses.R;
import outletwise.com.twentydresses.utilities.Constants;
import outletwise.com.twentydresses.controller.adapter.HorizontalGridAdapter;
import outletwise.com.twentydresses.view.custom.FavoritesButton;

/**
 * Created by User-PC on 27/07/2015.
 */
public class DragListener implements View.OnDragListener {
    public static final int PAGE_ADDED = 1;
    public static final int PAGE_REMOVED = 2;

    private onDropComplete dropComplete;
    private Context context;
    private int tag;

    public DragListener(Context context, onDropComplete dropComplete, int tag) {
        this.dropComplete = dropComplete;
        this.context = context;
        this.tag = tag;
    }

    @Override
    public boolean onDrag(View v, DragEvent event) {
        switch (event.getAction()) {
            case DragEvent.ACTION_DRAG_STARTED:
                break;
            case DragEvent.ACTION_DRAG_ENTERED:
                break;
            case DragEvent.ACTION_DRAG_EXITED:
                break;
            case DragEvent.ACTION_DROP:
                actionDrop(v, event);
                break;
            case DragEvent.ACTION_DRAG_ENDED:
                break;
            case DragEvent.ACTION_DRAG_LOCATION:
                break;
            default:
        }
        return true;
    }

    // Dropped, reassign View to ViewGroup
    private void actionDrop(View v, DragEvent event) {
        View view = (View) event.getLocalState();
        RecyclerView container = (RecyclerView) v;
        if (tag == Constants.SELECT_STYLE_ADAPTER) {
            HorizontalGridAdapter adapter = (HorizontalGridAdapter) container.getAdapter();
            adapter.addItem(view.getTag());
            dropComplete.dropComplete(view, PAGE_REMOVED, null);
        } else if (tag == Constants.FAVORITES_ADAPTER) {
            ((FavoritesButton) view.findViewById(R.id.bFavorites)).startProgress();
            dropComplete.dropComplete(v, Constants.TAG_SHORTLIST, view.getTag());
        }
    }

    public interface onDropComplete {
        void dropComplete(View view, int tag, Object obj);
    }
}
