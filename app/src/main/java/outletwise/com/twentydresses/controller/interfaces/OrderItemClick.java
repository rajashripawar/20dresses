package outletwise.com.twentydresses.controller.interfaces;

import outletwise.com.twentydresses.model.Order;
import outletwise.com.twentydresses.model.Reason;
import outletwise.com.twentydresses.utilities.Constants;

/**
 * Created by User-PC on 04-11-2015.
 */
public interface OrderItemClick {
    void onItemClicked(Order order,Constants.OrderAction orderAction,Reason.ReasonsEntity reason);
}
