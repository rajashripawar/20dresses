package outletwise.com.twentydresses.controller.receiver;

import android.app.Notification;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.drawable.Drawable;
import android.support.v4.app.NotificationCompat;

import com.google.gson.Gson;
import com.parse.ParsePushBroadcastReceiver;
import com.squareup.picasso.Picasso;
import com.squareup.picasso.Target;

import org.json.JSONException;
import org.json.JSONObject;

import outletwise.com.twentydresses.R;
import outletwise.com.twentydresses.model.CustomNotification;
import outletwise.com.twentydresses.utilities.Constants;
import outletwise.com.twentydresses.view.activity.SplashScreen;

/**
 * Created by User-PC on 17-11-2015.
 */
public class CustomParseReceiver extends ParsePushBroadcastReceiver {

    private Intent parseIntent;
    private Context context;
    private CustomNotification notification;

    @Override
    protected void onPushReceive(Context context, Intent intent) {
        super.onPushReceive(context, intent);
        if (intent == null)
            return;

        this.context = context;
        this.parseIntent = intent;

        try {
            JSONObject json = new JSONObject(intent.getExtras().getString("com.parse.Data"));
            Constants.debug("Push received: " + json);
            notification = new Gson().fromJson(json.toString(), CustomNotification.class);
        } catch (JSONException e) {
            Constants.error("Push message json exception: " + e.getMessage());
        }

        if (notification != null)
            createNotification();
    }

    /**
     * Picasso issue #1034 loading notification twice
     * */
    void createNotification() {
        final NotificationManager mNotificationManager = (NotificationManager) context.getSystemService(Context.NOTIFICATION_SERVICE);
        final NotificationCompat.BigPictureStyle notiStyle = new
                NotificationCompat.BigPictureStyle();
        notiStyle.setBigContentTitle(notification.getTitle());
        notiStyle.setSummaryText(notification.getText());

        Intent intent = new Intent(context, SplashScreen.class);
        PendingIntent resultPendingIntent = PendingIntent.getActivity(context, 0, intent, PendingIntent.FLAG_UPDATE_CURRENT);

        final NotificationCompat.Builder myNotificationBuilder = new NotificationCompat.Builder(context);
        myNotificationBuilder.setSmallIcon(R.drawable.ic_launcher)
                .setAutoCancel(true)
                .setContentIntent(resultPendingIntent)
                .setContentTitle(notification.getTitle())
                .setContentText(notification.getText())
                .setStyle(notiStyle)
                .setSmallIcon(R.drawable.ic_launcher);

        Target target = new Target() {
            @Override
            public void onBitmapLoaded(Bitmap bitmap, Picasso.LoadedFrom from) {
                Constants.debug("Showing notification with image");
                myNotificationBuilder.setLargeIcon(bitmap);
                notiStyle.bigPicture(bitmap);
                final Notification myNotification = myNotificationBuilder.build();
                myNotification.defaults |= Notification.DEFAULT_LIGHTS;
                myNotification.defaults |= Notification.DEFAULT_VIBRATE;
                myNotification.defaults |= Notification.DEFAULT_SOUND;
                myNotification.flags |= Notification.FLAG_ONLY_ALERT_ONCE;
                mNotificationManager.notify(0, myNotification);
            }

            @Override
            public void onBitmapFailed(Drawable errorDrawable) {
                Constants.debug("Showing notification without image");
                final Notification myNotification = myNotificationBuilder.build();
                myNotification.defaults |= Notification.DEFAULT_LIGHTS;
                myNotification.defaults |= Notification.DEFAULT_VIBRATE;
                myNotification.defaults |= Notification.DEFAULT_SOUND;
                myNotification.flags |= Notification.FLAG_ONLY_ALERT_ONCE;
                mNotificationManager.notify(0, myNotification);
            }

            @Override
            public void onPrepareLoad(Drawable placeHolderDrawable) {

            }
        };
        Picasso.with(context).load(notification.getImage()).into(target);
    }
}
