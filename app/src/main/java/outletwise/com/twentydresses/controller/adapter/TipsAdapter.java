package outletwise.com.twentydresses.controller.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.squareup.picasso.Picasso;

import java.util.ArrayList;
import java.util.List;

import outletwise.com.twentydresses.R;
import outletwise.com.twentydresses.model.Recommended;
import outletwise.com.twentydresses.utilities.Constants;

/**
 * Created by User-PC on 06-08-2015.
 */
public class TipsAdapter extends BaseRecyclerAdapter implements View.OnClickListener {

    private ArrayList<Recommended.Tip> lock_data;

    public TipsAdapter(Context context, ArrayList<Recommended.Tip> items, ArrayList<Recommended.Tip> lock_data) {
        super(context, items);
        this.lock_data = lock_data;
    }

    @Override
    public void onClick(View v) {
        if (mItemClickListener != null) {
            mItemClickListener.onItemClick(v, (Integer) v.getTag());
        }
    }


    private class ViewHolder extends BaseViewHolder {

        public View mView;

        public ImageView ivProduct1;
        public ImageView ivProduct2;
        public ImageView ivProduct3;
        //public ImageView ivProduct4;
        public TextView tvProductCount;
        public TextView tvFooter;
        public List<ImageView> thumbnails = new ArrayList<>();
        RelativeLayout rl_cluster_lock;
        ImageView ivCluster;

        public ViewHolder(View view) {
            super(view);
            mView = view;
            ivProduct1 = (ImageView) view.findViewById(R.id.ivProduct1);
            ivProduct2 = (ImageView) view.findViewById(R.id.ivProduct2);
            ivProduct3 = (ImageView) view.findViewById(R.id.ivProduct3);
            // ivProduct4 = (ImageView) view.findViewById(R.id.ivProduct4);
            thumbnails.add(ivProduct1);
            thumbnails.add(ivProduct2);
            thumbnails.add(ivProduct3);
            // thumbnails.add(ivProduct4);
            tvProductCount = (TextView) view.findViewById(R.id.tvProductCount);
            tvFooter = (TextView) view.findViewById(R.id.tvFooterText);
            rl_cluster_lock = (RelativeLayout) view.findViewById(R.id.rl_cluster_lock);
            ivCluster = (ImageView) view.findViewById(R.id.ivCluster);
        }

        @Override
        public void onClick(View v) {
            if (mItemClickListener != null) {
                mItemClickListener.onItemClick(v, getAdapterPosition());
            }
        }
    }

    @Override
    public BaseViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.layout_adapter_recommendations, parent, false);
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(BaseViewHolder holder, int position) {

        if (position < items.size()) {
            //Normal cluster

            ViewHolder viewHolder = (ViewHolder) holder;
            Recommended.Tip tip = (Recommended.Tip) items.get(position);
            viewHolder.itemView.setTag(tip);

            for (int i = 0; i < tip.getThumbnails().size() - 1; i++)
                Picasso.with(context).load(tip.getThumbnails().get(i)).into(viewHolder.thumbnails.get(i));
            int totaltxtproductcount =  tip.getTotal()-3;

            viewHolder.tvProductCount.setText("+" +totaltxtproductcount );
            viewHolder.tvFooter.setText(tip.getTip_text());
            //      viewHolder.rl_cluster_lock.setTag(position);
            viewHolder.rl_cluster_lock.setVisibility(View.GONE);

        } else {
            //Locked Cluster
            ViewHolder viewHolder = (ViewHolder) holder;
            int tempPosition = position - items.size();
            Recommended.Tip tip = (Recommended.Tip) lock_data.get(tempPosition);
            viewHolder.itemView.setTag(tip);

            if (viewHolder.thumbnails != null && viewHolder.thumbnails.size() > 0) {
                for (int i = 0; i < tip.getThumbnails().size() - 1; i++)
                    Picasso.with(context).load(tip.getThumbnails().get(i)).into(viewHolder.thumbnails.get(i));
            }
            int totaltxtproductcount =  tip.getTotal()-3;
            viewHolder.tvProductCount.setText("+" + totaltxtproductcount);
            viewHolder.tvFooter.setText(tip.getTip_text());

            Picasso.with(context).load(tip.getTip_lock_image()).into(viewHolder.ivCluster);
            viewHolder.rl_cluster_lock.setVisibility(View.VISIBLE);

            viewHolder.ivCluster.setTag(tempPosition);
            viewHolder.ivCluster.setOnClickListener(this);
        }

    }

    @Override
    public int getItemCount() {
        return items.size() + lock_data.size();
    }
}