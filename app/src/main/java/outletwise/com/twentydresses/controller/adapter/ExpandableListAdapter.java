package outletwise.com.twentydresses.controller.adapter;

import android.content.Context;
import android.content.res.Resources;
import android.graphics.Paint;
import android.graphics.drawable.ColorDrawable;
import android.util.TypedValue;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseExpandableListAdapter;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.NumberPicker;
import android.widget.TableLayout;
import android.widget.TableRow;
import android.widget.TextView;

import java.lang.reflect.Field;
import java.util.HashMap;
import java.util.List;

import outletwise.com.twentydresses.R;
import outletwise.com.twentydresses.model.Size;
import outletwise.com.twentydresses.model.UserAttrEntity;
import outletwise.com.twentydresses.utilities.Constants;

/**
 * Created by User-PC on 28-07-2015.
 */
public class ExpandableListAdapter extends BaseExpandableListAdapter implements NumberPicker.OnValueChangeListener {

    private static final String[] HT_FOOT = {"4\'", "5\'", "6\'"};
    private static final String[] HT_INCH = {"0\"", "1\"", "2\"", "3\"", "4\"", "5\"", "6\"", "7\"", "8\"", "9\"", "10\"", "11\""};
    private static final int NO_OF_ROWS = 2;
    private static final int CHILD_FOR_DRESS_SIZE = 3;
    private static final int CHILD_FOR_PANT_SHOE_SIZE = 4;

    private Context mContext;
    private List<String> mListDataHeader; // header titles
    private View expandedHeaderView;
    // child data in format of header title, child title
    private HashMap<String, List<Size>> mListDataChild;
    private HashMap<Constants.SizeTypes, Integer> selectedSize = new HashMap<>();

    private ChildItemClick clickListener = new ChildItemClick();
    private OnSizeSelected sizeSelected;
    private NumberPicker htFootPicker;
    private NumberPicker htInchPicker;
    private int footValue = 5, inchValue = 5;
    private UserAttrEntity userAttr;

    public ExpandableListAdapter(Context context, List<String> listDataHeader,
                                 HashMap<String, List<Size>> listChildData, OnSizeSelected sizeSelected) {
        this.mContext = context;
        this.mListDataHeader = listDataHeader;
        this.mListDataChild = listChildData;
        this.sizeSelected = sizeSelected;
    }

    @Override
    public List<Size> getChild(int groupPosition, int childPosition) {
        return this.mListDataChild.get(this.mListDataHeader.get(groupPosition));
    }

    @Override
    public long getChildId(int groupPosition, int childPosition) {
        return childPosition;
    }

    @Override
    public View getChildView(int groupPosition, final int childPosition,
                             boolean isLastChild, View convertView, ViewGroup parent) {

        final List<Size> child = getChild(groupPosition, childPosition);
        LayoutInflater inflater = (LayoutInflater) this.mContext
                .getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        switch (groupPosition) {
            case 0:
                convertView = inflater.inflate(R.layout.layout_expandlist_dress_content, parent, false);
                convertView.setTag("Dress Size");
                setView(convertView, child, R.layout.layout_option_dress, NO_OF_ROWS, CHILD_FOR_DRESS_SIZE);
                break;
            case 1:
            case 2:
                convertView = inflater.inflate(R.layout.layout_expandlist_pant_shoe_content, parent, false);
                convertView.setTag("Pant Size");
                setView(convertView, child, R.layout.layout_option_pant_shoe, NO_OF_ROWS, CHILD_FOR_PANT_SHOE_SIZE);
                break;
            case 3:
                convertView = inflater.inflate(R.layout.layout_height_picker, parent, false);
                convertView.setTag("Height");
                htFootPicker = (NumberPicker) convertView.findViewById(R.id.htfootPicker);
                htFootPicker.setTag("Foot");
                htFootPicker.setMinValue(4);
                htFootPicker.setMaxValue(6);
                htFootPicker.setValue(footValue);
                htFootPicker.setOnValueChangedListener(this);
                htFootPicker.setDisplayedValues(HT_FOOT);

                htInchPicker = (NumberPicker) convertView.findViewById(R.id.htinchPicker);
                htInchPicker.setTag("Inch");
                htInchPicker.setMinValue(0);
                htInchPicker.setMaxValue(11);
                htInchPicker.setValue(inchValue);
                htInchPicker.setOnValueChangedListener(this);
                htInchPicker.setDisplayedValues(HT_INCH);

                setNumberPickerTextColor(htFootPicker, mContext.getResources().getColor(R.color.color_accent));
                setNumberPickerTextColor(htInchPicker, mContext.getResources().getColor(R.color.color_accent));
                setDividerColor(htFootPicker, mContext.getResources().getColor(R.color.md_black_1000));
                setDividerColor(htInchPicker, mContext.getResources().getColor(R.color.md_black_1000));
                if (userAttr != null && userAttr.getHeight() != null) {

                    if (!userAttr.getHeight().equalsIgnoreCase("")) {
                        String height[] = userAttr.getHeight().split("\\.");

                        htFootPicker.setValue(Integer.parseInt(height[0]));
                        htInchPicker.setValue(Integer.parseInt(height[1]));
                    }

                }

                break;
        }

        return convertView;
    }

    private void setView(View convertView, List<Size> child, int textResId, int noOfRows, int childPerRow) {
        TableLayout tableLayout = (TableLayout) convertView;
        int position = 0;

        for (int i = 0; i < noOfRows; i++) {
            TableRow row = new TableRow(mContext);
            row.setGravity(Gravity.CENTER_HORIZONTAL);
            int j = 0;
            while (position < child.size() && j < childPerRow) {
                Size size = child.get(position++);
                int margin, textSize, width, height;
                if (childPerRow == CHILD_FOR_DRESS_SIZE) {
                    margin = mContext.getResources().getDimensionPixelSize(R.dimen.text_measure_dress_margin);
                    textSize = mContext.getResources().getDimensionPixelSize(R.dimen.text_size_measure_dress);
                    width = mContext.getResources().getDimensionPixelSize(R.dimen.layout_option_dress_width);
                    height = mContext.getResources().getDimensionPixelSize(R.dimen.layout_option_dress_height);
                } else {
                    margin = mContext.getResources().getDimensionPixelSize(R.dimen.text_measure_shoe_pant_margin);
                    textSize = mContext.getResources().getDimensionPixelSize(R.dimen.text_size_measure_shoe_pant);
                    width = mContext.getResources().getDimensionPixelSize(R.dimen.layout_option_pant_shoe_width);
                    height = mContext.getResources().getDimensionPixelSize(R.dimen.layout_option_pant_shoe_height);
                }
                TextView textView = (TextView) View.inflate(mContext, textResId, null);
                textView.setText(size.getSize());
                textView.setTag(size);
                textView.setOnClickListener(clickListener);
                textView.setTextSize(TypedValue.COMPLEX_UNIT_PX, textSize);

                if (userAttr != null) {
                    if (size.getType() == Constants.SizeTypes.DRESS) {
                        String strSize = size.getSize().replace("\n", " ");
                        if (strSize.equalsIgnoreCase(userAttr.getSize().trim()))
                            textView.setSelected(true);
                    } else if (size.getType() == Constants.SizeTypes.PANT) {
                        if (size.getSize().equalsIgnoreCase(userAttr.getBottoms_Size().replace("Inch", "").trim()))
                            textView.setSelected(true);
                    } else if (size.getType() == Constants.SizeTypes.SHOE) {
                        if (size.getSize().equalsIgnoreCase(userAttr.getShoes_Size().trim()))
                            textView.setSelected(true);
                    }
                }

                TableRow.LayoutParams params = new TableRow.LayoutParams(width, height);
                params.setMargins(margin, margin, margin, margin);
                textView.setLayoutParams(params);
                int id = selectedSize.get(size.getType()) == null ? 999 : selectedSize.get(size.getType());
                if (id == size.getSizeId())
                    textView.setSelected(true);
                row.addView(textView);
                j++;
            }
            tableLayout.addView(row);
        }
    }

    @Override
    public int getChildrenCount(int groupPosition) {
        return 1;
    }

    @Override
    public Object getGroup(int groupPosition) {
        return this.mListDataHeader.get(groupPosition);
    }

    @Override
    public int getGroupCount() {
        return this.mListDataHeader.size();
    }

    @Override
    public long getGroupId(int groupPosition) {
        return groupPosition;
    }

    @Override
    public View getGroupView(int groupPosition, boolean isExpanded,
                             View convertView, ViewGroup parent) {
        String headerTitle = (String) getGroup(groupPosition);
        if (convertView == null) {
            LayoutInflater inflater = (LayoutInflater) this.mContext
                    .getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            convertView = inflater.inflate(R.layout.layout_expadlist_header, parent, false);
        }

        TextView lblListHeader = (TextView) convertView
                .findViewById(R.id.tvListHeader);
        lblListHeader.setText(headerTitle);
        switch (groupPosition) {
            case 0:
                lblListHeader.setCompoundDrawablesWithIntrinsicBounds(R.drawable.ic_topsize, 0, 0, 0);
                break;
            case 1:
                lblListHeader.setCompoundDrawablesWithIntrinsicBounds(R.drawable.ic_waistsize, 0, 0, 0);
                break;
            case 2:
                lblListHeader.setCompoundDrawablesWithIntrinsicBounds(R.drawable.ic_shoesize, 0, 0, 0);
                break;
            case 3:
                lblListHeader.setCompoundDrawablesWithIntrinsicBounds(R.drawable.ic_height_unselected, 0, 0, 0);
                break;

        }
        ImageView imageView = (ImageView) convertView.findViewById(R.id.ivDropDown);
        if (isExpanded) {
            imageView.setImageResource(R.drawable.dropdownmenu_selected);
            expandedHeaderView = convertView;
        } else
            imageView.setImageResource(R.drawable.dropdownmenu_unselected);

        if (userAttr != null) {
            TextView textView = ((TextView) convertView.findViewById(R.id.tvMeasure));
            textView.setVisibility(View.VISIBLE);
            switch (groupPosition) {
                case 0:
                    textView.setText(userAttr.getSize());
                    break;
                case 1:
                    textView.setText(userAttr.getBottoms_Size().replace("Inch", ""));
                    break;
                case 2:
                    textView.setText(userAttr.getShoes_Size());
                    break;
                case 3:
                    textView.setText(userAttr.getHeight());
                    break;
            }
        }
        return convertView;
    }

    public View getExpandedHeaderView() {
        return expandedHeaderView;
    }

    @Override
    public boolean hasStableIds() {
        return false;
    }

    @Override
    public boolean isChildSelectable(int groupPosition, int childPosition) {
        return true;
    }

    @Override
    public void onValueChange(NumberPicker picker, int oldVal, int newVal) {
        switch ((String) picker.getTag()) {
            case "Foot":
                footValue = newVal;
                break;
            case "Inch":
                inchValue = newVal;
                break;
        }

    }

    class ChildItemClick implements View.OnClickListener {

        @Override
        public void onClick(View v) {
            Size size = (Size) v.getTag();
            selectedSize.put(size.getType(), size.getSizeId());
            sizeSelected.onSizeSelected(size);
        }
    }

    public static boolean setNumberPickerTextColor(NumberPicker numberPicker, int color) {
        final int count = numberPicker.getChildCount();
        for (int i = 0; i < count; i++) {
            View child = numberPicker.getChildAt(i);
            if (child instanceof EditText) {
                try {
                    Field selectorWheelPaintField = numberPicker.getClass()
                            .getDeclaredField("mSelectorWheelPaint");
                    selectorWheelPaintField.setAccessible(true);
                    ((Paint) selectorWheelPaintField.get(numberPicker)).setColor(color);
                    ((EditText) child).setTextColor(color);
                    numberPicker.invalidate();
                    return true;
                } catch (NoSuchFieldException e) {
                    e.printStackTrace();
                } catch (IllegalAccessException e) {
                    e.printStackTrace();
                } catch (IllegalArgumentException e) {
                    e.printStackTrace();
                }
            }
        }
        return false;
    }

    public void setUserAttr(UserAttrEntity userAttr) {
        this.userAttr = userAttr;
    }

    private void setDividerColor(NumberPicker picker, int color) {

        java.lang.reflect.Field[] pickerFields = NumberPicker.class.getDeclaredFields();
        for (java.lang.reflect.Field pf : pickerFields) {
            if (pf.getName().equals("mSelectionDivider")) {
                pf.setAccessible(true);
                try {
                    ColorDrawable colorDrawable = new ColorDrawable(color);
                    pf.set(picker, colorDrawable);
                } catch (IllegalArgumentException e) {
                    e.printStackTrace();
                } catch (Resources.NotFoundException e) {
                    e.printStackTrace();
                } catch (IllegalAccessException e) {
                    e.printStackTrace();
                }
                break;
            }
        }
    }

    public String getHeight() {

        String mHeight = "";
        if (htFootPicker != null && htInchPicker != null)
            mHeight = htFootPicker.getValue() + "." + htInchPicker.getValue();

        return mHeight;
    }

    public interface OnSizeSelected {
        void onSizeSelected(Size size);
    }
}