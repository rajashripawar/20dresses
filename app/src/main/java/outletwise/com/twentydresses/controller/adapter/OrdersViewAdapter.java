package outletwise.com.twentydresses.controller.adapter;

import android.app.Activity;
import android.content.Context;
import android.graphics.Color;
import android.graphics.Paint;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.Spinner;
import android.widget.TextView;

import com.squareup.picasso.Picasso;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import outletwise.com.twentydresses.R;
import outletwise.com.twentydresses.model.OrderItem;
import outletwise.com.twentydresses.model.Product;
import outletwise.com.twentydresses.model.Reason;
import outletwise.com.twentydresses.model.ReturnExchangeRequest;
import outletwise.com.twentydresses.utilities.Constants;
import outletwise.com.twentydresses.view.activity.BaseActivity;
import outletwise.com.twentydresses.view.custom.CustomFancyButton;

/**
 * Created by User-PC on 04-11-2015.
 */
public class OrdersViewAdapter implements View.OnClickListener {

    private ArrayList<OrderItem.OrderItemsEntity> items;
    private Context context;
    private LinearLayout linearLayout;
    private boolean isReturnExchange;
    private ReturnExchange onReturnExchange;
    private List<Reason.ReasonsEntity> reasons;
    private TextView tvProductName, tvProductPrice, tvProductSize, tvProductQty, tvProductFinalPrice;
    private ImageView ivProductImage;
    private List<TextView> textReasons = new ArrayList<>();
    private List<TextView> textSizes = new ArrayList<>();
    private List<Spinner> spinners = new ArrayList<>();
    private List<CustomFancyButton> bReturnList = new ArrayList<>();
    private List<CustomFancyButton> bExchangeList = new ArrayList<>();
    private List<LinearLayout> llSizes = new ArrayList<>();
    private ReturnExchangeRequest returnExchangeRequest = new ReturnExchangeRequest();
    private ArrayList<Product.ProductSizesEntity> productSizes;
    private Constants.OrderAction orderAction;

    public OrdersViewAdapter(Context context, ArrayList<OrderItem.OrderItemsEntity> items, boolean isReturnExchange, Constants.OrderAction orderAction) {
        this.context = context;
        this.items = items;
        this.linearLayout = (LinearLayout) ((Activity) context).findViewById(R.id.rvOrders);
        //this.isReturnExchange = isReturnExchange;
        this.orderAction = orderAction;

        addItems();
    }

    private void addItems() {
        int i = 0;
        for (final OrderItem.OrderItemsEntity orderItem : items) {
            final View itemLayout = LayoutInflater.from(context).inflate(
                    R.layout.layout_adapter_order_view, null);
            tvProductName = (TextView) itemLayout.findViewById(R.id.tvProductName);
            tvProductPrice = (TextView) itemLayout.findViewById(R.id.tvProductPrice);
            tvProductSize = (TextView) itemLayout.findViewById(R.id.tvProductSize);
            tvProductQty = (TextView) itemLayout.findViewById(R.id.tvProductQty);
            tvProductFinalPrice = (TextView) itemLayout.findViewById(R.id.tvFinalProductPrice);
            ivProductImage = (ImageView) itemLayout.findViewById(R.id.ivProductImage);
            LinearLayout llSize = (LinearLayout) itemLayout.findViewById(R.id.llSizes);
            this.llSizes.add(llSize);
            CustomFancyButton bReturn = (CustomFancyButton) itemLayout.findViewById(R.id.bReturn);
            CustomFancyButton bExchange = (CustomFancyButton) itemLayout.findViewById(R.id.bExchange);
            bReturn.setSelectedColor(context.getResources().getColor(R.color.color_accent));
            bReturn.setSelectedTextColor(context.getResources().getColor(R.color.color_text_white));
            bExchange.setSelectedColor(context.getResources().getColor(R.color.color_accent));
            bExchange.setSelectedTextColor(context.getResources().getColor(R.color.color_text_white));
            bReturn.setTag(i);
            bExchange.setTag(i);
            bReturnList.add(bReturn);
            bExchangeList.add(bExchange);

            this.isReturnExchange = orderItem.isItem_retrun();

            if (isReturnExchange) {
                bExchange.setVisibility(View.VISIBLE);
                bReturn.setVisibility(View.VISIBLE);
            } else {
                bExchange.setVisibility(View.INVISIBLE);
                bReturn.setVisibility(View.INVISIBLE);
            }

          /*  if (!isReturnExchange) {
                bExchange.setVisibility(View.INVISIBLE);
                bReturn.setVisibility(View.INVISIBLE);
            }*/

            if (orderAction != null && orderAction == Constants.OrderAction.VIEW) {
                bExchange.setVisibility(View.INVISIBLE);
                bReturn.setVisibility(View.INVISIBLE);
            }


            final Spinner spinner = (Spinner) itemLayout.findViewById(R.id.spReasons1);
            spinner.setTag(i);
            spinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
                @Override
                public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                    if (position == 0)
                        return;
                    if (returnExchangeRequest.getReturnItem((Integer) spinner.getTag()) != null) {
                        ReturnExchangeRequest.Return returnItem = returnExchangeRequest.getReturnItem((Integer) spinner.getTag());
                        returnItem.setItem_reason_id(((Reason.ReasonsEntity) view.getTag()).getReason_id());
                        returnItem.setRefund_amount(orderItem.getItem_refund_amount());
                    } else {
                        ReturnExchangeRequest.Exchange exchangeItem = returnExchangeRequest.getExchangeItem((Integer) spinner.getTag());
                        exchangeItem.setItem_reason_id(((Reason.ReasonsEntity) view.getTag()).getReason_id());
                    }
                }

                @Override
                public void onNothingSelected(AdapterView<?> parent) {

                }
            });

            spinners.add(spinner);

            TextView textReason = (TextView) itemLayout.findViewById(R.id.tvSelectReason);
            textReasons.add(textReason);
            final TextView textSize = (TextView) itemLayout.findViewById(R.id.tvSelectSize);
            textSizes.add(textSize);

            itemLayout.setTag(orderItem);
            tvProductName.setText(orderItem.getItem_name());
            tvProductPrice.setText(Constants.RUPEE +
                    Constants.formatAmount(orderItem.getItem_mrp()));


            // if return Exchange don't show status & item final price

            if (orderAction != null && orderAction == Constants.OrderAction.RETURN_EXCHANGE) {
                ((TextView) itemLayout.findViewById(R.id.tvItemStatus)).setText("Refund Amount : " + Constants.RUPEE + Constants.formatAmount(orderItem.getItem_refund_amount()));
                tvProductFinalPrice.setVisibility(View.GONE);
            } else {
                ((TextView) itemLayout.findViewById(R.id.tvItemStatus)).setText(orderItem.getItem_status());
                tvProductFinalPrice.
                        setText(Constants.RUPEE + Constants.formatAmount(orderItem.getItem_price()));
            }


            if (orderItem.getItem_discount() != 0) {
                itemLayout.findViewById(R.id.tvProductDiscountPrice).setVisibility(View.VISIBLE);
                ((TextView) itemLayout.findViewById(R.id.tvProductDiscountPrice)).setText(Constants.RUPEE + Constants.getPriceWithDiscount(orderItem.getItem_mrp(), String.valueOf(orderItem.getItem_discount())));
                tvProductPrice.setPaintFlags(tvProductPrice.getPaintFlags() | Paint.STRIKE_THRU_TEXT_FLAG);
                tvProductPrice.setTextColor(Color.parseColor("#D50000"));
            } else
                itemLayout.findViewById(R.id.tvProductDiscountPrice).setVisibility(View.GONE);


            if (orderItem.getItem_savings() == 0) {
                itemLayout.findViewById(R.id.tvProductSaving).setVisibility(View.GONE);
            } else
                ((TextView) itemLayout.findViewById(R.id.tvProductSaving)).setText("Savings: " + Constants.RUPEE +
                        Constants.formatAmount(orderItem.getItem_savings()));

            if (orderItem.getItem_size() == null)
                tvProductSize.setVisibility(View.GONE);
            else
                tvProductSize.setText("Size: " + orderItem.getItem_size());
            tvProductQty.setText("Quantity: " + orderItem.getItem_quantity());
         /*   tvProductFinalPrice.
                    setText(Constants.RUPEE + Constants.getPriceWithDiscount(orderItem.getItem_price(), orderItem.getItem_savings()));*/

            Picasso.with(context).load(orderItem.getItem_image()).into(ivProductImage);

            bReturn.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    int position = (Integer) v.getTag();
                    //If the item is in exchange remove from exchange and add to return list
                    if (returnExchangeRequest.getExchangeItem(position) != null)
                        returnExchangeRequest.removeExchangeItem(position);

                    ReturnExchangeRequest.Return returnItem = new ReturnExchangeRequest.Return();
                    returnItem.setItem_id(items.get(position).getItem_id());
                    returnExchangeRequest.addReturn(position, returnItem);

                    //toggle buttons
                    bReturnList.get(position).setSelected(true);
                    bExchangeList.get(position).setSelected(false);
                    textSizes.get((Integer) itemLayout.getTag()).setVisibility(View.GONE);
                    llSizes.get((Integer) itemLayout.getTag()).setVisibility(View.GONE);
                    if (onReturnExchange != null)
                        onReturnExchange.onReturnExchangeClick("return", (Integer) itemLayout.getTag());
                }
            });

            bExchange.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    int position = (Integer) v.getTag();

                    if (returnExchangeRequest.getReturnItem(position) != null)
                        returnExchangeRequest.removeReturnItem(position);

                    ReturnExchangeRequest.Exchange exchangeItem = new ReturnExchangeRequest.Exchange();
                    exchangeItem.setItem_id(items.get(position).getItem_id());
                    returnExchangeRequest.addExchange(position, exchangeItem);

                    bReturnList.get(position).setSelected(false);
                    bExchangeList.get(position).setSelected(true);
                    if (onReturnExchange != null)
                        onReturnExchange.onReturnExchangeClick("exchange", (Integer) itemLayout.getTag());
                }
            });

            itemLayout.setTag(i);
            if (this.linearLayout != null)
                this.linearLayout.addView(itemLayout);
            i++;

            itemLayout.setOnClickListener(this);
        }
    }

    public void setReasons(List<Reason.ReasonsEntity> reasons, int position, String strMsg) {
        this.reasons = reasons;
        if (reasons != null) {
            Reason.ReasonsEntity reasonsEntity = new Reason.ReasonsEntity();
            reasonsEntity.setReason_id("-1");
            reasonsEntity.setReason("Select Reason");
            this.reasons.add(0, reasonsEntity);
            ReasonsAdapter adapter = new ReasonsAdapter(context, R.layout.layout_adapter_reasons, this.reasons);
            spinners.get(position).setAdapter(adapter);
            spinners.get(position).setVisibility(View.VISIBLE);
            textReasons.get(0).setText("Select Reason");
            textReasons.get(position).setVisibility(View.VISIBLE);

        } else {
            spinners.get(position).setVisibility(View.GONE);
            textReasons.get(position).setVisibility(View.VISIBLE);
            textReasons.get(0).setText(strMsg);
        }
    }

    public void setOnReturnExchange(ReturnExchange onReturnExchange) {
        this.onReturnExchange = onReturnExchange;
    }

    public void setSizes(ArrayList<Product.ProductSizesEntity> productSizes, final int position) {
        this.productSizes = productSizes;
        if (productSizes != null && productSizes.size() > 0) {

            if (productSizes.size() == 1 && productSizes.get(0).getAttr_id().equalsIgnoreCase(Constants.DEFAULT_SIZE_ID)) {
                ReturnExchangeRequest.Exchange exchangeItem = returnExchangeRequest.getExchangeItem(position);
                exchangeItem.setItem_size_id(productSizes.get(0).getAttr_id());
                textSizes.get(position).setVisibility(View.GONE);
                llSizes.get(position).setVisibility(View.GONE);
            } else {
                textSizes.get(position).setVisibility(View.VISIBLE);
                llSizes.get(position).setVisibility(View.VISIBLE);
            }
            SizesAdapter adapter = new SizesAdapter(context, productSizes, llSizes.get(position), false);
            adapter.setSizeSelected(new SizesAdapter.SizeSelected() {
                @Override
                public void onSizeSelected(Product.ProductSizesEntity size) {
                    ReturnExchangeRequest.Exchange exchangeItem = returnExchangeRequest.getExchangeItem(position);
                    exchangeItem.setItem_size_id(size.getAttr_id());
                }
            });
        }
    }

    public boolean validate() {
        if (returnExchangeRequest.getReturnMap().size() == 0 && returnExchangeRequest.getExchangeMap().size() == 0) {
            ((BaseActivity) context).showToast("Please select item to return or exchange");
            return false;
        }

        HashMap<Integer, ReturnExchangeRequest.Return> returnHashMap = returnExchangeRequest.getReturnMap();
        for (Map.Entry<Integer, ReturnExchangeRequest.Return> entry : returnHashMap.entrySet()) {
            ReturnExchangeRequest.Return returnItem = entry.getValue();

            if (returnItem.getItem_id() == null || returnItem.getItem_reason_id() == null) {
                ((BaseActivity) context).showToast("Please select reason");
                return false;
            }
        }

        HashMap<Integer, ReturnExchangeRequest.Exchange> exchangeHashMap = returnExchangeRequest.getExchangeMap();
        for (Map.Entry<Integer, ReturnExchangeRequest.Exchange> entry : exchangeHashMap.entrySet()) {
            ReturnExchangeRequest.Exchange exchangeItem = entry.getValue();

            if (productSizes == null) {
                ((BaseActivity) context).showToast("You can't exchange the product as it is out of stock.");
                return false;
            } else if (exchangeItem.getItem_id() == null || exchangeItem.getItem_reason_id() == null) {
                ((BaseActivity) context).showToast("Please select reason");
                return false;
            } else if (productSizes != null && productSizes.size() > 0 && exchangeItem.getItem_size_id() == null) {
                ((BaseActivity) context).showToast("Please select new size");
                return false;
            }
        }
        return true;
    }

    public ReturnExchangeRequest getReturnExchangeRequest() {
        return returnExchangeRequest;
    }

    @Override
    public void onClick(View v) {
        if (onReturnExchange != null)
            onReturnExchange.onClick(v.getTag());

    }

    public interface ReturnExchange {
        void onReturnExchangeClick(String action, int itemPosition);

        void onClick(Object tag);

        void validationSuccessful(boolean isSuccessful);
    }
}
