package outletwise.com.twentydresses.controller.adapter;

import android.content.Context;
import android.os.Build;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.squareup.picasso.Picasso;

import java.util.ArrayList;

import outletwise.com.twentydresses.R;
import outletwise.com.twentydresses.model.Rewards;
import outletwise.com.twentydresses.view.custom.ArcProgress;

/**
 * Created by User-PC on 04-05-2016.
 */
public class RewardAdapter {
    private final ArrayList<Rewards.DataEntity> items;
    private Context context;
    private LinearLayout linearLayout;

    public RewardAdapter(Context context, LinearLayout llRewardItems, ArrayList<Rewards.DataEntity> items) {
        this.context = context;
        this.linearLayout = llRewardItems;
        this.items = items;
        addItems();
    }

    private void addItems() {

        if (items.size() != 0) {
            for (int i = 0; i < items.size(); i++) {
                View rewardLayout = LayoutInflater.from(context).inflate(
                        R.layout.layout_reward_adapter, null);

                rewardLayout.findViewById(R.id.llReward).setVisibility(View.VISIBLE);
                //  rewardLayout.findViewById(R.id.tvNoInvites).setVisibility(View.GONE);

                anim(i, rewardLayout);
                linearLayout.addView(rewardLayout);
            }
        } else {
            View rewardLayout = LayoutInflater.from(context).inflate(
                    R.layout.layout_reward_adapter, null);

            rewardLayout.findViewById(R.id.llReward).setVisibility(View.GONE);
            // rewardLayout.findViewById(R.id.tvNoInvites).setVisibility(View.VISIBLE);
            linearLayout.addView(rewardLayout);
        }
    }


    private void anim(int position, View rewardLayout) {

        ((TextView) rewardLayout.findViewById(R.id.tvLevel)).setText(items.get(position).getTitle());

        if (items.get(position).getDescription() != null)
            ((TextView) rewardLayout.findViewById(R.id.tvDesc)).setText(items.get(position).getDescription());

        if (items.get(position).getText() != null)
            ((TextView) rewardLayout.findViewById(R.id.tvStatus)).setText(items.get(position).getText());

       /* List<Rewards.DataEntity.CategoriesEntity> categoriesList = items.get(position).getCategories();
        ArrayList<String> category_name = new ArrayList<>();

        String strCategoryName = "";

        for (int i = 0; i < categoriesList.size(); i++) {

            if (i != categoriesList.size() - 1)
                strCategoryName = strCategoryName + categoriesList.get(i).getCategory_name() + ", ";
            else
                strCategoryName = strCategoryName + categoriesList.get(i).getCategory_name();
            category_name.add(categoriesList.get(i).getCategory_name());
        }*/

        //  ((TextView) rewardLayout.findViewById(R.id.tvCategory)).setText(items.get(position).getText());

        Picasso.with(context).load(items.get(position).getImage()).into((ImageView) rewardLayout.findViewById(R.id.imgCategory));

        ArcProgress arcProgress = (ArcProgress) rewardLayout.findViewById(R.id.circular_progressbar);
        arcProgress.setProgress(items.get(position).getProgress_percent());
        arcProgress.setBottomText(items.get(position).getProgress() + "/" + items.get(position).getScore());
        arcProgress.setBottomTextSize(30);
        arcProgress.setTextSize(40);

        arcProgress.setSuffixTextPadding(0);

        arcProgress.setStrokeWidth(15);
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            arcProgress.setUnfinishedStrokeColor(context.getColor(R.color.md_grey_400));
            arcProgress.setFinishedStrokeColor(context.getColor(R.color.color_accent));
            arcProgress.setTextColor(context.getColor(R.color.color_accent));
        } else {
            arcProgress.setUnfinishedStrokeColor(context.getResources().getColor(R.color.md_grey_400));
            arcProgress.setFinishedStrokeColor(context.getResources().getColor(R.color.color_accent));
            arcProgress.setTextColor(context.getResources().getColor(R.color.color_accent));
        }

    /*    if (items.get(position).isAchieved())
            ((TextView) rewardLayout.findViewById(R.id.tvStatus)).setText("Achieved");
        else if (items.get(position).isClaimed())
            ((TextView) rewardLayout.findViewById(R.id.tvStatus)).setText("Claimed");
        else if (items.get(position).isExpired())
            ((TextView) rewardLayout.findViewById(R.id.tvStatus)).setText("Expired");*/


       /* final CircularProgressBar c3 = (CircularProgressBar) rewardLayout.findViewById(R.id.circularprogressbar);
       *//* c3.setTitle("2/5");
        c3.setSubTitle("2013");*//*
        c3.setProgress(progress);
        c3.animateProgressTo(0, progress, new CircularProgressBar.ProgressAnimationListener() {

            @Override
            public void onAnimationStart() {
            }

            @Override
            public void onAnimationProgress(int progress) {
                c3.setTitle(progress + "%");
            }

            @Override
            public void onAnimationFinish() {
                c3.setSubTitle("done");
            }
        });*/
    }

}
