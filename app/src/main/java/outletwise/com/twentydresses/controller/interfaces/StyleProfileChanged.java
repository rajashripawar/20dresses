package outletwise.com.twentydresses.controller.interfaces;

import outletwise.com.twentydresses.model.StyleAttrEntity;
import outletwise.com.twentydresses.model.UserAttrEntity;

/**
 * Created by User-PC on 14-12-2015.
 */
public interface StyleProfileChanged {
    void onChange(UserAttrEntity userAttrEntity);
    void onChangeStyle(StyleAttrEntity styleArr);
}
