package outletwise.com.twentydresses.controller.adapter;

import android.content.Context;
import android.graphics.Paint;
import android.support.v7.widget.RecyclerView;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.lsjwzh.widget.materialloadingprogressbar.CircleProgressBar;
import com.squareup.picasso.Picasso;

import org.solovyev.android.views.llm.LinearLayoutManager;

import java.util.ArrayList;
import java.util.HashMap;

import outletwise.com.twentydresses.R;
import outletwise.com.twentydresses.controller.SwipeDetector;
import outletwise.com.twentydresses.model.Product;
import outletwise.com.twentydresses.model.database.greenbot.Shortlist;
import outletwise.com.twentydresses.utilities.Constants;
import outletwise.com.twentydresses.view.custom.FavoritesButton;
import outletwise.com.twentydresses.view.custom.ImageDragShadowBuilder;

/**
 * Created by User-PC on 06-08-2015.
 */
public class ProductsAdapter extends BaseRecyclerAdapter{
    private int type;

    public static final int PRODUCT_VERTICAL = 0;
    public static final int PRODUCT_HORIZONTAL = 1;
    public static final int PRODUCT_HORIZONTAL_SMALL = 2; //For you may like adapter

    private final static int ITEM_VIEW_TYPE_HEADER = 100;
    private final static int ITEM_VIEW_TYPE_FOOTER = 99;
    private final static int ITEM_VIEW_TYPE_ITEM = 98;

    private boolean isForYou;
    private CircleProgressBar progressWheel;
    private LinearLayout llProgressFooter;
    private HashMap<String, Integer> productState = new HashMap<>();
    private ShortListed shortListed;
    private SubTipsAdapter subTipsAdapter;
    private TrendBannerAdapter trendBannerAdapter;
    private RecyclerView rvSubTips;
    ArrayList<Shortlist> shortlists;

    public ProductsAdapter(Context context, ArrayList<Product> items, int type,
                           ShortListed shortListed, boolean isForYou) {
        super(context, items);

        this.type = type;
        this.shortListed = shortListed;
        this.isForYou = isForYou;
    }


    public void setType(int type) {
        this.type = type;
    }


    class ViewHolder extends BaseViewHolder {

        public View mView;
        public ImageView ivProduct;
        public TextView tvProductDesc;
        public TextView tvProductPrice;
        public TextView tvProductMRP;
        public TextView tvDiscountPer;
        public FavoritesButton bFavorites;
        public TextView tvProductOutOfStock;

        public ViewHolder(View view) {
            super(view);
            mView = view;
            ivProduct = (ImageView) view.findViewById(R.id.ivProduct);
            tvProductPrice = (TextView) view.findViewById(R.id.tvProductPriceScroll);
            tvProductDesc = (TextView) view.findViewById(R.id.tvProductDesc);
            tvProductMRP = (TextView) view.findViewById(R.id.tvProductMRP);
            bFavorites = (FavoritesButton) view.findViewById(R.id.bFavorites);
            tvDiscountPer = (TextView) view.findViewById(R.id.tvDiscountPer);
            tvProductOutOfStock = (TextView) view.findViewById(R.id.tv_outof_stock);
        }

        @Override
        public void onClick(View v) {
            if (mItemClickListener != null)
                mItemClickListener.onItemClick(v, getAdapterPosition());
        }
    }

    @Override
    public BaseViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        final View view;
        if (viewType == ITEM_VIEW_TYPE_HEADER && type == PRODUCT_VERTICAL) {
            view = LayoutInflater.from(parent.getContext())
                    .inflate(R.layout.layout_adapter_products_header, parent, false);
            rvSubTips = (RecyclerView) view;
            ((RecyclerView) view).setLayoutManager(new LinearLayoutManager(view.getContext(),
                    LinearLayoutManager.HORIZONTAL, false));
            if (subTipsAdapter != null)
                rvSubTips.setAdapter(subTipsAdapter);
            else
                rvSubTips.setAdapter(trendBannerAdapter);

        } else if (viewType == ITEM_VIEW_TYPE_FOOTER && type != PRODUCT_HORIZONTAL_SMALL) {
            view = LayoutInflater.from(parent.getContext())
                    .inflate(R.layout.layout_adapter_product_footer, parent, false);
            progressWheel = (CircleProgressBar) view.findViewById(R.id.pbcircularWheel);
            llProgressFooter = (LinearLayout) view.findViewById(R.id.rlProgressFooter);

        } else {
            if (type == PRODUCT_VERTICAL || type == PRODUCT_HORIZONTAL_SMALL) {
                view = LayoutInflater.from(parent.getContext())
                        .inflate(R.layout.layout_adapter_product_grid, parent, false);
            } else {
                view = LayoutInflater.from(parent.getContext())
                        .inflate(R.layout.layout_adapter_product_hori, parent, false);
                new SwipeDetector(view).setOnSwipeListener(new SwipeDetector.onSwipeEvent() {
                    @Override
                    public void SwipeEventDetected(View v, SwipeDetector.SwipeTypeEnum swipeType) {
                        View.DragShadowBuilder shadowBuilder = ImageDragShadowBuilder.fromView(view);
                        if (swipeType == SwipeDetector.SwipeTypeEnum.TOP_TO_BOTTOM_LEFT ||
                                swipeType == SwipeDetector.SwipeTypeEnum.TOP_TO_BOTTOM_RIGHT ||
                                swipeType == SwipeDetector.SwipeTypeEnum.TOP_TO_BOTTOM)
                            view.startDrag(null, shadowBuilder, view, 0);
                        else if (swipeType == SwipeDetector.SwipeTypeEnum.TAP && mItemClickListener != null)
                            mItemClickListener.onItemClick(view, -1);
                    }
                });

            }
        }
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(BaseViewHolder holder, int position) {

        final ViewHolder viewHolder = (ViewHolder) holder;
        if (isFooter(position) && type != PRODUCT_HORIZONTAL_SMALL) {
            int gravity;
            LinearLayout.LayoutParams params;
            if (type == PRODUCT_VERTICAL) {
                params = new LinearLayout.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT,
                        context.getResources().getDimensionPixelSize(R.dimen.progress_product_adapter_vertical_height));
                gravity = Gravity.CENTER_HORIZONTAL;
            } else {
                params = new LinearLayout.LayoutParams(ViewGroup.LayoutParams.WRAP_CONTENT,
                        context.getResources().getDimensionPixelSize(R.dimen.progress_product_adapter_hori_height));
                gravity = Gravity.CENTER_VERTICAL;
            }
            llProgressFooter.setGravity(gravity);
            llProgressFooter.setLayoutParams(params);
        } else if (isHeader(position)) {
            Constants.debug("isHeader");

//            if (!isForYou)
//                ((ViewHolder) holder).mView.setVisibility(View.GONE);
        } else {
            Product tempProduct;
            if (isForYou && type == PRODUCT_VERTICAL)
                tempProduct = (Product) items.get(position - 1);
            else
                tempProduct = (Product) items.get(position);

            final Product product = tempProduct;
            viewHolder.mView.setTag(product);

            Picasso.with(context).load(product.getProduct_img()).into(viewHolder.ivProduct);

            if (product.getProduct_stock() != null && product.getProduct_stock().equalsIgnoreCase("0"))
                viewHolder.tvProductOutOfStock.setVisibility(View.VISIBLE);
            else
                viewHolder.tvProductOutOfStock.setVisibility(View.GONE);


            if (!product.getProduct_discount().equalsIgnoreCase("0")) {
                viewHolder.tvDiscountPer.setVisibility(View.VISIBLE);
                viewHolder.tvDiscountPer.setText(product.getProduct_discount() + "% Off");

                //    if (!(type == PRODUCT_HORIZONTAL_SMALL))
                {
                    viewHolder.tvProductMRP.setVisibility(View.VISIBLE);
                    viewHolder.tvProductMRP.setText(Constants.RUPEE + Constants.formatAmount(product.getProduct_price()));
                    viewHolder.tvProductMRP.setPaintFlags(viewHolder.tvProductMRP.getPaintFlags() | Paint.STRIKE_THRU_TEXT_FLAG);
                }

                viewHolder.tvProductPrice.setText(Constants.RUPEE +
                        Constants.getPriceWithDiscount(product.getProduct_price(), product.getProduct_discount()));
            } else {
                viewHolder.tvDiscountPer.setVisibility(View.GONE);
                viewHolder.tvProductPrice.setText(Constants.RUPEE + Constants.formatAmount(product.getProduct_price()));
                viewHolder.tvProductMRP.setVisibility(View.GONE);
            }

            viewHolder.tvProductDesc.setText(product.getProduct_name());

            if (shortListed == null && type == PRODUCT_HORIZONTAL_SMALL) {
                viewHolder.bFavorites.setVisibility(View.GONE);
            } else {
                int favorite_state = productState.get(product.getProduct_id()) == null ?
                        FavoritesButton.PROGRESS_IDLE : productState.get(product.getProduct_id());
                // Constants.debug("Changing state to: " + favorite_state);
                switch (favorite_state) {
                    case FavoritesButton.PROGRESS_IDLE:
                        viewHolder.bFavorites.stopProgress(false);
                        break;
                    case FavoritesButton.IN_PROGRESS:
                        viewHolder.bFavorites.startProgress();
                        break;
                    case FavoritesButton.PROGRESS_COMPLETE:
                        viewHolder.bFavorites.stopProgress(true);
                        break;
                }
                viewHolder.bFavorites.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        int favorite_state = productState.get(product.getProduct_id()) == null ?
                                FavoritesButton.PROGRESS_IDLE : productState.get(product.getProduct_id());
                        if (favorite_state == FavoritesButton.PROGRESS_COMPLETE) {
                            viewHolder.bFavorites.startProgress();
                            productState.put(product.getProduct_id(), FavoritesButton.IN_PROGRESS);
                            shortListed.removeProductFromShortList(product);
                        } else {
                            viewHolder.bFavorites.startProgress();
                            productState.put(product.getProduct_id(), FavoritesButton.IN_PROGRESS);
                            shortListed.addProductToShortList(product);
                        }
                    }
                });
            }
        }
    }

    @Override
    public int getItemViewType(int position) {
        if (isHeader(position))
            return ITEM_VIEW_TYPE_HEADER;
        else if (isFooter(position))
            return ITEM_VIEW_TYPE_FOOTER;
        else
            return ITEM_VIEW_TYPE_ITEM;
    }

    @Override
    public int getItemCount() {
        if (type == PRODUCT_HORIZONTAL_SMALL)
            return items.size();
        else
            return isForYou && type == PRODUCT_VERTICAL ? items.size() + 2 : items.size() + 1; //one for header & one for footer
    }

    public int getItemWeight(int position) {
        if (isFooter(position))
            return 2;
        else if (isHeader(position))
            return 2;
        else
            return 1;
    }


    public void hideProgressBar() {
        if (progressWheel != null)
            progressWheel.setVisibility(View.GONE);
    }

    public void showProgressBar() {
        if (progressWheel != null)
            progressWheel.setVisibility(View.VISIBLE);
    }

    public boolean isHeader(int position) {
        return isForYou && type == PRODUCT_VERTICAL && position == 0;
    }

    public boolean isFooter(int position) {
        return position == (isForYou && type == PRODUCT_VERTICAL ? items.size() + 1 : items.size());
    }

    public void setSubTips(ArrayList<String> subTips) {
        subTipsAdapter = new SubTipsAdapter(context, subTips);
        rvSubTips.setAdapter(subTipsAdapter);
    }

    public void setTrendBanners(ArrayList<String> banners) {
        LinearLayoutManager layoutManager = new LinearLayoutManager(rvSubTips.getContext());
        layoutManager.setOrientation(LinearLayoutManager.VERTICAL);
        rvSubTips.setLayoutManager(layoutManager);
        trendBannerAdapter = new TrendBannerAdapter(context, banners);
        rvSubTips.setAdapter(trendBannerAdapter);
    }

    public void changeProductState(String product_id, int progressState) {
        productState.put(product_id, progressState);
    }

    public interface ShortListed {
        void addProductToShortList(Product product);

        void removeProductFromShortList(Product product);
    }
}
