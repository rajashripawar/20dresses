package outletwise.com.twentydresses.controller.adapter;

import android.content.Context;
import android.graphics.Paint;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.squareup.picasso.Picasso;

import java.util.ArrayList;

import outletwise.com.twentydresses.R;
import outletwise.com.twentydresses.model.database.greenbot.Shortlist;
import outletwise.com.twentydresses.utilities.Constants;

/**
 * Created by User-PC on 08-08-2015.
 */
public class FavoritesRecyclerAdapter extends BaseRecyclerAdapter {
    private int type;

    public static final int PRODUCT_VERTICAL = 0;
    public static final int PRODUCT_HORIZONTAL = 1;

    private FavoriteAction favoriteAction;

    public FavoritesRecyclerAdapter(Context context, ArrayList<Shortlist> items, int type) {
        super(context, items);
        this.type = type;
    }

    public class ViewHolder extends BaseViewHolder {

        public View mView;
        public ImageView mImage;
        public ImageView mRemoveItem;
        public TextView tvProductMRP;
        public TextView tvProductPrice;
        public TextView tvProductDiscountPer;

        public ViewHolder(View view) {
            super(view);
            mView = view;
            mImage = (ImageView) view.findViewById(R.id.ivFavorites);
            mRemoveItem = (ImageView) view.findViewById(R.id.ivRemove);
            tvProductMRP = (TextView) view.findViewById(R.id.tvProductMRP);
            tvProductPrice = (TextView) view.findViewById(R.id.tvProductPriceScroll);
            tvProductDiscountPer = (TextView) view.findViewById(R.id.tvDiscountPer);
        }


        @Override
        public void onClick(View v) {
            if (mItemClickListener != null)
                mItemClickListener.onItemClick(v, getAdapterPosition());
        }
    }

    @Override
    public BaseViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view;
        if (type == PRODUCT_VERTICAL)
            view = LayoutInflater.from(parent.getContext())
                    .inflate(R.layout.layout_shortlisted_vertical, parent, false);
        else
            view = LayoutInflater.from(parent.getContext())
                    .inflate(R.layout.layout_shortlisted_horizontal, parent, false);

        mItemClickListener.onItemClick(view, 0);

        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(BaseViewHolder holder, int position) {
        ViewHolder viewHolder = (ViewHolder) holder;
        final Shortlist item = (Shortlist) items.get(position);
        Picasso.with(context).load(item.getProduct_img()).into(viewHolder.mImage);

        if (type == PRODUCT_VERTICAL) {
            viewHolder.tvProductPrice.setText(Constants.RUPEE + Constants.formatAmount(item.getProduct_price()));

            if (!item.getProduct_discount().equalsIgnoreCase("0")) {
                viewHolder.tvProductDiscountPer.setVisibility(View.VISIBLE);
                viewHolder.tvProductMRP.setVisibility(View.VISIBLE);

                viewHolder.tvProductPrice.setText(Constants.RUPEE +
                        Constants.getPriceWithDiscount(item.getProduct_price(), item.getProduct_discount()));

                viewHolder.tvProductMRP.setText(Constants.RUPEE + Constants.formatAmount(item.getProduct_price()));

                viewHolder.tvProductMRP.setPaintFlags(viewHolder.tvProductMRP.getPaintFlags() | Paint.STRIKE_THRU_TEXT_FLAG);
                viewHolder.tvProductDiscountPer.setText(item.getProduct_discount() + "% Off");
            }

            viewHolder.mRemoveItem.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if (favoriteAction != null)
                        favoriteAction.onItemRemoved(item);
                }
            });
        }

        Shortlist tempProduct = (Shortlist) items.get(position);
        Shortlist product = tempProduct;
        viewHolder.mView.setTag(product);
    }

    @Override
    public int getItemCount() {
        return items.size();
    }

    public void setFavoriteAction(FavoriteAction favoriteAction) {
        this.favoriteAction = favoriteAction;
    }

    public interface FavoriteAction {
        void onItemRemoved(Shortlist item);
    }
}

