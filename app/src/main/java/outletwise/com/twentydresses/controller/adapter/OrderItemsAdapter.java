package outletwise.com.twentydresses.controller.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;

import com.squareup.picasso.Picasso;

import java.util.List;

import outletwise.com.twentydresses.R;
import outletwise.com.twentydresses.model.Order;

/**
 * Created by User-PC on 09-10-2015.
 */
public class OrderItemsAdapter{
    private Context context;
    private List<Order.OrderItemsEntity> orderItems;
    private LinearLayout linearLayout;

    public OrderItemsAdapter(Context context, List<Order.OrderItemsEntity> orderItems) {
        this.context = context;
        this.orderItems = orderItems;
    }

    public void addItems(LinearLayout linearLayout) {
        this.linearLayout = linearLayout;
        removeViews();
        for (Order.OrderItemsEntity orderItemsEntity : orderItems) {
            View orderLayout = LayoutInflater.from(context).inflate(
                    R.layout.layout_adapter_order_item, null);

            ImageView imageView = (ImageView) orderLayout.findViewById(R.id.ivProductImage);
            if(!orderItemsEntity.getItem_image().isEmpty() && !orderItemsEntity.getItem_image().equalsIgnoreCase(""))
            Picasso.with(context).load(orderItemsEntity.getItem_image()).into(imageView);
            if(orderItemsEntity.isItem_retrun())
                orderLayout.findViewById(R.id.rlReturn).setVisibility(View.VISIBLE);
            else
                orderLayout.findViewById(R.id.rlReturn).setVisibility(View.GONE);

            orderLayout.setTag(orderItemsEntity);
            linearLayout.addView(orderLayout);

        }
    }

    private void removeViews() {
        if(linearLayout != null)
        linearLayout.removeAllViews();
    }

    public boolean isEmpty() {
        return linearLayout == null || linearLayout.getChildCount() == 0;
    }

    public boolean isListSizeChanged(int listSize) {
        return listSize != orderItems.size();
    }

    public void notifyDataSetChanged() {
        removeViews();
        addItems(linearLayout);
    }

}
