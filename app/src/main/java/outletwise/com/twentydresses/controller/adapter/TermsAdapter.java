package outletwise.com.twentydresses.controller.adapter;

import android.app.Activity;
import android.content.Context;
import android.text.Html;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.TextView;

import java.util.ArrayList;

import outletwise.com.twentydresses.R;

public class TermsAdapter {

    private final Context context;
    private ArrayList<String> keys, values;
    private final LinearLayout linearLayout;


    public TermsAdapter(Context context, ArrayList<String> keys, ArrayList<String> values, LinearLayout llTerms) {

        this.context = context;
        this.keys = keys;
        this.values = values;
        this.linearLayout = llTerms;
        addViews();
    }

    private void addViews() {

        for (int i = 0; i < keys.size(); i++) {
            View rootView = ((Activity) context).getLayoutInflater().inflate(R.layout.layout_adapter_terms, null);
            TextView tvKey = (TextView) rootView.findViewById(R.id.tvKey);
            tvKey.setText(keys.get(i));

            TextView tvValue = (TextView) rootView.findViewById(R.id.tvValue);
            tvValue.setText(Html.fromHtml(values.get(i)));

            linearLayout.addView(rootView);
        }


    }
}
