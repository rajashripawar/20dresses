package outletwise.com.twentydresses.controller.adapter;

import android.app.Activity;
import android.content.Context;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.TextView;

import java.util.ArrayList;

import outletwise.com.twentydresses.R;
import outletwise.com.twentydresses.model.Product;

/**
 * Created by User-PC on 27/07/2015.
 */
public class SizesAdapter implements View.OnClickListener {

    private Context context;
    private ArrayList<Product.ProductSizesEntity> sizes;
    private LinearLayout linearLayout;
    private View previousView;
    private SizeSelected sizeSelected;
    private Boolean blnOutOfStock;
    ArrayList<TextView> tvSizeList = new ArrayList<>();

    public SizesAdapter(Context context, ArrayList<Product.ProductSizesEntity> sizes, LinearLayout linearLayout, boolean bOutOfStock) {
        this.context = context;
        this.sizes = sizes;
        this.linearLayout = linearLayout;
        blnOutOfStock = bOutOfStock;
        addViews();
    }

    private void addViews() {
        if ((linearLayout).getChildCount() > 0)
            (linearLayout).removeAllViews();

        for (Product.ProductSizesEntity size : sizes) {
            {
                TextView tv = (TextView) ((Activity) context).getLayoutInflater().inflate(R.layout.layout_adapter_sizes, null);
                tv.setTag(size);
                tv.setText(size.getAttr_value());

                int quantity = Integer.parseInt(size.getQuantity());
                if (!blnOutOfStock && quantity <= 0) {
                    // tv.setBackgroundColor(Color.GRAY);
                    tv.setBackgroundResource(R.drawable.background_sizes_disabled);
                    tv.setEnabled(false);
                    tv.setClickable(false);
                }
                LinearLayout.LayoutParams layoutParams = new LinearLayout.LayoutParams(
                        context.getResources().getDimensionPixelSize(R.dimen.text_sizes_width),
                        context.getResources().getDimensionPixelSize(R.dimen.text_sizes_height));
                layoutParams.setMargins(4, 4, 4, 4);
                tv.setLayoutParams(layoutParams);
                tv.setOnClickListener(this);
                linearLayout.addView(tv);
                tvSizeList.add(tv);
            }
        }
    }

    @Override
    public void onClick(View v) {
        if (previousView != null)
            previousView.setSelected(false);
        v.setSelected(true);
        previousView = v;
        if (sizeSelected != null)
            sizeSelected.onSizeSelected((Product.ProductSizesEntity) v.getTag());
    }

    public void setSizeSelected(SizeSelected sizeSelected) {
        this.sizeSelected = sizeSelected;
    }

    public interface SizeSelected {
        void onSizeSelected(Product.ProductSizesEntity size);
    }

    public void setSelectedView(int position) {
        tvSizeList.get(position).setSelected(true);
    }
}
