package outletwise.com.twentydresses.model;

import com.google.gson.annotations.SerializedName;

/**
 * Created by webwerks on 11/24/16.
 */
public class SingleProductModel {
    public Product getProduct() {
        return product;
    }

    public void setProduct(Product product) {
        this.product = product;
    }

    @SerializedName("data")
    public Product product;

}
