package outletwise.com.twentydresses.model;

import java.util.List;

/**
 * Created by webwerks on 1/8/16.
 */

public class ForyouQuizResponse {


    /**
     * status : Success
     * message :
     * question : Select the face shape closest to yours
     * answer_type : Image
     * answers : [{"image":"http://www.20dresses.com/uploads/quiz_image/OBLONGshape.jpg","image_description":"Oblong","text":""},{"image":"http://www.20dresses.com/uploads/quiz_image/OVALshape.jpg","image_description":"Oval","text":""},{"image":"http://www.20dresses.com/uploads/quiz_image/SQUAREfaceshape.jpg","image_description":"Square","text":""}]
     */

    private String status;
    private String message;
    private String question;
    private String answer_type;
    /**
     * image : http://www.20dresses.com/uploads/quiz_image/OBLONGshape.jpg
     * image_description : Oblong
     * text :
     */

    private List<AnswersBean> answers;

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public String getQuestion() {
        return question;
    }

    public void setQuestion(String question) {
        this.question = question;
    }

    public String getAnswer_type() {
        return answer_type;
    }

    public void setAnswer_type(String answer_type) {
        this.answer_type = answer_type;
    }

    public List<AnswersBean> getAnswers() {
        return answers;
    }

    public void setAnswers(List<AnswersBean> answers) {
        this.answers = answers;
    }

    public static class AnswersBean {
        private String image;
        private String image_description;
        private String text;

        public String getImage() {
            return image;
        }

        public void setImage(String image) {
            this.image = image;
        }

        public String getImage_description() {
            return image_description;
        }

        public void setImage_description(String image_description) {
            this.image_description = image_description;
        }

        public String getText() {
            return text;
        }

        public void setText(String text) {
            this.text = text;
        }
    }
}
