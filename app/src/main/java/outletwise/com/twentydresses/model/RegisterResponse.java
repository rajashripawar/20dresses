package outletwise.com.twentydresses.model;

/**
 * Created by User-PC on 04-12-2015.
 */
public class RegisterResponse {

    /**
     * user : 390612
     * token : 5989655595661517456b0b8.
     * status : Success
     */

    private Long user;
    private String token;
    private String status;
    private String message;
    private String email_address;

    public void setToken(String token) {
        this.token = token;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public Long getUser() {
        return user;
    }

    public void setUser(Long user) {
        this.user = user;
    }

    public String getToken() {
        return token;
    }

    public String getStatus() {
        return status;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public String getEmail_address() {
        return email_address;
    }

    public void setEmail_address(String email_address) {
        this.email_address = email_address;
    }
}
