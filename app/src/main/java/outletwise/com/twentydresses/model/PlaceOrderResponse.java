package outletwise.com.twentydresses.model;

import java.util.List;

/**
 * Created by User-PC on 07-11-2015.
 */
public class PlaceOrderResponse {

    /**
     * order_id : 39733
     * order : {"user_id":"187523","user_info_id":"187523","order_total":2919,"order_subtotal":3985,"credit_points":0,"order_wallet":30,"order_discount":1036,"order_bag_discount":239,"order_status":"OH","ip_address":"115.97.23.177","cdate":"2015-11-06 05:22:14","user_ship_id":"7414"}
     * order_items : [{"cart_item_id":"192042","product_id":"6072","product_name":"Pink Femme Fatale Backpack","product_category":"30","attr_id":"65","quantity":"0","attr_value":"Default Size","product_discount":"0","product_mrp":"1495","product_img":"http://www.20dresses.com/uploads/product/temp_img/thumb_Pink-Femme-Fatale-Backpack-1.JPG","stock":"12","product_price":1107,"product_savings":388},{"cart_item_id":"192043","product_id":"6074","product_name":"Brown Mini Me Essential Backpack","product_category":"30","attr_id":"65","quantity":"0","attr_value":"Default Size","product_discount":"0","product_mrp":"1195","product_img":"http://www.20dresses.com/uploads/product/temp_img/thumb_Brown-Mini-Me-Essential-Backpack-1.jpg","stock":"7","product_price":885,"product_savings":310},{"cart_item_id":"192041","product_id":"6208","product_name":"Monochrome Funky Town Backpack","product_category":"30","attr_id":"65","quantity":"0","attr_value":"Default Size","product_discount":"0","product_mrp":"1295","product_img":"http://www.20dresses.com/uploads/product/temp_img/thumb_Monochrome-Funky-Town-Backpack-1.JPG","stock":"5","product_price":959,"product_savings":336}]
     * mobile_verified : true
     */
    private String status;
    private String message;
    private int order_id;
    /**
     * user_id : 187523
     * user_info_id : 187523
     * order_total : 2919
     * order_subtotal : 3985
     * credit_points : 0
     * order_wallet : 30
     * order_discount : 1036
     * order_bag_discount : 239
     * order_status : OH
     * ip_address : 115.97.23.177
     * cdate : 2015-11-06 05:22:14
     * user_ship_id : 7414
     */

    private OrderEntity order;
    private boolean mobile_verified;
    /**
     * cart_item_id : 192042
     * product_id : 6072
     * product_name : Pink Femme Fatale Backpack
     * product_category : 30
     * attr_id : 65
     * quantity : 0
     * attr_value : Default Size
     * product_discount : 0
     * product_mrp : 1495
     * product_img : http://www.20dresses.com/uploads/product/temp_img/thumb_Pink-Femme-Fatale-Backpack-1.JPG
     * stock : 12
     * product_price : 1107
     * product_savings : 388
     */

    private List<OrderItemsEntity> order_items;

    public void setOrder_id(int order_id) {
        this.order_id = order_id;
    }

    public void setOrder(OrderEntity order) {
        this.order = order;
    }

    public void setMobile_verified(boolean mobile_verified) {
        this.mobile_verified = mobile_verified;
    }

    public void setOrder_items(List<OrderItemsEntity> order_items) {
        this.order_items = order_items;
    }

    public int getOrder_id() {
        return order_id;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public OrderEntity getOrder() {
        return order;
    }

    public boolean isMobile_verified() {
        return mobile_verified;
    }

    public List<OrderItemsEntity> getOrder_items() {
        return order_items;
    }

    public static class OrderEntity {
        private String user_id;
        private String user_info_id;
        private int order_total;
        private int order_subtotal;
        private int credit_points;
        private int order_wallet;
        private int order_discount;
        private int order_bag_discount;
        private String order_status;
        private String ip_address;
        private String cdate;
        private String user_ship_id;

        public void setUser_id(String user_id) {
            this.user_id = user_id;
        }

        public void setUser_info_id(String user_info_id) {
            this.user_info_id = user_info_id;
        }

        public void setOrder_total(int order_total) {
            this.order_total = order_total;
        }

        public void setOrder_subtotal(int order_subtotal) {
            this.order_subtotal = order_subtotal;
        }

        public void setCredit_points(int credit_points) {
            this.credit_points = credit_points;
        }

        public void setOrder_wallet(int order_wallet) {
            this.order_wallet = order_wallet;
        }

        public void setOrder_discount(int order_discount) {
            this.order_discount = order_discount;
        }

        public void setOrder_bag_discount(int order_bag_discount) {
            this.order_bag_discount = order_bag_discount;
        }

        public void setOrder_status(String order_status) {
            this.order_status = order_status;
        }

        public void setIp_address(String ip_address) {
            this.ip_address = ip_address;
        }

        public void setCdate(String cdate) {
            this.cdate = cdate;
        }

        public void setUser_ship_id(String user_ship_id) {
            this.user_ship_id = user_ship_id;
        }

        public String getUser_id() {
            return user_id;
        }

        public String getUser_info_id() {
            return user_info_id;
        }

        public int getOrder_total() {
            return order_total;
        }

        public int getOrder_subtotal() {
            return order_subtotal;
        }

        public int getCredit_points() {
            return credit_points;
        }

        public int getOrder_wallet() {
            return order_wallet;
        }

        public int getOrder_discount() {
            return order_discount;
        }

        public int getOrder_bag_discount() {
            return order_bag_discount;
        }

        public String getOrder_status() {
            return order_status;
        }

        public String getIp_address() {
            return ip_address;
        }

        public String getCdate() {
            return cdate;
        }

        public String getUser_ship_id() {
            return user_ship_id;
        }
    }

    public static class OrderItemsEntity {
        private String cart_item_id;
        private String product_id;
        private String product_name;
        private String product_category;
        private String product_category_name;
        private String product_sku;
        private String attr_id;
        private String quantity;
        private String attr_value;
        private String product_discount;
        private String product_mrp;
        private String product_img;
        private String stock;
        private int product_price;
        private int product_savings;

        public void setCart_item_id(String cart_item_id) {
            this.cart_item_id = cart_item_id;
        }

        public void setProduct_id(String product_id) {
            this.product_id = product_id;
        }

        public void setProduct_name(String product_name) {
            this.product_name = product_name;
        }

        public void setProduct_category(String product_category) {
            this.product_category = product_category;
        }

        public void setProduct_category_name(String product_category_name) {
            this.product_category_name = product_category_name;
        }

        public void setProduct_sku(String product_sku) {
            this.product_sku = product_sku;
        }

        public void setAttr_id(String attr_id) {
            this.attr_id = attr_id;
        }

        public void setQuantity(String quantity) {
            this.quantity = quantity;
        }

        public void setAttr_value(String attr_value) {
            this.attr_value = attr_value;
        }

        public void setProduct_discount(String product_discount) {
            this.product_discount = product_discount;
        }

        public void setProduct_mrp(String product_mrp) {
            this.product_mrp = product_mrp;
        }

        public void setProduct_img(String product_img) {
            this.product_img = product_img;
        }

        public void setStock(String stock) {
            this.stock = stock;
        }

        public void setProduct_price(int product_price) {
            this.product_price = product_price;
        }

        public void setProduct_savings(int product_savings) {
            this.product_savings = product_savings;
        }

        public String getCart_item_id() {
            return cart_item_id;
        }

        public String getProduct_id() {
            return product_id;
        }

        public String getProduct_name() {
            return product_name;
        }

        public String getProduct_category() {
            return product_category;
        }

        public String getProduct_category_name() {
            return product_category_name;
        }

        public String getProduct_sku() {
            return product_sku;
        }

        public String getAttr_id() {
            return attr_id;
        }

        public String getQuantity() {
            return quantity;
        }

        public String getAttr_value() {
            return attr_value;
        }

        public String getProduct_discount() {
            return product_discount;
        }

        public String getProduct_mrp() {
            return product_mrp;
        }

        public String getProduct_img() {
            return product_img;
        }

        public String getStock() {
            return stock;
        }

        public int getProduct_price() {
            return product_price;
        }

        public int getProduct_savings() {
            return product_savings;
        }
    }
}
