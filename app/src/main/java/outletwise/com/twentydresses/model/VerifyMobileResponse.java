package outletwise.com.twentydresses.model;

/**
 * Created by User-PC on 20-08-2015.
 */
public class VerifyMobileResponse {


    /**
     * status : Success
     * message : Succesfully Verified
     */

    private String status;
    private String message;

    public void setStatus(String status) {
        this.status = status;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public String getStatus() {
        return status;
    }

    public String getMessage() {
        return message;
    }
}
