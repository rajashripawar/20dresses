package outletwise.com.twentydresses.model;

/**
 * Created by User-PC on 28-08-2015.
 */
public class Quiz {

    /**
     * Shape : O
     * Size : S (34-35)
     * Bottoms_Size : 38
     * Height : 5.4
     * Shoes_Size : 39
     * Face_Color : Tan
     */
    private static Quiz quiz;
    private UserAttrEntity user_attr;
    /**
     * maybe : 2,4
     * like : 3,5,6
     */

    private Quiz() {
    }

    public static Quiz getInstance() {
        if (quiz == null)
            quiz = new Quiz();
        return quiz;
    }


    private StyleAttrEntity style_attr;

    public void setUser_attr(UserAttrEntity user_attr) {
        this.user_attr = user_attr;
    }


    public UserAttrEntity getUser_attr() {
        return user_attr;
    }

    public StyleAttrEntity getStyle_attr() {
        return style_attr;
    }

    public void setStyle_attr(StyleAttrEntity style_attr) {
        this.style_attr = style_attr;
    }

}
