package outletwise.com.twentydresses.model;

import com.google.gson.annotations.SerializedName;

import java.util.List;

/**
 * Created by User-PC on 25-11-2015.
 */
public class SubTip extends GenericResponse{

    public String getCount() {
        return count;
    }

    public void setCount(String count) {
        this.count = count;
    }

    /**
     * status : Success
     * products : [{"product_id":"6231","product_name":"The Road Trip Life Tee","product_discount":"0","product_price":"595","product_img":"http://www.20dresses.com/uploads/product/temp_img/thumb_The-Road-Trip-Life-Top-1.JPG"},{"product_id":"6230","product_name":"For The Doughnut Lovers Tee","product_discount":"0","product_price":"595","product_img":"http://www.20dresses.com/uploads/product/temp_img/thumb_For-The-Doughnut-Lovers-Tee-1.JPG"},{"product_id":"6229","product_name":"Love Struck Arrow Tee","product_discount":"0","product_price":"595","product_img":"http://www.20dresses.com/uploads/product/temp_img/thumb_Love-Struck-Arrow-Top-1.JPG"},{"product_id":"6228","product_name":"Blurry Blues Aztec Crop Top","product_discount":"0","product_price":"795","product_img":"http://www.20dresses.com/uploads/product/temp_img/thumb_Blurry-Blues-Aztec-Crop-Top-1.JPG"},{"product_id":"6227","product_name":"Run With The Stripes Tee","product_discount":"0","product_price":"895","product_img":"http://www.20dresses.com/uploads/product/temp_img/thumb_Run-With-The-Stripes-Top-1.JPG"},{"product_id":"6226","product_name":"The Classic Camouflage Printed Top","product_discount":"0","product_price":"895","product_img":"http://www.20dresses.com/uploads/product/temp_img/thumb_The-Classic-Camouflage-Printed-Top-1.JPG"},{"product_id":"6225","product_name":"Deep In The Sky Maxi Dress","product_discount":"0","product_price":"1495","product_img":"http://www.20dresses.com/uploads/product/temp_img/thumb_Deep-In-The-Sky-Maxi-Dress-1.jpg"},{"product_id":"6224","product_name":"Buttoned Down Tube Dress","product_discount":"0","product_price":"1295","product_img":"http://www.20dresses.com/uploads/product/temp_img/thumb_Buttoned-Down-Tube-Dress-1.jpg"},{"product_id":"6223","product_name":"Florals In Fall Belted Dress","product_discount":"0","product_price":"1295","product_img":"http://www.20dresses.com/uploads/product/temp_img/thumb_Florals-In-Fall-Belted-Dress-1.JPG"},{"product_id":"6222","product_name":"A Royal Affair Skater Dress","product_discount":"0","product_price":"1495","product_img":"http://www.20dresses.com/uploads/product/temp_img/thumb_A-Royal-Affair-Skater-Dress-1.JPG"}]
     * extra_tips : ["This is a test tip","Final tips will come from database","Will be added by admin"]
     */
   @SerializedName("count")
   private  String count;

    @SerializedName("data")
    private List<Product> products;

    private List<String> extra_tips;


    public void setProducts(List<Product> products) {
        this.products = products;
    }

    public void setExtra_tips(List<String> extra_tips) {
        this.extra_tips = extra_tips;
    }


    public List<Product> getProducts() {
        return products;
    }

    public List<String> getExtra_tips() {
        return extra_tips;
    }
}
