package outletwise.com.twentydresses.model;

/**
 * Created by User-PC on 28-01-2016.
 */
public class UpdateStyleProfile {

    private UserAttrEntity user_attr;
    private String user;
    private StyleAttrEntity style_attr;

    private String auth_token;

    public void setUser_attr(UserAttrEntity user_attr) {
        this.user_attr = user_attr;
    }

    public void setUserId(String userId) {
        this.user = userId;
    }


    public void setStyle_attr(StyleAttrEntity style_attr) {
        this.style_attr = style_attr;
    }

    public void setAuth_token(String auth_token) {
        this.auth_token = auth_token;
    }
}
