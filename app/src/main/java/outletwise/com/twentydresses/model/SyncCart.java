package outletwise.com.twentydresses.model;

import java.util.List;

import outletwise.com.twentydresses.model.database.greenbot.Cart;

/**
 * Created by User-PC on 20-10-2015.
 */
public class SyncCart {

   /* *
     * wallet_amount : 30
     * cart_message : Add 1 more Item to get 10% Discount
     * cart_items : [{"attr_id":"65","stock":"0","product_category":"15","product_id":"4053","product_price":"345","product_savings":0,"product_mrp":"345","product_name":"Red Put A Bow On It Belt","product_img":"http://www.20dresses.com/uploads/product/temp_img/thumb_Red-Put-A-Bow-On-It-Belt-1.jpg","quantity":"0","attr_value":"Default Size","product_discount":"0"},{"attr_id":"65","stock":"23","product_category":"10","product_id":"4846","product_price":207,"product_savings":138,"product_mrp":"345","product_name":"Pearls In The Star Necklace","product_img":"http://www.20dresses.com/uploads/product/temp_img/thumb_Pearls-In-The-Star-Necklace-1.JPG","quantity":"0","attr_value":"Default Size","product_discount":"0"},{"attr_id":"204","stock":"3","product_category":"39","product_id":"6156","product_price":"1195","product_savings":0,"product_mrp":"1195","product_name":"Aztec Funk Espadrilles","product_img":"http://www.20dresses.com/uploads/product/temp_img/thumb_Aztec-Funk-Espadrilles-1.jpg","quantity":"0","attr_value":"37","product_discount":"0"},{"attr_id":"2","stock":"16","product_category":"4","product_id":"6190","product_price":"895","product_savings":0,"product_mrp":"895","product_name":"The Perfect Plaid Top ","product_img":"http://www.20dresses.com/uploads/product/temp_img/thumb_The-Perfect-Plaid-Top-1.JPG","quantity":"1","attr_value":"S","product_discount":"0"},{"attr_id":"2","stock":"18","product_category":"4","product_id":"6191","product_price":"895","product_savings":0,"product_mrp":"895","product_name":"Plaids In Play Top ","product_img":"http://www.20dresses.com/uploads/product/temp_img/thumb_Plaids-In-Play-Top-1.JPG","quantity":"1","attr_value":"S","product_discount":"0"}]
     * credits_available : 0
     * cart_values : {"bag_savings":239,"shipping_charges":0,"total_saving":377,"deals_savings":138,"offer_savings":0,"wallet_applied":0,"credits_applied":0,"sub_total":3330,"bag_discount":7.5,"price_total":3192,"grand_total":2953}
     * credit_applicable : false
     * offer_applicable : true
     * cart_offers : [{"categories":"39","discount":"20","offer":"FLAT 20% OFF ON SNEAKERS AND LOAFERS"}]*/



    private String wallet_amount;
    private String cart_message;
    private String credits_available;
    private String redeemable_credits;
  /*  *
     * bag_savings : 239
     * shipping_charges : 0
     * total_saving : 377
     * deals_savings : 138
     * offer_savings : 0
     * wallet_applied : 0
     * credits_applied : 0
     * sub_total : 3330
     * bag_discount : 7.5
     * price_total : 3192
     * grand_total : 2953*/


    private CartValuesEntity cart_values;
    private boolean credit_applicable;
    private boolean offer_applicable;
   /* *
     * attr_id : 65
     * stock : 0
     * product_category : 15
     * product_id : 4053
     * product_price : 345
     * product_savings : 0
     * product_mrp : 345
     * product_name : Red Put A Bow On It Belt
     * product_img : http://www.20dresses.com/uploads/product/temp_img/thumb_Red-Put-A-Bow-On-It-Belt-1.jpg
     * quantity : 0
     * attr_value : Default Size
     * product_discount : 0*/


    private List<Cart> cart_items;
  /*  *
     * categories : 39
     * discount : 20
     * offer : FLAT 20% OFF ON SNEAKERS AND LOAFERS*/


    private List<CartOffersEntity> cart_offers;
   /* *
     * message : cart empty
     * status : Failed*/


    private String message;
    private String status;
   /* *
     * offer_text : <ul><li>*(Maximum allowable credits: 50% of Bag Value)</li><li>*(Credits are only appicable on non-discounted products)</li></ul>

*/
    private String offer_text;
   /* *
     * credit_text : <ul><li>* Offer not applicable on products on SALE</li><li>* Bag Discount not applicable on offer</li></ul>
*/

    private String credit_text;


    public void setWallet_amount(String wallet_amount) {
        this.wallet_amount = wallet_amount;
    }

    public void setCart_message(String cart_message) {
        this.cart_message = cart_message;
    }

    public void setCredits_available(String credits_available) {
        this.credits_available = credits_available;
    }

    public void setRedeemable_credits(String redeemable_credits) {
        this.redeemable_credits = redeemable_credits;
    }


    public void setCart_values(CartValuesEntity cart_values) {
        this.cart_values = cart_values;
    }

    public void setCredit_applicable(boolean credit_applicable) {
        this.credit_applicable = credit_applicable;
    }

    public void setOffer_applicable(boolean offer_applicable) {
        this.offer_applicable = offer_applicable;
    }

    public void setCart_items(List<Cart> cart_items) {
        this.cart_items = cart_items;
    }

    public void setCart_offers(List<CartOffersEntity> cart_offers) {
        this.cart_offers = cart_offers;
    }

    public String getWallet_amount() {
        return wallet_amount;
    }

    public String getCart_message() {
        return cart_message;
    }

    public String getCredits_available() {
        return credits_available;
    }

    public String getRedeemable_credits() {
        return redeemable_credits;
    }

    public CartValuesEntity getCart_values() {
        return cart_values;
    }

    public boolean isCredit_applicable() {
        return credit_applicable;
    }

    public boolean isOffer_applicable() {
        return offer_applicable;
    }

    public List<Cart> getCart_items() {
        return cart_items;
    }

    public List<CartOffersEntity> getCart_offers() {
        return cart_offers;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getMessage() {
        return message;
    }

    public String getStatus() {
        return status;
    }

    public void setOffer_text(String offer_text) {
        this.offer_text = offer_text;
    }

    public String getOffer_text() {
        return offer_text;
    }

    public void setCredit_text(String credit_text) {
        this.credit_text = credit_text;
    }

    public String getCredit_text() {
        return credit_text;
    }

    public static class CartValuesEntity {
        private double bag_savings;
        private double shipping_charges;
        private double total_saving;
        private double deals_savings;
        private double offer_savings;
        private double wallet_applied;
        private double credits_applied;
        private double sub_total;
        private double bag_discount;
        private double price_total;
        private double grand_total;
        private double product_total;
        private double order_total;
        private double credits_savings;
        private List<CartDiscountEntity> cart_discount;

        public void setCredits_savings(double credits_savings) {
            this.credits_savings = credits_savings;
        }

        public void setProduct_total(double product_total) {
            this.product_total = product_total;
        }

        public void setOrder_total(double order_total) {
            this.order_total = order_total;
        }

        public void setBag_savings(double bag_savings) {
            this.bag_savings = bag_savings;
        }

        public void setShipping_charges(double shipping_charges) {
            this.shipping_charges = shipping_charges;
        }

        public void setTotal_saving(double total_saving) {
            this.total_saving = total_saving;
        }

        public void setDeals_savings(double deals_savings) {
            this.deals_savings = deals_savings;
        }

        public void setOffer_savings(double offer_savings) {
            this.offer_savings = offer_savings;
        }

        public void setWallet_applied(double wallet_applied) {
            this.wallet_applied = wallet_applied;
        }

        public void setCredits_applied(double credits_applied) {
            this.credits_applied = credits_applied;
        }

        public void setSub_total(double sub_total) {
            this.sub_total = sub_total;
        }

        public void setBag_discount(double bag_discount) {
            this.bag_discount = bag_discount;
        }

        public void setPrice_total(double price_total) {
            this.price_total = price_total;
        }

        public void setGrand_total(double grand_total) {
            this.grand_total = grand_total;
        }

        public double getBag_savings() {
            return bag_savings;
        }

        public double getShipping_charges() {
            return shipping_charges;
        }

        public double getTotal_saving() {
            return total_saving;
        }

        public double getDeals_savings() {
            return deals_savings;
        }

        public double getOffer_savings() {
            return offer_savings;
        }

        public double getWallet_applied() {
            return wallet_applied;
        }

        public double getCredits_applied() {
            return credits_applied;
        }

        public double getSub_total() {
            return sub_total;
        }

        public double getBag_discount() {
            return bag_discount;
        }

        public double getPrice_total() {
            return price_total;
        }

        public double getGrand_total() {
            return grand_total;
        }

        public double getProduct_total() {
            return product_total;
        }

        public double getOrder_total() {
            return order_total;
        }

        public double getCredits_savings() {
            return credits_savings;
        }


          public List<CartDiscountEntity> getCart_discount() {
            return cart_discount;
        }

        public void setCart_discount(List<CartDiscountEntity> cart_discount) {
            this.cart_discount = cart_discount;
        }

        public static class CartDiscountEntity {
            private String discount_name;
            private int discount_value;

            public String getDiscount_name() {
                return discount_name;
            }

            public void setDiscount_name(String discount_name) {
                this.discount_name = discount_name;
            }

            public int getDiscount_value() {
                return discount_value;
            }

            public void setDiscount_value(int discount_value) {
                this.discount_value = discount_value;
            }
        }
    }

    public static class CartOffersEntity {
        private String categories;
        private String discount;
        private String offer;

        public void setCategories(String categories) {
            this.categories = categories;
        }

        public void setDiscount(String discount) {
            this.discount = discount;
        }

        public void setOffer(String offer) {
            this.offer = offer;
        }

        public String getCategories() {
            return categories;
        }

        public String getDiscount() {
            return discount;
        }

        public String getOffer() {
            return offer;
        }
    }
}
