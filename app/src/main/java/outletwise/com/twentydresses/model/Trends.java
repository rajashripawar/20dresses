package outletwise.com.twentydresses.model;

import com.google.gson.annotations.SerializedName;

import java.util.List;

/**
 * Created by User-PC on 11-05-2016.
 */
public class Trends {


    /**
     * status : Success
     * data : [{"trend_id":"24","trend_caption":"Beach Babes and Bash","trend_banner":"http://www.20dresses.com/uploads/trends/BEACHBABESnBASHtrendbnr.jpg","total":"44","thumbnails":["http://www.20dresses.com/uploads/product/temp_img/thumb_Blue-Sea-Tales-Top-1.JPG","http://www.20dresses.com/uploads/product/temp_img/thumb_Moonlit-Tales-Top-1.JPG","http://www.20dresses.com/uploads/product/temp_img/thumb_Simplicity-Body-Chain-1.JPG"]},{"trend_id":"25","trend_caption":"Date Night Dressing","trend_banner":"http://www.20dresses.com/uploads/trends/DATENIGHTDRESSINGtrendbnr.jpg","total":"39","thumbnails":["http://www.20dresses.com/uploads/product/temp_img/thumb_A-Vintage-Overcast-Dress-1.JPG","http://www.20dresses.com/uploads/product/temp_img/thumb_Green-Cut-To-The-Chase-Top-1.JPG","http://www.20dresses.com/uploads/product/temp_img/thumb_Pink-Rosy-Lace-Bellies-1.JPG","http://www.20dresses.com/uploads/product/temp_img/thumb_Pretty-In-Bling-Hairband-1.JPG"]},{"trend_id":"26","trend_caption":"Do The Polka","trend_banner":"http://www.20dresses.com/uploads/trends/DOTHEPOLKAtrendbnr.jpg","total":"20","thumbnails":["http://www.20dresses.com/uploads/product/temp_img/thumb_The-Classic-Polka-Top-1.JPG","http://www.20dresses.com/uploads/product/temp_img/thumb_Betty-White-Polka-Scarf-1.JPG","http://www.20dresses.com/uploads/product/temp_img/thumb_Pink-Fifties-Girl-Hairband-1.JPG","http://www.20dresses.com/uploads/product/temp_img/thumb_Polka-And-Ruffles-Top-1.JPG"]},{"trend_id":"27","trend_caption":"Go Rock Chic Or Go Home","trend_banner":"http://www.20dresses.com/uploads/trends/GOROCKchictrendbnr.jpg","total":"35","thumbnails":["http://www.20dresses.com/uploads/product/temp_img/thumb_Dance-The-Night-Away-Peplum-Dress-1.JPG","http://www.20dresses.com/uploads/product/temp_img/thumb_My-Inner-Diva-Leather-Trim-Dress-1.JPG","http://www.20dresses.com/uploads/product/temp_img/thumb_Little-Punk-Dungaree-1.JPG"]},{"trend_id":"28","trend_caption":"Valentine Love Shop","trend_banner":"http://www.20dresses.com/uploads/trends/VALENTINEcatbanner.jpg","total":"93","thumbnails":["http://www.20dresses.com/uploads/product/temp_img/thumb_Little-Hearts-In-Pink-Printed-Scarf-1.JPG","http://www.20dresses.com/uploads/product/temp_img/thumb_Red-Voodoo-Va-Va-Voom-Envelope-Clutch-1.JPG","http://www.20dresses.com/uploads/product/temp_img/thumb_Wings-Of-My-Heart-Necklace-1.jpg","http://www.20dresses.com/uploads/product/temp_img/thumb_Maroon-Affair-Of-The-Heart-Sneakers-1.jpg"]}]
     */

    private String status;
    private String message;
    /**
     * trend_id : 24
     * trend_caption : Beach Babes and Bash
     * trend_banner : http://www.20dresses.com/uploads/trends/BEACHBABESnBASHtrendbnr.jpg
     * total : 44
     * thumbnails : ["http://www.20dresses.com/uploads/product/temp_img/thumb_Blue-Sea-Tales-Top-1.JPG","http://www.20dresses.com/uploads/product/temp_img/thumb_Moonlit-Tales-Top-1.JPG","http://www.20dresses.com/uploads/product/temp_img/thumb_Simplicity-Body-Chain-1.JPG"]
     */
    @SerializedName("data")
    private List<DataEntity> data;

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public List<DataEntity> getData() {
        return data;
    }

    public void setData(List<DataEntity> data) {
        this.data = data;
    }

    public static class DataEntity {
        private String trend_id;
        private String trend_caption;
        private String trend_banner;
        private String total;
        private List<String> thumbnails;

        public String getTrend_id() {
            return trend_id;
        }

        public void setTrend_id(String trend_id) {
            this.trend_id = trend_id;
        }

        public String getTrend_caption() {
            return trend_caption;
        }

        public void setTrend_caption(String trend_caption) {
            this.trend_caption = trend_caption;
        }

        public String getTrend_banner() {
            return trend_banner;
        }

        public void setTrend_banner(String trend_banner) {
            this.trend_banner = trend_banner;
        }

        public String getTotal() {
            return total;
        }

        public void setTotal(String total) {
            this.total = total;
        }

        public List<String> getThumbnails() {
            return thumbnails;
        }

        public void setThumbnails(List<String> thumbnails) {
            this.thumbnails = thumbnails;
        }
    }
}
