package outletwise.com.twentydresses.model;

/**
 * Created by User-PC on 27-01-2016.
 */
public class ProfilePictureResponse {


    /**
     * status : Success
     * message : Successfully Updated
     * data : {"user_picture":"http://www.20dresses.com/images/profile/"}
     */

    private String status;
    private String message;
    private String user_picture;
    /**
     * user_picture : http://www.20dresses.com/images/profile/
     */

    private DataEntity data;

    public void setStatus(String status) {
        this.status = status;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public void setData(DataEntity data) {
        this.data = data;
    }

    public String getStatus() {
        return status;
    }

    public String getMessage() {
        return message;
    }


    public void setUser_picture(String user_picture) {
        this.user_picture = user_picture;
    }

    public String getUser_picture() {
        return user_picture;
    }

    public DataEntity getData() {
        return data;
    }

    public static class DataEntity {
        private String user_picture;

        public void setUser_picture(String user_picture) {
            this.user_picture = user_picture;
        }

        public String getUser_picture() {
            return user_picture;
        }
    }
}
