package outletwise.com.twentydresses.model;

/**
 * Created by User-PC on 20-08-2015.
 */
public class VisitorResponse {

    /**
     * message : Email Already Registered
     * status : Failed
     */
    private String message;
    private String status;
    private String user_registered;
    private String refer_code;


    public void setUser_registered(String user_registered) {
        this.user_registered = user_registered;
    }

    public void setMessage(String message) {
        this.message = message;
    }
    public void setStatus(String status) {
        this.status = status;
    }

    public String getMessage() {
        return message;
    }

    public String getStatus() {
        return status;
    }

    public String getUser_registered() {
        return user_registered;
    }

    public void setRefer_code(String refer_code) {
        this.refer_code = refer_code;
    }

    public String getRefer_code() {
        return refer_code;
    }
}
