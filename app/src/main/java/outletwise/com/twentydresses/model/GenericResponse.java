package outletwise.com.twentydresses.model;

import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

/**
 * Created by webwerks on 11/23/16.
 */
public class GenericResponse implements Serializable{

    @SerializedName("status")
    private String status;

    @SerializedName("message")
    private String message;

    public String getStatus() {
        return status;
    }

    public String getMessage() {
        return message;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public void setMessage(String message) {
        this.message = message;
    }
}
