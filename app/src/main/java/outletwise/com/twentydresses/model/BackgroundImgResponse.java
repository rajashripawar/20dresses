package outletwise.com.twentydresses.model;

/**
 * Created by User-PC on 07-03-2016.
 */
public class BackgroundImgResponse {


    /**
     * status : success
     * image : http://www.20dresses.com/images/background/home.png
     */

    private String status;
    private String image;

    public void setStatus(String status) {
        this.status = status;
    }

    public void setImage(String image) {
        this.image = image;
    }

    public String getStatus() {
        return status;
    }

    public String getImage() {
        return image;
    }
}
