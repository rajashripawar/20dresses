package outletwise.com.twentydresses.model;

import com.google.gson.annotations.SerializedName;

import outletwise.com.twentydresses.model.database.greenbot.User;

/**
 * Created by User-PC on 28-08-2015.
 */
public class Registration {

    private String email;
    private String password;
    private String auth_token;
    @SerializedName("user")
    private Long user_id;

    private Quiz quiz;
    @SerializedName("data")
    private User user;

    private static Registration registration;
    private String refer_code;
    private Login login;
    private String facebook_id;
    private String facebook_access_token;
    private String user_device_id;
    private Float android_app_version;

    private Registration() {
    }

    public static Registration getInstance() {
        if (registration == null)
            registration = new Registration();
        return registration;
    }

    public Float getCurrent_app_version() {
        return android_app_version;
    }

    public void setCurrent_app_version(Float current_app_version) {
        this.android_app_version = current_app_version;
    }

    public String getUser_device_id() {
        return user_device_id;
    }

    public void setUser_device_id(String user_device_id) {
        this.user_device_id = user_device_id;
    }

    public String getEmail() {
        return email;
    }

    public void setToken(String auth_token) {
        this.auth_token = auth_token;
    }

    public void setUser_id(Long user_id) {
        this.user_id = user_id;
    }

    public Long getUser_id() {
        return user_id;
    }

    public String getToken() {
        return auth_token;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public void setFacebookId(String facebook_id) {
        this.facebook_id = facebook_id;
    }

    public void setFacebook_access_token(String facebook_access_token) {
        this.facebook_access_token = facebook_access_token;
    }

    public String getFacebook_access_token() {
        return facebook_access_token;
    }

    public void setLogin(Login login) {
        email = login.getEmail();
        password = login.getPassword();
        this.login = login;
    }

    public Login getLogin() {
        return login;
    }

    public User getUser() {
        return user;
    }

    public void setUser(User user) {
        this.user = user;
    }

    public Quiz getQuiz() {
        return quiz;
    }

    public void setQuiz(Quiz quiz) {
        this.quiz = quiz;
    }

    public void setReferCode(String referCode) {
        this.refer_code = referCode;
    }
}
