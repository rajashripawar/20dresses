package outletwise.com.twentydresses.model;

/**
 * Created by User-PC on 21-03-2016.
 */
public class ContactUs {

    /**
     * details : Have a query regarding returns or exchange? Please refer to our Help & FAQ page. <br>For any other query please feel free to contact us on:
     * email : Email: hello@20dresses.com
     * phone : Telephone: +919699822211
     * address : To post something, our address is: <br>20 dresses.com < br >Outletwise Retail Private Limited,319,Adhyaru Industrial Estate,Sun Mills Compound,Lower Parel,Mumbai - 400013.
     */

    private String details;
    private String email;
    private String phone;
    private String address;
    private String aboutus;

    public void setDetails(String details) {
        this.details = details;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public void setAboutus(String aboutus) {
        this.aboutus = aboutus;
    }

    public String getDetails() {
        return details;
    }

    public String getEmail() {
        return email;
    }

    public String getPhone() {
        return phone;
    }

    public String getAddress() {
        return address;
    }

    public String getAboutus() {
        return aboutus;
    }

}
