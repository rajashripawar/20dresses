package outletwise.com.twentydresses.model;

import java.util.List;

/**
 * Created by User-PC on 05-11-2015.
 */
public class WalletResponse {


    /**
     * wallet : 20765
     * show_wallet : true
     * show_refund_button : true
     * wallet_description : Hey Rajshri, 20D Wallet is your money with us. Its 100% safe and very convenient.
     20D Wallet will never expire and you can use this to pay for anything you wish to buy from our site.
     You can always ask for full refund of this 20D Wallet to your bank account.

     In cases of products returns, the 20D Wallet is refunded once the product comes back to our warehouse.
     This generally takes 5-7 days after the reverse pickup. In other cases, it would be done with-in 24-48 hrs of refund request.

     In case you have any issues with your 20D Wallet, please don't hesitate to get in touch with us.
     * refund_types : [{"credit":true,"credit_text":"Credit into my account","bank_details":[{"bank_id":54,"bank_name":"ICICI Bank","bank_account_no":"78547999","bank_account_type":"Savings","bank_branch_name":"Worli Naka","bank_account_name":"Adolf","bank_ifsc_code":"IC457898"},{"bank_id":54,"bank_name":"ICICI Bank","bank_account_no":"78547999","bank_account_type":"Savings","bank_branch_name":"Worli Naka","bank_account_name":"Adolf","bank_ifsc_code":"IC457898"}]},{"bank":false}]
     */

    private String wallet;
    private boolean show_wallet;
    private boolean show_refund_button;
    private String wallet_description;
    private String request_message;
    /**
     * credit : true
     * credit_text : Credit into my account
     * bank_details : [{"bank_id":54,"bank_name":"ICICI Bank","bank_account_no":"78547999","bank_account_type":"Savings","bank_branch_name":"Worli Naka","bank_account_name":"Adolf","bank_ifsc_code":"IC457898"},{"bank_id":54,"bank_name":"ICICI Bank","bank_account_no":"78547999","bank_account_type":"Savings","bank_branch_name":"Worli Naka","bank_account_name":"Adolf","bank_ifsc_code":"IC457898"}]
     */

    private List<RefundTypesEntity> refund_types;

    public void setRequest_message(String request_message) {
        this.request_message = request_message;
    }
    public void setWallet(String wallet) {
        this.wallet = wallet;
    }

    public void setShow_wallet(boolean show_wallet) {
        this.show_wallet = show_wallet;
    }

    public void setShow_refund_button(boolean show_refund_button) {
        this.show_refund_button = show_refund_button;
    }

    public void setWallet_description(String wallet_description) {
        this.wallet_description = wallet_description;
    }

    public void setRefund_types(List<RefundTypesEntity> refund_types) {
        this.refund_types = refund_types;
    }

    public String getWallet() {
        return wallet;
    }

    public boolean isShow_wallet() {
        return show_wallet;
    }

    public boolean isShow_refund_button() {
        return show_refund_button;
    }

    public String getWallet_description() {
        return wallet_description;
    }

    public String getRequest_message() {
        return request_message;
    }

    public List<RefundTypesEntity> getRefund_types() {
        return refund_types;
    }

    public static class RefundTypesEntity {
        private boolean credit;
        private boolean bank;
        private String credit_text;
        /**
         * bank_id : 54
         * bank_name : ICICI Bank
         * bank_account_no : 78547999
         * bank_account_type : Savings
         * bank_branch_name : Worli Naka
         * bank_account_name : Adolf
         * bank_ifsc_code : IC457898
         */

        private List<BankDetailsEntity> bank_details;

        public void setCredit(boolean credit) {
            this.credit = credit;
        }

        public void setBank(boolean bank) {
            this.bank = bank;
        }

        public void setCredit_text(String credit_text) {
            this.credit_text = credit_text;
        }

        public void setBank_details(List<BankDetailsEntity> bank_details) {
            this.bank_details = bank_details;
        }

        public boolean isCredit() {
            return credit;
        }

        public boolean isBank() {
            return bank;
        }

        public String getCredit_text() {
            return credit_text;
        }

        public List<BankDetailsEntity> getBank_details() {
            return bank_details;
        }

        public static class BankDetailsEntity {
            private int bank_id;
            private String bank_name;
            private String bank_account_no;
            private String bank_account_type;
            private String bank_branch_name;
            private String bank_account_name;
            private String bank_ifsc_code;

            public void setBank_id(int bank_id) {
                this.bank_id = bank_id;
            }

            public void setBank_name(String bank_name) {
                this.bank_name = bank_name;
            }

            public void setBank_account_no(String bank_account_no) {
                this.bank_account_no = bank_account_no;
            }

            public void setBank_account_type(String bank_account_type) {
                this.bank_account_type = bank_account_type;
            }

            public void setBank_branch_name(String bank_branch_name) {
                this.bank_branch_name = bank_branch_name;
            }

            public void setBank_account_name(String bank_account_name) {
                this.bank_account_name = bank_account_name;
            }

            public void setBank_ifsc_code(String bank_ifsc_code) {
                this.bank_ifsc_code = bank_ifsc_code;
            }

            public int getBank_id() {
                return bank_id;
            }

            public String getBank_name() {
                return bank_name;
            }

            public String getBank_account_no() {
                return bank_account_no;
            }

            public String getBank_account_type() {
                return bank_account_type;
            }

            public String getBank_branch_name() {
                return bank_branch_name;
            }

            public String getBank_account_name() {
                return bank_account_name;
            }

            public String getBank_ifsc_code() {
                return bank_ifsc_code;
            }
        }
    }
}
