package outletwise.com.twentydresses.model;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by User-PC on 12-05-2016.
 */
public class Rewards {


    /**
     * status : Success
     * data : [{"score":"15","title":"test1","description":"description for level 1","image":"http://www.20dresses.com/uploads/product/cart_img/Linked-To-Gold-Hand-Harness-1.JPG","categories":[{"category_id":"45","category_name":"T-Shirts"},{"category_id":"25","category_name":"Pants and Palazzos"}],"progress":0,"required":15,"achieved":false,"claimed":false,"expired":false,"max_reward":"2000","expire":"40"},{"score":"25","title":"Test","description":"level 2 description ","image":"http://www.20dresses.com/uploads/product/cart_img/Linked-To-Gold-Hand-Harness-1.JPG","categories":[{"category_id":"28","category_name":"Jackets and Shrugs"},{"category_id":"24","category_name":"Shorts and Skirts"}],"progress":0,"required":25,"achieved":false,"claimed":false,"expired":false,"max_reward":"3000","expire":"30"},{"score":"35","title":"Test3","description":"","image":"http://www.20dresses.com/uploads/product/cart_img/Linked-To-Gold-Hand-Harness-1.JPG","categories":[{"category_id":"5","category_name":"Dresses"}],"progress":0,"required":35,"achieved":false,"claimed":false,"expired":false,"max_reward":"4000","expire":"45"}]
     * user : {"score":"0"}
     */

    private String status;
    private String message;
    /**
     * score : 0
     */

    private UserEntity user;
    /**
     * score : 15
     * title : test1
     * description : description for level 1
     * image : http://www.20dresses.com/uploads/product/cart_img/Linked-To-Gold-Hand-Harness-1.JPG
     * categories : [{"category_id":"45","category_name":"T-Shirts"},{"category_id":"25","category_name":"Pants and Palazzos"}]
     * progress : 0
     * required : 15
     * achieved : false
     * claimed : false
     * expired : false
     * max_reward : 2000
     * expire : 40
     */

    private ArrayList<DataEntity> data;

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public UserEntity getUser() {
        return user;
    }

    public void setUser(UserEntity user) {
        this.user = user;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public String getMessage() {
        return message;
    }

    public ArrayList<DataEntity> getData() {
        return data;
    }

    public void setData(ArrayList<DataEntity> data) {
        this.data = data;
    }

    public static class UserEntity {
        private String score;

        public String getScore() {
            return score;
        }

        public void setScore(String score) {
            this.score = score;
        }
    }

    public static class DataEntity {
        private String score;
        private String title;
        private String description;
        private String text;
        private String image;
        private int progress;
        private int progress_percent;
        private int required;
        private boolean achieved;
        private boolean claimed;
        private boolean expired;
        private String max_reward;
        private String expire;
        /**
         * category_id : 45
         * category_name : T-Shirts
         */

        private List<CategoriesEntity> categories;

        public String getScore() {
            return score;
        }

        public void setScore(String score) {
            this.score = score;
        }

        public String getTitle() {
            return title;
        }

        public void setTitle(String title) {
            this.title = title;
        }

        public String getDescription() {
            return description;
        }

        public void setDescription(String description) {
            this.description = description;
        }

        public String getText() {
            return text;
        }

        public void setText(String text) {
            this.text = text;
        }

        public String getImage() {
            return image;
        }

        public void setImage(String image) {
            this.image = image;
        }

        public int getProgress() {
            return progress;
        }

        public int getProgress_percent() {
            return progress_percent;
        }

        public void setProgress(int progress) {
            this.progress = progress;
        }

        public void setProgress_percent(int progress_percent) {
            this.progress_percent = progress_percent;
        }

        public int getRequired() {
            return required;
        }

        public void setRequired(int required) {
            this.required = required;
        }

        public boolean isAchieved() {
            return achieved;
        }

        public void setAchieved(boolean achieved) {
            this.achieved = achieved;
        }

        public boolean isClaimed() {
            return claimed;
        }

        public void setClaimed(boolean claimed) {
            this.claimed = claimed;
        }

        public boolean isExpired() {
            return expired;
        }

        public void setExpired(boolean expired) {
            this.expired = expired;
        }

        public String getMax_reward() {
            return max_reward;
        }

        public void setMax_reward(String max_reward) {
            this.max_reward = max_reward;
        }

        public String getExpire() {
            return expire;
        }

        public void setExpire(String expire) {
            this.expire = expire;
        }

        public List<CategoriesEntity> getCategories() {
            return categories;
        }

        public void setCategories(List<CategoriesEntity> categories) {
            this.categories = categories;
        }

        public static class CategoriesEntity {
            private String category_id;
            private String category_name;

            public String getCategory_id() {
                return category_id;
            }

            public void setCategory_id(String category_id) {
                this.category_id = category_id;
            }

            public String getCategory_name() {
                return category_name;
            }

            public void setCategory_name(String category_name) {
                this.category_name = category_name;
            }
        }
    }
}
