package outletwise.com.twentydresses.model;

import android.os.Parcel;
import android.os.Parcelable;

/**
 * Created by User-PC on 06-11-2015.
 */
public class Address implements Parcelable {
    /**
     * address_id : 7414
     * firstname : Fname
     * lastname : Lname
     * mobile : 9022301126
     * city : Mumbai
     * address : 314,Adhyaru Ind Estate,Lower Parel West
     * pincode : 400013
     * state : Maharashtra
     */

    private String address_id;
    private String firstname;
    private String lastname;
    private String mobile;
    private String city;
    private String address;
    private String pincode;
    private String state;
    private boolean pick_up_available;

    public Address() {

    }

    protected Address(Parcel in) {
        address_id = in.readString();
        firstname = in.readString();
        lastname = in.readString();
        mobile = in.readString();
        city = in.readString();
        address = in.readString();
        pincode = in.readString();
        state = in.readString();
        pick_up_available = in.readByte() != 0;
    }

    public static final Creator<Address> CREATOR = new Creator<Address>() {
        @Override
        public Address createFromParcel(Parcel in) {
            return new Address(in);
        }

        @Override
        public Address[] newArray(int size) {
            return new Address[size];
        }
    };

    public boolean isPick_up_available() {
        return pick_up_available;
    }

    public void setPick_up_available(boolean pick_up_available) {
        this.pick_up_available = pick_up_available;
    }

    public void setAddress_id(String address_id) {
        this.address_id = address_id;
    }

    public void setFirstname(String firstname) {
        this.firstname = firstname;
    }

    public void setLastname(String lastname) {
        this.lastname = lastname;
    }

    public void setMobile(String mobile) {
        this.mobile = mobile;
    }

    public void setCity(String city) {
        this.city = city;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public void setPincode(String pincode) {
        this.pincode = pincode;
    }

    public void setState(String state) {
        this.state = state;
    }

    public String getAddress_id() {
        return address_id;
    }

    public String getFirstname() {
        return firstname;
    }

    public String getLastname() {
        return lastname;
    }

    public String getMobile() {
        return mobile;
    }

    public String getCity() {
        return city;
    }

    public String getAddress() {
        return address;
    }

    public String getPincode() {
        return pincode;
    }

    public String getState() {
        return state;
    }

    @Override
    public String toString() {
        return address + "\n" + city + "\n" + pincode;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(address_id);
        dest.writeString(firstname);
        dest.writeString(lastname);
        dest.writeString(mobile);
        dest.writeString(city);
        dest.writeString(address);
        dest.writeString(pincode);
        dest.writeString(state);
        dest.writeByte((byte) (pick_up_available ? 1 : 0));
    }
}
