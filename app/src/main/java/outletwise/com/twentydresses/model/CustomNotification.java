package outletwise.com.twentydresses.model;

/**
 * Created by User-PC on 19-11-2015.
 */
public class CustomNotification {
    String title;
    String text;
    String image;

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getText() {
        return text;
    }

    public void setText(String text) {
        this.text = text;
    }

    public String getImage() {
        return image;
    }

    public void setImage(String image) {
        this.image = image;
    }
}
