package outletwise.com.twentydresses.model;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by User-PC on 07-12-2015.
 */
public class Reason {

    private String message;
    private String status;
    public String product_exchange_meesage;
    /**
     * reason_id : 2
     * reason : Fit Issues - I don't like the fit
     */

    private List<ReasonsEntity> reasons;
    /**
     * size_value : XS
     * size_id : 1
     */

    private ArrayList<Product.ProductSizesEntity> product_sizes;

    public void setProduct_exchange_meesage(String product_exchange_meesage) {
        this.product_exchange_meesage = product_exchange_meesage;
    }

    public String getProduct_exchange_meesage() {
        return product_exchange_meesage;
    }

    public void setReasons(List<ReasonsEntity> reasons) {
        this.reasons = reasons;
    }

    public void setProduct_sizes(ArrayList<Product.ProductSizesEntity> product_sizes) {
        this.product_sizes = product_sizes;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getMessage() {
        return message;
    }

    public String getStatus() {
        return status;
    }

    public List<ReasonsEntity> getReasons() {
        return reasons;
    }

    public ArrayList<Product.ProductSizesEntity> getProduct_sizes() {
        return product_sizes;
    }

    public static class ReasonsEntity {
        private String reason_id;
        private String reason;

        public void setReason_id(String reason_id) {
            this.reason_id = reason_id;
        }

        public void setReason(String reason) {
            this.reason = reason;
        }

        public String getReason_id() {
            return reason_id;
        }

        public String getReason() {
            return reason;
        }
    }
}
