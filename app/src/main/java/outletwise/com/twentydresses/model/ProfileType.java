package outletwise.com.twentydresses.model;

/**
 * Created by User-PC on 25-08-2015.
 */
public class ProfileType {
    private int ivProfileIcon;
    private String tvProfileType;
    private String tvProfileTypeValue;


    public int getIvProfileIcon() {
        return ivProfileIcon;
    }

    public void setIvProfileIcon(int ivProfileIcon) {
        this.ivProfileIcon = ivProfileIcon;
    }

    public String getTvProfileType() {
        return tvProfileType;
    }

    public void setTvProfileType(String tvProfileType) {
        this.tvProfileType = tvProfileType;
    }

    public String getTvProfileTypeValue() {
        return tvProfileTypeValue;
    }

    public void setTvProfileTypeValue(String tvProfileTypeValue) {
        this.tvProfileTypeValue = tvProfileTypeValue;
    }
}
