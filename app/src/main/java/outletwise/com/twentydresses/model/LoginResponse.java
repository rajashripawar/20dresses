package outletwise.com.twentydresses.model;

import com.google.gson.annotations.SerializedName;

import outletwise.com.twentydresses.model.database.greenbot.User;
import outletwise.com.twentydresses.utilities.Constants;

/**
 * Created by User-PC on 12-08-2015.
 */
public class LoginResponse {

    /**
     * data : {"user_id":"391749",
     *          "user_first_name":"Rajashri",
     *          "user_last_name":"test",
     *          "user_picture":"",
     *          "user_gender":"",
     *          "user_mobile":"9503290109",
     *          "mobile_verified":false,
     *          "user_background_image": "http://www.20dresses.com/images/background/background.png",
     *          "user_style": "Classic"}
     * status : Success
     * token : 210108836755cadcfc23b2c8
     */
    @SerializedName("data")
    private User user;
    private String status;
    private String token;
    private String message;

    public void setUser(User user) {
        this.user = user;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public void setToken(String token) {
        this.token = token;
    }

    public User getUser() {
        return user;
    }

    public String getStatus() {
        return status;
    }

    public String getToken() {
        return token;
    }

    public String getMessage() {
        return message == null ? Constants.MESSAGE_DEFAULT : message;
    }

    public void setMessage(String message) {
        this.message = message;
    }
}
