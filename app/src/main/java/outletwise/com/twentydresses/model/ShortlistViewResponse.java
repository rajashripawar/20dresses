package outletwise.com.twentydresses.model;

/**
 * Created by User-PC on 15-04-2016.
 */
public class ShortlistViewResponse {


    /**
     * product_id : 6533
     * product_name : Branded By Love Red Broad Belt
     * product_discount : 0
     * product_price : 499
     * product_img : http://www.20dresses.com/uploads/product/temp_img/thumb_Branded-By-Love-Red-Broad-Belt-1.jpg
     * product_slug : Accessories/Belts/Branded-By-Love-Red-Broad-Belt
     * category_url : Accessories/Belts
     */

    private String product_id;
    private String product_name;
    private String product_discount;
    private String product_price;
    private String product_img;
    private String product_slug;
    private String category_url;

    public String getProduct_id() {
        return product_id;
    }

    public void setProduct_id(String product_id) {
        this.product_id = product_id;
    }

    public String getProduct_name() {
        return product_name;
    }

    public void setProduct_name(String product_name) {
        this.product_name = product_name;
    }

    public String getProduct_discount() {
        return product_discount;
    }

    public void setProduct_discount(String product_discount) {
        this.product_discount = product_discount;
    }

    public String getProduct_price() {
        return product_price;
    }

    public void setProduct_price(String product_price) {
        this.product_price = product_price;
    }

    public String getProduct_img() {
        return product_img;
    }

    public void setProduct_img(String product_img) {
        this.product_img = product_img;
    }

    public String getProduct_slug() {
        return product_slug;
    }

    public void setProduct_slug(String product_slug) {
        this.product_slug = product_slug;
    }

    public String getCategory_url() {
        return category_url;
    }

    public void setCategory_url(String category_url) {
        this.category_url = category_url;
    }
}
