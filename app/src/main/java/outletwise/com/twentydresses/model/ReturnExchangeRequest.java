package outletwise.com.twentydresses.model;

import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import outletwise.com.twentydresses.utilities.Constants;

/**
 * Created by User-PC on 09-12-2015.
 */
public class ReturnExchangeRequest implements Parcelable {

    private HashMap<Integer, Return> returnMap = new HashMap<>();
    private HashMap<Integer, Exchange> exchangeMap = new HashMap<>();

    @SerializedName("return")
    private List<Return> returnList = new ArrayList<>();
    @SerializedName("exchange")
    private List<Exchange> exchangeList = new ArrayList<>();

    private String user;
    private String order;
    private String auth_token;
    /**
     * ship_it_urself : false
     * delhivery_address : true
     * user_addresses : true
     * address_id : 5
     * new_address : true
     * new_address_details : {"firstname":"Addu","lastname":"s","address":"Worli","mobile":"9874587896","city":"Mumabi","state":"Maharashtra","pincode":"400015"}
     */

    private AddressEntity address;
    /**
     * current_bank : true
     * bank_id : 57
     * is_new_bank_details : true
     * bank_details : {"bank_name":"ICICI Bank","bank_account_no":"78547999","bank_account_type":"Savings","bank_branch_name":"Worli Naka","bank_account_name":"Adolf","bank_ifsc_code":"IC457898"}
     * wallet : true
     */

    private RefundEntity refund;


    public ReturnExchangeRequest() {

    }

    protected ReturnExchangeRequest(Parcel in) {
        user = in.readString();
        order = in.readString();
        auth_token = in.readString();
    }


    public static final Creator<ReturnExchangeRequest> CREATOR = new Creator<ReturnExchangeRequest>() {
        @Override
        public ReturnExchangeRequest createFromParcel(Parcel in) {
            return new ReturnExchangeRequest(in);
        }

        @Override
        public ReturnExchangeRequest[] newArray(int size) {
            return new ReturnExchangeRequest[size];
        }
    };

    public String getUser() {
        return user;
    }

    public void setUser(String user) {
        this.user = user;
    }

    public String getOrder() {
        return order;
    }

    public void setOrder(String order) {
        this.order = order;
    }

    public String getAuth_token() {
        return auth_token;
    }

    public void setAuth_token(String auth_token) {
        this.auth_token = auth_token;
    }

    public HashMap<Integer, Return> getReturnMap() {
        return returnMap;
    }

    public Return getReturnItem(int pos) {
        return returnMap.get(pos);
    }

    public void removeReturnItem(int pos) {
        returnMap.remove(pos);
    }

    public HashMap<Integer, Exchange> getExchangeMap() {
        return exchangeMap;
    }

    public Exchange getExchangeItem(int pos) {
        return exchangeMap.get(pos);
    }

    public void removeExchangeItem(int pos) {
        exchangeMap.remove(pos);
    }

    public void addReturn(int pos, Return item) {
        returnMap.put(pos, item);
    }

    public void addExchange(int pos, Exchange item) {
        exchangeMap.put(pos, item);
    }

    public List<Return> getReturnList() {
        return returnList;
    }

    public void setReturnList(List<Return> returnList) {
        this.returnList = returnList;
    }

    public List<Exchange> getExchangeList() {
        return exchangeList;
    }

    public void setExchangeList(List<Exchange> exchangeList) {
        this.exchangeList = exchangeList;
    }

    public void setAddress(AddressEntity address) {
        this.address = address;
    }

    public AddressEntity getAddress() {
        return address;
    }

    public void setRefund(RefundEntity refund) {
        this.refund = refund;
    }

    public RefundEntity getRefund() {
        return refund;
    }

    public String getRefundAmount() {
        double amount = 0;
        for (Return returnItem : returnList) {
            amount += returnItem.getRefund_amount();
        }

        return Constants.formatAmount(amount);
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(user);
        dest.writeString(order);
        dest.writeString(auth_token);
    }


    public static class Return {
        private String item_id;
        private String item_reason_id;
        private double refund_amount;

        public double getRefund_amount() {
            return refund_amount;
        }

        public void setRefund_amount(double refund_amount) {
            this.refund_amount = refund_amount;
        }

        public String getItem_id() {
            return item_id;
        }

        public void setItem_id(String item_id) {
            this.item_id = item_id;
        }

        public String getItem_reason_id() {
            return item_reason_id;
        }

        public void setItem_reason_id(String item_reason_id) {
            this.item_reason_id = item_reason_id;
        }
    }

    public static class Exchange {
        private String item_id;
        private String item_size_id;
        private String item_reason_id;

        public String getItem_id() {
            return item_id;
        }

        public void setItem_id(String item_id) {
            this.item_id = item_id;
        }

        public String getItem_size_id() {
            return item_size_id;
        }

        public void setItem_size_id(String item_size_id) {
            this.item_size_id = item_size_id;
        }

        public String getItem_reason_id() {
            return item_reason_id;
        }

        public void setItem_reason_id(String item_reason_id) {
            this.item_reason_id = item_reason_id;
        }
    }

    public static class AddressEntity implements Parcelable {
        private boolean ship_it_urself;
        private boolean delhivery_address;
        private boolean user_addresses;
        private String address_id;
        private boolean new_address;
        @SerializedName("address_type")
        private Constants.AddressType addressType;
        /**
         * firstname : Addu
         * lastname : s
         * address : Worli
         * mobile : 9874587896
         * city : Mumabi
         * state : Maharashtra
         * pincode : 400015
         */

        private Address new_address_details;

        public AddressEntity() {
        }

        protected AddressEntity(Parcel in) {
            ship_it_urself = in.readByte() != 0;
            delhivery_address = in.readByte() != 0;
            user_addresses = in.readByte() != 0;
            address_id = in.readString();
            new_address = in.readByte() != 0;
            new_address_details = in.readParcelable(Address.class.getClassLoader());
        }

        public static final Creator<AddressEntity> CREATOR = new Creator<AddressEntity>() {
            @Override
            public AddressEntity createFromParcel(Parcel in) {
                return new AddressEntity(in);
            }

            @Override
            public AddressEntity[] newArray(int size) {
                return new AddressEntity[size];
            }
        };

        public Constants.AddressType getAddressType() {
            return addressType;
        }

        public void setAddressType(Constants.AddressType addressType) {
            this.addressType = addressType;
        }

        public void setShip_it_urself(boolean ship_it_urself) {
            this.ship_it_urself = ship_it_urself;
        }

        public void setDelhivery_address(boolean delhivery_address) {
            this.delhivery_address = delhivery_address;
        }

        public void setUser_addresses(boolean user_addresses) {
            this.user_addresses = user_addresses;
        }

        public void setAddress_id(String address_id) {
            this.address_id = address_id;
        }

        public void setNew_address(boolean new_address) {
            this.new_address = new_address;
        }

        public boolean getNew_address() {
            return new_address;
        }

        public void setNew_address_details(Address new_address_details) {
            this.new_address_details = new_address_details;
        }

        public boolean isShip_it_urself() {
            return ship_it_urself;
        }

        public boolean isDelhivery_address() {
            return delhivery_address;
        }

        public boolean isUser_addresses() {
            return user_addresses;
        }

        public String getAddress_id() {
            return address_id;
        }

        public boolean isNew_address() {
            return new_address;
        }

        public Address getNew_address_details() {
            return new_address_details;
        }

        @Override
        public int describeContents() {
            return 0;
        }

        @Override
        public void writeToParcel(Parcel dest, int flags) {
            dest.writeByte((byte) (ship_it_urself ? 1 : 0));
            dest.writeByte((byte) (delhivery_address ? 1 : 0));
            dest.writeByte((byte) (user_addresses ? 1 : 0));
            dest.writeString(address_id);
            dest.writeByte((byte) (new_address ? 1 : 0));
            dest.writeParcelable(new_address_details, flags);
        }
    }

    public static class RefundEntity {

        public boolean is_new_bank_details() {
            return is_new_bank_details;
        }

        private Constants.RefundType refundType;
        private boolean current_bank;
        private int bank_id;
        private boolean is_new_bank_details;
        /**
         * bank_name : ICICI Bank
         * bank_account_no : 78547999
         * bank_account_type : Savings
         * bank_branch_name : Worli Naka
         * bank_account_name : Adolf
         * bank_ifsc_code : IC457898
         */

        @SerializedName("new_bank_details")
        private OrderReturnResponse.RefundEntity.BankDetailsEntity bank_details;
        private boolean wallet;

        public boolean isCredit() {
            return credit;
        }

        public void setCredit(boolean credit) {
            this.credit = credit;
        }

        private boolean credit;


        public Constants.RefundType getRefundType() {
            return refundType;
        }

        public void setRefundType(Constants.RefundType refundType) {
            this.refundType = refundType;
        }

        public void setCurrent_bank(boolean current_bank) {
            this.current_bank = current_bank;
        }

        public void setBank_id(int bank_id) {
            this.bank_id = bank_id;
        }

        public void setIs_new_bank_details(boolean is_new_bank_details) {
            this.is_new_bank_details = is_new_bank_details;
        }

        public void setBank_details(OrderReturnResponse.RefundEntity.BankDetailsEntity bank_details) {
            this.bank_details = bank_details;
        }

        public void setWallet(boolean wallet) {
            this.wallet = wallet;
        }

        public boolean isCurrent_bank() {
            return current_bank;
        }

        public int getBank_id() {
            return bank_id;
        }

        public boolean isIs_new_bank_details() {
            return is_new_bank_details;
        }

        public OrderReturnResponse.RefundEntity.BankDetailsEntity getBank_details() {
            return bank_details;
        }

        public boolean isWallet() {
            return wallet;
        }


    }
}
