package outletwise.com.twentydresses.model;

/**
 * Created by Admin on 12/31/2015.
 */
public class NewAddressResponse {
    private String status;
    private String address_id;

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }


    public String getAddress_id() {
        return address_id;
    }

    public void setAddress_id(String address_id) {
        this.address_id = address_id;
    }
}
