package outletwise.com.twentydresses.model;

import android.os.Parcel;
import android.os.Parcelable;

import java.util.List;

/**
 * Created by User-PC on 07-12-2015.
 */
public class OrderReturnResponse implements Parcelable {

    /**
     * order_total : 1132.00
     * order_number : 41651
     * order_items : [{"item_refund_amount":566,"item_image":"http://www.20dresses.com/uploads/product/cart_img/","item_name":"The Quoteless Humor Tee","item_id":"69863","item_size":"S"},{"item_refund_amount":566,"item_image":"http://www.20dresses.com/uploads/product/cart_img/","item_name":"Keep The Peace Tee ","item_id":"69864","item_size":"M"}]
     * paymentm_mode : PREPAID
     * order_date : 3 Dec,2015
     * address : [{"user_addresses":[{"pincode":"400015","address":"Worli","state":"Maharashtra","lastname":"s","firstname":"Addu","pick_up_available":true,"city":"Mumabi","mobile":"9874587896"}],"delhivery_address":[{"pincode":"401201","address":"Test test","state":"Maharashtra","lastname":"D'silva","firstname":"Adolf","pick_up_available":true,"city":"Mumbai ","mobile":"8698673954"}]}]
     * refund : [{"credit":false,"wallet":"We will credit your 20D Wallet of amount {amount} on account of this return. This amount will be credited immediately on the completion of this return request.\n\nYou can use your wallet to purchase anything from our website until the refund is processed. You can choose to request for refund later also.\n\nPS. 20D WALLET never expires.","bank_details":[{"bank_account_no":"78547999","bank_name":"ICICI Bank","bank_account_type":"Savings","bank_branch_name":"Worli Naka","bank_ifsc_code":"IC457898","bank_account_name":"Adolf"}],"bank":true}]
     */

    private String order_total;
    private String order_number;
    private String paymentm_mode;
    private String order_date;
    /**
     * item_refund_amount : 566
     * item_image : http://www.20dresses.com/uploads/product/cart_img/
     * item_name : The Quoteless Humor Tee
     * item_id : 69863
     * item_size : S
     */

    private List<OrderItemsEntity> order_items;
    private List<AddressEntity> address;
    /**
     * credit : false
     * wallet : We will credit your 20D Wallet of amount {amount} on account of this return. This amount will be credited immediately on the completion of this return request.
     * <p/>
     * You can use your wallet to purchase anything from our website until the refund is processed. You can choose to request for refund later also.
     * <p/>
     * PS. 20D WALLET never expires.
     * bank_details : [{"bank_account_no":"78547999","bank_name":"ICICI Bank","bank_account_type":"Savings","bank_branch_name":"Worli Naka","bank_ifsc_code":"IC457898","bank_account_name":"Adolf"}]
     * bank : true
     */

    private List<RefundEntity> refund;

    protected OrderReturnResponse(Parcel in) {
        order_total = in.readString();
        order_number = in.readString();
        paymentm_mode = in.readString();
        order_date = in.readString();
    }

    public static final Creator<OrderReturnResponse> CREATOR = new Creator<OrderReturnResponse>() {
        @Override
        public OrderReturnResponse createFromParcel(Parcel in) {
            return new OrderReturnResponse(in);
        }

        @Override
        public OrderReturnResponse[] newArray(int size) {
            return new OrderReturnResponse[size];
        }
    };

    public void setOrder_total(String order_total) {
        this.order_total = order_total;
    }

    public void setOrder_number(String order_number) {
        this.order_number = order_number;
    }

    public void setPaymentm_mode(String paymentm_mode) {
        this.paymentm_mode = paymentm_mode;
    }

    public void setOrder_date(String order_date) {
        this.order_date = order_date;
    }

    public void setOrder_items(List<OrderItemsEntity> order_items) {
        this.order_items = order_items;
    }

    public void setAddress(List<AddressEntity> address) {
        this.address = address;
    }

    public void setRefund(List<RefundEntity> refund) {
        this.refund = refund;
    }

    public String getOrder_total() {
        return order_total;
    }

    public String getOrder_number() {
        return order_number;
    }

    public String getPaymentm_mode() {
        return paymentm_mode;
    }

    public String getOrder_date() {
        return order_date;
    }

    public List<OrderItemsEntity> getOrder_items() {
        return order_items;
    }

    public List<AddressEntity> getAddress() {
        return address;
    }

    public List<RefundEntity> getRefund() {
        return refund;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(order_total);
        dest.writeString(order_number);
        dest.writeString(paymentm_mode);
        dest.writeString(order_date);
    }

    public static class OrderItemsEntity {
        private double item_refund_amount;
        private String item_image;
        private String item_name;
        private String item_id;
        private String item_size;

        public void setItem_refund_amount(double item_refund_amount) {
            this.item_refund_amount = item_refund_amount;
        }

        public void setItem_image(String item_image) {
            this.item_image = item_image;
        }

        public void setItem_name(String item_name) {
            this.item_name = item_name;
        }

        public void setItem_id(String item_id) {
            this.item_id = item_id;
        }

        public void setItem_size(String item_size) {
            this.item_size = item_size;
        }

        public double getItem_refund_amount() {
            return item_refund_amount;
        }

        public String getItem_image() {
            return item_image;
        }

        public String getItem_name() {
            return item_name;
        }

        public String getItem_id() {
            return item_id;
        }

        public String getItem_size() {
            return item_size;
        }
    }

    public static class AddressEntity {
        /**
         * pincode : 400015
         * address : Worli
         * state : Maharashtra
         * lastname : s
         * firstname : Addu
         * pick_up_available : true
         * city : Mumabi
         * mobile : 9874587896
         */

        private List<Address> user_addresses;
        /**
         * pincode : 401201
         * address : Test test
         * state : Maharashtra
         * lastname : D'silva
         * firstname : Adolf
         * pick_up_available : true
         * city : Mumbai
         * mobile : 8698673954
         */

        private List<Address> delivery_address;

        public void setUser_addresses(List<Address> user_addresses) {
            this.user_addresses = user_addresses;
        }

        public void setDelhivery_address(List<Address> delivery_address) {
            this.delivery_address = delivery_address;
        }

        public List<Address> getUser_addresses() {
            return user_addresses;
        }

        public List<Address> getDelhivery_address() {
            return delivery_address;
        }

    }

    public static class RefundEntity {
        private boolean credit;
        private String wallet;
        private boolean bank;

        public String getCredit_description() {
            return credit_description;
        }

        public void setCredit_description(String credit_description) {
            this.credit_description = credit_description;
        }

        private String credit_description;




        /**
         * bank_account_no : 78547999
         * bank_name : ICICI Bank
         * bank_account_type : Savings
         * bank_branch_name : Worli Naka
         * bank_ifsc_code : IC457898
         * bank_account_name : Adolf
         */

        private List<BankDetailsEntity> bank_details;

        public void setCredit(boolean credit) {
            this.credit = credit;
        }

        public void setWallet(String wallet) {
            this.wallet = wallet;
        }

        public void setBank(boolean bank) {
            this.bank = bank;
        }

        public void setBank_details(List<BankDetailsEntity> bank_details) {
            this.bank_details = bank_details;
        }

        public boolean isCredit() {
            return credit;
        }

        public String getWallet() {
            return wallet;
        }

        public boolean isBank() {
            return bank;
        }

        public List<BankDetailsEntity> getBank_details() {
            return bank_details;
        }

        public static class BankDetailsEntity {
            private String bank_id;
            private String bank_account_no;
            private String bank_name;
            private String bank_account_type;
            private String bank_branch_name;
            private String bank_ifsc_code;
            private String bank_account_name;

            public String getBank_id() {
                return bank_id;
            }

            public void setBank_id(String bank_id) {
                this.bank_id = bank_id;
            }

            public void setBank_account_no(String bank_account_no) {
                this.bank_account_no = bank_account_no;
            }

            public void setBank_name(String bank_name) {
                this.bank_name = bank_name;
            }

            public void setBank_account_type(String bank_account_type) {
                this.bank_account_type = bank_account_type;
            }

            public void setBank_branch_name(String bank_branch_name) {
                this.bank_branch_name = bank_branch_name;
            }

            public void setBank_ifsc_code(String bank_ifsc_code) {
                this.bank_ifsc_code = bank_ifsc_code;
            }

            public void setBank_account_name(String bank_account_name) {
                this.bank_account_name = bank_account_name;
            }

            public String getBank_account_no() {
                return bank_account_no;
            }

            public String getBank_name() {
                return bank_name;
            }

            public String getBank_account_type() {
                return bank_account_type;
            }

            public String getBank_branch_name() {
                return bank_branch_name;
            }

            public String getBank_ifsc_code() {
                return bank_ifsc_code;
            }

            public String getBank_account_name() {
                return bank_account_name;
            }

            @Override
            public String toString() {
                return "Bank Name: " + bank_name + "\nAccount Type: " + bank_account_type + "\nAccount No:" + bank_account_no +
                        "\nBranch: " + bank_branch_name + "\nIFSC Code: " + bank_ifsc_code;
            }
        }
    }
}
