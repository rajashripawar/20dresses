package outletwise.com.twentydresses.model;

/**
 * Created by User-PC on 06-04-2016.
 */
public class AppVersionResponse {

    /**
     * status : Success
     * data : {"version":1.1,"outdated_version":1,"menu":"","products":"","categories":"","offers":"","tips":""}
     */

    private String status;
    /**
     * version : 1.1
     * outdated_version : 1.0
     * menu :
     * products :
     * categories :
     * offers :
     * tips :
     */

    private DataEntity data;

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public DataEntity getData() {
        return data;
    }

    public void setData(DataEntity data) {
        this.data = data;
    }

    public static class DataEntity {
        private float version;
        private float outdated_version;
        private String menu;
        private String products;
        private String categories;
        private String offers;
        private String tips;
        private String user;
        private String last_purchase_date;

        public float getVersion() {
            return version;
        }

        public void setVersion(float version) {
            this.version = version;
        }

        public float getOutdated_version() {
            return outdated_version;
        }

        public void setOutdated_version(float outdated_version) {
            this.outdated_version = outdated_version;
        }

        public String getMenu() {
            return menu;
        }

        public void setMenu(String menu) {
            this.menu = menu;
        }

        public String getProducts() {
            return products;
        }

        public void setProducts(String products) {
            this.products = products;
        }

        public String getCategories() {
            return categories;
        }

        public void setCategories(String categories) {
            this.categories = categories;
        }

        public String getOffers() {
            return offers;
        }

        public void setOffers(String offers) {
            this.offers = offers;
        }

        public String getTips() {
            return tips;
        }

        public void setTips(String tips) {
            this.tips = tips;
        }

        public void setUser(String user) {
            this.user = user;
        }

        public String getUser() {
            return user;
        }

        public String getLast_purchase_date() {
            return last_purchase_date;
        }

        public void setLast_purchase_date(String last_purchase_date) {
            this.last_purchase_date = last_purchase_date;
        }
    }
}
