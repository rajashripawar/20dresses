package outletwise.com.twentydresses.model;

import java.util.ArrayList;

/**
 * Created by webwerks on 25/7/16.
 */
public class HomeBanner extends GenericResponse{


    /**
     * banner_type : Large
     * position : 0
     * data : [{"offer_image":"http://www.20dresses.com/uploads/cat_banner/dressesbanner2.jpg","category_id":"5","sale":"0"}]
     */

    private ArrayList<BannersBean> banners;
    /**
     * offer_description : 20% Off On All Earrings
     * category_id : 12
     * banner_link : http://www.20dresses.com/Accessories/Earrings
     * offer_percentage : 20
     */

    private ArrayList<TodayOfferBean> today_offer;

    public ArrayList<BannersBean> getBanners() {
        return banners;
    }

    public void setBanners(ArrayList<BannersBean> banners) {
        this.banners = banners;
    }

    public ArrayList<TodayOfferBean> getToday_offer() {
        return today_offer;
    }

    public void setToday_offer(ArrayList<TodayOfferBean> today_offer) {
        this.today_offer = today_offer;
    }

    public static class BannersBean {
        private String banner_type;
        private String position;
        /**
         * offer_image : http://www.20dresses.com/uploads/cat_banner/dressesbanner2.jpg
         * category_id : 5
         * sale : 0
         */

        private ArrayList<DataBean> data;

        public String getBanner_type() {
            return banner_type;
        }

        public void setBanner_type(String banner_type) {
            this.banner_type = banner_type;
        }

        public String getPosition() {
            return position;
        }

        public void setPosition(String position) {
            this.position = position;
        }

        public ArrayList<DataBean> getData() {
            return data;
        }

        public void setData(ArrayList<DataBean> data) {
            this.data = data;
        }

        public static class DataBean {
            private String offer_image;
            private String category_id;
            private String sale;
            private String trend_id;
            private String trend_page;
            private String trend_caption;
            private String total;
            private String product_id;

            public String getOffer_image() {
                return offer_image;
            }

            public void setOffer_image(String offer_image) {
                this.offer_image = offer_image;
            }

            public String getCategory_id() {
                return category_id;
            }

            public void setCategory_id(String category_id) {
                this.category_id = category_id;
            }

            public void setTrend_id(String trend_id) {
                this.trend_id = trend_id;
            }

            public void setTrend_page(String trend_page) {
                this.trend_page = trend_page;
            }

            public void setTrend_caption(String trend_caption) {
                this.trend_caption = trend_caption;
            }

            public void setTotal(String total) {
                this.total = total;
            }

            public String getTrend_id() {
                return trend_id;
            }

            public String getTrend_page() {
                return trend_page;
            }

            public String getTrend_caption() {
                return trend_caption;
            }

            public String getTotal() {
                return total;
            }

            public String getSale() {
                return sale;
            }

            public void setSale(String sale) {
                this.sale = sale;
            }

            public String getProduct_id() {
                return product_id;
            }

            public void setProduct_id(String product_id) {
                this.product_id = product_id;
            }
        }
    }

    public static class TodayOfferBean {
        private String offer_description;
        private String category_id;
        private String banner_link;
        private String offer_percentage;

        private String offer_image;
        private String sale;
        private String trend_id;
        private String trend_page;
        private String trend_caption;
        private String total;
        private String product_id;


        public String getOffer_description() {
            return offer_description;
        }

        public void setOffer_description(String offer_description) {
            this.offer_description = offer_description;
        }

        public String getCategory_id() {
            return category_id;
        }

        public void setCategory_id(String category_id) {
            this.category_id = category_id;
        }

        public String getBanner_link() {
            return banner_link;
        }

        public void setBanner_link(String banner_link) {
            this.banner_link = banner_link;
        }

        public String getOffer_percentage() {
            return offer_percentage;
        }

        public void setOffer_percentage(String offer_percentage) {
            this.offer_percentage = offer_percentage;
        }

        public String getOffer_image() {
            return offer_image;
        }

        public void setOffer_image(String offer_image) {
            this.offer_image = offer_image;
        }

        public void setTrend_page(String trend_page) {
            this.trend_page = trend_page;
        }

        public void setTrend_caption(String trend_caption) {
            this.trend_caption = trend_caption;
        }

        public void setTotal(String total) {
            this.total = total;
        }

        public String getTrend_id() {
            return trend_id;
        }

        public String getTrend_page() {
            return trend_page;
        }

        public String getTrend_caption() {
            return trend_caption;
        }

        public String getTotal() {
            return total;
        }

        public String getSale() {
            return sale;
        }

        public void setSale(String sale) {
            this.sale = sale;
        }

        public String getProduct_id() {
            return product_id;
        }

        public void setProduct_id(String product_id) {
            this.product_id = product_id;
        }
    }
}
