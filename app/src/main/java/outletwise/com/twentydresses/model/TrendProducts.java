package outletwise.com.twentydresses.model;

import java.util.List;

/**
 * Created by User-PC on 13-05-2016.
 */
public class TrendProducts {


    /**
     * status : Success
     * title : Beach Babes and Bash
     * total_products : 44
     * data : [{"product_id":"4146","product_name":"Currently Electric Culottes","product_discount":"60","product_price":"995","product_img":"http://www.20dresses.com/uploads/product/temp_img/thumb_Currently-Electric-Culottes-1.JPG"},{"product_id":"4085","product_name":"Pop Goes My Heart Sunglasses","product_discount":"60","product_price":"695","product_img":"http://www.20dresses.com/uploads/product/temp_img/thumb_Pop-Goes-My-Heart-Sunglasses-1.JPG"},{"product_id":"3800","product_name":"Simplicity Body Chain","product_discount":"50","product_price":"395","product_img":"http://www.20dresses.com/uploads/product/temp_img/thumb_Simplicity-Body-Chain-1.JPG"},{"product_id":"3760","product_name":"Abstract Memories Maxi Dress","product_discount":"60","product_price":"1495","product_img":"http://www.20dresses.com/uploads/product/temp_img/thumb_Abstract-Memories-Maxi-Dress-1.JPG"},{"product_id":"3670","product_name":"Moonlit Tales Top","product_discount":"60","product_price":"595","product_img":"http://www.20dresses.com/uploads/product/temp_img/thumb_Moonlit-Tales-Top-1.JPG"},{"product_id":"3669","product_name":"Blue Sea Tales Top","product_discount":"60","product_price":"595","product_img":"http://www.20dresses.com/uploads/product/temp_img/thumb_Blue-Sea-Tales-Top-1.JPG"},{"product_id":"3664","product_name":"Brighten My Day Top","product_discount":"60","product_price":"595","product_img":"http://www.20dresses.com/uploads/product/temp_img/thumb_Brighten-My-Day-Top-1.JPG"},{"product_id":"1452","product_name":"Ice Blue Summer Fling Shorts","product_discount":"70","product_price":"795","product_img":"http://www.20dresses.com/uploads/product/temp_img/thumb_Ice-Blue-Summer-Fling-Shorts-1.JPG"}]
     */

    private String status;
    private String title;
    private String total_products;
    /**
     * product_id : 4146
     * product_name : Currently Electric Culottes
     * product_discount : 60
     * product_price : 995
     * product_img : http://www.20dresses.com/uploads/product/temp_img/thumb_Currently-Electric-Culottes-1.JPG
     */

    private List<Product> data;
    private List<String> banner;

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getTotal_products() {
        return total_products;
    }

    public void setTotal_products(String total_products) {
        this.total_products = total_products;
    }

    public List<Product> getData() {
        return data;
    }

    public void setData(List<Product> data) {
        this.data = data;
    }

    public void setBanner(List<String> banner) {
        this.banner = banner;
    }

    public List<String> getBanner() {
        return banner;
    }


}
