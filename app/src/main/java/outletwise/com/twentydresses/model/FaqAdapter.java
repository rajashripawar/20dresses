package outletwise.com.twentydresses.model;

import android.app.Activity;
import android.content.Context;
import android.graphics.Typeface;
import android.text.Html;
import android.text.Spannable;
import android.text.SpannableString;
import android.text.style.RelativeSizeSpan;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.ScrollView;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.HashMap;

import outletwise.com.twentydresses.R;
import outletwise.com.twentydresses.model.database.greenbot.Cart;
import outletwise.com.twentydresses.view.activity.HelpFaqActivity;

/**
 * Created by User-PC on 17-02-2016.
 */
public class FaqAdapter {

    private Context context;
    private LinearLayout linearLayout;
    private ArrayList<String> Questions;
    private ArrayList<String> Answers;
    private ScrollView sv;

    public FaqAdapter(Context context, ArrayList<String> Questions, ArrayList<String> Answers) {
        this.context = context;
        this.linearLayout = (LinearLayout) ((Activity) context).findViewById(R.id.llFaq);
        sv = (ScrollView) ((Activity) context).findViewById(R.id.scrollView);
        this.Questions = Questions;
        this.Answers = Answers;

        if (linearLayout != null)
            addItems();
    }

    ArrayList<String> questionsFaq = new ArrayList<>();
    ArrayList<String> answersFaq = new ArrayList<>();
    HashMap<String, String> hashMapFaq;

    public FaqAdapter(Context context, HashMap<String, String> hashMapFaq) {
        this.hashMapFaq = hashMapFaq;
        for (String key : hashMapFaq.keySet()) {
            questionsFaq.add(key);
        }

        for (int i = 0; i < questionsFaq.size(); i++) {
            answersFaq.add(hashMapFaq.get(questionsFaq.get(i)));
        }

        this.context = context;
        this.linearLayout = (LinearLayout) ((Activity) context).findViewById(R.id.llFaq);
        sv = (ScrollView) ((Activity) context).findViewById(R.id.scrollView);
        this.Questions = questionsFaq;
        this.Answers = answersFaq;

        if (linearLayout != null)
            addItems();

    }

    private void addItems() {

        for (int i = 0; i < Questions.size(); i++) {
            final View faqLayout = LayoutInflater.from(context).inflate(
                    R.layout.layout_adapter_faq, null);

            TextView tvQn = (TextView) faqLayout.findViewById(R.id.tvQn);
            tvQn.setText(Html.fromHtml(Questions.get(i)));
            tvQn.setTypeface(null, Typeface.BOLD);

            faqLayout.setTag(i);
            linearLayout.addView(faqLayout);

            final int temp = i;
            faqLayout.findViewById(R.id.tvQn).setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {

                    TextView tvAns = (TextView) faqLayout.findViewById(R.id.tvAns);
                    tvAns.setText(Html.fromHtml(Answers.get(temp)));

                    if (tvAns.getVisibility() == View.VISIBLE)
                        tvAns.setVisibility(View.GONE);
                    else {
                        tvAns.setVisibility(View.VISIBLE);
                        tvAns.setFocusableInTouchMode(true);
                        tvAns.requestFocus();
                    }
                }
            });
        }
    }


    private SpannableString setSpan(String finalString1) {
        SpannableString sp = new SpannableString(finalString1);
        sp.setSpan(new RelativeSizeSpan(1.2f),
                0, finalString1.length() - 1, Spannable.SPAN_INCLUSIVE_INCLUSIVE);

        return sp;
    }

    public interface OnEvent {
        void onItemClicked(Cart cartItem);
    }
}
