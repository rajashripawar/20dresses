package outletwise.com.twentydresses.model;

import com.google.gson.annotations.SerializedName;

import outletwise.com.twentydresses.model.database.greenbot.User;

/**
 * Created by User-PC on 12-08-2015.
 */
public class Login {
    private String email;
    private String password;
    @SerializedName("data")
    private User user;

    private static Login login;
    private String user_device_id;
    private Float android_app_version;

    public Login() {
    }

    public static Login getInstance() {
        if (login == null)
            login = new Login();
        return login;
    }

    public Float getCurrent_app_version() {
        return android_app_version;
    }

    public void setCurrent_app_version(Float current_app_version) {
        this.android_app_version = current_app_version;
    }


    public String getUser_device_id() {
        return user_device_id;
    }

    public void setUser_device_id(String user_device_id) {
        this.user_device_id = user_device_id;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email.trim();
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password.trim();
    }

    public User getUser() {
        return user;
    }

    public void setUser(User user) {
        this.user = user;
    }
}
