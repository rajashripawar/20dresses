package outletwise.com.twentydresses.model;

/**
 * Created by User-PC on 04-02-2016.
 */
public class UpdateCartResponse {


    /**
     * status : Success
     * message : Successfully Added
     * data : {"product_id":"6243","product_name":"Green Wanderlust Braided Backpack","product_discount":"30","product_price":"1495","product_img":"http://www.20dresses.com/uploads/product/temp_img/thumb_Green-Wanderlust-Braided-Backpack-1.JPG","product_addinfo":"<b>NOTE:<\/b> The colours of the product as seen on the screen and actual colours of the product may slightly differ due to your screen resolution"}
     */

    private String status;
    private String message;
    /**
     * product_id : 6243
     * product_name : Green Wanderlust Braided Backpack
     * product_discount : 30
     * product_price : 1495
     * product_img : http://www.20dresses.com/uploads/product/temp_img/thumb_Green-Wanderlust-Braided-Backpack-1.JPG
     * product_addinfo : <b>NOTE:</b> The colours of the product as seen on the screen and actual colours of the product may slightly differ due to your screen resolution
     */

    private DataEntity data;

    public void setStatus(String status) {
        this.status = status;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public void setData(DataEntity data) {
        this.data = data;
    }

    public String getStatus() {
        return status;
    }

    public String getMessage() {
        return message;
    }

    public DataEntity getData() {
        return data;
    }

    public static class DataEntity {
        private String product_id;
        private String product_name;
        private String product_discount;
        private String product_price;
        private String product_img;
        private String product_addinfo;

        public void setProduct_id(String product_id) {
            this.product_id = product_id;
        }

        public void setProduct_name(String product_name) {
            this.product_name = product_name;
        }

        public void setProduct_discount(String product_discount) {
            this.product_discount = product_discount;
        }

        public void setProduct_price(String product_price) {
            this.product_price = product_price;
        }

        public void setProduct_img(String product_img) {
            this.product_img = product_img;
        }

        public void setProduct_addinfo(String product_addinfo) {
            this.product_addinfo = product_addinfo;
        }

        public String getProduct_id() {
            return product_id;
        }

        public String getProduct_name() {
            return product_name;
        }

        public String getProduct_discount() {
            return product_discount;
        }

        public String getProduct_price() {
            return product_price;
        }

        public String getProduct_img() {
            return product_img;
        }

        public String getProduct_addinfo() {
            return product_addinfo;
        }
    }
}
