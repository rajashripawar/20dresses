package outletwise.com.twentydresses.model;

import android.os.Parcel;
import android.os.Parcelable;
import android.text.TextUtils;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by User-PC on 06-08-2015.
 */
public class Product implements Parcelable {

    /**
     * product_id : 6225
     * product_name : Deep In The Sky Maxi Dress
     * product_discount : 0
     * product_price : 1495
     * size_chart   : "http://www.20dresses.com/uploads/sizechart/Size_Chart.jpg"
     * product_img : http://www.20dresses.com/uploads/product/temp_img/thumb_Deep-In-The-Sky-Maxi-Dress-1.jpg
     * product_addinfo : <b>NOTE:</b> The colours of the product as seen on the screen and actual colours of the product may slightly differ due to your screen resolution
     * product_style : <p>&nbsp;</p><p><b>Accessorize with:</b> A pair of strappy flats, feather earrings and a bold black cuff will complete this look!</p>
     * product_details : <ul><li>Blue Rayon Princess Cut Dress</li><li>Sleeveless and V Neckline</li><li>Slit in The Centre Front</li><li>Corset Tie up at the back for fastening and style</li><li>Note: Accessories shown with the product are only for styling purposes.</li></ul>
     * product_care : <p></p><ul><li>Machine Wash</li><li>Do not Bleach</li><li>Wash separately or with similar colours</li></ul>
     * product_material : Rayon
     * product_stock : 46
     * product_thumbnails : ["http://www.20dresses.com/uploads/product/temp_img/thumb_Deep-In-The-Sky-Maxi-Dress-3.jpg","http://www.20dresses.com/uploads/product/temp_img/thumb_Deep-In-The-Sky-Maxi-Dress-1.jpg","http://www.20dresses.com/uploads/product/temp_img/thumb_Deep-In-The-Sky-Maxi-Dress-2.jpg","http://www.20dresses.com/uploads/product/temp_img/thumb_Deep-In-The-Sky-Maxi-Dress-5.jpg","http://www.20dresses.com/uploads/product/temp_img/thumb_Deep-In-The-Sky-Maxi-Dress-4.jpg","http://www.20dresses.com/uploads/product/temp_img/thumb_Deep-In-The-Sky-Maxi-Dress-6.jpg"]
     * product_images : ["http://www.20dresses.com/uploads/product/temp_img/Deep-In-The-Sky-Maxi-Dress-3.jpg","http://www.20dresses.com/uploads/product/temp_img/Deep-In-The-Sky-Maxi-Dress-1.jpg","http://www.20dresses.com/uploads/product/temp_img/Deep-In-The-Sky-Maxi-Dress-2.jpg","http://www.20dresses.com/uploads/product/temp_img/Deep-In-The-Sky-Maxi-Dress-5.jpg","http://www.20dresses.com/uploads/product/temp_img/Deep-In-The-Sky-Maxi-Dress-4.jpg","http://www.20dresses.com/uploads/product/temp_img/Deep-In-The-Sky-Maxi-Dress-6.jpg"]
     * product_sizes : [{"attr_id":"1","quantity":"10","attr_value":"XS"},{"attr_id":"2","quantity":"8","attr_value":"S"},{"attr_id":"3","quantity":"7","attr_value":"M"},{"attr_id":"4","quantity":"10","attr_value":"L"},{"attr_id":"5","quantity":"7","attr_value":"XL"},{"attr_id":"209","quantity":"4","attr_value":"XXL"}]
     * dimensions: ["Length: 1", "Breadth: 12.2 inches", "Width: 1"]
     */

    private String product_id;
    private String category_id;
    private String product_name;
    private String product_discount;
    private String product_price;
    private String product_img;
    private String size_chart;
    private String product_addinfo;
    private String product_style;
    private String product_details;
    private String product_care;
    private String product_note;
    private String product_material;
    private String product_color;
    private String product_offer;
    private String product_offer_desc;
    private String product_stock;
    private ArrayList<String> product_thumbnails;
    private ArrayList<String> product_images;
    // private String product_size;
    private String product_notify_text;
    private String pincode;
    private boolean product_returnable;
    private String product_offer_title;
    private String product_alert_message;
    private ArrayList<String> product_size = new ArrayList<>();
    private String product_count;
    private String product_url;

    /**
     * attr_id : 1
     * quantity : 10
     * attr_value : XS
     */

    public Product() {

    }

    private ArrayList<ProductSizesEntity> product_sizes;
    private ArrayList<ProductSizesEntity> product_notify_sizes;

    private List<String> dimensions;

    protected Product(Parcel in) {
        product_id = in.readString();
        category_id = in.readString();
        product_name = in.readString();
        product_discount = in.readString();
        product_price = in.readString();
        product_img = in.readString();
        size_chart = in.readString();
        product_addinfo = in.readString();
        product_style = in.readString();
        product_details = in.readString();
        product_care = in.readString();
        product_note = in.readString();
        product_material = in.readString();
        product_color = in.readString();
        product_offer = in.readString();
        product_offer_desc = in.readString();
        product_stock = in.readString();
        product_thumbnails = in.createStringArrayList();
        product_images = in.createStringArrayList();
        //product_size = in.readString();
        product_size = in.createStringArrayList();
        product_notify_text = in.readString();
        pincode = in.readString();
        product_returnable = in.readByte() != 0;
        product_offer_title = in.readString();
        product_alert_message = in.readString();
        product_count = in.readString();
        product_url = in.readString();
    }

    public static final Creator<Product> CREATOR = new Creator<Product>() {
        @Override
        public Product createFromParcel(Parcel in) {
            return new Product(in);
        }

        @Override
        public Product[] newArray(int size) {
            return new Product[size];
        }
    };

    public String getProduct_offer() {
        return product_offer;
    }

    public String getProduct_offer_desc() {
        return product_offer_desc;
    }

    public String getCategory_id() {
        return category_id;
    }

    public void setProduct_offer(String product_offer) {
        this.product_offer = product_offer;
    }

    public void setProduct_offer_desc(String product_offer_desc) {
        this.product_offer_desc = product_offer_desc;
    }

    public void setCategory_id(String category_id) {
        this.category_id = category_id;
    }

    public void setProduct_id(String product_id) {
        this.product_id = product_id;
    }

    public void setProduct_name(String product_name) {
        this.product_name = product_name;
    }

    public void setProduct_discount(String product_discount) {
        this.product_discount = product_discount;
    }

    public void setProduct_price(String product_price) {
        this.product_price = product_price;
    }

    public void setProduct_img(String product_img) {
        this.product_img = product_img;
    }

    public void setSizeChart_img(String sizechart_img) {
        this.size_chart = sizechart_img;
    }

    public void setProduct_addinfo(String product_addinfo) {
        this.product_addinfo = product_addinfo;
    }

    public void setProduct_style(String product_style) {
        this.product_style = product_style;
    }

    public void setProduct_details(String product_details) {
        this.product_details = product_details;
    }

    public void setProduct_care(String product_care) {
        this.product_care = product_care;
    }

    public void setProduct_note(String product_note) {
        this.product_note = product_note;
    }

    public void setProduct_material(String product_material) {
        this.product_material = product_material;
    }

    public void setProduct_color(String product_color) {
        this.product_color = product_color;
    }


    public void setProduct_stock(String product_stock) {
        this.product_stock = product_stock;
    }

    public void setProduct_thumbnails(ArrayList<String> product_thumbnails) {
        this.product_thumbnails = product_thumbnails;
    }

    public void setProduct_images(ArrayList<String> product_images) {
        this.product_images = product_images;
    }

    public void setProduct_sizes(ArrayList<ProductSizesEntity> product_sizes) {
        this.product_sizes = product_sizes;
    }

    public void setProduct_notify_sizes(ArrayList<ProductSizesEntity> product_notify_sizes) {
        this.product_notify_sizes = product_notify_sizes;
    }

    public void setDimensions(List<String> dimensions) {
        this.dimensions = dimensions;
    }

    public String getProduct_id() {
        return product_id;
    }

    public String getProduct_name() {
        return product_name;
    }

    public String getProduct_discount() {
        return product_discount;
    }

    public String getProduct_price() {
        return product_price;
    }

    public String getProduct_img() {
        return product_img;
    }

    public String getSizeChart_img() {
        return size_chart;
    }

    public String getProduct_addinfo() {
        return product_addinfo;
    }

    public String getProduct_style() {
        return product_style;
    }

    public String getProduct_details() {
        return product_details;
    }

    public String getProduct_care() {
        return product_care;
    }

    public String getProduct_note() {
        return product_note;
    }

    public String getProduct_material() {
        return product_material;
    }

    public String getProduct_color() {
        return product_color;
    }

    public String getProduct_stock() {
        return product_stock;
    }

    public ArrayList<String> getProduct_thumbnails() {
        return product_thumbnails;
    }

    public ArrayList<String> getProduct_images() {
        return product_images;
    }

    public ArrayList<ProductSizesEntity> getProduct_sizes() {
        return product_sizes;
    }

    public ArrayList<ProductSizesEntity> getProduct_notify_sizes() {
        return product_notify_sizes;
    }

    public List<String> getDimensions(List<String> dimensions) {
        return dimensions;
    }

    public String getProduct_url() {
        return product_url;
    }

    public void setProduct_url(String product_url) {
        this.product_url = product_url;
    }

    public String getDimensionAsString() {
        String strText = null;
        if (!(dimensions == null))
            strText = TextUtils.join(", ", dimensions);
        return strText;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(product_id);
        dest.writeString(category_id);
        dest.writeString(product_name);
        dest.writeString(product_discount);
        dest.writeString(product_price);
        dest.writeString(product_img);
        dest.writeString(size_chart);
        dest.writeString(product_addinfo);
        dest.writeString(product_style);
        dest.writeString(product_details);
        dest.writeString(product_care);
        dest.writeString(product_note);
        dest.writeString(product_material);
        dest.writeString(product_color);
        dest.writeString(product_offer);
        dest.writeString(product_offer_desc);
        dest.writeString(product_stock);
        dest.writeStringList(product_thumbnails);
        dest.writeStringList(product_images);
        //dest.writeString(product_size);
        dest.writeStringList(product_size);
        dest.writeString(product_notify_text);
        dest.writeString(pincode);
        dest.writeByte((byte) (product_returnable ? 1 : 0));
        dest.writeString(product_offer_title);
        dest.writeString(product_alert_message);
        dest.writeString(product_count);
        dest.writeString(product_url);
    }

    public void setProduct_size(String product_size) {
        // this.product_size = product_size;
        this.product_size.add(product_size);
    }

    public ArrayList<String> getProduct_size() {
        return product_size;
    }

    public void setProduct_notify_text(String product_notify_text) {
        this.product_notify_text = product_notify_text;
    }

    public String getProduct_notify_text() {
        return product_notify_text;
    }

    public void setPincode(String pincode) {
        this.pincode = pincode;
    }

    public String getPincode() {
        return pincode;
    }

    public void setProduct_returnable(boolean product_returnable) {
        this.product_returnable = product_returnable;
    }

    public boolean isProduct_returnable() {
        return product_returnable;
    }

    public void setProduct_offer_title(String product_offer_title) {
        this.product_offer_title = product_offer_title;
    }

    public String getProduct_offer_title() {
        return product_offer_title;
    }

    public void setProduct_alert_message(String product_alert_message) {
        this.product_alert_message = product_alert_message;
    }

    public String getProduct_alert_message() {
        return product_alert_message;
    }


    public void setProduct_count(String product_count) {
        this.product_count = product_count;
    }

    public String getProduct_count() {
        return product_count;
    }

    public static class ProductSizesEntity {
        private String attr_id;
        private String quantity;
        private String attr_value;

        public void setAttr_id(String attr_id) {
            this.attr_id = attr_id;
        }

        public void setQuantity(String quantity) {
            this.quantity = quantity;
        }

        public void setAttr_value(String attr_value) {
            this.attr_value = attr_value;
        }

        public String getAttr_id() {
            return attr_id;
        }

        public String getQuantity() {
            return quantity;
        }

        public String getAttr_value() {
            return attr_value;
        }
    }
}
