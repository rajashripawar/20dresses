package outletwise.com.twentydresses.model;

/**
 * Created by User-PC on 12-01-2016.
 */
public class ReferCodeResponse {


    /**
     * refer_code : UWAKN7
     * refer_banner : http://www.20dresses.com/images/invitebanner.jpg
     * description : <li>Invite friends and Earn Credits</li>
     <li>When the friend you invited registers with us, your friend get 150 Credits</li>
     <li>When the friend you invited makes her first purchase, you get 150 Credits</li>

     */

    private String refer_code;
    private String refer_banner;
    private String description;

    public String getRefer_code() {
        return refer_code;
    }

    public void setRefer_code(String refer_code) {
        this.refer_code = refer_code;
    }

    public String getRefer_banner() {
        return refer_banner;
    }

    public void setRefer_banner(String refer_banner) {
        this.refer_banner = refer_banner;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }
}
