package outletwise.com.twentydresses.model;

import android.provider.ContactsContract;


import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;


/**
 * Created by webwerks on 10/14/16.
 */
public class TestimonialSocial {

    public String getCount() {
        return count;
    }

    public void setCount(String count) {
        this.count = count;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }



    private String count;

    private String status;

    public ArrayList<Testimonials> getData() {
        return data;
    }

    public void setData(ArrayList<Testimonials> data) {
        this.data = data;
    }

    private ArrayList<Testimonials> data;

    public class Testimonials implements Serializable {
        private String t_id;

        public String getTestimonial_via() {
            return testimonial_via;
        }

        public void setTestimonial_via(String testimonial_via) {
            this.testimonial_via = testimonial_via;
        }

        private String testimonial_via;


        public String getT_id() {
            return t_id;
        }

        public void setT_id(String t_id) {
            this.t_id = t_id;
        }

        public String getTestimonial() {
            return testimonial;
        }

        public void setTestimonial(String testimonial) {
            this.testimonial = testimonial;
        }

        public String getLocation() {
            return location;
        }

        public void setLocation(String location) {
            this.location = location;
        }

        public String getImage() {
            return image;
        }

        public void setImage(String image) {
            this.image = image;
        }

        public String getDate() {
            return date;
        }

        public void setDate(String date) {
            this.date = date;
        }

        public String getMessage() {
            return message;
        }

        public void setMessage(String message) {
            this.message = message;
        }

        public String getProduct_id() {
            return product_id;
        }

        public void setProduct_id(String product_id) {
            this.product_id = product_id;
        }

        public String getPosted_image() {
            return posted_image;
        }

        public void setPosted_image(String posted_image) {
            this.posted_image = posted_image;
        }

        public String getName() {
            return name;
        }

        public void setName(String name) {
            this.name = name;
        }

        public String getTestimonial_from() {
            return testimonial_from;
        }

        public void setTestimonial_from(String testimonial_from) {
            this.testimonial_from = testimonial_from;
        }

        private String testimonial;
        private String location;
        private String date;
        private String image;
        private String message;
        private String product_id;
        private String posted_image;
        private String name;
        private String testimonial_from;
    }
}
