package outletwise.com.twentydresses.model;

/**
 * Created by User-PC on 02-12-2015.
 */
public class Offer {


    /**
     * offer_image : http://www.20dresses.com/uploads/cat_banner/SALEshowroombnr22012016.jpg
     * category_id :
     * sale : 10
     * offer_percentage :
     * offer_description :
     */

    private String offer_image;
    private String category_id;
    private String sale;
    private String offer_percentage;
    private String offer_description;
    private String trend_id;
    private String trend_page;
    private String trend_caption;
    private String total;


    public void setOffer_image(String offer_image) {
        this.offer_image = offer_image;
    }

    public void setCategory_id(String category_id) {
        this.category_id = category_id;
    }

    public void setTrend_id(String trend_id) {
        this.trend_id = trend_id;
    }

    public void setTrend_page(String trend_page) {
        this.trend_page = trend_page;
    }

    public void setTrend_caption(String trend_caption) {
        this.trend_caption = trend_caption;
    }

    public void setTotal(String total) {
        this.total = total;
    }

    public void setSale(String sale) {
        this.sale = sale;
    }

    public void setOffer_percentage(String offer_percentage) {
        this.offer_percentage = offer_percentage;
    }

    public void setOffer_description(String offer_description) {
        this.offer_description = offer_description;
    }

    public String getOffer_image() {
        return offer_image;
    }

    public String getCategory_id() {
        return category_id;
    }

    public String getTrend_id() {
        return trend_id;
    }

    public String getTrend_page() {
        return trend_page;
    }

    public String getTrend_caption() {
        return trend_caption;
    }

    public String getTotal() {
        return total;
    }

    public String getSale() {
        return sale;
    }

    public String getOffer_percentage() {
        return offer_percentage;
    }

    public String getOffer_description() {
        return offer_description;
    }
}
