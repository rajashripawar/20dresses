package outletwise.com.twentydresses.model;

import android.app.Notification;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Canvas;
import android.graphics.drawable.BitmapDrawable;
import android.graphics.drawable.Drawable;
import android.media.RingtoneManager;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.support.v7.app.NotificationCompat;

import com.clevertap.android.sdk.CleverTapAPI;
import com.google.android.gms.gcm.GoogleCloudMessaging;
import com.squareup.picasso.Picasso;
import com.squareup.picasso.Target;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

import outletwise.com.twentydresses.R;
import outletwise.com.twentydresses.utilities.Constants;
import outletwise.com.twentydresses.view.activity.ProductsActivity;
import outletwise.com.twentydresses.view.activity.SplashScreen;
import semusi.ruleengine.pushmanager.SdkGcmListenerService;


/**
 * Created by User-PC on 18-03-2016.
 */
public class CustomBroadcastReceiver extends BroadcastReceiver {
    public static final String NOTIFICATION_TAG = "wzrk_pn";

    @Override
    public void onReceive(Context context, Intent intent) {
        String action = intent.getAction();
        if ("com.google.android.c2dm.intent.RECEIVE".equals(action)) {
            handleNotificationIntent(context, intent);
        }

        //new SdkGcmListenerService().onReceive(context, intent);
    }

    private void handleNotificationIntent(Context context, Intent intent) {

        Bundle extras = intent.getExtras();

        GoogleCloudMessaging gcm = GoogleCloudMessaging.getInstance(context);
        String messageType = gcm.getMessageType(intent);

        // To check if notification is from cleverTap
        boolean fromCleverTap = extras.containsKey(NOTIFICATION_TAG);

        if (fromCleverTap) {
            if (GoogleCloudMessaging.MESSAGE_TYPE_MESSAGE.equals(messageType))
                CleverTapAPI.createNotification(context, extras);
           // cleverTapNotification(context, extras);
        } else {
            //mixpanelNotification(context, extras, intent);
            new SdkGcmListenerService().onReceive(context, intent);
        }

    }

    /*
  * Create notification for mixpanel
  * */
    private void mixpanelNotification(Context context, Bundle extras, Intent intent) {

        String message = extras.getString("message");
        String iconName = extras.getString("icon");
        String category_id = intent.getStringExtra("category_id");
        String notificationTitle = extras.getString("title");
        String category_name = intent.getStringExtra("category_name");

        final NotificationManager mNotificationManager = (NotificationManager) context.getSystemService(Context.NOTIFICATION_SERVICE);
        final NotificationCompat.BigPictureStyle notiStyle = new
                NotificationCompat.BigPictureStyle();
        notiStyle.setBigContentTitle(notificationTitle);
        notiStyle.setSummaryText(message);

        Intent intent1;
        if (category_id != null) {
            Category.SubCat subCat = new Category.SubCat();
            subCat.setId(category_id);
            subCat.setName(category_name);
            intent1 = new Intent(context, ProductsActivity.class);
            intent1.putExtra(Constants.CATEGORY, subCat);
        } else
            intent1 = new Intent(context, SplashScreen.class);


        PendingIntent resultPendingIntent = PendingIntent.getActivity(context, 0, intent1, PendingIntent.FLAG_UPDATE_CURRENT);

        final NotificationCompat.Builder myNotificationBuilder = new NotificationCompat.Builder(context);
        myNotificationBuilder
                .setAutoCancel(true)
                .setContentIntent(resultPendingIntent)
                .setContentTitle(notificationTitle)
                .setContentText(message)
                .setStyle(notiStyle)
                .setSmallIcon(getNotificationIcon());


        Target target = new Target() {
            @Override
            public void onBitmapLoaded(Bitmap bitmap, Picasso.LoadedFrom from) {
                Constants.debug("Showing notification with image");
                myNotificationBuilder.setLargeIcon(bitmap);
                notiStyle.bigPicture(bitmap);
                final Notification myNotification = myNotificationBuilder.build();
                myNotification.defaults |= Notification.DEFAULT_LIGHTS;
                myNotification.defaults |= Notification.DEFAULT_VIBRATE;
                myNotification.defaults |= Notification.DEFAULT_SOUND;
                myNotification.flags |= Notification.FLAG_ONLY_ALERT_ONCE;
                mNotificationManager.notify(0, myNotification);
            }

            @Override
            public void onBitmapFailed(Drawable errorDrawable) {
                Constants.debug("Showing notification without image");
                final Notification myNotification = myNotificationBuilder.build();
                myNotification.defaults |= Notification.DEFAULT_LIGHTS;
                myNotification.defaults |= Notification.DEFAULT_VIBRATE;
                myNotification.defaults |= Notification.DEFAULT_SOUND;
                myNotification.flags |= Notification.FLAG_ONLY_ALERT_ONCE;
                mNotificationManager.notify(0, myNotification);
            }

            @Override
            public void onPrepareLoad(Drawable placeHolderDrawable) {

            }
        };
        Picasso.with(context).load(iconName).into(target);
    }


    /*
    * Create notification for clevertap
    * */

    private void cleverTapNotification(final Context context, final Bundle extras) {
        //noinspection ConstantConditions
        if (extras == null || extras.get(NOTIFICATION_TAG) == null) {
            return;
        }

        try {
            postAsyncSafely("CleverTapAPI#createNotification", new Runnable() {
                @Override
                public void run() {
                    try {
                        // Check if this is a test notification
                        if (extras.containsKey("d")
                                && "y".equals(extras.getString("d"))) {
                            int r = (int) (Math.random() * 10);
                            if (r != 8) {
                                // Discard acknowledging this notif
                                return;
                            }
                            JSONObject event = new JSONObject();
                            try {
                                JSONObject actions = new JSONObject();
                                for (String x : extras.keySet()) {
                                    Object value = extras.get(x);
                                    actions.put(x, value);
                                }
                                event.put("evtName", "wzrk_d");
                                event.put("evtData", actions);
                                //QueueManager.addToQueue(context, event, 4);
                            } catch (JSONException ignored) {
                                // Won't happen
                            }
                            // Drop further processing
                            return;
                        }
                        String notifTitle = extras.getString("nt");
                        // If not present, set it to the app name
                        notifTitle = (notifTitle != null) ? notifTitle : context.getApplicationInfo().name;
                        String notifMessage = extras.getString("nm");
                        if (notifMessage == null) {
                            // What else is there to show then?
                            return;
                        }
                        String icoPath = extras.getString("ico");
                        Intent launchIntent;

                        if (extras.containsKey("wzrk_dl")) {
                            launchIntent = new Intent(Intent.ACTION_VIEW, Uri.parse(extras.getString("wzrk_dl")));
                        } else {
                            launchIntent = context.getPackageManager().getLaunchIntentForPackage(context.getPackageName());
                        }

                        PendingIntent pIntent;

                        // Take all the properties from the notif and add it to the intent
                        launchIntent.putExtras(extras);
                        launchIntent.putExtra("wzrk_rts", System.currentTimeMillis());
                        launchIntent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_SINGLE_TOP);
                        pIntent = PendingIntent.getActivity(context, (int) System.currentTimeMillis(),
                                launchIntent, PendingIntent.FLAG_UPDATE_CURRENT);

                        Constants.debug("Notif msg :: " + notifMessage);
                        String messages[] = new String[5];
                        if (notifMessage.contains("+")) {
                            messages = notifMessage.split("\\+");
                        } else {
                            messages[0] = notifMessage;
                            messages[1] = "";
                        }

                        android.support.v4.app.NotificationCompat.Style style;
                        String bigPictureUrl = extras.getString("wzrk_bp");
                        Bitmap bpMap = null;
                        if (bigPictureUrl != null && bigPictureUrl.startsWith("http")) {
                            try {
                                String name = bigPictureUrl;

                                URL url_value = null;
                                try {
                                    url_value = new URL(name);
                                    bpMap = BitmapFactory.decodeStream(url_value.openConnection().getInputStream());
                                } catch (MalformedURLException e) {
                                    e.printStackTrace();
                                } catch (IOException e) {
                                    e.printStackTrace();
                                }

                                //   Bitmap bpMap = Utils.getNotificationBitmap(bigPictureUrl, false, context);

                                if (bpMap == null)
                                    throw new Exception("Failed to fetch big picture!");

                                style = new android.support.v4.app.NotificationCompat.BigPictureStyle()
                                        .bigPicture(bpMap);
                            } catch (Throwable t) {
                                style = new android.support.v4.app.NotificationCompat.BigTextStyle()
                                        .bigText(messages[0]);
                                // Logger.error("Falling back to big text notification, couldn't fetch big picture", t);
                            }
                        } else {
                            style = new android.support.v4.app.NotificationCompat.BigTextStyle()
                                    .bigText(messages[0]);
                        }

                        int smallIcon;
                        try {
                            //  String x = ManifestMetaData.getMetaData(context, com.clevertap.android.sdk.Constants.LABEL_NOTIFICATION_ICON);
                            smallIcon = R.drawable.notification_icon;
                            if (smallIcon == 0) throw new IllegalArgumentException();
                        } catch (Throwable t) {
                            //smallIcon = DeviceInfo.getAppIconAsIntId(context);
                        }

                        android.support.v4.app.NotificationCompat.Builder nb;
                        if (!messages[1].equalsIgnoreCase("")) {
                            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
                                nb = new android.support.v4.app.NotificationCompat.Builder(context)
                                        .setContentTitle(notifTitle)
                                        .setContentText(messages[0])
                                        .setSubText(messages[1])
                                        .setLargeIcon(drawableToBitmap(context.getDrawable(R.drawable.ic_launcher)))
                                        .setContentIntent(pIntent)
                                        .setAutoCancel(true)
                                        .setStyle(style)
                                        .setSmallIcon(R.drawable.notification_icon);
                            }
                            else
                            {
                                nb = new android.support.v4.app.NotificationCompat.Builder(context)
                                        .setContentTitle(notifTitle)
                                        .setContentText(messages[0])
                                        .setSubText(messages[1])
                                        .setLargeIcon(drawableToBitmap(context.getResources().getDrawable(R.drawable.ic_launcher)))
                                        .setContentIntent(pIntent)
                                        .setAutoCancel(true)
                                        .setStyle(style)
                                        .setSmallIcon(R.drawable.notification_icon);
                            }
                        } else {
                            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
                                nb = new android.support.v4.app.NotificationCompat.Builder(context)
                                        .setContentTitle(notifTitle)
                                        .setContentText(messages[0])
                                        .setLargeIcon(drawableToBitmap(context.getDrawable(R.drawable.ic_launcher)))
                                        .setContentIntent(pIntent)
                                        .setAutoCancel(true)
                                        .setStyle(style)
                                        .setSmallIcon(R.drawable.notification_icon);
                            }
                            else
                            {
                                nb = new android.support.v4.app.NotificationCompat.Builder(context)
                                        .setContentTitle(notifTitle)
                                        .setContentText(messages[0])
                                        .setLargeIcon(drawableToBitmap(context.getResources().getDrawable(R.drawable.ic_launcher)))
                                        .setContentIntent(pIntent)
                                        .setAutoCancel(true)
                                        .setStyle(style)
                                        .setSmallIcon(R.drawable.notification_icon);
                            }
                        }

                        try {
                            if (extras.containsKey("wzrk_sound")) {
                                Object o = extras.get("wzrk_sound");
                                if ((o instanceof String && o.equals("true")) || (o instanceof Boolean && (Boolean) o)) {
                                    Uri defaultSoundUri = RingtoneManager.getDefaultUri(RingtoneManager.TYPE_NOTIFICATION);
                                    nb.setSound(defaultSoundUri);
                                }
                            }
                        } catch (Throwable t) {
                            //   Logger.error("Could not process sound parameter", t);
                        }

                        Notification n = nb.build();

                        NotificationManager notificationManager =
                                (NotificationManager) context.getSystemService(Context.NOTIFICATION_SERVICE);

                        notificationManager.notify((int) (Math.random() * 100), n);
                    } catch (Throwable t) {
                        // Occurs if the notification image was null
                        // Let's return, as we couldn't get a handle on the app's icon
                        // Some devices throw a PackageManager* exception too
                        // Logger.error("Couldn't render notification!", t);
                    }
                }
            });
        } catch (Throwable t) {
            //Logger.error("Failed to process GCM notification", t);
        }
    }


    public Bitmap drawableToBitmap(Drawable drawable)
            throws NullPointerException, PackageManager.NameNotFoundException {
        if (drawable instanceof BitmapDrawable) {
            return ((BitmapDrawable) drawable).getBitmap();
        }

        Bitmap bitmap = Bitmap.createBitmap(drawable.getIntrinsicWidth(),
                drawable.getIntrinsicHeight(), Bitmap.Config.ARGB_8888);
        Canvas canvas = new Canvas(bitmap);
        drawable.setBounds(0, 0, canvas.getWidth(), canvas.getHeight());
        drawable.draw(canvas);

        return bitmap;
    }


    static void postAsyncSafely(final String name, final Runnable runnable) {
        ExecutorService es = Executors.newFixedThreadPool(1);
        try {
            es.submit(new Runnable() {
                @Override
                public void run() {
                    try {
                        //  Logger.logFine("Executor service: Starting task - " + name);
                        final long start = System.currentTimeMillis();
                        runnable.run();
                        final long time = System.currentTimeMillis() - start;
                        // Logger.logFine("Executor service: Task completed successfully in " + time + "ms (" + name + ")");
                    } catch (Throwable t) {
                        // Logger.logFine("Executor service: Failed to complete the scheduled task", t);
                    }
                }
            });
        } catch (Throwable t) {
            // Logger.logFine("Failed to submit task to the executor service", t);
        }
    }


    private int getNotificationIcon() {
        boolean useWhiteIcon = (android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.LOLLIPOP);
        return useWhiteIcon ? R.drawable.notification_icon : R.drawable.ic_launcher;
    }
}
