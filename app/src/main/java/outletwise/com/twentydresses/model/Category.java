package outletwise.com.twentydresses.model;

import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by User-PC on 02-09-2015.
 */
public class Category implements Parcelable {

    /**
     * id : 1
     * name : Apparels
     * subCats : [{"id":"4","name":"Tops","subCats":0},{"id":"5","name":"Dresses","subCats":0},{"id":"18","name":"Winter Wear","subCats":0},{"id":"23","name":"Denims Jeggings and Leggings","subCats":0},{"id":"24","name":"Shorts and Skirts","subCats":0},{"id":"25","name":"Pants and Palazzos","subCats":0},{"id":"28","name":"Jackets and Shrugs","subCats":0},{"id":"29","name":"Jumpsuits","subCats":0}]
     */

    private String id;
    private String name;
    @SerializedName("new")
    private String newCat;
    @SerializedName("subcat")
    private List<SubCat> subCats = new ArrayList<>();
    private boolean isDivider;
    private String icon;
    private String position;
    private String page;
    private String page_type;
    private String page_url;

    public Category() {

    }

    protected Category(Parcel in) {
        id = in.readString();
        name = in.readString();
        newCat = in.readString();
        subCats = in.createTypedArrayList(SubCat.CREATOR);
        isDivider = in.readByte() != 0;
        icon = in.readString();
        position = in.readString();
        page = in.readString();
        page_type = in.readString();
        page_url = in.readString();
    }

    public static final Creator<Category> CREATOR = new Creator<Category>() {
        @Override
        public Category createFromParcel(Parcel in) {
            return new Category(in);
        }

        @Override
        public Category[] newArray(int size) {
            return new Category[size];
        }
    };

    public void setId(String id) {
        this.id = id;
    }

    public void setName(String name) {
        this.name = name;
      /*  if (name.equalsIgnoreCase("Divider"))
            isDivider = true;*/
    }

    public void setNewCat(String newCat) {
        this.newCat = newCat;
    }

    public void setSubCats(List<SubCat> subCats) {
        this.subCats = subCats;
    }

    public String getId() {
        return id;
    }

    public String getName() {
        return name;
    }

    public String getNewCat() {
        return newCat;
    }

    public String getIcon() {
        return icon;
    }

    public void setIcon(String icon) {
        this.icon = icon;
    }

    public String getPosition() {
        return position;
    }

    public void setPosition(String position) {
        this.position = position;
    }

    public String getPage() {
        return page;
    }

    public void setPage(String page) {
        this.page = page;
    }

    public String getPage_type() {
        return page_type;
    }

    public void setPage_type(String page_type) {
        this.page_type = page_type;
    }

    public String getPage_url() {
        return page_url;
    }

    public void setPage_url(String page_url) {
        this.page_url = page_url;
    }

    public List<SubCat> getSubCats() {
        return subCats;
    }

    public void setSubCatsString(List<String> strings) {
        int i = 0;
        for (String item : strings) {
            SubCat subCat = new SubCat();
            subCat.setId(i++ + "");
            subCat.setName(item);
            this.subCats.add(subCat);
        }
    }

    public boolean isDivider() {
        return isDivider;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(id);
        dest.writeString(name);
        dest.writeString(newCat);
        dest.writeTypedList(subCats);
        dest.writeByte((byte) (isDivider ? 1 : 0));
        dest.writeString(icon);
        dest.writeString(position);
        dest.writeString(page);
        dest.writeString(page_type);
        dest.writeString(page_url);
    }

    public static class SubCat implements Parcelable {
        /**
         * id : 4
         * name : Tops
         * subCats : 0
         * new: true
         */

        private String id;
        private String name;
        @SerializedName("new")
        private String newSubCat;
        private String parent_page;
        private String child_position;
        private String page;

        public SubCat() {
        }

        public SubCat(Parcel in) {
            id = in.readString();
            name = in.readString();
            newSubCat = in.readString();
            parent_page = in.readString();
            child_position = in.readString();
            page = in.readString();
        }

        public static final Creator<SubCat> CREATOR = new Creator<SubCat>() {
            @Override
            public SubCat createFromParcel(Parcel in) {
                return new SubCat(in);
            }

            @Override
            public SubCat[] newArray(int size) {
                return new SubCat[size];
            }
        };

        public void setId(String id) {
            this.id = id;
        }

        public void setName(String name) {
            this.name = name;
        }

        public void setNew(String newSubCat) {
            this.newSubCat = newSubCat;
        }

        public String getId() {
            return id;
        }

        public String getName() {
            return name;
        }

        public String getNew() {
            return newSubCat;
        }

        public String getParent_page() {
            return parent_page;
        }

        public String getChild_position() {
            return child_position;
        }

        public void setParent_page(String parent_position) {
            this.parent_page = parent_position;
        }

        public void setChild_position(String child_position) {
            this.child_position = child_position;
        }

        public String getPage() {
            return page;
        }

        public void setPage(String page) {
            this.page = page;
        }

        @Override
        public String toString() {
            return name;
        }

        @Override
        public int describeContents() {
            return 0;
        }

        @Override
        public void writeToParcel(Parcel dest, int flags) {
            dest.writeString(id);
            dest.writeString(name);
            dest.writeString(newSubCat);
            dest.writeString(parent_page);
            dest.writeString(child_position);
            dest.writeString(page);
        }
    }

}
