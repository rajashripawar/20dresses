package outletwise.com.twentydresses.model;

/**
 * Created by User-PC on 05-11-2015.
 */
public class CartDeleteItemResponse {


    /**
     * status : Success
     * message : Successfully Deleted
     * data : {"product_id":"242","product_name":"Vintage wonder ring"}
     */

    private String status;
    private String message;
    /**
     * product_id : 242
     * product_name : Vintage wonder ring
     */

    private DataEntity data;

    public void setStatus(String status) {
        this.status = status;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public void setData(DataEntity data) {
        this.data = data;
    }

    public String getStatus() {
        return status;
    }

    public String getMessage() {
        return message;
    }

    public DataEntity getData() {
        return data;
    }

    public static class DataEntity {
        private String product_id;
        private String product_name;

        public void setProduct_id(String product_id) {
            this.product_id = product_id;
        }

        public void setProduct_name(String product_name) {
            this.product_name = product_name;
        }

        public String getProduct_id() {
            return product_id;
        }

        public String getProduct_name() {
            return product_name;
        }
    }
}
