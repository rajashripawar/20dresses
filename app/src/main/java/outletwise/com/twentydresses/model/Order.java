package outletwise.com.twentydresses.model;

import android.os.Parcel;
import android.os.Parcelable;

import java.util.List;

/**
 * Created by User-PC on 04-11-2015.
 */
public class Order implements Parcelable {

    /**
     * order_total : 2071.00
     * order_number : 20268
     * order_items : [{"item_image":"http://www.20dresses.com/uploads/product/cart_img/The-Black-Dahlia-High-Waist-Skirt-1.JPG","item_return_status":true},{"item_image":"http://www.20dresses.com/uploads/product/cart_img/Denim-Daze-Bellies-1.jpg","item_return_status":true}]
     * paymentm_mode : PREPAID
     * order_date : 31 Jan,2015
     * cancel_button : false
     * order_status : X
     * return_button : false
     */

    private String order_count;
    private String order_total;
    private String order_number;
    private String paymentm_mode;
    private String order_date;
    private boolean cancel_button;
    private String order_status;
    private String track_number;
    private String track_url;
    private String track_status;
    private String track_text;
    private boolean return_button;
    /**
     * item_image : http://www.20dresses.com/uploads/product/cart_img/The-Black-Dahlia-High-Waist-Skirt-1.JPG
     * item_return_status : true
     */

    private List<OrderItemsEntity> order_items;

    protected Order(Parcel in) {
        order_count = in.readString();
        order_total = in.readString();
        order_number = in.readString();
        paymentm_mode = in.readString();
        order_date = in.readString();
        cancel_button = in.readByte() != 0;
        order_status = in.readString();
        track_number = in.readString();
        track_url = in.readString();
        track_status = in.readString();
        track_text = in.readString();
        return_button = in.readByte() != 0;
    }


    public static final Creator<Order> CREATOR = new Creator<Order>() {
        @Override
        public Order createFromParcel(Parcel in) {
            return new Order(in);
        }

        @Override
        public Order[] newArray(int size) {
            return new Order[size];
        }
    };

    public void setOrder_count(String order_count) {
        this.order_count = order_count;
    }

    public void setOrder_total(String order_total) {
        this.order_total = order_total;
    }

    public void setOrder_number(String order_number) {
        this.order_number = order_number;
    }

    public void setPaymentm_mode(String paymentm_mode) {
        this.paymentm_mode = paymentm_mode;
    }

    public void setOrder_date(String order_date) {
        this.order_date = order_date;
    }

    public void setCancel_button(boolean cancel_button) {
        this.cancel_button = cancel_button;
    }

    public void setOrder_status(String order_status) {
        this.order_status = order_status;
    }

    public void setTracking_number(String tracking_number) {
        this.track_number = tracking_number;
    }

    public void setTracking_url(String tracking_url) {
        this.track_url = tracking_url;
    }

    public void setTrack_text(String track_text) {
        this.track_text = track_text;
    }

    public void setTrack_status(String track_status) {
        this.track_status = track_status;
    }


    public void setReturn_button(boolean return_button) {
        this.return_button = return_button;
    }

    public void setOrder_items(List<OrderItemsEntity> order_items) {
        this.order_items = order_items;
    }

    public String getOrder_count() {
        return order_count;
    }

    public String getOrder_total() {
        return order_total;
    }

    public String getOrder_number() {
        return order_number;
    }

    public String getPaymentm_mode() {
        return paymentm_mode;
    }

    public String getOrder_date() {
        return order_date;
    }

    public boolean isCancel_button() {
        return cancel_button;
    }

    public String getOrder_status() {
        return order_status;
    }

    public String getTracking_number() {
        return track_number;
    }

    public String getTracking_url() {
        return track_url;
    }

    public String getTrack_text() {
        return track_text;
    }

    public String getTrack_status() {
        return track_status;
    }

    public boolean isReturn_button() {
        return return_button;
    }

    public List<OrderItemsEntity> getOrder_items() {
        return order_items;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(order_count);
        dest.writeString(order_total);
        dest.writeString(order_number);
        dest.writeString(paymentm_mode);
        dest.writeString(order_date);
        dest.writeByte((byte) (cancel_button ? 1 : 0));
        dest.writeString(order_status);
        dest.writeString(track_number);
        dest.writeString(track_url);
        dest.writeByte((byte) (return_button ? 1 : 0));
    }

    public static class OrderItemsEntity {
        private String item_image;
        private boolean item_return_status;
        private boolean item_retrun;

        public void setItem_image(String item_image) {
            this.item_image = item_image;
        }

        public void setItem_return_status(boolean item_return_status) {
            this.item_return_status = item_return_status;
        }

        public void setItem_retrun(boolean item_retrun) {
            this.item_retrun = item_retrun;
        }


        public String getItem_image() {
            return item_image;
        }

        public boolean isItem_retrun() {
            return item_retrun;
        }
    }
}
