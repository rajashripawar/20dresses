package outletwise.com.twentydresses.model;

import java.util.ArrayList;

/**
 * Created by User-PC on 02-01-2016.
 */
public class CreditResponse {


    /**
     * credit_summary : [{"credits":99,"expire_date":"2016-05-24"}]
     * credits : 99
     * credit_description : <li>Invite friends and Earn Credits</li>
     <li>When the friend you invited registers with us, you get 150 Credits</li>
     <li>When the friend you invited makes her first purchase, you get 150 Credits</li>
     <li>Every time you make a purchase, you get credits worth 20% of your net purchase value</li>
     * credit_work : <li>Each credit is worth Rs 1</li>
     <li>You can redeem credits upto 50% of your Bag Value at the time of purchase</li>
     <li>Apply credits on the cart page</li>
     */

    private int credits;
    private String credit_description;
    private String credit_work;
    /**
     * credits : 99
     * expire_date : 2016-05-24
     */

    private ArrayList<CreditSummaryEntity> credit_summary;

    public void setCredits(int credits) {
        this.credits = credits;
    }

    public void setCredit_description(String credit_description) {
        this.credit_description = credit_description;
    }

    public void setCredit_work(String credit_work) {
        this.credit_work = credit_work;
    }

    public void setCredit_summary(ArrayList<CreditSummaryEntity> credit_summary) {
        this.credit_summary = credit_summary;
    }

    public int getCredits() {
        return credits;
    }

    public String getCredit_description() {
        return credit_description;
    }

    public String getCredit_work() {
        return credit_work;
    }

    public ArrayList<CreditSummaryEntity> getCredit_summary() {
        return credit_summary;
    }

    public static class CreditSummaryEntity {
        private int credits;
        private String expire_date;
        private String description;

        public void setCredits(int credits) {
            this.credits = credits;
        }

        public void setDescription(String description) {
            this.description = description;
        }

        public void setExpire_date(String expire_date) {
            this.expire_date = expire_date;
        }

        public int getCredits() {
            return credits;
        }

        public String getExpire_date() {
            return expire_date;
        }
        public String getDescription() {
            return description;
        }

    }
}
