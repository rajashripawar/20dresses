package outletwise.com.twentydresses.model;

import android.os.Parcel;
import android.os.Parcelable;
import android.text.TextUtils;

import com.google.gson.annotations.SerializedName;

import java.util.List;

import outletwise.com.twentydresses.utilities.Constants;

/**
 * Created by User-PC on 04-12-2015.
 */
public class StyleProfile implements Parcelable {


    /**
     * name : Adolf D'silva
     * image : http://graph.facebook.com/915133758554672/picture
     * user_attr : {"Shape":"Athletic","Size":"M (36-37)","Bottoms Size":"32 Inch","Height":"5.4","Shoes Size":"38","Face Color":"I'm Tan"}
     * style_attr : {"Bohemian":24,"Casual":11,"Classic":61,"Edgy":0,"Fashion Forward":4,"Feminine":0}
     */


    @SerializedName("user")
    private String userId;
    @SerializedName(Constants.AUTH_TOKEN)
    private String auth_token;
    @SerializedName("user_name")
    private String name;
    @SerializedName("user_picture")
    private String image;
    /**
     * Shape : Athletic
     * Size : M (36-37)
     * Bottoms Size : 32 Inch
     * Height : 5.4
     * Shoes Size : 38
     * Face Color : I'm Tan
     */
    private UserAttrEntity user_attr;
    private StyleAttrEntity style_attrEntity;
    private StyleAttr style_attr;
    private UserAttributesEntity user_attributes;
    /**
     * user_style : Classic
     */

    private String user_style;
    /**
     * style_message : You are a person of grace and tasteful style. You are classic, natural and romantic, all in one! You love a timeless, clean and tailored look and your style bends towards elegant and minimalistic.<br/><br/>
     * You are gifted with a natural athletic body and your fit and toned body is envied by lot of women. Lets work on adding curves at the right places and adding a touch of femininity to your look.<br/><br/> Star Stunners who match your fashion personality are: Cameron Diaz, Gwyneth Paltrow, Diya Mirza
     */

    private String user_style_message;
    private List<String> style_message;

    public StyleProfile() {

    }

    protected StyleProfile(Parcel in) {
        name = in.readString();
        image = in.readString();
        user_style = in.readString();
        // style_message = in.readString();
    }

    public static final Creator<StyleProfile> CREATOR = new Creator<StyleProfile>() {
        @Override
        public StyleProfile createFromParcel(Parcel in) {
            return new StyleProfile(in);
        }

        @Override
        public StyleProfile[] newArray(int size) {
            return new StyleProfile[size];
        }
    };

    public String getUserId() {
        return userId;
    }

    public void setUserId(String userId) {
        this.userId = userId;
    }

    public String getAuth_token() {
        return auth_token;
    }

    public void setAuth_token(String auth_token) {
        this.auth_token = auth_token;
    }

    public StyleAttr getStyle_attr() {
        return style_attr;
    }

    public void setStyle_attr(StyleAttr style_attr) {
        this.style_attr = style_attr;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getImage() {
        return image;
    }

    public void setImage(String image) {
        this.image = image;
    }

    public UserAttrEntity getUser_attr() {
        return user_attr;
    }

    public void setUser_attributes(UserAttributesEntity user_attributes) {
        this.user_attributes = user_attributes;
    }

    public void setUser_style(String user_style) {
        this.user_style = user_style;
    }

    public String getUser_style() {
        return user_style;
    }

    public List<String> getStyle_message() {
        return style_message;
    }

    public void setStyle_message(List<String> style_message) {
        this.style_message = style_message;
    }

    public String getStyleMessageAsString() {
        return TextUtils.join(". ", style_message);
    }

    public void setUser_style_message(String user_style_message) {
        this.user_style_message = user_style_message;
    }

    public String getUser_style_message() {
        return user_style_message;
    }

    public UserAttributesEntity getUser_attributes() {
        return user_attributes;
    }

    public void setUser_attr(UserAttrEntity user_attr) {
        this.user_attr = user_attr;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(name);
        dest.writeString(image);
        dest.writeParcelable(user_attr, flags);
        dest.writeParcelable(style_attr, flags);
        dest.writeString(user_style);
        // dest.writeString(style_message);
    }

    public void setStyle_attrEntity(StyleAttrEntity style_attrEntity) {
        this.style_attrEntity = style_attrEntity;
    }

    public static class StyleAttr implements Parcelable {

        /**
         * Bohemian : 24
         * Casual : 11
         * Classic : 61
         * Edgy : 0
         * FashionForward : 4
         * Feminine : 0
         */

        private int Bohemian;
        private int Casual;
        private int Classic;
        private int Edgy;
        @SerializedName("Fashion Forward")
        private int FashionForward;
        private int Feminine;

        protected StyleAttr(Parcel in) {
            Bohemian = in.readInt();
            Casual = in.readInt();
            Classic = in.readInt();
            Edgy = in.readInt();
            FashionForward = in.readInt();
            Feminine = in.readInt();
        }

        public static final Creator<StyleAttr> CREATOR = new Creator<StyleAttr>() {
            @Override
            public StyleAttr createFromParcel(Parcel in) {
                return new StyleAttr(in);
            }

            @Override
            public StyleAttr[] newArray(int size) {
                return new StyleAttr[size];
            }
        };

        public void setBohemian(int Bohemian) {
            this.Bohemian = Bohemian;
        }

        public void setCasual(int Casual) {
            this.Casual = Casual;
        }

        public void setClassic(int Classic) {
            this.Classic = Classic;
        }

        public void setEdgy(int Edgy) {
            this.Edgy = Edgy;
        }

        public void setFashionForward(int FashionForward) {
            this.FashionForward = FashionForward;
        }

        public void setFeminine(int Feminine) {
            this.Feminine = Feminine;
        }

        public int getBohemian() {
            return Bohemian;
        }

        public int getCasual() {
            return Casual;
        }

        public int getClassic() {
            return Classic;
        }

        public int getEdgy() {
            return Edgy;
        }

        public int getFashionForward() {
            return FashionForward;
        }

        public int getFeminine() {
            return Feminine;
        }

        @Override
        public int describeContents() {
            return 0;
        }

        @Override
        public void writeToParcel(Parcel dest, int flags) {
            dest.writeInt(Bohemian);
            dest.writeInt(Casual);
            dest.writeInt(Classic);
            dest.writeInt(Edgy);
            dest.writeInt(FashionForward);
            dest.writeInt(Feminine);
        }
    }
}
