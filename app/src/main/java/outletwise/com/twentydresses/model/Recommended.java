package outletwise.com.twentydresses.model;

import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by User-PC on 06-08-2015.
 */
public class Recommended extends GenericResponse{

    /**
     * tips : [{"total":20,"tip_text":"Draw attention to your upper body","thumbnails":["Boulevard-Dreams-Dress-4.jpg","Deep-Blue-Sea-Shift-Dress-1.JPG","Courtney-Love-Dress-1.JPG"],"tip_id":"1"},{"total":20,"tip_text":"Slimming your lower body","thumbnails":["Boulevard-Dreams-Dress-4.jpg","Deep-Blue-Sea-Shift-Dress-1.JPG","Courtney-Love-Dress-1.JPG"],"tip_id":"2"},{"total":20,"tip_text":"Accessories for your Body Type","thumbnails":["Struck-By-Lightening-Necklace-1.jpg","Army-Of-Arrows-Necklace-1.jpg","Braid-Parade-Necklace-1.jpg"],"tip_id":"3"},{"total":20,"tip_text":"Shoes for your Body Type","thumbnails":["Black-Pointy-Toes-Peep-Toes-1.JPG","Brown-Stud-Alert-Heels-1.JPG","Grey-Stud-Alert-Heels-1.JPG","Gold-The-Sleek-Life-Heels-1.JPG"],"tip_id":"4"},{"total":20,"tip_text":"Enhance yor neckline","thumbnails":["Boulevard-Dreams-Dress-4.jpg","Deep-Blue-Sea-Shift-Dress-1.JPG","Courtney-Love-Dress-1.JPG","Pebblestone-Penelope-Dress-1.JPG"],"tip_id":"5"},{"total":20,"tip_text":"Create the illusion of a waist","thumbnails":["Blurred-Lines-Mesh-Top-1.JPG","Lost-Stories-Formal-Top-1.JPG","Stripe-Stunner-Top-1.JPG","Beige-Formally-Yours-Top-1.JPG"],"tip_id":"6"}]
     * status : Success
     */

    /**
     * total : 20
     * tip_text : Draw attention to your upper body
     * thumbnails : ["Boulevard-Dreams-Dress-4.jpg","Deep-Blue-Sea-Shift-Dress-1.JPG","Courtney-Love-Dress-1.JPG"]
     * tip_id : 1
     */

    @SerializedName("data")
    private ArrayList<Tip> tips;

    private ArrayList<Tip> lock_data;

    public void setTips(ArrayList<Tip> tips) {
        this.tips = tips;
    }

    public void setLock_data(ArrayList<Tip> lock_data) {
        this.lock_data = lock_data;
    }


    public ArrayList<Tip> getTips() {
        return tips;
    }

    public ArrayList<Tip> getLock_data() {
        return lock_data;
    }

    public static class Tip {
        private int total;
        private String tip_text;
        private String tip_id;
        private ArrayList<String> thumbnails;
        private String que_id;
        private String tip_lock_image;

        public void setTotal(int total) {
            this.total = total;
        }

        public void setTip_text(String tip_text) {
            this.tip_text = tip_text;
        }

        public void setTip_id(String tip_id) {
            this.tip_id = tip_id;
        }

        public void setQue_id(String que_id) {
            this.que_id = que_id;
        }

        public void setTip_lock_image(String tip_lock_image) {
            this.tip_lock_image = tip_lock_image;
        }

        public void setThumbnails(ArrayList<String> thumbnails) {
            this.thumbnails = thumbnails;
        }

        public int getTotal() {
            return total;
        }

        public String getTip_text() {
            return tip_text;
        }

        public String getTip_id() {
            return tip_id;
        }

        public ArrayList<String> getThumbnails() {
            return thumbnails;
        }

        public String getQue_id() {
            return que_id;
        }

        public String getTip_lock_image() {
            return tip_lock_image;
        }
    }
}
