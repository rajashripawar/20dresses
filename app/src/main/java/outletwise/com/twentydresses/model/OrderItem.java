package outletwise.com.twentydresses.model;

import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by User-PC on 04-11-2015.
 */
public class OrderItem implements Parcelable {


    /**
     * order_number : 49063
     * order_shipping_charges : 0
     * order_total : 834.00
     * order_wallet : 834
     * product_total : 834
     * order_status : Confirmed
     * cancel_button : false
     * return_button : false
     * paymentm_mode : PREPAID
     * order_items : [{"item_product_id":"4943","item_name":"The Edge Of Funk Heels","item_image":"http://www.20dresses.com/uploads/product/cart_img/The-Edge-Of-Funk-Heels-1.jpg","item_quantity":"1","item_mrp":"2195.00","item_discount":40,"item_price":834,"item_savings":44,"item_status":"Confirmed","item_size":"39"}]
     * order_date : 25 Feb, 2016
     * delivery_address : Ishita Sanghani<br/>MGM medical college,N6,cidco,aurangabad<br/>Aurangabad,Maharashtra<br/>India-431003<br/>Mobile: 9028874667
     */

    private String order_number;
    private int order_shipping_charges;
    private String order_total;
    private String order_wallet;
    private int product_total;
    private String order_status;
    private boolean cancel_button;
    private boolean return_button;
    @SerializedName("payment_mode")
    private String paymentm_mode;
    private String order_date;
    private String delivery_address;
    private boolean show_contact_details;
    /**
     * item_product_id : 4943
     * item_name : The Edge Of Funk Heels
     * item_image : http://www.20dresses.com/uploads/product/cart_img/The-Edge-Of-Funk-Heels-1.jpg
     * item_quantity : 1
     * item_mrp : 2195.00
     * item_discount : 40
     * item_price : 834
     * item_savings : 44
     * item_status : Confirmed
     * item_size : 39
     */

    private List<OrderItemsEntity> order_items;

    public void setOrder_number(String order_number) {
        this.order_number = order_number;
    }

    public void setOrder_shipping_charges(int order_shipping_charges) {
        this.order_shipping_charges = order_shipping_charges;
    }

    public void setOrder_total(String order_total) {
        this.order_total = order_total;
    }

    public void setOrder_wallet(String order_wallet) {
        this.order_wallet = order_wallet;
    }

    public void setProduct_total(int product_total) {
        this.product_total = product_total;
    }

    public void setOrder_status(String order_status) {
        this.order_status = order_status;
    }

    public void setCancel_button(boolean cancel_button) {
        this.cancel_button = cancel_button;
    }

    public void setReturn_button(boolean return_button) {
        this.return_button = return_button;
    }

    public void setPaymentm_mode(String paymentm_mode) {
        this.paymentm_mode = paymentm_mode;
    }

    public void setOrder_date(String order_date) {
        this.order_date = order_date;
    }

    public void setDelivery_address(String delivery_address) {
        this.delivery_address = delivery_address;
    }

    public void setShow_contact_details(boolean show_contact_details) {
        this.show_contact_details = show_contact_details;
    }

    public void setOrder_items(List<OrderItemsEntity> order_items) {
        this.order_items = order_items;
    }

    public String getOrder_number() {
        return order_number;
    }

    public int getOrder_shipping_charges() {
        return order_shipping_charges;
    }

    public String getOrder_total() {
        return order_total;
    }

    public String getOrder_wallet() {
        return order_wallet;
    }

    public int getProduct_total() {
        return product_total;
    }

    public String getOrder_status() {
        return order_status;
    }

    public boolean isCancel_button() {
        return cancel_button;
    }

    public boolean isReturn_button() {
        return return_button;
    }

    public String getPaymentm_mode() {
        return paymentm_mode;
    }

    public String getOrder_date() {
        return order_date;
    }

    public String getDelivery_address() {
        return delivery_address;
    }

    public boolean getShow_contact_details() {
        return show_contact_details;
    }

    public List<OrderItemsEntity> getOrder_items() {
        return order_items;
    }

    public static class OrderItemsEntity {
        private String item_id;
        private String item_product_id;
        private String item_name;
        private String item_image;
        private String item_quantity;
        private String item_mrp;
        private int item_discount;
        private double item_price;
        private int item_savings;
        private String item_status;
        private String item_size;
        private double item_refund_amount;
        private boolean item_retrun;


        public void setItem_retrun(boolean item_retrun) {
            this.item_retrun = item_retrun;
        }

        public boolean isItem_retrun() {
            return item_retrun;
        }

        public void setItem_id(String item_id) {
            this.item_id = item_id;
        }

        public void setItem_product_id(String item_product_id) {
            this.item_product_id = item_product_id;
        }

        public void setItem_name(String item_name) {
            this.item_name = item_name;
        }

        public void setItem_image(String item_image) {
            this.item_image = item_image;
        }

        public void setItem_quantity(String item_quantity) {
            this.item_quantity = item_quantity;
        }

        public void setItem_mrp(String item_mrp) {
            this.item_mrp = item_mrp;
        }

        public void setItem_discount(int item_discount) {
            this.item_discount = item_discount;
        }

        public void setItem_price(double item_price) {
            this.item_price = item_price;
        }

        public void setItem_savings(int item_savings) {
            this.item_savings = item_savings;
        }

        public void setItem_status(String item_status) {
            this.item_status = item_status;
        }

        public void setItem_size(String item_size) {
            this.item_size = item_size;
        }

        public String getItem_id() {
            return item_id;
        }

        public String getItem_product_id() {
            return item_product_id;
        }

        public String getItem_name() {
            return item_name;
        }

        public String getItem_image() {
            return item_image;
        }

        public String getItem_quantity() {
            return item_quantity;
        }

        public String getItem_mrp() {
            return item_mrp;
        }

        public int getItem_discount() {
            return item_discount;
        }

        public double getItem_price() {
            return item_price;
        }

        public int getItem_savings() {
            return item_savings;
        }

        public String getItem_status() {
            return item_status;
        }

        public String getItem_size() {
            return item_size;
        }

        public void setItem_refund_amount(double item_refund_amount) {
            this.item_refund_amount = item_refund_amount;
        }

        public double getItem_refund_amount() {
            return item_refund_amount;
        }
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(this.order_number);
        dest.writeInt(this.order_shipping_charges);
        dest.writeString(this.order_total);
        dest.writeString(this.order_wallet);
        dest.writeInt(this.product_total);
        dest.writeString(this.order_status);
        dest.writeByte(cancel_button ? (byte) 1 : (byte) 0);
        dest.writeByte(return_button ? (byte) 1 : (byte) 0);
        dest.writeString(this.paymentm_mode);
        dest.writeString(this.order_date);
        dest.writeString(this.delivery_address);
        dest.writeList(this.order_items);

    }

    public OrderItem() {
    }

    protected OrderItem(Parcel in) {
        this.order_number = in.readString();
        this.order_shipping_charges = in.readInt();
        this.order_total = in.readString();
        this.order_wallet = in.readString();
        this.product_total = in.readInt();
        this.order_status = in.readString();
        this.cancel_button = in.readByte() != 0;
        this.return_button = in.readByte() != 0;
        this.paymentm_mode = in.readString();
        this.order_date = in.readString();
        this.delivery_address = in.readString();
        this.order_items = new ArrayList<OrderItemsEntity>();
        in.readList(this.order_items, List.class.getClassLoader());
    }

    public static final Parcelable.Creator<OrderItem> CREATOR = new Parcelable.Creator<OrderItem>() {
        public OrderItem createFromParcel(Parcel source) {
            return new OrderItem(source);
        }

        public OrderItem[] newArray(int size) {
            return new OrderItem[size];
        }
    };
}