package outletwise.com.twentydresses.model;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Created by User-PC on 21-03-2016.
 */
public class HelpFaq {


    /**
     * Cancel_Order : Changed your mind about an order? Please go on to the My Orders page to cancel your order.
     * Return : Not Happy with the Product? No Worries! We have a 20 day hassle free, no questions asked return policy. Just go on the My Order page and you will be able to return the product without any delays.
     * Refund : Your money is safe with us in your wallet. Want a refund? Please go on to the My Wallet page to ask for a full Refund.
     * Track_Order : You can now track your order directly from our My Order page. Go on to the page and you will know exactly where your shipment has reached and when will it be delivered to you.
     * Exchange : Does the product not fit you right? Don't Worry! You can now get it exchanged by going on to the My Orders page without any hassles.
     */

    public ArrayList<HelpBean> help;
    /**
     * What_is_your_Return_or_Refund_Policy? : A) We have a 100% no question asked return or refund policy. If the order you receive is not upto your satisfaction for whatever reason, you need to inform us through customer service or email with in 20 days of receipt of your product. You can initiate return, exchange or refund though this App itself or by informing 20D customer service. Your account will be added with 20D wallet instantly which you can use to buy something else. Alternatively, once the returned product is received by us at our warehouse, we will process your refund. Refund would be subject to our quality check. For hassle free refund/return/exchange, please ensure that the product that you are returning has not been used or worn (other than trials), not stained or damaged. The decision of 20Dresses Team would be final with respect to the same.
     * Do_you_have_COD-(Cash_On_Delivery)_facility? : A) Yes, we do have COD facility in most of the postal pincodes in India. All product pages have a section where you can type your pincode and check if COD is available in your pincode or not and get estimated timelines for delivery as well.<br/>
     * 3: Do you have reverse pickup facility in case I wish to return the product?<br/>
     * A) Yes, we have reverse pickup facility in all the pincodes where we have COD facility. All product pages have a section where you can type your pincode and check if COD is available in your pincode.
     * Do_you_charge_for_Shipping? : A) We ship all orders above Rs 495 free of cost. We charge shipping costs of flat Rs 75 for all orders below Rs 495.
     * Is_it_safe_to_transact_with_my_Credit/Debitcard_ on_20D? : A) Yes, shopping at 20D is 100% safe. Credit & Debit card payments are processed through various Payment Gateways viz. Citrus Payment Solutions and PayTM that interface with various banks. Citrus & PayTM use latest encryption technology to protect your information. They encrypt your information so that it cannot be read as it travels over the Internet. Furthermore, the Payment Gateways also asks you to enter the CVV (Credit Verification Value) number on your credit card. This is a 3 digit number which follows your credit card number and is given at the back of the card to ensure that the person entering the card number has the physical plastic card with him/her. You can be assured that 20D offers you the highest standards of security currently available on the net so as to ensure that your shopping experience is private, safe and secure.
     * Are_my_Banking_Information_safe_with_20D? : A) 20D would require your Bank account details to process your refunds incase the orders were placed using COD facility. 20D will only ask you basic information that are required for the same and not any more. The information are never shared with anyone except the finance team. You can be assured that 20D offers you the highest standards of security currently available on the net so as to ensure that your shopping experience is private, safe and secure.
     * What_payment_options_does_20D_support?_Does_20D_accept_international_credit_cards? : A) We accept all Visa and Mastercard Credit Cards & Debit Cards. We also accept payments through secure Internet Banking facilities with large reputed banks including ICICI Bank, HDFC Bank, Citibank, State Bank of India, Union Bank of India, Axis Bank, Deutsche Bank and Yes Bank.
     * We also provide Cash on Delivery (COD) option in select Pincodes.<br/><br/>
     * <p/>
     * No, at this moment we are do not accept international credit cards.
     */

    public ArrayList<HashMap<String, String>> faq;

    public static class HelpBean {
        private String Cancel_Order;
        private String Return;
        private String Refund;
        private String Track_Order;
        private String Exchange;

        public String getCancel_Order() {
            return Cancel_Order;
        }

        public void setCancel_Order(String Cancel_Order) {
            this.Cancel_Order = Cancel_Order;
        }

        public String getReturn() {
            return Return;
        }

        public void setReturn(String Return) {
            this.Return = Return;
        }

        public String getRefund() {
            return Refund;
        }

        public void setRefund(String Refund) {
            this.Refund = Refund;
        }

        public String getTrack_Order() {
            return Track_Order;
        }

        public void setTrack_Order(String Track_Order) {
            this.Track_Order = Track_Order;
        }

        public String getExchange() {
            return Exchange;
        }

        public void setExchange(String Exchange) {
            this.Exchange = Exchange;
        }
    }
}
