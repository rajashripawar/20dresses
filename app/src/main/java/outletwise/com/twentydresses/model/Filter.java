package outletwise.com.twentydresses.model;

import android.os.Parcel;
import android.os.Parcelable;

import java.util.ArrayList;
import java.util.HashMap;

/**
 * Created by User-PC on 26-11-2015.
 */
public class Filter implements Parcelable {

    /**
     * name : categories
     * parameter : category
     * values : [{"category":"4","category_name":"Tops"},{"category":"5","category_name":"Dresses"},{"category":"6","category_name":"Bottoms"},{"category":"18","category_name":"Winter Wear"},{"category":"20","category_name":"Shrugs"},{"category":"23","category_name":"Denims Jeggings and Leggings"},{"category":"24","category_name":"Shorts and Skirts"},{"category":"25","category_name":"Pants and Palazzos"},{"category":"27","category_name":"Jackets"},{"category":"28","category_name":"Jackets and Shrugs"},{"category":"29","category_name":"Jumpsuits"},{"category":"45","category_name":"T-Shirts"}]
     */

    private CategoriesEntity categories;
    /**
     * name : Sizes
     * parameter : attr_id
     * values : [{"attr_id":"1","attr_value":"XS"},{"attr_id":"2","attr_value":"S"},{"attr_id":"3","attr_value":"M"},{"attr_id":"4","attr_value":"L"},{"attr_id":"5","attr_value":"XL"},{"attr_id":"65","attr_value":"Default Size"},{"attr_id":"209","attr_value":"XXL"}]
     */

    private SizesEntity sizes;
    /**
     * name : Colours
     * parameter : color_id
     * values : [{"color_id":"1","color_name":"Red","color_code":"#C00707"},{"color_id":"2","color_name":"Blue","color_code":"#1664c4"},{"color_id":"3","color_name":"Black","color_code":"#000000"},{"color_id":"4","color_name":"White","color_code":"#ffffff"},{"color_id":"5","color_name":"Green","color_code":"#6fcc14"},{"color_id":"6","color_name":"Yellow","color_code":"#FFFC01"},{"color_id":"7","color_name":"Brown","color_code":"#953D3C"},{"color_id":"8","color_name":"Pink","color_code":"#FF1CAE"},{"color_id":"9","color_name":"Grey","color_code":"#BFBFBF"},{"color_id":"10","color_name":"Orange","color_code":"#FE7700"},{"color_id":"11","color_name":"Purple","color_code":"#5D00DC"},{"color_id":"12","color_name":"Cream","color_code":"#F8F0E0"},{"color_id":"13","color_name":"Multicolor","color_code":""},{"color_id":"14","color_name":"Gold","color_code":"#ffd700"},{"color_id":"15","color_name":"Silver","color_code":"#c0c0c0"},{"color_id":"16","color_name":"Transparent","color_code":"#ffffff"}]
     */

    private ColorsEntity colors;
    /**
     * name : Discount
     * parameter : sale
     * values : [{"discount":"Non Discounted Items","sale":0},{"discount":"Minimum 10%","sale":10},{"discount":"Minimum 20%","sale":20},{"discount":"Minimum 30%","sale":30},{"discount":"Minimum 40%","sale":40},{"discount":"Minimum 50%","sale":50},{"discount":"Minimum 60%","sale":60},{"discount":"Minimum 70%","sale":70}]
     */

    private DiscountsEntity discounts;

    protected Filter(Parcel in) {
    }

    public static final Creator<Filter> CREATOR = new Creator<Filter>() {
        @Override
        public Filter createFromParcel(Parcel in) {
            return new Filter(in);
        }

        @Override
        public Filter[] newArray(int size) {
            return new Filter[size];
        }
    };

    public void setCategories(CategoriesEntity categories) {
        this.categories = categories;
    }

    public void setSizes(SizesEntity sizes) {
        this.sizes = sizes;
    }

    public void setColors(ColorsEntity colors) {
        this.colors = colors;
    }

    public void setDiscounts(DiscountsEntity discounts) {
        this.discounts = discounts;
    }

    public CategoriesEntity getCategories() {
        return categories;
    }

    public SizesEntity getSizes() {
        return sizes;
    }

    public ColorsEntity getColors() {
        return colors;
    }

    public DiscountsEntity getDiscounts() {
        return discounts;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
    }

    public static class CategoriesEntity {
        private String name;
        private String parameter;

        /**
         * category : 4
         * category_name : Tops
         */
        private HashMap<String, ValuesEntity> appliedFilters = new HashMap<>();

        public int getAppliedFiltersSize() {
            return appliedFilters.size();
        }

        public ValuesEntity getAppliedFilter(String key) {
            return appliedFilters.get(key);
        }

        public void removeFilter(String key) {
            appliedFilters.remove(key);
        }

        public void addFilters(ValuesEntity filter) {
            this.appliedFilters.put(filter.getCategory(), filter);
        }

        private ArrayList<ValuesEntity> values;

        public void setName(String name) {
            this.name = name;
        }

        public void setParameter(String parameter) {
            this.parameter = parameter;
        }

        public void setValues(ArrayList<ValuesEntity> values) {
            this.values = values;
        }

        public String getName() {
            return name;
        }

        public String getParameter() {
            return parameter;
        }

        public ArrayList<ValuesEntity> getValues() {
            return values;
        }

        public static class ValuesEntity {
            private String category;
            private String category_name;
            private String category_count;


            public void setCategory(String category) {
                this.category = category;
            }

            public void setCategory_name(String category_name) {
                this.category_name = category_name;
            }

            public String getCategory() {
                return category;
            }

            public String getCategory_name() {
                return category_name;
            }

            public void setCategory_count(String category_count) {
                this.category_count = category_count;
            }

            public String getCategory_count() {
                return category_count;
            }
        }

    }

    public static class SizesEntity {
        private String name;
        private String parameter;
        /**
         * attr_id : 1
         * attr_value : XS
         */
        private HashMap<String, ValuesEntity> appliedFilters = new HashMap<>();

        public int getAppliedFiltersSize() {
            return appliedFilters.size();
        }

        public ValuesEntity getAppliedFilter(String key) {
            return appliedFilters.get(key);
        }

        public void removeFilter(String key) {
            appliedFilters.remove(key);
        }

        public void addFilters(ValuesEntity filter) {
            this.appliedFilters.put(filter.getAttr_id(), filter);
        }

        private ArrayList<ValuesEntity> values;

        public void setName(String name) {
            this.name = name;
        }

        public void setParameter(String parameter) {
            this.parameter = parameter;
        }

        public void setValues(ArrayList<ValuesEntity> values) {
            this.values = values;
        }

        public String getName() {
            return name;
        }

        public String getParameter() {
            return parameter;
        }

        public ArrayList<ValuesEntity> getValues() {
            return values;
        }


        public static class ValuesEntity {
            private String attr_id;
            private String attr_value;
            private String attr_count;

            public void setAttr_id(String attr_id) {
                this.attr_id = attr_id;
            }

            public void setAttr_value(String attr_value) {
                this.attr_value = attr_value;
            }

            public void setAttr_count(String attr_count) {
                this.attr_count = attr_count;
            }

            public String getAttr_id() {
                return attr_id;
            }

            public String getAttr_value() {
                return attr_value;
            }

            public String getAttr_count() {
                return attr_count;
            }

        }
    }

    public static class ColorsEntity {
        private String name;
        private String parameter;

        /**
         * color_id : 1
         * color_name : Red
         * color_code : #C00707
         */
        private HashMap<String, ValuesEntity> appliedFilters = new HashMap<>();

        public int getAppliedFiltersSize() {
            return appliedFilters.size();
        }

        public ValuesEntity getAppliedFilter(String key) {
            return appliedFilters.get(key);
        }

        public void removeFilter(String key) {
            appliedFilters.remove(key);
        }

        public void addFilters(ValuesEntity filter) {
            this.appliedFilters.put(filter.getColor_id(), filter);
        }

        private ArrayList<ValuesEntity> values;

        public void setName(String name) {
            this.name = name;
        }

        public void setParameter(String parameter) {
            this.parameter = parameter;
        }

        public void setValues(ArrayList<ValuesEntity> values) {
            this.values = values;
        }

        public String getName() {
            return name;
        }

        public String getParameter() {
            return parameter;
        }

        public ArrayList<ValuesEntity> getValues() {
            return values;
        }

        public static class ValuesEntity {
            private String color_id;
            private String color_name;
            private String color_code;
            private String color_count;

            public void setColor_id(String color_id) {
                this.color_id = color_id;
            }
            public void setColor_count(String color_count) {
                this.color_count = color_count;
            }

            public void setColor_name(String color_name) {
                this.color_name = color_name;
            }

            public void setColor_code(String color_code) {
                this.color_code = color_code;
            }

            public String getColor_id() {
                return color_id;
            }

            public String getColor_name() {
                return color_name;
            }

            public String getColor_code() {
                return color_code;
            }
            public String getColor_count() {
                return color_count;
            }
        }
    }

    public static class DiscountsEntity {
        private String name;
        private String parameter;
        /**
         * discount : Non Discounted Items
         * sale : 0
         */
      //  private HashMap<String, ValuesEntity> appliedFilters = new HashMap<>();
        ValuesEntity appliedFilters;

        public int getAppliedFiltersSize() {
            //return appliedFilters.size();
            if(appliedFilters == null)
            return 0;
            else
                return 1;
        }

        public ValuesEntity getAppliedFilter() {
            //return appliedFilters.get(key);
            return appliedFilters;
        }

        public void removeFilter() {
            appliedFilters = null;
        }

        public void addFilters(ValuesEntity filter) {
           // this.appliedFilters.put(filter.getDiscount(), filter);
            appliedFilters = filter;
        }

        private ArrayList<ValuesEntity> values;


        public void setName(String name) {
            this.name = name;
        }

        public void setParameter(String parameter) {
            this.parameter = parameter;
        }

        public void setValues(ArrayList<ValuesEntity> values) {
            this.values = values;
        }

        public String getName() {
            return name;
        }

        public String getParameter() {
            return parameter;
        }

        public ArrayList<ValuesEntity> getValues() {
            return values;
        }

        public static class ValuesEntity {
            private String discount;
            private String sale_count;
            private int sale;

            public void setDiscount(String discount) {
                this.discount = discount;
            }
            public void setDiscount_count(String discount_count) {
                this.sale_count = sale_count;
            }

            public void setSale(int sale) {
                this.sale = sale;
            }

            public String getDiscount() {
                return discount;
            }

            public String getDiscount_count() {
                return sale_count;
            }

            public int getSale() {
                return sale;
            }
        }
    }
}
