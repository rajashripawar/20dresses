package outletwise.com.twentydresses.model;

/**
 * Created by User-PC on 04-12-2015.
 */
public class StyleAttrEntity {
    private String maybe;
    private String like;

    public void setMaybe(String maybe) {
        this.maybe = maybe;
    }

    public void setLike(String like) {
        this.like = like;
    }

    public String getMaybe() {
        return maybe;
    }

    public String getLike() {
        return like;
    }
}
