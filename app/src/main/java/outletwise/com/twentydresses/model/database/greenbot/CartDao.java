package outletwise.com.twentydresses.model.database.greenbot;

import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteStatement;

import de.greenrobot.dao.AbstractDao;
import de.greenrobot.dao.Property;
import de.greenrobot.dao.internal.DaoConfig;

import outletwise.com.twentydresses.model.database.greenbot.Cart;

// THIS CODE IS GENERATED BY greenDAO, DO NOT EDIT.
/** 
 * DAO for table CART.
*/
public class CartDao extends AbstractDao<Cart, Long> {

    public static final String TABLENAME = "CART";

    /**
     * Properties of entity Cart.<br/>
     * Can be used for QueryBuilder and for referencing column names.
    */
    public static class Properties {
        public final static Property Id = new Property(0, Long.class, "id", true, "_id");
        public final static Property Cart_item_id = new Property(1, String.class, "cart_item_id", false, "CART_ITEM_ID");
        public final static Property Product_id = new Property(2, String.class, "product_id", false, "PRODUCT_ID");
        public final static Property Product_name = new Property(3, String.class, "product_name", false, "PRODUCT_NAME");
        public final static Property Product_attr_id = new Property(4, String.class, "product_attr_id", false, "PRODUCT_ATTR_ID");
        public final static Property Product_qty = new Property(5, String.class, "product_qty", false, "PRODUCT_QTY");
        public final static Property Product_size = new Property(6, String.class, "product_size", false, "PRODUCT_SIZE");
        public final static Property Product_discount = new Property(7, String.class, "product_discount", false, "PRODUCT_DISCOUNT");
        public final static Property Product_mrp = new Property(8, String.class, "product_mrp", false, "PRODUCT_MRP");
        public final static Property Product_img = new Property(9, String.class, "product_img", false, "PRODUCT_IMG");
        public final static Property Product_max_qty = new Property(10, String.class, "product_max_qty", false, "PRODUCT_MAX_QTY");
        public final static Property Product_price = new Property(11, String.class, "product_price", false, "PRODUCT_PRICE");
        public final static Property Product_savings = new Property(12, Integer.class, "product_savings", false, "PRODUCT_SAVINGS");
    };


    public CartDao(DaoConfig config) {
        super(config);
    }
    
    public CartDao(DaoConfig config, DaoSession daoSession) {
        super(config, daoSession);
    }

    /** Creates the underlying database table. */
    public static void createTable(SQLiteDatabase db, boolean ifNotExists) {
        String constraint = ifNotExists? "IF NOT EXISTS ": "";
        db.execSQL("CREATE TABLE " + constraint + "'CART' (" + //
                "'_id' INTEGER PRIMARY KEY ," + // 0: id
                "'CART_ITEM_ID' TEXT," + // 1: cart_item_id
                "'PRODUCT_ID' TEXT," + // 2: product_id
                "'PRODUCT_NAME' TEXT," + // 3: product_name
                "'PRODUCT_ATTR_ID' TEXT," + // 4: product_attr_id
                "'PRODUCT_QTY' TEXT," + // 5: product_qty
                "'PRODUCT_SIZE' TEXT," + // 6: product_size
                "'PRODUCT_DISCOUNT' TEXT," + // 7: product_discount
                "'PRODUCT_MRP' TEXT," + // 8: product_mrp
                "'PRODUCT_IMG' TEXT," + // 9: product_img
                "'PRODUCT_MAX_QTY' TEXT," + // 10: product_max_qty
                "'PRODUCT_PRICE' TEXT," + // 11: product_price
                "'PRODUCT_SAVINGS' INTEGER);"); // 12: product_savings
    }

    /** Drops the underlying database table. */
    public static void dropTable(SQLiteDatabase db, boolean ifExists) {
        String sql = "DROP TABLE " + (ifExists ? "IF EXISTS " : "") + "'CART'";
        db.execSQL(sql);
    }

    /** @inheritdoc */
    @Override
    protected void bindValues(SQLiteStatement stmt, Cart entity) {
        stmt.clearBindings();
 
        Long id = entity.getId();
        if (id != null) {
            stmt.bindLong(1, id);
        }
 
        String cart_item_id = entity.getCart_item_id();
        if (cart_item_id != null) {
            stmt.bindString(2, cart_item_id);
        }
 
        String product_id = entity.getProduct_id();
        if (product_id != null) {
            stmt.bindString(3, product_id);
        }
 
        String product_name = entity.getProduct_name();
        if (product_name != null) {
            stmt.bindString(4, product_name);
        }
 
        String product_attr_id = entity.getProduct_attr_id();
        if (product_attr_id != null) {
            stmt.bindString(5, product_attr_id);
        }
 
        String product_qty = entity.getProduct_qty();
        if (product_qty != null) {
            stmt.bindString(6, product_qty);
        }
 
        String product_size = entity.getProduct_size();
        if (product_size != null) {
            stmt.bindString(7, product_size);
        }
 
        String product_discount = entity.getProduct_discount();
        if (product_discount != null) {
            stmt.bindString(8, product_discount);
        }
 
        String product_mrp = entity.getProduct_mrp();
        if (product_mrp != null) {
            stmt.bindString(9, product_mrp);
        }
 
        String product_img = entity.getProduct_img();
        if (product_img != null) {
            stmt.bindString(10, product_img);
        }
 
        String product_max_qty = entity.getProduct_max_qty();
        if (product_max_qty != null) {
            stmt.bindString(11, product_max_qty);
        }
 
        String product_price = entity.getProduct_price();
        if (product_price != null) {
            stmt.bindString(12, product_price);
        }
 
        Integer product_savings = entity.getProduct_savings();
        if (product_savings != null) {
            stmt.bindLong(13, product_savings);
        }
    }

    /** @inheritdoc */
    @Override
    public Long readKey(Cursor cursor, int offset) {
        return cursor.isNull(offset + 0) ? null : cursor.getLong(offset + 0);
    }    

    /** @inheritdoc */
    @Override
    public Cart readEntity(Cursor cursor, int offset) {
        Cart entity = new Cart( //
            cursor.isNull(offset + 0) ? null : cursor.getLong(offset + 0), // id
            cursor.isNull(offset + 1) ? null : cursor.getString(offset + 1), // cart_item_id
            cursor.isNull(offset + 2) ? null : cursor.getString(offset + 2), // product_id
            cursor.isNull(offset + 3) ? null : cursor.getString(offset + 3), // product_name
            cursor.isNull(offset + 4) ? null : cursor.getString(offset + 4), // product_attr_id
            cursor.isNull(offset + 5) ? null : cursor.getString(offset + 5), // product_qty
            cursor.isNull(offset + 6) ? null : cursor.getString(offset + 6), // product_size
            cursor.isNull(offset + 7) ? null : cursor.getString(offset + 7), // product_discount
            cursor.isNull(offset + 8) ? null : cursor.getString(offset + 8), // product_mrp
            cursor.isNull(offset + 9) ? null : cursor.getString(offset + 9), // product_img
            cursor.isNull(offset + 10) ? null : cursor.getString(offset + 10), // product_max_qty
            cursor.isNull(offset + 11) ? null : cursor.getString(offset + 11), // product_price
            cursor.isNull(offset + 12) ? null : cursor.getInt(offset + 12) // product_savings
        );
        return entity;
    }
     
    /** @inheritdoc */
    @Override
    public void readEntity(Cursor cursor, Cart entity, int offset) {
        entity.setId(cursor.isNull(offset + 0) ? null : cursor.getLong(offset + 0));
        entity.setCart_item_id(cursor.isNull(offset + 1) ? null : cursor.getString(offset + 1));
        entity.setProduct_id(cursor.isNull(offset + 2) ? null : cursor.getString(offset + 2));
        entity.setProduct_name(cursor.isNull(offset + 3) ? null : cursor.getString(offset + 3));
        entity.setProduct_attr_id(cursor.isNull(offset + 4) ? null : cursor.getString(offset + 4));
        entity.setProduct_qty(cursor.isNull(offset + 5) ? null : cursor.getString(offset + 5));
        entity.setProduct_size(cursor.isNull(offset + 6) ? null : cursor.getString(offset + 6));
        entity.setProduct_discount(cursor.isNull(offset + 7) ? null : cursor.getString(offset + 7));
        entity.setProduct_mrp(cursor.isNull(offset + 8) ? null : cursor.getString(offset + 8));
        entity.setProduct_img(cursor.isNull(offset + 9) ? null : cursor.getString(offset + 9));
        entity.setProduct_max_qty(cursor.isNull(offset + 10) ? null : cursor.getString(offset + 10));
        entity.setProduct_price(cursor.isNull(offset + 11) ? null : cursor.getString(offset + 11));
        entity.setProduct_savings(cursor.isNull(offset + 12) ? null : cursor.getInt(offset + 12));
     }
    
    /** @inheritdoc */
    @Override
    protected Long updateKeyAfterInsert(Cart entity, long rowId) {
        entity.setId(rowId);
        return rowId;
    }
    
    /** @inheritdoc */
    @Override
    public Long getKey(Cart entity) {
        if(entity != null) {
            return entity.getId();
        } else {
            return null;
        }
    }

    /** @inheritdoc */
    @Override    
    protected boolean isEntityUpdateable() {
        return true;
    }
    
}
