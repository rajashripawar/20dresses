package outletwise.com.twentydresses.model;

import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.annotations.SerializedName;

/**
 * Created by User-PC on 04-12-2015.
 */
public class UserAttrEntity implements Parcelable{
    private String Shape;
    private String Size;
    @SerializedName("Bottoms Size")
    private String Bottoms_Size;
    private String Height;
    @SerializedName("Shoes Size")
    private String Shoes_Size;
    @SerializedName("Complexion")
    private String Face_Color;
    @SerializedName("Face Shape")
    private String Face_Shape;

    public UserAttrEntity(){

    }

    protected UserAttrEntity(Parcel in) {
        Shape = in.readString();
        Size = in.readString();
        Bottoms_Size = in.readString();
        Height = in.readString();
        Shoes_Size = in.readString();
        Face_Color = in.readString();
        Face_Shape = in.readString();
    }

    public static final Creator<UserAttrEntity> CREATOR = new Creator<UserAttrEntity>() {
        @Override
        public UserAttrEntity createFromParcel(Parcel in) {
            return new UserAttrEntity(in);
        }

        @Override
        public UserAttrEntity[] newArray(int size) {
            return new UserAttrEntity[size];
        }
    };

    public void setShape(String Shape) {
        this.Shape = Shape;
    }

    public void setSize(String Size) {
        this.Size = Size.replace("\n", " ");
    }

    public void setBottoms_Size(String Bottoms_Size) {
        this.Bottoms_Size = Bottoms_Size + " Inch";
    }

    public void setHeight(String Height) {
        this.Height = Height;
    }

    public void setShoes_Size(String Shoes_Size) {
        this.Shoes_Size = Shoes_Size;
    }

    public void setFace_Color(String Face_Color) {
        this.Face_Color = Face_Color;
    }

    public void setFace_Shape(String Face_Shape) {
        this.Face_Shape = Face_Shape;
    }

    public String getShape() {
        return Shape;
    }

    public String getSize() {
        return Size;
    }

    public String getBottoms_Size() {
        return Bottoms_Size;
    }

    public String getHeight() {
        return Height;
    }

    public String getShoes_Size() {
        return Shoes_Size;
    }

    public String getFace_Color() {
        return Face_Color;
    }

    public String getFace_Shape() {
        return Face_Shape;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(Shape);
        dest.writeString(Size);
        dest.writeString(Bottoms_Size);
        dest.writeString(Height);
        dest.writeString(Shoes_Size);
        dest.writeString(Face_Color);
        dest.writeString(Face_Shape);
    }
}
