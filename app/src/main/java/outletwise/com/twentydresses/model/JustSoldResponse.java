package outletwise.com.twentydresses.model;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by webwerks on 20/7/16.
 */
public class JustSoldResponse extends GenericResponse{

    /**
     * status : Success
     * message :
     * products : [{"product_id":"5698","product_name":"Fifty Shades Of Grey Wrap Around Dress","product_price":"1495","product_discount":"30","product_img":"http://www.20dresses.com/uploads/product/front_img/Fifty-Shades-Of-Grey-Wrap-Around-Dress-1.jpg"},{"product_id":"1256","product_name":"Pink Filigree Necklace","product_price":"595","product_discount":"50","product_img":"http://www.20dresses.com/uploads/product/front_img/Neon-Pink-Filigree-Necklace-2.jpg"},{"product_id":"6839","product_name":"Black Give In To Animal Instinct Backpack","product_price":"1895","product_discount":"0","product_img":"http://www.20dresses.com/uploads/product/front_img/Black-Give-In-To-Animal-Instinct-Backpack-1.JPG"},{"product_id":"5700","product_name":"Yellow The Cute Frill Bow Top ","product_price":"795","product_discount":"40","product_img":"http://www.20dresses.com/uploads/product/front_img/Yellow-The-Cute-Frill-Bow-Top-1.jpg"},{"product_id":"5335","product_name":"My Dancing Queen Turquoise Maxi Dress","product_price":"1795","product_discount":"60","product_img":"http://www.20dresses.com/uploads/product/front_img/My-Dancing-Queen-Turquoise-Maxi-Dress-1.JPG"},{"product_id":"5705","product_name":"A Hint Of Scotland Checked Top","product_price":"895","product_discount":"0","product_img":"http://www.20dresses.com/uploads/product/front_img/A-Hint-Of-Scotland-Checked-Top-1.jpg"},{"product_id":"5698","product_name":"Fifty Shades Of Grey Wrap Around Dress","product_price":"1495","product_discount":"30","product_img":"http://www.20dresses.com/uploads/product/front_img/Fifty-Shades-Of-Grey-Wrap-Around-Dress-1.jpg"},{"product_id":"5814","product_name":"The Inner Boho Freed Silver Necklace ","product_price":"699","product_discount":"0","product_img":"http://www.20dresses.com/uploads/product/front_img/The-Inner-Boho-Freed-Silver-Necklace-2.JPG"},{"product_id":"6861","product_name":"Nude Classic Edition Block Heels","product_price":"1695","product_discount":"0","product_img":"http://www.20dresses.com/uploads/product/front_img/Nude-Classic-Edition-Block-Heels-1.JPG"},{"product_id":"5846","product_name":"Loving The Peace Earring Set","product_price":"399","product_discount":"0","product_img":"http://www.20dresses.com/uploads/product/front_img/Loving-The-Peace-Earring-Set-1.jpg"}]
     */


    /**
     * product_id : 5698
     * product_name : Fifty Shades Of Grey Wrap Around Dress
     * product_price : 1495
     * product_discount : 30
     * product_img : http://www.20dresses.com/uploads/product/front_img/Fifty-Shades-Of-Grey-Wrap-Around-Dress-1.jpg
     */

    private ArrayList<Product> products;

    public ArrayList<Product> getProducts() {
        return products;
    }

    public void setProducts(ArrayList<Product> products) {
        this.products = products;
    }

}
