package outletwise.com.twentydresses.model;

/**
 * Created by User-PC on 24-02-2016.
 */
public class Refund {


    /**
     * auth_token : ......
     * user : user_id
     * refund : {"refund_type":"credit","bank_deposit":"existing","bank_id":57,"new_bank_details":{"bank_name":"ICICI Bank","bank_account_no":"78547999","bank_account_type":"Savings","bank_branch_name":"Worli Naka","bank_account_name":"Adolf","bank_ifsc_code":"IC457898"}}
     */

    private String auth_token;
    private String user;
    /**
     * refund_type : credit
     * bank_deposit : existing
     * bank_id : 57
     * new_bank_details : {"bank_name":"ICICI Bank","bank_account_no":"78547999","bank_account_type":"Savings","bank_branch_name":"Worli Naka","bank_account_name":"Adolf","bank_ifsc_code":"IC457898"}
     */

    private RefundEntity refund;

    public void setAuth_token(String auth_token) {
        this.auth_token = auth_token;
    }

    public void setUser(String user) {
        this.user = user;
    }

    public void setRefund(RefundEntity refund) {
        this.refund = refund;
    }

    public String getAuth_token() {
        return auth_token;
    }

    public String getUser() {
        return user;
    }

    public RefundEntity getRefund() {
        return refund;
    }

    public static class RefundEntity {
        private String refund_type;
        private String bank_deposit;
        private int bank_id;
        /**
         * bank_name : ICICI Bank
         * bank_account_no : 78547999
         * bank_account_type : Savings
         * bank_branch_name : Worli Naka
         * bank_account_name : Adolf
         * bank_ifsc_code : IC457898
         */

        private NewBankDetailsEntity new_bank_details;
        private WalletResponse.RefundTypesEntity.BankDetailsEntity bankDetails;

        public void setRefund_type(String refund_type) {
            this.refund_type = refund_type;
        }

        public void setBank_deposit(String bank_deposit) {
            this.bank_deposit = bank_deposit;
        }

        public void setBank_id(int bank_id) {
            this.bank_id = bank_id;
        }

        public void setNew_bank_details(NewBankDetailsEntity new_bank_details) {
            this.new_bank_details = new_bank_details;
        }

        public String getRefund_type() {
            return refund_type;
        }

        public String getBank_deposit() {
            return bank_deposit;
        }

        public int getBank_id() {
            return bank_id;
        }

        public NewBankDetailsEntity getNew_bank_details() {
            return new_bank_details;
        }



        public static class NewBankDetailsEntity {
            private String bank_name;
            private String bank_account_no;
            private String bank_account_type;
            private String bank_branch_name;
            private String bank_account_name;
            private String bank_ifsc_code;

            public void setBank_name(String bank_name) {
                this.bank_name = bank_name;
            }

            public void setBank_account_no(String bank_account_no) {
                this.bank_account_no = bank_account_no;
            }

            public void setBank_account_type(String bank_account_type) {
                this.bank_account_type = bank_account_type;
            }

            public void setBank_branch_name(String bank_branch_name) {
                this.bank_branch_name = bank_branch_name;
            }

            public void setBank_account_name(String bank_account_name) {
                this.bank_account_name = bank_account_name;
            }

            public void setBank_ifsc_code(String bank_ifsc_code) {
                this.bank_ifsc_code = bank_ifsc_code;
            }

            public String getBank_name() {
                return bank_name;
            }

            public String getBank_account_no() {
                return bank_account_no;
            }

            public String getBank_account_type() {
                return bank_account_type;
            }

            public String getBank_branch_name() {
                return bank_branch_name;
            }

            public String getBank_account_name() {
                return bank_account_name;
            }

            public String getBank_ifsc_code() {
                return bank_ifsc_code;
            }
        }
    }
}
