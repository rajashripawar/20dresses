package outletwise.com.twentydresses.model.database.greenbot;

import java.util.List;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteStatement;

import de.greenrobot.dao.AbstractDao;
import de.greenrobot.dao.Property;
import de.greenrobot.dao.internal.DaoConfig;
import de.greenrobot.dao.query.Query;
import de.greenrobot.dao.query.QueryBuilder;

import outletwise.com.twentydresses.model.database.greenbot.Address;

// THIS CODE IS GENERATED BY greenDAO, DO NOT EDIT.
/** 
 * DAO for table ADDRESS.
*/
public class AddressDao extends AbstractDao<Address, Integer> {

    public static final String TABLENAME = "ADDRESS";

    /**
     * Properties of entity Address.<br/>
     * Can be used for QueryBuilder and for referencing column names.
    */
    public static class Properties {
        public final static Property AddressId = new Property(0, Integer.class, "addressId", true, "ADDRESS_ID");
        public final static Property User_id = new Property(1, Long.class, "user_id", false, "USER_ID");
        public final static Property StreetName = new Property(2, String.class, "streetName", false, "STREET_NAME");
        public final static Property PinCode = new Property(3, Long.class, "pinCode", false, "PIN_CODE");
    };

    private Query<Address> user_AddressListQuery;

    public AddressDao(DaoConfig config) {
        super(config);
    }
    
    public AddressDao(DaoConfig config, DaoSession daoSession) {
        super(config, daoSession);
    }

    /** Creates the underlying database table. */
    public static void createTable(SQLiteDatabase db, boolean ifNotExists) {
        String constraint = ifNotExists? "IF NOT EXISTS ": "";
        db.execSQL("CREATE TABLE " + constraint + "'ADDRESS' (" + //
                "'ADDRESS_ID' INTEGER PRIMARY KEY ," + // 0: addressId
                "'USER_ID' INTEGER," + // 1: user_id
                "'STREET_NAME' TEXT," + // 2: streetName
                "'PIN_CODE' INTEGER);"); // 3: pinCode
    }

    /** Drops the underlying database table. */
    public static void dropTable(SQLiteDatabase db, boolean ifExists) {
        String sql = "DROP TABLE " + (ifExists ? "IF EXISTS " : "") + "'ADDRESS'";
        db.execSQL(sql);
    }

    /** @inheritdoc */
    @Override
    protected void bindValues(SQLiteStatement stmt, Address entity) {
        stmt.clearBindings();
 
        Integer addressId = entity.getAddressId();
        if (addressId != null) {
            stmt.bindLong(1, addressId);
        }
 
        Long user_id = entity.getUser_id();
        if (user_id != null) {
            stmt.bindLong(2, user_id);
        }
 
        String streetName = entity.getStreetName();
        if (streetName != null) {
            stmt.bindString(3, streetName);
        }
 
        Long pinCode = entity.getPinCode();
        if (pinCode != null) {
            stmt.bindLong(4, pinCode);
        }
    }

    /** @inheritdoc */
    @Override
    public Integer readKey(Cursor cursor, int offset) {
        return cursor.isNull(offset + 0) ? null : cursor.getInt(offset + 0);
    }    

    /** @inheritdoc */
    @Override
    public Address readEntity(Cursor cursor, int offset) {
        Address entity = new Address( //
            cursor.isNull(offset + 0) ? null : cursor.getInt(offset + 0), // addressId
            cursor.isNull(offset + 1) ? null : cursor.getLong(offset + 1), // user_id
            cursor.isNull(offset + 2) ? null : cursor.getString(offset + 2), // streetName
            cursor.isNull(offset + 3) ? null : cursor.getLong(offset + 3) // pinCode
        );
        return entity;
    }
     
    /** @inheritdoc */
    @Override
    public void readEntity(Cursor cursor, Address entity, int offset) {
        entity.setAddressId(cursor.isNull(offset + 0) ? null : cursor.getInt(offset + 0));
        entity.setUser_id(cursor.isNull(offset + 1) ? null : cursor.getLong(offset + 1));
        entity.setStreetName(cursor.isNull(offset + 2) ? null : cursor.getString(offset + 2));
        entity.setPinCode(cursor.isNull(offset + 3) ? null : cursor.getLong(offset + 3));
     }
    
    /** @inheritdoc */
    @Override
    protected Integer updateKeyAfterInsert(Address entity, long rowId) {
        return entity.getAddressId();
    }
    
    /** @inheritdoc */
    @Override
    public Integer getKey(Address entity) {
        if(entity != null) {
            return entity.getAddressId();
        } else {
            return null;
        }
    }

    /** @inheritdoc */
    @Override    
    protected boolean isEntityUpdateable() {
        return true;
    }
    
    /** Internal query to resolve the "addressList" to-many relationship of User. */
    public List<Address> _queryUser_AddressList(Long user_id) {
        synchronized (this) {
            if (user_AddressListQuery == null) {
                QueryBuilder<Address> queryBuilder = queryBuilder();
                queryBuilder.where(Properties.User_id.eq(null));
                user_AddressListQuery = queryBuilder.build();
            }
        }
        Query<Address> query = user_AddressListQuery.forCurrentThread();
        query.setParameter(0, user_id);
        return query.list();
    }

}
