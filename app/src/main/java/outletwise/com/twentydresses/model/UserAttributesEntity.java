package outletwise.com.twentydresses.model;

import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.annotations.SerializedName;

/**
 * Created by webwerks on 4/8/16.
 */
public class UserAttributesEntity  {


    /**
     * value : Square
     * que_id : 50
     */
    @SerializedName("Face Shape")
    private FaceShapeBean FaceShape;

    public FaceShapeBean getFaceShape() {
        return FaceShape;
    }

    public void setFaceShape(FaceShapeBean FaceShape) {
        this.FaceShape = FaceShape;
    }


    public static class FaceShapeBean {
        private String value;
        private String que_id;

        public String getValue() {
            return value;
        }

        public void setValue(String value) {
            this.value = value;
        }

        public String getQue_id() {
            return que_id;
        }

        public void setQue_id(String que_id) {
            this.que_id = que_id;
        }
    }
}
