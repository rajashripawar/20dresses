package outletwise.com.twentydresses.model;

import com.google.gson.annotations.SerializedName;

import outletwise.com.twentydresses.model.database.greenbot.Shortlist;

/**
 * Created by User-PC on 10-09-2015.
 */
public class ShortListResponse {

    /**
     * status : Success
     * message : Successfully Added
     * data : {"product_id":"242","product_name":"Vintage wonder ring","product_discount":"0","product_price":"245","product_img":"http://www.20dresses.com/uploads/product/temp_img/thumb_Vintage-wonder-ring-1.jpg","product_addinfo":"<b>NOTE:<\/b> The colours of the product as seen on the screen and actual colours of the product may slightly differ due to your screen resolution"}
     */

    private String status;
    private String message;

    @SerializedName("data")
    private Shortlist shortlistProduct;

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public Shortlist getShortlistProduct() {
        return shortlistProduct;
    }

    public void setShortlistProduct(Shortlist shortlistProduct) {
        this.shortlistProduct = shortlistProduct;
    }

}
