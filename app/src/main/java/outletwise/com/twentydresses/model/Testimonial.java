package outletwise.com.twentydresses.model;

/**
 * Created by User-PC on 17-11-2015.
 */
public class Testimonial {

    /**
     * name : renuka dasari
     * testimonial :  I love my bag.!! Thank you for the gift and the personal message, made my day..
     * location : Mumbai
     * date : 2015-05-07
     * image : http://www.20dresses.com/system/application/views/images/defaultpp.jpg
     */

    private String name;
    private String testimonial;
    private String location;
    private String date;
    private String image;
    private String status;
    private String message;
    private String total_testimonials;
    private boolean add_testimonial;
    private boolean testimonial_added;

    public void setMessage(String message) {
        this.message = message;
    }

    public String getMessage() {
        return message;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getStatus() {
        return status;
    }

    public void setTestimonial_added(boolean testimonial_added) {
        this.testimonial_added = testimonial_added;
    }

    public boolean getTestimonial_added() {
        return testimonial_added;
    }

    public void setAdd_testimonial(boolean add_testimonial) {
        this.add_testimonial = add_testimonial;
    }

    public boolean getAdd_testimonial() {
        return add_testimonial;
    }

    public void setName(String name) {
        this.name = name;
    }

    public void setTestimonial(String testimonial) {
        this.testimonial = testimonial;
    }

    public void setLocation(String location) {
        this.location = location;
    }

    public void setDate(String date) {
        this.date = date;
    }

    public void setImage(String image) {
        this.image = image;
    }

    public String getName() {
        return name;
    }

    public String getTestimonial() {
        return testimonial;
    }

    public String getLocation() {
        return location;
    }

    public String getDate() {
        return date;
    }

    public String getImage() {
        return image;
    }

    public void setTotal_testimonials(String total_testimonials) {
        this.total_testimonials = total_testimonials;
    }

    public String getTotal_testimonials() {
        return total_testimonials;
    }
}
