package outletwise.com.twentydresses.model;

import java.util.ArrayList;

/**
 * Created by User-PC on 25-09-2015.
 */
public class PinCodeResponse {


    /**
     * status : success
     * msg : Available with COD
     * cod_available : true
     * pickup_available : true
     * days_text : 2-3
     * expected_delivery : Expected Delivery in 2-3 days to 400607
     * city : Thane
     * state : Maharashtra
     * states : ["Andhra Pradesh","Arunachal Pradesh","Assam","Bihar","Chhattisgarh","Goa","Gujarat","Haryana","Himachal Pradesh","Jammu and Kashmir","Jharkhand","Karnataka","Kerala","Madhya Pradesh","Maharashtra","Manipur","Meghalaya","Mizoram","Nagaland","Odisha","Punjab","Rajasthan","Sikkim","Tamil Nadu","Tripura","Uttar Pradesh","Uttarakhand","West Bengal","Chandigarh","Daman and Diu","Lakshadweep","Pondicherry","Delhi","National Capital Territor"]
     */

    private String status;
    private String msg;
    private String cod_available;
    private String pickup_available;
    private String days_text;
    private String expected_delivery;

    public String getUser_email_message() {
        return user_email_message;
    }

    public void setUser_email_message(String user_email_message) {
        this.user_email_message = user_email_message;
    }

    private String user_email_message;


    public String getUser_email() {
        return user_email;
    }

    public void setUser_email(String user_email) {
        this.user_email = user_email;
    }

    private String user_email;
    private String state;
    private ArrayList<String> states;
    private boolean user_cod_block;
    private boolean paytm_option;
    private String checkout_message;
    private String city;

    public boolean getUser_cod_block() {
        return user_cod_block;
    }

    public boolean getPaytm_option() {
        return paytm_option;
    }

    public void setPaytm_option(boolean paytm_option) {
        this.paytm_option = paytm_option;
    }

    public void setUser_cod_block(boolean user_cod_block) {
        this.user_cod_block = user_cod_block;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public void setMsg(String msg) {
        this.msg = msg;
    }

    public String getCheckout_message() {
        return checkout_message;
    }

    public void setCheckout_message(String checkout_message) {
        this.checkout_message = checkout_message;
    }

    public void setCod_available(String cod_available) {
        this.cod_available = cod_available;
    }

    public void setPickup_available(String pickup_available) {
        this.pickup_available = pickup_available;
    }

    public void setDays_text(String days_text) {
        this.days_text = days_text;
    }

    public void setExpected_delivery(String expected_delivery) {
        this.expected_delivery = expected_delivery;
    }

    public void setCity(String city) {
        this.city = city;
    }

    public void setState(String state) {
        this.state = state;
    }

    public void setStates(ArrayList<String> states) {
        this.states = states;
    }

    public String getStatus() {
        return status;
    }

    public String getMsg() {
        return msg;
    }

    public boolean getCod_available() {
        return cod_available.equalsIgnoreCase("true");

    }

    public boolean getPickup_available() {
        return pickup_available.equalsIgnoreCase("true");

    }

    public String getDays_text() {
        return days_text;
    }

    public String getExpected_delivery() {
        return expected_delivery;
    }

    public String getCity() {
        return city;
    }

    public String getState() {
        return state;
    }

    public ArrayList<String> getStates() {
        return states;
    }
}
