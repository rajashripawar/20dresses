package outletwise.com.twentydresses.model;

import com.google.gson.annotations.SerializedName;

/**
 * Created by User-PC on 03-12-2015.
 */
public class Profile {
    /**
     * user_id : 4
     * user_first_name : renuka
     * user_last_name : dasari
     * user_picture : http://www.20dresses.com/images/profile/default100.jpg
     * user_gender : F
     * user_mobile :
     * facebook_email :
     * facebook_id :
     * dob : 1955-10-14
     * email : dasari.renuka@gmail.com
     * cust_bio :
     * user_style : Casual
     * user_background_image : http://www.20dresses.com/images/background/Casual.png
     *
     *  {
     "user_id": "4",
     "user_first_name": "Renuka",
     "user_last_name": "Dasari",
     "user_picture": "http:\/\/www.20dresses.com\/images\/profile\/test_1476794825038.jpg",
     "user_gender": "F",
     "user_mobile": "9968587460",
     "user_dob": "1989-01-04",
     "user_email": "dasari.renuka@gmail.com",
     "user_bio": "iOS Developer",
     "user": "Valid",
     "user_style": "Dynamic",
     "user_background_image": "http:\/\/www.20dresses.com\/images\/background\/Dynamic.png",
     "check_mobile_verification": false,
     "user_mobile_verified": false,
     "last_purchase_date": "1970-01-01",
     "status": "Success"
     }
     */

    private String user_id;
    private String user_first_name;
    private String user_last_name;
    private String user_picture;
    private String user_gender;
    private String user_mobile;
    private String facebook_email;
    private String facebook_id;
    @SerializedName("user_dob")
    private String dob;
    @SerializedName("user_email")
    private String email;
    @SerializedName("user_bio")
    private String cust_bio;
    private String user_style;
    private String user_background_image;
    private boolean check_mobile_verification;
    private boolean user_mobile_verified;

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getLast_purchase_date() {
        return last_purchase_date;
    }

    public void setLast_purchase_date(String last_purchase_date) {
        this.last_purchase_date = last_purchase_date;
    }

    public String getUser() {
        return user;
    }

    public void setUser(String user) {
        this.user = user;
    }

    public boolean isCheck_mobile_verification() {
        return check_mobile_verification;
    }

    public void setCheck_mobile_verification(boolean check_mobile_verification) {
        this.check_mobile_verification = check_mobile_verification;
    }

    public void setUser_mobile_verified(boolean user_mobile_verified) {
        this.user_mobile_verified = user_mobile_verified;
    }

    @SerializedName("status")
    private String status;

    @SerializedName("last_purchase_date")
    private String last_purchase_date;

    @SerializedName("user")
    private String user;

    public void setCheck_mobile_verification() {
        this.check_mobile_verification = check_mobile_verification;
    }

    public void setUser_mobile_verified() {
        this.user_mobile_verified = user_mobile_verified;
    }

    public void setUser_id(String user_id) {
        this.user_id = user_id;
    }

    public void setUser_first_name(String user_first_name) {
        this.user_first_name = user_first_name;
    }

    public void setUser_last_name(String user_last_name) {
        this.user_last_name = user_last_name;
    }

    public void setUser_picture(String user_picture) {
        this.user_picture = user_picture;
    }

    public void setUser_gender(String user_gender) {
        this.user_gender = user_gender;
    }

    public void setUser_mobile(String user_mobile) {
        this.user_mobile = user_mobile;
    }

    public void setFacebook_email(String facebook_email) {
        this.facebook_email = facebook_email;
    }

    public void setFacebook_id(String facebook_id) {
        this.facebook_id = facebook_id;
    }

    public void setDob(String dob) {
        this.dob = dob;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public void setCust_bio(String cust_bio) {
        this.cust_bio = cust_bio;
    }

    public void setUser_style(String user_style) {
        this.user_style = user_style;
    }

    public void setUser_background_image(String user_background_image) {
        this.user_background_image = user_background_image;
    }

    public String getUser_id() {
        return user_id;
    }

    public String getUser_first_name() {
        return user_first_name;
    }

    public String getUser_last_name() {
        return user_last_name;
    }

    public String getUser_picture() {
        return user_picture;
    }

    public String getUser_gender() {
        return user_gender;
    }

    public String getUser_mobile() {
        return user_mobile;
    }

    public String getFacebook_email() {
        return facebook_email;
    }

    public String getFacebook_id() {
        return facebook_id;
    }

    public String getDob() {
        return dob;
    }

    public String getEmail() {
        return email;
    }

    public String getCust_bio() {
        return cust_bio;
    }

    public String getUser_style() {
        return user_style;
    }

    public String getUser_background_image() {
        return user_background_image;
    }

    public boolean getCheck_mobile_verification() {
        return check_mobile_verification;
    }

    public boolean isUser_mobile_verified() {
        return user_mobile_verified;
    }
}


