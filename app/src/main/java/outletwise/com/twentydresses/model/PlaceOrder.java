package outletwise.com.twentydresses.model;

import com.citrus.sdk.classes.Amount;
import com.citrus.sdk.payment.PaymentOption;

/**
 * Created by User-PC on 19-11-2015.
 */
public class PlaceOrder {
    private boolean apply_offers, apply_credits, apply_wallet, gift_wrap;
    private Address address;
    private String payment_mode;
    private long user;
    private String auth_token;
    private PaymentOption paymentOption;
    private Amount amount;
    private String billUri;
    private String payment_gateway;
    private Float android_app_version;


    public String getUser_email() {
        return user_email;
    }

    public void setUser_email(String user_email) {
        this.user_email = user_email;
    }

    private String user_email;

    public void setPayment_gateway(String payment_gateway) {
        this.payment_gateway = payment_gateway;
    }

    public boolean isApply_offers() {
        return apply_offers;
    }

    public void setIsOfferApplied(boolean isOfferApplied) {
        this.apply_offers = isOfferApplied;
    }

    public boolean isApply_credits() {
        return apply_credits;
    }

    public void setIsCreditApplied(boolean isCreditApplied) {
        this.apply_credits = isCreditApplied;
    }

    public boolean isApply_wallet() {
        return apply_wallet;
    }

    public void setIsWalletApplied(boolean isWalletApplied) {
        this.apply_wallet = isWalletApplied;
    }

    public boolean isGift_wrap() {
        return gift_wrap;
    }

    public void setIsGiftWrap(boolean isGiftWrap) {
        this.gift_wrap = isGiftWrap;
    }

    public Address getAddress() {
        return address;
    }

    public void setAddress(Address address) {
        this.address = address;
    }

    public String getPayment_mode() {
        return payment_mode;
    }

    public void setPayment_mode(String payment_mode) {
        this.payment_mode = payment_mode;
    }

    public long getUser() {
        return user;
    }

    public void setUser(long user) {
        this.user = user;
    }

    public String getAuth_token() {
        return auth_token;
    }

    public String getPayment_gateway() {
        return payment_gateway;
    }

    public void setAuth_token(String auth_token) {
        this.auth_token = auth_token;
    }

    public PaymentOption getPaymentOption() {
        return paymentOption;
    }

    public void setPaymentOption(PaymentOption paymentOption) {
        this.paymentOption = paymentOption;
    }

    public Amount getAmount() {
        return amount;
    }

    public void setAmount(double amount) {
        this.amount = new Amount(amount + "");
    }

    public String getBillUri() {
        return billUri;
    }

    public void setBillUri(String billUri) {
        this.billUri = billUri;
    }

    public Float getCurrent_app_version() {
        return android_app_version;
    }

    public void setCurrent_app_version(Float current_app_version) {
        this.android_app_version = current_app_version;
    }
}
