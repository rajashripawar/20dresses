package outletwise.com.twentydresses.model;

import outletwise.com.twentydresses.utilities.Constants;

/**
 * Created by User-PC on 17-08-2015.
 */
public class Size {
    private int sizeId;
    private Constants.SizeTypes type;
    private String size;

    public int getSizeId() {
        return sizeId;
    }

    public void setSizeId(int sizeId) {
        this.sizeId = sizeId;
    }

    public Constants.SizeTypes getType() {
        return type;
    }

    public void setType(Constants.SizeTypes type) {
        this.type = type;
    }

    public String getSize() {
        return size;
    }

    public void setSize(String size) {
        this.size = size;
    }
}
