package outletwise.com.twentydresses.utilities.database;

import android.content.Context;
import android.os.AsyncTask;

import java.util.ArrayList;

import de.greenrobot.dao.DaoException;
import outletwise.com.twentydresses.ApplicationUtil;
import outletwise.com.twentydresses.model.database.greenbot.Cart;
import outletwise.com.twentydresses.model.database.greenbot.CartDao;
import outletwise.com.twentydresses.model.database.greenbot.DaoSession;
import outletwise.com.twentydresses.model.database.greenbot.Shortlist;
import outletwise.com.twentydresses.model.database.greenbot.ShortlistDao;
import outletwise.com.twentydresses.utilities.Constants;
import outletwise.com.twentydresses.utilities.Constants.Records;

/**
 * Created by User-PC on 13-08-2015.
 */
public class DatabaseOperation extends AsyncTask<Void, Integer, Void> {

    private DatabaseCallBack mReceiver;
    private DaoSession daoSession;
    private Context mContext;

    private Records Type;
    private Object obj;
    private int tag;
    private ArrayList mArrObj;

    private ArrayList mRetrievedArray = new ArrayList();
    private boolean mErrorOccurred = false;
    private String msg = "Database Error";

    public DatabaseOperation(DatabaseCallBack receiver, Context mContext, Object obj, int tag, Records Type) {
        this(receiver, mContext, obj, tag, Type, false);
    }

    public DatabaseOperation(DatabaseCallBack receiver, Context mContext, ArrayList mArrObj, int tag, Records Type) {
        this(receiver, mContext, mArrObj, tag, Type, false);
    }

    public DatabaseOperation(DatabaseCallBack receiver, Context mContext, Object obj, int tag, Records Type, boolean newSession) {
        this.mContext = mContext;
        mReceiver = receiver;
        if (newSession)
            daoSession = ApplicationUtil.getNewSession();
        else
            daoSession = ApplicationUtil.getSession();
        this.Type = Type;
        this.obj = obj;
        this.tag = tag;
    }

    public DatabaseOperation(DatabaseCallBack receiver, Context mContext, ArrayList mArrObj, int tag, Records Type, boolean newSession) {
        this.mContext = mContext;
        mReceiver = receiver;
        if (newSession)
            daoSession = ApplicationUtil.getNewSession();
        else
            daoSession = ApplicationUtil.getSession();
        this.Type = Type;
        this.mArrObj = mArrObj;
        this.tag = tag;
    }


    @Override
    protected void onPostExecute(Void result) {
        super.onPostExecute(result);

        if (Type == Records.INSERT || Type == Records.INSERT_ALL) {
            if (mErrorOccurred) {
                mReceiver.onDatabaseError(tag, Constants.DATABASE_INSERT_ERROR, "Error while inserting");
                Constants.error("Error while inserting");
            } else {
                mReceiver.onCompleteInsertion(tag, msg);
            }

        } else if (Type == Records.UPDATE) {
            if (mErrorOccurred) {
                mReceiver.onDatabaseError(tag, Constants.DATABASE_UPDATE_ERROR, "Error while updating");
                Constants.error("Error while updating");
            } else {
                mReceiver.onCompleteUpdation(tag, msg);
            }

        } else if (Type == Records.SELECT) {
            if (mErrorOccurred) {
                mReceiver.onDatabaseError(tag, Constants.DATABASE_RETRIVAL_ERROR, "Error while retriving");
                Constants.error("Error while retriving");

            } else {
                //send your fetched data instead of obj.
                mReceiver.onCompleteRetrieval(tag, mRetrievedArray, msg);
            }


        } else if (Type == Records.RAW_QUERY) {
            if (mErrorOccurred) {
                mReceiver.onDatabaseError(tag, Constants.DATABASE_RETRIVAL_ERROR, "Error while retriving");
            } else {

            }
            //send your fetched data instead of obj.
            return;
        } else if (Type == Records.DELETE || Type == Records.DELETE_ALL) {
            if (mErrorOccurred) {
                mReceiver.onDatabaseError(tag, Constants.DATABASE_RETRIVAL_ERROR, "Error while Deleting");
                Constants.error("Error while Deleting");
            } else {
                mReceiver.onCompleteDeletion(tag, msg);
            }

        }
    }

    @Override
    protected Void doInBackground(Void... params) {
        try {
            if (Type == Records.INSERT_ALL) {
                insertAllRecord(mArrObj, tag);
            } else if (Type == Records.INSERT) {

                insertRecord(obj, tag);

            } else if (Type == Records.UPDATE) {

                updateRecord(obj, tag);

            } else if (Type == Records.DELETE) {

                deleteRecord(obj, tag);

            } else if (Type == Records.DELETE_ALL) {

                deleteAllRecords(tag);

            } else if (Type == Records.SELECT) {
                mRetrievedArray = retrieveAllRecords(tag);

            } else if (Type == Records.RAW_QUERY) {
                return null;
            }
        } catch (DaoException e) {
            e.printStackTrace();
            mErrorOccurred = true;
        }

        return null;
    }

    private void insertAllRecord(ArrayList mArrObj, int tag) {
        for (Object object : mArrObj)
            daoSession.insertOrReplace(object);
    }

    private void insertRecord(Object obj, int tag) {
        daoSession.insertOrReplace(obj);
    }

    private void updateRecord(Object obj, int tag) {
        switch (tag) {
            case Constants.TAG_CART_UPDATE:
                //daoSession.getCartDao().update((Cart) obj);
                daoSession.getCartDao().updateInTx((Cart) obj);
                break;
        }
    }

    private void deleteRecord(Object obj, int tag) {
        switch (tag) {
            case Constants.TAG_USER:
                daoSession.getUserDao().deleteAll();
                break;
            case Constants.TAG_CART_DELETE:
                daoSession.getCartDao().queryBuilder().where(CartDao.Properties.Product_id.eq(obj)).
                        buildDelete().executeDeleteWithoutDetachingEntities();
                break;
            case Constants.TAG_SHORTLIST_REMOVE:
                daoSession.getShortlistDao().queryBuilder().where(ShortlistDao.Properties.Product_id.eq(((Shortlist) obj).getProduct_id())).
                        buildDelete().executeDeleteWithoutDetachingEntities();
        }
    }

    private void deleteAllRecords(int tag) {
        switch (tag) {
            case Constants.TAG_USER:
                daoSession.getUserDao().deleteAll();
                break;
            case Constants.TAG_CART_REPLACE_ALL:
            case Constants.TAG_CART_DELETE_ALL:
                daoSession.getCartDao().deleteAll();
                break;
            case Constants.TAG_SHORTLIST_ALL:
                daoSession.getShortlistDao().deleteAll();
                break;
        }
    }

    private ArrayList retrieveAllRecords(int tag) {
        switch (tag) {
            case Constants.TAG_USER:
                return (ArrayList) daoSession.getUserDao().loadAll();
            case Constants.TAG_SHORTLIST:
                return (ArrayList) daoSession.getShortlistDao().loadAll();
            case Constants.TAG_CART_SELECT:
                return (ArrayList) daoSession.getCartDao().loadAll();
        }
        return null;
    }
}
