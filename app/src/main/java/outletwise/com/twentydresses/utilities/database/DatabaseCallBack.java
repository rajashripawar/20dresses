package outletwise.com.twentydresses.utilities.database;

import java.util.ArrayList;

/**
 * Created by User-PC on 13-08-2015.
 */
public interface DatabaseCallBack {

    /**
     * @param tag : Request Tag
     * @param msg : Message
     */

    void onCompleteInsertion(int tag, String msg);

    /**
     * @param tag : Request Tag
     * @param msg : Message
     */
    void onCompleteUpdation(int tag, String msg);

    /**
     * @param tag : Request Tag
     * @param obj : Object of Pojo class
     * @param msg : Message
     */
    void onCompleteRetrieval(int tag, ArrayList obj, String msg);

    /**
     * @param tag : Request Tag
     * @param msg : Message
     */
    void onCompleteDeletion(int tag, String msg);

    /**
     * @param status : status code for error
     * @param msg    : error message
     */
    void onDatabaseError(int tag, int status, String msg);

}
