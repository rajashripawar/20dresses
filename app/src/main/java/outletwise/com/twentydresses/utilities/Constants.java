package outletwise.com.twentydresses.utilities;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.drawable.BitmapDrawable;
import android.graphics.drawable.Drawable;
import android.support.annotation.DrawableRes;
import android.util.Log;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.regex.Pattern;

/**
 * Created by User-PC on 24/07/2015.
 */
public class Constants {

    public static final String APP_KEY = "sf7unj4r8924bn248934nc4843n534hnf8";

    public static final String TAG = "20Dresses";
    public static final String DB_NAME = "20Dresses";

    public static final int REGISTRATION_ADAPTER = 0;
    public static final int SELECT_STYLE_ADAPTER = 1;
    public static final int FAVORITES_ADAPTER = 2;

    public static final String SWIPE_TYPE = "swipeType";

    //Intent Extras
    public static final String APPLIED_FILTERS = "applied_filters";
    public static final String ORDER = "order";
    public static final String FILTER = "filter";
    public static final String FILTER_PRODUCTS = "filter_products";
    public static final String CATEGORY = "category";
    public static final String PRODUCT_NAME = "product_name";
    public static final String CLEAR_ACTIVITIES = "clear_activities";
    public static final String START_PROFILE_ACTIVITY = "start_profile_activity";
    public static final String GRAND_TOTAL = "grand_total";
    public static final String ADDRESS = "address";
    public static final String PRODUCT_ZOOM_IMAGES = "product_zoom_images";
    public static final String PRODUCT_THUMB_IMAGES = "product_thumbnail_images";
    public static final String SIZE_CHART_IMAGE = "size_chart_image";
    public static final String CATEGORY_ID = "category_id";
    public static final String GET_REFUND = "get_refund";
    public static final String DEFAULT_SIZE_ID = "65";

    // Type of background image
    public static final String IMG_HOME = "home";
    public static final String IMG_GENDER = "gender";
    public static final String IMG_MEASUREMENT = "measurement";
    public static final String IMG_SIGNIN = "signin";
    public static final String IMG_THANK_YOU = "thankyou";


    public static String ORDER_ID_RETURN = "";
    public static boolean TAKE_QUIZ = false;
    public static boolean COD_AVAILABLE = false;
    public static String QUIZ = "quiz_param";
    public static final String LOGIN = "login";
    public static final String REGISTER = "register";
    public static final String PROFILE = "profile";
    public static String TIP_NAME = "tip_name";
    public static int TIP_TOTAL = 0;
    public static boolean GOTO_WISHLIST = false;
    public static boolean EDITPROFILE = false;
    public static boolean ForYouTakeQuiz = false;
    public static boolean SKIPQUIZ = false;


    //Network Messages
    public static final String RESPONSE_SUCCESS = "Success";
    public static final String RESPONSE_FAILED = "Failed";
    public static final String MESSAGE_DEFAULT = "Something went wrong..Please try again!";
    public static final String VALID = "Valid";
    public static final String INVALID = "Invalid";


    //TAGS
    public static final int TAG_SHORTLIST = 0;
    public static final int TAG_LOGIN = 1;
    public static final int TAG_USER = 2;
    public static final int TAG_VISITOR = 3;
    public static final int TAG_REGISTRATION = 4;
    public static final int TAG_PRODUCTS = 5;
    public static final int TAG_CATEGORIES = 6;
    public static final int TAG_SINGLE_PRODUCT = 7;
    public static final int TAG_CHK_DEL_OPTS = 8;
    public static final int TAG_CART_ADD = 9;
    public static final int TAG_CART_SELECT = 10;
    public static final int TAG_CART_DELETE = 11;
    public static final int TAG_CART_VIEW = 12;
    public static final int TAG_ORDER_LIST = 13;
    public static final int TAG_ORDER_VIEW = 14;
    public static final int TAG_WALLET = 15;
    public static final int TAG_ADDRESS_VIEW = 16;
    public static final int TAG_ORDER_PLACE = 17;
    public static final int TAG_USER_VERIFY_MOBILE = 18;
    public static final int TAG_USER_CONFIRM_MOBILE = 19;
    public static final int TAG_CART_DELETE_ALL = 20;
    public static final int TAG_CART_REPLACE_ALL = 21;
    public static final int TAG_TESTIMONIALS = 22;
    public static final int TAG_YOU_MAY_LIKE = 24;
    public static final int TAG_FOR_YOU = 25;
    public static final int TAG_FOR_YOU_PRODUCTS = 26;
    public static final int TAG_ORDER_RETURN_REASONS = 27;
    public static final int TAG_FILTER = 28;
    public static final int TAG_OFFERS = 29;
    public static final int TAG_PROFILE = 30;
    public static final int TAG_STYLE_PROFILE = 31;
    public static final int TAG_ORDER_RETURN = 32;
    public static final int TAG_ORDER_EXCHANGE_REASONS = 33;
    public static final int TAG_ORDER_RETURN_EXCHANGE = 34;
    public static final int TAG_ORDER_CANCEL = 35;
    public static final int TAG_UPDATE_PROFILE = 36;
    public static final int TAG_SHORTLIST_REMOVE = 37;
    public static final int TAG_CART_UPDATE = 38;
    public static final int TAG_ORDER_CANCEL_REASONS = 39;
    public static final int TAG_CHANGE_PASSWORD = 40;
    public static final int TAG_CREDITS = 41;
    public static final int TAG_UPDATE_STYLE_PROFILE = 42;
    public static final int TAG_CHK_DEL_OPTS_FRAG = 43;
    public static final int TAG_ADD_ADDRESS = 44;
    public static final int TAG_FILTER_PRODUCTS = 45;
    public static final int TAG_REFER = 46;
    public static final int TAG_FORGOT_PASSWORD = 47;
    public static final int TAG_UPDATE_PROFILE_PIC = 48;
    public static final int TAG_REFUND_WALLET = 49;
    public static final int TAG_CONFIRM_PREPAID = 50;
    public static final int TAG_SEND_INVOICE = 51;
    public static final int TAG_CHK_REF_CODE = 52;
    public static final int TAG_IMG_BACKGROUND = 53;
    public static final int TAG_USER_TESTIMONIALS = 54;

    public static final int TAG_TESTIMONIALS_SOCIAL = 78;


    public static final int TAG_CONTACT_US = 55;
    public static final int TAG_HELP_FAQ = 56;
    public static final int TAG_TERMS = 57;
    public static final int TAG_ABOUT_US = 58;
    public static final int TAG_NOTIFY_PRODUCT = 59;
    public static final int TAG_CHECKOUT_CART = 60;
    public static final int TAG_CHECK_APP_VESRION = 61;
    public static final int TAG_ADD_QUIZ = 62;
    public static final int TAG_SHORTLIST_VIEW = 63;
    public static final int TAG_SHORTLIST_ALL = 64;
    public static final int TAG_INVITE_FAQ = 65;
    public static final int TAG_TRENDS = 66;
    public static final int TAG_INVITE_REWARDS = 67;
    public static final int TAG_TREND_PRODUCTS = 68;
    public static final int TAG_REFERRAL_CONFIRM_MOBILE = 69;
    public static final int TAG_REFERRAL_VERIFY_MOBILE = 70;
    public static final int TAG_UPDATE_ADDRESS = 71;
    public static final int TAG_PRIVACY_POLICY = 72;
    public static final int TAG_JUST_SOLD = 73;
    public static final int TAG_HOME_BANNERS = 74;
    public static final int TAG_SEARCH = 75;
    public static final int TAG_DEL_ADDRESS = 76;
    public static final int TAG_FOR_YOU_QUIZ = 77;




    //Database Error codes
    public static final int DATABASE_INSERT_ERROR = -100;
    public static final int DATABASE_UPDATE_ERROR = -101;
    public static final int DATABASE_DELETE_ERROR = -102;
    public static final int DATABASE_RETRIVAL_ERROR = -103;


    //Http Error Code
    public static final int NETWORK_ERROR_CODE = -1;
    public static final int UNAUTHORIZED_CODE = 401;
    public static final int SERVER_DOWN_CODE = 443;
    public static final int SERVER_DOWN_CODE1 = 503;

    //Volley Constants
    public static final int REQUEST_TIMEOUT = 60000;
    public static final float BACKOFF_MULT = 1;
    public static final int MAX_RETRIES = 1;
    public static int LATESTSORT = 1;

    //Volley Errors
    public static final String NO_INTERNET_CONNECTION = "No Internet Connection. Please try again later.";
    public static final String SERVER_ERROR = "Server Error";
    public static final String NETWORK_ERROR = "Network Error";
    public static final String CONNECTION_TIME_OUT = "Connection Timeout";

    //Server Errors
    public static final String NO_MORE_RESULTS = "no results found";

    //Others
    public static final int CATEGORY_DEFAULT = 101;
    public static final int CATEGORY_REGISTRATION = 102;
    private static final String ISLOCKED_ARG = "isLocked";
    public static final String RUPEE = "\u20B9 ";
    public static final String SPACE = "\u0020 ";
    public static final String ARROW_RIGHT = " \u27A4";
    public static final String EXCHANGE = "Exchange";
    public static final String COD = "COD";
    public static final String PREPAID = "Prepaid";
    public static final String INTERNET_BANKING = "Internet Banking";
    public static final String DEBIT_CARD = "Debit Card";
    public static final String CREDIT_CARD = "Credit Card";
    public static final String PATYM = "Paytm";
    public static final String CITRUS = "Citrus";
    public static final String SALE_CAT = "1";
    public static final String SALE = "Sale";
    public static final String NEW = "New";
    public static Boolean UPDATE_PROFILE = false;
    public static Boolean CHECK_RECOMMENTATION = false;
    public static String SINGLE_ITEM = "SINGLE_ITEM";


    //Json keys
    public static final String AUTH_TOKEN = "auth_token";
    public static final String USER_ID = "user";
    public static final String PRODUCT = "product";
    public static final String ATTR_ID = "attr_id";
    public static final String ATTR_VALUE = "attr_value";
    public static final String ORDER_ID = "order";
    public static final String APPLY_OFFERS = "apply_offers";
    public static final String APPLY_WALLET = "apply_wallet";
    public static final String APPLY_CREDITS = "apply_credits";
    public static final String GIFT_WRAP = "gift_wrap";
    public static final String MOBILE = "mobile";
    public static final String OTP = "otp";
    public static final String ORDER_ITEM_ID = "order_item_id";
    public static final String ITEM_ID = "item_id";
    public static final String QUANTITY = "quantity";
    public static final String OLD_PASSWORD = "password";
    public static final String NEW_PASSWORD = "new_password";
    public static final String PINCODE = "pincode";

    public static final String CART_ITEM_ID = "cart_item_id";
    public static final String TESTIMONIAL = "testimonial";
    public static final String ADDRESS_ID = "address_id";

    //Address Json Keys
    public static final String FIRST_NAME = "firstname";
    public static final String LAST_NAME = "lastname";
    public static final String COUNTRY = "country";
    public static final String STATE = "state";
    public static final String CITY = "city";


    //Preferences
    public static final String KEY_TOKEN = "token";
    public static final String PASSWORD = "password";

    private static HashMap<String, String> orderStatus = new HashMap<>();
    public static boolean pincodeTextchange = false;
    public static String SESSION_EXPIRED_MSG = "Session expired! Please Login again.";
    public static String SERVER_DOWN_MSG = "Alert! Server under maintenance. Please try after some time.";
    public static int itemHeight = 0;

    public static String getOrderStatus(String key) {
        return orderStatus.get(key);
    }

    static {
        orderStatus.put("C", "CONFIRMED");
        orderStatus.put("X", "CANCELED");
        orderStatus.put("P", "PENDING");
        orderStatus.put("R", "REFUNDED");
        orderStatus.put("S", "SHIPPED");
        orderStatus.put("D", "DELIVERED");
        orderStatus.put("T", "RETURN INITIATED");
        orderStatus.put("PT", "PART RETURN INITIATED");
        orderStatus.put("PR", "PART REFUNDED");
        orderStatus.put("RTO", "DID NOT ACCEPT");
        orderStatus.put("RT", "PRODUCT RETURNED");
        orderStatus.put("L", "LOST IN TRANSIT");
        orderStatus.put("PK", "PACKED");
        orderStatus.put("OH", "ON HOLD");
        orderStatus.put("EP", "EXCHANGE PENDING");
        orderStatus.put("LR", "LOST IN REVERSE PICK UP");
        orderStatus.put("PLR", "PART LOST IN REV PICK UP");
    }

    public static void debug(String msg) {
        Log.d(TAG, "" + msg);
    }

    public static void info(String msg) {
        Log.i(TAG, "" + msg);
    }

    public static void error(String msg) {
        Log.e(TAG, "" + msg);
    }

    public static void warning(String msg) {
        Log.w(TAG, "" + msg);
    }

    public static Drawable resize(Context context, @DrawableRes Integer drawableId, int imageWidth, int imageHeight) {
        Drawable image = context.getResources().getDrawable(drawableId);
        return resize(context, image, imageWidth, imageHeight);
    }

    public static Drawable resize(Context context, Drawable drawable, int imageWidth, int imageHeight) {
        Bitmap b = ((BitmapDrawable) drawable).getBitmap();
        Bitmap bitmapResized = Bitmap.createScaledBitmap(b, imageWidth, imageHeight, false);
        return new BitmapDrawable(context.getResources(), bitmapResized);
    }

    public static final Pattern EMAIL_ADDRESS
            = Pattern.compile(
            "[a-zA-Z0-9\\+\\.\\_\\%\\-\\+]{1,256}" +
                    "\\@" +
                    "[a-zA-Z0-9][a-zA-Z0-9\\-]{0,64}" +
                    "(" +
                    "\\." +
                    "[a-zA-Z0-9][a-zA-Z0-9\\-]{0,25}" +
                    ")+"
    );

    private static int calculateInSampleSize(
            BitmapFactory.Options options, int reqWidth, int reqHeight) {
        // Raw height and width of image
        final int height = options.outHeight;
        final int width = options.outWidth;
        int inSampleSize = 1;

        if (height > reqHeight || width > reqWidth) {

            final int halfHeight = height / 2;
            final int halfWidth = width / 2;

            // Calculate the largest inSampleSize value that is a power of 2 and keeps both
            // height and width larger than the requested height and width.
            while ((halfHeight / inSampleSize) > reqHeight
                    && (halfWidth / inSampleSize) > reqWidth) {
                inSampleSize *= 2;
            }
        }

        return inSampleSize;
    }

    private static Bitmap decodeSampledBitmapFromFile(String fileName, int reqWidth, int reqHeight) {

        // First decode with inJustDecodeBounds=true to check dimensions
        final BitmapFactory.Options options = new BitmapFactory.Options();
        options.inJustDecodeBounds = true;
        BitmapFactory.decodeFile(fileName, options);

        // Calculate inSampleSize
        options.inSampleSize = calculateInSampleSize(options, reqWidth, reqHeight);

        // Decode bitmap with inSampleSize set
        options.inJustDecodeBounds = false;
        return BitmapFactory.decodeFile(fileName, options);
    }

    public static File getCompressedImage(Context context, File file, int width, int height) {
        File f = null;
        try {
            f = new File(context.getCacheDir(), file.getName());
            f.createNewFile();

            //Convert bitmap to byte array
            Bitmap bitmap = decodeSampledBitmapFromFile(file.getPath(), width, height);
            ByteArrayOutputStream bos = new ByteArrayOutputStream();
            bitmap.compress(Bitmap.CompressFormat.PNG, 0, bos);
            byte[] bitmapData = bos.toByteArray();

            //write the bytes in file
            FileOutputStream fos = new FileOutputStream(f);
            fos.write(bitmapData);
            fos.flush();
            fos.close();

        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }

        return f;
    }

    public static boolean isValidEmail(String email) {
        return EMAIL_ADDRESS.matcher(email).matches();
    }

    /**
     * Gets discounted price
     *
     * @param price    : FrameLayout for the fragment
     * @param discount : Request Tag for adding to backstack
     */
    public static String getPriceWithDiscount(String price, String discount) {
        double value = Double.parseDouble(price);
        double percentage = 0;
        try {
            percentage = Double.parseDouble(discount);
        } catch (NumberFormatException e) {
            e.printStackTrace();
        }
        if (percentage <= 0)
            return formatAmount(price);
        return formatAmount(String.format("%.2f", value - (value * (percentage / 100.0f))));
    }

    /**
     * Returns list of quantities
     *
     * @param start : Will be 1
     * @param end   : Max quantity
     */
    public static List<String> getQtys(int start, int end) {
        List<String> items = new ArrayList<>();
        for (int i = start; i <= end; i++) {
            items.add(i + "");
        }
        return items;
    }

    public static String formatAmount(Integer value) {
        return formatAmount(String.valueOf(value));
    }

    public static String formatAmount(Double value) {
        return formatAmount(String.valueOf(value));
    }

    public static String formatAmount(String value) {
        try {
            float number = Float.parseFloat(value);
            DecimalFormat df;
            if ((int) number == number)
                df = new DecimalFormat("##,##,##,##,##,##,###");
            else
                df = new DecimalFormat("##,##,##,##,##,##,###");
            return df.format(number);
        } catch (Exception e) {
            return value;
        }
    }

    /**
     * Padds the amount with white spaces
     *
     * @param amount      : Amount
     * @param rupeeSymbol : if true it'll append rupee symbol at start
     */
    public static String getPaddedAmount(Double amount, boolean rupeeSymbol) {
        String result = rupeeSymbol ? Constants.RUPEE + Constants.formatAmount(amount) : "" + Constants.formatAmount(amount);
        int pad = 16 - result.length();
        for (int i = 0; i < pad; i++) {
            result = " " + result;
        }
        return result;
    }

    public static String toCardExpiryFormat(int year, int month) {
        month = month + 1;
        return (String.valueOf(month).length() > 1 ? String.valueOf(month) : "0" + month) + "/" + String.valueOf(year).substring(2);
    }


    public enum Records {
        INSERT_ALL, INSERT, UPDATE, SELECT, RAW_QUERY, DELETE, DELETE_ALL
    }

    public enum SizeTypes {
        DRESS, PANT, SHOE
    }

    public enum OrderAction {
        VIEW, RETURN_EXCHANGE, CANCEL
    }

    public enum Attributes {
        BODY_SHAPE, COMPLEXION, SHOE_SIZES, BOTTOM_SIZE, TOP_SIZE, HEIGHT, STYLE, FACE_SHAPE
    }

    public enum AddressType {
        same {
            @Override
            public String toString() {
                return "same";
            }
        }, previous {
            @Override
            public String toString() {
                return "previous";
            }
        }, new_address {
            @Override
            public String toString() {
                return "new_address";
            }
        }

    }

    public enum RefundType {
        credit {
            @Override
            public String toString() {
                return "credit";
            }
        }, bank_deposit {
            @Override
            public String toString() {
                return "bank_deposit";
            }
        }, wallet {
            @Override
            public String toString() {
                return "wallet";
            }
        }
    }
}
