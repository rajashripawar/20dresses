package outletwise.com.twentydresses.utilities;

/**
 * Created by User-PC on 12-08-2015.
 */
public class Urls {
    private static String DOMAIN_NAME_PORT_80 = "https://20dresses.com/";
    //private static final String webServer = "api/";

//    private static final String webServer = "api2/";
private static final String webServer = "api3/";
    public static final String BASE_URL = DOMAIN_NAME_PORT_80 + webServer;

    //Sub urls
    public static final String LOGIN = BASE_URL + "login";
    public static final String FORGOT_PASSWORD = BASE_URL + "forgot-password";
    public static final String VISITOR = BASE_URL + "visitor";
    public static final String REGISTRATION = BASE_URL + "register";// Registration api for v1.0
    public static final String REGISTRATION_USER = BASE_URL + "register-user"; // Registration api for v1.1
    public static final String ADD_QUIZ = BASE_URL + "add-quiz"; // Registration api for v1.1
    public static final String PRODUCTS = BASE_URL + "products";
    public static final String SINGLE_PRODUCT = BASE_URL + "product";
    // public static final String CATEGORIES = BASE_URL + "categories-list";
    public static final String CATEGORIES = BASE_URL + "menu-list";
    private static final String SHORTLIST = BASE_URL + "shortlist/";

    public static final String SHORTLIST_ADD = SHORTLIST + "add";
    public static final String SHORTLIST_REMOVE = SHORTLIST + "remove";
    public static final String SHORTLIST_VIEW = SHORTLIST + "view";

    public static final String CHK_DEL_OPTS = BASE_URL + "pincode";
    private static final String CART = BASE_URL + "cart/";
    public static final String CART_ADD = CART + "add";
    public static final String CART_DELETE = CART + "delete";
    public static final String CART_VIEW = CART + "view";
    public static final String CART_UPDATE = CART + "update";

    private static final String ORDER = BASE_URL + "order/";
    public static final String ORDER_LIST = ORDER + "list";
    public static final String ORDER_VIEW = ORDER + "view";
    public static final String ORDER_PLACE = ORDER + "place";
    public static final String ORDER_CANCEL_REASONS = ORDER + "reasons";
    public static final String ORDER_RETURN_REASONS = ORDER + "return-item";
    public static final String ORDER_EXCHANGE_REASONS = ORDER + "exchange-item";
    public static final String ORDER_RETURN = ORDER + "return";
    public static final String ORDER_CANCEL = ORDER + "cancel";
    public static final String ORDER_RETURN_EXCHANGE = ORDER + "update-returns";
    public static final String WALLET = BASE_URL + "wallet";

    private static final String ADDRESS = BASE_URL + "address/";
    public static final String ADDRESS_VIEW = ADDRESS + "view";
    public static final String ADDRESS_ADD = ADDRESS + "add";
    public static final String ADDRESS_UPDATE = ADDRESS + "update";
    public static final String ADDRESS_DELETE = ADDRESS + "delete";
    private static final String USER = BASE_URL + "user/";
    public static final String USER_VERIFY_MOBILE = USER + "verify-mobile";
    public static final String REFERRAL_MOBILE_VERIFY = USER + "verify-user-mobile";
    public static final String USER_WALLET = USER + "wallet";
    public static final String TESTIMONIALS = BASE_URL + "testimonials";


    public static final String USER_TESTIMONIALS = BASE_URL + "user-testimonial";
    public static final String USER_TESTIMONIALS_SOCIAL = BASE_URL + "testimonials-social";
    public static final String YOU_MAY_LIKE = BASE_URL + "youmayalsolike";
    public static final String CHECKSUM_GENERATOR = "https://20dresses.com/api/order/paytm-generate";
    public static final String CHECKSUM_VALIDATE = "https://20dresses.com/api/order/paytm-verify";
    public static final String FOR_YOU = BASE_URL + "tips";
    public static final String FOR_YOU_PRODUCTS = BASE_URL + "tip-products";
    public static final String FILTERS = BASE_URL + "filters";
    public static final String OFFERS = BASE_URL + "offers";
    public static final String HOME_BANNERS = BASE_URL + "banners";

    public static final String CONFIRM_PREPAID = ORDER + "confirm_prepaid";
    public static final String SEND_INVOICE = ORDER + "send_invoice";
    public static final String CHK_REF_CODE = BASE_URL + "refercode";
    public static final String IMG_BACKGROUND = BASE_URL + "background-images";
    public static final String TRENDS = BASE_URL + "trends";
    public static final String INVITE_REWARDS = BASE_URL + "invite/rewards";
    public static final String TREND_PRODUCTS = BASE_URL + "trend-products";
    public static final String JUST_SOLD_PRODUCTS = BASE_URL + "just-sold";
    public static final String FOR_YOU_QUIZ = BASE_URL + "quiz/question";
    // public static final String BANNERS = BASE_URL + "banners";
    public static final String PROFILE = BASE_URL + "profile";

    public static final String UPDATE_PROFILE = BASE_URL + "update-profile";
    public static final String UPDATE_PROFILE_PIC = USER + "update-profile-pic";
    public static final String UPDATE_STYLE_PROFILE = BASE_URL + "update-style-profile";
    public static final String STYLE_PROFILE = BASE_URL + "style-profile";
    public static final String CHANGE_PASSWORD = USER + "change-password";
    public static final String CREDITS = USER + "credits";
    public static final String REFER = USER + "refer";
    public static final String REFUND_WALLET = USER + "refund-wallet";
    public static final String CONTACT_US = BASE_URL + "contactus";
    public static final String ABOUT_US = BASE_URL + "aboutus";

    //   public static final String HELP_FAQ = BASE_URL + "faq-help";
    public static final String HELP_FAQ = BASE_URL + "help-faq";
    public static final String TERMS = BASE_URL + "terms";
    public static final String PRIVACY_POLICY = BASE_URL + "privacy";
    public static final String NOTIFY_PRODUCT = BASE_URL + "notify-product";
    public static final String CHECKOUT_CART = ORDER + "checkout-cart";
    public static final String CHECK_APP_VERSION = BASE_URL + "updates";
    public static final String INVITE_FAQ = BASE_URL + "invite/faq";
    public static final String SEARCH = BASE_URL + "search";

}
