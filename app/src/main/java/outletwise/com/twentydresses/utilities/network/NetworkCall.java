package outletwise.com.twentydresses.utilities.network;

import android.app.Activity;
import android.widget.Toast;

import com.android.volley.DefaultRetryPolicy;
import com.android.volley.NetworkError;
import com.android.volley.NoConnectionError;
import com.android.volley.Request;
import com.android.volley.Request.Method;
import com.android.volley.Response;
import com.android.volley.ServerError;
import com.android.volley.TimeoutError;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonArrayRequest;
import com.android.volley.toolbox.JsonObjectRequest;
import com.android.volley.toolbox.StringRequest;
import com.google.gson.Gson;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.HashMap;
import java.util.Map;

import outletwise.com.twentydresses.ApplicationUtil;
import outletwise.com.twentydresses.model.AddCartResponse;
import outletwise.com.twentydresses.model.Address;
import outletwise.com.twentydresses.model.AppVersionResponse;
import outletwise.com.twentydresses.model.BackgroundImgResponse;
import outletwise.com.twentydresses.model.CartDeleteItemResponse;
import outletwise.com.twentydresses.model.Category;
import outletwise.com.twentydresses.model.ContactUs;
import outletwise.com.twentydresses.model.CreditResponse;
import outletwise.com.twentydresses.model.Filter;
import outletwise.com.twentydresses.model.ForyouQuizResponse;
import outletwise.com.twentydresses.model.HomeBanner;
import outletwise.com.twentydresses.model.JustSoldResponse;
import outletwise.com.twentydresses.model.LoginResponse;
import outletwise.com.twentydresses.model.NewAddressResponse;
import outletwise.com.twentydresses.model.Offer;
import outletwise.com.twentydresses.model.Order;
import outletwise.com.twentydresses.model.OrderItem;
import outletwise.com.twentydresses.model.OrderReturnResponse;
import outletwise.com.twentydresses.model.PinCodeResponse;
import outletwise.com.twentydresses.model.PlaceOrderResponse;
import outletwise.com.twentydresses.model.Product;
import outletwise.com.twentydresses.model.Profile;
import outletwise.com.twentydresses.model.ProfilePictureResponse;
import outletwise.com.twentydresses.model.Reason;
import outletwise.com.twentydresses.model.Recommended;
import outletwise.com.twentydresses.model.ReferCodeResponse;
import outletwise.com.twentydresses.model.RegisterResponse;
import outletwise.com.twentydresses.model.Rewards;
import outletwise.com.twentydresses.model.ShortListResponse;
import outletwise.com.twentydresses.model.StyleProfile;
import outletwise.com.twentydresses.model.SubTip;
import outletwise.com.twentydresses.model.SyncCart;
import outletwise.com.twentydresses.model.Terms;
import outletwise.com.twentydresses.model.Testimonial;
import outletwise.com.twentydresses.model.TrendProducts;
import outletwise.com.twentydresses.model.Trends;
import outletwise.com.twentydresses.model.VerifyMobileResponse;
import outletwise.com.twentydresses.model.VisitorResponse;
import outletwise.com.twentydresses.model.WalletResponse;
import outletwise.com.twentydresses.model.database.greenbot.Shortlist;
import outletwise.com.twentydresses.utilities.Constants;

/**
 * Created by User-PC on 11-08-2015.
 */
public class NetworkCall {

    // Error code 99 if response does not contains any data
    private static HashMap<Integer, String> networkResponse = new HashMap<>();

    static {
        networkResponse.put(204, Constants.NO_MORE_RESULTS);
    }

    private NetworkReceiver mReceiver;
    private Activity mActivity;
    private String app_key = "app_key";

    public NetworkCall(Activity mActivity, NetworkReceiver mReceiver) {
        this.mReceiver = mReceiver;
        this.mActivity = mActivity;
    }

    private static String getErrorMessage(int errCode) {
        return networkResponse.get(errCode) != null ? networkResponse.get(errCode) : Constants.SERVER_ERROR;
    }

    /**
     * @return User userId stored in session.
     */
    protected String getUserId() {
        // get user data from session
        String userId = "0";

        Constants.debug("Network Call User Id " + userId);
        return userId;
    }


    /**
     * @param URL         : URL to fetch data from.
     * @param jsonRequest : JSONObject to be posted on server.
     * @param reqId       : Volley Request Id.
     */
    public void postJsonDataArray(String URL, final JSONObject jsonRequest, final int reqId) {
        //Add default app_key
        try {
            jsonRequest.put(app_key, Constants.APP_KEY);
        } catch (JSONException e) {
            e.printStackTrace();
        }
        Constants.debug("url: " + URL + "\nBody: " + jsonRequest.toString());
        updatePref();

        final JsonArrayRequest jsonObjReq = new JsonArrayRequest(Request.Method.POST, URL,
                jsonRequest, new Response.Listener<JSONArray>() {
            @Override
            public void onResponse(JSONArray response) {
                Constants.debug(response.toString());
                parseJsonData(response, reqId);
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError err) {
                if (err != null) {
                    if (err.getMessage() != null)
                        Constants.error(err.getMessage());
                    int errorCode = (err.networkResponse != null) ? err.networkResponse.statusCode : -1;
                    mReceiver.onError(errorCode, reqId, jsonRequest);
                } else
                    mReceiver.onError(-1, reqId, jsonRequest);

            }
        });
//        {
//            *
//             * Passing some request headers
//             *
//            @Override
//            public Map<String, String> getHeaders() throws AuthFailureError {
//                HashMap<String, String> headers = new HashMap<>();
//                headers.put("userId", getUserId());
//                return headers;
//            }
//        };
        jsonObjReq.setShouldCache(false);

        jsonObjReq
                .setRetryPolicy(new DefaultRetryPolicy(Constants.REQUEST_TIMEOUT,
                        Constants.MAX_RETRIES, Constants.BACKOFF_MULT));
        // Adding request to request queue
        ApplicationUtil.getInstance().addToRequestQueue(jsonObjReq, reqId + "");

    }
    /**
     * @param URL         : URL to fetch data from.
     * @param jsonRequest : JSONObject to be posted on server.
     * @param reqId       : Volley Request Id.
     */
    public void postJsonData(String URL, final JSONObject jsonRequest, final int reqId) {
        //Add default app_key
        try {
            jsonRequest.put(app_key, Constants.APP_KEY);
        } catch (JSONException e) {
            e.printStackTrace();
        }
        Constants.debug("url: " + URL + "\nBody: " + jsonRequest.toString());
        updatePref();

        final JsonObjectRequest jsonObjReq = new JsonObjectRequest(Request.Method.POST, URL,
                jsonRequest, new Response.Listener<JSONObject>() {
            @Override
            public void onResponse(JSONObject response) {
                Constants.debug(response.toString());
                parseJsonData(response, reqId);
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError err) {
                if (err != null) {
                    err.printStackTrace();
                    if (err.getMessage() != null)
                        Constants.error(err.getMessage());
                    if (reqId != Constants.TAG_FOR_YOU)
                        showError(err);
                    int errorCode = (err.networkResponse != null) ? err.networkResponse.statusCode : -1;
                    mReceiver.onError(errorCode, reqId, jsonRequest);
                } else
                    mReceiver.onError(-1, reqId, jsonRequest);
            }
        });
//        {
//            /**
//             * Passing some request headers
//             * */
//            @Override
//            public Map<String, String> getHeaders() throws AuthFailureError {
//                HashMap<String, String> headers = new HashMap<>();
//                headers.put("userId", getUserId());
//                return headers;
//            }
//        };
        jsonObjReq.setShouldCache(false);

        jsonObjReq
                .setRetryPolicy(new DefaultRetryPolicy(Constants.REQUEST_TIMEOUT,
                        Constants.MAX_RETRIES, Constants.BACKOFF_MULT));
        // Adding request to request queue
        ApplicationUtil.getInstance().addToRequestQueue(jsonObjReq, reqId + "");

    }

    /**
     * @param URL : URL to clear cache.
     */
    public void clearUrlCache(String URL) {
        try {
            ApplicationUtil.getInstance().getRequestQueue().getCache()
                    .remove(URL);
        } catch (Exception ex) {
            ex.printStackTrace();
        }
    }

    /**
     * @param URL   : URL to fetch data from
     * @param reqId : Volley Request Id.
     */
    public void getJsonData(String URL, final int reqId) {
        Constants.debug(URL);
        updatePref();
        JsonObjectRequest jsonObjReq = new JsonObjectRequest(Method.GET, URL,
                (String) null, new Response.Listener<JSONObject>() {
            @Override
            public void onResponse(JSONObject response) {
                Constants.debug(response.toString());
                parseJsonData(response, reqId);
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError err) {
                if (err != null) {
                    if (err.getMessage() != null)
                        Constants.error(err.getMessage());
                    showError(err);
                    int errorCode = (err.networkResponse != null) ? err.networkResponse.statusCode : -1;
                    mReceiver.onError(errorCode, reqId, null);
                } else
                    mReceiver.onError(-1, reqId, null);
            }
        });
//        {
//            /**
//             * Passing some request headers
//             * */
//            @Override
//            public Map<String, String> getHeaders() throws AuthFailureError {
//                HashMap<String, String> headers = new HashMap<>();
//                headers.put("userId", getUserId());
//                return headers;
//            }
//        };
//        if (!URL.equalsIgnoreCase(Constants.CATEGORIES_URL))
//            jsonObjReq.setShouldCache(false);
        jsonObjReq
                .setRetryPolicy(new DefaultRetryPolicy(Constants.REQUEST_TIMEOUT,
                        Constants.MAX_RETRIES, Constants.BACKOFF_MULT));
        // Adding request to request queue
        ApplicationUtil.getInstance().addToRequestQueue(jsonObjReq, reqId + "");
    }

    /**
     * @param URL   : URL to fetch data from
     * @param reqId : Volley Request Id.
     */
    public void getJsonArrayData(String URL, final int reqId) {
        updatePref();
        JsonArrayRequest jsonArrayRequest = new JsonArrayRequest(URL,
                new Response.Listener<JSONArray>() {
                    @Override
                    public void onResponse(JSONArray jsonArray) {
                        Constants.debug("JSONArray " + jsonArray.toString());
                        parseJsonData(jsonArray, reqId);
                    }
                }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError err) {
                if (err != null) {
                    if (err.getMessage() != null)
                        Constants.error(err.getMessage());
                    showError(err);
                    int errorCode = (err.networkResponse != null) ? err.networkResponse.statusCode : -1;
                    mReceiver.onError(errorCode, reqId, null);
                } else
                    mReceiver.onError(-1, reqId, null);
            }
        });
//        {
//
//            /**
//             * Passing some request headers
//             * */
//            @Override
//            public Map<String, String> getHeaders() throws AuthFailureError {
//                HashMap<String, String> headers = new HashMap<>();
//                headers.put("userId", getUserId());
//                return headers;
//            }
//        };

        jsonArrayRequest.setShouldCache(false);
        jsonArrayRequest
                .setRetryPolicy(new DefaultRetryPolicy(Constants.REQUEST_TIMEOUT,
                        Constants.MAX_RETRIES, Constants.BACKOFF_MULT));

        ApplicationUtil.getInstance().addToRequestQueue(jsonArrayRequest,
                reqId + "");
    }

    /**
     * @param obj JSONObject to be parsed.
     * @param tag Volley Request Tag.
     */
    public void parseJsonData(JSONObject obj, int tag) {
        Gson gson = new Gson();
        switch (tag) {
            case Constants.TAG_LOGIN:
                LoginResponse loginResponse = gson.fromJson(obj.toString(), LoginResponse.class);
                mReceiver.onResponse(loginResponse, tag);
                break;
            case Constants.TAG_DEL_ADDRESS:
            case Constants.TAG_UPDATE_ADDRESS:
            case Constants.TAG_ADD_QUIZ:
            case Constants.TAG_NOTIFY_PRODUCT:
            case Constants.TAG_CONFIRM_PREPAID:
            case Constants.TAG_CHANGE_PASSWORD:
            case Constants.TAG_UPDATE_STYLE_PROFILE:
            case Constants.TAG_UPDATE_PROFILE:
            case Constants.TAG_ORDER_RETURN_EXCHANGE:
            case Constants.TAG_ORDER_CANCEL:
            case Constants.TAG_CHK_REF_CODE:
            case Constants.TAG_VISITOR:
                VisitorResponse response = gson.fromJson(obj.toString(), VisitorResponse.class);
                mReceiver.onResponse(response, tag);
                break;
            case Constants.TAG_REGISTRATION:
                RegisterResponse registerResponse = gson.fromJson(obj.toString(), RegisterResponse.class);
                mReceiver.onResponse(registerResponse, tag);
                break;
            case Constants.TAG_SHORTLIST_REMOVE:
            case Constants.TAG_SHORTLIST:

                ShortListResponse shortListResponse = gson.fromJson(obj.toString(), ShortListResponse.class);

                if (shortListResponse.getShortlistProduct() != null)
                    mReceiver.onResponse(shortListResponse.getShortlistProduct(), tag);
                else {
                    showToast(shortListResponse.getMessage());
                    mReceiver.onError(99, tag, obj.toString());
                }

                break;

            case Constants.TAG_CART_UPDATE:
            case Constants.TAG_CART_ADD:
                AddCartResponse addCartResponse = gson.fromJson(obj.toString(), AddCartResponse.class);
                mReceiver.onResponse(addCartResponse, tag);
                break;
            case Constants.TAG_CART_VIEW:
                SyncCart syncCart = gson.fromJson(obj.toString(), SyncCart.class);
                mReceiver.onResponse(syncCart, tag);
                break;

            case Constants.TAG_CART_DELETE:
                CartDeleteItemResponse cartDeleteItemResponse = gson.fromJson(obj.toString(), CartDeleteItemResponse.class);
                mReceiver.onResponse(cartDeleteItemResponse, tag);
                break;
            case Constants.TAG_ORDER_VIEW:
                OrderItem orderItem = gson.fromJson(obj.toString(), OrderItem.class);
                mReceiver.onResponse(orderItem, tag);
                break;
            case Constants.TAG_CREDITS:
                CreditResponse creditResponse = gson.fromJson(obj.toString(), CreditResponse.class);
                mReceiver.onResponse(creditResponse, tag);
                break;
            case Constants.TAG_WALLET:
                WalletResponse walletResponse = gson.fromJson(obj.toString(), WalletResponse.class);
                mReceiver.onResponse(walletResponse, tag);
                break;
            case Constants.TAG_ORDER_PLACE:
                PlaceOrderResponse placeOrderResponse = gson.fromJson(obj.toString(), PlaceOrderResponse.class);
                mReceiver.onResponse(placeOrderResponse, tag);
                break;
            case Constants.TAG_USER_VERIFY_MOBILE:
            case Constants.TAG_USER_CONFIRM_MOBILE:
                VerifyMobileResponse verifyMobileResponse = gson.fromJson(obj.toString(), VerifyMobileResponse.class);
                mReceiver.onResponse(verifyMobileResponse, tag);
                break;
            case Constants.TAG_FOR_YOU:
                Recommended recommended = gson.fromJson(obj.toString(), Recommended.class);
                mReceiver.onResponse(recommended, tag);
                break;
            case Constants.TAG_OFFERS:
                Offer offer = gson.fromJson(obj.toString(), Offer.class);
                mReceiver.onResponse(offer, tag);
                break;
            case Constants.TAG_PROFILE:
                Profile profile = gson.fromJson(obj.toString(), Profile.class);
                mReceiver.onResponse(profile, tag);
                break;
            case Constants.TAG_STYLE_PROFILE:
                StyleProfile styleProfile = gson.fromJson(obj.toString(), StyleProfile.class);
                mReceiver.onResponse(styleProfile, tag);
                break;
            case Constants.TAG_ORDER_RETURN:
                OrderReturnResponse returnResponse = gson.fromJson(obj.toString(), OrderReturnResponse.class);
                mReceiver.onResponse(returnResponse, tag);
                break;
            case Constants.TAG_ORDER_RETURN_REASONS:
            case Constants.TAG_ORDER_EXCHANGE_REASONS:
                Reason reason = gson.fromJson(obj.toString(), Reason.class);
                mReceiver.onResponse(reason, tag);
                break;
            case Constants.TAG_ADD_ADDRESS:
                NewAddressResponse resonse = gson.fromJson(obj.toString(), NewAddressResponse.class);
                mReceiver.onResponse(resonse, tag);
                break;
            case Constants.TAG_REFER:
                ReferCodeResponse referCodeResponse = gson.fromJson(obj.toString(), ReferCodeResponse.class);
                mReceiver.onResponse(referCodeResponse, tag);
                break;
            case Constants.TAG_UPDATE_PROFILE_PIC:
                ProfilePictureResponse profilePicResponse = gson.fromJson(obj.toString(), ProfilePictureResponse.class);
                mReceiver.onResponse(profilePicResponse, tag);
                break;
            case Constants.TAG_CHK_DEL_OPTS_FRAG:
            case Constants.TAG_CHK_DEL_OPTS:
                PinCodeResponse pinCodeResponse = gson.fromJson(obj.toString(), PinCodeResponse.class);
                mReceiver.onResponse(pinCodeResponse, tag);
                break;
            case Constants.TAG_IMG_BACKGROUND:
                BackgroundImgResponse imgResponse = gson.fromJson(obj.toString(), BackgroundImgResponse.class);
                mReceiver.onResponse(imgResponse, tag);
                break;
            case Constants.TAG_USER_TESTIMONIALS:
                Testimonial userTestimonialResponse = gson.fromJson(obj.toString(), Testimonial.class);
                mReceiver.onResponse(userTestimonialResponse, tag);
                break;
            case Constants.TAG_ABOUT_US:
            case Constants.TAG_CONTACT_US:
                ContactUs contactResponse = gson.fromJson(obj.toString(), ContactUs.class);
                mReceiver.onResponse(contactResponse, tag);
                break;
            case Constants.TAG_INVITE_FAQ:
                String faqResponse = obj.toString();
                mReceiver.onResponse(faqResponse, tag);
                break;
            case Constants.TAG_HELP_FAQ:
                // String faqResponse = obj.toString();
                mReceiver.onResponse(obj, tag);
                break;
            case Constants.TAG_PRIVACY_POLICY:
            case Constants.TAG_TERMS:
                Terms termsresponse = gson.fromJson(obj.toString(), Terms.class);
                mReceiver.onResponse(termsresponse, tag);
               /* String termsResponse = obj.toString();
                mReceiver.onResponse(termsResponse, tag);*/
                break;
            case Constants.TAG_REFUND_WALLET:
                VisitorResponse refundResponse = gson.fromJson(obj.toString(), VisitorResponse.class);
                mReceiver.onResponse(refundResponse, tag);
                break;
            case Constants.TAG_CHECK_APP_VESRION:
                AppVersionResponse appVesrionResponse = gson.fromJson(obj.toString(), AppVersionResponse.class);
                mReceiver.onResponse(appVesrionResponse, tag);
                break;
            case Constants.TAG_SHORTLIST_VIEW:
                if (!obj.toString().contains("No items shortlisted")) {
                    Shortlist shortlistViewResponse = gson.fromJson(obj.toString(), Shortlist.class);
                    mReceiver.onResponse(shortlistViewResponse, tag);
                } else
                    mReceiver.onError(99, tag, obj.toString());
                break;
            case Constants.TAG_TRENDS:
                Trends trends = gson.fromJson(obj.toString(), Trends.class);
                mReceiver.onResponse(trends, tag);
                break;
            case Constants.TAG_INVITE_REWARDS:
              /*  Type mapType = new TypeToken<Map<String,Map<String, String>>>() {}.getType();
                Map<String,Map<String, String>> map = gson.fromJson(obj.toString(), mapType);*/
                Rewards rewards = gson.fromJson(obj.toString(), Rewards.class);
                mReceiver.onResponse(rewards, tag);
                break;
            case Constants.TAG_HOME_BANNERS:
                HomeBanner bannerResponse = gson.fromJson(obj.toString(), HomeBanner.class);
                mReceiver.onResponse(bannerResponse, tag);
            /*    if (!response.equalsIgnoreCase(Constants.NO_MORE_RESULTS)) {
                    HomeBanner banners = gson.fromJson(response, HomeBanner.class);
                    mReceiver.onResponse(banners, tag);
                } else {
                    mReceiver.onError(99, tag, response);
                }*/
                break;
            case Constants.TAG_FOR_YOU_QUIZ:
                ForyouQuizResponse quizResponse = gson.fromJson(obj.toString(), ForyouQuizResponse.class);
                mReceiver.onResponse(quizResponse, tag);
                break;
            default:
                break;
        }
    }

    /**
     * @param jsonArray : JsonArray to be parsed.
     * @param tag       : Volley Request Tag.
     */

    @SuppressWarnings("unchecked")
    public void parseJsonData(JSONArray jsonArray, int tag) {
        Gson gson = new Gson();
        switch (tag) {
            case Constants.TAG_ORDER_LIST:
                Order orders[] = gson.fromJson(jsonArray.toString(), Order[].class);
                mReceiver.onResponse(orders, tag);
                break;
            case Constants.TAG_ADDRESS_VIEW:
                Address addresses[] = gson.fromJson(jsonArray.toString(), Address[].class);
                mReceiver.onResponse(addresses, tag);
                break;
            case Constants.TAG_TESTIMONIALS:
                Testimonial testimonials[] = gson.fromJson(jsonArray.toString(), Testimonial[].class);
                mReceiver.onResponse(testimonials, tag);
                break;
            case Constants.TAG_YOU_MAY_LIKE:
                Product products[] = gson.fromJson(jsonArray.toString(), Product[].class);
                mReceiver.onResponse(products, tag);
                break;
            case Constants.TAG_ORDER_CANCEL_REASONS:
                Reason.ReasonsEntity reasonsEntity[] = gson.fromJson(jsonArray.toString(), Reason.ReasonsEntity[].class);
                mReceiver.onResponse(reasonsEntity, tag);
                break;
            default:
                break;
        }
    }

    /**
     * @param response
     * @param tag
     */
    public void parseJsonString(String response, int tag) {
        Gson gson = new Gson();
        switch (tag) {
            /*case Constants.TAG_LOGIN:
                LoginResponse loginResponse = gson.fromJson(response, LoginResponse.class);
                mReceiver.onResponse(loginResponse, tag);
                break;*/
            case Constants.TAG_JUST_SOLD:
                JustSoldResponse justSoldResponse = gson.fromJson(response, JustSoldResponse.class);
                mReceiver.onResponse(justSoldResponse, tag);
                break;
            // case Constants.TAG_SEARCH:
            case Constants.TAG_PRODUCTS:
                if (!response.contains(Constants.NO_MORE_RESULTS)) {
//                    SubTip tips = gson.fromJson(response, SubTip.class);
//                    mReceiver.onResponse(tips, tag);
                    Product products[] = gson.fromJson(response, Product[].class);
                    mReceiver.onResponse(products, tag);
                } else {
                    mReceiver.onError(99, tag, response);
                }
                break;
            case Constants.TAG_OFFERS:
                if (!response.contains(Constants.NO_MORE_RESULTS)) {
                    Offer products[] = gson.fromJson(response, Offer[].class);
                    mReceiver.onResponse(products, tag);
                } else {
                    mReceiver.onError(99, tag, response);
                }
                break;

            case Constants.TAG_FILTER_PRODUCTS:
                if (!response.contains(Constants.NO_MORE_RESULTS)) {
                    Product products[] = gson.fromJson(response, Product[].class);
                    mReceiver.onResponse(products, tag);
                } else {
                    mReceiver.onError(99, tag, response);
                }
                break;
            case Constants.TAG_CATEGORIES:
                Category categories[] = gson.fromJson(response, Category[].class);
                mReceiver.onResponse(categories, tag);
                break;
            case Constants.TAG_SINGLE_PRODUCT:
                if (!response.contains(Constants.NO_MORE_RESULTS)) {
                    Product product = gson.fromJson(response, Product.class);
                    mReceiver.onResponse(product, tag);
                } else {
                    mReceiver.onError(99, tag, response);
                }
                break;
            case Constants.TAG_CHK_DEL_OPTS_FRAG:
            case Constants.TAG_CHK_DEL_OPTS:
                PinCodeResponse pinCodeResponse = gson.fromJson(response, PinCodeResponse.class);
                mReceiver.onResponse(pinCodeResponse, tag);
                break;
            case Constants.TAG_FOR_YOU_PRODUCTS:
                SubTip subTip = gson.fromJson(response, SubTip.class);
                mReceiver.onResponse(subTip, Constants.TAG_FOR_YOU_PRODUCTS);
                break;
            case Constants.TAG_TREND_PRODUCTS:
                TrendProducts trendProducts = gson.fromJson(response, TrendProducts.class);
                mReceiver.onResponse(trendProducts, Constants.TAG_TREND_PRODUCTS);
                break;
            case Constants.TAG_FILTER:
                Filter filter = gson.fromJson(response, Filter.class);
                mReceiver.onResponse(filter, Constants.TAG_FILTER);
                break;
            case Constants.TAG_CHK_REF_CODE:
            case Constants.TAG_FORGOT_PASSWORD:
                VisitorResponse visitorResponse = gson.fromJson(response, VisitorResponse.class);
                mReceiver.onResponse(visitorResponse, tag);
                break;

            default:
                break;
        }
    }

    /**
     * @param URL
     * @param params
     * @param reqId
     */
    public void postStringData(final String URL, final Map<String, String> params,
                               final int reqId) {
        updatePref();
        StringRequest stringRequest = new StringRequest(Request.Method.POST, URL,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        Constants.debug(response);
                        parseJsonString(response, reqId);
                    }

                }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError err) {
                if (err != null) {
                    if (err.getMessage() != null)
                        Constants.error(err.getMessage());
                    if (reqId != Constants.TAG_OFFERS)
                        showError(err);
                    int errorCode = (err.networkResponse != null) ? err.networkResponse.statusCode : -1;
                    mReceiver.onError(errorCode, reqId, params);
                } else
                    mReceiver.onError(-1, reqId, params);
            }
        }) {

            protected Map<String, String> getParams()
                    throws com.android.volley.AuthFailureError {
                params.put(app_key, Constants.APP_KEY);
                Constants.debug("url: " + URL + "\nParams: " + params.toString());
                return params;
            }

//            /**
//             * Passing some request headers
//             * */
//            @Override
//            public Map<String, String> getHeaders() throws AuthFailureError {
//                HashMap<String, String> headers = new HashMap<>();
//                headers.put("userId", getUserId());
//                return headers;
//            }
        };
        stringRequest
                .setRetryPolicy(new DefaultRetryPolicy(Constants.REQUEST_TIMEOUT,
                        Constants.MAX_RETRIES, Constants.BACKOFF_MULT));
        stringRequest.setShouldCache(false);
        // Adding request to request queue
        ApplicationUtil.getInstance().addToRequestQueue(stringRequest,
                reqId + "");
    }

    public void showError(VolleyError error) {
        if (error instanceof NetworkError) {
            showToast(Constants.NETWORK_ERROR);
        } else if (error instanceof ServerError) {
            if (error.networkResponse != null)
                showToast(getErrorMessage(error.networkResponse.statusCode));
            else
                showToast(Constants.SERVER_ERROR);
        } else if (error instanceof NoConnectionError) {
            showToast(Constants.NO_INTERNET_CONNECTION);
        } else if (error instanceof TimeoutError) {
            showToast(Constants.CONNECTION_TIME_OUT);
        } else {
            showToast(Constants.SERVER_ERROR);
        }
    }

    private void showToast(String message) {
        Toast.makeText(mActivity, message, Toast.LENGTH_LONG).show();
    }

    public void updatePref() {
    }
}