package outletwise.com.twentydresses.view.activity;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.ActionBar;
import android.text.TextUtils;
import android.view.View;
import android.widget.RadioButton;
import android.widget.TextView;

import com.wdullaer.materialdatetimepicker.date.DatePickerDialog;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.GregorianCalendar;

import outletwise.com.twentydresses.R;
import outletwise.com.twentydresses.model.Profile;
import outletwise.com.twentydresses.model.VisitorResponse;
import outletwise.com.twentydresses.model.database.greenbot.User;
import outletwise.com.twentydresses.utilities.Constants;
import outletwise.com.twentydresses.utilities.Urls;

/**
 * Created by User-PC on 02-12-2015.
 * This class is use to show profile information of user.
 * User can update profile from here.
 */
public class MyAccount extends BaseActivity implements DatePickerDialog.OnDateSetListener {
    private TextView tvFirstName, tvLastName, tvEmail, tvMobile, tvBio, tvDateDay, tvDateMonth, tvDateYear;
    private String selectedGender = "F";
    RadioButton rbFemale, rbMale;
    private User user;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_my_account);

        setUpToolBar(getString(R.string.toolbar_my_account));
        final ActionBar ab = getSupportActionBar();
        ab.setDisplayHomeAsUpEnabled(true);

        init();
    }

    void init() {
        tvFirstName = (TextView) findViewById(R.id.tvFirstName);
        tvLastName = (TextView) findViewById(R.id.tvLastName);
        tvEmail = (TextView) findViewById(R.id.tvEmail);
        tvMobile = (TextView) findViewById(R.id.tvMobile);
        tvBio = (TextView) findViewById(R.id.tvBio);


        findViewById(R.id.bSelectDate).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Calendar calendar = new GregorianCalendar(1980, 0, 1);
                DatePickerDialog dpd = DatePickerDialog.newInstance(
                        MyAccount.this,
                        calendar.get(Calendar.YEAR),
                        calendar.get(Calendar.MONTH),
                        calendar.get(Calendar.DAY_OF_MONTH)
                );

                Calendar calendarInst = Calendar.getInstance();
                int year = calendarInst.get(Calendar.YEAR);
                int month = calendarInst.get(Calendar.MONTH);
                int date = calendarInst.get(Calendar.DATE);
                Calendar cMaxDate = new GregorianCalendar(year, month, date);

                dpd.setMaxDate(cMaxDate);
                dpd.showYearPickerFirst(true);
                dpd.show(getFragmentManager(), "DatePickerDialog");

            }
        });

        findViewById(R.id.bSubmit).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String firstChar = null;
                if (!tvMobile.getText().toString().trim().equalsIgnoreCase(""))
                    firstChar = String.valueOf(tvMobile.getText().toString().charAt(0));

                if (!tvMobile.getText().toString().trim().equalsIgnoreCase("") && tvMobile.getText().length() == 10 && !firstChar.equalsIgnoreCase("0")) {
                    JSONObject jsonObject = new JSONObject();
                    try {
                        jsonObject.put(Constants.AUTH_TOKEN, preferences.getString(Constants.KEY_TOKEN, ""));
                        if (user.getUser_id() != null)
                        jsonObject.put(Constants.USER_ID, user.getUser_id());
                        else if (preferences.getString(Constants.USER_ID, "") != null){
                          jsonObject.put(Constants.USER_ID, preferences.getString(Constants.USER_ID, ""));
                      }
                        jsonObject.put("user_first_name", tvFirstName.getText().toString());
                        jsonObject.put("user_last_name", tvLastName.getText().toString());
                        String dob = tvDateYear.getText().toString() + "-" + tvDateMonth.getText().toString() + "-" + tvDateDay.getText().toString();
                        jsonObject.put("dob", dob);
                        jsonObject.put("email", tvEmail.getText().toString());
                        jsonObject.put("mobile_number", tvMobile.getText().toString());
                        jsonObject.put("bio", tvBio.getText().toString());
                        jsonObject.put("user_gender", selectedGender);
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                    postJsonData(Urls.UPDATE_PROFILE, jsonObject, Constants.TAG_UPDATE_PROFILE, true);
                } else
                    showToast("Invalid mobile number");
            }
        });

       /* RadioGroup radioGroup = (RadioGroup) findViewById(R.id.rgGender);
        radioGroup.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(RadioGroup group, int checkedId) {
                Constants.debug("Id :"+checkedId);
                if(checkedId == R.id.rbFemale)
                    selectedGender = "F";
                else
                    selectedGender = "M";
            }
        });*/

        rbFemale = (RadioButton) findViewById(R.id.rbFemale);
        rbMale = (RadioButton) findViewById(R.id.rbMale);

        rbFemale.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                rbFemale.setChecked(true);
                rbMale.setChecked(false);
                selectedGender = "F";
            }
        });


        rbMale.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                rbMale.setChecked(true);
                rbFemale.setChecked(false);
                selectedGender = "M";
            }
        });

        findViewById(R.id.btnVerify).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                Intent intent = new Intent(getApplicationContext(), MobileVerifyActivity.class);
                intent.putExtra("VERIFY_MYACCOUNT", true);
                startActivity(intent);
            }
        });

    }

    boolean validate() {
        return !TextUtils.isEmpty(tvFirstName.getText().toString().trim()) &&
                !TextUtils.isEmpty(tvLastName.getText().toString().trim()) &&
                !TextUtils.isEmpty(tvEmail.getText().toString().trim()) &&
                !TextUtils.isEmpty(tvMobile.getText().toString().trim());
    }

    @Override
    public void onCompleteRetrieval(int tag, ArrayList obj, String msg) {
        super.onCompleteRetrieval(tag, obj, msg);
        switch (tag) {
            case Constants.TAG_USER:

                if (obj.size() > 0)
                    user = (User) obj.get(0);


                JSONObject jsonObject = new JSONObject();
                try {
                    if (user != null && String.valueOf(user.getUser_id()) != null){
                        if (!String.valueOf(user.getUser_id()).trim().isEmpty()){
                            jsonObject.put(Constants.USER_ID, user.getUser_id());
                        }
                    }
                    else
                        jsonObject.put(Constants.USER_ID, preferences.getString(Constants.USER_ID,""));
                    jsonObject.put(Constants.AUTH_TOKEN, preferences.getString(Constants.KEY_TOKEN, ""));
                } catch (JSONException e) {
                    e.printStackTrace();
                }
                postJsonData(Urls.PROFILE, jsonObject, Constants.TAG_PROFILE, true);
                break;
        }
    }

    @Override
    public <T> void onResponse(T obj, int tag) {
        super.onResponse(obj, tag);
        findViewById(R.id.pbWheel).setVisibility(View.GONE);
        switch (tag) {
            case Constants.TAG_UPDATE_PROFILE:
                VisitorResponse response = (VisitorResponse) obj;
                if (response.getStatus().equalsIgnoreCase(Constants.RESPONSE_SUCCESS)) {
                    showToast(response.getMessage().toString());
                    Constants.UPDATE_PROFILE = true;
                    finish();
                } else
                    showToast(response.getMessage().toString());
                break;
            case Constants.TAG_PROFILE:
                Profile profile = (Profile) obj;
                tvEmail.setText(profile.getEmail());        //email is mandatory
                if (!TextUtils.isEmpty(profile.getUser_first_name()))
                    tvFirstName.setText(profile.getUser_first_name());
                if (!TextUtils.isEmpty(profile.getUser_last_name()))
                    tvLastName.setText(profile.getUser_last_name());
                if (!TextUtils.isEmpty(profile.getUser_mobile()))
                    tvMobile.setText(profile.getUser_mobile());
                if (!TextUtils.isEmpty(profile.getDob())) {
                    String date[] = profile.getDob().split("-");
                    setDate(date[2], date[1], date[0]);
                }
                if (!TextUtils.isEmpty(profile.getCust_bio()))
                    tvBio.setText(profile.getCust_bio());

                if (!TextUtils.isEmpty(profile.getUser_gender())) {
                    if (profile.getUser_gender().equalsIgnoreCase("F")) {
                        rbFemale.setChecked(true);
                    } else {
                        rbMale.setChecked(true);
                    }
                }
        /*Stop mobile varification from here*/
              /*  if (profile.getCheck_mobile_verification())
                    findViewById(R.id.btnVerify).setVisibility(View.VISIBLE);
                else*/
                    findViewById(R.id.btnVerify).setVisibility(View.GONE);

                if (profile.isUser_mobile_verified())
                    findViewById(R.id.tvMobileVerified).setVisibility(View.VISIBLE);
                else
                    findViewById(R.id.tvMobileVerified).setVisibility(View.GONE);

                break;
        }
    }

    @Override
    public void onError(int error_code, int tag, Object object) {
        checkErrorCode(error_code, object);
    }

    @Override
    public void onDateSet(DatePickerDialog view, int year, int monthOfYear, int dayOfMonth) {
        setDate(dayOfMonth + "", ++monthOfYear + "", year + "");
    }

    void setDate(String dayOfMonth, String monthOfYear, String year) {

        tvDateDay = (TextView) findViewById(R.id.tvDateDay);
        tvDateMonth = (TextView) findViewById(R.id.tvDateMonth);
        tvDateYear = (TextView) findViewById(R.id.tvDateYear);

        tvDateDay.setText(dayOfMonth);
        tvDateMonth.setText(monthOfYear);
        tvDateYear.setText(year);
    }
}
