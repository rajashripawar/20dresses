package outletwise.com.twentydresses.view.activity;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.ActionBar;
import android.view.View;

import java.util.ArrayList;

import outletwise.com.twentydresses.R;
import outletwise.com.twentydresses.controller.interfaces.StyleProfileChanged;
import outletwise.com.twentydresses.model.StyleAttrEntity;
import outletwise.com.twentydresses.model.StyleProfile;
import outletwise.com.twentydresses.model.UpdateStyleProfile;
import outletwise.com.twentydresses.model.UserAttrEntity;
import outletwise.com.twentydresses.model.VisitorResponse;
import outletwise.com.twentydresses.model.database.greenbot.User;
import outletwise.com.twentydresses.utilities.Constants;
import outletwise.com.twentydresses.utilities.Urls;
import outletwise.com.twentydresses.view.fragment.BaseFragment;
import outletwise.com.twentydresses.view.fragment.QuizSelectComplexionFragment;
import outletwise.com.twentydresses.view.fragment.QuizSelectShapeFragment;
import outletwise.com.twentydresses.view.fragment.QuizSelectStyleFragment;
import outletwise.com.twentydresses.view.fragment.QuizSelectmeasureFragment;

/**
 * Created by User-PC on 14-12-2015.
 * Class is use to edit style profile (Body type, body shape, height etc)
 */
public class EditProfileActivity extends BaseActivity implements StyleProfileChanged {

    private Constants.Attributes attribute;
    private UserAttrEntity userAttr;
    private User user;
    private StyleAttrEntity styleAttrEntity;
    private boolean isChange = false;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_edit_profile);

        //    selectRecords(Constants.TAG_USER);
        setUpToolBar(getString(R.string.toolbar_edit_prof));
        final ActionBar ab = getSupportActionBar();
        ab.setDisplayHomeAsUpEnabled(true);

        init();
    }

    void init() {
        attribute = (Constants.Attributes) getIntent().getSerializableExtra(Constants.Attributes.class.getSimpleName());
        userAttr = getIntent().getParcelableExtra(UserAttrEntity.class.getSimpleName());

        BaseFragment fragment = null;
        Bundle bundle = new Bundle();
        bundle.putSerializable(Constants.Attributes.class.getSimpleName(), attribute);
        bundle.putParcelable(StyleProfile.class.getSimpleName(), userAttr);
        if (attribute == Constants.Attributes.BODY_SHAPE) {
            fragment = new QuizSelectShapeFragment();
        } else if (attribute == Constants.Attributes.COMPLEXION) {
            fragment = new QuizSelectComplexionFragment();
        } else if (attribute == Constants.Attributes.SHOE_SIZES || attribute == Constants.Attributes.HEIGHT
                || attribute == Constants.Attributes.TOP_SIZE || attribute == Constants.Attributes.BOTTOM_SIZE) {
            fragment = new QuizSelectmeasureFragment();
        } else if (attribute == Constants.Attributes.STYLE)
            fragment = new QuizSelectStyleFragment();
        else if (attribute == Constants.Attributes.FACE_SHAPE) {

            String qn_id = getIntent().getStringExtra("que_id");
            finish();
            Intent intent = new Intent(getApplicationContext(), ForYouQuizActivity.class);
            intent.putExtra("fromProfile", true);
            intent.putExtra("que_id", qn_id);
            intent.putExtra(UserAttrEntity.class.getSimpleName(), userAttr);
            startActivity(intent);

        }


        if (fragment != null) {
            fragment.setArguments(bundle);
            Constants.EDITPROFILE = true;
            replaceFragment(R.id.frame_container, fragment, fragment.getClass().getSimpleName());
        }
    }

    @Override
    public void onChange(UserAttrEntity userAttr) {
        // if (user != null)
        {
            findViewById(R.id.pbWheelProfile).setVisibility(View.VISIBLE);
            this.userAttr = userAttr;
            isChange = true;
            selectRecords(Constants.TAG_USER);
            /*StyleProfile styleProfile = new StyleProfile();
            styleProfile.setUser_attr(userAttr);
            styleProfile.setUserId(user.getUser_id() + "");
            styleProfile.setAuth_token(preferences.getString(Constants.KEY_TOKEN, ""));

            postJsonData(Urls.UPDATE_STYLE_PROFILE, styleProfile, Constants.TAG_UPDATE_STYLE_PROFILE, true);*/
        }
    }

    @Override
    public void onChangeStyle(StyleAttrEntity styleAttrEntity) {

        findViewById(R.id.pbWheelProfile).setVisibility(View.VISIBLE);
        Constants.UPDATE_PROFILE = true;
        this.styleAttrEntity = styleAttrEntity;
        isChange = true;
        selectRecords(Constants.TAG_USER);
      /*  UpdateStyleProfile styleProfile = new UpdateStyleProfile();
        styleProfile.setUser_attr(userAttr);
        styleProfile.setStyle_attr(styleAttrEntity);
        styleProfile.setUserId(user.getUser_id() + "");
        styleProfile.setAuth_token(preferences.getString(Constants.KEY_TOKEN, ""));

        postJsonData(Urls.UPDATE_STYLE_PROFILE, styleProfile, Constants.TAG_UPDATE_STYLE_PROFILE, true);*/
    }

    @Override
    public <T> void onResponse(T obj, int tag) {
        super.onResponse(obj, tag);
        if (tag == Constants.TAG_UPDATE_STYLE_PROFILE) {
            findViewById(R.id.pbWheelProfile).setVisibility(View.GONE);
            VisitorResponse response = (VisitorResponse) obj;
            if (response.getStatus().equalsIgnoreCase(Constants.RESPONSE_SUCCESS)) {
                Constants.UPDATE_PROFILE = true;
                Intent intent = new Intent();
                intent.putExtra(UserAttrEntity.class.getSimpleName(), userAttr);
                setResult(RESULT_OK, intent);
                finish();
            }
            showToast(response.getMessage());
        }
    }

    @Override
    public void onBackPressed() {
        if (getmFragmentManager() != null && getmFragmentManager().getBackStackEntryCount() > 1)
            getmFragmentManager().popBackStack();
        else {
            setResult(RESULT_CANCELED);
            finish();
        }
    }

    @Override
    public void onCompleteRetrieval(int tag, ArrayList obj, String msg) {
        switch (tag) {
            case Constants.TAG_USER:
                if (obj != null && obj.size() > 0) {
                    user = (User) obj.get(0);
                    if (isChange) {
                        isChange = false;
                        UpdateStyleProfile styleProfile = new UpdateStyleProfile();
                        styleProfile.setUser_attr(userAttr);
                        styleProfile.setStyle_attr(styleAttrEntity);
                        if (user != null && String.valueOf(user.getUser_id()) != null)
                            styleProfile.setUserId(""+user.getUser_id());
                        else
                            styleProfile.setUserId(preferences.getString(Constants.USER_ID,""));
                        styleProfile.setUserId(user.getUser_id() + "");
                        styleProfile.setAuth_token(preferences.getString(Constants.KEY_TOKEN, ""));

                        postJsonData(Urls.UPDATE_STYLE_PROFILE, styleProfile, Constants.TAG_UPDATE_STYLE_PROFILE, true);
                    }
                }
                break;
        }
    }

}
