package outletwise.com.twentydresses.view.activity;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.ActionBar;
import android.widget.LinearLayout;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.Iterator;

import outletwise.com.twentydresses.R;
import outletwise.com.twentydresses.controller.adapter.TermsAdapter;
import outletwise.com.twentydresses.utilities.Constants;
import outletwise.com.twentydresses.utilities.Urls;

/**
 * This class is use to show terms & conditions
 */
public class PrivacyPolicyActivity extends BaseActivity {
    private Intent intent;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_terms);
        setUpToolBar(getString(R.string.toolbar_privacy));
        final ActionBar ab = getSupportActionBar();
        ab.setDisplayHomeAsUpEnabled(true);

        getData();
    }

    private void getData() {
        postJsonData(Urls.PRIVACY_POLICY, new JSONObject(), Constants.TAG_PRIVACY_POLICY, true);
    }

    @Override
    public <T> void onResponse(T obj, int tag) {
        super.onResponse(obj, tag);
        switch (tag) {
            case Constants.TAG_PRIVACY_POLICY:
                String json = (String) obj;
                ArrayList<String> keys = new ArrayList<>();
                ArrayList<String> values = new ArrayList<>();
                try {
                    JSONObject issueObj = new JSONObject(json);
                    Iterator iterator = issueObj.keys();
                    int temp = 0;
                    while (iterator.hasNext()) {
                        String key = (String) iterator.next();
                        String keyReplace = key.replace("_", " ");
                        keys.add(temp, keyReplace);

                        String value = issueObj.optString(key);
                        values.add(temp, value);
                        temp++;
                    }
                    LinearLayout llCredit = (LinearLayout) findViewById(R.id.llTerms);
                    TermsAdapter termsAdapter = new TermsAdapter(this, keys, values, llCredit);
                } catch (JSONException ex) {
                }
                break;

        }
    }

    @Override
    public void onError(int error_code, int tag, Object object) {
        checkErrorCode(error_code, object);
    }
}
