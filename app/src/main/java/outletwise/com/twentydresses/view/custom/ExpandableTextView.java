package outletwise.com.twentydresses.view.custom;

import android.content.Context;
import android.content.res.TypedArray;
import android.text.Spannable;
import android.text.SpannableStringBuilder;
import android.text.style.ForegroundColorSpan;
import android.util.AttributeSet;
import android.view.View;
import android.widget.TextView;

import outletwise.com.twentydresses.R;

public class ExpandableTextView extends TextView {
    private static final int DEFAULT_TRIM_LENGTH = 130;
    private static final String ELLIPSIS_MORE = "...Show more";
    private static final String ELLIPSIS_LESS = "...Show less";

    private CharSequence originalText;
    private CharSequence trimmedText;
    private BufferType bufferType;
    private boolean trim = true;
    private int trimLength;
    public Context context;

    public ExpandableTextView(Context context) {
        this(context, null);
        this.context = context;
    }

    public ExpandableTextView(final Context context, AttributeSet attrs) {
        super(context, attrs);
        this.context = context;

        TypedArray typedArray = context.obtainStyledAttributes(attrs, R.styleable.ExpandableTextView);
        this.trimLength = typedArray.getInt(R.styleable.ExpandableTextView_trimLength, DEFAULT_TRIM_LENGTH);
        typedArray.recycle();

        setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                trim = !trim;
                setText();
                requestFocusFromTouch();
            }
        });
    }

    private void setText() {
        super.setText(getDisplayableText(), bufferType);
    }

    private CharSequence getDisplayableText() {

        SpannableStringBuilder sp = new SpannableStringBuilder(originalText, 0, originalText.length());
        sp.append(ELLIPSIS_LESS);
        if(context != null)
        sp.setSpan(new ForegroundColorSpan(context.getResources().getColor(R.color.color_accent)),
                originalText.length(), originalText.length() + 12, Spannable.SPAN_INCLUSIVE_INCLUSIVE);

        return trim ? trimmedText : sp;
    }

    @Override
    public void setText(CharSequence text, BufferType type) {
        originalText = text;
        trimmedText = getTrimmedText(text);
        bufferType = type;
        setText();
    }

    private CharSequence getTrimmedText(CharSequence text) {
        if (originalText != null && originalText.length() > trimLength) {
            SpannableStringBuilder sp = new SpannableStringBuilder(originalText, 0, trimLength + 1);
            sp.append(ELLIPSIS_MORE);
            sp.setSpan(new ForegroundColorSpan(context.getResources().getColor(R.color.color_accent)),
                    trimLength + 1, trimLength + 13, Spannable.SPAN_INCLUSIVE_INCLUSIVE);
            return sp;
        } else {
            return originalText;
        }
    }

    public CharSequence getOriginalText() {
        return originalText;
    }

    public void setTrimLength(int trimLength) {
        this.trimLength = trimLength;
        trimmedText = getTrimmedText(originalText);
        setText();
    }

    public int getTrimLength() {
        return trimLength;
    }
}