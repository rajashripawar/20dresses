package outletwise.com.twentydresses.view.fragment;

import android.annotation.SuppressLint;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Build;
import android.os.Bundle;
import android.os.Parcelable;
import android.preference.PreferenceManager;
import android.support.annotation.Nullable;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.sothree.slidinguppanel.SlidingUpPanelLayout;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;

import outletwise.com.twentydresses.R;
import outletwise.com.twentydresses.controller.AnalyticsTracker;
import outletwise.com.twentydresses.controller.CartOperations;
import outletwise.com.twentydresses.controller.ProductOptions;
import outletwise.com.twentydresses.controller.adapter.BaseRecyclerAdapter;
import outletwise.com.twentydresses.controller.adapter.ProductsAdapter;
import outletwise.com.twentydresses.model.Category;
import outletwise.com.twentydresses.model.Product;
import outletwise.com.twentydresses.model.SubTip;
import outletwise.com.twentydresses.model.database.greenbot.Cart;
import outletwise.com.twentydresses.model.database.greenbot.Shortlist;
import outletwise.com.twentydresses.utilities.Constants;
import outletwise.com.twentydresses.utilities.Urls;
import outletwise.com.twentydresses.view.activity.BaseActivity;
import outletwise.com.twentydresses.view.activity.FilterActivity;
import outletwise.com.twentydresses.view.activity.ProductViewActivity;
import outletwise.com.twentydresses.view.custom.CustomRecyclerView;
import outletwise.com.twentydresses.view.custom.FavoritesButton;

/**
 * Created by User-PC on 06-08-2015.
 */
public class HomeSaleFragment extends BaseFragment implements ProductsAdapter.ShortListed, SwipeRefreshLayout.OnRefreshListener {

    private View rootView;
    private View llProductOptions;
    private CustomRecyclerView rv;
    private ArrayList<Product> products = new ArrayList<>();
    private boolean isLoading;
    private int page = 1;
    private ProductsAdapter productAdapter;
    private RecyclerScrollListener recyclerScrollListener;
    private boolean isCalled;
    private Category.SubCat subCat;
    private String sortBy;
    private ProductOptions productOptions;
    private SwipeRefreshLayout mSwipeRefreshLayout;
    public static SharedPreferences preferences;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        rootView = inflater.inflate(R.layout.fragment_sale, container, false);
        mSwipeRefreshLayout = (SwipeRefreshLayout) rootView.findViewById(R.id.swipeRefreshLayout);
        mSwipeRefreshLayout.setOnRefreshListener(this);
        isCalled = false;
        // subCat = (Category.SubCat)getArguments().getParcelable(Constants.CATEGORY);
        subCat = getArguments().getParcelable(Constants.CATEGORY);
        rootView.findViewById(R.id.pbWheel).setVisibility(View.VISIBLE);
        preferences = PreferenceManager.getDefaultSharedPreferences(getActivity());
        setUpRecyclerView();
        setUpProductOptions();

        return rootView;
    }

    private void setUpProductOptions() {
        llProductOptions = rootView.findViewById(R.id.llProductOptions);
        productOptions = new ProductOptions(mActivity, llProductOptions, Constants.SALE);

        productOptions.hideToggle();
        productOptions.setOptionClickListener(new ProductOptions.OptionClick() {
            @Override
            public void onOptionClick(ProductOptions.Option tag) {
                if (tag.getTag() == ProductOptions.FILTER) {
                    Intent intent = new Intent(mActivity, FilterActivity.class);
                    // Category.SubCat subCat = new Category.SubCat();
                    subCat.setId("1");

                    if (subCat.getName().toString().equalsIgnoreCase(Constants.SALE))
                        subCat.setName(Constants.SALE);
                    else
                        subCat.setName(Constants.NEW);

                    intent.putExtra(Constants.CATEGORY, subCat);
                    startActivity(intent);
                }
            }
        });

        productOptions.setSortCallback(new ProductsSortFragment.Callback() {
            @Override
            public void onCallback(String sort) {
                sortBy = sort;
                productAdapter.getItems().clear();
                productAdapter.showProgressBar();
                productAdapter.notifyDataSetChanged();
                page = 1;
                getProducts(page);

                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
                    ((TextView) rootView.findViewById(R.id.tvSort)).setCompoundDrawablesWithIntrinsicBounds(mActivity.getDrawable(R.drawable.selected_sort), null, null, null);
                } else
                    ((TextView) rootView.findViewById(R.id.tvSort)).setCompoundDrawablesWithIntrinsicBounds(getResources().getDrawable(R.drawable.selected_sort), null, null, null);

                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                    ((TextView) rootView.findViewById(R.id.tvSort)).setTextColor(mActivity.getColor(R.color.color_accent));
                } else
                    ((TextView) rootView.findViewById(R.id.tvSort)).setTextColor(getResources().getColor(R.color.color_accent));
            }
        });

    }

  /*  private void getProducts() {
        HashMap<String, String> params = new HashMap<>();
        if (sortBy != null) {
            params.put("sort", sortBy);
            page = 1;
        }

        if (page > 1)
            params.put("page", page + "");

        params.put("sale", "true");
        postStringData(Urls.PRODUCTS, params, Constants.TAG_PRODUCTS, false);
    }
*/

    private void setUpRecyclerView() {
        rv = (CustomRecyclerView) rootView.findViewById(R.id.rvProducts);
        productAdapter = new ProductsAdapter(mActivity, products, ProductsAdapter.PRODUCT_VERTICAL, this, false);
        productAdapter.setOnItemClickListener(new BaseRecyclerAdapter.OnItemClickListener() {
            @Override
            public void onItemClick(View view, int position) {
                if (view.getTag() != null) {
                    Product product = (Product) view.getTag();
                    /*if (CartOperations.getInstance().getCartList().size() > 0) {
                        for (Cart cart : CartOperations.getInstance().getCartList()) {
                            if (cart.getProduct_id().equalsIgnoreCase(product.getProduct_id()))
                                product.setProduct_size(cart.getProduct_size());
                        }
                    }*/
                    Intent intent = new Intent(mActivity, ProductViewActivity.class);
                    intent.putExtra(Constants.PRODUCT_NAME, product);
                    intent.putExtra(Constants.CATEGORY, subCat);
                    startActivity(intent);
                }
            }
        });
        recyclerScrollListener = new RecyclerScrollListener();
        resetRecyclerView(ProductsAdapter.PRODUCT_VERTICAL);
        rv.addOnScrollListener(recyclerScrollListener);
        getProducts(page);
        selectRecords(Constants.TAG_SHORTLIST);
    }

    private void getProducts(int page) {
        HashMap<String, String> params = new HashMap<>();

        if (sortBy != null) {
            params.put("sort", sortBy);
        }

        if (subCat.getName().toString().equalsIgnoreCase(Constants.SALE))
            params.put("sale", 10 + "");
        if (page != 1)
            params.put("page", page + "");

        postStringData(Urls.PRODUCTS, params, Constants.TAG_PRODUCTS, false);
        isLoading = true;
    }

    void resetRecyclerView(int orientation) {
        LinearLayoutManager layoutManager;
        if (orientation == ProductsAdapter.PRODUCT_VERTICAL) {
            layoutManager = new GridLayoutManager(rv.getContext(), 2);
            ((GridLayoutManager) layoutManager).setSpanSizeLookup(new GridLayoutManager.SpanSizeLookup() {
                @Override
                public int getSpanSize(int position) {
                    if (productAdapter.isFooter(position))
                        return 2;
                    return 1;
                }
            });
            rv.setIsHorizontal(false);
        } else {
            layoutManager = new LinearLayoutManager(rv.getContext());
            layoutManager.setOrientation(LinearLayoutManager.HORIZONTAL);
            rv.setIsHorizontal(true);
        }
        productAdapter.setType(orientation);
        rv.setLayoutManager(layoutManager);
        rv.setAdapter(productAdapter);
        recyclerScrollListener.setLayoutManager(layoutManager);
        rv.scrollToPosition(recyclerScrollListener.getPastVisibleItem());
    }

    @Override
    public <T> void onResponse(T obj, int tag) {
        super.onResponse(obj, tag);
        switch (tag) {
            case Constants.TAG_PRODUCTS:
                //   ArrayList<Shortlist> shortlists = ((BaseActivity)mActivity).shortlists;
                rootView.findViewById(R.id.pbWheel).setVisibility(View.GONE);
                productAdapter.hideProgressBar();
                if (obj instanceof String && ((String) obj).equalsIgnoreCase(Constants.NO_MORE_RESULTS)) {
                    showToast(obj.toString());
                }/*else if (obj instanceof SubTip) {
                        if (page == 1) {
                            products.clear();
                            products.addAll(((SubTip) obj).getProducts());
                        } else
                            products.addAll(((SubTip) obj).getProducts());
                        Constants.TIP_TOTAL = ((SubTip) obj).getProducts().size();
                        productAdapter.notifyDataSetChanged();
                        isLoading = false;
                    }*/
                else if (obj instanceof Product[]) {
                    //    if (!isCalled)
                    {
                        //  products.addAll((Arrays.asList((Product[]) obj)));
                        if (page == 1) {
                            products.clear();
                            products.addAll((Arrays.asList((Product[]) obj)));
                        } else
                            products.addAll((Arrays.asList((Product[]) obj)));

                        Constants.TIP_TOTAL = products.size();
                        productAdapter.notifyDataSetChanged();
                        isLoading = false;
                    }
                    //Gets called twice due to view pager so set variable here
                    isCalled = true;
                }
                mSwipeRefreshLayout.setRefreshing(false);
                break;
            case Constants.TAG_SHORTLIST:
                showToast(getString(R.string.toast_added_to_shortlist));
                Shortlist shortlist = (Shortlist) obj;
                productAdapter.changeProductState(shortlist.getProduct_id() + "", FavoritesButton.PROGRESS_COMPLETE);
                productAdapter.notifyDataSetChanged();
                insertRecord(obj, tag);
                if ((((BaseActivity) mActivity).shortlists.size() == 0))
                    ((BaseActivity) mActivity).slidePanelTo(SlidingUpPanelLayout.PanelState.ANCHORED);
                break;
            case Constants.TAG_SHORTLIST_REMOVE:
                showToast(getString(R.string.toast_removed_from_shortlist));
                Shortlist shortlist1 = (Shortlist) obj;
                productAdapter.changeProductState(shortlist1.getProduct_id() + "", FavoritesButton.PROGRESS_IDLE);
                productAdapter.notifyDataSetChanged();
                deleteRecord(obj, Constants.TAG_SHORTLIST_REMOVE);
                break;
        }
    }


    @Override
    public void addProductToShortList(Product product) {
        JSONObject jsonObject = new JSONObject();
        try {
            jsonObject.put(Constants.AUTH_TOKEN, ((BaseActivity) mActivity).preferences.getString(Constants.KEY_TOKEN, ""));


            if (((BaseActivity) mActivity).user != null && String.valueOf(((BaseActivity) mActivity).user.getUser_id()) != null)
                jsonObject.put(Constants.USER_ID, ((BaseActivity) mActivity).user.getUser_id());
            else if (preferences.getString(Constants.USER_ID, "") != null){
                jsonObject.put(Constants.USER_ID, preferences.getString(Constants.USER_ID, ""));
            }
//            jsonObject.put(Constants.USER_ID, ((BaseActivity) mActivity).user.getUser_id() + "");
            jsonObject.put(Constants.PRODUCT, product.getProduct_id());
        } catch (JSONException e) {
            e.printStackTrace();
        }
        postJsonData(Urls.SHORTLIST_ADD, jsonObject, Constants.TAG_SHORTLIST, false);

        if (subCat != null && subCat.getName() != null)
            AnalyticsTracker.getInstance().trackShortlist(product, subCat.getName());
        else
            AnalyticsTracker.getInstance().trackShortlist(product, "");
    }

    @Override
    public void removeProductFromShortList(Product product) {

        JSONObject jsonObject = new JSONObject();
        try {
            jsonObject.put(Constants.AUTH_TOKEN, ((BaseActivity) mActivity).preferences.getString(Constants.KEY_TOKEN, ""));
            jsonObject.put(Constants.USER_ID, ((BaseActivity) mActivity).user.getUser_id());
            jsonObject.put(Constants.PRODUCT, product.getProduct_id());
        } catch (JSONException e) {
            e.printStackTrace();
        }
        postJsonData(Urls.SHORTLIST_REMOVE, jsonObject, Constants.TAG_SHORTLIST_REMOVE, false);
    }

    @Override
    public void onCompleteRetrieval(int tag, ArrayList obj, String msg) {
        super.onCompleteRetrieval(tag, obj, msg);
        switch (tag) {
            case Constants.TAG_SHORTLIST:
                final ArrayList<Shortlist> shortlists = obj;
                new Thread(new Runnable() {
                    @Override
                    public void run() {
                        for (Shortlist shortlist : shortlists)
                            productAdapter.changeProductState(shortlist.getProduct_id(), FavoritesButton.PROGRESS_COMPLETE);
                    }
                }).start();
                break;
        }
    }

    public void refresh() {
        rootView.findViewById(R.id.pbWheel).setVisibility(View.VISIBLE);
        page = 1;
        products.clear();
        setUpRecyclerView();
    }

    @Override
    public void onRefresh() {
        rootView.findViewById(R.id.pbWheel).setVisibility(View.GONE);
        page = 1;
        products.clear();
        setUpRecyclerView();
    }

    class RecyclerScrollListener extends RecyclerView.OnScrollListener {
        private int pastVisibleItem, visibleItemCount, totalItemCount;
        private RecyclerView.LayoutManager layoutManager;

        public void setLayoutManager(RecyclerView.LayoutManager layoutManager) {
            this.layoutManager = layoutManager;
        }

        public int getPastVisibleItem() {
            return pastVisibleItem;
        }

        @Override
        public void onScrolled(RecyclerView recyclerView, int dx, int dy) {
            visibleItemCount = layoutManager.getChildCount();
            totalItemCount = layoutManager.getItemCount();
            pastVisibleItem = ((LinearLayoutManager) layoutManager).findFirstVisibleItemPosition();

            if ((visibleItemCount + pastVisibleItem) >= totalItemCount) {
                if (!isLoading) {
                    //reset isCalled here
                    isCalled = false;
                    productAdapter.showProgressBar();
                    getProducts(++page);
                }
            }
        }
    }

    @Override
    public void onError(int error_code, int tag, Object object) {
        super.onError(error_code, tag, object);
        productAdapter.hideProgressBar();
        ((BaseActivity) mActivity).checkErrorCode(error_code, object);
    }
}
