package outletwise.com.twentydresses.view.activity;

import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.ActivityInfo;
import android.content.pm.PackageManager;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.provider.Settings;
import android.support.annotation.DrawableRes;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.Gravity;
import android.view.MenuItem;
import android.view.View;
import android.view.WindowManager;
import android.widget.ExpandableListView;
import android.widget.ImageView;
import android.widget.Toast;

import com.google.android.gms.analytics.HitBuilders;
import com.google.android.gms.analytics.Tracker;
import com.google.gson.Gson;
import com.mikepenz.actionitembadge.library.ActionItemBadge;
import com.pnikosis.materialishprogress.ProgressWheel;
import com.sothree.slidinguppanel.SlidingUpPanelLayout;
import com.sromku.simple.fb.Permission;
import com.sromku.simple.fb.SimpleFacebook;
import com.sromku.simple.fb.SimpleFacebookConfiguration;
import com.sromku.simple.fb.entities.Profile;
import com.sromku.simple.fb.listeners.OnLoginListener;
import com.sromku.simple.fb.listeners.OnLogoutListener;
import com.sromku.simple.fb.listeners.OnProfileListener;
import com.sromku.simple.fb.utils.Attributes;
import com.sromku.simple.fb.utils.PictureAttributes;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.File;
import java.lang.ref.WeakReference;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import it.sauronsoftware.ftp4j.FTPClient;
import it.sauronsoftware.ftp4j.FTPDataTransferListener;
import outletwise.com.twentydresses.ApplicationUtil;
import outletwise.com.twentydresses.R;
import outletwise.com.twentydresses.controller.DragListener;
import outletwise.com.twentydresses.controller.adapter.BaseRecyclerAdapter;
import outletwise.com.twentydresses.controller.adapter.DrawerAdapter;
import outletwise.com.twentydresses.controller.adapter.FavoritesRecyclerAdapter;
import outletwise.com.twentydresses.model.Category;
import outletwise.com.twentydresses.model.Product;
import outletwise.com.twentydresses.model.Quiz;
import outletwise.com.twentydresses.model.Registration;
import outletwise.com.twentydresses.model.StyleAttrEntity;
import outletwise.com.twentydresses.model.UserAttrEntity;
import outletwise.com.twentydresses.model.database.greenbot.Shortlist;
import outletwise.com.twentydresses.model.database.greenbot.User;
import outletwise.com.twentydresses.utilities.Constants;
import outletwise.com.twentydresses.utilities.Urls;
import outletwise.com.twentydresses.utilities.database.DatabaseCallBack;
import outletwise.com.twentydresses.utilities.database.DatabaseOperation;
import outletwise.com.twentydresses.utilities.network.NetworkCall;
import outletwise.com.twentydresses.utilities.network.NetworkReceiver;
import outletwise.com.twentydresses.view.fragment.BaseFragment;
import uk.co.chrisjenx.calligraphy.CalligraphyContextWrapper;

/**
 * Created by User-PC on 28-07-2015.
 * This is Base activity for all other activities. Declared methods for network call, database operations
 */
public abstract class BaseActivity extends AppCompatActivity implements DragListener.onDropComplete, NetworkReceiver,
        DatabaseCallBack, View.OnClickListener {
    private final String ERROR_MSG1 = "Fragment Manager Null";
    private final float SLIDING_ANCHOR_POS = 0.22f;
    protected static final int SHORTLIST_HORIZONTAL = 1;
    protected static final int SHORTLIST_VERTICAL = 2;

    private FragmentManager mFragmentManager;
    private DrawerLayout mDrawerLayout;
    private Toolbar toolbar;
    private SlidingUpPanelLayout panelLayout;
    private RecyclerView rvFavorites;
    private NetworkCall mInstance;
    private boolean showProgress = true;
    private ProgressWheel progressWheel;
    private HashMap<Integer, BaseFragment> fragmentHashMap = new HashMap<>();
    private DrawerAdapter drawerAdapter;
    private boolean clearActivities;
    private List<Integer> tags = new ArrayList<>();
    private int shortListOrientation = SHORTLIST_HORIZONTAL;
    private Toast t;
    public ArrayList<Shortlist> shortlists;
    public User user;
    public static SharedPreferences preferences;
    private Tracker mTracker;
    private int lastExpandedPosition = -1;
    public static String ret;
    private String user_device_id;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_PORTRAIT);
        init();
        initQuiz();
        // Initialization of facebook
        initFacebookConfig();

        final SharedPreferences prefs = getSharedPreferences("MIXPANEL_PREF", MODE_PRIVATE);
        ret = prefs.getString(ApplicationUtil.MIXPANEL_DISTINCT_ID_NAME, null);
    }

    /*
    * Sign out is called on HomeActivity, QuizActivity & on Session expired
    * Clear all data on sign out
    *
    * */
    public void signOutFromApp() {
        preferences.edit().remove(Constants.KEY_TOKEN).commit();
        preferences.edit().putString(Constants.KEY_TOKEN, "").apply();
        preferences.edit().putString(Constants.USER_ID, "").apply();
        logoutFromFacebook();
        deleteAllRecords(Constants.TAG_USER);
        Intent i  = new Intent(BaseActivity.this, LoginActivity.class);
        i.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP|Intent.FLAG_ACTIVITY_SINGLE_TOP);
        startActivity(i);
    }

    /*
    * Common method to check server error codes
    * */
    public void checkErrorCode(int error_code, Object object) {
        if (error_code == Constants.UNAUTHORIZED_CODE) {
            showErrorDialog(Constants.SESSION_EXPIRED_MSG, new Intent(getApplicationContext(), LoginActivity.class));
        } else if (error_code == Constants.SERVER_DOWN_CODE || error_code == Constants.SERVER_DOWN_CODE1) {
            showErrorDialog(Constants.SERVER_DOWN_MSG, null);
        } else if (error_code == 99) // Error code is 99 if response contains no any data
            showToast(object.toString());
        else
            showToast(Constants.SERVER_ERROR);
    }

    /*
    * Show alert for server down, session expired, no internet connection etc
    *
    * */
    public void showErrorDialog(String msg, final Intent intent) {

        final WeakReference<BaseActivity> baseActivityWeakReference;
        baseActivityWeakReference = new WeakReference<BaseActivity>(this);
        if (baseActivityWeakReference.get() != null && !baseActivityWeakReference.get().isFinishing()) {
            AlertDialog.Builder builder1 = new AlertDialog.Builder(BaseActivity.this);
            builder1.setMessage(msg);
            builder1.setCancelable(false);
            builder1.setNegativeButton(
                    "Ok",
                    new DialogInterface.OnClickListener() {
                        public void onClick(DialogInterface dialog, int id) {
                            finish();
                            if (intent != null) {
                                signOutFromApp();
                            /* Currently intent is LoginActivity
                             If Intent contains other than LoginActivity Remove logout code*/
                            /*Start Logout code*/
                               /* logoutFromFacebook();
                                deleteAllRecords(Constants.TAG_USER);*/
                             /*End Logout code*/
                                //   intent.addCategory(Intent.CATEGORY_HOME);
                                // intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                                //startActivity(new Intent(getApplicationContext(), LoginActivity.class));
                            }
                        }
                    });

            AlertDialog alert11 = builder1.create();
            alert11.show();
        }
    }

    /*Method to get app's current version*/
    public float getCurrentAppVersion() {
        String versionName = null;
        try {
            versionName = getPackageManager().getPackageInfo(getPackageName(), 0).versionName;
        } catch (PackageManager.NameNotFoundException e) {
            e.printStackTrace();
        }

        return Float.parseFloat(versionName);
    }

    /*
    * Method to get users unique device id
    * */
    public String getDeviceId() {

        user_device_id = Settings.Secure.getString(getApplicationContext().getContentResolver(),
                Settings.Secure.ANDROID_ID);

        return user_device_id;
    }


    /*
    * Method to set cart count on option menu
    * */

    public void setCartCount(MenuItem menuCart) {

        if (user != null && user.getCart_count() != null) {
            if (user.getCart_count().equalsIgnoreCase("0"))
                ActionItemBadge.update(this, menuCart,
                        getResources().getDrawable(R.drawable.ic_cart), ActionItemBadge.BadgeStyles.RED, null);
            else
                ActionItemBadge.update(this, menuCart,
                        getResources().getDrawable(R.drawable.ic_cart), ActionItemBadge.BadgeStyles.RED,
                        user.getCart_count());
        } else
            ActionItemBadge.update(this, menuCart,
                    getResources().getDrawable(R.drawable.ic_cart), ActionItemBadge.BadgeStyles.RED, null);
    }


    /*
    * Method to track page view in Google Analytics
    * */
    public void trackPage(String page) {
        //Google analytics
        mTracker = ApplicationUtil.getInstance().getDefaultTracker();
        mTracker.setScreenName(page);
        mTracker.send(new HitBuilders.ScreenViewBuilder().build());

        //Mixpanel analytics
        // ApplicationUtil.getInstance().mMixpanel.track(page, null);
        // ApplicationUtil.getInstance().mMixpanel.track("Page Loaded", {"Page Viewed" :page});
    }


    /*
    * Method to set background image of page from API
    * */
    public void setBackgroundImage(String type) {
        JSONObject jsonObject = new JSONObject();
        try {
            jsonObject.put("type", type);
        } catch (JSONException e) {
            e.printStackTrace();
        }
        postJsonData(Urls.IMG_BACKGROUND, jsonObject, Constants.TAG_IMG_BACKGROUND, false);
    }

    private void initQuiz() {

        Quiz.getInstance().setStyle_attr(new StyleAttrEntity());
        Quiz.getInstance().setUser_attr(new UserAttrEntity());

        Registration.getInstance().setQuiz(Quiz.getInstance());
    }

    private void init() {

        mFragmentManager = getSupportFragmentManager();
        mFragmentManager.addOnBackStackChangedListener(new BaseBackStackChangedListener());
        mInstance = new NetworkCall(this, this);
        shortlists = new ArrayList<>();
        selectRecords(Constants.TAG_USER);
        preferences = PreferenceManager.getDefaultSharedPreferences(this);
    }

    /*==================================================================================================
         Navigation Drawer Methods
     ===================================================================================================*/

    //Used to setup navigation drawer, pass categories list received from WebService
    protected void setUpNavDrawer(DrawerAdapter.OnDrawerItemClick drawerItemClick, ArrayList<Category> categories) {
        mDrawerLayout = (DrawerLayout) findViewById(R.id.drawer_layout);
        final ActionBar ab = getSupportActionBar();
        ab.setHomeAsUpIndicator(R.drawable.ic_menu);
        ab.setDisplayHomeAsUpEnabled(true);

        setUpDrawerList(drawerItemClick, categories);
    }

    public DrawerAdapter getDrawerAdapter() {
        return drawerAdapter;
    }

    private void setUpDrawerList(final DrawerAdapter.OnDrawerItemClick drawerItemClick, ArrayList<Category> categories) {
        //Expandable items
        final ExpandableListView firstLevel = (ExpandableListView) findViewById(R.id.elvDrawer);
        drawerAdapter = new DrawerAdapter(this);
        firstLevel.setAdapter(drawerAdapter);
        drawerAdapter.setOnDrawerItemClick(drawerItemClick);

        firstLevel.setOnChildClickListener(new ExpandableListView.OnChildClickListener() {
            @Override
            public boolean onChildClick(ExpandableListView parent, View v, int groupPosition, int childPosition, long id) {
                if (drawerItemClick != null)
                    drawerItemClick.onChildItemClick(v.getTag());
                return false;
            }
        });
        firstLevel.setOnGroupClickListener(new ExpandableListView.OnGroupClickListener() {
            @Override
            public boolean onGroupClick(ExpandableListView parent, View v, int groupPosition, long id) {
                if (drawerItemClick != null)
                    drawerItemClick.onChildItemClick(v.getTag());
                return false;
            }
        });

        firstLevel.setOnGroupExpandListener(new ExpandableListView.OnGroupExpandListener() {
            @Override
            public void onGroupExpand(int groupPosition) {
                if (lastExpandedPosition != -1
                        && groupPosition != lastExpandedPosition) {
                    firstLevel.collapseGroup(lastExpandedPosition);
                }
                lastExpandedPosition = groupPosition;
            }
        });

        if (categories == null || categories.isEmpty())
            postStringData(Urls.CATEGORIES, new HashMap<String, String>(), Constants.TAG_CATEGORIES, false);
        else
            drawerAdapter.setCategories(categories);
    }

    protected void closeDrawer() {
        mDrawerLayout.closeDrawers();

    }



    /*==================================================================================================
         Activity Helper Methods
     ===================================================================================================*/

    public void disableTouch() {
        if (showProgress)
            getWindow().setFlags(WindowManager.LayoutParams.FLAG_NOT_TOUCHABLE,
                    WindowManager.LayoutParams.FLAG_NOT_TOUCHABLE);
    }

    public void enableTouch() {
        getWindow().clearFlags(WindowManager.LayoutParams.FLAG_NOT_TOUCHABLE);
    }

    /**
     * Call this method in activity to show default progress bar
     * Note : You should declare progress bar instance in xml file
     */
    protected void showProgress() {
        if (!showProgress)
            return;

        if (progressWheel == null) {
            setUpProgressBar();
        }

        if (progressWheel != null) {
            progressWheel.bringToFront();
            progressWheel.setVisibility(View.VISIBLE);
        }
    }

    /* To hide Progress */
    protected void hideProgress() {
        if (progressWheel != null)
            progressWheel.setVisibility(View.GONE);
    }

    /* Call this method in any activity to show toast, it cancels the previous toast*/
    public void showToast(String text) {
        if (text == null || text.equalsIgnoreCase(""))
            return;

        if (null != t)
            t.cancel();

        t = Toast.makeText(this, text, Toast.LENGTH_SHORT);
        t.show();
    }

    /**
     * Call this method to setup toolbar title
     * Note:
     * You need to declare a toolbar instance with @id toolbar
     */
    protected Toolbar setUpToolBar(String title) {
        return setUpToolBar(R.id.toolbar, title);
    }

    //Use this if you have a different id
    private Toolbar setUpToolBar(int id, String title) {
        toolbar = (Toolbar) findViewById(id);
        toolbar.setVisibility(View.VISIBLE);
        toolbar.setTitle(title);
        setSupportActionBar(toolbar);
        return toolbar;
    }

    protected void setToolbarLogo() {
        setToolbarLogo(R.drawable.ic_logo);
    }

    protected void setToolbarLogo(@DrawableRes int id) {
        toolbar.setLogo(id);
    }

    /**
     * Used for setting up shortlist sliding panel, this loads shortlisted
     * products from local database.
     */
    protected void setUpSlidingPanel() {
        panelLayout = (SlidingUpPanelLayout) findViewById(R.id.sliding_layout);
        panelLayout.setPanelHeight(0);
        panelLayout.setPanelHeight(getResources().getDimensionPixelSize(R.dimen.umano_collapsed_height));
        panelLayout.setOverlayed(true);
        panelLayout.setBackgroundColor(getResources().getColor(android.R.color.transparent));
        panelLayout.setShadowHeight(0);
        panelLayout.setCoveredFadeColor(getResources().getColor(android.R.color.transparent));
        View dragView = findViewById(R.id.ivFavorites);
        panelLayout.setDragView(dragView);
        rvFavorites = (RecyclerView) findViewById(R.id.rvFavorites);
        dragView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if ((rvFavorites.getLayoutManager() instanceof LinearLayoutManager &&
                        panelLayout.getPanelState() == SlidingUpPanelLayout.PanelState.EXPANDED) ||
                        panelLayout.getPanelState() == SlidingUpPanelLayout.PanelState.COLLAPSED)
                    slidePanelTo(SlidingUpPanelLayout.PanelState.ANCHORED);
                else if (panelLayout.getPanelState() == SlidingUpPanelLayout.PanelState.ANCHORED)
                    slidePanelTo(SlidingUpPanelLayout.PanelState.EXPANDED);
            }
        });
        panelLayout.setPanelSlideListener(new SlidingPanelListener());
        panelLayout.setAnchorPoint(SLIDING_ANCHOR_POS);
        //load shortlisted items from db
        selectRecords(Constants.TAG_SHORTLIST);
        // getShortlistItems();

    }


    //Used to slide the panel
    public void slidePanelTo(SlidingUpPanelLayout.PanelState panelState) {
        if (panelLayout != null)
            panelLayout.setPanelState(panelState);
    }

    protected void setSlidingPanelHeight(int htInPx) {
        panelLayout.setPanelHeight(htInPx);
    }

    private void showShortlistedItems(int type) {
        rvFavorites = (RecyclerView) findViewById(R.id.rvFavorites);
        if (rvFavorites == null) {
            Constants.error("Added to shortlist but sliding panel not available");
            return;
        }
        rvFavorites.setOnDragListener(new DragListener(this, this, Constants.FAVORITES_ADAPTER));
        RecyclerView.LayoutManager layoutManager;
        FavoritesRecyclerAdapter adapter;

        if (type == SHORTLIST_VERTICAL) {
            if (shortlists.size() == 0) {
                findViewById(R.id.rvFavorites).setVisibility(View.GONE);
                findViewById(R.id.tvShortlistCompare).setVisibility(View.GONE);
                findViewById(R.id.rlNoFavItems).setVisibility(View.VISIBLE);

                findViewById(R.id.rlNoFavItems).setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {

                    }
                });

                layoutManager = new GridLayoutManager(this, 3);
                adapter = new FavoritesRecyclerAdapter(this, shortlists, FavoritesRecyclerAdapter.PRODUCT_VERTICAL);
            } else {
                findViewById(R.id.rvFavorites).setVisibility(View.VISIBLE);
                findViewById(R.id.tvShortlistCompare).setVisibility(View.VISIBLE);
                findViewById(R.id.rlNoFavItems).setVisibility(View.GONE);
                layoutManager = new GridLayoutManager(this, 3);
                adapter = new FavoritesRecyclerAdapter(this, shortlists, FavoritesRecyclerAdapter.PRODUCT_VERTICAL);
            }

        } else {
            findViewById(R.id.rvFavorites).setVisibility(View.VISIBLE);
            findViewById(R.id.tvShortlistCompare).setVisibility(View.VISIBLE);
            findViewById(R.id.rlNoFavItems).setVisibility(View.GONE);
            layoutManager = new LinearLayoutManager(this, LinearLayoutManager.HORIZONTAL, false);
            adapter = new FavoritesRecyclerAdapter(this, shortlists, FavoritesRecyclerAdapter.PRODUCT_HORIZONTAL);
        }

        adapter.setFavoriteAction(new FavoritesRecyclerAdapter.FavoriteAction() {
            @Override
            public void onItemRemoved(Shortlist item) {
                findViewById(R.id.pbWheelShortlist).setVisibility(View.VISIBLE);
                JSONObject jsonObject = new JSONObject();
                try {
                    jsonObject.put(Constants.AUTH_TOKEN, preferences.getString(Constants.KEY_TOKEN, ""));
                    jsonObject.put(Constants.USER_ID, user.getUser_id());
                    jsonObject.put(Constants.PRODUCT, item.getProduct_id());
                } catch (JSONException e) {
                    e.printStackTrace();
                }
                postJsonData(Urls.SHORTLIST_REMOVE, jsonObject, Constants.TAG_SHORTLIST_REMOVE, false);
            }
        });

        adapter.setOnItemClickListener(new BaseRecyclerAdapter.OnItemClickListener() {
            @Override
            public void onItemClick(View view, int position) {
                if (view.getTag() != null) {
                    Intent intent = new Intent(getApplicationContext(), ProductViewActivity.class);
                    Shortlist shortlistProduct = (Shortlist) view.getTag();

                    ArrayList<String> arrayList_img = new ArrayList<String>();
                    Product product = new Product();
                    product.setProduct_id(shortlistProduct.getProduct_id());
                    product.setCategory_id("");
                    product.setProduct_addinfo("");
                    product.setProduct_care("");
                    product.setProduct_details("");
                    product.setProduct_discount("0");
                    String str = shortlistProduct.getProduct_img().toString();
                    product.setProduct_img(str);
                    product.setProduct_name("");
                    product.setSizeChart_img("");
                    product.setProduct_name("");
                    product.setProduct_price("");
                    // product.setProduct_thumbnails(arrayList_img);
                    intent.putExtra(Constants.PRODUCT_NAME, product);
                    startActivity(intent);
                }
            }
        });

        rvFavorites.setLayoutManager(layoutManager);
        rvFavorites.setAdapter(adapter);
    }




    /*==================================================================================================
        Network Call Methods
    ===================================================================================================*/

    /*******
     * Used for photo upload and show progress after user registration/login
     **********/

    public class MyTransferListener implements FTPDataTransferListener {

        void showToastOnMainThread(final String msg) {
            runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    showToast(msg);
                }
            });
        }

        public void started() {
            // Transfer started
            Constants.debug(" Upload Started ...");
            //System.out.println(" Upload Started ...");
        }

        public void transferred(int length) {

            // Yet other length bytes has been transferred since the last time this
            // method was called
            // showToastOnMainThread(" transferred ..." + length);
            //System.out.println(" transferred ..." + length);
        }

        public void completed() {

            // Transfer completed
            Constants.debug(" image uploaded ...");
            showToastOnMainThread("Image uploaded");
            //showToast("Image uploaded succcessfully..!!");
            //System.out.println(" completed ..." );

        }

        public void aborted() {

            // Transfer aborted
            Constants.debug(" transfer aborted , please try again...");
            //System.out.println(" aborted ..." );
        }

        public void failed() {
            // Transfer failed
            Constants.debug("Failed");
        }
    }


    //Volley cancel request
    protected void cancelRequest(int reqId) {
        ApplicationUtil.getInstance().getRequestQueue().cancelAll(reqId);
    }

    //Volley cancel request
    protected void cancelAllRequest() {
        for (Integer tag : tags)
            ApplicationUtil.getInstance().getRequestQueue().cancelAll(tag);
    }

    //Volley cancel request for Fragments
    public void cancelAllRequest(BaseFragment baseFragment) {
        if (baseFragment != null) {
            for (Integer tag : tags)
                ApplicationUtil.getInstance().getRequestQueue().cancelAll(tag);
        }
    }

    /**
     * Send data over network
     * This method uses Query Params & POST method
     */
    protected void postStringData(String URL, final Map<String, String> params,
                                  final int reqId, boolean disableTouch) {
        if (disableTouch) {
            showProgress();
            disableTouch();
        }
        mInstance.postStringData(URL, params, reqId);
        tags.add(reqId);
    }

    /**
     * Send data over network
     * This method uses POJO which is internally converted to JSON Object & POST method
     */
    protected void postJsonData(String URL, Object object, final int reqId, boolean disableTouch) {
        if (disableTouch) {
            showProgress();
            disableTouch();
        }
        try {
            mInstance.postJsonData(URL, new JSONObject(new Gson().toJson(object)), reqId);
        } catch (JSONException e) {
            e.printStackTrace();
        }
        tags.add(reqId);
    }

    /**
     * Send data over network
     * This method uses JSON Object & POST method
     */
    protected void postJsonData(String URL, JSONObject object, final int reqId, boolean disableTouch) {
        if (disableTouch) {
            showProgress();
            disableTouch();
        }
        mInstance.postJsonData(URL, object, reqId);
        tags.add(reqId);
    }

    /**
     * Send data over network which expects json array from server
     * This method uses Query Params & POST method
     */
    protected void postJsonDataArray(String URL, JSONObject object, final int reqId, boolean disableTouch) {
        if (disableTouch) {
            showProgress();
            disableTouch();
        }
        mInstance.postJsonDataArray(URL, object, reqId);
        tags.add(reqId);
    }

    //Same network methods used for fragments

    /**
     * Send data over network
     * This method uses Query Params & POST method
     */
    public void postStringData(BaseFragment fragment, String URL, final Map<String, String> params,
                               final int reqId, boolean disableTouch) {
        if (fragment != null) {
            fragmentHashMap.put(reqId, fragment);
            if (disableTouch) {
                showProgress();
                disableTouch();
            }
            mInstance.postStringData(URL, params, reqId);
            tags.add(reqId);
        }
    }

    /**
     * Send data over network
     * This method uses POJO which is internally converted to JSON Object & POST method
     */
    public void postJsonData(BaseFragment fragment, String URL, Object object, final int reqId,
                             boolean disableTouch) {
        if (fragment != null) {
            fragmentHashMap.put(reqId, fragment);
            if (disableTouch) {
                showProgress();
                disableTouch();
            }
            try {
                mInstance.postJsonData(URL, new JSONObject(new Gson().toJson(object)), reqId);
            } catch (JSONException e) {
                e.printStackTrace();
            }
            tags.add(reqId);
        }
    }

    /**
     * Send data over network
     * This method uses JSON Object & POST method
     */
    public void postJsonData(BaseFragment fragment, String URL, JSONObject jsonObject, final int reqId,
                             boolean disableTouch) {
        if (fragment != null) {
            fragmentHashMap.put(reqId, fragment);
            if (disableTouch) {
                showProgress();
                disableTouch();
            }
            mInstance.postJsonData(URL, jsonObject, reqId);
            tags.add(reqId);
        }
    }

    /**
     * Send data over network which expects json array from server
     * This method uses Query Params & POST method
     */
    public void postJsonDataArray(BaseFragment fragment, String URL, JSONObject object, final int reqId,
                                  boolean disableTouch) {
        if (fragment != null) {
            fragmentHashMap.put(reqId, fragment);
            if (disableTouch) {
                showProgress();
                disableTouch();
            }
            mInstance.postJsonDataArray(URL, object, reqId);
            tags.add(reqId);
        }
    }

    //Used to upload user profile image
    public void uploadFileToFtp(final File file, final int width, final int height) {
        new Thread(new Runnable() {
            @Override
            public void run() {
                FTPClient client = new FTPClient();
                try {
                    File f = Constants.getCompressedImage(getApplicationContext(), file, width, height);
                    client.connect(getString(R.string.ftp_address));
                    client.login(getString(R.string.ftp_username), getString(R.string.ftp_password));
                    client.setType(FTPClient.TYPE_BINARY);
                    client.changeDirectory("/images/");
                    client.upload(f, new MyTransferListener());

                } catch (Exception e) {
                    e.printStackTrace();
                    try {
                        client.disconnect(true);
                    } catch (Exception e2) {
                        e2.printStackTrace();
                    }
                }
            }
        }).start();
    }

    /**
     * Network callback if the server returns 200 msg code
     * Override this method in your Activity and type cast the object
     * Use @param tag : tag to distinguish multiple request made by activity
     */
    @Override
    public <T> void onResponse(T obj, int tag) {
        hideProgress();
        enableTouch();
        BaseFragment baseFragment = fragmentHashMap.get(tag);

        if (baseFragment != null)
            baseFragment.onResponse(obj, tag);

        switch (tag) {
            case Constants.TAG_CATEGORIES:
                drawerAdapter.setCategories(new ArrayList<>(Arrays.asList((Category[]) obj)));
                break;
            case Constants.TAG_SHORTLIST_REMOVE:
                showToast(getString(R.string.toast_removed_from_shortlist));
                Shortlist shortlist = (Shortlist) obj;
                deleteRecord(shortlist, Constants.TAG_SHORTLIST_REMOVE);
                findViewById(R.id.pbWheelShortlist).setVisibility(View.GONE);
                break;
        }
    }

    /**
     * Network callback if the server returns err msg
     */
    @Override
    public void onError(int error_code, int tag, Object object) {
        hideProgress();
        enableTouch();
        BaseFragment baseFragment = fragmentHashMap.get(tag);

        if (baseFragment != null)
            baseFragment.onError(error_code, tag, object);

    }


    private void setUpProgressBar() {
        progressWheel = (ProgressWheel) findViewById(R.id.pbWheel);
        if (progressWheel != null)
            progressWheel.setBarColor(getResources().getColor(R.color.color_accent));
    }

    /*======================================================================================
        Database Call Methods
     ======================================================================================*/

    /**
     * Used for inserting single object into database
     * After successful insertion onCompleteInsertion will be called
     */
    protected void insertRecord(Object object, int tag) {
        new DatabaseOperation(this, this, object, tag, Constants.Records.INSERT).execute();
    }

    /**
     * Used for inserting list of objects into database
     * After successful insertion onCompleteInsertion will be called
     */
    protected void insertRecord(ArrayList objects, int tag) {
        new DatabaseOperation(this, this, objects, tag, Constants.Records.INSERT_ALL).execute();
    }

    /**
     * Used for retrieving list of objects from database
     * After successful insertion onCompleteRetrieval will be called
     */
    protected void selectRecords(int tag) {
        new DatabaseOperation(this, this, null, tag, Constants.Records.SELECT).execute();
    }

    /**
     * Used for deleting single object from database
     * After successful insertion onCompleteDeletion will be called
     */
    protected void deleteRecord(Object object, int tag) {
        new DatabaseOperation(this, this, object, tag, Constants.Records.DELETE).execute();
    }

    /**
     * Used for deleting list of objects from database
     * After successful insertion onCompleteDeletion will be called
     */
    public void deleteAllRecords(int tag) {
        new DatabaseOperation(this, this, null, tag, Constants.Records.DELETE_ALL).execute();
    }

    //For fragments
    public void insertRecord(BaseFragment fragment, Object object, int tag) {
        if (fragment != null) {
            fragmentHashMap.put(tag, fragment);
            new DatabaseOperation(this, this, object, tag, Constants.Records.INSERT).execute();
        }
    }

    public void deleteRecord(BaseFragment fragment, Object object, int tag) {
        if (fragment != null) {
            fragmentHashMap.put(tag, fragment);
            new DatabaseOperation(this, this, object, tag, Constants.Records.DELETE).execute();
        }
    }

    public void insertRecord(BaseFragment fragment, ArrayList objects, int tag) {
        if (fragment != null) {
            fragmentHashMap.put(tag, fragment);
            new DatabaseOperation(this, this, objects, tag, Constants.Records.INSERT_ALL).execute();
        }
    }

    public void selectRecords(BaseFragment fragment, int tag) {
        if (fragment != null) {
            fragmentHashMap.put(tag, fragment);
            new DatabaseOperation(this, this, null, tag, Constants.Records.SELECT).execute();
        }
    }

    @Override
    public void onCompleteInsertion(int tag, String msg) {
        switch (tag) {
            case Constants.TAG_SHORTLIST:
                selectRecords(Constants.TAG_SHORTLIST);
                break;
        }
        BaseFragment baseFragment = fragmentHashMap.get(tag);

        if (baseFragment != null) {
            baseFragment.onCompleteInsertion(tag, msg);
        }
    }

    @Override
    public void onCompleteUpdation(int tag, String msg) {
        BaseFragment baseFragment = fragmentHashMap.get(tag);

        if (baseFragment != null) {
            baseFragment.onCompleteUpdation(tag, msg);
        }
    }

    @Override
    public void onCompleteRetrieval(int tag, ArrayList obj, String msg) {
        switch (tag) {
            case Constants.TAG_USER:
                if (obj != null && obj.size() > 0)
                    user = (User) obj.get(0);
                if (drawerAdapter != null)
                    drawerAdapter.setUser(user);
                break;
            case Constants.TAG_SHORTLIST:
                if (obj != null) {
                    shortlists = obj;
                    showShortlistedItems(shortListOrientation);
                }
                break;
        }
        BaseFragment baseFragment = fragmentHashMap.get(tag);

        if (baseFragment != null) {
            baseFragment.onCompleteRetrieval(tag, obj, msg);
        }
    }

    @Override
    public void onCompleteDeletion(int tag, String msg) {
        BaseFragment baseFragment = fragmentHashMap.get(tag);

        if (baseFragment != null) {
            baseFragment.onCompleteDeletion(tag, msg);
        }

        switch (tag) {
            case Constants.TAG_SHORTLIST_REMOVE:
                selectRecords(Constants.TAG_SHORTLIST);
                break;
        }
    }

    @Override
    public void onDatabaseError(int tag, int status, String msg) {
        showToast(msg);
        BaseFragment baseFragment = fragmentHashMap.get(tag);
        if (baseFragment != null) {
            baseFragment.onDatabaseError(status, msg);
        }
    }

    /*======================================================================================
        Fragment Methods
     ======================================================================================*/

    /**
     * User for adding fragment in a activity
     *
     * @param container : FrameLayout for the fragment
     * @param tag       : Request Tag for adding to backstack
     */
    protected void addFragment(int container, Fragment fragment, String tag) {
        if (mFragmentManager != null) {
            mFragmentManager.beginTransaction().add(container, fragment, tag).
                    addToBackStack(tag).commit();
        } else
            Constants.error(ERROR_MSG1);
    }

    /**
     * User for replacing fragment in a activity
     *
     * @param container : FrameLayout for the fragment
     * @param tag       : Request Tag for adding to backstack
     */
    protected void replaceFragment(int container, Fragment fragment, String tag) {
        if (mFragmentManager != null) {
            mFragmentManager.beginTransaction().replace(container, fragment, tag).
                    addToBackStack(tag).commitAllowingStateLoss();
        } else
            Constants.error(ERROR_MSG1);
    }

    /**
     * User for replacing fragment in a fragment
     *
     * @param container : FrameLayout for the fragment
     * @param tag       : Request Tag for adding to backstack
     */
    public void replaceFragfromFrag(BaseFragment baseFragment, int container, Fragment fragment, String tag) {
        if (baseFragment != null) {
            replaceFragment(container, fragment, tag);
        }
    }

    /**
     * User for adding fragment in a fragment
     *
     * @param container : FrameLayout for the fragment
     * @param tag       : Request Tag for adding to backstack
     */
    public void addFragfromFrag(BaseFragment baseFragment, int container, Fragment fragment, String tag) {
        if (baseFragment != null) {
            addFragment(container, fragment, tag);
        }
    }

    /*======================================================================================
        Callbacks
     ======================================================================================*/

    @Override
    public void dropComplete(View view, int tag, Object object) {
    }

    //    abstract void backStackChanged();

    /**
     * SlidingPanelListerner called whenever sliding panel state is changed
     */
    class SlidingPanelListener implements SlidingUpPanelLayout.PanelSlideListener {

        @Override
        public void onPanelSlide(View view, float v) {
        }

        @Override
        public void onPanelCollapsed(View view) {

        }

        @Override
        public void onPanelExpanded(View view) {
            shortListOrientation = SHORTLIST_VERTICAL;
            showShortlistedItems(shortListOrientation);

            int imageResource = getResources().getIdentifier("@drawable/down_arrow", null, getPackageName());
            Drawable res = getResources().getDrawable(imageResource);
            ((ImageView) findViewById(R.id.ivArrow)).setImageDrawable(res);

        }

        @Override
        public void onPanelAnchored(View view) {
            shortListOrientation = SHORTLIST_HORIZONTAL;
            showShortlistedItems(shortListOrientation);
            int imageResource = getResources().getIdentifier("@drawable/up_arrow", null, getPackageName());
            Drawable res = getResources().getDrawable(imageResource);
            ((ImageView) findViewById(R.id.ivArrow)).setImageDrawable(res);
        }

        @Override
        public void onPanelHidden(View view) {

        }
    }

    /**
     * Fragment back stack listener
     */
    class BaseBackStackChangedListener implements FragmentManager.OnBackStackChangedListener {

        @Override
        public void onBackStackChanged() {

        }
    }

    /*======================================================================================
        Facebook Methods
     ======================================================================================*/
    public SimpleFacebook mSimpleFacebook;
    private Permission[] permissions = new Permission[]{
            Permission.EMAIL,
            Permission.PUBLIC_PROFILE,
            Permission.USER_ABOUT_ME,
            Permission.USER_BIRTHDAY,
            Permission.USER_EDUCATION_HISTORY,
            Permission.USER_RELATIONSHIPS
    };
    private FacebookCallbacks facebookCallbacks;

    /**
     * Used for setting facebook config,call this method only once
     */
    protected void initFacebookConfig() {
        SimpleFacebookConfiguration configuration = new SimpleFacebookConfiguration.Builder()
                .setAppId(getResources().getString(R.string.facebook_app_id))
                .setNamespace(getString(R.string.app_name))
                .setPermissions(permissions)
                .build();
        SimpleFacebook.setConfiguration(configuration);
        mSimpleFacebook = SimpleFacebook.getInstance(this);
    }

    /**
     * Login with facebook
     */
    protected void loginWithFacebook() {
        mSimpleFacebook.login(onLoginListener);
    }

    public void logoutFromFacebook() {
        mSimpleFacebook.logout(onLogoutListener);
    }


    /**
     * Gets User profile details from facebook
     * OnGetProfile is called when details are fetched from facebook
     */
    protected void getFacebookProfile() {
        showProgress();
        PictureAttributes pictureAttributes = Attributes.createPictureAttributes();
        pictureAttributes.setType(PictureAttributes.PictureType.LARGE);

        Profile.Properties properties = new Profile.Properties.Builder()
                .add(Profile.Properties.ID)
                .add(Profile.Properties.FIRST_NAME)
                .add(Profile.Properties.LAST_NAME)
                .add(Profile.Properties.PICTURE, pictureAttributes)
                .add(Profile.Properties.EMAIL)
                .add(Profile.Properties.GENDER)
                .add(Profile.Properties.AGE_RANGE)
                .add(Profile.Properties.BIRTHDAY)
                .build();
        mSimpleFacebook.getProfile(properties, onProfileListener);
    }

    private OnProfileListener onProfileListener = new OnProfileListener() {
        @Override
        public void onComplete(Profile profile) {
            Constants.debug(profile.toString());
            hideProgress();
            if (facebookCallbacks != null)
                facebookCallbacks.onGetProfile(profile);
        }
    };

    private OnLoginListener onLoginListener = new OnLoginListener() {

        @Override
        public void onLogin(String accessToken, List<Permission> acceptedPermissions, List<Permission> declinedPermissions) {
            if (facebookCallbacks != null) {
                facebookCallbacks.onLoginSuccess(accessToken);
            }
        }

        @Override
        public void onCancel() {
            Constants.debug("Login cancel");
            showToast("Login Canceled");
        }

        @Override
        public void onFail(String reason) {
            showToast(reason);
            Constants.error("Login failed: " + reason);
        }

        @Override
        public void onException(Throwable throwable) {
            Constants.error(throwable.getMessage());
            throwable.printStackTrace();
        }
    };


    //
    //
    // listener
    OnLogoutListener onLogoutListener = new OnLogoutListener() {

        @Override
        public void onLogout() {
            Log.i("FB", "You are logged out from fb");
        }

    };

    protected interface FacebookCallbacks {
        void onLoginSuccess(String accessToken);

        void onGetProfile(Profile profile);

        void onLogoutSuccess();
    }

    /*======================================================================================
        @Override Activity Methods
     ======================================================================================*/

    @Override
    public void onClick(View v) {
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (mSimpleFacebook != null)
            mSimpleFacebook.onActivityResult(requestCode, resultCode, data);
        super.onActivityResult(requestCode, resultCode, data);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                if (mDrawerLayout != null)
                    mDrawerLayout.openDrawer(GravityCompat.START);
                else
                    onBackPressed();
                return true;
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    protected void onPause() {
        super.onPause();
    }

    @Override
    protected void onResume() {
        super.onResume();
    }

    /**
     * OnBackPress handles fragment lifecycle, closes drawer if open and
     * closes panel if it is in expanded state
     */
    @Override
    public void onBackPressed() {
        if (mDrawerLayout != null && mDrawerLayout.isDrawerOpen(Gravity.LEFT)) {
            closeDrawer();
            return;
        }

        if (panelLayout != null && panelLayout.getPanelState() == SlidingUpPanelLayout.PanelState.EXPANDED) {
            panelLayout.setPanelState(SlidingUpPanelLayout.PanelState.COLLAPSED);
            return;
        }

        if (mFragmentManager != null && mFragmentManager.getBackStackEntryCount() > 1)
            mFragmentManager.popBackStack();
        else {
            if (clearActivities) {
                Intent intent = new Intent(Intent.ACTION_MAIN);
                intent.addCategory(Intent.CATEGORY_HOME);
                intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                startActivity(intent);
            } else
                finish();
        }
//            super.onBackPressed();
    }

    @Override
    protected void attachBaseContext(Context newBase) {
        super.attachBaseContext(CalligraphyContextWrapper.wrap(newBase));
    }

    /*======================================================================================
        Getter Setters
     ======================================================================================*/

    public boolean isShowProgress() {
        return showProgress;
    }

    public void setShowProgress(boolean showProgress) {
        this.showProgress = showProgress;
    }

    public Toolbar getToolbar() {
        return toolbar;
    }

    public void setFacebookCallbacks(FacebookCallbacks facebookCallbacks) {
        this.facebookCallbacks = facebookCallbacks;
    }

    public FragmentManager getmFragmentManager() {
        return mFragmentManager;
    }

    public void setClearActivities(boolean clearActivities) {
        this.clearActivities = clearActivities;
    }
}
