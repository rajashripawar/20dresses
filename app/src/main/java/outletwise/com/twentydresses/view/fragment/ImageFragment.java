package outletwise.com.twentydresses.view.fragment;

import android.os.Bundle;
import android.support.v4.app.DialogFragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;

import com.squareup.picasso.Picasso;

import outletwise.com.twentydresses.R;
import outletwise.com.twentydresses.utilities.Constants;


public class ImageFragment extends DialogFragment {

    private String sizeChartImg;
    private ImageView imgvw_size_chart;
    private View rootView;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        rootView = inflater.inflate(R.layout.fragment_image, container, false);
        init();
        // Inflate the layout for this fragment
        return rootView;
    }

    private void init() {

        sizeChartImg = getArguments().getString(Constants.SIZE_CHART_IMAGE);
        Constants.debug(sizeChartImg);
        if(sizeChartImg != null)
        imgvw_size_chart = (ImageView) rootView.findViewById(R.id.img_sizechart);

        Picasso.with(getContext()).load(sizeChartImg).into(imgvw_size_chart);

        rootView.findViewById(R.id.img_cancel).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dismiss();
            }
        });


    }


}
