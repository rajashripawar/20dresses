package outletwise.com.twentydresses.view.fragment;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;

import org.json.JSONException;
import org.json.JSONObject;

import outletwise.com.twentydresses.R;
import outletwise.com.twentydresses.model.OrderReturnResponse;
import outletwise.com.twentydresses.model.PinCodeResponse;
import outletwise.com.twentydresses.model.ReturnExchangeRequest;
import outletwise.com.twentydresses.utilities.Constants;
import outletwise.com.twentydresses.utilities.Urls;

public class OrderReturnFragment extends BaseFragment {

    private TextView tvZipCode;
    private Button bSubmit;
    private View rootView;
    private OrderReturnResponse orderReturnResponse;
    private ReturnExchangeRequest returnExchangeRequest;


    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        rootView = inflater.inflate(R.layout.fragment_myorders_return, container, false);
        getToolBar().setTitle(getString(R.string.toolbar_return));
        init();

        bSubmit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String zipCode = tvZipCode.getText().toString().trim();
                if (!zipCode.equalsIgnoreCase("") && zipCode.length() == 6) {
                  /*  HashMap<String, String> params = new HashMap<>();
                    params.put(Constants.PINCODE, zipCode);
                    postStringData(Urls.CHK_DEL_OPTS, params, Constants.TAG_CHK_DEL_OPTS_FRAG, true);*/

                    JSONObject jsonObject = new JSONObject();
                    try {
                        jsonObject.put(Constants.PINCODE, zipCode);

                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                    postJsonData(Urls.CHK_DEL_OPTS, jsonObject, Constants.TAG_CHK_DEL_OPTS_FRAG, true);


                } else
                    showToast(getString(R.string.valid_pincode));
            }
        });

        rootView.findViewById(R.id.bShip).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                ((ReturnMode) mActivity).onReturnModeSelected(((Button) v).getText().toString());
            }
        });

        rootView.findViewById(R.id.bArrange).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                ((ReturnMode) mActivity).onReturnModeSelected(getString(R.string.button_arrange));
            }
        });

        return rootView;
    }

    void init() {
        orderReturnResponse = getArguments().getParcelable(OrderReturnResponse.class.getSimpleName());
        returnExchangeRequest = getArguments().getParcelable(ReturnExchangeRequest.class.getSimpleName());

        tvZipCode = (TextView) rootView.findViewById(R.id.tvZipCode);
        bSubmit = (Button) rootView.findViewById(R.id.bSubmit);
    }

    public OrderReturnResponse getOrderReturnResponse() {
        return orderReturnResponse;
    }

    public ReturnExchangeRequest getReturnExchangeRequest() {
        return returnExchangeRequest;
    }


    @Override
    public <T> void onResponse(T obj, int tag) {
        super.onResponse(obj, tag);
        switch (tag) {
            case Constants.TAG_CHK_DEL_OPTS_FRAG:
                PinCodeResponse pinCodeResponse = (PinCodeResponse) obj;
                if (pinCodeResponse.getStatus().equalsIgnoreCase(Constants.RESPONSE_SUCCESS)) {
                    bSubmit.setVisibility(View.GONE);
                    rootView.findViewById(R.id.llReturnItem).setVisibility(View.VISIBLE);
                    if (!pinCodeResponse.getPickup_available())
                        rootView.findViewById(R.id.bArrange).setVisibility(View.GONE);
                } else
                    showToast(pinCodeResponse.getMsg());

                break;
        }
    }

    public interface ReturnMode {
        void onReturnModeSelected(String mode);
    }
}
