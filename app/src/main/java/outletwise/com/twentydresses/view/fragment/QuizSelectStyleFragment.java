package outletwise.com.twentydresses.view.fragment;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.view.ViewPager;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.Html;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

import java.util.ArrayList;

import outletwise.com.twentydresses.R;
import outletwise.com.twentydresses.controller.DragListener;
import outletwise.com.twentydresses.controller.adapter.DragDropAdapter;
import outletwise.com.twentydresses.controller.adapter.HorizontalGridAdapter;
import outletwise.com.twentydresses.controller.interfaces.StyleProfileChanged;
import outletwise.com.twentydresses.model.Quiz;
import outletwise.com.twentydresses.model.Registration;
import outletwise.com.twentydresses.model.StyleProfile;
import outletwise.com.twentydresses.model.UserAttrEntity;
import outletwise.com.twentydresses.utilities.Constants;
import outletwise.com.twentydresses.view.custom.PagerContainer;
import outletwise.com.twentydresses.view.decoration.SelectStylePageTransformer;

/**
 * Created by User-PC on 24/07/2015.
 */
public class QuizSelectStyleFragment extends BaseFragment implements DragListener.onDropComplete {

    private static final int PAGE_MARGIN = 30;
    private View rootView;
    private ViewPager viewPager;
    private DragDropAdapter adapter;
    private PagerContainer mContainer;
    private RecyclerView rvMaybe, rvLoveIt;
    private Quiz quiz;
    private ImageView imgvw_help;
    private Constants.Attributes attributes;
    private UserAttrEntity userAttr;
    private TextView bSkipQuiz;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        rootView = inflater.inflate(R.layout.fragment_quiz_select_your_style, container, false);

        imgvw_help = (ImageView) rootView.findViewById(R.id.image_help);
      /*  if (Constants.QUIZ.equalsIgnoreCase(Constants.REGISTER) && isFirstTime()) {
            imgvw_help.setVisibility(View.GONE);
        }*/
        init();
        setUpViewPager();
        setUpRecyclerView();
        quiz = Registration.getInstance().getQuiz();
        bSkipQuiz = (TextView) rootView. findViewById(R.id.bSkipQuiz);
        bSkipQuiz.setText(Html.fromHtml(getString(R.string.button_skip)));

        if (Constants.EDITPROFILE ||Constants.TAKE_QUIZ){
            bSkipQuiz.setVisibility(View.GONE);
        }

        bSkipQuiz.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Constants.SKIPQUIZ = true;
                addFragment(R.id.frame_container, new QuizEnterUserDetails(), QuizEnterUserDetails.class.getSimpleName());
            }
        });
        return rootView;
    }

    private void init() {

        try {
            // For edit profile
            attributes = (Constants.Attributes) getArguments().getSerializable(Constants.Attributes.class.getSimpleName());
            if (attributes == Constants.Attributes.STYLE)
                userAttr = getArguments().getParcelable(StyleProfile.class.getSimpleName());
        } catch (NullPointerException ex) {
            if (!Constants.TAKE_QUIZ && isFirstTime())
                imgvw_help.setVisibility(View.GONE);
        }

    }

    private void setUpRecyclerView() {
        //Maybe view
        ArrayList<Integer> drawablesMaybe = new ArrayList<>();
        rvMaybe = (RecyclerView) rootView.findViewById(R.id.rvDropViewMaybe);
        LinearLayoutManager layoutManager = new LinearLayoutManager(rvMaybe.getContext());
        layoutManager.setOrientation(LinearLayoutManager.HORIZONTAL);
        rvMaybe.setLayoutManager(layoutManager);
        rvMaybe.setOnDragListener(new DragListener(mActivity, this, Constants.SELECT_STYLE_ADAPTER));
        final HorizontalGridAdapter adapterMaybe = new HorizontalGridAdapter(mActivity, drawablesMaybe);
        rvMaybe.setAdapter(adapterMaybe);
        adapterMaybe.setOnItemClickListener(new HorizontalGridAdapter.OnItemClickListener() {
            @Override
            public void onItemClick(View view, int position) {
                view.setTag(adapterMaybe.getItemAtPosition(position));
                addView(view);
                adapterMaybe.removeItem(position);
            }
        });

        //Love it view
        ArrayList<Integer> drawablesLoveIt = new ArrayList<>();
        rvLoveIt = (RecyclerView) rootView.findViewById(R.id.rvDropViewLoveIt);
        layoutManager = new LinearLayoutManager(rvLoveIt.getContext());
        layoutManager.setOrientation(LinearLayoutManager.HORIZONTAL);
        rvLoveIt.setLayoutManager(layoutManager);
        rvLoveIt.setOnDragListener(new DragListener(mActivity, this, Constants.SELECT_STYLE_ADAPTER));
        final HorizontalGridAdapter adapterLoveIt = new HorizontalGridAdapter(mActivity, drawablesLoveIt);
        rvLoveIt.setAdapter(adapterLoveIt);
        adapterLoveIt.setOnItemClickListener(new HorizontalGridAdapter.OnItemClickListener() {
            @Override
            public void onItemClick(View view, int position) {
                view.setTag(adapterLoveIt.getItemAtPosition(position));
                addView(view);
                adapterLoveIt.removeItem(position);
            }
        });
    }

    private void setUpViewPager() {
        mContainer = (PagerContainer) rootView.findViewById(R.id.pager_container);
        viewPager = mContainer.getViewPager();
        adapter = new DragDropAdapter(mActivity);
        viewPager.setAdapter(adapter);
        viewPager.setPageMargin(PAGE_MARGIN);
        viewPager.setOffscreenPageLimit(adapter.getCount());
        viewPager.setClipChildren(false);
        viewPager.setPageTransformer(true, new SelectStylePageTransformer(mActivity));
        viewPager.addOnPageChangeListener(new ViewPager.OnPageChangeListener() {
            @Override
            public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {

            }

            @Override
            public void onPageSelected(int position) {
                if (position == adapter.getCount() - 1) {

                    if (position != adapter.getCount() - 1)
                        adapter.reset(viewPager);

                    quiz.getStyle_attr().setLike(TextUtils.join(",", ((HorizontalGridAdapter) rvLoveIt.getAdapter()).getItemIds()));
                    quiz.getStyle_attr().setMaybe(TextUtils.join(",", ((HorizontalGridAdapter) rvMaybe.getAdapter()).getItemIds()));

                    if (Constants.TAKE_QUIZ) {
                       /* showToast("Please wait while we create your profile..");
                        rootView.findViewById(R.id.pbWheel).setVisibility(View.VISIBLE);
                        new HomeRecommendationFragment().onChangeStyle(quiz.getStyle_attr());*/

                        Intent intent = new Intent();
                        //intent.putExtra(UserAttrEntity.class.getSimpleName(), userAttr);
                        mActivity.setResult(mActivity.RESULT_OK, intent);
                        mActivity.finish();
                    } else if (attributes == Constants.Attributes.STYLE) {
                        showToast(getString(R.string.text_create_profile));
                        rootView.findViewById(R.id.pbWheel).setVisibility(View.VISIBLE);
                        ((StyleProfileChanged) mActivity).onChangeStyle(quiz.getStyle_attr());
                    } else
                        addFragment(R.id.frame_container, new QuizEnterUserDetails(), QuizEnterUserDetails.class.getSimpleName());

                }
            }

            @Override
            public void onPageScrollStateChanged(int state) {
            }
        });
    }

    @Override
    public void dropComplete(View view, int tag, Object object) {
        int currentItem = viewPager.getCurrentItem();
        if (viewPager.getCurrentItem() != adapter.getCount() - 1)
            viewPager.setCurrentItem(currentItem + 1);
        removeView(adapter.getView(currentItem));
    }

    public void addView(final View view) {
        adapter.addView(view, viewPager);
//        viewPager.setCurrentItem(0);
    }

    public void removeView(final View defunctPage) {
        new Thread(new Runnable() {
            @Override
            public void run() {
                try {
                    Thread.sleep(500);
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
                mActivity.runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        int pos = adapter.removeView(viewPager, defunctPage);
                        viewPager.setCurrentItem(pos);
                    }
                });
            }
        }).start();
    }

    @Override
    public void onResume() {
        super.onResume();
        try {
            ((TextView) getToolBar().findViewById(R.id.tvStep)).setText("4 of 5");
        } catch (NullPointerException ex) {
            ex.printStackTrace();
        }
    }
    // Method to check if user is first time registering to show instruction view
    private boolean isFirstTime() {
        String strEmail = "";
        try {
            strEmail = Registration.getInstance().getLogin().getEmail();
        } catch (NullPointerException e) {
            e.printStackTrace();
        }
        SharedPreferences preferences = mActivity.getPreferences(Context.MODE_PRIVATE);
        boolean ranBefore = preferences.getBoolean(strEmail, false);
        if (!ranBefore) {
            SharedPreferences.Editor editor = preferences.edit();
            editor.putBoolean(strEmail, true);
            editor.commit();
            imgvw_help.setVisibility(View.VISIBLE);
            imgvw_help.setOnTouchListener(new View.OnTouchListener() {

                @Override
                public boolean onTouch(View v, MotionEvent event) {
                    imgvw_help.setVisibility(View.GONE);
                    return false;
                }

            });

        }
        return ranBefore;

    }
}
