package outletwise.com.twentydresses.view.fragment;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.Iterator;

import outletwise.com.twentydresses.R;
import outletwise.com.twentydresses.model.FaqAdapter;
import outletwise.com.twentydresses.utilities.Constants;
import outletwise.com.twentydresses.utilities.Urls;

public class InviteFaqFragment extends BaseFragment {

    private View rootView;

    public InviteFaqFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        rootView = inflater.inflate(R.layout.fragment_invite_faq, container, false);
        postJsonData(Urls.INVITE_FAQ, new JSONObject(), Constants.TAG_INVITE_FAQ, true);
        return rootView;
    }

    @Override
    public <T> void onResponse(T obj, int tag) {
        super.onResponse(obj, tag);
        switch (tag) {
            case Constants.TAG_INVITE_FAQ:
                String json = (String) obj;
                ArrayList<String> keys = new ArrayList<>();
                ArrayList<String> values = new ArrayList<>();
                try {
                    JSONObject issueObj = new JSONObject(json);
                    Iterator iterator = issueObj.keys();
                    int temp = 0;
                    while (iterator.hasNext()) {
                        String key = (String) iterator.next();
                        String keyReplace = key.replace("_", " ");
                        keys.add(temp, keyReplace);

                        String value = issueObj.optString(key);
                        values.add(temp, value);
                        temp++;
                    }
                } catch (JSONException ex) {
                }

                String jsonFaq = values.get(0);

                ArrayList<String> questions = new ArrayList<>();
                ArrayList<String> answers = new ArrayList<>();
                try {
                    JSONObject issueObj = new JSONObject(jsonFaq);
                    Iterator iterator = issueObj.keys();
                    int temp = 0;
                    while (iterator.hasNext()) {
                        String key = (String) iterator.next();
                        String keyReplace = key.replace("_", " ");
                        questions.add(temp, keyReplace);

                        String value = issueObj.optString(key);
                        answers.add(temp, value);
                        temp++;
                    }

                    FaqAdapter faqAdapter = new FaqAdapter(mActivity, questions, answers);
                } catch (JSONException ex) {
                }

                break;
        }
    }

}
