package outletwise.com.twentydresses.view.activity;

import android.app.AlertDialog;
import android.content.ActivityNotFoundException;
import android.content.DialogInterface;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.support.v4.app.TaskStackBuilder;
import android.util.Log;
import android.view.View;
import android.widget.Toast;

import com.appsflyer.AppsFlyerConversionListener;
import com.appsflyer.AppsFlyerLib;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.UnsupportedEncodingException;
import java.lang.ref.WeakReference;
import java.net.URLEncoder;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.Map;

import outletwise.com.twentydresses.ApplicationUtil;
import outletwise.com.twentydresses.R;
import outletwise.com.twentydresses.controller.AnalyticsTracker;
import outletwise.com.twentydresses.controller.CartOperations;
import outletwise.com.twentydresses.model.AppVersionResponse;
import outletwise.com.twentydresses.model.Category;
import outletwise.com.twentydresses.model.Login;
import outletwise.com.twentydresses.model.Registration;
import outletwise.com.twentydresses.model.database.greenbot.User;
import outletwise.com.twentydresses.utilities.Constants;
import outletwise.com.twentydresses.utilities.Urls;

/**
 * Created by User-PC on 13-08-2015.
 * This is first screen of app when app launched.
 */
public class SplashScreen extends BaseActivity {
    private Intent intent;
    private Bundle extras;
    private ArrayList obj;
    private User user;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_splashscreen);

          /*Appsflyer - It is used to track fb campaign, custom campaign*/
        AppsFlyerLib.getInstance().startTracking(this, getResources().getString(R.string.appsflyer_dev_id));
        AppsFlyerLib.getInstance().registerConversionListener(getApplicationContext(), new AppsFlyerConversionListener() {
            @Override
            public void onInstallConversionDataLoaded(Map<String, String> map) {

                String medium = map.get("media_source");
                String campaign = map.get("campaign");
                String source = map.get("agency");
                ApplicationUtil.getInstance().ct.pushInstallReferrer(source, medium, campaign);
//                ContextSdk.setInstallReferrer(urlEncodeUTF8(map), getApplicationContext());
            }

            @Override
            public void onInstallConversionFailure(String s) {
                Constants.debug("Conversion failure :" + s);
            }

            @Override
            public void onAppOpenAttribution(Map<String, String> map) {
                Constants.debug("onAppOpenAttribution :");
            }

            @Override
            public void onAttributionFailure(String s) {
                Constants.debug("onAttributionFailure :" + s);
            }
        });



      /*  //To generate key hash
        try {
            PackageInfo info = getPackageManager().getPackageInfo(
                    "outletwise.com.twentydresses",
                    PackageManager.GET_SIGNATURES);
            for (Signature signature : info.signatures) {
                MessageDigest md = MessageDigest.getInstance("SHA");
                md.update(signature.toByteArray());
                Log.d("KeyHash:", Base64.encodeToString(md.digest(), Base64.DEFAULT));
            }
        } catch (PackageManager.NameNotFoundException e) {

        } catch (NoSuchAlgorithmException e) {

        }*/

    }


    static String urlEncodeUTF8(Map<?, ?> map) {
        StringBuilder sb = new StringBuilder();
        for (Map.Entry<?, ?> entry : map.entrySet()) {
            if (sb.length() > 0) {
                sb.append("&");
            }
            sb.append(String.format("%s=%s", entry.getKey().toString(), entry.getValue().toString()));
        }
        try {
            return URLEncoder.encode(sb.toString(), "UTF-8");
        } catch (UnsupportedEncodingException e) {
            e.printStackTrace();
        }
        return "";
    }

   /* private void handleNotificationClick() {

        intent = getIntent();
       *//* String action = intent.getAction();
        Uri data = intent.getData();

        if (data != null) {
            Constants.debug("Notification data :" + data.toString());

            //Parse data from link
      *//**//*      String scheme = data.getScheme();
            String host = data.getHost();

            List<String> params = data.getPathSegments();
            Constants.debug("Param size on splash: " + params.size());

            for (int i = 0; i < params.size(); i++)
                Constants.debug("Params :: " + params.get(i));*//**//*

        }*//*

        extras = intent.getExtras();
        if (extras != null) {

            // Check if user already logged in else navigate user to registration
            if (obj != null && obj.size() > 0) {

                Uri data = intent.getData();

                if (data != null) {
                    Constants.debug("Notification data :" + data.toString());
                    //Parse data from link
                    String scheme = data.getScheme();
                    String host = data.getHost();

                    List<String> params = data.getPathSegments();
                    Constants.debug("Param size on splash: " + params.size());

                    for (int i = 0; i < params.size(); i++) {
                        Constants.debug("Params :: " + params.get(i));

                        Product p = new Product();
                        p.setProduct_id(params.get(0));

                        Category.SubCat subCat = new Category.SubCat();
                        subCat.setName(params.get(2));
                        subCat.setId(params.get(1));

                        Intent intent = new Intent(getApplicationContext(), ProductViewActivity.class);
                        intent.putExtra(Constants.PRODUCT_NAME, p);
                        intent.putExtra(Constants.CATEGORY, subCat);
                        startActivity(intent);

                        Class parent = ProductViewActivity.class;

                        intent.setFlags(Intent.FLAG_ACTIVITY_MULTIPLE_TASK);
                        TaskStackBuilder tsb = TaskStackBuilder.from(this);
                        tsb.addParentStack(parent);
                        tsb.addNextIntent(intent);
                        tsb.startActivities();
                        finish();
                    }

                } else {

                    String category_id = intent.getStringExtra("category_id");
                    String category_name = intent.getStringExtra("category_name");
                    String cart = intent.getStringExtra("cart");
                    String sale = intent.getStringExtra("sale");
                    String testimonial = intent.getStringExtra("testimonial");
                    String wallet = intent.getStringExtra("wallet");
                    String for_you_category_id = intent.getStringExtra("for_you_category_id");
                    String tip_text = intent.getStringExtra("tip_text");
                    String tip_total = intent.getStringExtra("tip_total");
                    String trends = intent.getStringExtra("trends");
                    String trend_id = intent.getStringExtra("trend_id");
                    String trend_caption = intent.getStringExtra("trend_caption");
                    String trend_total = intent.getStringExtra("trend_total");
                    String invite_earn = intent.getStringExtra("invite_earn");

                    Class parent;
                    Intent intent1;
                    if (category_id != null) {
                        Category.SubCat subCat = new Category.SubCat();
                        subCat.setId(category_id);
                        subCat.setName(category_name);
                        intent1 = new Intent(getApplicationContext(), ProductsActivity.class);
                        intent1.putExtra(Constants.CATEGORY, subCat);
                        parent = ProductsActivity.class;
                    } else if (cart != null && cart.equalsIgnoreCase("true")) {
                        intent1 = new Intent(this, CartActivity.class);
                        parent = CartActivity.class;
                    } else if (sale != null && sale.equalsIgnoreCase("true")) {
                        Category.SubCat subCat = new Category.SubCat();
                        subCat.setId(Constants.SALE_CAT);
                        subCat.setName("Sale");
                        intent1 = new Intent(getApplicationContext(), ProductsActivity.class);
                        intent1.putExtra(Constants.CATEGORY, subCat);
                        parent = ProductsActivity.class;
                    } else if (testimonial != null && testimonial.equalsIgnoreCase("true")) {
                        intent1 = new Intent(getApplicationContext(), TestimonialsActivity.class);
                        parent = TestimonialsActivity.class;
                    } else if (wallet != null && wallet.equalsIgnoreCase("true")) {
                        intent1 = new Intent(getApplicationContext(), MyWalletActivity.class);
                        parent = MyWalletActivity.class;
                    } else if (for_you_category_id != null) {
                        Category.SubCat subCat = new Category.SubCat();
                        subCat.setId(for_you_category_id);
                        subCat.setName("FOR YOU");
                        intent1 = new Intent(getApplicationContext(), ProductsActivity.class);
                        intent1.putExtra(Constants.CATEGORY, subCat);
                        Constants.TIP_NAME = tip_text;
                        Constants.TIP_TOTAL = Integer.parseInt(tip_total);
                        parent = ProductsActivity.class;
                    } else if (trends != null && trends.equalsIgnoreCase("true")) {
                        intent1 = new Intent(getApplicationContext(), TrendsActivity.class);
                        parent = TrendsActivity.class;
                    } else if (trend_id != null) {
                        Category.SubCat subCat = new Category.SubCat();
                        subCat.setId(trend_id);
                        subCat.setName("TRENDS");
                        intent1 = new Intent(getApplicationContext(), ProductsActivity.class);
                        intent1.putExtra(Constants.CATEGORY, subCat);
                        Constants.TIP_NAME = trend_caption;
                        Constants.TIP_TOTAL = Integer.parseInt(trend_total);
                        parent = ProductsActivity.class;
                    } else if (invite_earn != null && invite_earn.equalsIgnoreCase("true")) {
                        intent1 = new Intent(getApplicationContext(), InviteActivity.class);
                        parent = InviteActivity.class;
                    } else {
                        intent1 = new Intent(this, SplashScreen.class);
                        parent = SplashScreen.class;
                    }

                    intent1.setFlags(Intent.FLAG_ACTIVITY_MULTIPLE_TASK);
                    TaskStackBuilder tsb = TaskStackBuilder.from(this);
                    tsb.addParentStack(parent);
                    tsb.addNextIntent(intent1);
                    tsb.startActivities();
                }
            } else {
                startActivity(new Intent(getApplicationContext(), RegistrationActivity.class));
                finish();
            }
        }
    }*/


    @Override
    protected void onNewIntent(Intent intent) {
        super.onNewIntent(intent);
        this.setIntent(intent);
    }

    private void handleNotificationClick() {

       intent = getIntent();
        Class parent;
        Intent intent1;
        /* HashMap<String, Object> deeplinkdata= ContextSdk.gatherDeepLinkData(intent);
        if (deeplinkdata != null) {
            Set set = deeplinkdata.entrySet();
            Iterator iterator = set.iterator();
            while(iterator.hasNext()) {
                Map.Entry mentry = (Map.Entry)iterator.next();
                System.out.print("key is:-------------------> "+ mentry.getKey() +
                        " & Value is:---------------> ");
                System.out.println(mentry.getValue());
                Category.SubCat subCat = new Category.SubCat();
                if (mentry.getKey().equals("category"))//category=4
                {
                    String cidval = new Gson().toJson(mentry.getValue(),String.class);
                    if (cidval != null) {
                        subCat.setId(cidval);
                        subCat.setName("Tops");
                        intent1 = new Intent(getApplicationContext(), ProductsActivity.class);
                        intent1.putExtra(Constants.CATEGORY, subCat);
                        startActivity(intent1);
                        parent = ProductsActivity.class;
                        intent1.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                        intent1.addFlags(Intent.FLAG_ACTIVITY_SINGLE_TOP);
                        TaskStackBuilder tsb = TaskStackBuilder.from(this);
                        tsb.addNextIntentWithParentStack(intent1);
                        tsb.addParentStack(parent);
                        tsb.addNextIntent(intent1);
                        tsb.startActivities();
                        finishAffinity();

                    }
                }

            }
            Constants.debug("Notification data deeplink:" + deeplinkdata.toString());
        }else{*/
        String action = intent.getAction();
        Uri data = intent.getData();
        if (data != null) {
            Constants.debug("Notification data :" + data.toString());
        }
        extras = intent.getExtras();
        if (extras != null) {
            // Check if user already logged in else navigate user to registration
            if (obj != null && obj.size() > 0) {

                String category_id = intent.getStringExtra("category_id");
                String category_name = intent.getStringExtra("category_name");
                String cart = intent.getStringExtra("cart");
                String sale = intent.getStringExtra("sale");
                String testimonial = intent.getStringExtra("testimonial");
                String wallet = intent.getStringExtra("wallet");
                String for_you_category_id = intent.getStringExtra("for_you_category_id");
                String tip_text = intent.getStringExtra("tip_text");
                String tip_total = intent.getStringExtra("tip_total");
                String trends = intent.getStringExtra("trends");
                String trend_id = intent.getStringExtra("trend_id");
                String trend_caption = intent.getStringExtra("trend_caption");
                String trend_total = intent.getStringExtra("trend_total");
                String invite_earn = intent.getStringExtra("invite_earn");


                if (category_id != null) {
                    Category.SubCat subCat = new Category.SubCat();
                    subCat.setId(category_id);
                    subCat.setName(category_name);
                    intent1 = new Intent(getApplicationContext(), ProductsActivity.class);
                    intent1.putExtra(Constants.CATEGORY, subCat);
                    parent = ProductsActivity.class;
                }
                else if (cart != null && cart.equalsIgnoreCase("true")) {
                    intent1 = new Intent(this, CartActivity.class);
                    parent = CartActivity.class;
                } else if (sale != null && sale.equalsIgnoreCase("true")) {
                    Category.SubCat subCat = new Category.SubCat();
                    subCat.setId(Constants.SALE_CAT);
                    subCat.setName("Sale");
                    intent1 = new Intent(getApplicationContext(), ProductsActivity.class);
                    intent1.putExtra(Constants.CATEGORY, subCat);
                    parent = ProductsActivity.class;
                } else if (testimonial != null && testimonial.equalsIgnoreCase("true")) {
                    intent1 = new Intent(getApplicationContext(), TestimonialsActivity.class);
                    parent = TestimonialsActivity.class;
                } else if (wallet != null && wallet.equalsIgnoreCase("true")) {
                    intent1 = new Intent(getApplicationContext(), MyWalletActivity.class);
                    parent = MyWalletActivity.class;
                } else if (for_you_category_id != null) {
                    Category.SubCat subCat = new Category.SubCat();
                    subCat.setId(for_you_category_id);
                    subCat.setName("FOR YOU");
                    intent1 = new Intent(getApplicationContext(), ProductsActivity.class);
                    intent1.putExtra(Constants.CATEGORY, subCat);
                    Constants.TIP_NAME = tip_text;
                    Constants.TIP_TOTAL = Integer.parseInt(tip_total);
                    parent = ProductsActivity.class;
                } else if (trends != null && trends.equalsIgnoreCase("true")) {
                    intent1 = new Intent(getApplicationContext(), TrendsActivity.class);
                    parent = TrendsActivity.class;
                } else if (trend_id != null) {
                    Category.SubCat subCat = new Category.SubCat();
                    subCat.setId(trend_id);
                    subCat.setName("TRENDS");
                    intent1 = new Intent(getApplicationContext(), ProductsActivity.class);
                    intent1.putExtra(Constants.CATEGORY, subCat);
                    Constants.TIP_NAME = trend_caption;
                    Constants.TIP_TOTAL = Integer.parseInt(trend_total);
                    parent = ProductsActivity.class;
                } else if (invite_earn != null && invite_earn.equalsIgnoreCase("true")) {
                    intent1 = new Intent(getApplicationContext(), InviteActivity.class);
                    parent = InviteActivity.class;
                } else {
                    intent1 = new Intent(this, SplashScreen.class);
                    parent = SplashScreen.class;
                }

                intent1.setFlags(Intent.FLAG_ACTIVITY_MULTIPLE_TASK);
                TaskStackBuilder tsb = TaskStackBuilder.from(this);
                tsb.addParentStack(parent);
                tsb.addNextIntent(intent1);
                tsb.startActivities();
            } else {
                startActivity(new Intent(getApplicationContext(), RegistrationActivity.class));
                finish();
            }
        }
      //  }//
    }

    /*
    * To check db for user
    * called when activity lanuch
    * */
    @Override
    public void onCompleteRetrieval(int tag, ArrayList obj, String msg) {
        super.onCompleteRetrieval(tag, obj, msg);
        switch (tag) {
            case Constants.TAG_USER:
                this.obj = obj;
                if (obj != null && obj.size() > 0)
                    user = (User) obj.get(0);
                Log.d("getDeviceId()------------>",""+getDeviceId());
                handleNotificationClick();
                getAppVersion();
                break;
            case Constants.TAG_CART_SELECT:
                CartOperations.getInstance().setCartList(obj);
                postStringData(Urls.CATEGORIES, new HashMap<String, String>(), Constants.TAG_CATEGORIES, false);
                break;
        }
    }

  /*
  * To check app version of app
  * If current version is less than availbale version on play store, show dialog to update app.
  * */

    private void getAppVersion() {
        JSONObject jsonObject = new JSONObject();
        try {
            jsonObject.put(Constants.USER_ID, user.getUser_id());
            jsonObject.put(Constants.AUTH_TOKEN, preferences.getString(Constants.KEY_TOKEN, ""));
        } catch (JSONException e) {
            e.printStackTrace();
        } catch (NullPointerException ex) {
            ex.printStackTrace();
        }

        try {
            jsonObject.put("user_device_id", getDeviceId());
        } catch (JSONException e) {
            e.printStackTrace();
        }

        postJsonData(Urls.CHECK_APP_VERSION, jsonObject, Constants.TAG_CHECK_APP_VESRION, false);
    }

  /*private void autoLogin() {

        JSONObject jsonObject = new JSONObject();
        try {
            jsonObject.put("email", user.getUser_email());
            if (user.getFb_token() == null || user.getFb_token().equalsIgnoreCase(""))
                jsonObject.put("password", user.getUser_password());
            else {
                jsonObject.put("password", "");
                jsonObject.put("facebook_access_token", user.getFb_token());
            }
        } catch (JSONException e) {
            e.printStackTrace();
        }
        postJsonData(Urls.LOGIN, jsonObject, Constants.TAG_LOGIN, true);
    }*/


    @Override
    public <T> void onResponse(T obj, int tag) {
        switch (tag) {
            case Constants.TAG_CATEGORIES:
                Login login = new Login();
                login.setEmail(user.getUser_email());
                Registration.getInstance().setUser(user);
                Registration.getInstance().setLogin(login);
                String user_registered = user.getUser_registered();
                if (extras == null && user_registered != null && user_registered.equalsIgnoreCase("Yes")) {
                    Intent intent = new Intent(this, HomeActivity.class);
                    intent.putParcelableArrayListExtra(Constants.CATEGORY, new ArrayList<>(Arrays.asList((Category[]) obj)));
                    startActivity(intent);
                    finish();
                } else if (user.getGender() != null) {
                    Constants.EDITPROFILE = false;
                    Intent intent = new Intent(getApplicationContext(), QuizActivity.class);
                    startActivity(intent);
                    finish();
                } else {
                    Constants.EDITPROFILE = false;
                    startActivity(new Intent(getApplicationContext(), SelectGenderActivity.class));
                    finish();
                }
                break;
            case Constants.TAG_CHECK_APP_VESRION:
                AppVersionResponse response = (AppVersionResponse) obj;
                if (response.getStatus().equalsIgnoreCase(Constants.RESPONSE_SUCCESS)) {
                    Log.e("","Last_purchase_date() ------------> "+response.getData().getLast_purchase_date());
                    /*Set claver tap event for event profile :: sarika comment below line*/
                    AnalyticsTracker.getInstance().setPurchaseDate(response.getData().getLast_purchase_date(), getApplicationContext());
                    if (response.getData().getUser().equalsIgnoreCase("") || response.getData().getUser().equalsIgnoreCase(Constants.VALID))
                        checkAppVersion(response.getData().getOutdated_version(), response.getData().getVersion());
                    else if (response.getData().getUser().equalsIgnoreCase(Constants.INVALID)) {
                        /*logoutFromFacebook();
                        deleteAllRecords(Constants.TAG_USER);*/
                        signOutFromApp();
                        startActivity(new Intent(getApplicationContext(), RegistrationActivity.class));
                        finish();
                    }
                }
                break;
        }
    }


    @Override
    public void onError(int error_code, int tag, Object object) {
        if (error_code == -1) {
            findViewById(R.id.pbWheel).setVisibility(View.GONE);
            findViewById(R.id.tvNetwork).setVisibility(View.VISIBLE);
            showErrorDialog(Constants.NO_INTERNET_CONNECTION, null);
        } else if (error_code == Constants.UNAUTHORIZED_CODE) {
            showErrorDialog(Constants.SESSION_EXPIRED_MSG, new Intent(getApplicationContext(), LoginActivity.class));
        } else if (error_code == Constants.SERVER_DOWN_CODE || error_code == Constants.SERVER_DOWN_CODE1) {
            showErrorDialog(Constants.SERVER_DOWN_MSG, null);
        } else if (error_code == 99)
            showToast(object.toString());
        else
            showToast(Constants.SERVER_ERROR);
    }


    /*
   * To check version of app
   * If current version is lower than published, show dialog to update version.
   * If we want user to update version forcefully, outDatedVersion from API need to change.
   * */
    public void checkAppVersion(Float outDatedVersion, Float newVersionAvailable) {
        Float newVersion = newVersionAvailable;
        Float outdatedVersion = outDatedVersion;
        Float currentVersion = null;
        currentVersion = getCurrentAppVersion();

        final WeakReference<SplashScreen> splashScreenWeakReference;
        splashScreenWeakReference = new WeakReference<SplashScreen>(this);

        int forceUpdateVal = Float.compare(currentVersion, outdatedVersion);// Check for force update
        int updateVal = Float.compare(currentVersion, newVersion);// Check for recommended update

        if (forceUpdateVal > 0) {
            System.out.println("current version is greater than outdated version - no force update");
            if (updateVal >= 0) {
                System.out.println("current version is equal to new version");
                if (extras == null) {
                    if (obj.size() != 0) {
                        user = (User) obj.get(0);
                        selectRecords(Constants.TAG_CART_SELECT);
                    } else {
                        startActivity(new Intent(getApplicationContext(), RegistrationActivity.class));
                        finish();
                    }
                } else if (obj.size() == 0) {
                    startActivity(new Intent(getApplicationContext(), RegistrationActivity.class));
                    finish();
                }

            } else {
                if (splashScreenWeakReference.get() != null && !splashScreenWeakReference.get().isFinishing()) {
                    AlertDialog.Builder builder1 = new AlertDialog.Builder(SplashScreen.this);
                    //Update your app now!
                    //Looks like you are running an old version.Upgrade to get a faster & smoother experience on our latest app
                    builder1.setMessage(getString(R.string.text_app_update));
                    builder1.setCancelable(false);
                    builder1.setPositiveButton(
                            "Update",
                            new DialogInterface.OnClickListener() {
                                public void onClick(DialogInterface dialog, int id) {
                                    finish();
                                    Uri uri = Uri.parse("market://details?id=" + getPackageName());
                                    Intent myAppLinkToMarket = new Intent(Intent.ACTION_VIEW, uri);
                                    try {
                                        startActivity(myAppLinkToMarket);
                                    } catch (ActivityNotFoundException e) {
                                        Toast.makeText(getApplicationContext(), " unable to find market app", Toast.LENGTH_LONG).show();
                                    }
                                }
                            });

                    builder1.setNegativeButton(
                            "Later",
                            new DialogInterface.OnClickListener() {
                                public void onClick(DialogInterface dialog, int id) {
                                    if (extras == null) {
                                        if (obj.size() != 0) {
                                            user = (User) obj.get(0);
                                            selectRecords(Constants.TAG_CART_SELECT);
                                        } else {
                                            startActivity(new Intent(getApplicationContext(), RegistrationActivity.class));
                                            finish();
                                        }
                                    } else if (obj.size() == 0) {
                                        startActivity(new Intent(getApplicationContext(), RegistrationActivity.class));
                                        finish();
                                    }


                                }
                            });

                    AlertDialog alert11 = builder1.create();
                    alert11.show();
                }
            }
        } else {
            System.out.println("current version is less than outdated version - force update");
            if (splashScreenWeakReference.get() != null && !splashScreenWeakReference.get().isFinishing()) {
                AlertDialog.Builder builder1 = new AlertDialog.Builder(SplashScreen.this);
                builder1.setMessage(getResources().getString(R.string.text_app_update));
                builder1.setCancelable(false);

                builder1.setPositiveButton(
                        "Update",
                        new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int id) {
                                finish();
                                Uri uri = Uri.parse("market://details?id=" + getPackageName());
                                Intent myAppLinkToMarket = new Intent(Intent.ACTION_VIEW, uri);
                                try {
                                    startActivity(myAppLinkToMarket);
                                } catch (ActivityNotFoundException e) {
                                    Toast.makeText(getApplicationContext(), " unable to find market app", Toast.LENGTH_LONG).show();
                                }
                            }
                        });

                builder1.setNegativeButton(
                        "Cancel",
                        new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int id) {
                                finish();
                            }
                        });

                AlertDialog alert11 = builder1.create();
                alert11.show();
            }

        }
    }
}
