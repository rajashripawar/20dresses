package outletwise.com.twentydresses.view.activity;

import android.os.Bundle;
import android.view.View;
import android.widget.TextView;

import java.util.HashMap;

import outletwise.com.twentydresses.R;
import outletwise.com.twentydresses.model.VisitorResponse;
import outletwise.com.twentydresses.utilities.Constants;
import outletwise.com.twentydresses.utilities.Urls;

/**
 * This class is use to sent mail to user if user forgot his/her password of 20d account.
 */
public class ForgotPasswordActivity extends BaseActivity {

    private TextView tv_email;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_forgot_password);
        tv_email = (TextView) findViewById(R.id.etEmail);

        findViewById(R.id.bSubmit).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (!Constants.isValidEmail(tv_email.getText().toString().trim())) {
                    showToast(getString(R.string.invalid_email));
                    tv_email.requestFocus();
                } else {
                    findViewById(R.id.pbWheel).setVisibility(View.VISIBLE);
                    HashMap<String, String> params = new HashMap<>();
                    params.put("email", tv_email.getText().toString());
                    postStringData(Urls.FORGOT_PASSWORD, params, Constants.TAG_FORGOT_PASSWORD, true);
                }
            }
        });
    }

    @Override
    public <T> void onResponse(T obj, int tag) {
        super.onResponse(obj, tag);
        switch (tag) {
            case Constants.TAG_FORGOT_PASSWORD:
                VisitorResponse visitorResponse = (VisitorResponse) obj;
                if (visitorResponse.getStatus().equalsIgnoreCase(Constants.RESPONSE_SUCCESS)) {
                    findViewById(R.id.pbWheel).setVisibility(View.GONE);
                    finish();
                    showToast(visitorResponse.getMessage());
                } else
                    showToast(visitorResponse.getMessage());
                break;
        }
    }
}
