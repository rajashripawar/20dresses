package outletwise.com.twentydresses.view.activity;

import android.content.Intent;
import android.os.Bundle;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.app.ActionBar;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;

import org.json.JSONObject;

import java.util.ArrayList;

import outletwise.com.twentydresses.R;
import outletwise.com.twentydresses.controller.AnalyticsTracker;
import outletwise.com.twentydresses.controller.adapter.BaseRecyclerAdapter;
import outletwise.com.twentydresses.controller.adapter.TrendsAdapter;
import outletwise.com.twentydresses.model.Category;
import outletwise.com.twentydresses.model.Trends;
import outletwise.com.twentydresses.utilities.Constants;
import outletwise.com.twentydresses.utilities.Urls;

public class TrendsActivity extends BaseActivity implements SwipeRefreshLayout.OnRefreshListener {

    private SwipeRefreshLayout mSwipeRefreshLayout;
    private TrendsAdapter adapter;
    private ArrayList<Trends.DataEntity> data = new ArrayList<>();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_trends);
        setUpToolBar(getString(R.string.toolbar_trends));
        final ActionBar ab = getSupportActionBar();
        ab.setDisplayHomeAsUpEnabled(true);

        mSwipeRefreshLayout = (SwipeRefreshLayout) findViewById(R.id.swipeRefreshLayout);
        mSwipeRefreshLayout.setOnRefreshListener(this);
        setUpRecyclerView();
    }

    private void setUpRecyclerView() {
        RecyclerView rv = (RecyclerView) findViewById(R.id.rvTrends);
        // rv.setLayoutManager(new LinearLayoutManager(rv.getContext(), LinearLayoutManager.VERTICAL, false));
        rv.setLayoutManager(new GridLayoutManager(getApplicationContext(), 1));

        adapter = new TrendsAdapter(getApplicationContext(), data);
        rv.setAdapter(adapter);
        adapter.setOnItemClickListener(new BaseRecyclerAdapter.OnItemClickListener() {
            @Override
            public void onItemClick(View view, int position) {
                Trends.DataEntity data = (Trends.DataEntity) view.getTag();
                Constants.debug("Trend Id:: " + data.getTrend_id());

                AnalyticsTracker.getInstance().trackTrendsClicked(data.getTrend_id(), data.getTrend_caption());

                Category.SubCat subCat = new Category.SubCat();
                subCat.setId(data.getTrend_id());
                subCat.setName("TRENDS");

                Intent intent = new Intent(getApplicationContext(), ProductsActivity.class);
                Constants.TIP_NAME = data.getTrend_caption();
                Constants.TIP_TOTAL = Integer.parseInt(data.getTotal());
                intent.putExtra(Constants.CATEGORY, subCat);
                startActivity(intent);
            }
        });

        postJsonData(Urls.TRENDS, new JSONObject(), Constants.TAG_TRENDS, false);
    }

    @Override
    public <T> void onResponse(T obj, int tag) {
        switch (tag) {
            case Constants.TAG_TRENDS:
                findViewById(R.id.pbWheel).setVisibility(View.GONE);
                Trends trends = (Trends) obj;
                if (trends.getStatus().equalsIgnoreCase(Constants.RESPONSE_SUCCESS)) {
                    data.clear();
                    data.addAll(trends.getData());
                    adapter.notifyDataSetChanged();
                } else
                    showToast(trends.getMessage());
                mSwipeRefreshLayout.setRefreshing(false);
                break;
        }
    }

    @Override
    public void onRefresh() {
        setUpRecyclerView();
    }

    @Override
    public void onError(int error_code, int tag, Object object) {
        super.onError(error_code, tag, object);
        checkErrorCode(error_code, object);
    }
}
