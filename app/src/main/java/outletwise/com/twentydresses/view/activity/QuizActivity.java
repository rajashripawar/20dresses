package outletwise.com.twentydresses.view.activity;

import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v7.app.ActionBar;
import android.view.Menu;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

import java.util.List;

import outletwise.com.twentydresses.R;
import outletwise.com.twentydresses.model.Registration;
import outletwise.com.twentydresses.model.UserAttrEntity;
import outletwise.com.twentydresses.model.database.greenbot.User;
import outletwise.com.twentydresses.utilities.Constants;
import outletwise.com.twentydresses.view.fragment.QuizEnterUserDetails;
import outletwise.com.twentydresses.view.fragment.QuizSelectComplexionFragment;
import outletwise.com.twentydresses.view.fragment.QuizSelectShapeFragment;
import outletwise.com.twentydresses.view.fragment.QuizSelectStyleFragment;
import outletwise.com.twentydresses.view.fragment.QuizSelectmeasureFragment;

/**
 * Created by User-PC on 24/07/2015.
 * User can take quiz after successful registration.
 */

public class QuizActivity extends BaseActivity {
    private User user;
    private UserAttrEntity userAttr;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_quiz);
        trackPage(getString(R.string.pgQuiz));
        initFirstFragment();

        setUpToolBar(getString(R.string.toolbar_quiz));
        final ActionBar ab = getSupportActionBar();
        ab.setDisplayHomeAsUpEnabled(true);

        findViewById(R.id.action_signout).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                /*preferences.edit().remove(Constants.KEY_TOKEN).commit();
                logoutFromFacebook();
                deleteAllRecords(Constants.TAG_USER);*/
                signOutFromApp();
            }
        });

       /* findViewById(R.id.bSkipQuiz).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (getSupportFragmentManager().findFragmentById(R.id.frame_container) instanceof QuizSelectComplexionFragment)
                {
                    addFragment(R.id.frame_container, new QuizEnterUserDetails(), QuizEnterUserDetails.class.getSimpleName());

//                    addFragment(R.id.frame_container, new QuizSelectmeasureFragment(), QuizSelectmeasureFragment.class.getSimpleName());

                }else if (getSupportFragmentManager().findFragmentById(R.id.frame_container) instanceof QuizSelectmeasureFragment)
                {
                    addFragment(R.id.frame_container, new QuizEnterUserDetails(), QuizEnterUserDetails.class.getSimpleName());

//                    addFragment(R.id.frame_container, new QuizSelectShapeFragment(), QuizSelectShapeFragment.class.getSimpleName());

                }else if (getSupportFragmentManager().findFragmentById(R.id.frame_container) instanceof QuizSelectShapeFragment)
                {
                    addFragment(R.id.frame_container, new QuizEnterUserDetails(), QuizEnterUserDetails.class.getSimpleName());

//                    addFragment(R.id.frame_container, new QuizSelectStyleFragment(), QuizSelectStyleFragment.class.getSimpleName());
                }
            }
        });*/

    }

    void initFirstFragment() {
        if (Constants.TAKE_QUIZ) {
            findViewById(R.id.action_signout).setVisibility(View.GONE);
            addFragment(R.id.frame_container, new QuizSelectComplexionFragment(), QuizSelectComplexionFragment.class.getSimpleName());
        } else {

            try {
                findViewById(R.id.action_signout).setVisibility(View.VISIBLE);
//                findViewById(R.id.bSkipQuiz).setVisibility(View.VISIBLE);
                user = Registration.getInstance().getUser();
                if (user.getGender() != null && user.getGender().equalsIgnoreCase("F")) {
                    addFragment(R.id.frame_container, new QuizSelectComplexionFragment(), QuizSelectComplexionFragment.class.getSimpleName());
                }else {
                    addFragment(R.id.frame_container, new QuizEnterUserDetails(), QuizEnterUserDetails.class.getSimpleName());
                }
            } catch (NullPointerException ex) {
                //addFragment(R.id.frame_container, new QuizEnterUserDetails(), QuizEnterUserDetails.class.getSimpleName());
            }
        }
    }


    @Override
    public void onBackPressed() {
        super.onBackPressed();
        TextView tv = (TextView) getToolbar().findViewById(R.id.tvStep);
        try {
            List<Fragment> fragmentList = getSupportFragmentManager().getFragments();
            if (fragmentList != null) {
                for (Fragment fragment : fragmentList) {
                    if (fragment instanceof QuizSelectStyleFragment)
                        tv.setText("4 of 5");
                }
            }

            Fragment f = this.getCurrentFragment();
            if (f != null) {
                switch (f.getClass().getSimpleName()) {
                    case "QuizSelectStyleFragment":
                        tv.setText("3 of 5");
                        break;
                    case "QuizSelectShapeFragment":
                        tv.setText("2 of 5");
                        break;
                    case "QuizSelectmeasureFragment":
                        tv.setText("1 of 5");
                        break;
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        }

    }

    private Fragment getCurrentFragment() {
        try {
            FragmentManager fragmentManager = getSupportFragmentManager();
            String fragmentTag = fragmentManager.getBackStackEntryAt(fragmentManager.getBackStackEntryCount() - 1).getName();
            Fragment currentFragment = getSupportFragmentManager()
                    .findFragmentByTag(fragmentTag);
            return currentFragment;
        } catch (NullPointerException ex) {
            return null;
        }
    }

    @Override
    public void onError(int error_code, int tag, Object object) {
        checkErrorCode(error_code, object);
    }

    @Override
    public void onCompleteDeletion(int tag, String msg) {
        super.onCompleteDeletion(tag, msg);
        switch (tag) {
            case Constants.TAG_USER:
                finish();
                startActivity(new Intent(this, RegistrationActivity.class));
                break;
        }
    }
}
