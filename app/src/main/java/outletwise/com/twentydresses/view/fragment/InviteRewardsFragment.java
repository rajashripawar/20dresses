package outletwise.com.twentydresses.view.fragment;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;

import org.json.JSONException;
import org.json.JSONObject;

import outletwise.com.twentydresses.R;
import outletwise.com.twentydresses.controller.adapter.RewardAdapter;
import outletwise.com.twentydresses.model.Rewards;
import outletwise.com.twentydresses.utilities.Constants;
import outletwise.com.twentydresses.utilities.Urls;
import outletwise.com.twentydresses.view.activity.BaseActivity;

public class InviteRewardsFragment extends BaseFragment {

    private View rootView;

    public InviteRewardsFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        rootView = inflater.inflate(R.layout.fragment_invite_rewards, container, false);
        getRewardsData();
        return rootView;
    }

    private void getRewardsData() {

        Long user_id = getArguments().getLong(Constants.USER_ID);

        JSONObject jsonObject = new JSONObject();
        try {
            jsonObject.put(Constants.AUTH_TOKEN, ((BaseActivity) mActivity).preferences.getString(Constants.KEY_TOKEN, ""));
            jsonObject.put(Constants.USER_ID, user_id);
        } catch (JSONException e) {
            e.printStackTrace();
        }

        postJsonData(Urls.INVITE_REWARDS, jsonObject, Constants.TAG_INVITE_REWARDS, false);
    }


    @Override
    public <T> void onResponse(T obj, int tag) {
        switch (tag) {
            case Constants.TAG_INVITE_REWARDS:
                Rewards rewards = (Rewards) obj;
                if (rewards.getStatus().equalsIgnoreCase(Constants.RESPONSE_SUCCESS)) {
                    LinearLayout llRewardItems = (LinearLayout) rootView.findViewById(R.id.llRewardItems);
                    RewardAdapter adapter = new RewardAdapter(mActivity, llRewardItems, rewards.getData());
                } else
                    showToast(rewards.getMessage());

                rootView.findViewById(R.id.pbWheel).setVisibility(View.GONE);
                break;
        }
    }

}
