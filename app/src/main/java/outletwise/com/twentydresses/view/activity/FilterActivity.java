package outletwise.com.twentydresses.view.activity;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.ActionBar;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.TextUtils;
import android.view.View;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.HashSet;

import outletwise.com.twentydresses.R;
import outletwise.com.twentydresses.controller.adapter.FilterDetailAdapter;
import outletwise.com.twentydresses.controller.adapter.FilterMainAdapter;
import outletwise.com.twentydresses.model.Category;
import outletwise.com.twentydresses.model.Filter;
import outletwise.com.twentydresses.model.Product;
import outletwise.com.twentydresses.utilities.Constants;
import outletwise.com.twentydresses.utilities.Urls;
import outletwise.com.twentydresses.view.decoration.DividerItemDecoration;

/**
 * Created by User-PC on 28-09-2015.
 * Class is use to show all filters main category
 */
public class FilterActivity extends BaseActivity {

    private RecyclerView rvMain, rvDetail;
    private FilterMainAdapter adapter;
    private FilterDetailAdapter detailAdapter;
    private ArrayList<Product> products = new ArrayList<>();
    private Category.SubCat subCat;
    private HashMap<String, String> params = new HashMap<>();


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_filter);

        init();
        setUpToolBar(getString(R.string.toolbar_filterby));
        final ActionBar ab = getSupportActionBar();
        ab.setDisplayHomeAsUpEnabled(true);

        findViewById(R.id.bApply).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
             /*   Intent intent = new Intent();
                setResult(RESULT_OK);
                finish();*/

                findViewById(R.id.pbWheel).setVisibility(View.VISIBLE);

                try {
                    if (adapter.getParameterColor() != null && adapter.getAllAppliedFilter_Color() != null)
                        params.put(adapter.getParameterColor(), TextUtils.join(",", adapter.getAllAppliedFilter_Color()));

                    if (adapter.getParameterCategory() != null && adapter.getAllAppliedFilter_Category() != null && adapter.getAllAppliedFilter_Category().size() > 0) {
                        HashSet<String> set = new HashSet<>(adapter.getAllAppliedFilter_Category());
                        // Create ArrayList from the set.
                        ArrayList<String> result = new ArrayList<>(set);
                        params.put(adapter.getParameterCategory(), TextUtils.join(",", result));
                    }

                    if (adapter.getParameterSize() != null && adapter.getAllAppliedFilter_Size() != null)
                        params.put(adapter.getParameterSize(), TextUtils.join(",", adapter.getAllAppliedFilter_Size()));

                    if (adapter.getParameterDiscount() != null && adapter.getAllAppliedFilter_Discount() != null)
                        params.put(adapter.getParameterDiscount(), adapter.getAllAppliedFilter_Discount());

                    if (subCat.getName().equalsIgnoreCase(Constants.NEW))
                        params.put("new", "true");
                    else if (subCat.getName().equalsIgnoreCase(Constants.SALE))
                        params.put("sale", "true");
                    else
                        params.put(Constants.CATEGORY, subCat.getId());

                    postStringData(Urls.PRODUCTS, params, Constants.TAG_FILTER_PRODUCTS, false);
                } catch (NullPointerException ex) {
                    showToast(getString(R.string.no_products));
                    ex.printStackTrace();
                    findViewById(R.id.pbWheel).setVisibility(View.GONE);
                }

            }
        });

        findViewById(R.id.bCancel).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });
    }

    void init() {

        FilterDetailAdapter.allAppliedFilterDiscount = null;
        FilterDetailAdapter.allAppliedFilterCategory.clear();
        FilterDetailAdapter.allAppliedFilterColor.clear();
        FilterDetailAdapter.allAppliedFilterSize.clear();

        rvMain = (RecyclerView) findViewById(R.id.rvMain);
        rvDetail = (RecyclerView) findViewById(R.id.rvDetail);

        subCat = getIntent().getParcelableExtra(Constants.CATEGORY);
        HashMap<String, String> params = new HashMap<>();
        //params.put(Constants.CATEGORY, subCat.getId());
        if (subCat.getName().equalsIgnoreCase(Constants.NEW))
            params.put("new", "true");
        else if (subCat.getName().equalsIgnoreCase(Constants.SALE))
            params.put("sale", "true");
        else
            params.put(Constants.CATEGORY, subCat.getId());

        postStringData(Urls.FILTERS, params, Constants.TAG_FILTER, true);
    }


    @Override
    public <T> void onResponse(T obj, int tag) {
        super.onResponse(obj, tag);
        switch (tag) {
            case Constants.TAG_FILTER:
                loadFilters((Filter) obj);
                break;

            case Constants.TAG_FILTER_PRODUCTS:
                try {
                    findViewById(R.id.pbWheel).setVisibility(View.GONE);
                    products.clear();
                    products.addAll((Arrays.asList((Product[]) obj)));
                    gotoNextActivity();
                } catch (Exception ex) {
                    showToast(getString(R.string.no_products));
                }
                break;
        }
    }

    private void gotoNextActivity() {
        Intent intent = new Intent(getApplicationContext(), FilterProductActivity.class);
        intent.putExtra(Constants.FILTER_PRODUCTS, products);
        intent.putExtra(Constants.APPLIED_FILTERS, params);
        intent.putExtra(Constants.CATEGORY, subCat);
        startActivityForResult(intent, 1);
    /*    startActivity(intent);
        finish();*/
    }


    void loadFilters(Filter filter) {

        adapter = new FilterMainAdapter(this, filter, subCat);
        adapter.setRvDetail(rvDetail);
        rvMain.setLayoutManager(new LinearLayoutManager(this));
        rvMain.addItemDecoration(new DividerItemDecoration(this, DividerItemDecoration.VERTICAL_LIST));
        rvMain.setAdapter(adapter);
    }


    @Override
    public void onError(int error_code, int tag, Object object) {
        super.onError(error_code, tag, object);
        switch (tag) {
            case Constants.TAG_FILTER_PRODUCTS:
                showToast(getString(R.string.no_products));
//                updateCartUI();
                break;
        }
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (resultCode == RESULT_OK)
            if (requestCode == 1)
                subCat = data.getParcelableExtra(Constants.CATEGORY);
    }
}

