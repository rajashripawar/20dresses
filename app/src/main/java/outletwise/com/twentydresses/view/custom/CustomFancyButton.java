package outletwise.com.twentydresses.view.custom;

import android.content.Context;
import android.util.AttributeSet;

import mehdi.sakout.fancybuttons.FancyButton;
import outletwise.com.twentydresses.R;

/**
 * Created by User-PC on 14-08-2015.
 */
public class CustomFancyButton extends FancyButton {

    private int unselectedColor;
    private int unselectedTextColor;
    private int selectedColor;
    private int selectedTextColor;

    public CustomFancyButton(Context context) {
        super(context);
    }

    public CustomFancyButton(Context context, AttributeSet attrs) {
        super(context, attrs);
    }


    @Override
    protected void onFinishInflate() {
        super.onFinishInflate();
        unselectedColor = getContext().getResources().getColor(android.R.color.transparent);
        unselectedTextColor = getContext().getResources().getColor(R.color.color_accent);
    }

    @Override
    public void setSelected(boolean selected) {
        super.setSelected(selected);
        if (selected) {
            setBackgroundColor(selectedColor);
            setTextColor(selectedTextColor);
        } else {
            setBackgroundColor(unselectedColor);
            setTextColor(unselectedTextColor);
        }
    }

    public void setSelectedColor(int selectedColor) {
        this.selectedColor = selectedColor;
    }

    public void setSelectedTextColor(int selectedTextColor) {
        this.selectedTextColor = selectedTextColor;
    }
}
