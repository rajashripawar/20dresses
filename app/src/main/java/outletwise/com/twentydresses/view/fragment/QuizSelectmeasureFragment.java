package outletwise.com.twentydresses.view.fragment;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.text.Html;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ExpandableListView;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.squareup.picasso.Picasso;

import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;

import outletwise.com.twentydresses.R;
import outletwise.com.twentydresses.controller.adapter.ExpandableListAdapter;
import outletwise.com.twentydresses.controller.interfaces.StyleProfileChanged;
import outletwise.com.twentydresses.model.BackgroundImgResponse;
import outletwise.com.twentydresses.model.Quiz;
import outletwise.com.twentydresses.model.Registration;
import outletwise.com.twentydresses.model.Size;
import outletwise.com.twentydresses.model.StyleProfile;
import outletwise.com.twentydresses.model.UserAttrEntity;
import outletwise.com.twentydresses.utilities.Constants;

/**
 * Created by User-PC on 28-07-2015.
 */
public class QuizSelectmeasureFragment extends BaseFragment implements ExpandableListAdapter.OnSizeSelected {

    private Button bNext;
    private View rootView;
    private ExpandableListView expandableListView;
    private ExpandableListAdapter listAdapter;
    private List<String> listDataHeader;
    private HashMap<String, List<Size>> listDataChild;
    private int lastExpandedPosition = -1;
    private Quiz quiz;
    private UserAttrEntity userAttr;
    private Constants.Attributes attributes;
    private TextView bSkipQuiz;
    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        rootView = inflater.inflate(R.layout.fragment_quiz_select_measure, container, false);
       // setBackgroundImage(Constants.IMG_MEASUREMENT);
        //initQuiz();
        init();
        expandableListView.setOnGroupExpandListener(new ExpandableListView.OnGroupExpandListener() {
            @Override
            public void onGroupExpand(int groupPosition) {
                if (lastExpandedPosition != -1
                        && groupPosition != lastExpandedPosition) {
                    expandableListView.collapseGroup(lastExpandedPosition);
                }
                lastExpandedPosition = groupPosition;
            }
        });
        bNext.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                //  if (validate())
                {
                   // rootView.findViewById(R.id.pbWheel).setVisibility(View.VISIBLE);

                    if (userAttr != null) {
                        userAttr.setHeight(listAdapter.getHeight());
                        ((StyleProfileChanged) mActivity).onChange(userAttr);
                    } else {
                        if (validate()) {
                            quiz.getUser_attr().setHeight(listAdapter.getHeight());
                            addFragment(R.id.frame_container, new QuizSelectShapeFragment(), QuizSelectShapeFragment.class.getSimpleName());
                        }
                    }
                }
            }
        });
        bSkipQuiz = (TextView)rootView. findViewById(R.id.bSkipQuiz);
        bSkipQuiz.setText(Html.fromHtml(getString(R.string.button_skip)));
        if (Constants.EDITPROFILE ||Constants.TAKE_QUIZ){
            bSkipQuiz.setVisibility(View.GONE);
        }
        bSkipQuiz.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Constants.SKIPQUIZ = true;
                addFragment(R.id.frame_container, new QuizEnterUserDetails(), QuizEnterUserDetails.class.getSimpleName());
            }
        });

        return rootView;
    }


    void init() {
        quiz = Registration.getInstance().getQuiz();
        //For Edit profile
        try {
            userAttr = getArguments().getParcelable(StyleProfile.class.getSimpleName());
            attributes = (Constants.Attributes) getArguments().getSerializable(Constants.Attributes.class.getSimpleName());
        } catch (NullPointerException np) {
            Constants.warning("Style Profile null");
            quiz.getUser_attr().setShoes_Size("");
            quiz.getUser_attr().setBottoms_Size("");
            quiz.getUser_attr().setSize("");
        }

        expandableListView = (ExpandableListView) rootView.findViewById(R.id.elvSelectMeasure);
        prepareListData();
        listAdapter = new ExpandableListAdapter(mActivity, listDataHeader, listDataChild, this);
        listAdapter.setUserAttr(userAttr);
        expandableListView.setAdapter(listAdapter);

        bNext = (Button) rootView.findViewById(R.id.bNext);

        if (userAttr != null) {
            if (attributes == Constants.Attributes.TOP_SIZE) {
                expandableListView.expandGroup(0);
            } else if (attributes == Constants.Attributes.BOTTOM_SIZE) {
                expandableListView.expandGroup(1);
            } else if (attributes == Constants.Attributes.SHOE_SIZES) {
                expandableListView.expandGroup(2);
            } else if (attributes == Constants.Attributes.HEIGHT) {
                expandableListView.expandGroup(3);
            }
            bNext.setEnabled(true);
            bNext.setText(getString(R.string.button_confirm));
        } else
            expandableListView.expandGroup(0);

    }

    private void prepareListData() {
        listDataHeader = new ArrayList<>();
        listDataChild = new HashMap<>();

        // Adding parent data
        String headers[] = getResources().getStringArray(R.array.measurementHeaders);
        Collections.addAll(listDataHeader, headers);

        List<Size> dummy = new ArrayList<>();
        dummy.add(null);

        listDataChild.put(listDataHeader.get(0), getDressSizes()); // Header, Child data
        listDataChild.put(listDataHeader.get(1), getWaistSizes());
        listDataChild.put(listDataHeader.get(2), getShoeSizes());
        listDataChild.put(listDataHeader.get(3), dummy); // for height
    }

    private List<Size> getDressSizes() {
        List<Size> sizes = new ArrayList<>();
        String dressSizes[] = getResources().getStringArray(R.array.dress_sizes);
        for (int i = 0; i < dressSizes.length; i++) {
            Size size = new Size();
            size.setType(Constants.SizeTypes.DRESS);
            size.setSize(dressSizes[i]);
            size.setSizeId(i + 1);
            sizes.add(size);
        }
        return sizes;
    }


    private List<Size> getWaistSizes() {
        List<Size> sizes = new ArrayList<>();
        int waistSizes[] = getResources().getIntArray(R.array.pant_sizes);
        for (int i = 0; i < waistSizes.length; i++) {
            Size size = new Size();
            size.setType(Constants.SizeTypes.PANT);
            size.setSize(waistSizes[i] + "");
            size.setSizeId(i + 1);
            sizes.add(size);
        }
        return sizes;
    }


    private List<Size> getShoeSizes() {
        List<Size> sizes = new ArrayList<>();
        int shoeSizes[] = getResources().getIntArray(R.array.shoe_sizes);
        for (int i = 0; i < shoeSizes.length; i++) {
            Size size = new Size();
            size.setType(Constants.SizeTypes.SHOE);
            size.setSize(shoeSizes[i] + "");
            size.setSizeId(i + 1);
            sizes.add(size);
        }
        return sizes;
    }


    @Override
    public void onResume() {
        super.onResume();
        TextView textView = ((TextView) getToolBar().findViewById(R.id.tvStep));
        if (textView != null)
            textView.setText("2 of 5");
    }

    @Override
    public void onSizeSelected(Size size) {
        int position = 0;
        if (size.getType() == Constants.SizeTypes.DRESS) {
            position = 0;
            if (userAttr != null)
                userAttr.setSize(size.getSize());
            else
                quiz.getUser_attr().setSize(size.getSize());
        } else if (size.getType() == Constants.SizeTypes.PANT) {
            position = 1;
            if (userAttr != null)
                userAttr.setBottoms_Size(size.getSize());
            else
                quiz.getUser_attr().setBottoms_Size(size.getSize());
        } else if (size.getType() == Constants.SizeTypes.SHOE) {
            position = 2;
            if (userAttr != null)
                userAttr.setShoes_Size(size.getSize());
            else
                quiz.getUser_attr().setShoes_Size(size.getSize());
        }

        View view = listAdapter.getExpandedHeaderView();
        if (view != null) {
            view.findViewById(R.id.tvMeasure).setVisibility(View.VISIBLE);
            ((TextView) view.findViewById(R.id.tvMeasure)).setText(size.getSize());
        }

        expandableListView.collapseGroup(position);
        expandableListView.expandGroup(position + 1);
      /*  if (validate())
            bNext.setEnabled(true);*/
    }

    private boolean validate() {
       /* return !(TextUtils.isEmpty(quiz.getUser_attr().getSize()) || TextUtils.isEmpty(quiz.getUser_attr().getBottoms_Size()) ||
                TextUtils.isEmpty(quiz.getUser_attr().getShoes_Size()));*/

        if (TextUtils.isEmpty(quiz.getUser_attr().getSize())) {
            showToast(getString(R.string.select_topsize));
            return false;
        } else if (TextUtils.isEmpty(quiz.getUser_attr().getBottoms_Size()) || quiz.getUser_attr().getBottoms_Size().equalsIgnoreCase(" Inch")) {
            showToast(getString(R.string.select_bottomsize));
            return false;
        } else if (TextUtils.isEmpty(quiz.getUser_attr().getShoes_Size())) {
            showToast(getString(R.string.select_shoesize));
            return false;
        }
        return true;
    }


    @Override
    public <T> void onResponse(T obj, int tag) {
        switch (tag) {
            case Constants.TAG_IMG_BACKGROUND:
                rootView.findViewById(R.id.pbWheel).setVisibility(View.GONE);

                BackgroundImgResponse imgResponse = (BackgroundImgResponse) obj;
                if (imgResponse.getStatus().equalsIgnoreCase(Constants.RESPONSE_SUCCESS)) {
                    String imgUrl = imgResponse.getImage();
                    ImageView iv_background = (ImageView) rootView.findViewById(R.id.iv_background);
                    Picasso.with(mActivity.getApplicationContext()).load(imgUrl).into(iv_background);
                    toggleVisibility();
                }

                break;
        }
    }

    private void toggleVisibility() {
        rootView.findViewById(R.id.llDetails).setVisibility(View.VISIBLE);
    }
}
