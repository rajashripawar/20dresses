package outletwise.com.twentydresses.view.activity;

import android.Manifest;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.BitmapFactory;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.support.v7.app.ActionBar;
import android.text.Html;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.ImageView;
import android.widget.TableLayout;
import android.widget.TableRow;
import android.widget.TextView;

import com.android.camera.CropImageIntentBuilder;
import com.mikepenz.actionitembadge.library.ActionItemBadge;
import com.squareup.picasso.Picasso;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.File;
import java.io.IOException;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;

import it.sauronsoftware.ftp4j.FTPClient;
import it.sauronsoftware.ftp4j.FTPDataTransferListener;
import outletwise.com.twentydresses.R;
import outletwise.com.twentydresses.controller.AnalyticsTracker;
import outletwise.com.twentydresses.controller.CartOperations;
import outletwise.com.twentydresses.controller.ProfileTypeAnimator;
import outletwise.com.twentydresses.model.ProfilePictureResponse;
import outletwise.com.twentydresses.model.Quiz;
import outletwise.com.twentydresses.model.StyleAttrEntity;
import outletwise.com.twentydresses.model.StyleProfile;
import outletwise.com.twentydresses.model.UpdateStyleProfile;
import outletwise.com.twentydresses.model.UserAttrEntity;
import outletwise.com.twentydresses.model.UserAttributesEntity;
import outletwise.com.twentydresses.model.VisitorResponse;
import outletwise.com.twentydresses.model.database.greenbot.User;
import outletwise.com.twentydresses.utilities.Constants;
import outletwise.com.twentydresses.utilities.Urls;
import outletwise.com.twentydresses.view.custom.ExpandableTextView;

/**
 * Created by User-PC on 03-08-2015.
 * This class is use to show user style profile based on quiz
 * User can edit style profile by retake quiz and updating attributes.
 */

public class ProfileActivity extends BaseActivity {
    private static final int REQUEST_EDIT_PROF = 1;
    private static final int REQUEST_TAKE_QUIZ = 2;
    private static final int RESULT_LOAD_IMAGE = 3;
    private static final int REQUEST_CROP_PICTURE = 4;

    private List<View> views;
    private ImageView ivProfile;
    private MenuItem menuCart;
    private StyleProfile styleProfile;
    //String[] array = new String[2];
    private UserAttrEntity userAttr;
    private StyleAttrEntity styleArr;
    private String strImgName;
    private File croppedImageFile;
    private User user;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_profile);
        trackPage(getString(R.string.pgProfile));

        setUpToolBar(getString(R.string.toolbar_profile));
        final ActionBar ab = getSupportActionBar();
        ab.setDisplayHomeAsUpEnabled(true);

        findViewById(R.id.bGetRecommend).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                showToast(getString(R.string.toast_profile));
                Constants.CHECK_RECOMMENTATION = true;
                startActivity(new Intent(getApplicationContext(), HomeActivity.class));
            }
        });

        findViewById(R.id.tvGotoQuiz).setTag(Constants.Attributes.STYLE);
        findViewById(R.id.tvGotoQuiz).setOnClickListener(this);
    }

   /* private void setProfile(User user) {
        ivProfile = (ImageView) findViewById(R.id.ivProfile);

        if (user != null)
            ((TextView) findViewById(R.id.tvName)).setText(user.getUser_first_name() + " " + user.getUser_last_name());
      *//*  if (user.getUser_picture() != null && !user.getUser_picture().equalsIgnoreCase(""))
            Picasso.with(this).load(user.getUser_picture()).into(ivProfile);*//*
    }*/

    void updateUI() {
        ivProfile = (ImageView) findViewById(R.id.ivProfile);
        ivProfile.setOnClickListener(this);
        if (styleProfile.getImage() != null && !styleProfile.getImage().equalsIgnoreCase(""))
            Picasso.with(this)
                    .load(styleProfile.getImage())
                    .error(R.drawable.ic_profile)
                    .into(ivProfile);

        ((TextView) findViewById(R.id.tvName)).setText(styleProfile.getName());
        //Animation
        ProfileTypeAnimator typeAnimator = new ProfileTypeAnimator(addViews(styleProfile.getStyle_attr()));
        typeAnimator.animateViews();
        ivProfile.bringToFront();
        ((TextView) findViewById(R.id.tvStyle)).setText(styleProfile.getUser_style());

        setStyleMessage();

        TableLayout tableLayout = (TableLayout) findViewById(R.id.tlAttributes);
        int k = 0;
        for (int i = 0; i < tableLayout.getChildCount(); i++) {
            View view = tableLayout.getChildAt(i);
            if (view instanceof TableRow) {
                TableRow row = (TableRow) view;
                for (int j = 0; j < row.getChildCount(); j++) {
                    View profileTypeView = row.getChildAt(j);
                    switch (k) {
                        case 0:
                            ((ImageView) profileTypeView.findViewById(R.id.ivProfileType)).setImageResource(R.drawable.ic_bodytype);
                            ((TextView) profileTypeView.findViewById(R.id.tvProfileType)).setText("Body Type");

                            ((TextView) profileTypeView.findViewById(R.id.tvProfileTypeValue)).
                                    setText(styleProfile.getUser_attr().getShape());
                            profileTypeView.setTag(Constants.Attributes.BODY_SHAPE);
                            profileTypeView.setOnClickListener(this);
                            break;
                        case 1:
                            ((ImageView) profileTypeView.findViewById(R.id.ivProfileType)).setImageResource(R.drawable.ic_complexion);
                            ((TextView) profileTypeView.findViewById(R.id.tvProfileType)).setText("Complexion");

                            if (styleProfile.getUser_attr().getFace_Color() != null && !styleProfile.getUser_attr().getFace_Color().equals(""))
                                ((TextView) profileTypeView.findViewById(R.id.tvProfileTypeValue)).
                                        setText(styleProfile.getUser_attr().getFace_Color().
                                                substring(styleProfile.getUser_attr().getFace_Color().indexOf(" ")));
                            profileTypeView.setTag(Constants.Attributes.COMPLEXION);
                            profileTypeView.setOnClickListener(this);
                            break;
                        case 2:
                            ((ImageView) profileTypeView.findViewById(R.id.ivProfileType)).setImageResource(R.drawable.ic_height);
                            ((TextView) profileTypeView.findViewById(R.id.tvProfileType)).setText("Height");
                            if (styleProfile.getUser_attr().getHeight() != null && !styleProfile.getUser_attr().getHeight().equals(""))
                                ((TextView) profileTypeView.findViewById(R.id.tvProfileTypeValue)).
                                        setText(styleProfile.getUser_attr().getHeight());
                            profileTypeView.setTag(Constants.Attributes.HEIGHT);
                            profileTypeView.setOnClickListener(this);
                            break;
                        case 3:
                            ((ImageView) profileTypeView.findViewById(R.id.ivProfileType)).setImageResource(R.drawable.ic_top);
                            ((TextView) profileTypeView.findViewById(R.id.tvProfileType)).setText("Top Size");

                            if (styleProfile.getUser_attr().getSize() != null && !styleProfile.getUser_attr().getSize().equals(""))
                                ((TextView) profileTypeView.findViewById(R.id.tvProfileTypeValue)).
                                        setText(styleProfile.getUser_attr().getSize());
                            profileTypeView.setTag(Constants.Attributes.TOP_SIZE);
                            profileTypeView.setOnClickListener(this);
                            break;
                        case 4:
                            ((ImageView) profileTypeView.findViewById(R.id.ivProfileType)).setImageResource(R.drawable.ic_pant);
                            ((TextView) profileTypeView.findViewById(R.id.tvProfileType)).setText("Pant Size");

                            if (styleProfile.getUser_attr().getBottoms_Size() != null && !styleProfile.getUser_attr().getBottoms_Size().equals(""))
                                ((TextView) profileTypeView.findViewById(R.id.tvProfileTypeValue)).
                                        setText(styleProfile.getUser_attr().getBottoms_Size().
                                                substring(0, styleProfile.getUser_attr().getBottoms_Size().indexOf(" ")));
                            profileTypeView.setTag(Constants.Attributes.BOTTOM_SIZE);
                            profileTypeView.setOnClickListener(this);
                            break;
                        case 5:
                            ((ImageView) profileTypeView.findViewById(R.id.ivProfileType)).setImageResource(R.drawable.ic_shoe);
                            ((TextView) profileTypeView.findViewById(R.id.tvProfileType)).setText("Shoe Size");

                            if (styleProfile.getUser_attr().getShoes_Size() != null && !styleProfile.getUser_attr().getShoes_Size().equals(""))
                                ((TextView) profileTypeView.findViewById(R.id.tvProfileTypeValue)).
                                        setText(styleProfile.getUser_attr().getShoes_Size());
                            profileTypeView.setTag(Constants.Attributes.SHOE_SIZES);
                            profileTypeView.setOnClickListener(this);
                            break;
                       /* case 6:
                            if (styleProfile.getUser_attr().getFace_Shape() != null && !styleProfile.getUser_attr().getFace_Shape().equals("")) {

                                findViewById(R.id.tblRow).setVisibility(View.VISIBLE);

                                ((ImageView) profileTypeView.findViewById(R.id.ivProfileType)).setImageResource(R.drawable.ic_faceshape);
                                ((TextView) profileTypeView.findViewById(R.id.tvProfileType)).setText("Face Shape");
                                ((TextView) profileTypeView.findViewById(R.id.tvProfileTypeValue)).
                                        setText(styleProfile.getUser_attr().getFace_Shape());
                                profileTypeView.setTag(Constants.Attributes.FACE_SHAPE);
                                profileTypeView.setOnClickListener(this);
                            } else
                                findViewById(R.id.tblRow).setVisibility(View.GONE);
                            break;*/
                    }
                    k++;
                }
            }
        }

        if (styleProfile.getUser_attr().getFace_Shape() != null && !styleProfile.getUser_attr().getFace_Shape().equals("")) {

            findViewById(R.id.tvGotoForYouQuiz).setVisibility(View.VISIBLE);


            ((ImageView) findViewById(R.id.ivProfileTypeFace)).setImageResource(R.drawable.ic_faceshape);
            ((TextView) findViewById(R.id.tvProfileTypeFace)).setText("Face Shape");
            ((TextView) findViewById(R.id.tvProfileTypeValueFace)).
                    setText(styleProfile.getUser_attr().getFace_Shape());
            findViewById(R.id.tvGotoForYouQuiz).setTag(Constants.Attributes.FACE_SHAPE);
            findViewById(R.id.tvGotoForYouQuiz).setOnClickListener(this);
        } else
            findViewById(R.id.tvGotoForYouQuiz).setVisibility(View.GONE);

     /*   findViewById(R.id.tvGotoForYouQuiz).setTag(Constants.Attributes.FACE_SHAPE);
        findViewById(R.id.tvGotoForYouQuiz).setOnClickListener(this);*/
    }


    private void setStyleMessage() {

        String style_message = styleProfile.getUser_style_message();
       /* int msgLength = style_message.length();
         String msg1, msg2;*/

        ExpandableTextView expandableTextView = (ExpandableTextView) findViewById(R.id.lorem_ipsum);
        expandableTextView.setText(Html.fromHtml(style_message));
    }

    @Override
    public void onClick(View v) {
        super.onClick(v);

        if (v.getId() == R.id.ivProfile) {
            setProfilePic();
        } else {
            Intent intent = new Intent(this, EditProfileActivity.class);
            intent.putExtra(Constants.Attributes.class.getSimpleName(), (Serializable) v.getTag());
            intent.putExtra(UserAttrEntity.class.getSimpleName(), styleProfile.getUser_attr());
            UserAttributesEntity.FaceShapeBean test = styleProfile.getUser_attributes().getFaceShape();
            intent.putExtra("que_id", test.getQue_id());
            startActivityForResult(intent, REQUEST_EDIT_PROF);
        }
    }

    private void setProfilePic() {

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            int hasReadStoragePermission = checkSelfPermission(Manifest.permission.READ_EXTERNAL_STORAGE);
            if (hasReadStoragePermission != PackageManager.PERMISSION_GRANTED) {
                requestPermissions(new String[]{Manifest.permission.READ_EXTERNAL_STORAGE}, 1);
                return;
            } else
                cropImage();
        } else
            cropImage();
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, String[] permissions, int[] grantResults) {

        if (grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
            // Permission Granted
            cropImage();
        } else
            showToast(getString(R.string.text_permission_denied));
    }

    private void cropImage() {
        strImgName = "test_" + Calendar.getInstance().getTimeInMillis() + ".jpg";
        croppedImageFile = new File(getCacheDir(), strImgName);
        try {
            croppedImageFile.createNewFile();
        } catch (IOException e) {
            e.printStackTrace();
        }
        Intent i = new Intent(
                Intent.ACTION_PICK, android.provider.MediaStore.Images.Media.EXTERNAL_CONTENT_URI);
        startActivityForResult(i, RESULT_LOAD_IMAGE);
    }
    List<View> addViews(StyleProfile.StyleAttr styleAttr) {
        views = new ArrayList<>();
        View view1 = findViewById(R.id.ivFirst);
        view1.setTag(styleAttr.getBohemian());
        views.add(view1);
        View view2 = findViewById(R.id.ivSecond);
        view2.setTag(styleAttr.getCasual());
        views.add(view2);
        View view3 = findViewById(R.id.ivThird);
        view3.setTag(styleAttr.getClassic());
        views.add(view3);
        View view4 = findViewById(R.id.ivFourth);
        view4.setTag(styleAttr.getEdgy());
        views.add(view4);
        View view5 = findViewById(R.id.ivFifth);
        view5.setTag(styleAttr.getFashionForward());
        views.add(view5);
        View view6 = findViewById(R.id.ivSixth);
        view6.setTag(styleAttr.getFeminine());
        views.add(view6);
        return views;
    }

    @Override
    protected void onResume() {
        super.onResume();
        ActionItemBadge.update(this, menuCart,
                getResources().getDrawable(R.drawable.ic_cart), ActionItemBadge.BadgeStyles.RED,
                CartOperations.getInstance().getCartSize());

        getProfileData();
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_home, menu);
        menu.findItem(R.id.action_profile).setIcon(R.drawable.ic_profile_selected);
        menuCart = menu.findItem(R.id.action_cart);

        if (user != null && user.getCart_count() != null) {
            ActionItemBadge.update(this, menuCart,
                    getResources().getDrawable(R.drawable.ic_cart), ActionItemBadge.BadgeStyles.RED,
                    user.getCart_count());

            if (user.getCart_count().equalsIgnoreCase("0"))
                ActionItemBadge.update(this, menuCart,
                        getResources().getDrawable(R.drawable.ic_cart), ActionItemBadge.BadgeStyles.RED, null);
        } else
            ActionItemBadge.update(this, menuCart,
                    getResources().getDrawable(R.drawable.ic_cart), ActionItemBadge.BadgeStyles.RED,
                    null);

        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.action_profile:
                return true;
            case R.id.action_home:
                finish();
                startActivity(new Intent(this, HomeActivity.class));
                return true;
            case R.id.action_cart:
                finish();
                startActivity(new Intent(this, CartActivity.class));
                return true;
            default:
                break;
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onCompleteRetrieval(int tag, ArrayList obj, String msg) {
        super.onCompleteRetrieval(tag, obj, msg);
        switch (tag) {
            case Constants.TAG_USER:
                if (obj != null && obj.size() > 0)
                    user = (User) obj.get(0);
                // setProfile(user);
                getProfileData();

                break;
        }
    }

    @Override
    public void onCompleteInsertion(int tag, String msg) {
        super.onCompleteInsertion(tag, msg);
    }

    private void getProfileData() {
        if (user != null) {
            JSONObject jsonObject = new JSONObject();
            try {
                jsonObject.put(Constants.AUTH_TOKEN, preferences.getString(Constants.KEY_TOKEN, ""));
               if (String.valueOf(user.getUser_id()) != null){
                   jsonObject.put(Constants.USER_ID, user.getUser_id());
               } else if (preferences.getString(Constants.USER_ID, "") != null){
                   jsonObject.put(Constants.USER_ID, preferences.getString(Constants.USER_ID, ""));
               }

//                jsonObject.put(Constants.USER_ID, user.getUser_id());
            } catch (JSONException e) {
                e.printStackTrace();
            }
            postJsonData(Urls.STYLE_PROFILE, jsonObject, Constants.TAG_STYLE_PROFILE, false);

        }
    }


    private void takeQuiz() {
        findViewById(R.id.svLayout).setVisibility(View.GONE);
        findViewById(R.id.txtView).setVisibility(View.VISIBLE);
        findViewById(R.id.tvTakeQuiz).setVisibility(View.VISIBLE);

        findViewById(R.id.tvTakeQuiz).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Constants.TAKE_QUIZ = true;
                Constants.EDITPROFILE = true;
                startActivityForResult(new Intent(getApplicationContext(), QuizActivity.class), REQUEST_TAKE_QUIZ);
            }
        });
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (resultCode == RESULT_OK) {
            switch (requestCode) {
                case REQUEST_TAKE_QUIZ:
                    Constants.TAKE_QUIZ = false;
                    userAttr = Quiz.getInstance().getUser_attr();
                    styleArr = Quiz.getInstance().getStyle_attr();
                    UpdateStyleProfile styleProfile1 = new UpdateStyleProfile();
                    styleProfile1.setUser_attr(userAttr);
                    styleProfile1.setStyle_attr(styleArr);
                    styleProfile1.setUserId(user.getUser_id() + "");
                    styleProfile1.setAuth_token(preferences.getString(Constants.KEY_TOKEN, ""));
                    postJsonData(Urls.UPDATE_STYLE_PROFILE, styleProfile1, Constants.TAG_UPDATE_STYLE_PROFILE, true);
                    break;

                case REQUEST_EDIT_PROF:
                    if (data != null) {
                        styleProfile.setUser_attr(data.<UserAttrEntity>getParcelableExtra(UserAttrEntity.class.getSimpleName()));
                        updateUI();
                    }
                    break;
                case RESULT_LOAD_IMAGE:
                    if (data != null) {
                        Uri croppedImage = Uri.fromFile(croppedImageFile);
                        CropImageIntentBuilder cropImage = new CropImageIntentBuilder(ivProfile.getWidth(), ivProfile.getWidth(), croppedImage);
                        cropImage.setCircleCrop(true);
                        cropImage.setOutlineCircleColor(getResources().getColor(R.color.color_accent));
                        cropImage.setSourceImage(data.getData());

                        startActivityForResult(cropImage.getIntent(this), REQUEST_CROP_PICTURE);
                    }
                    break;

                case REQUEST_CROP_PICTURE:

                    BitmapFactory.Options options = new BitmapFactory.Options();
                    options.inJustDecodeBounds = true;
                    BitmapFactory.decodeFile(croppedImageFile.getPath(), options);
                    int width = options.outWidth;
                    int height = options.outHeight;
                    this.uploadFileToFtp(croppedImageFile, width, height);

                    //updateProfilePic();

                    user.setUser_picture(Uri.fromFile(croppedImageFile).toString());

                    // getDrawerAdapter().setUser(user);   //Update Drawer Profile icon
                    Picasso.with(this).load(Uri.fromFile(croppedImageFile).toString())
                            .resize(ivProfile.getWidth(), ivProfile.getHeight())
                            .into(ivProfile);
                    insertRecord(user, Constants.TAG_USER);
                    break;
            }
        }

    }

    @Override
    public <T> void onResponse(T obj, int tag) {
        super.onResponse(obj, tag);
        switch (tag) {
            case Constants.TAG_STYLE_PROFILE:
                findViewById(R.id.pbWheelProfile).setVisibility(View.GONE);
                findViewById(R.id.svLayout).setVisibility(View.VISIBLE);
                styleProfile = (StyleProfile) obj;
                AnalyticsTracker.getInstance().setRegProperties(styleProfile, user, getApplicationContext());

                if (styleProfile.getUser_attr().getSize().equalsIgnoreCase(""))
                    takeQuiz();

                updateUI();
                break;
            case Constants.TAG_UPDATE_STYLE_PROFILE:
                VisitorResponse response = (VisitorResponse) obj;
                if (response.getStatus().equalsIgnoreCase(Constants.RESPONSE_SUCCESS)) {
                    findViewById(R.id.svLayout).setVisibility(View.VISIBLE);
                    findViewById(R.id.txtView).setVisibility(View.GONE);
                    findViewById(R.id.tvTakeQuiz).setVisibility(View.GONE);

                    getProfileData();
                }
                showToast(response.getMessage());
                break;

            case Constants.TAG_UPDATE_PROFILE_PIC:
                ProfilePictureResponse profilePictureResponse = (ProfilePictureResponse) obj;
                if (profilePictureResponse.getStatus().equalsIgnoreCase(Constants.RESPONSE_SUCCESS)) {
                    showToast("Image uploaded successfully..!!");
                    Picasso.with(this).load(profilePictureResponse.getUser_picture()).into(ivProfile);
                }
                break;
        }
    }


    //Used to upload user profile image
    public void uploadFileToFtp(final File file, final int width, final int height) {
        runOnUiThread(new Runnable() {
            @Override
            public void run() {
                FTPClient client = new FTPClient();
                try {
                    File f = Constants.getCompressedImage(getApplicationContext(), file, width, height);
                    client.connect("ftp.20dresses.com");
                    client.login("appdev@20dresses.com", "App_Dev20D");
                    client.setType(FTPClient.TYPE_BINARY);
                    client.changeDirectory("/images/");
                    // client.upload(f, new MyTransferListener());
                    client.upload(f, new FTPDataTransferListener() {
                        @Override
                        public void started() {
                            Constants.debug("Image upload started...");
                        }

                        @Override
                        public void transferred(int i) {

                        }

                        @Override
                        public void completed() {
                            Constants.debug("Image upload completed...");
                            //After completion of uploading image to ftp call update prof pic webservice
                            updateProfilePic();
                        }

                        @Override
                        public void aborted() {

                        }

                        @Override
                        public void failed() {

                        }
                    });
                } catch (Exception e) {
                    e.printStackTrace();
                    try {
                        client.disconnect(true);
                    } catch (Exception e2) {
                        e2.printStackTrace();
                    }
                }
            }
        });
    }

    public void updateProfilePic() {

        JSONObject jsonObject = new JSONObject();
        try {
            jsonObject.put(Constants.USER_ID, user.getUser_id());
            jsonObject.put(Constants.AUTH_TOKEN, preferences.getString(Constants.KEY_TOKEN, ""));
            jsonObject.put("user_picture", strImgName);
        } catch (JSONException e) {
            e.printStackTrace();
        }
        postJsonData(Urls.UPDATE_PROFILE_PIC, jsonObject, Constants.TAG_UPDATE_PROFILE_PIC, true);
    }


    @Override
    public void onError(int error_code, int tag, Object object) {
        checkErrorCode(error_code, object);
    }
}
