package outletwise.com.twentydresses.view.decoration;

import android.content.Context;
import android.view.View;
import android.widget.TextView;

import outletwise.com.twentydresses.R;

/**
 * Created by User-PC on 27-08-2015.
 */
public class SelectStylePageTransformer extends ZoomOutPageTransformer {
    protected static float MIN_TRANSLATE = 0.2f;
    private Context mContext;

    {
        MIN_SCALE = 0.5f;
    }

    public SelectStylePageTransformer(Context mContext) {
        this.mContext = mContext;
    }

    @Override
    public void transformPage(View view, float position) {
//        Constants.debug("transform page called: " + position);
        int pageWidth = view.getWidth();
        int pageHeight = view.getHeight();

        if (position < -1) { // [-Infinity,-1)
            // This page is way off-screen to the left.
            view.setAlpha(0);

        } else if (position <= 0 && position >= -1) { // 0 to -1 is for current page
            // Modify the default slide transition to shrink the page as well
            if (position != 0) {
                view.findViewById(R.id.imageView).setVisibility(View.GONE);
                view.setBackgroundColor(mContext.getResources().getColor(android.R.color.transparent));
            } else {
                view.findViewById(R.id.imageView).setVisibility(View.VISIBLE);
                view.setBackgroundColor(mContext.getResources().getColor(R.color.md_grey_300));
            }
            ((TextView) view.findViewById(R.id.tvSetStyle)).setText("Nope");

            float scaleFactor = Math.max(MIN_SCALE, 1 - Math.abs(position));
            float translateFactor = Math.max(MIN_TRANSLATE, 1 - Math.abs(position));
            float vertMargin = pageHeight * (1 - translateFactor) / 2;
            float horzMargin = pageWidth * (1 - translateFactor);
            if (position < 0) {
                view.setTranslationX(horzMargin - vertMargin / 2);
            }

//            // Scale the page down (between MIN_SCALE and 1)
//            view.setScaleX(scaleFactor);
//            view.setScaleY(scaleFactor);

            // Fade the page relative to its size.
            view.setAlpha(MIN_ALPHA +
                    (scaleFactor - MIN_SCALE) /
                            (1 - MIN_SCALE) * (1 - MIN_ALPHA));
        }
    }
}
