package outletwise.com.twentydresses.view.activity;

import android.app.SearchManager;
import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.os.Build;
import android.os.Bundle;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.app.ActionBar;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.SearchView;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.TextView;

import com.mikepenz.actionitembadge.library.ActionItemBadge;
import com.sothree.slidinguppanel.SlidingUpPanelLayout;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;

import outletwise.com.twentydresses.R;
import outletwise.com.twentydresses.controller.AnalyticsTracker;
import outletwise.com.twentydresses.controller.CartOperations;
import outletwise.com.twentydresses.controller.ProductOptions;
import outletwise.com.twentydresses.controller.adapter.BaseRecyclerAdapter;
import outletwise.com.twentydresses.controller.adapter.ProductsAdapter;
import outletwise.com.twentydresses.model.Category;
import outletwise.com.twentydresses.model.JustSoldResponse;
import outletwise.com.twentydresses.model.Product;
import outletwise.com.twentydresses.model.SubTip;
import outletwise.com.twentydresses.model.TrendProducts;
import outletwise.com.twentydresses.model.database.greenbot.Cart;
import outletwise.com.twentydresses.model.database.greenbot.Shortlist;
import outletwise.com.twentydresses.utilities.Constants;
import outletwise.com.twentydresses.utilities.Urls;
import outletwise.com.twentydresses.view.custom.CustomRecyclerView;
import outletwise.com.twentydresses.view.custom.FavoritesButton;
import outletwise.com.twentydresses.view.fragment.ProductsSortFragment;

/**
 * Created by User-PC on 06-08-2015.
 * This class is use to show product listing
 */
public class ProductsActivity extends BaseActivity implements ProductsAdapter.ShortListed, SwipeRefreshLayout.OnRefreshListener {
    private CustomRecyclerView rv;
    private ArrayList<Product> products = new ArrayList<>();
    private boolean isLoading;
    private int page = 1;
    private ProductsAdapter productAdapter;
    private RecyclerScrollListener recyclerScrollListener;
    private Category.SubCat subCat;
    boolean isForYou;
    boolean isTrends;
    boolean isJustSold;
    private String sortBy;
    private MenuItem menuCart;
    private ProductOptions options;
    private SwipeRefreshLayout mSwipeRefreshLayout;
    public ProductOptions.Option productOption;
    private boolean blnProductState = true;
    private String strProductCount;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_products);
        mSwipeRefreshLayout = (SwipeRefreshLayout) findViewById(R.id.swipeRefreshLayout);
        mSwipeRefreshLayout.setOnRefreshListener(this);
        try {
            subCat = getIntent().getParcelableExtra(Constants.CATEGORY);
            trackPage(subCat.getName());
            AnalyticsTracker.getInstance().trackCategory(subCat.getName());
            if (subCat.getName().equalsIgnoreCase("For You")) {
                isForYou = true;
                findViewById(R.id.llFilterSort).setVisibility(View.GONE);
            }
            else if (subCat.getName().equalsIgnoreCase("TRENDS")){
                isTrends = true;
                findViewById(R.id.llFilterSort).setVisibility(View.GONE);
            }
            else if (subCat.getName().equalsIgnoreCase("JUST SOLD")) {
                isJustSold = true;
                findViewById(R.id.llFilterSort).setVisibility(View.GONE);
            }
            else
                findViewById(R.id.llFilterSort).setVisibility(View.VISIBLE);

            if (subCat.getName().equalsIgnoreCase("Offers_Sale") || subCat.getName().equalsIgnoreCase("Offers_Category"))
                setUpToolBar("Home".toUpperCase());
            else
                setUpToolBar(subCat.getName().toUpperCase());
            final ActionBar ab = getSupportActionBar();
            ab.setDisplayHomeAsUpEnabled(true);

        } catch (NullPointerException ex) {
            ex.printStackTrace();
           /* setUpToolBar("SEARCH RESULT");
            findViewById(R.id.llFilterSort).setVisibility(View.GONE);*/
        }

        findViewById(R.id.pbWheel).setVisibility(View.VISIBLE);
        // setUpSlidingPanel();
        setUpRecyclerView();
        setUpProductOptions();
    }

    private void setUpProductOptions() {

        if (isForYou || isTrends)
            options = new ProductOptions(this, "ForYou");
        else if (isJustSold)
            options = new ProductOptions(this, "JustSold");
        else
            options = new ProductOptions(this);

        options.setOptionClickListener(new ProductOptions.OptionClick() {

            @Override
            public void onOptionClick(ProductOptions.Option option) {
                if (option.getTag() == ProductOptions.TOGGLE) {
                    if (option.isToggleState()) {
                        mSwipeRefreshLayout.setEnabled(false);
                        resetRecyclerView(ProductsAdapter.PRODUCT_HORIZONTAL);
                        slidePanelTo(SlidingUpPanelLayout.PanelState.ANCHORED);
                    } else {
                        mSwipeRefreshLayout.setEnabled(true);
                        resetRecyclerView(ProductsAdapter.PRODUCT_VERTICAL);
                        slidePanelTo(SlidingUpPanelLayout.PanelState.COLLAPSED);
                    }
                } else if (option.getTag() == ProductOptions.FILTER) {
                    Intent intent = new Intent(getApplicationContext(), FilterActivity.class);
                    intent.putExtra(Constants.CATEGORY, subCat);
                    //  startActivityForResult(intent, 1);
                    startActivity(intent);
                    //   finish();
                }
                productOption = option;
            }


        });

        options.setSortCallback(new ProductsSortFragment.Callback() {
            @Override
            public void onCallback(String sort) {
                sortBy = sort;
                productAdapter.showProgressBar();
                productAdapter.getItems().clear();
//                productAdapter.notifyDataSetChanged();
                page = 1;
                getProducts(page);

                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
                    ((TextView) findViewById(R.id.tvSort)).setCompoundDrawablesWithIntrinsicBounds(getDrawable(R.drawable.selected_sort), null, null, null);
                } else
                    ((TextView) findViewById(R.id.tvSort)).setCompoundDrawablesWithIntrinsicBounds(getResources().getDrawable(R.drawable.selected_sort), null, null, null);
                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                    ((TextView) findViewById(R.id.tvSort)).setTextColor(getColor(R.color.color_accent));
                } else
                    ((TextView) findViewById(R.id.tvSort)).setTextColor(getResources().getColor(R.color.color_accent));
            }
        });
    }

    private void getProducts(int page) {

       /* if (!Intent.ACTION_SEARCH.equals(getIntent().getAction()))
        {*/

        HashMap<String, String> params = new HashMap<>();
        if (sortBy != null) {
            params.put("sort", sortBy);
            // page = 1;
        }

        if (page > 1)
            params.put("page", page + "");

        if (isForYou) {
            params.put("tip", subCat.getId());
            postStringData(Urls.FOR_YOU_PRODUCTS, params, Constants.TAG_FOR_YOU_PRODUCTS, false);
        } else if (isTrends) {
            params.put("trend", subCat.getId());
            postStringData(Urls.TREND_PRODUCTS, params, Constants.TAG_TREND_PRODUCTS, false);
        } else if (isJustSold) {
            //  HashMap<String, String> params = new HashMap<>();
            params.put("app_key", Constants.APP_KEY);
            postStringData(Urls.JUST_SOLD_PRODUCTS, params, Constants.TAG_JUST_SOLD, false);
        } else {
            if (subCat.getName().equalsIgnoreCase(Constants.SALE))
                params.put("sale", 10 + "");
            else if (subCat.getName().equalsIgnoreCase("Offers_Sale"))
                params.put("sale", subCat.getId());
            else if (subCat.getName().equalsIgnoreCase("Offers_Category"))
                params.put("category", subCat.getId());
            else
                params.put("category", subCat.getId());
            postStringData(Urls.PRODUCTS, params, Constants.TAG_PRODUCTS, false);

        }
      /*  }
        else {
            String query = getIntent().getStringExtra(SearchManager.QUERY);
            Constants.debug("Search ::" + query);

            HashMap<String, String> params = new HashMap<>();
            params.put("keyword", query);
            postStringData(Urls.SEARCH, params, Constants.TAG_PRODUCTS, false);

        }*/
        isLoading = true;
    }

    /*From here you can see the datail of project*/
    void setUpRecyclerView() {
        rv = (CustomRecyclerView) findViewById(R.id.rvProducts);

        if (isTrends || isForYou)
            productAdapter = new ProductsAdapter(this, products, ProductsAdapter.PRODUCT_VERTICAL, this, true);
        else
            productAdapter = new ProductsAdapter(this, products, ProductsAdapter.PRODUCT_VERTICAL, this, false);

        productAdapter.setOnItemClickListener(new BaseRecyclerAdapter.OnItemClickListener() {
            @Override
            public void onItemClick(View view, int position) {
                if (view.getTag() != null) {
                    Product product = (Product) view.getTag();
                    Intent intent = new Intent(getApplicationContext(), ProductViewActivity.class);
                    intent.putExtra(Constants.PRODUCT_NAME, product);
                    intent.putExtra(Constants.CATEGORY, subCat);
                    startActivity(intent);
                }
            }
        });
        recyclerScrollListener = new RecyclerScrollListener();
        resetRecyclerView(ProductsAdapter.PRODUCT_VERTICAL);
        rv.addOnScrollListener(recyclerScrollListener);
        getProducts(page);
        selectRecords(Constants.TAG_SHORTLIST);
    }

    void resetRecyclerView(int orientation) {
        LinearLayoutManager layoutManager;
        if (orientation == ProductsAdapter.PRODUCT_VERTICAL) {
            layoutManager = new GridLayoutManager(rv.getContext(), 2);
            ((GridLayoutManager) layoutManager).setSpanSizeLookup(new GridLayoutManager.SpanSizeLookup() {
                @Override
                public int getSpanSize(int position) {
                    return productAdapter.getItemWeight(position);
                }
            });
            rv.setIsHorizontal(false);
        } else {
            layoutManager = new LinearLayoutManager(rv.getContext());
            layoutManager.setOrientation(LinearLayoutManager.HORIZONTAL);
            rv.setIsHorizontal(true);
        }
        productAdapter.setType(orientation);
        rv.setLayoutManager(layoutManager);
        rv.setAdapter(productAdapter);
        recyclerScrollListener.setLayoutManager(layoutManager);
        rv.scrollToPosition(recyclerScrollListener.getPastVisibleItem());
    }

    @Override
    protected void onResume() {
        super.onResume();
        ActionItemBadge.update(this, menuCart,
                getResources().getDrawable(R.drawable.ic_cart), ActionItemBadge.BadgeStyles.RED,
                CartOperations.getInstance().getCartSize());

        setUpSlidingPanel();
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_home, menu);
        menuCart = menu.findItem(R.id.action_cart);
        ActionItemBadge.update(this, menuCart,
                getResources().getDrawable(R.drawable.ic_cart), ActionItemBadge.BadgeStyles.RED,
                CartOperations.getInstance().getCartSize());

       /* SearchManager searchManager =
                (SearchManager) ProductsActivity.this.getSystemService(Context.SEARCH_SERVICE);
        SearchView searchView =
                (SearchView) menu.findItem(R.id.action_search).getActionView();

        searchView.setSearchableInfo(searchManager.getSearchableInfo(
                new ComponentName(getApplicationContext(), ProductsActivity.class)));*/

        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.action_profile:
                startActivity(new Intent(this, ProfileActivity.class));
                return true;

            case R.id.action_home:
                startActivity(new Intent(this, HomeActivity.class));
                return true;

            case R.id.action_cart:
                startActivity(new Intent(this, CartActivity.class));
                return true;

            default:
                break;
        }
        return super.onOptionsItemSelected(item);
    }


    @Override
    public void addProductToShortList(Product product) {

        JSONObject jsonObject = new JSONObject();
        try {
            jsonObject.put(Constants.AUTH_TOKEN, preferences.getString(Constants.KEY_TOKEN, ""));

            if (user != null && String.valueOf(user.getUser_id()) != null){
                if (!String.valueOf(user.getUser_id()).trim().isEmpty()){
                    jsonObject.put(Constants.USER_ID, user.getUser_id());
                }
            }
            else
                jsonObject.put(Constants.USER_ID, preferences.getString(Constants.USER_ID,""));

            jsonObject.put(Constants.PRODUCT, product.getProduct_id());
        } catch (JSONException e) {
            e.printStackTrace();
        }
        postJsonData(Urls.SHORTLIST_ADD, jsonObject, Constants.TAG_SHORTLIST, false);

        if (subCat != null && subCat.getName() != null)
            AnalyticsTracker.getInstance().trackShortlist(product, subCat.getName());
        else
            AnalyticsTracker.getInstance().trackShortlist(product, "");
    }

    @Override
    public void removeProductFromShortList(Product product) {

        JSONObject jsonObject = new JSONObject();
        try {
            jsonObject.put(Constants.AUTH_TOKEN, preferences.getString(Constants.KEY_TOKEN, ""));
            jsonObject.put(Constants.USER_ID, user.getUser_id());
            jsonObject.put(Constants.PRODUCT, product.getProduct_id());
        } catch (JSONException e) {
            e.printStackTrace();
        }

        postJsonData(Urls.SHORTLIST_REMOVE, jsonObject, Constants.TAG_SHORTLIST_REMOVE, false);
    }

    @Override
    public void dropComplete(View view, int tag, Object object) {
        super.dropComplete(view, tag, object);
        switch (tag) {
            case Constants.TAG_SHORTLIST:
                Product product = (Product) object;
                productAdapter.changeProductState(product.getProduct_id(), FavoritesButton.IN_PROGRESS);
                addProductToShortList(product);
                break;
            default:
        }
    }

    @Override
    public void onCompleteRetrieval(int tag, ArrayList obj, String msg) {
        super.onCompleteRetrieval(tag, obj, msg);
        switch (tag) {
            case Constants.TAG_SHORTLIST:
                final ArrayList<Shortlist> shortlists = obj;
                runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        for (Shortlist shortlist : shortlists)
                            productAdapter.changeProductState(shortlist.getProduct_id(), FavoritesButton.PROGRESS_COMPLETE);
                        findViewById(R.id.progressbar).setVisibility(View.GONE);
                        enableTouch();
                        blnProductState = false;
                    }
                });
                productAdapter.notifyDataSetChanged();
                break;
        }
    }

    @Override
    public <T> void onResponse(T obj, int tag) {
        super.onResponse(obj, tag);
        switch (tag) {
            case Constants.TAG_PRODUCTS:
                findViewById(R.id.pbWheel).setVisibility(View.GONE);
                productAdapter.hideProgressBar();
                if (page == 1 && blnProductState) {
                    findViewById(R.id.progressbar).setVisibility(View.VISIBLE);
                    disableTouch();
                }

                if (obj instanceof String && ((String) obj).equalsIgnoreCase(Constants.NO_MORE_RESULTS)) {
                    showToast(obj.toString());
                }/*else if (obj instanceof SubTip) {
                    products.addAll(((SubTip) obj).getProducts());
                    productAdapter.notifyDataSetChanged();
                    isLoading = false;
                }*/
                else if (obj instanceof Product[]) {
                    products.addAll((Arrays.asList((Product[]) obj)));
                    productAdapter.notifyDataSetChanged();
                    isLoading = false;
                }

                if (page == 1) {
                    strProductCount = products.get(0).getProduct_count();
                    ((TextView) findViewById(R.id.tvProductCount)).setVisibility(View.VISIBLE);
                    ((TextView) findViewById(R.id.tvProductCount)).setText("Total Products : " + strProductCount);
                }


              /*  ((TextView) findViewById(R.id.tvProductCount)).setVisibility(View.VISIBLE);
                ((TextView) findViewById(R.id.tvProductCount)).setText(products.size() + " out of " + strProductCount);*/


                mSwipeRefreshLayout.setRefreshing(false);
                break;
            case Constants.TAG_SHORTLIST:
                showToast(getString(R.string.toast_added_to_shortlist));
                Shortlist shortlist = (Shortlist) obj;
                productAdapter.changeProductState(shortlist.getProduct_id() + "", FavoritesButton.PROGRESS_COMPLETE);
                productAdapter.notifyDataSetChanged();
                insertRecord(obj, tag);
                if (shortlists.size() == 0)
                    slidePanelTo(SlidingUpPanelLayout.PanelState.ANCHORED);
                break;

            case Constants.TAG_SHORTLIST_REMOVE:
                showToast(getString(R.string.toast_removed_from_shortlist));
                Shortlist shortlist1 = (Shortlist) obj;
                productAdapter.changeProductState(shortlist1.getProduct_id() + "", FavoritesButton.PROGRESS_IDLE);
                productAdapter.notifyDataSetChanged();
                deleteRecord(shortlist1, Constants.TAG_SHORTLIST_REMOVE);
                break;

            case Constants.TAG_FOR_YOU_PRODUCTS:
                findViewById(R.id.pbWheel).setVisibility(View.GONE);
                productAdapter.hideProgressBar();
                if (page == 1 && blnProductState) {
                    findViewById(R.id.progressbar).setVisibility(View.VISIBLE);
                    disableTouch();
                }
                SubTip subTip = (SubTip) obj;
                if (subTip.getStatus().equalsIgnoreCase(Constants.RESPONSE_SUCCESS)) {
                    products.addAll(subTip.getProducts());
                    productAdapter.setSubTips(new ArrayList<>(subTip.getExtra_tips()));
                    productAdapter.notifyDataSetChanged();
                    isLoading = false;
                }
                mSwipeRefreshLayout.setRefreshing(false);
                break;

            case Constants.TAG_TREND_PRODUCTS:
                findViewById(R.id.pbWheel).setVisibility(View.GONE);
                productAdapter.hideProgressBar();
                if (page == 1 && blnProductState) {
                    findViewById(R.id.progressbar).setVisibility(View.VISIBLE);
                    disableTouch();
                }
                TrendProducts trendProducts = (TrendProducts) obj;
                if (trendProducts.getStatus().equalsIgnoreCase(Constants.RESPONSE_SUCCESS)) {
                    products.addAll(trendProducts.getData());
                    productAdapter.setTrendBanners(new ArrayList<>(trendProducts.getBanner()));
                    productAdapter.notifyDataSetChanged();
                    isLoading = false;
                }

                mSwipeRefreshLayout.setRefreshing(false);
                break;
            case Constants.TAG_JUST_SOLD:
                JustSoldResponse justSoldResponse = (JustSoldResponse) obj;

                findViewById(R.id.pbWheel).setVisibility(View.GONE);
                productAdapter.hideProgressBar();
                if (page == 1 && blnProductState) {
                    findViewById(R.id.progressbar).setVisibility(View.VISIBLE);
                    disableTouch();
                }

                if (justSoldResponse.getStatus().equalsIgnoreCase(Constants.RESPONSE_SUCCESS)) {
                    products.addAll(justSoldResponse.getProducts());
                    productAdapter.notifyDataSetChanged();
                    isLoading = false;
                }

                mSwipeRefreshLayout.setRefreshing(false);
                break;
        }
    }

    @Override
    public void onError(int error_code, int tag, Object object) {
        checkErrorCode(error_code, object);

        switch (tag) {
            case Constants.TAG_SHORTLIST:
                try {
                    JSONObject jsonObject = (JSONObject) object;
                    productAdapter.changeProductState(jsonObject.get("product") + "", FavoritesButton.PROGRESS_IDLE);
                    productAdapter.notifyDataSetChanged();

                } catch (JSONException e) {
                    e.printStackTrace();
                }
                break;
        }
        productAdapter.hideProgressBar();
    }

    @Override
    public void onRefresh() {
        findViewById(R.id.pbWheel).setVisibility(View.GONE);
        page = 1;
        products.clear();
        setUpRecyclerView();

        try {
            if (productOption.isToggleState()) {
                mSwipeRefreshLayout.setRefreshing(false);
                resetRecyclerView(ProductsAdapter.PRODUCT_HORIZONTAL);
                slidePanelTo(SlidingUpPanelLayout.PanelState.ANCHORED);
            } else {
                resetRecyclerView(ProductsAdapter.PRODUCT_VERTICAL);
                slidePanelTo(SlidingUpPanelLayout.PanelState.COLLAPSED);
            }
        } catch (NullPointerException ex) {
            ex.printStackTrace();
        }
    }

    class RecyclerScrollListener extends RecyclerView.OnScrollListener {
        private int pastVisibleItem, visibleItemCount, totalItemCount;
        private RecyclerView.LayoutManager layoutManager;

        public void setLayoutManager(RecyclerView.LayoutManager layoutManager) {
            this.layoutManager = layoutManager;

        }

        public int getPastVisibleItem() {
            return pastVisibleItem;
        }

        @Override
        public void onScrolled(RecyclerView recyclerView, int dx, int dy) {


           /* if (dy > 0) {
                System.out.println("Scrolled Downwards");*/
            visibleItemCount = layoutManager.getChildCount();
            totalItemCount = layoutManager.getItemCount();
            pastVisibleItem = ((LinearLayoutManager) layoutManager).findFirstVisibleItemPosition();

            if ((visibleItemCount + pastVisibleItem) >= totalItemCount) {
                if (!isLoading) {
                    productAdapter.showProgressBar();
                    page++;

                    Constants.debug("page :" + page);
                    getProducts(page);
                }
            }
         /*   } else if (dy < 0) {
                System.out.println("Scrolled Upwards");
                page--;

                Constants.debug("page :" + page);

               *//* ((TextView) findViewById(R.id.tvProductCount)).setVisibility(View.VISIBLE);
                ((TextView) findViewById(R.id.tvProductCount)).setText(variable + " out of " + strProductCount);*//*

            }*/

        }


    }
}
