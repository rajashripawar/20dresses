package outletwise.com.twentydresses.view.activity;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.ActionBar;
import android.text.TextUtils;
import android.view.View;
import android.widget.EditText;
import android.widget.TextView;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

import outletwise.com.twentydresses.R;
import outletwise.com.twentydresses.model.VerifyMobileResponse;
import outletwise.com.twentydresses.model.database.greenbot.User;
import outletwise.com.twentydresses.utilities.Constants;
import outletwise.com.twentydresses.utilities.Urls;

/**
 * Created by User-PC on 28-07-2015.
 * Class is used to verify mobile number after completing quiz or from my account
 */
public class MobileVerifyActivity extends BaseActivity {

    private String mobile;
    private EditText tvMobile;
    private TextView tvOtp, tvResendOtp;
    private User user;
    boolean blnFromMyAccount;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_mobile_verify);
        setUpToolBar(getString(R.string.toolbar_mobverify));
        final ActionBar ab = getSupportActionBar();
        ab.setDisplayHomeAsUpEnabled(true);

        init();
        initOnclickListener();

        /*findViewById(R.id.bVerify).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                verifyNumber();
            }
        });*/


     /*   tvResendOtp.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                if (tvResendOtp.getText().toString().equalsIgnoreCase(getString(R.string.text_resend_otp)))
                    getOTP(tvMobile.getText().toString());

                tvResendOtp.setText("wait..");
                tvResendOtp.postDelayed(new Runnable() {
                    public void run() {
                        tvResendOtp.setText(getString(R.string.text_resend_otp));
                    }
                }, 10000);
            }
        });*/

     /*   findViewById(R.id.btnSubmit).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String firstChar = null;
                if (!tvMobile.getText().toString().trim().equalsIgnoreCase(""))
                    firstChar = String.valueOf(tvMobile.getText().toString().charAt(0));

                if (!tvMobile.getText().toString().trim().equalsIgnoreCase("") && tvMobile.getText().length() == 10 && !firstChar.equalsIgnoreCase("0")) {
                    getOTP(tvMobile.getText().toString());
                    tvMobile.setEnabled(false);
                    tvMobile.setFocusable(false);
                    tvMobile.setFocusableInTouchMode(false);
                    findViewById(R.id.btnSubmit).setVisibility(View.GONE);
                    findViewById(R.id.btnEdit).setVisibility(View.VISIBLE);
                    user.setUser_mobile(tvMobile.getText().toString());
                    insertRecord(user, Constants.TAG_USER);
                } else
                    showToast(getString(R.string.invalid_mob));
            }
        });*/


       /* findViewById(R.id.tvSkip).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                goToNextActivity();
            }
        });*/


    /*    findViewById(R.id.btnEdit).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                tvMobile.setEnabled(true);
                tvMobile.setFocusable(true);
                tvMobile.setFocusableInTouchMode(true);
                tvMobile.requestFocus();
                tvMobile.setSelection(tvMobile.getText().length());
                findViewById(R.id.btnSubmit).setVisibility(View.VISIBLE);
                findViewById(R.id.btnEdit).setVisibility(View.GONE);
            }
        });*/

    }


    private void verifyNumber() {

        String otp = tvOtp.getText().toString().trim();
        if (!TextUtils.isEmpty(otp)) {
            JSONObject jsonObject = new JSONObject();
            try {
                jsonObject.put(Constants.USER_ID, user.getUser_id());
                jsonObject.put(Constants.AUTH_TOKEN, preferences.getString(Constants.KEY_TOKEN, ""));
                jsonObject.put(Constants.MOBILE, tvMobile.getText().toString());
                jsonObject.put(Constants.OTP, otp);

                postJsonData(Urls.REFERRAL_MOBILE_VERIFY, jsonObject, Constants.TAG_USER_CONFIRM_MOBILE, true);

            } catch (JSONException e) {
                e.printStackTrace();
            } catch (NullPointerException ex) {
                showToast(getString(R.string.text_something_wrong));
            }


        } else {
            showToast(getString(R.string.text_enter_otp));
        }
    }


    void init() {
        tvMobile = ((EditText) findViewById(R.id.tvMobile));
        tvOtp = ((TextView) findViewById(R.id.tvOTP));
        tvResendOtp = (TextView) findViewById(R.id.tvResendOTP);

        Intent intent = getIntent();
        blnFromMyAccount = intent.getBooleanExtra("VERIFY_MYACCOUNT", false);

        if (blnFromMyAccount)
            findViewById(R.id.tvSkip).setVisibility(View.GONE);
        else
            findViewById(R.id.tvSkip).setVisibility(View.VISIBLE);
    }


    private void initOnclickListener() {

        findViewById(R.id.bVerify).setOnClickListener(this);
        tvResendOtp.setOnClickListener(this);
        findViewById(R.id.btnSubmit).setOnClickListener(this);
        findViewById(R.id.tvSkip).setOnClickListener(this);
        findViewById(R.id.btnEdit).setOnClickListener(this);
    }


    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.bVerify:
                verifyNumber();
                break;
            case R.id.tvResendOTP:
                if (tvResendOtp.getText().toString().equalsIgnoreCase(getString(R.string.text_resend_otp)))
                    getOTP(tvMobile.getText().toString());

                tvResendOtp.setText("wait..");
                tvResendOtp.postDelayed(new Runnable() {
                    public void run() {
                        tvResendOtp.setText(getString(R.string.text_resend_otp));
                    }
                }, 10000);
                break;
            case R.id.btnSubmit:
                String firstChar = null;
                if (!tvMobile.getText().toString().trim().equalsIgnoreCase(""))
                    firstChar = String.valueOf(tvMobile.getText().toString().charAt(0));

                if (!tvMobile.getText().toString().trim().equalsIgnoreCase("") && tvMobile.getText().length() == 10 && !firstChar.equalsIgnoreCase("0")) {
                    getOTP(tvMobile.getText().toString());
                    tvMobile.setEnabled(false);
                    tvMobile.setFocusable(false);
                    tvMobile.setFocusableInTouchMode(false);
                    findViewById(R.id.btnSubmit).setVisibility(View.GONE);
                    findViewById(R.id.btnEdit).setVisibility(View.VISIBLE);
                    user.setUser_mobile(tvMobile.getText().toString());
                    insertRecord(user, Constants.TAG_USER);
                } else
                    showToast(getString(R.string.invalid_mob));
                break;
            case R.id.tvSkip:
                goToNextActivity();
                break;
            case R.id.btnEdit:
                tvMobile.setEnabled(true);
                tvMobile.setFocusable(true);
                tvMobile.setFocusableInTouchMode(true);
                tvMobile.requestFocus();
                tvMobile.setSelection(tvMobile.getText().length());
                findViewById(R.id.btnSubmit).setVisibility(View.VISIBLE);
                findViewById(R.id.btnEdit).setVisibility(View.GONE);
                break;
        }
    }

    @Override
    public void onCompleteRetrieval(int tag, ArrayList obj, String msg) {
        super.onCompleteRetrieval(tag, obj, msg);
        switch (tag) {
            case Constants.TAG_USER:
                if (obj.size() > 0)
                    user = (User) obj.get(0);
                mobile = user.getUser_mobile();
                tvMobile.setText(mobile);
                getOTP(mobile);
                break;
        }
    }

    private void getOTP(String mobile_number) {
        JSONObject jsonObject = new JSONObject();
        try {
            jsonObject.put(Constants.USER_ID, user.getUser_id());
            jsonObject.put(Constants.AUTH_TOKEN, preferences.getString(Constants.KEY_TOKEN, ""));
            jsonObject.put(Constants.MOBILE, mobile_number);
        } catch (JSONException e) {
            e.printStackTrace();
        }
        postJsonData(Urls.REFERRAL_MOBILE_VERIFY, jsonObject, Constants.TAG_USER_VERIFY_MOBILE, true);

    }


    @Override
    public <T> void onResponse(T obj, int tag) {
        super.onResponse(obj, tag);
        switch (tag) {
            case Constants.TAG_USER_CONFIRM_MOBILE:
                VerifyMobileResponse response = (VerifyMobileResponse) obj;
                if (response.getStatus().equalsIgnoreCase(Constants.RESPONSE_SUCCESS)) {
                    showToast(response.getMessage());
                    if (blnFromMyAccount) {
                        Intent intent = new Intent(this, HomeActivity.class);
                        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_NEW_TASK);
                        startActivity(intent);
                        finish();
                    } else
                        goToNextActivity();
                } else
                    showToast(response.getMessage());
                findViewById(R.id.pbWheel).setVisibility(View.GONE);

                break;

            case Constants.TAG_USER_VERIFY_MOBILE:
                VerifyMobileResponse response1 = (VerifyMobileResponse) obj;
                showToast(response1.getMessage());
                break;
        }
    }


    private void goToNextActivity() {

        Intent intent = new Intent(getApplicationContext(), HomeActivity.class);
        if (user.getGender() != null && user.getGender().equalsIgnoreCase("M"))
            intent.putExtra(Constants.CLEAR_ACTIVITIES, false);
        else
            intent.putExtra(Constants.CLEAR_ACTIVITIES, true);
        intent.putExtra(Constants.START_PROFILE_ACTIVITY, true);
        startActivity(intent);
    }

    @Override
    public void onError(int error_code, int tag, Object object) {
        checkErrorCode(error_code, object);
    }
}
