package outletwise.com.twentydresses.view.activity;

import android.Manifest;
import android.accounts.Account;
import android.accounts.AccountManager;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.graphics.Typeface;
import android.os.Build;
import android.os.Bundle;
import android.support.v4.app.ActivityCompat;
import android.text.Editable;
import android.text.InputType;
import android.text.TextWatcher;
import android.util.Log;
import android.view.View;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.ImageView;
import android.widget.TextView;

import com.squareup.picasso.Picasso;
import com.sromku.simple.fb.entities.Profile;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.HashMap;

import outletwise.com.twentydresses.ApplicationUtil;
import outletwise.com.twentydresses.R;
import outletwise.com.twentydresses.controller.AnalyticsTracker;
import outletwise.com.twentydresses.model.BackgroundImgResponse;
import outletwise.com.twentydresses.model.Login;
import outletwise.com.twentydresses.model.LoginResponse;
import outletwise.com.twentydresses.model.Registration;
import outletwise.com.twentydresses.model.VisitorResponse;
import outletwise.com.twentydresses.model.database.greenbot.User;
import outletwise.com.twentydresses.utilities.Constants;
import outletwise.com.twentydresses.utilities.Urls;

/**
 * This class is use to login user by email & facebook
 */
public class LoginActivity extends BaseActivity {
    private TextView tv_email;
    private TextView tv_password;
    private boolean isFromFacebook;
    private String email;
    private LoginResponse loginResponse;
    private Boolean mobileVerified;
    private static User user;
    private Boolean blnIsChecked = false;
    private CheckBox checkBox;
    private String fbAccessToken;
    private SharedPreferences prefs;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);
        //  setBackgroundImage(Constants.IMG_SIGNIN);
        initFacebookConfig();
        init();

        findViewById(R.id.bSignIn).setOnClickListener(
                new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        // saveDataToPref();
                        if (validateFields()) {
                            email = tv_email.getText().toString().trim();
                            Login login = Login.getInstance();
                            login.setEmail(email);
                            login.setPassword(tv_password.getText().toString().trim());

                           /* HashMap<String, String> params = new HashMap<>();
                            params.put("email", login.getEmail());
                            params.put("password", login.getPassword());
                            postStringData(Urls.LOGIN, params, Constants.TAG_LOGIN, true);
                            */
                            JSONObject jsonObject = new JSONObject();
                            try {
                                jsonObject.put("email", login.getEmail());
                                jsonObject.put("password", login.getPassword());
                                jsonObject.put("user_device_id", getDeviceId());
                            } catch (JSONException e) {
                                e.printStackTrace();
                            }
                            postJsonData(Urls.LOGIN, jsonObject, Constants.TAG_LOGIN, true);
                        } else {
                            showToast(getString(R.string.toast_invalid_details));
                        }
                    }
                }
        );

        findViewById(R.id.tvRegister).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(getApplicationContext(), RegistrationActivity2.class));
                finish();
            }
        });

        findViewById(R.id.bFbLogin).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                loginWithFacebook();
            }
        });

        checkBox.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                blnIsChecked = isChecked;
                if (tv_password.getText().toString().trim().equalsIgnoreCase(""))
                    return;
                if (isChecked) {
                    tv_password.setInputType(InputType.TYPE_TEXT_VARIATION_PASSWORD);

                } else {
                    tv_password.setInputType(129);
                }
            }
        });


        tv_password.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
            }

            @Override
            public void afterTextChanged(Editable s) {
                if (tv_password.getText().length() > 0)
                    checkBox.setEnabled(true);
                else
                    checkBox.setEnabled(false);
            }
        });


        findViewById(R.id.tvForgotPassword).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(getApplicationContext(), ForgotPasswordActivity.class));
            }
        });


        setFacebookCallbacks(new FacebookCallbacks() {

            @Override
            public void onLoginSuccess(String accessToken) {
                Constants.debug("Access Token ::" + accessToken);
                fbAccessToken = accessToken;
                getFacebookProfile();
            }

            @Override
            public void onGetProfile(Profile profile) {
                JSONObject json = new JSONObject();
                try {
                    json.put("name", profile.getName());
                    json.put("first_name", profile.getFirstName());
                    json.put("last_name", profile.getLastName());
                    json.put("birthday", profile.getBirthday());
                    json.put("picture", profile.getPicture());
                    json.put("gender", profile.getGender());
                    json.put("email", profile.getEmail());
                    json.put("education", profile.getEducation());
                    json.put("bio", profile.getBio());
                    json.put("age_range", profile.getAgeRange());
                    json.put("cover", profile.getCover());
                    json.put("hometown", profile.getHometown());
                    json.put("locale", profile.getLocale());
                    json.put("location", profile.getLocation());
                    json.put("relationship_status", profile.getRelationshipStatus());
                } catch (JSONException e) {
                    e.printStackTrace();
                }

                ApplicationUtil.getInstance().ct.profile.pushFacebookUser(json);

                Login login = new Login();
                try {
                    login.setEmail(profile.getEmail());
                } catch (NullPointerException ex) {
                    login.setEmail(profile.getId() + "@facebook.com");
                }

                login.setPassword(profile.getId());
                User user = new User();
                user.setUser_mobile("");
                user.setUser_first_name(profile.getFirstName());
                user.setUser_last_name(profile.getLastName());
                user.setDob(profile.getBirthday());
                user.setUser_picture(profile.getPicture());
                user.setGender(profile.getGender());
                try {
                    user.setUser_email(profile.getEmail());
                } catch (NullPointerException ex) {
                    user.setUser_email(profile.getId() + "@facebook.com");
                }

                Registration registration = Registration.getInstance();
                registration.setUser(user);
                registration.setLogin(login);

                isFromFacebook = true;

                JSONObject jsonObject = new JSONObject();
                try {
                    jsonObject.put("email", login.getEmail());
                    jsonObject.put("password", "");
                    jsonObject.put("facebook_access_token", fbAccessToken);
                    jsonObject.put("user_device_id", getDeviceId());

                } catch (JSONException e) {
                    e.printStackTrace();
                }
                postJsonData(Urls.LOGIN, jsonObject, Constants.TAG_LOGIN, true);
                // postJsonData(Urls.VISITOR, login, Constants.TAG_VISITOR, true);
            }

            @Override
            public void onLogoutSuccess() {

            }
        });
    }

    private void init() {

        tv_email = (TextView) findViewById(R.id.etEmail);
        tv_password = (TextView) findViewById(R.id.etPassword);
        checkBox = (CheckBox) findViewById(R.id.checkBox);
        tv_password.setTypeface(Typeface.DEFAULT);
        preferences.edit().remove(Constants.KEY_TOKEN).commit();
        preferences.edit().putString(Constants.KEY_TOKEN, "").apply();
        preferences.edit().remove(Constants.USER_ID).commit();
        preferences.edit().putString(Constants.USER_ID, "").apply();
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            int hasReadStoragePermission = checkSelfPermission(Manifest.permission.GET_ACCOUNTS);
            if (hasReadStoragePermission != PackageManager.PERMISSION_GRANTED) {
                requestPermissions(new String[]{Manifest.permission.GET_ACCOUNTS}, 1);
                return;
            } else
                tv_email.setText(getMailId());
        } else
            tv_email.setText(getMailId());

    }


    @Override
    public void onRequestPermissionsResult(int requestCode, String[] permissions, int[] grantResults) {

        if (grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
            // Permission Granted
            tv_email.setText(getMailId());
        } else
            showToast(getString(R.string.text_permission_denied));
    }

    public String getMailId() {
        String strGmail = "";
        try {
            //  if (ActivityCompat.checkSelfPermission(this, Manifest.permission.GET_ACCOUNTS) != PackageManager.PERMISSION_GRANTED)
            {
                Account[] accounts = AccountManager.get(this).getAccounts();
                for (Account account : accounts) {
                    String possibleEmail = account.name;
                    String type = account.type;

                    if (type.equals("com.google")) {
                        strGmail = possibleEmail;
                        Log.e("PIKLOG", "Emails: " + strGmail);
                        break;
                    }
                }
            }

        } catch (Exception e) {
            e.printStackTrace();
        }

        return strGmail;
    }

    boolean validateFields() {
        if (tv_email.getText().toString().trim().equalsIgnoreCase("")) {
            tv_email.requestFocus();
            return false;
        } else if (tv_password.getText().toString().trim().equalsIgnoreCase("")) {
            tv_password.requestFocus();
            return false;
        }

        return true;
    }

    @Override
    public <T> void onResponse(T obj, int tag) {
        super.onResponse(obj, tag);
        switch (tag) {
            case Constants.TAG_VISITOR:
                VisitorResponse response = (VisitorResponse) obj;
               /* if (response.getStatus().equalsIgnoreCase(Constants.RESPONSE_SUCCESS))
                {*/
                if (isFromFacebook) {
                    HashMap<String, String> params = new HashMap<>();
                    params.put("email", Registration.getInstance().getEmail());
                    params.put("password", Registration.getInstance().getPassword());
                    postStringData(Urls.LOGIN, params, Constants.TAG_LOGIN, true);
                } else
                    showToast(response.getMessage());

                break;
            case Constants.TAG_LOGIN:
                loginResponse = (LoginResponse) obj;

                if (loginResponse.getStatus().equalsIgnoreCase(Constants.RESPONSE_SUCCESS)) {
                    if (isFromFacebook) {
                      /*  loginResponse.getUser().setUser_password("");
                        loginResponse.getUser().setFb_token(fbAccessToken);*/
                        trackPage(getString(R.string.pgFbLogins));
                    } else {
                        /*loginResponse.getUser().setUser_password(tv_password.getText().toString());
                        loginResponse.getUser().setFb_token("");*/
                        trackPage(getString(R.string.pgLogins));
                    }

                    //   AnalyticsTracker.getInstance().trackSignIn(loginResponse.getUser(), "");

                    preferences.edit().remove(Constants.KEY_TOKEN).commit();
                    preferences.edit().remove(Constants.USER_ID).commit();

                    preferences.edit().putString(Constants.KEY_TOKEN, loginResponse.getToken()).apply();
                    preferences.edit().putString(Constants.USER_ID, String.valueOf(loginResponse.getUser().getUser_id())).apply();

                    deleteAllRecords(Constants.TAG_USER);
                    deleteAllRecords(Constants.TAG_SHORTLIST_ALL);

                    insertRecord(loginResponse.getUser(), Constants.TAG_USER);

                } else
                    showToast(loginResponse.getMessage());

                break;

            case Constants.TAG_FORGOT_PASSWORD:
                VisitorResponse visitorResponse = (VisitorResponse) obj;
                if (visitorResponse.getStatus().equalsIgnoreCase(Constants.RESPONSE_SUCCESS))
                    showToast(visitorResponse.getMessage());
                else
                    showToast(visitorResponse.getMessage());
                break;

            case Constants.TAG_IMG_BACKGROUND:
                findViewById(R.id.pbWheel).setVisibility(View.GONE);
                BackgroundImgResponse imgResponse = (BackgroundImgResponse) obj;
                if (imgResponse.getStatus().equalsIgnoreCase(Constants.RESPONSE_SUCCESS)) {
                    String imgUrl = imgResponse.getImage();
                    ImageView iv_background = (ImageView) findViewById(R.id.iv_background);
                    Picasso.with(getApplicationContext()).load(imgUrl).into(iv_background);
                    toggleVisibility();
                }
                break;
            default:
        }
    }

    private void toggleVisibility() {
        findViewById(R.id.rlLogin).setVisibility(View.VISIBLE);
        findViewById(R.id.bFbLogin).setVisibility(View.VISIBLE);
    }

    @Override
    public void onCompleteInsertion(int tag, String msg) {
        super.onCompleteInsertion(tag, msg);
        if (tag == Constants.TAG_USER) {
            Login login = new Login();
            login.setEmail(loginResponse.getUser().getUser_email());
            Registration.getInstance().setUser(loginResponse.getUser());
            Registration.getInstance().setLogin(login);

            String user_registered = loginResponse.getUser().getUser_registered();
            if (user_registered.equalsIgnoreCase("Yes")) {
                mobileVerified = loginResponse.getUser().getMobile_verified();
                finish();
                startActivity(new Intent(this, HomeActivity.class));
            } else if (loginResponse.getUser().getGender() != null) {
                Constants.EDITPROFILE = false;
                Intent intent = new Intent(getApplicationContext(), QuizActivity.class);
                startActivity(intent);
            } else {
                Constants.EDITPROFILE = false;
                startActivity(new Intent(getApplicationContext(), SelectGenderActivity.class));
            }
        }
    }

    @Override
    public void onError(int error_code, int tag, Object object) {
        checkErrorCode(error_code, object);
        findViewById(R.id.pbWheel).setVisibility(View.GONE);
        enableTouch();
    }


    @Override
    public void onBackPressed() {
        startActivity(new Intent(getApplicationContext(), RegistrationActivity.class));
    }
}
