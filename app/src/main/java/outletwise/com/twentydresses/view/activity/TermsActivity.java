package outletwise.com.twentydresses.view.activity;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.ActionBar;
import android.widget.LinearLayout;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

import outletwise.com.twentydresses.R;
import outletwise.com.twentydresses.controller.adapter.TermsAdapter;
import outletwise.com.twentydresses.model.Terms;
import outletwise.com.twentydresses.utilities.Constants;
import outletwise.com.twentydresses.utilities.Urls;

/**
 * This class is use to show terms & conditions
 */
public class TermsActivity extends BaseActivity {
    private String url, name;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_terms);

        url = getIntent().getStringExtra("page_url");
        name = getIntent().getStringExtra("page_name");

        setUpToolBar(name.toUpperCase());
        final ActionBar ab = getSupportActionBar();
        ab.setDisplayHomeAsUpEnabled(true);

        getData();
    }

    private void getData() {
        postJsonData(url, new JSONObject(), Constants.TAG_TERMS, true);
    }

    @Override
    public <T> void onResponse(T obj, int tag) {
        super.onResponse(obj, tag);
        switch (tag) {
            case Constants.TAG_TERMS:

                Terms termsResponse = (Terms) obj;
                List<Terms.ContentBean> list = termsResponse.getContent();

                String strKey = list.get(0).getKey();
                String strValue = list.get(0).getValue();

                ArrayList<String> keys = new ArrayList<>();
                ArrayList<String> values = new ArrayList<>();

                for (int i = 0; i < list.size(); i++) {
                    keys.add(list.get(i).getKey());
                    values.add(list.get(i).getValue());
                }

                LinearLayout llCredit = (LinearLayout) findViewById(R.id.llTerms);
                TermsAdapter termsAdapter = new TermsAdapter(this, keys, values, llCredit);
               /* String json = (String) obj;
                ArrayList<String> keys = new ArrayList<>();
                ArrayList<String> values = new ArrayList<>();
                try {
                    JSONObject issueObj = new JSONObject(json);
                    Iterator iterator = issueObj.keys();
                    int temp = 0;
                    while (iterator.hasNext()) {
                        String key = (String) iterator.next();
                        String keyReplace = key.replace("_", " ");
                        keys.add(temp, keyReplace);

                        String value = issueObj.optString(key);
                        values.add(temp, value);
                        temp++;
                    }
                } catch (JSONException ex) {
                }

                String jsonFaq = values.get(0);

                ArrayList<String> questions = new ArrayList<>();
                ArrayList<String> answers = new ArrayList<>();
                try {
                    JSONObject issueObj = new JSONObject(jsonFaq);
                    Iterator iterator = issueObj.keys();
                    int temp = 0;
                    while (iterator.hasNext()) {
                        String key = (String) iterator.next();
                        String keyReplace = key.replace("_", " ");
                        questions.add(temp, keyReplace);

                        String value = issueObj.optString(key);
                        answers.add(temp, value);
                        temp++;
                    }

                    LinearLayout llCredit = (LinearLayout) findViewById(R.id.llTerms);
                    TermsAdapter termsAdapter = new TermsAdapter(this, questions, answers, llCredit);
                } catch (JSONException ex) {
                }*/
                break;

        }
    }

    @Override
    public void onError(int error_code, int tag, Object object) {
        checkErrorCode(error_code, object);
    }
}
