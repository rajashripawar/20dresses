package outletwise.com.twentydresses.view.activity;

import android.Manifest;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.support.v7.app.ActionBar;
import android.text.Html;
import android.text.Spannable;
import android.text.SpannableString;
import android.text.style.RelativeSizeSpan;
import android.view.View;
import android.widget.ScrollView;
import android.widget.TextView;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;

import outletwise.com.twentydresses.R;
import outletwise.com.twentydresses.model.FaqAdapter;
import outletwise.com.twentydresses.model.HelpFaq;
import outletwise.com.twentydresses.utilities.Constants;
import outletwise.com.twentydresses.utilities.Urls;

/**
 * Created by User-PC on 05-11-2015.
 * This class is use to show Faq and some help like how to cancel order, return exchange etc.
 */
public class HelpFaqActivity extends BaseActivity {

    private ScrollView sv;
    private ArrayList<String> Questions = new ArrayList<>();
    private ArrayList<String> Answers = new ArrayList<>();
    private Intent intent;
    private TextView tvCancel, tvReturn, tvRefund, tvExchange, tvTrackOrder;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_help_faq);

        setUpToolBar(getString(R.string.toolbar_helpfaq));
        final ActionBar ab = getSupportActionBar();
        ab.setDisplayHomeAsUpEnabled(true);

        init();
    }

    void init() {

        postJsonData(Urls.HELP_FAQ, new JSONObject(), Constants.TAG_HELP_FAQ, true);

        tvCancel = (TextView) findViewById(R.id.tvCancel);
        tvRefund = (TextView) findViewById(R.id.tvRefund);
        tvReturn = (TextView) findViewById(R.id.tvReturn);
        tvExchange = (TextView) findViewById(R.id.tvExchange);
        tvTrackOrder = (TextView) findViewById(R.id.tvTrackOrder);

        findViewById(R.id.cvCancelOrder).setOnClickListener(this);
        findViewById(R.id.cvReturn).setOnClickListener(this);
        findViewById(R.id.cvRefund).setOnClickListener(this);
        findViewById(R.id.cvTrackOrder).setOnClickListener(this);
        findViewById(R.id.cvExchange).setOnClickListener(this);
        sv = (ScrollView) findViewById(R.id.scrollView);

        findViewById(R.id.txtvw_email).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent emailIntent = new Intent(Intent.ACTION_SEND);
                emailIntent.setData(Uri.parse("mailto:"));
                emailIntent.setType("text/plain");
                emailIntent.putExtra(Intent.EXTRA_EMAIL, new String[]{"hello@20Dresses.com"});
                startActivity(Intent.createChooser(emailIntent, "Send mail..."));
                //  finish();
            }
        });

        findViewById(R.id.txtvw_number).setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View view) {
                intent = new Intent(Intent.ACTION_CALL, Uri.parse("tel:" + "+919699822211"));

                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                    int hasCallPermission = checkSelfPermission(Manifest.permission.CALL_PHONE);
                    if (hasCallPermission != PackageManager.PERMISSION_GRANTED) {
                        requestPermissions(new String[]{Manifest.permission.CALL_PHONE}, 1);
                        return;
                    } else
                        startActivity(intent);
                } else
                    startActivity(intent);
            }
        });
    }

    private SpannableString setSpan(String finalString1) {
        SpannableString sp = new SpannableString(finalString1);
        sp.setSpan(new RelativeSizeSpan(1.2f),
                0, finalString1.length() - 1, Spannable.SPAN_INCLUSIVE_INCLUSIVE);

        return sp;
    }

    @Override
    public void onClick(View v) {
        super.onClick(v);
        switch (v.getId()) {
            case R.id.cvCancelOrder:
                if (tvCancel.getVisibility() == View.VISIBLE)
                    tvCancel.setVisibility(View.GONE);
                else
                    tvCancel.setVisibility(View.VISIBLE);
                //startActivity(new Intent(getApplicationContext(), MyOrdersActivity.class));
                break;
            case R.id.cvReturn:
                if (tvReturn.getVisibility() == View.VISIBLE)
                    tvReturn.setVisibility(View.GONE);
                else
                    tvReturn.setVisibility(View.VISIBLE);
                // startActivity(new Intent(getApplicationContext(), MyOrdersActivity.class));
                break;
            case R.id.cvRefund:
                if (tvRefund.getVisibility() == View.VISIBLE)
                    tvRefund.setVisibility(View.GONE);
                else
                    tvRefund.setVisibility(View.VISIBLE);
                // startActivity(new Intent(getApplicationContext(), MyWalletActivity.class));
                break;
            case R.id.cvTrackOrder:
                if (tvTrackOrder.getVisibility() == View.VISIBLE)
                    tvTrackOrder.setVisibility(View.GONE);
                else
                    tvTrackOrder.setVisibility(View.VISIBLE);
                //startActivity(new Intent(getApplicationContext(), MyOrdersActivity.class));
                break;
            case R.id.cvExchange:
                if (tvExchange.getVisibility() == View.VISIBLE)
                    tvExchange.setVisibility(View.GONE);
                else
                    tvExchange.setVisibility(View.VISIBLE);
                // startActivity(new Intent(getApplicationContext(), MyOrdersActivity.class));
                break;
        }
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, String[] permissions, int[] grantResults) {
        // super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        if (grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
            // Permission Granted
            startActivity(intent);
        }
        else
            showToast(getString(R.string.text_permission_denied));
    }


    @Override
    public <T> void onResponse(T obj, int tag) {
        super.onResponse(obj, tag);
        switch (tag) {
            case Constants.TAG_HELP_FAQ:
                JSONObject issueObj = (JSONObject) obj;
                try {
                 //   *****************************HELP********************************
                    JSONArray jsonArrayHelp = (JSONArray) issueObj.get("help");
                    HashMap<String, String> hashMapHelp = new HashMap<>();
                    JSONObject jsonObject = jsonArrayHelp.getJSONObject(0);
                    Iterator iterator = jsonObject.keys();
                    int temp = 0;
                    while (iterator.hasNext()) {
                        String key = (String) iterator.next();
                        String value = jsonObject.optString(key);
                        hashMapHelp.put(key, value);
                        temp++;
                    }

                 //   *****************************FAQ********************************

                    JSONArray jsonArrayFaq = (JSONArray) issueObj.get("faq");
                    HashMap<String, String> hashMapFaq = new HashMap<>();
                    JSONObject jsonObjectFaq = jsonArrayFaq.getJSONObject(0);
                    Iterator iteratorFaq = jsonObjectFaq.keys();
                    int tempFaq = 0;
                    while (iteratorFaq.hasNext()) {
                        String key = (String) iteratorFaq.next();
                        String keyReplace = key.replace("_", " ");

                        String value = jsonObjectFaq.optString(key);
                        hashMapFaq.put(keyReplace, value);
                        tempFaq++;
                    }

                    FaqAdapter faqAdapter = new FaqAdapter(this, hashMapFaq);
                    ArrayList<String> help_keys = new ArrayList<>();

                    tvCancel.setText(Html.fromHtml(hashMapHelp.get("Cancel_Order")));
                    tvReturn.setText(Html.fromHtml(hashMapHelp.get("Return")));
                    tvRefund.setText(Html.fromHtml(hashMapHelp.get("Refund")));
                    tvTrackOrder.setText(Html.fromHtml(hashMapHelp.get("Track_Order")));
                    tvExchange.setText(Html.fromHtml(hashMapHelp.get("Exchange")));

                } catch (JSONException e) {
                    e.printStackTrace();
                }
                break;
        }
    }

    @Override
    public void onError(int error_code, int tag, Object object) {
        checkErrorCode(error_code, object);
    }
}
