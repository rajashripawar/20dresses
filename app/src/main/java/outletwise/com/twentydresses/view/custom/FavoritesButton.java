package outletwise.com.twentydresses.view.custom;

import android.annotation.TargetApi;
import android.content.Context;
import android.content.res.TypedArray;
import android.os.Build;
import android.util.AttributeSet;
import android.view.LayoutInflater;
import android.widget.ImageView;
import android.widget.RelativeLayout;

import com.pnikosis.materialishprogress.ProgressWheel;

import outletwise.com.twentydresses.R;

/**
 * Created by User-PC on 07-09-2015.
 */
public class FavoritesButton extends RelativeLayout {

    public static final int PROGRESS_IDLE = 0;
    public static final int IN_PROGRESS = 1;
    public static final int PROGRESS_COMPLETE = 2;

    private ImageView ivFavorites;
    private ProgressWheel pwFavorites;
    private int currentState;
    private double buttonPadding;

    public FavoritesButton(Context context) {
        super(context);
        initializeViews(context);
    }

    public FavoritesButton(Context context, AttributeSet attrs) {
        super(context, attrs);
        setAttr(context, attrs);
        initializeViews(context);
    }

    public FavoritesButton(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        setAttr(context, attrs);
        initializeViews(context);
    }

    @TargetApi(Build.VERSION_CODES.LOLLIPOP)
    public FavoritesButton(Context context, AttributeSet attrs, int defStyleAttr, int defStyleRes) {
        super(context, attrs, defStyleAttr, defStyleRes);
        setAttr(context, attrs);
        initializeViews(context);
    }

    private void setAttr(Context context, AttributeSet attrs) {
        TypedArray a = context.getTheme().obtainStyledAttributes(
                attrs,
                R.styleable.FavoritesButton,
                0, 0);

        try {
            buttonPadding = a.getDimension(R.styleable.FavoritesButton_padding, 0);
        } finally {
            a.recycle();
        }

    }

    /**
     * Inflates the views in the layout.
     *
     * @param context the current context for the view.
     */
    private void initializeViews(Context context) {
        LayoutInflater inflater = (LayoutInflater) context
                .getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        inflater.inflate(R.layout.layout_button_favorites, this);
    }

    @Override
    protected void onFinishInflate() {
        super.onFinishInflate();
        ivFavorites = (ImageView) this.findViewById(R.id.tbAddToFav);
        pwFavorites = (ProgressWheel) this.findViewById(R.id.pwFavorites);
        pwFavorites.setVisibility(GONE);
        this.setClickable(true);
        currentState = PROGRESS_IDLE;
    }

    @Override
    protected void onMeasure(int widthMeasureSpec, int heightMeasureSpec) {
        widthMeasureSpec += buttonPadding;
        heightMeasureSpec += buttonPadding;
        super.onMeasure(widthMeasureSpec, heightMeasureSpec);
    }

    public void startProgress() {
        ivFavorites.setVisibility(GONE);
        pwFavorites.setVisibility(VISIBLE);
        currentState = IN_PROGRESS;
    }

    public void stopProgress(boolean success) {
        ivFavorites.setVisibility(VISIBLE);
        pwFavorites.setVisibility(GONE);
        toggleImage(success);
    }

    private void toggleImage(boolean flag) {
        if (flag) {
            currentState = PROGRESS_COMPLETE;
            ivFavorites.setImageResource(R.drawable.ic_favorites_selected);
        } else {
            currentState = PROGRESS_IDLE;
            ivFavorites.setImageResource(R.drawable.ic_favorites_unselected);
        }
    }

    public int getCurrentState() {
        return currentState;
    }
}
