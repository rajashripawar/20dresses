package outletwise.com.twentydresses.view.activity;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.ActionBar;
import android.view.View;
import android.widget.TextView;

import outletwise.com.twentydresses.R;
import outletwise.com.twentydresses.utilities.Constants;

/**
 * Created by User-PC on 07-11-2015.
 * This class will be called after successful order placement
 */
public class OrderSuccessfulActivity extends BaseActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_order_successful);
        trackPage(getString(R.string.pgThankYou));
        setUpToolBar(getString(R.string.toolbar_order_complete));
        final ActionBar ab = getSupportActionBar();
        ab.setDisplayHomeAsUpEnabled(true);

        int orderId = getIntent().getIntExtra(Constants.ORDER_ID, 0);
        ((TextView) findViewById(R.id.tvOrderId)).setText("Order id: " + orderId);

        findViewById(R.id.bContinueShopping).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                gotoHome();
            }
        });
    }

    @Override
    public void onBackPressed() {
        gotoHome();
    }

    void gotoHome() {
        Intent intent = new Intent(this, HomeActivity.class);
        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_NEW_TASK);
        startActivity(intent);
        finish();
    }
}
