package outletwise.com.twentydresses.view.fragment;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.text.Html;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.RelativeLayout;
import android.widget.TextView;

import java.util.HashMap;

import outletwise.com.twentydresses.R;
import outletwise.com.twentydresses.controller.interfaces.StyleProfileChanged;
import outletwise.com.twentydresses.model.Registration;
import outletwise.com.twentydresses.model.StyleProfile;
import outletwise.com.twentydresses.model.UserAttrEntity;
import outletwise.com.twentydresses.utilities.Constants;
import outletwise.com.twentydresses.view.activity.BaseActivity;
import outletwise.com.twentydresses.view.activity.QuizActivity;

/**
 * Created by User-PC on 29-07-2015.
 */

public class QuizSelectComplexionFragment extends BaseFragment {

    private View rootView;
    private View previousView;
    private TextView tvWheatish;
    private TextView tvTan;
    private TextView tvDusky;
    private UserAttrEntity userAttr;
    private HashMap<String, View> complexion = new HashMap<>();
    private TextView bSkipQuiz;
    private String wheatish = "I'm Fair";
    private String tan = "I'm Wheatish";
    private String dusky = "I'm Dusky";

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        rootView = inflater.inflate(R.layout.fragment_quiz_select_complexion, container, false);

        tvWheatish = (TextView) rootView.findViewById(R.id.tvWheatish);
        tvTan = (TextView) rootView.findViewById(R.id.tvTan);
        tvDusky = (TextView) rootView.findViewById(R.id.tvDusky);

        complexion.put(wheatish, tvWheatish);
        complexion.put(tan, tvTan);
        complexion.put(dusky, tvDusky);

        //For Edit profile
        try {
            userAttr = getArguments().getParcelable(StyleProfile.class.getSimpleName());
        } catch (NullPointerException np) {
            Constants.warning("Style Profile null");
        }
        if (userAttr != null) {
            resetViews(complexion.get(userAttr.getFace_Color()));
        }


        tvWheatish.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                resetViews(v);
                rootView.findViewById(R.id.pbWheel).setVisibility(View.VISIBLE);

                if (userAttr != null) {
                    userAttr.setFace_Color(wheatish);
                    // if(Constants.QUIZ.equalsIgnoreCase(Constants.REGISTER))
                    ((StyleProfileChanged) mActivity).onChange(userAttr);
                   /* else
                    {
                        Quiz.getInstance().getUser_attr().setFace_Color(wheatish);
                        addFragment(R.id.frame_container, new QuizSelectmeasureFragment(), QuizSelectmeasureFragment.class.getSimpleName());
                    }*/
                } else {
                    Registration.getInstance().getQuiz().getUser_attr().setFace_Color(wheatish);
                    addFragment(R.id.frame_container, new QuizSelectmeasureFragment(), QuizSelectmeasureFragment.class.getSimpleName());
                }
            }
        });

        tvTan.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                resetViews(v);
                rootView.findViewById(R.id.pbWheel).setVisibility(View.VISIBLE);

                if (userAttr != null) {
                    userAttr.setFace_Color(tan);
                    //   if(Constants.QUIZ.equalsIgnoreCase(Constants.REGISTER))
                    ((StyleProfileChanged) mActivity).onChange(userAttr);
                 /*   else
                    {
                        Quiz.getInstance().getUser_attr().setFace_Color(tan);
                        addFragment(R.id.frame_container, new QuizSelectmeasureFragment(), QuizSelectmeasureFragment.class.getSimpleName());
                    }*/
                } else {
                    Registration.getInstance().getQuiz().getUser_attr().setFace_Color(tan);
                    addFragment(R.id.frame_container, new QuizSelectmeasureFragment(), QuizSelectmeasureFragment.class.getSimpleName());
                }
            }
        });

        tvDusky.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                resetViews(v);
                rootView.findViewById(R.id.pbWheel).setVisibility(View.VISIBLE);
                if (userAttr != null) {
                    userAttr.setFace_Color(dusky);
                        //   if(Constants.QUIZ.equalsIgnoreCase(Constants.REGISTER))
                    ((StyleProfileChanged) mActivity).onChange(userAttr);
                  /*  else
                    {
                        Quiz.getInstance().getUser_attr().setFace_Color(dusky);
                        addFragment(R.id.frame_container, new QuizSelectmeasureFragment(), QuizSelectmeasureFragment.class.getSimpleName());
                    }*/
                } else {
                    Registration.getInstance().getQuiz().getUser_attr().setFace_Color(dusky);
                    addFragment(R.id.frame_container, new QuizSelectmeasureFragment(), QuizSelectmeasureFragment.class.getSimpleName());
                }
            }
        });
        bSkipQuiz = (TextView)rootView. findViewById(R.id.bSkipQuiz);
        bSkipQuiz.setText(Html.fromHtml(getString(R.string.button_skip)));
        if (Constants.EDITPROFILE ||Constants.TAKE_QUIZ){
            bSkipQuiz.setVisibility(View.GONE);
        }
        bSkipQuiz.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Constants.SKIPQUIZ = true;
                addFragment(R.id.frame_container, new QuizEnterUserDetails(), QuizEnterUserDetails.class.getSimpleName());
            }
        });
        return rootView;
    }

    void resetViews(View v) {
        if (v != null) {
            if (previousView != null)
                previousView.setSelected(false);
            previousView = v;
            v.setSelected(true);
        }
    }

    @Override
    public void onResume() {
        super.onResume();
        TextView textView = ((TextView) getToolBar().findViewById(R.id.tvStep));
        if (textView != null)
            textView.setText("1 of 5");
    }

}
