package outletwise.com.twentydresses.view.activity;

import android.content.Intent;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextUtils;
import android.text.TextWatcher;
import android.text.method.KeyListener;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.Toast;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

import outletwise.com.twentydresses.R;
import outletwise.com.twentydresses.model.Address;
import outletwise.com.twentydresses.model.NewAddressResponse;
import outletwise.com.twentydresses.model.PinCodeResponse;
import outletwise.com.twentydresses.model.VisitorResponse;
import outletwise.com.twentydresses.utilities.Constants;
import outletwise.com.twentydresses.utilities.Urls;

public class EditAddressActivity extends BaseActivity {

    Address address;
    EditText tvPinCode, tvFirstName, tvLastName, tvAddress, tvCity, tvMobile;
    private Spinner tvState;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_edit_address);
        setUpToolBar(getString(R.string.toolbar_edit_address));
        final ActionBar ab = getSupportActionBar();
        ab.setDisplayHomeAsUpEnabled(true);

        address = getIntent().getParcelableExtra(Constants.ADDRESS);

        initialization();

        findViewById(R.id.tvProceed).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (validateAddress())
                    postAddressData();
            }
        });

    }


    private void postAddressData() {

        JSONObject jsonObject = new JSONObject();
        try {
            if (user != null && String.valueOf(user.getUser_id()) != null)
                jsonObject.put(Constants.USER_ID, user.getUser_id());
            else
                jsonObject.put(Constants.USER_ID, preferences.getString(Constants.USER_ID,""));

            jsonObject.put(Constants.ADDRESS_ID, address.getAddress_id());
            jsonObject.put(Constants.USER_ID, user.getUser_id());
            jsonObject.put(Constants.AUTH_TOKEN, preferences.getString(Constants.KEY_TOKEN, ""));
            jsonObject.put(Constants.FIRST_NAME, tvFirstName.getText().toString());
            jsonObject.put(Constants.LAST_NAME, tvLastName.getText().toString());
            jsonObject.put(Constants.MOBILE, tvMobile.getText().toString());
            jsonObject.put(Constants.COUNTRY, "India");
            jsonObject.put(Constants.STATE, tvState.getSelectedItem().toString());
            jsonObject.put(Constants.CITY, tvCity.getText().toString());
            jsonObject.put(Constants.ADDRESS, tvAddress.getText().toString());
            jsonObject.put(Constants.PINCODE, tvPinCode.getText().toString());

        } catch (JSONException e) {
            e.printStackTrace();
        }
        postJsonData(Urls.ADDRESS_UPDATE, jsonObject, Constants.TAG_UPDATE_ADDRESS, true);
    }



    private void initialization() {
        tvPinCode = ((EditText) findViewById(R.id.tvPinCode));
        tvFirstName = ((EditText) findViewById(R.id.tvFirstName));
        tvLastName = ((EditText) findViewById(R.id.tvLastName));
        tvAddress = ((EditText) findViewById(R.id.tvAddress));
        tvCity = ((EditText) findViewById(R.id.tvCity));
        tvMobile = ((EditText) findViewById(R.id.tvMobile));
        tvState = (Spinner) findViewById(R.id.tvState);

        ArrayList<String> simpleState = new ArrayList<>();
        simpleState.add(address.getState());
        ArrayAdapter<String> adapter = new ArrayAdapter<String>(getApplicationContext(), R.layout.layout_spinner, simpleState);
        tvState.setAdapter(adapter);
        tvState.setSelection(adapter.getPosition(address.getState()));

        tvCity.setTag(tvCity.getKeyListener());
        tvCity.setKeyListener(null);

        tvPinCode.setText(address.getPincode());
        tvFirstName.setText(address.getFirstname());
        tvLastName.setText(address.getLastname());
        tvAddress.setText(address.getAddress());
        tvCity.setText(address.getCity());
        tvMobile.setText(address.getMobile());

        tvPinCode.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {
                String pincode = tvPinCode.getText().toString();
                if (pincode.length() == 6) {
                    Constants.pincodeTextchange = true;
                    JSONObject jsonObject = new JSONObject();
                    try {
                        jsonObject.put(Constants.PINCODE, pincode);
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                    postJsonData(Urls.CHK_DEL_OPTS, jsonObject, Constants.TAG_CHK_DEL_OPTS, false);

                } else
                    showToast(getString(R.string.text_invalid_pincode));
            }

            @Override
            public void afterTextChanged(Editable editable) {

            }
        });
    }


    boolean validateAddress() {
        if (TextUtils.isEmpty(tvPinCode.getText().toString().trim())) {
            showToast(getString(R.string.enter_pincode));
            tvPinCode.requestFocus();
            return false;
        } else if (TextUtils.isEmpty(tvFirstName.getText().toString().trim())) {
            showToast(getString(R.string.enter_first_name));
            tvFirstName.requestFocus();
            return false;
        } else if (TextUtils.isEmpty(tvLastName.getText().toString().trim())) {
            showToast(getString(R.string.enter_last_name));
            tvLastName.requestFocus();
            return false;
        } else if (TextUtils.isEmpty(tvCity.getText().toString().trim())) {
            showToast(getString(R.string.enter_city));
            tvCity.requestFocus();
            return false;
        } else if (TextUtils.isEmpty(tvState.getSelectedItem().toString().trim()) || tvState.getSelectedItem().toString().equalsIgnoreCase("Select State")) {
            showToast(getString(R.string.select_state));
            tvState.requestFocus();
            return false;
        } else if (TextUtils.isEmpty(tvAddress.getText().toString().trim())) {
            showToast(getString(R.string.enter_address));
            tvAddress.requestFocus();
            return false;
        } else if (TextUtils.isEmpty(tvMobile.getText().toString().trim()) || tvMobile.getText().length() != 10) {
            showToast(getString(R.string.invalid_mob));
            tvMobile.requestFocus();
            return false;
        }
        return true;
    }


    @Override
    public <T> void onResponse(T obj, int tag) {
        switch (tag) {
            case Constants.TAG_CHK_DEL_OPTS:
                PinCodeResponse response1 = (PinCodeResponse) obj;
                if (Constants.pincodeTextchange) {
                    if (response1.getStatus().equalsIgnoreCase("success")) {
                        if (response1.getCity() != null && !response1.getCity().equalsIgnoreCase(""))
                            tvCity.setText(response1.getCity());
                        else {
                            tvCity.setText("");
                            tvCity.setKeyListener((KeyListener) tvCity.getTag());
                        }

                        ArrayAdapter<String> adapter = new ArrayAdapter<String>(getApplicationContext(), R.layout.layout_spinner, response1.getStates());
                        tvState.setAdapter(adapter);
                        adapter.notifyDataSetChanged();
                        tvState.setSelection(adapter.getPosition(response1.getState()));

                        if (response1.getPickup_available())
                            Constants.COD_AVAILABLE = true;
                        else {
                            tvPinCode.setCompoundDrawablesWithIntrinsicBounds(null, null, null, null);
                            showToast(response1.getMsg());
                            Constants.COD_AVAILABLE = false;
                        }
                        Constants.pincodeTextchange = false;
                    } else
                        showToast(response1.getMsg());
                }
                break;
            case Constants.TAG_UPDATE_ADDRESS:
                VisitorResponse response = (VisitorResponse) obj;
                if (response.getStatus().equalsIgnoreCase(Constants.RESPONSE_SUCCESS)) {
                    Intent intent = new Intent();
                    setResult(1, intent);
                    finish();
                } else
                    showToast(response.getMessage());
        }
    }
}
