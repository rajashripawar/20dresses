package outletwise.com.twentydresses.view.fragment;

import android.Manifest;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.text.Html;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.TextView;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import mehdi.sakout.fancybuttons.FancyButton;
import outletwise.com.twentydresses.R;
import outletwise.com.twentydresses.controller.MyTagHandler;
import outletwise.com.twentydresses.controller.adapter.OrdersViewAdapter;
import outletwise.com.twentydresses.controller.adapter.ReasonsAdapter;
import outletwise.com.twentydresses.model.Category;
import outletwise.com.twentydresses.model.Order;
import outletwise.com.twentydresses.model.OrderItem;
import outletwise.com.twentydresses.model.OrderReturnResponse;
import outletwise.com.twentydresses.model.Product;
import outletwise.com.twentydresses.model.Reason;
import outletwise.com.twentydresses.model.ReturnExchangeRequest;
import outletwise.com.twentydresses.model.VisitorResponse;
import outletwise.com.twentydresses.utilities.Constants;
import outletwise.com.twentydresses.utilities.Urls;
import outletwise.com.twentydresses.view.activity.BaseActivity;
import outletwise.com.twentydresses.view.activity.LoginActivity;
import outletwise.com.twentydresses.view.activity.ProductViewActivity;

/**
 * Created by User-PC on 04-11-2015.
 */
public class OrderViewFragment extends BaseFragment {

    private View rootView;
    private OrdersViewAdapter adapter;
    private OrderItem orderItem;
    private Reason reason;
    private int itemPosition;
    private OrderReturnResponse returnResponse;
    private Constants.OrderAction orderAction;
    private Intent intent;
    private Reason.ReasonsEntity selectedReason;
    private String strComments;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        rootView = inflater.inflate(R.layout.fragment_order_item, container, false);
        init();
        return rootView;
    }

    void init() {
        final Order order = getArguments().getParcelable(Constants.ORDER);
        orderAction = (Constants.OrderAction) getArguments().getSerializable(Constants.OrderAction.class.getSimpleName());

        if (orderAction == Constants.OrderAction.CANCEL && order.isCancel_button()) {
            getCancelReasons();
            rootView.findViewById(R.id.llCancelOrder).setVisibility(View.VISIBLE);
            rootView.findViewById(R.id.llTotal).setVisibility(View.GONE);
            rootView.findViewById(R.id.rlHelp).setVisibility(View.GONE);

        } else {
            rootView.findViewById(R.id.llCancelOrder).setVisibility(View.GONE);
            rootView.findViewById(R.id.llTotal).setVisibility(View.VISIBLE);
            rootView.findViewById(R.id.rlHelp).setVisibility(View.VISIBLE);
        }


        rootView.findViewById(R.id.bCancel).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (selectedReason != null) {

                    strComments = ((EditText) rootView.findViewById(R.id.edtAddComments)).getText().toString();
                    JSONObject jsonObject = new JSONObject();
                    try {
                        jsonObject.put(Constants.USER_ID, ((BaseActivity) mActivity).user.getUser_id());
                        jsonObject.put(Constants.AUTH_TOKEN, ((BaseActivity) mActivity).preferences.getString(Constants.KEY_TOKEN, ""));
                        jsonObject.put(Constants.ORDER, order.getOrder_number());
                        jsonObject.put("reason", selectedReason.getReason_id());
                        jsonObject.put("comments", strComments);

                    } catch (JSONException e) {
                        e.printStackTrace();
                    }

                    postJsonData(Urls.ORDER_CANCEL, jsonObject, Constants.TAG_ORDER_CANCEL, true);
                } else
                    showToast(getString(R.string.select_reason));
            }
        });

        if (orderAction == Constants.OrderAction.RETURN_EXCHANGE)
            rootView.findViewById(R.id.llTotal).setVisibility(View.GONE);


        JSONObject jsonObject = new JSONObject();
        try {
            jsonObject.put(Constants.USER_ID, ((BaseActivity) mActivity).user.getUser_id());
            jsonObject.put(Constants.AUTH_TOKEN, ((BaseActivity) mActivity).preferences.getString(Constants.KEY_TOKEN, ""));
            jsonObject.put(Constants.ORDER_ID, order.getOrder_number());
        } catch (JSONException e) {
            e.printStackTrace();
        }
        postJsonData(Urls.ORDER_VIEW, jsonObject, Constants.TAG_ORDER_VIEW, true);


        rootView.findViewById(R.id.txtvw_email).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                Intent emailIntent = new Intent(Intent.ACTION_SEND);

                emailIntent.setData(Uri.parse("mailto:"));
                emailIntent.setType("text/plain");
                emailIntent.putExtra(Intent.EXTRA_EMAIL, new String[]{"hello@20Dresses.com"});
                startActivity(Intent.createChooser(emailIntent, "Send mail..."));
                //finish();
            }
        });

        rootView.findViewById(R.id.txtvw_number).setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View view) {
                intent = new Intent(Intent.ACTION_CALL, Uri.parse("tel:" + "+919699822211"));

                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                    int hasCallPermission = mActivity.checkSelfPermission(Manifest.permission.CALL_PHONE);
                    if (hasCallPermission != PackageManager.PERMISSION_GRANTED) {
                        requestPermissions(new String[]{Manifest.permission.CALL_PHONE}, 1);
                        return;
                    } else
                        startActivity(intent);
                } else
                    startActivity(intent);
            }
        });

    }

    @Override
    public void onRequestPermissionsResult(int requestCode, String[] permissions, int[] grantResults) {
        // super.onRequestPermissionsResult(requestCode, permissions, grantResults);

        if (grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
            // Permission Granted
            startActivity(intent);
        }
        else
            showToast(getString(R.string.text_permission_denied));
    }

    void updateUI() {
        rootView.findViewById(R.id.rlOrderDetail).setVisibility(View.VISIBLE);
        getToolBar().setTitle("ORDER NO. " + orderItem.getOrder_number());

        if (orderItem.getShow_contact_details())
            rootView.findViewById(R.id.rlHelp).setVisibility(View.VISIBLE);
        else
            rootView.findViewById(R.id.rlHelp).setVisibility(View.GONE);

        ((TextView) rootView.findViewById(R.id.tvOrderNo)).setText("Order # " + orderItem.getOrder_number());

        ((TextView) rootView.findViewById(R.id.tvPaymentMode)).setText("Payment Mode : " + orderItem.getPaymentm_mode());

        ((TextView) rootView.findViewById(R.id.tvOrderDetails)).
                setText("Placed on: " + orderItem.getOrder_date() + " | Order Total: " + Constants.RUPEE +
                        Constants.formatAmount(orderItem.getOrder_total()));


        ((TextView) rootView.findViewById(R.id.tvProductTotal)).setText("Product Total: " + Constants.RUPEE +
                Constants.formatAmount(orderItem.getProduct_total()));

        String shippingText = "Shipping (+): " + (orderItem.getOrder_shipping_charges() == 0 ? "Free" : Constants.RUPEE + Constants.formatAmount(orderItem.getOrder_shipping_charges()));
        ((TextView) rootView.findViewById(R.id.tvShipping)).setText(shippingText);

        ((TextView) rootView.findViewById(R.id.tvOrderTotal)).setText("Order Total : " + Constants.RUPEE +
                Constants.formatAmount(orderItem.getOrder_total()));

        if (!orderItem.getOrder_wallet().equalsIgnoreCase("0")) {
            ((TextView) rootView.findViewById(R.id.tvWallet)).setVisibility(View.VISIBLE);
            ((TextView) rootView.findViewById(R.id.tvWallet)).setText("Wallet : " + Constants.RUPEE +
                    Constants.formatAmount(orderItem.getOrder_wallet()));

            double orderTotal = Double.parseDouble(orderItem.getOrder_total());
            double orderWallet = Double.parseDouble(orderItem.getOrder_wallet());
            ((TextView) rootView.findViewById(R.id.tvGrandTotal)).setVisibility(View.VISIBLE);
            ((TextView) rootView.findViewById(R.id.tvGrandTotal)).setText("Grand Total: " + Constants.RUPEE + Constants.formatAmount(orderTotal - orderWallet));
        }


        ((TextView) rootView.findViewById(R.id.tvAddress)).setText("Shipping Address : \n" + Html.fromHtml(orderItem.getDelivery_address(), null, new MyTagHandler()));


        ((FancyButton) rootView.findViewById(R.id.bConfirmed)).setText(orderItem.getOrder_status());

        adapter = new OrdersViewAdapter(mActivity,
                new ArrayList<>(orderItem.getOrder_items()), orderItem.isReturn_button(), orderAction);
        adapter.setOnReturnExchange(new OrdersViewAdapter.ReturnExchange() {
            @Override
            public void onReturnExchangeClick(String action, int itemPosition) {
                OrderViewFragment.this.itemPosition = itemPosition;
                if (action.equalsIgnoreCase("return")) {
                    JSONObject jsonObject = new JSONObject();
                    try {
                        jsonObject.put(Constants.USER_ID, ((BaseActivity) mActivity).user.getUser_id());
                        jsonObject.put(Constants.AUTH_TOKEN, ((BaseActivity) mActivity).preferences.getString(Constants.KEY_TOKEN, ""));
                        jsonObject.put(Constants.ORDER, orderItem.getOrder_number());
                        jsonObject.put(Constants.ITEM_ID, orderItem.getOrder_items().get(itemPosition).getItem_id());
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                    postJsonData(Urls.ORDER_RETURN_REASONS, jsonObject, Constants.TAG_ORDER_RETURN_REASONS, true);
                } else if (action.equalsIgnoreCase("exchange")) {
                    JSONObject jsonObject = new JSONObject();
                    try {
                        jsonObject.put(Constants.USER_ID, ((BaseActivity) mActivity).user.getUser_id());
                        jsonObject.put(Constants.AUTH_TOKEN, ((BaseActivity) mActivity).preferences.getString(Constants.KEY_TOKEN, ""));
                        jsonObject.put(Constants.ORDER, orderItem.getOrder_number());
                        jsonObject.put(Constants.ITEM_ID, orderItem.getOrder_items().get(itemPosition).getItem_id());
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                    postJsonData(Urls.ORDER_EXCHANGE_REASONS, jsonObject, Constants.TAG_ORDER_EXCHANGE_REASONS, true);
                }
            }

            @Override
            public void onClick(Object tag) {
                int i = 0;
                if (orderAction != null && orderAction == Constants.OrderAction.VIEW)
                    i = (int) tag;

                Constants.debug("i :" + i);
                Product product = new Product();
                /*if (CartOperations.getInstance().getCartList().size() > 0) {
                    for (Cart cart : CartOperations.getInstance().getCartList()) {
                        if (cart.getProduct_id().equalsIgnoreCase(orderItem.getOrder_items().get(i).getItem_product_id()))
                            product.setProduct_size(cart.getProduct_size());
                    }
                }*/

                product.setProduct_id(orderItem.getOrder_items().get(i).getItem_product_id());

                Category.SubCat subCat = new Category.SubCat();
                subCat.setId("");
                subCat.setName("");

                Intent intent = new Intent(mActivity.getApplicationContext(), ProductViewActivity.class);
                intent.putExtra(Constants.PRODUCT_NAME, product);
                intent.putExtra(Constants.CATEGORY, subCat);
                startActivity(intent);
            }

            @Override
            public void validationSuccessful(boolean isSuccessful) {
                ((ShowProceed) mActivity).showProceed(isSuccessful);
            }

        });

    }

    public boolean validateReturnExchange() {
        return adapter.validate();
    }

    public ReturnExchangeRequest getReturnExchangeRequest() {
        ReturnExchangeRequest returnExchangeRequest = adapter.getReturnExchangeRequest();
        returnExchangeRequest.setOrder(orderItem.getOrder_number());
        return returnExchangeRequest;
    }

    public OrderReturnResponse getReturnResponse() {
        return returnResponse;
    }

    @Override
    public <T> void onResponse(T obj, int tag) {
        super.onResponse(obj, tag);
        switch (tag) {
            case Constants.TAG_ORDER_VIEW:

                rootView.findViewById(R.id.rlOrderDetail).setVisibility(View.VISIBLE);
                orderItem = (OrderItem) obj;
                updateUI();

                if (orderAction == Constants.OrderAction.RETURN_EXCHANGE) {

                    JSONObject jsonObject = new JSONObject();
                    try {
                        jsonObject.put(Constants.USER_ID, ((BaseActivity) mActivity).user.getUser_id());
                        jsonObject.put(Constants.AUTH_TOKEN, ((BaseActivity) mActivity).preferences.getString(Constants.KEY_TOKEN, ""));
                        jsonObject.put(Constants.ORDER, orderItem.getOrder_number());
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                    postJsonData(Urls.ORDER_RETURN, jsonObject, Constants.TAG_ORDER_RETURN, true);
                }

                break;
            case Constants.TAG_ORDER_RETURN:
                returnResponse = (OrderReturnResponse) obj;
                for (int i = 0; i < returnResponse.getOrder_items().size(); i++) {
                    this.orderItem.getOrder_items().get(i).setItem_id(returnResponse.getOrder_items().get(i).getItem_id());
                    this.orderItem.getOrder_items().get(i).setItem_refund_amount(returnResponse.getOrder_items().get(i).getItem_refund_amount());
                }
                break;
            case Constants.TAG_ORDER_EXCHANGE_REASONS:
                reason = (Reason) obj;
                if (reason.getStatus().equalsIgnoreCase(Constants.RESPONSE_SUCCESS)) {
                    if (reason.getProduct_sizes().size() == 0) {
                        //showToast(reason.getProduct_exchange_meesage());
                        adapter.setReasons(null, itemPosition, reason.getProduct_exchange_meesage());
                        adapter.setSizes(null, itemPosition);
                    } else {
                        adapter.setReasons(reason.getReasons(), itemPosition, "");
                        adapter.setSizes(reason.getProduct_sizes(), itemPosition);
                    }
                } else
                    showToast(reason.getMessage());
                break;
            case Constants.TAG_ORDER_RETURN_REASONS:
                reason = (Reason) obj;
                if (reason.getStatus().equalsIgnoreCase(Constants.RESPONSE_SUCCESS))
                    adapter.setReasons(reason.getReasons(), itemPosition, "");
                else
                    showToast(reason.getMessage());
                break;
            case Constants.TAG_ORDER_CANCEL_REASONS:
                List<Reason.ReasonsEntity> reasons = new ArrayList<>();
                reasons.clear();
                reasons.addAll(Arrays.asList((Reason.ReasonsEntity[]) obj));
                setCancelReasons(reasons, 0, "");
                break;
            case Constants.TAG_ORDER_CANCEL:
                VisitorResponse visitorResponse = (VisitorResponse) obj;
                if (visitorResponse.getStatus().equalsIgnoreCase(Constants.RESPONSE_SUCCESS))
                    mActivity.finish();
                showToast(visitorResponse.getMessage());
                break;
        }
    }

    public void setCancelReasons(List<Reason.ReasonsEntity> reasons, int position, String strMsg) {

        if (reasons != null) {
            Reason.ReasonsEntity reasonsEntity = new Reason.ReasonsEntity();
            reasonsEntity.setReason_id("-1");
            reasonsEntity.setReason("Select Reason");
            reasons.add(0, reasonsEntity);
            ReasonsAdapter adapter = new ReasonsAdapter(getContext(), R.layout.layout_adapter_reasons, reasons);
           /* spinners.get(position).setAdapter(adapter);
            spinners.get(position).setVisibility(View.VISIBLE);
            textReasons.get(0).setText("Select Reason");
            textReasons.get(position).setVisibility(View.VISIBLE);*/
            Spinner spinner = (Spinner) rootView.findViewById(R.id.spReasons1);
            spinner.setAdapter(adapter);

            spinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
                @Override
                public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                    if (position == 0) {
                        selectedReason = null;
                        return;
                    }
                    selectedReason = (Reason.ReasonsEntity) parent.getItemAtPosition(position);
                }

                @Override
                public void onNothingSelected(AdapterView<?> parent) {

                }
            });

        }
    }

    private void getCancelReasons() {

        JSONObject jsonObject = new JSONObject();
        try {
            jsonObject.put(Constants.AUTH_TOKEN, ((BaseActivity) mActivity).preferences.getString(Constants.KEY_TOKEN, ""));
            jsonObject.put("reason_type", "cancel");

        } catch (JSONException e) {
            e.printStackTrace();
        }
        postJsonDataArray(Urls.ORDER_CANCEL_REASONS, jsonObject, Constants.TAG_ORDER_CANCEL_REASONS, true);
    }


    public interface ShowProceed {
        void showProceed(boolean isSuccessful);
    }

    private String getPaddedString(String string) {
        int pad = 16 - string.length();
        for (int i = 0; i < pad; i++) {
            string = " " + string;
        }
        return string;
    }

    private String getPaddedAmount(Double amount, boolean rupeeSymbol) {
        String result = rupeeSymbol ? Constants.RUPEE + Constants.formatAmount(amount) : "" + Constants.formatAmount(amount);
        int pad = 16 - result.length();
        for (int i = 0; i < pad; i++) {
            result = " " + result;
        }
        return result;
    }

    @Override
    public void onError(int error_code, int tag, Object object) {
        if (error_code == Constants.UNAUTHORIZED_CODE) {
            ((BaseActivity) mActivity).showErrorDialog(Constants.SESSION_EXPIRED_MSG, new Intent(mActivity, LoginActivity.class));
        } else if (error_code == Constants.SERVER_DOWN_CODE || error_code == Constants.SERVER_DOWN_CODE1) {
            ((BaseActivity) mActivity).showErrorDialog(Constants.SERVER_DOWN_MSG, null);
        }
    }
}
