package outletwise.com.twentydresses.view.activity;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.app.ActionBar;
import android.text.method.KeyListener;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.LinearLayout;
import android.widget.Spinner;
import android.widget.TextView;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

import outletwise.com.twentydresses.R;
import outletwise.com.twentydresses.controller.interfaces.OrderItemClick;
import outletwise.com.twentydresses.model.Address;
import outletwise.com.twentydresses.model.Order;
import outletwise.com.twentydresses.model.OrderReturnResponse;
import outletwise.com.twentydresses.model.PinCodeResponse;
import outletwise.com.twentydresses.model.Reason;
import outletwise.com.twentydresses.model.ReturnExchangeRequest;
import outletwise.com.twentydresses.model.VisitorResponse;
import outletwise.com.twentydresses.model.database.greenbot.User;
import outletwise.com.twentydresses.utilities.Constants;
import outletwise.com.twentydresses.utilities.Urls;
import outletwise.com.twentydresses.view.fragment.BaseFragment;
import outletwise.com.twentydresses.view.fragment.OrderListFragment;
import outletwise.com.twentydresses.view.fragment.OrderRefundFragment;
import outletwise.com.twentydresses.view.fragment.OrderReturnFragment;
import outletwise.com.twentydresses.view.fragment.OrderShipItFragment;
import outletwise.com.twentydresses.view.fragment.OrderViewFragment;
import outletwise.com.twentydresses.view.fragment.ReturnAddressFragment;

/**
 * Created by User-PC on 04-11-2015.
 * This class is use to view users purchase history.
 * User can cancel, return exchange order from here.
 */
public class MyOrdersActivity extends BaseActivity implements OrderItemClick, OrderViewFragment.ShowProceed, OrderReturnFragment.ReturnMode {
    private User user;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_my_orders);
        setUpToolBar(getString(R.string.toolbar_orders));
        final ActionBar ab = getSupportActionBar();
        ab.setDisplayHomeAsUpEnabled(true);

        findViewById(R.id.bProceed).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Fragment fragment = getmFragmentManager().getFragments().get(getmFragmentManager().getBackStackEntryCount() - 1);
                if (fragment instanceof OrderViewFragment) {
                    OrderViewFragment orderViewFragment = (OrderViewFragment) fragment;
                    if (orderViewFragment.validateReturnExchange()) {
                        ReturnExchangeRequest returnExchangeRequest = orderViewFragment.getReturnExchangeRequest();
                        returnExchangeRequest.setExchangeList(new ArrayList<>(returnExchangeRequest.getExchangeMap().values()));
                        returnExchangeRequest.setReturnList(new ArrayList<>(returnExchangeRequest.getReturnMap().values()));

                        Bundle b = new Bundle();
                        b.putParcelable(ReturnExchangeRequest.class.getSimpleName(), returnExchangeRequest);
                        b.putParcelable(OrderReturnResponse.class.getSimpleName(), orderViewFragment.getReturnResponse());
                        OrderReturnFragment newFragment = new OrderReturnFragment();
                        newFragment.setArguments(b);
                        replaceFragment(R.id.frame_container, newFragment, OrderReturnFragment.class.getSimpleName());
                        showProceed(false);
                    }
                }
                else if (fragment instanceof ReturnAddressFragment) {
                    ReturnAddressFragment returnAddressFragment = (ReturnAddressFragment) fragment;
                    if (returnAddressFragment.validate()) {
                        ReturnExchangeRequest returnExchangeRequest = returnAddressFragment.getReturnExchangeRequest();

                        //Set address type
                        if (returnExchangeRequest.getAddress().isDelhivery_address())
                            returnExchangeRequest.getAddress().setAddressType(Constants.AddressType.same);
                        else if (returnExchangeRequest.getAddress().isUser_addresses())
                            returnExchangeRequest.getAddress().setAddressType(Constants.AddressType.previous);
                        else if (returnExchangeRequest.getAddress().isNew_address())
                            returnExchangeRequest.getAddress().setAddressType(Constants.AddressType.new_address);

                        if (returnExchangeRequest.getReturnList().size() > 0) {
                            if (returnExchangeRequest.getAddress().isNew_address()) {
                                Address address = returnAddressFragment.getNewAddress();
                                if (address != null) {
                                    JSONObject jsonObject = new JSONObject();
                                    try {
                                        jsonObject.put(Constants.PINCODE, address.getPincode());
                                    } catch (JSONException e) {
                                        e.printStackTrace();
                                    }
                                    postJsonData(Urls.CHK_DEL_OPTS, jsonObject, Constants.TAG_CHK_DEL_OPTS, false);
                                }
                            } else {
                                migrateToRefund(returnAddressFragment);
                            }
                        } else {
                            returnExchangeRequest.setUser(String.valueOf(user.getUser_id()));
                            returnExchangeRequest.setAuth_token(preferences.getString(Constants.KEY_TOKEN, ""));
                            postJsonData(Urls.ORDER_RETURN_EXCHANGE, returnExchangeRequest, Constants.TAG_ORDER_RETURN_EXCHANGE, true);
                        }
                    }
                }
                else if (fragment instanceof OrderShipItFragment) {
                    OrderShipItFragment orderShipItFragment = (OrderShipItFragment) fragment;
                    Bundle b = new Bundle();
                    b.putParcelable(ReturnExchangeRequest.class.getSimpleName(), orderShipItFragment.getReturnExchangeRequest());
                    b.putParcelable(OrderReturnResponse.class.getSimpleName(), orderShipItFragment.getOrderReturnResponse());
                    OrderRefundFragment newFragment = new OrderRefundFragment();
                    newFragment.setArguments(b);
                    replaceFragment(R.id.frame_container, newFragment, OrderRefundFragment.class.getSimpleName());
                }
                else if (fragment instanceof OrderRefundFragment) {
                    OrderRefundFragment orderRefundFragment = (OrderRefundFragment) fragment;
                    if (orderRefundFragment.validate()) {
                        ReturnExchangeRequest returnExchangeRequest = orderRefundFragment.getReturnExchangeRequest();
                        //Set refund type
                        if (returnExchangeRequest.getRefund().isWallet()) {
                            returnExchangeRequest.getRefund().setRefundType(Constants.RefundType.wallet);
                            returnExchangeRequest.getRefund().setIs_new_bank_details(false);
                        }
                        else if (returnExchangeRequest.getRefund().isCredit()) {
                            returnExchangeRequest.getRefund().setRefundType(Constants.RefundType.credit);
                            returnExchangeRequest.getRefund().setIs_new_bank_details(false);
                        }

                        else if (returnExchangeRequest.getRefund().isCurrent_bank() || returnExchangeRequest.getRefund().isIs_new_bank_details()) {
                            if (returnExchangeRequest.getRefund().isCurrent_bank())
                                returnExchangeRequest.getRefund().setCurrent_bank(true);

                            if (returnExchangeRequest.getRefund().isIs_new_bank_details())
                                returnExchangeRequest.getRefund().setIs_new_bank_details(true);
                            returnExchangeRequest.getRefund().setRefundType(Constants.RefundType.bank_deposit);
                        }
                        if (returnExchangeRequest.getRefund().isIs_new_bank_details()) {
                            OrderReturnResponse.RefundEntity.BankDetailsEntity bankDetailsEntity = orderRefundFragment.getNewBankDetails();
                            if (bankDetailsEntity != null) {
                                returnExchangeRequest.getRefund().setBank_details(bankDetailsEntity);
                                returnExchangeRequest.setUser(String.valueOf(user.getUser_id()));
                                returnExchangeRequest.setAuth_token(preferences.getString(Constants.KEY_TOKEN, ""));
                                postJsonData(Urls.ORDER_RETURN_EXCHANGE, returnExchangeRequest, Constants.TAG_ORDER_RETURN_EXCHANGE, true);
                            }
                        } else {
                            returnExchangeRequest.setUser(String.valueOf(user.getUser_id()));
                            returnExchangeRequest.setAuth_token(preferences.getString(Constants.KEY_TOKEN, ""));
                            postJsonData(Urls.ORDER_RETURN_EXCHANGE, returnExchangeRequest, Constants.TAG_ORDER_RETURN_EXCHANGE, true);

                        }
                    }
                }
            }
        });

        findViewById(R.id.bCancel).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });
    }

    @Override
    public <T> void onResponse(T obj, int tag) {
        super.onResponse(obj, tag);
        switch (tag) {
            case Constants.TAG_CHK_DEL_OPTS:
                PinCodeResponse response = (PinCodeResponse) obj;

                if (Constants.pincodeTextchange) {
                    if (response.getStatus().equalsIgnoreCase(Constants.RESPONSE_SUCCESS)) {
                        LinearLayout llNewAddress = (LinearLayout) findViewById(R.id.llNewAddress);
                        TextView tvCity = ((TextView) llNewAddress.findViewById(R.id.tvCity));

                        if (response.getCity() != null && !response.getCity().equalsIgnoreCase(""))
                            tvCity.setText(response.getCity());
                        else {
                            tvCity.setText("");
                            tvCity.setKeyListener((KeyListener) tvCity.getTag());
                        }

                        ArrayAdapter<String> adapter = new ArrayAdapter<String>(this, R.layout.layout_spinner, response.getStates());
                        ((Spinner) findViewById(R.id.tvState)).setAdapter(adapter);
                        ((Spinner) findViewById(R.id.tvState)).setSelection(adapter.getPosition(response.getState()));
                        adapter.notifyDataSetChanged();
                        // ((TextView)llNewAddress.findViewById(R.id.tvState)).setText(response.getState());
                        if (response.getPickup_available()) {
                           /* Drawable img = getApplicationContext().getResources().getDrawable(R.drawable.pincode_valid);
                            EditText edtText = (EditText) findViewById(R.id.tvPinCode);
                            edtText.setCompoundDrawablesWithIntrinsicBounds(null, null, img, null);*/

                            showToast(getString(R.string.toast_pick_up));
                        } else {
                            //Drawable img = getApplicationContext().getResources().getDrawable(R.drawable.ic_cancel);
                           /* EditText edtText = (EditText) findViewById(R.id.tvPinCode);
                            edtText.setCompoundDrawablesWithIntrinsicBounds(null, null, null, null);*/
                            showToast(getString(R.string.toast_no_pick_up));
                        }

                        Constants.pincodeTextchange = false;
                    } else
                        showToast(response.getMsg());
                } else if (response.getPickup_available()) {
                    migrateToRefund((ReturnAddressFragment) getmFragmentManager().getFragments().
                            get(getmFragmentManager().getBackStackEntryCount() - 1));
                }

                break;

            case Constants.TAG_ORDER_CANCEL:
                VisitorResponse visitorResponse = (VisitorResponse) obj;
                if (visitorResponse.getStatus().equalsIgnoreCase(Constants.RESPONSE_SUCCESS))
                    finish();
                showToast(visitorResponse.getMessage());
                break;

            case Constants.TAG_ORDER_VIEW:
                break;
            case Constants.TAG_ORDER_RETURN_EXCHANGE:
                VisitorResponse returnExchangeResponse = (VisitorResponse) obj;
                if (returnExchangeResponse.getStatus().equalsIgnoreCase(Constants.RESPONSE_SUCCESS))
                    finish();
                showToast(returnExchangeResponse.getMessage());
                break;
        }
    }

    /*@Override
    public void onError(int error_code, int tag, Object object) {
        checkErrorCode(error_code, object);
    }*/

    void migrateToRefund(ReturnAddressFragment returnAddressFragment) {
        Bundle b = new Bundle();
        b.putParcelable(ReturnExchangeRequest.class.getSimpleName(), returnAddressFragment.getReturnExchangeRequest());
        b.putParcelable(OrderReturnResponse.class.getSimpleName(), returnAddressFragment.getOrderReturnResponse());
        OrderRefundFragment newFragment = new OrderRefundFragment();
        newFragment.setArguments(b);
        replaceFragment(R.id.frame_container, newFragment, OrderRefundFragment.class.getSimpleName());
    }

    @Override
    public void onCompleteRetrieval(int tag, ArrayList obj, String msg) {
        super.onCompleteRetrieval(tag, obj, msg);
        switch (tag) {
            case Constants.TAG_USER:
                if (!isFinishing()) {
                    if (obj != null && obj.size() > 0)
                        user = (User) obj.get(0);
                    if (user != null &&  String.valueOf(user.getUser_id()) != null) {
                        Bundle b = new Bundle();
                        b.putString("user_id", "" + user.getUser_id());
                        Fragment fragment = new OrderListFragment();
                        fragment.setArguments(b);
                        replaceFragment(R.id.frame_container, fragment, OrderListFragment.class.getSimpleName());

                    }
                }
                break;
        }
    }


    @Override
    public void onItemClicked(Order order, Constants.OrderAction orderAction, Reason.ReasonsEntity reason) {

        Constants.ORDER_ID_RETURN = order.getOrder_number();

        if (orderAction == Constants.OrderAction.RETURN_EXCHANGE || orderAction == Constants.OrderAction.VIEW) {
            Bundle bundle = new Bundle();
            bundle.putParcelable(Constants.ORDER, order);
            bundle.putSerializable(Constants.OrderAction.class.getSimpleName(), orderAction);
            BaseFragment fragment = new OrderViewFragment();
            fragment.setArguments(bundle);
            replaceFragment(R.id.frame_container, fragment, OrderViewFragment.class.getSimpleName());

            if (orderAction == Constants.OrderAction.RETURN_EXCHANGE)
                findViewById(R.id.llProceed).setVisibility(View.VISIBLE);
            else
                findViewById(R.id.llProceed).setVisibility(View.GONE);

        } else {
           /* JSONObject jsonObject = new JSONObject();
            try {
                jsonObject.put(Constants.USER_ID, user.getUser_id());
                jsonObject.put(Constants.AUTH_TOKEN, preferences.getString(Constants.KEY_TOKEN, ""));
                jsonObject.put(Constants.ORDER, order.getOrder_number());
                jsonObject.put("reason", reason.getReason_id());
                jsonObject.put("comments", reason.getReason());

            } catch (JSONException e) {
                e.printStackTrace();
            }

            postJsonData(Urls.ORDER_CANCEL, jsonObject, Constants.TAG_ORDER_CANCEL, true);*/

            Bundle bundle = new Bundle();
            bundle.putParcelable(Constants.ORDER, order);
            bundle.putSerializable(Constants.OrderAction.class.getSimpleName(), orderAction);
            BaseFragment fragment = new OrderViewFragment();
            fragment.setArguments(bundle);
            replaceFragment(R.id.frame_container, fragment, OrderViewFragment.class.getSimpleName());

            if (orderAction == Constants.OrderAction.RETURN_EXCHANGE)
                findViewById(R.id.llProceed).setVisibility(View.VISIBLE);
            else
                findViewById(R.id.llProceed).setVisibility(View.GONE);
        }
    }

    @Override
    public void showProceed(boolean isSuccessful) {
        if (isSuccessful)
            findViewById(R.id.llProceed).setVisibility(View.VISIBLE);
        else
            findViewById(R.id.llProceed).setVisibility(View.GONE);
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        try {
            if (getmFragmentManager().getBackStackEntryCount() < 3 && getmFragmentManager().getBackStackEntryAt(0).getName().equals("OrderListFragment"))
                findViewById(R.id.llProceed).setVisibility(View.GONE);
            else
                findViewById(R.id.llProceed).setVisibility(View.VISIBLE);

            if (getmFragmentManager().getBackStackEntryCount() == 4 && getmFragmentManager().getBackStackEntryAt(2).getName().equals("OrderReturnFragment"))
                findViewById(R.id.llProceed).setVisibility(View.GONE);

        } catch (NullPointerException ex) {
            ex.printStackTrace();
        }

    }


    @Override
    public void onReturnModeSelected(String mode) {
        OrderReturnFragment orderReturnFragment = (OrderReturnFragment) getmFragmentManager().getFragments().get(getmFragmentManager().getBackStackEntryCount() - 1);
        ReturnExchangeRequest returnExchangeRequest = orderReturnFragment.getReturnExchangeRequest();
        ReturnExchangeRequest.AddressEntity addressEntity = new ReturnExchangeRequest.AddressEntity();
        BaseFragment newFragment;

        if (mode.equalsIgnoreCase(getString(R.string.button_ship))) {
            addressEntity.setShip_it_urself(true);
            newFragment = new OrderShipItFragment();
        } else {
            addressEntity.setShip_it_urself(false);
            newFragment = new ReturnAddressFragment();
        }

        returnExchangeRequest.setAddress(addressEntity);
        Bundle b = new Bundle();
        b.putParcelable(ReturnExchangeRequest.AddressEntity.class.getSimpleName(), addressEntity);
        b.putParcelable(ReturnExchangeRequest.class.getSimpleName(), returnExchangeRequest);
        b.putParcelable(OrderReturnResponse.class.getSimpleName(), orderReturnFragment.getOrderReturnResponse());
        newFragment.setArguments(b);
        replaceFragment(R.id.frame_container, newFragment, newFragment.getClass().getSimpleName());
        showProceed(true);
    }
}
