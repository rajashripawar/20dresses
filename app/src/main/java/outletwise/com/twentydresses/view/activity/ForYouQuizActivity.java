package outletwise.com.twentydresses.view.activity;

import android.content.Intent;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.text.Html;
import android.view.View;
import android.widget.ImageView;
import android.widget.TableLayout;
import android.widget.TableRow;
import android.widget.TextView;

import com.squareup.picasso.Picasso;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;

import outletwise.com.twentydresses.R;
import outletwise.com.twentydresses.controller.interfaces.StyleProfileChanged;
import outletwise.com.twentydresses.model.Category;
import outletwise.com.twentydresses.model.ForyouQuizResponse;
import outletwise.com.twentydresses.model.UpdateStyleProfile;
import outletwise.com.twentydresses.model.UserAttrEntity;
import outletwise.com.twentydresses.model.VisitorResponse;
import outletwise.com.twentydresses.model.database.greenbot.User;
import outletwise.com.twentydresses.utilities.Constants;
import outletwise.com.twentydresses.utilities.Urls;

public class ForYouQuizActivity extends BaseActivity {
    private String qn_id;
    private String tip_id;
    private String tip_text;
    private int tip_total;
    private View previousView;
    private UserAttrEntity userAttr;
    private boolean blnFromProf;
    private HashMap<String, View> bodyShapes = new HashMap<>();
    private boolean isChange = false;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_for_you_quiz);

        setUpToolBar(getString(R.string.toolbar_quiz));
        final ActionBar ab = getSupportActionBar();
        ab.setDisplayHomeAsUpEnabled(true);

        blnFromProf = getIntent().getBooleanExtra("fromProfile", false);
        qn_id = getIntent().getStringExtra("que_id");

        userAttr = getIntent().getParcelableExtra(UserAttrEntity.class.getSimpleName());

        if (!blnFromProf) {

            tip_id = getIntent().getStringExtra("tip_id");
            tip_text = getIntent().getStringExtra("tip_text");
            tip_total = getIntent().getIntExtra("tip_total", 0);
        }


        getQnData();


    }

    private void getQnData() {

        JSONObject jsonObject = new JSONObject();
        try {
            jsonObject.put(Constants.AUTH_TOKEN, preferences.getString(Constants.KEY_TOKEN, ""));
            jsonObject.put("que_id", qn_id);
        } catch (JSONException e) {
            e.printStackTrace();
        } catch (Exception ex) {
            ex.printStackTrace();
        }

        postJsonData(Urls.FOR_YOU_QUIZ, jsonObject, Constants.TAG_FOR_YOU_QUIZ, true);
    }

    @Override
    public <T> void onResponse(T obj, int tag) {
        super.onResponse(obj, tag);
        switch (tag) {
            case Constants.TAG_FOR_YOU_QUIZ:
                ForyouQuizResponse response = (ForyouQuizResponse) obj;
                if (response.getStatus().equalsIgnoreCase(Constants.RESPONSE_SUCCESS)) {
                    if (response.getAnswer_type().equalsIgnoreCase("Image")) {

                        ((TextView) findViewById(R.id.tv_qn)).setText(Html.fromHtml(response.getQuestion()));

                        TableLayout tablelayout = (TableLayout) findViewById(R.id.tlBodyShapes);
                        int k = 0;
                        for (int i = 0; i < tablelayout.getChildCount(); i++) {
                            TableRow row = (TableRow) tablelayout.getChildAt(i);
                            for (int j = 0; j < row.getChildCount(); j++) {
                                View v = row.getChildAt(j);
                                ImageView ivBodyShape = (ImageView) v.findViewById(R.id.ivBodyShape);
                                Picasso.with(getApplicationContext()).load(response.getAnswers().get(k).getImage()).into(ivBodyShape);

                                ((TextView) v.findViewById(R.id.tvBodyShape)).setText(response.getAnswers().get(k).getImage_description());

                                v.setTag(response.getAnswers().get(k).getImage_description());
                                v.findViewById(R.id.ivBodyShape).setOnClickListener(this);
                                bodyShapes.put(response.getAnswers().get(k).getImage_description(), v);

                                k++;
                            }
                        }


                        if (userAttr != null) {
                            View view = bodyShapes.get(userAttr.getFace_Shape());
                            resetViews(view);
                        }
                    }
                }
                break;
            case Constants.TAG_UPDATE_STYLE_PROFILE:
                // findViewById(R.id.pbWheelProfile).setVisibility(View.GONE);
                VisitorResponse response1 = (VisitorResponse) obj;
                if (response1.getStatus().equalsIgnoreCase(Constants.RESPONSE_SUCCESS)) {

                    if (blnFromProf) {
                        finish();
                    } else {

                        finish();

                        Category.SubCat subCat = new Category.SubCat();
                        subCat.setId(tip_id);
                        subCat.setName("FOR YOU");
                        Intent intent = new Intent(getApplicationContext(), ProductsActivity.class);
                        intent.putExtra(Constants.CATEGORY, subCat);
                        Constants.TIP_NAME = tip_text;
                        Constants.TIP_TOTAL = tip_total;
                        startActivity(intent);
                    }
                } else
                    showToast(response1.getMessage());
                break;
        }
    }

    @Override
    public void onClick(View view) {
        findViewById(R.id.pbWheel).setVisibility(View.VISIBLE);
        view = (View) view.getParent();
        resetViews(view);
        userAttr = new UserAttrEntity();
        userAttr.setFace_Shape((String) view.getTag());
        isChange = true;

        selectRecords(Constants.TAG_USER);


    }

    void resetViews(View v) {
        if (v != null) {
            if (previousView != null) {
                previousView.findViewById(R.id.ivTick).setVisibility(View.INVISIBLE);
                previousView.findViewById(R.id.tvBodyShape).setSelected(false);
                previousView.findViewById(R.id.ivBodyShape).setSelected(false);
            }
            previousView = v;
            v.findViewById(R.id.ivTick).setVisibility(View.VISIBLE);
            v.findViewById(R.id.tvBodyShape).setSelected(true);
            v.findViewById(R.id.ivBodyShape).setSelected(true);
        }
    }


    @Override
    public void onCompleteRetrieval(int tag, ArrayList obj, String msg) {
        switch (tag) {
            case Constants.TAG_USER:
                if (obj != null && obj.size() > 0) {
                    user = (User) obj.get(0);
                    if (isChange) {
                        isChange = false;
                        UpdateStyleProfile styleProfile = new UpdateStyleProfile();
                        styleProfile.setUser_attr(userAttr);
                        // styleProfile.setStyle_attr(styleAttrEntity);
                        styleProfile.setUserId(user.getUser_id() + "");
                        styleProfile.setAuth_token(preferences.getString(Constants.KEY_TOKEN, ""));

                        postJsonData(Urls.UPDATE_STYLE_PROFILE, styleProfile, Constants.TAG_UPDATE_STYLE_PROFILE, true);
                    }
                }
                break;
        }
    }

}
