package outletwise.com.twentydresses.view.activity;

import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.support.v7.app.ActionBar;
import android.text.Html;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.squareup.picasso.Picasso;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

import outletwise.com.twentydresses.R;
import outletwise.com.twentydresses.controller.MyTagHandler;
import outletwise.com.twentydresses.model.ReferCodeResponse;
import outletwise.com.twentydresses.model.database.greenbot.User;
import outletwise.com.twentydresses.utilities.Constants;
import outletwise.com.twentydresses.utilities.Urls;

/**
 * Created by User-PC on 28-12-2015.
 */
public class ReferralActivity extends BaseActivity {

    private User user;
    private String appLink;
    private String msg;
    private String fbMsg;
    private TextView tvRefereCode;
    private ReferCodeResponse response;
    private TextView tvDesc;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_referral);
        trackPage(getString(R.string.pgInvite));
        setUpToolBar(getString(R.string.toolbar_invite));
        final ActionBar ab = getSupportActionBar();
        ab.setDisplayHomeAsUpEnabled(true);
      //  selectRecords(Constants.TAG_USER);

        //appLink = "https://play.google.com/store/apps/details?id=" + getApplicationContext().getPackageName();
        appLink = "http://tinyurl.com/zrqpw9k";
        msg = "Checkout this exciting online and fashion styling destination for women - " + appLink + " \nAll you got to do is fill out a fun fashion quiz and register. Don't forget to use the code ";
        fbMsg = "https://www.facebook.com/sharer/sharer.php?u=https%3A//tinyurl.com/zrqpw9k";

    }


    void init() {
        findViewById(R.id.tvWhatsApp).setOnClickListener(this);
        findViewById(R.id.tvFacebook).setOnClickListener(this);
        findViewById(R.id.tvMessaging).setOnClickListener(this);
        findViewById(R.id.tvMail).setOnClickListener(this);
        findViewById(R.id.tvSeeMore).setOnClickListener(this);
        tvRefereCode = (TextView) findViewById(R.id.txtvw_refer_code);
        tvDesc = (TextView) findViewById(R.id.tvDesc);

        //set user profile
        ImageView img = (ImageView) findViewById(R.id.ivBackground);

        if (user.getUser_picture() != null && !user.getUser_picture().equalsIgnoreCase(""))
            Picasso.with(this).load(user.getUser_picture()).into(img);

        getReferralCode();
    }

    private void getReferralCode() {

        JSONObject jsonObject = new JSONObject();
        try {
            jsonObject.put(Constants.AUTH_TOKEN, preferences.getString(Constants.KEY_TOKEN, ""));
            jsonObject.put(Constants.USER_ID, user.getUser_id() + "");
        } catch (JSONException e) {
            e.printStackTrace();
        }
        postJsonData(Urls.REFER, jsonObject, Constants.TAG_REFER, true);
    }


    @Override
    public void onClick(View v) {
        super.onClick(v);
        switch (v.getId()) {
            case R.id.tvWhatsApp:
                try {
                    Intent sendIntent = new Intent();
                    sendIntent.setAction(Intent.ACTION_SEND);
                    sendIntent.putExtra(Intent.EXTRA_TEXT, msg);
                    sendIntent.setType("text/plain");
                    sendIntent.setPackage("com.whatsapp");
                    startActivity(sendIntent);
                } catch (Exception e) {
                    e.printStackTrace();
                }
                break;

            case R.id.tvFacebook:
                try {
                    Intent myWebLink = new Intent(android.content.Intent.ACTION_VIEW);
                    myWebLink.setData(Uri.parse(fbMsg));
                    startActivity(myWebLink);
                  /*  Intent intent = new Intent("android.intent.category.LAUNCHER");
                    intent.setClassName("com.facebook.katana", "com.facebook.katana.LoginActivity");
                    startActivity(intent);*/
                } catch (Exception e) {
                    e.printStackTrace();
                }
                break;

            case R.id.tvSeeMore:
                try {
                    Intent sendIntent = new Intent();
                    sendIntent.setAction(Intent.ACTION_SEND);
                    sendIntent.putExtra(Intent.EXTRA_TEXT, msg);
                    sendIntent.setType("text/plain");
                    startActivity(Intent.createChooser(sendIntent, "Refer Friends"));
                } catch (Exception e) {
                    e.printStackTrace();
                }
                break;
            case R.id.tvMail:
                try {
                    Intent emailIntent = new Intent(android.content.Intent.ACTION_SEND);
                    emailIntent.setType("plain/text");
                    emailIntent.putExtra(android.content.Intent.EXTRA_SUBJECT, "20Dresses App");
                    emailIntent.putExtra(android.content.Intent.EXTRA_TEXT, msg);
                    startActivity(Intent.createChooser(emailIntent, "Share..."));
                } catch (Exception e) {
                    e.printStackTrace();
                }
                break;
            case R.id.tvMessaging:
                try {
                   /* for earlier versions
                    if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.KITKAT) //At least KitKat
                    {
                        String defaultSmsPackageName = Telephony.Sms.getDefaultSmsPackage(activity); //Need to change the build to API 19

                        Intent sendIntent = new Intent(Intent.ACTION_SEND);
                        sendIntent.setType("text/plain");
                        sendIntent.putExtra(Intent.EXTRA_TEXT, smsText);

                        if (defaultSmsPackageName != null)//Can be null in case that there is no default, then the user would be able to choose any app that support this intent.
                        {
                            sendIntent.setPackage(defaultSmsPackageName);
                        }
                        activity.startActivity(sendIntent);

                    }
                    else //For early versions, do what worked for you before.
                    {
                        Intent sendIntent = new Intent(Intent.ACTION_VIEW);
                        sendIntent.setData(Uri.parse("sms:"));
                        sendIntent.putExtra("sms_body", smsText);
                        activity.startActivity(sendIntent);
                    }*/

                    Intent sendIntent = new Intent(Intent.ACTION_VIEW);
                    sendIntent.putExtra("sms_body", msg);
                    sendIntent.setType("vnd.android-dir/mms-sms");
                    startActivity(sendIntent);
                } catch (Exception e) {
                    e.printStackTrace();
                }
                break;

        }
    }


    @Override
    public void onCompleteRetrieval(int tag, ArrayList obj, String msg) {
        switch (tag) {
            case Constants.TAG_USER:
                if (obj != null && obj.size() > 0)
                    user = (User) obj.get(0);

                init();
                break;
        }
    }

    @Override
    public <T> void onResponse(T obj, int tag) {
        super.onResponse(obj, tag);
        switch (tag) {
            case Constants.TAG_REFER:
                findViewById(R.id.pbWheel).setVisibility(View.GONE);
                findViewById(R.id.scrollView).setVisibility(View.VISIBLE);
                response = (ReferCodeResponse) obj;
                tvRefereCode.setText(response.getRefer_code());
                tvDesc.setText(Html.fromHtml(String.valueOf(response.getDescription()), null, new MyTagHandler()));
                msg = msg + response.getRefer_code();
                break;
        }
    }
}
