package outletwise.com.twentydresses.view.custom;

import android.app.DatePickerDialog;
import android.content.Context;
import android.content.res.Resources;
import android.view.View;

/**
 * Created by User-PC on 19-11-2015.
 */
public class CustomDatePicker extends DatePickerDialog {

    public CustomDatePicker(Context context, OnDateSetListener callBack, int year, int monthOfYear) {
        super(context, callBack, year, monthOfYear, 0);
        hideDate();
        setTitle("Card Expiry");
    }

    public CustomDatePicker(Context context, int theme, OnDateSetListener listener, int year, int monthOfYear) {
        super(context, theme, listener, year, monthOfYear, 0);
        hideDate();
        setTitle("Card Expiry");
    }

    void hideDate() {
        try
        {
            getDatePicker().findViewById(Resources.getSystem().getIdentifier("day", "id", "android")).setVisibility(View.GONE);
        }
        catch (NullPointerException ex){}
    }
}
