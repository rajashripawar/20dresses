package outletwise.com.twentydresses.view.activity;

import android.content.Intent;
import android.os.Build;
import android.os.Bundle;
import android.support.v7.app.ActionBar;
import android.text.Html;
import android.text.TextUtils;
import android.view.Gravity;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.mikepenz.actionitembadge.library.ActionItemBadge;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

import outletwise.com.twentydresses.R;
import outletwise.com.twentydresses.controller.CartOperations;
import outletwise.com.twentydresses.controller.MyTagHandler;
import outletwise.com.twentydresses.controller.adapter.CartDiscountAdapter;
import outletwise.com.twentydresses.controller.adapter.CartItemsAdapter;
import outletwise.com.twentydresses.model.AddCartResponse;
import outletwise.com.twentydresses.model.CartDeleteItemResponse;
import outletwise.com.twentydresses.model.Product;
import outletwise.com.twentydresses.model.SyncCart;
import outletwise.com.twentydresses.model.database.greenbot.Cart;
import outletwise.com.twentydresses.model.database.greenbot.User;
import outletwise.com.twentydresses.utilities.Constants;
import outletwise.com.twentydresses.utilities.Urls;

/**
 * Created by User-PC on 29-09-2015.
 * Class is used to display cart items with prices & handled multiple cart operations
 * like delete, update quantity of cart items.
 */
public class CartActivity extends BaseActivity {
    private CartItemsAdapter cartItemsAdapter;
    private CartOperations cartOperations;
    private MenuItem actionCart;
    private CartActivity cartActivity;
    private SyncCart syncCart;
    private boolean isOfferApplied, isCreditApplied, isWalletApplied, isGiftWrap;
    private ArrayList<Cart> cartList;
    private CartDiscountAdapter cartDiscountAdapter;
    private User user;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_cart);
        trackPage(getString(R.string.pgCart));
        init();
        setUpToolBar(getString(R.string.toolbar_cart));
        final ActionBar ab = getSupportActionBar();
        ab.setDisplayHomeAsUpEnabled(true);

        cartOperations.setCartUpdation(new CartOperations.CartUpdation() {
            @Override
            public void onCartUpdate() {
                updateCartUI();
            }
        });

        findViewById(R.id.tvProceed).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (cartItemsAdapter.getOutOfStockItems().size() == 0) {
                    Intent intent = new Intent(getApplicationContext(), SelectAddressActivity.class);
                    intent.putExtra(Constants.GRAND_TOTAL, syncCart.getCart_values().getGrand_total());
                    intent.putExtra(Constants.APPLY_OFFERS, isOfferApplied);
                    intent.putExtra(Constants.APPLY_WALLET, isWalletApplied);
                    intent.putExtra(Constants.APPLY_CREDITS, isCreditApplied);
                    intent.putExtra(Constants.GIFT_WRAP, isGiftWrap);
                    startActivity(intent);
                } else {
                    showToast(getString(R.string.toast_remove_out_of_stock_products));
                }
            }
        });


        findViewById(R.id.tvGotoWishlist).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Constants.GOTO_WISHLIST = true;
                finish();
                startActivity(new Intent(getApplicationContext(), HomeActivity.class));
            }
        });
    }

    private void init() {
        findViewById(R.id.llCheckout).bringToFront();
        cartActivity = this;
        cartOperations = CartOperations.getInstance();
        ((TextView) findViewById(R.id.tvItems)).setText("Items (" + cartOperations.getNoOfItems() + ")");
    }

    private void updateCartUI() {
//        cartItemsAdapter.notifyDataSetChanged();
        ActionItemBadge.update(cartActivity, actionCart,
                getResources().getDrawable(R.drawable.ic_cart_selected), ActionItemBadge.BadgeStyles.RED,
                cartOperations.getCartSize());

        if (cartOperations.getCartList().size() == 0) {
            findViewById(R.id.llCheckout).setVisibility(View.GONE);
            findViewById(R.id.svCart).setVisibility(View.GONE);
            findViewById(R.id.tvCartEmpty).setVisibility(View.GONE);
            findViewById(R.id.rlNoCartItems).setVisibility(View.VISIBLE);
            return;
        } else {
            findViewById(R.id.svCart).setVisibility(View.VISIBLE);
            findViewById(R.id.llCheckout).setVisibility(View.VISIBLE);
            findViewById(R.id.tvCartEmpty).setVisibility(View.GONE);
            findViewById(R.id.rlNoCartItems).setVisibility(View.GONE);
        }

        ((TextView) findViewById(R.id.tvItems)).setText("Items (" + cartOperations.getNoOfItems() + ")");
        if (syncCart != null && syncCart.getCart_message() != null && TextUtils.isEmpty(syncCart.getCart_message()))
            findViewById(R.id.tvOffers).setVisibility(View.GONE);
        else if (syncCart != null && syncCart.getCart_message() != null)
            ((TextView) findViewById(R.id.tvOffers)).setText(syncCart.getCart_message());

        ((TextView) findViewById(R.id.tvFinalAmount)).setText(Constants.RUPEE + " " + Constants.formatAmount(syncCart.getCart_values().getGrand_total()));
        ((TextView) findViewById(R.id.tvOrderTotal)).setText("Order Total: " +
                getPaddedAmount(syncCart.getCart_values().getSub_total(), true));

       /* final ExpandableLayout elCredits = (ExpandableLayout) findViewById(R.id.elCredits);
        final ExpandableLayout elOffers = (ExpandableLayout) findViewById(R.id.elOffers);*/

        if (syncCart.isCredit_applicable()) {
            ((TextView) findViewById(R.id.tvExpandableHeaderCredits)).setText("Redeemable Credits: " + syncCart.getRedeemable_credits());
            findViewById(R.id.cbCredits).setEnabled(true);
        } else {
            //elCredits.getHeaderLayout().setClickable(false);
            findViewById(R.id.cbCredits).setEnabled(false);

           /* TextView textView = ((TextView) elCredits.findViewById(R.id.tvExpandableHeader));
            *//*Typeface typeface = TypefaceUtils.load(getAssets(), "fonts/PrimaryFont_Proxima.ttf");
            textView.setTypeface(typeface);*//*
            textView.setTextColor(getResources().getColor(R.color.material_grey_600));*/
        }

        if (syncCart.isOffer_applicable()) {
            ((TextView) findViewById(R.id.tvExpandableHeaderOffer)).setText("Offer: " + syncCart.getCart_offers().get(0).getOffer());
            ((TextView) findViewById(R.id.tvExpandableContentOffer)).setText(syncCart.getOffer_text());

            findViewById(R.id.cbOffers).setEnabled(true);
        } else {
            // elOffers.getHeaderLayout().setClickable(false);
            findViewById(R.id.cbOffers).setEnabled(false);
            TextView textView = ((TextView) findViewById(R.id.tvExpandableHeaderOffer));
            /*Typeface typeface = TypefaceUtils.load(getAssets(), "fonts/PrimaryFont_Proxima.ttf");
            textView.setTypeface(typeface);*/
            //  textView.setTextColor(getResources().getColor(R.color.material_grey_600));
            if (syncCart.getCart_offers() != null && syncCart.getCart_offers().size() > 0)
                textView.setText(syncCart.getCart_offers().get(0).getOffer());
        }

        if (syncCart.getCart_values().getShipping_charges() != 0)
            findViewById(R.id.tvShippingMsg).setVisibility(View.VISIBLE);
        else
            findViewById(R.id.tvShippingMsg).setVisibility(View.GONE);

        //Set Amount values
        ((TextView) findViewById(R.id.tvSubTotal)).setText("Product Total   :" + getPaddedAmount(syncCart.getCart_values().getProduct_total(), true));
        ((TextView) findViewById(R.id.tvGiftWrap)).setText("Gift wrap (+):" + getPaddedAmount(25d, false));
        String shippingText = "Shipping Charges (+):" + (syncCart.getCart_values().getShipping_charges() == 0 ? getPaddedString("Free") : getPaddedAmount(syncCart.getCart_values().getShipping_charges(), true));
        ((TextView) findViewById(R.id.tvShipping)).setText(shippingText);
        ((TextView) findViewById(R.id.tvSubTotal2)).setText(getPaddedAmount(syncCart.getCart_values().getSub_total(), true));

        /********** Set Savings values ************/
        setCartDiscountAdapter();
        /**********End Set Savings values ************/
        ((TextView) findViewById(R.id.tvSubTotal3)).setText(getPaddedAmount(syncCart.getCart_values().getOrder_total(), true));
        if (!syncCart.getWallet_amount().equalsIgnoreCase("0")) {
            findViewById(R.id.cvRedeemWallet).setVisibility(View.VISIBLE);
            double walletAmt = Double.parseDouble(syncCart.getWallet_amount()) - syncCart.getCart_values().getWallet_applied();
            ((TextView) findViewById(R.id.tvRedeemWallet)).setText("Pay with Wallet (" + Constants.RUPEE + Constants.formatAmount(walletAmt) + " balance)");
        } else
            findViewById(R.id.cvRedeemWallet).setVisibility(View.GONE);


        ((TextView) findViewById(R.id.tvWallet)).setText("Paid with wallet (-):" +
                getPaddedAmount(syncCart.getCart_values().getWallet_applied(), false));
        ((TextView) findViewById(R.id.tvFinalTotal)).setText(getPaddedAmount(syncCart.getCart_values().getGrand_total(), true));
        ((TextView) findViewById(R.id.tvExpandableContentCredits)).setText(Html.fromHtml(syncCart.getCredit_text(), null, new MyTagHandler()));
        ((TextView) findViewById(R.id.tvExpandableContentOffer)).setText(Html.fromHtml(syncCart.getOffer_text(), null, new MyTagHandler()));

        //CheckBox listeners
        ((CheckBox) findViewById(R.id.cbGiftWrap)).setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                if (isChecked)
                    findViewById(R.id.tvGiftWrap).setVisibility(View.VISIBLE);
                else
                    findViewById(R.id.tvGiftWrap).setVisibility(View.INVISIBLE);
                isGiftWrap = isChecked;
                syncCart();
            }
        });


        findViewById(R.id.cbCredits).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                ImageView img = (ImageView) findViewById(R.id.cbCredits);
                if (img.isEnabled() && img.getTag().equals("uncheck")) {
                    isCreditApplied = true;
                    img.setTag("check");
                    img.setImageDrawable(getResources().getDrawable(R.drawable.check));
                } else {
                    isCreditApplied = false;
                    img.setTag("uncheck");
                    img.setImageDrawable(getResources().getDrawable(R.drawable.uncheck));
                }
                syncCart();
            }
        });


        findViewById(R.id.cbOffers).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                ImageView img = (ImageView) findViewById(R.id.cbOffers);
                if (img.isEnabled() && img.getTag().equals("uncheck")) {
                    isOfferApplied = true;
                    img.setTag("check");
                    img.setImageDrawable(getResources().getDrawable(R.drawable.check));
                } else {
                    isOfferApplied = false;
                    img.setTag("uncheck");
                    img.setImageDrawable(getResources().getDrawable(R.drawable.uncheck));
                }
                syncCart();
            }
        });

       /* ((CheckBox) elCredits.findViewById(R.id.cbCredits)).setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                isCreditApplied = isChecked;
                if (isChecked)
                    elCredits.show();
                else
                    elCredits.hide();

                syncCart();
            }
        });*/

      /*  ((CheckBox) elOffers.findViewById(R.id.cbCredits)).setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                isOfferApplied = isChecked;
                if (isChecked)
                    elOffers.show();
                else
                    elOffers.hide();
                syncCart();
            }
        });*/


        findViewById(R.id.cbRedeemWallet).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                ImageView img = (ImageView) findViewById(R.id.cbRedeemWallet);
                if (img.isEnabled() && img.getTag().equals("uncheck")) {
                    findViewById(R.id.tvWallet).setVisibility(View.VISIBLE);
                    img.setTag("check");
                    img.setImageDrawable(getResources().getDrawable(R.drawable.check));
                    isWalletApplied = true;
                } else {
                    findViewById(R.id.tvWallet).setVisibility(View.INVISIBLE);
                    ((TextView) findViewById(R.id.tvWallet)).setText("Redeem Wallet");
                    img.setTag("uncheck");
                    img.setImageDrawable(getResources().getDrawable(R.drawable.uncheck));
                    isWalletApplied = false;
                }

                syncCart();
            }
        });

        /* final CheckBox cbWallet = ((CheckBox) findViewById(R.id.cbRedeemWallet));
        cbWallet.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                if (isChecked) {
                    findViewById(R.id.tvWallet).setVisibility(View.VISIBLE);
                } else {
                    findViewById(R.id.tvWallet).setVisibility(View.INVISIBLE);
                }
                isWalletApplied = isChecked;
                syncCart();
            }
        });*/

        findViewById(R.id.tvRedeemWallet).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                // cbWallet.toggle();
            }
        });
    }


    private void setCartDiscountAdapter() {

        ArrayList<SyncCart.CartValuesEntity.CartDiscountEntity> cartDiscount = (ArrayList<SyncCart.CartValuesEntity.CartDiscountEntity>) syncCart.getCart_values().getCart_discount();
        LinearLayout linearlayout = (LinearLayout) findViewById(R.id.llCartDiscount);

        if (linearlayout != null && linearlayout.getChildCount() > 0)
            linearlayout.removeAllViews();

        for (int i = 0; i < cartDiscount.size(); i++) {
            TextView tv = new TextView(this);
            tv.setText(cartDiscount.get(i).getDiscount_name() + " (-):" +
                    getPaddedAmount(cartDiscount.get(i).getDiscount_value(), true));
            tv.setTextSize(14);
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                tv.setTextColor(getColor(R.color.color_text_primary));
            } else
                tv.setTextColor(getResources().getColor(R.color.color_text_primary));
            tv.setGravity(Gravity.RIGHT);

            RelativeLayout.LayoutParams params = new RelativeLayout.LayoutParams(RelativeLayout.LayoutParams.WRAP_CONTENT, RelativeLayout.LayoutParams.WRAP_CONTENT);
            params.addRule(RelativeLayout.ALIGN_PARENT_RIGHT);

            // RelativeLayout.LayoutParams params = null;
            if (android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.KITKAT) {
                //  params = new RelativeLayout.LayoutParams(new RelativeLayout.LayoutParams(RelativeLayout.LayoutParams.WRAP_CONTENT, RelativeLayout.LayoutParams.WRAP_CONTENT));
                params.setMargins(0, 8, 16, 0);
                //params.gravity = Gravity.RIGHT;
                tv.setLayoutParams(params);
            }
            linearlayout.addView(tv);
        }

       /* if (syncCart.getCart_values().getOffer_savings() == 0 && syncCart.getCart_values().getCredits_savings() == 0 && syncCart.getCart_values().getBag_savings() == 0) {
            ((TextView) findViewById(R.id.tvDiscount)).setVisibility(View.VISIBLE);
            ((TextView) findViewById(R.id.tvDiscount)).setText("Discount (-):" +
                    getPaddedAmount(syncCart.getCart_values().getTotal_saving(), true));
        } else {
            ((TextView) findViewById(R.id.tvDiscount)).setVisibility(View.GONE);
        }*/

    }

    private void setCartItemsAdapter(ArrayList<Cart> cartList) {
        cartItemsAdapter = new CartItemsAdapter(cartActivity, cartList);
        cartItemsAdapter.setOnCartEvent(new CartItemsAdapter.OnCartEvent() {
            @Override
            public void onItemRemoved(Cart cartItem) {
                JSONObject jsonObject = new JSONObject();
                try {
                    jsonObject.put(Constants.USER_ID, user.getUser_id());
                    jsonObject.put(Constants.AUTH_TOKEN, preferences.getString(Constants.KEY_TOKEN, ""));
                    jsonObject.put(Constants.PRODUCT, cartItem.getProduct_id());
                    jsonObject.put(Constants.ATTR_ID, cartItem.getProduct_attr_id());
                    jsonObject.put(Constants.ATTR_VALUE, cartItem.getProduct_size());

                } catch (JSONException e) {
                    e.printStackTrace();
                }
                postJsonData(Urls.CART_DELETE, jsonObject, Constants.TAG_CART_DELETE, true);
            }

            @Override
            public void onItemClicked(Cart cartItem) {
                Intent intent = new Intent(getApplicationContext(), ProductViewActivity.class);
                Product product = cartItem.getProduct(cartItem);
                Constants.debug("Size :" + product.getProduct_size());
                intent.putExtra(Constants.PRODUCT_NAME, product);
                startActivity(intent);
            }

            @Override
            public void onItemQuantityChanged(Cart cartItem) {
                JSONObject jsonObject = new JSONObject();
                try {
                    jsonObject.put(Constants.USER_ID, user.getUser_id());
                    jsonObject.put(Constants.AUTH_TOKEN, preferences.getString(Constants.KEY_TOKEN, ""));
                    jsonObject.put(Constants.PRODUCT, cartItem.getProduct_id());
                    jsonObject.put(Constants.ATTR_ID, cartItem.getProduct_attr_id());
                    // jsonObject.put(Constants.ATTR_VALUE, cartItem.getProduct_size());
                    jsonObject.put(Constants.QUANTITY, cartItem.getProduct_qty());
                    jsonObject.put(Constants.CART_ITEM_ID, cartItem.getCart_item_id());

                } catch (JSONException e) {
                    e.printStackTrace();
                }
                postJsonData(Urls.CART_UPDATE, jsonObject, Constants.TAG_CART_UPDATE, true);
            }
        });
    }

    void syncCart() {
        JSONObject jsonObject = new JSONObject();
        try {
            if (user != null && String.valueOf(user.getUser_id()) != null)
                jsonObject.put(Constants.USER_ID, user.getUser_id());
            else if (preferences.getString(Constants.USER_ID, "") != null){
                jsonObject.put(Constants.USER_ID, preferences.getString(Constants.USER_ID, ""));
            }
            jsonObject.put(Constants.AUTH_TOKEN, preferences.getString(Constants.KEY_TOKEN, ""));

            jsonObject.put(Constants.APPLY_OFFERS, isOfferApplied);
            jsonObject.put(Constants.APPLY_WALLET, isWalletApplied);
            jsonObject.put(Constants.APPLY_CREDITS, isCreditApplied);
            jsonObject.put(Constants.GIFT_WRAP, isGiftWrap);
        } catch (JSONException e) {
            e.printStackTrace();
        }
        postJsonData(Urls.CART_VIEW, jsonObject, Constants.TAG_CART_VIEW, true);
    }

    @Override
    public void onCompleteRetrieval(int tag, ArrayList obj, String msg) {
        super.onCompleteRetrieval(tag, obj, msg);
        switch (tag) {
            case Constants.TAG_USER:
                if (obj.size() > 0)
                    user = (User) obj.get(0);
                //setCartItemsAdapter();
                syncCart();
                break;
        }
    }

    @Override
    public <T> void onResponse(T obj, int tag) {
        super.onResponse(obj, tag);
        switch (tag) {
            case Constants.TAG_CART_VIEW:
                syncCart = (SyncCart) obj;
                if (syncCart != null && syncCart.getStatus().equalsIgnoreCase(Constants.RESPONSE_SUCCESS)) {
                    findViewById(R.id.rlNoCartItems).setVisibility(View.GONE);
                    cartList = new ArrayList(syncCart.getCart_items());

                    setCartItemsAdapter(cartList);
                    cartOperations.replaceItems(cartList);
                    cartItemsAdapter.setCartItems(cartList);
                    // cartItemsAdapter.setOutOfStockItems();
                    cartItemsAdapter.notifyDataSetChanged();
                    user.setCart_count(String.valueOf(cartList.size()));
                    insertRecord(user, Constants.TAG_USER);

                } else {
                    //showToast(syncCart.getMessage());
                    findViewById(R.id.rlNoCartItems).setVisibility(View.VISIBLE);
                    CartOperations.getInstance().removeAll();
                    findViewById(R.id.llCheckout).setVisibility(View.GONE);
                    findViewById(R.id.svCart).setVisibility(View.GONE);
                    findViewById(R.id.tvCartEmpty).setVisibility(View.GONE);
                    user.setCart_count("" + 0);
                    insertRecord(user, Constants.TAG_USER);
                }
                break;
            case Constants.TAG_CART_UPDATE:
                AddCartResponse addCartResponse = (AddCartResponse) obj;
                if (addCartResponse.getStatus().equalsIgnoreCase(Constants.RESPONSE_SUCCESS)) {
                    //cartOperations.updateItem(addCartResponse.getProductDetails());
                    syncCart();
                } else
                    showToast(addCartResponse.getMessage());
                break;
            case Constants.TAG_CART_DELETE:
                CartDeleteItemResponse response = (CartDeleteItemResponse) obj;
                if (response.getStatus().equalsIgnoreCase(Constants.RESPONSE_SUCCESS)) {
                    cartOperations.removeItem(response.getData().getProduct_id());
                    syncCart();
                } else
                    showToast(response.getMessage());
                break;
        }
    }


    @Override
    public void onError(int error_code, int tag, Object object) {
        checkErrorCode(error_code, object);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_home, menu);
        actionCart = menu.findItem(R.id.action_cart).setIcon(R.drawable.ic_cart_selected);
        ActionItemBadge.update(this, menu.findItem(R.id.action_cart),
                getResources().getDrawable(R.drawable.ic_cart_selected), ActionItemBadge.BadgeStyles.RED,
                cartOperations.getCartSize());
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.action_profile:
                finish();
                startActivity(new Intent(this, ProfileActivity.class));
                return true;
            case R.id.action_home:
                finish();
                startActivity(new Intent(this, HomeActivity.class));
                return true;
            case R.id.action_cart:
                return true;
            default:
                break;
        }
        return super.onOptionsItemSelected(item);
    }

    private String getPaddedAmount(String amount, boolean rupeeSymbol) {
        String result = rupeeSymbol ? Constants.RUPEE : "" + Constants.formatAmount(amount);
        int pad = 16 - result.length();
        for (int i = 0; i < pad; i++) {
            result = " " + result;
        }
        return result;
    }

    private String getPaddedAmount(Double amount, boolean rupeeSymbol) {
        String result = rupeeSymbol ? Constants.RUPEE + Constants.formatAmount(amount) : "" + Constants.formatAmount(amount);
        int pad = 16 - result.length();
        for (int i = 0; i < pad; i++) {
            result = " " + result;
        }
        return result;
    }

    private String getPaddedAmount(Integer amount, boolean rupeeSymbol) {
        String result = rupeeSymbol ? Constants.RUPEE + Constants.formatAmount(amount) : "" + Constants.formatAmount(amount);
        int pad = 16 - result.length();
        for (int i = 0; i < pad; i++) {
            result = " " + result;
        }
        return result;
    }

    private String getPaddedString(String string) {
        int pad = 16 - string.length();
        for (int i = 0; i < pad; i++) {
            string = " " + string;
        }
        return string;
    }

}
