package outletwise.com.twentydresses.view.activity;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.ActionBar;
import android.text.Html;
import android.view.View;
import android.widget.TextView;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

import outletwise.com.twentydresses.R;
import outletwise.com.twentydresses.controller.MyTagHandler;
import outletwise.com.twentydresses.model.WalletResponse;
import outletwise.com.twentydresses.utilities.Constants;
import outletwise.com.twentydresses.utilities.Urls;

/**
 * Created by User-PC on 05-11-2015.
 * This clas is use to show wallet amount
 */
public class MyWalletActivity extends BaseActivity {
    private WalletResponse response;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_my_wallet);
        trackPage(getString(R.string.pgWallet));

        setUpToolBar(getString(R.string.toolbar_wallet));
        final ActionBar ab = getSupportActionBar();
        ab.setDisplayHomeAsUpEnabled(true);


        findViewById(R.id.bGetRefund).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                Intent intent = new Intent(getApplicationContext(), GetRefundActivity.class);
                //  intent.putExtra(Constants.GET_REFUND, response);
                startActivity(intent);
            }
        });
    }


    @Override
    public void onCompleteRetrieval(int tag, ArrayList obj, String msg) {
        super.onCompleteRetrieval(tag, obj, msg);
        JSONObject jsonObject = new JSONObject();
        try {
            jsonObject.put(Constants.USER_ID, user.getUser_id());
            jsonObject.put(Constants.AUTH_TOKEN, preferences.getString(Constants.KEY_TOKEN, ""));
        } catch (JSONException e) {
            e.printStackTrace();
        }

        postJsonData(Urls.USER_WALLET, jsonObject, Constants.TAG_WALLET, false);
    }

    @Override
    public <T> void onResponse(T obj, int tag) {
        super.onResponse(obj, tag);

        TextView tvWalletLarge, tvWallet, tvRequestMsg;
        tvWalletLarge = ((TextView) findViewById(R.id.tvWalletLarge));
        tvWallet = ((TextView) findViewById(R.id.tvWallet));
        tvRequestMsg = ((TextView) findViewById(R.id.tvRequestMsg));

        findViewById(R.id.pbWheel).setVisibility(View.GONE);
        findViewById(R.id.cvBalance).setVisibility(View.VISIBLE);

        response = (WalletResponse) obj;
        if (response.isShow_wallet())
            tvWalletLarge.setText(Constants.RUPEE + Constants.formatAmount(response.getWallet()));
        else
            tvWalletLarge.setText(Constants.RUPEE + "0");

        tvWallet.setText(Html.fromHtml(response.getWallet_description(), null, new MyTagHandler()));

        if (response.isShow_refund_button())
            findViewById(R.id.bGetRefund).setVisibility(View.VISIBLE);

        if (response.getRequest_message() != null) {
            tvRequestMsg.setVisibility(View.VISIBLE);
            tvRequestMsg.setText(Html.fromHtml(response.getRequest_message(), null, new MyTagHandler()));
        } else
            tvRequestMsg.setVisibility(View.GONE);
    }


    @Override
    public void onError(int error_code, int tag, Object object) {
        checkErrorCode(error_code, object);
    }
}
