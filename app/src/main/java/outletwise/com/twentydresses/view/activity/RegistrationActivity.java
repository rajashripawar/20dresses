package outletwise.com.twentydresses.view.activity;

import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.support.v4.view.ViewPager;
import android.view.View;
import android.widget.ImageView;

import com.squareup.picasso.Picasso;
import com.viewpagerindicator.CirclePageIndicator;

import java.util.Timer;
import java.util.TimerTask;

import outletwise.com.twentydresses.R;
import outletwise.com.twentydresses.controller.adapter.RegistrationSingleImageAdapter;
import outletwise.com.twentydresses.model.BackgroundImgResponse;
import outletwise.com.twentydresses.utilities.Constants;
import outletwise.com.twentydresses.view.custom.CustomViewPager;

/**
 * This class is use to show Login/Registration option
 */
public class RegistrationActivity extends BaseActivity {

    private CustomViewPager viewPager;
    private RegistrationSingleImageAdapter adapter;
    private int currentPage;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_register);
     //   findViewById(R.id.pbWheelHome).setVisibility(View.VISIBLE);
       // setBackgroundImage(Constants.IMG_HOME);

        // Function to call if contains viepager on Home screen
      /*  //setUpViewPager();
        //initPagerTimer();*/
       /* ParseUser user = new ParseUser();
        user.setUsername("my name");
        user.setPassword("my pass");
        user.setEmail("email@example.com");

        // other fields can be set just like with ParseObject
        user.put("phone", "650-555-0000");
        user.signUpInBackground();*/

        //If back pressed exit app
        setClearActivities(true);

        findViewById(R.id.bSignIn).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(getApplicationContext(), LoginActivity.class);
                startActivity(intent);
            }
        });

        findViewById(R.id.bRegister).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(getApplicationContext(), RegistrationActivity2.class);
                startActivity(intent);
            }
        });
    }


    private void setUpViewPager() {
        viewPager = (CustomViewPager) findViewById(R.id.pager);
        viewPager.setPagingEnabled(false);
        adapter = new RegistrationSingleImageAdapter(this);
        viewPager.setAdapter(adapter);
        CirclePageIndicator pageIndicator = (CirclePageIndicator) findViewById(R.id.indicator);
        pageIndicator.setViewPager(viewPager);
        pageIndicator.setVisibility(View.GONE);

        viewPager.addOnPageChangeListener(new ViewPager.OnPageChangeListener() {
            @Override
            public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {

            }

            @Override
            public void onPageSelected(int position) {
                currentPage = position;
            }

            @Override
            public void onPageScrollStateChanged(int state) {

            }
        });

    }

    private void initPagerTimer() {
        final Handler handler = new Handler();

        final Runnable Update = new Runnable() {
            public void run() {
                if (currentPage == adapter.getCount()) {
                    currentPage = 0;
                }
                viewPager.setCurrentItem(currentPage++, true);
            }
        };

        final Timer swipeTimer = new Timer();
        swipeTimer.schedule(new TimerTask() {

            @Override
            public void run() {
                handler.post(Update);
            }
        }, 1000, 2000);
    }

    @Override
    public <T> void onResponse(T obj, int tag) {

        switch (tag) {
            case Constants.TAG_IMG_BACKGROUND:
                findViewById(R.id.pbWheelHome).setVisibility(View.GONE);
                BackgroundImgResponse response = (BackgroundImgResponse) obj;
                if (response.getStatus().equalsIgnoreCase(Constants.RESPONSE_SUCCESS)) {
                    String imgUrl = response.getImage();

                    ImageView iv_background = (ImageView) findViewById(R.id.iv_background);
                    Picasso.with(getApplicationContext()).load(imgUrl).into(iv_background);

                    toggelVisibility();
                }

                break;
        }
    }

    private void toggelVisibility() {
        findViewById(R.id.rlLogo).setVisibility(View.VISIBLE);
        findViewById(R.id.llButton).setVisibility(View.VISIBLE);
    }

}
