package outletwise.com.twentydresses.view.fragment;

import android.content.ClipData;
import android.content.ClipboardManager;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.text.Html;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.facebook.share.model.ShareLinkContent;
import com.facebook.share.widget.ShareDialog;
import com.squareup.picasso.Picasso;

import org.json.JSONException;
import org.json.JSONObject;

import outletwise.com.twentydresses.R;
import outletwise.com.twentydresses.controller.MyTagHandler;
import outletwise.com.twentydresses.model.ReferCodeResponse;
import outletwise.com.twentydresses.utilities.Constants;
import outletwise.com.twentydresses.utilities.Urls;
import outletwise.com.twentydresses.view.activity.BaseActivity;

public class InviteFragment extends BaseFragment implements View.OnClickListener {

    private String appLink;
    private String msg;
    private String fbMsg;
    private TextView tvRefereCode;
    private ReferCodeResponse response;
    private TextView tvDesc;
    private View rootView;

    public InviteFragment() {
        // Required empty public constructor
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        rootView = inflater.inflate(R.layout.fragment_invite, container, false);
        init();

        appLink = "http://tinyurl.com/zrqpw9k";
        msg = "Checkout this exciting online and fashion styling destination for women - " + appLink + " \nAll you got to do is fill out a fun fashion quiz and register. Don't forget to use the code ";
        fbMsg = "https://www.facebook.com/sharer/sharer.php?u=https%3A//tinyurl.com/zrqpw9k";
        // Inflate the layout for this fragment

        return rootView;
    }


    void init() {
        rootView.findViewById(R.id.tvWhatsApp).setOnClickListener(this);
        rootView.findViewById(R.id.tvFacebook).setOnClickListener(this);
        rootView.findViewById(R.id.tvMessaging).setOnClickListener(this);
        rootView.findViewById(R.id.tvMail).setOnClickListener(this);
        rootView.findViewById(R.id.tvSeeMore).setOnClickListener(this);
        tvRefereCode = (TextView) rootView.findViewById(R.id.txtvw_refer_code);
        tvDesc = (TextView) rootView.findViewById(R.id.tvDesc);

        rootView.findViewById(R.id.cvCode).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                if (android.os.Build.VERSION.SDK_INT < android.os.Build.VERSION_CODES.HONEYCOMB) {
                    android.text.ClipboardManager clipboard = (android.text.ClipboardManager) mActivity.getSystemService(mActivity.CLIPBOARD_SERVICE);
                    clipboard.setText(msg);
                } else {
                    ClipboardManager clipboard = (ClipboardManager) mActivity.getSystemService(mActivity.CLIPBOARD_SERVICE);
                    ClipData clip = ClipData.newPlainText("Copied Text", msg);
                    clipboard.setPrimaryClip(clip);
                }

                showToast(getString(R.string.text_copy));
            }
        });

        getReferralCode();
    }

    private void getReferralCode() {

        Long user_id = getArguments().getLong(Constants.USER_ID);

        JSONObject jsonObject = new JSONObject();
        try {
            jsonObject.put(Constants.AUTH_TOKEN, ((BaseActivity) mActivity).preferences.getString(Constants.KEY_TOKEN, ""));
            jsonObject.put(Constants.USER_ID, user_id);
        } catch (JSONException e) {
            e.printStackTrace();
        }
        postJsonData(Urls.REFER, jsonObject, Constants.TAG_REFER, true);
    }


    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.tvWhatsApp:
                try {
                    Intent sendIntent = new Intent();
                    sendIntent.setAction(Intent.ACTION_SEND);
                    sendIntent.putExtra(Intent.EXTRA_TEXT, msg);
                    sendIntent.setType("text/plain");
                    sendIntent.setPackage("com.whatsapp");
                    startActivity(sendIntent);
                } catch (Exception e) {
                    e.printStackTrace();
                }
                break;

            case R.id.tvFacebook:
                ShareLinkContent linkContent = new ShareLinkContent.Builder()
                        .setContentTitle("Hey !")
                        .setContentDescription("Don't forget to use the code" + response.getRefer_code())
                        .setContentUrl(Uri.parse("https://play.google.com/store/apps/details?id=outletwise.com.twentydresses"))
                        .build();
                ShareDialog shareDialog = new ShareDialog(mActivity);
                shareDialog.show(linkContent);

               /* try {
                    Intent myWebLink = new Intent(android.content.Intent.ACTION_VIEW);
                    myWebLink.setData(Uri.parse(fbMsg));
                    startActivity(myWebLink);
                } catch (Exception e) {
                    e.printStackTrace();
                }*/
                break;

            case R.id.tvSeeMore:
                try {
                    Intent sendIntent = new Intent();
                    sendIntent.setAction(Intent.ACTION_SEND);
                    sendIntent.putExtra(Intent.EXTRA_TEXT, msg);
                    sendIntent.setType("text/plain");
                    startActivity(Intent.createChooser(sendIntent, "INVITE FRIENDS"));
                } catch (Exception e) {
                    e.printStackTrace();
                }
                break;
            case R.id.tvMail:
                try {
                    Intent emailIntent = new Intent(android.content.Intent.ACTION_SEND);
                    emailIntent.setType("plain/text");
                    emailIntent.putExtra(android.content.Intent.EXTRA_SUBJECT, "20Dresses App");
                    emailIntent.putExtra(android.content.Intent.EXTRA_TEXT, msg);
                    startActivity(Intent.createChooser(emailIntent, "Share..."));
                } catch (Exception e) {
                    e.printStackTrace();
                }
                break;
            case R.id.tvMessaging:
                try {
                    Intent sendIntent = new Intent(Intent.ACTION_VIEW);
                    sendIntent.putExtra("sms_body", msg);
                    sendIntent.setType("vnd.android-dir/mms-sms");
                    startActivity(sendIntent);
                } catch (Exception e) {
                    e.printStackTrace();
                }
                break;
        }
    }

    @Override
    public <T> void onResponse(T obj, int tag) {
        super.onResponse(obj, tag);
        switch (tag) {
            case Constants.TAG_REFER:
                rootView.findViewById(R.id.pbWheel).setVisibility(View.GONE);
                rootView.findViewById(R.id.rlRefer).setVisibility(View.VISIBLE);
                // rootView.findViewById(R.id.scrollView).setVisibility(View.VISIBLE);
                response = (ReferCodeResponse) obj;
                tvRefereCode.setText(response.getRefer_code());
                tvDesc.setText(Html.fromHtml(String.valueOf(response.getDescription()), null, new MyTagHandler()));

                Picasso.with(mActivity).load(response.getRefer_banner()).into((ImageView)(rootView.findViewById(R.id.ivBanner)));
                msg = msg + response.getRefer_code();
                break;
        }
    }

}
