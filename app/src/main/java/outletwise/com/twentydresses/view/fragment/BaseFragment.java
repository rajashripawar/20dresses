package outletwise.com.twentydresses.view.fragment;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.support.v4.app.Fragment;
import android.support.v7.widget.Toolbar;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.Map;

import outletwise.com.twentydresses.utilities.Constants;
import outletwise.com.twentydresses.utilities.Urls;
import outletwise.com.twentydresses.view.activity.BaseActivity;
import outletwise.com.twentydresses.view.activity.LoginActivity;

/**
 * Created by User-PC on 28-07-2015.
 */
public abstract class BaseFragment extends Fragment {
    private static final String ERROR_MSG1 = "Activity not attached or not an instance of base activity";
    protected Activity mActivity;

    /**
     * Attach activity to fragment
     * */
    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        mActivity = (Activity) context;
    }



    public void setBackgroundImage(String type) {

        JSONObject jsonObject = new JSONObject();
        try {
            jsonObject.put("type", type);
        } catch (JSONException e) {
            e.printStackTrace();
        }
        postJsonData(Urls.IMG_BACKGROUND, jsonObject, Constants.TAG_IMG_BACKGROUND, false);
    }

    @Override
    public void onPause() {
        super.onPause();
    }

    @Override
    public void onResume() {
        super.onResume();
    }

    /**
     * User for adding fragment in a fragment
     * @param container : FrameLayout for the fragment
     * @param tag : Request Tag for adding to backstack
     * */
    protected void addFragment(int container, Fragment fragment, String tag) {
        if (mActivity != null && mActivity instanceof BaseActivity) {
            ((BaseActivity) mActivity).addFragfromFrag(this, container, fragment, tag);
        } else
            Constants.error(ERROR_MSG1);
    }

    /**
     * User for replacing fragment in a fragment
     * @param container : FrameLayout for the fragment
     * @param tag : Request Tag for adding to backstack
     * */
    protected void replaceFragment(int container, Fragment fragment, String tag) {
        if (mActivity != null && mActivity instanceof BaseActivity) {
            ((BaseActivity) mActivity).replaceFragfromFrag(this, container, fragment, tag);
        } else
            Constants.error(ERROR_MSG1);
    }

    protected Toolbar getToolBar() {
        return ((BaseActivity) mActivity).getToolbar();
    }

    protected void showToast(String msg) {
        ((BaseActivity) mActivity).showToast(msg);
    }


    /*==================================================================================================
        Network Call Methods
    ===================================================================================================*/

    /**
     * Send data over network
     * This method uses Query Params & POST method
     */
    protected void postStringData(String URL, final Map<String, String> params,
                                  final int reqId, boolean disableTouch) {
        if (mActivity != null && mActivity instanceof BaseActivity) {
            ((BaseActivity) mActivity).postStringData(this, URL, params, reqId, disableTouch);
        } else
            Constants.error(ERROR_MSG1);
    }

    /**
     * Send data over network
     * This method uses POJO which is internally converted to JSON Object & POST method
     */
    protected void postJsonData(String URL, Object object, final int reqId, boolean disableTouch) {
        if (mActivity != null && mActivity instanceof BaseActivity) {
            ((BaseActivity) mActivity).postJsonData(this, URL, object, reqId, disableTouch);
        } else
            Constants.error(ERROR_MSG1);
    }

    /**
     * Send data over network
     * This method uses JSON Object & POST method
     */
    protected void postJsonData(String URL, JSONObject jsonObject, final int reqId, boolean disableTouch) {
        if (mActivity != null && mActivity instanceof BaseActivity) {
            ((BaseActivity) mActivity).postJsonData(this, URL, jsonObject, reqId, disableTouch);
        } else
            Constants.error(ERROR_MSG1);
    }

    /**
     * Send data over network which expects json array from server
     * This method uses Query Params & POST method
     */
    protected void postJsonDataArray(String URL, JSONObject object, final int reqId, boolean disableTouch) {
        if (mActivity != null && mActivity instanceof BaseActivity) {
            ((BaseActivity) mActivity).postJsonDataArray(this, URL, object, reqId, disableTouch);
        } else
            Constants.error(ERROR_MSG1);
    }

    public <T> void onResponse(T obj, int tag) {

    }

    public void onError(int error_code, int tag, Object object) {

    }

    /*======================================================================================
        Database Call Methods
     ======================================================================================*/

    protected void insertRecord(Object object, int tag) {
        if (mActivity != null && mActivity instanceof BaseActivity) {
            ((BaseActivity) mActivity).insertRecord(this, object, tag);
        } else
            Constants.error(ERROR_MSG1);
    }

    protected void deleteRecord(Object object, int tag) {
        if (mActivity != null && mActivity instanceof BaseActivity) {
            ((BaseActivity) mActivity).deleteRecord(this, object, tag);
        } else
            Constants.error(ERROR_MSG1);
    }

    protected void insertRecord(ArrayList objects, int tag) {
        if (mActivity != null && mActivity instanceof BaseActivity) {
            ((BaseActivity) mActivity).insertRecord(this, objects, tag);
        } else
            Constants.error(ERROR_MSG1);
    }

    protected void selectRecords(int tag) {
        if (mActivity != null && mActivity instanceof BaseActivity) {
            ((BaseActivity) mActivity).selectRecords(this, tag);
        } else
            Constants.error(ERROR_MSG1);
    }

    public void onCompleteInsertion(int tag, String msg) {
    }

    public void onCompleteUpdation(int tag, String msg) {

    }

    public void onCompleteRetrieval(int tag, ArrayList obj, String msg) {
    }

    public void onCompleteDeletion(int tag, String msg) {

    }

    public void onDatabaseError(int status, String msg) {

    }

}
