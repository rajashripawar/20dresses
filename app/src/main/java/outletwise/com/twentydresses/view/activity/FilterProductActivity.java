package outletwise.com.twentydresses.view.activity;

import android.app.Activity;
import android.content.Intent;
import android.os.Build;
import android.os.Bundle;
import android.support.v7.app.ActionBar;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.TextView;

import com.mikepenz.actionitembadge.library.ActionItemBadge;
import com.sothree.slidinguppanel.SlidingUpPanelLayout;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;

import outletwise.com.twentydresses.R;
import outletwise.com.twentydresses.controller.CartOperations;
import outletwise.com.twentydresses.controller.ProductOptions;
import outletwise.com.twentydresses.controller.adapter.BaseRecyclerAdapter;
import outletwise.com.twentydresses.controller.adapter.ProductsAdapter;
import outletwise.com.twentydresses.model.Category;
import outletwise.com.twentydresses.model.Product;
import outletwise.com.twentydresses.model.SubTip;
import outletwise.com.twentydresses.model.database.greenbot.Shortlist;
import outletwise.com.twentydresses.utilities.Constants;
import outletwise.com.twentydresses.utilities.Urls;
import outletwise.com.twentydresses.view.custom.CustomRecyclerView;
import outletwise.com.twentydresses.view.custom.FavoritesButton;
import outletwise.com.twentydresses.view.fragment.ProductsSortFragment;

/**
 * Created by User-PC on 06-08-2015.
 * This class is use to show filtered products. Filters are applied from FilterActivity
 */
public class FilterProductActivity extends BaseActivity implements ProductsAdapter.ShortListed {

    private CustomRecyclerView rv;
    private ArrayList<Product> products = new ArrayList<>();
    private boolean isLoading = false;
    private int page = 1;
    private ProductsAdapter productAdapter;
    private RecyclerScrollListener recyclerScrollListener;
    private Category.SubCat subCat;
    boolean isForYou = false;
    private String sortBy;
    private MenuItem menuCart;
    private HashMap<String, String> params;
    private String strProductCount;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_filter_product);

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            ((TextView) findViewById(R.id.tvFilter)).setCompoundDrawablesWithIntrinsicBounds(getDrawable(R.drawable.selected_filter), null, null, null);
        } else
            ((TextView) findViewById(R.id.tvFilter)).setCompoundDrawablesWithIntrinsicBounds(getResources().getDrawable(R.drawable.selected_filter), null, null, null);

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            ((TextView) findViewById(R.id.tvFilter)).setTextColor(getColor(R.color.color_accent));
        } else
            ((TextView) findViewById(R.id.tvFilter)).setTextColor(getResources().getColor(R.color.color_accent));


        try {
            Bundle extras = getIntent().getExtras();
            if (extras != null) {
                params = (HashMap<String, String>) extras.get(Constants.APPLIED_FILTERS);
                products = (ArrayList<Product>) extras.get(Constants.FILTER_PRODUCTS);
                subCat = (Category.SubCat) extras.get(Constants.CATEGORY);

                strProductCount = products.get(0).getProduct_count();
                ((TextView) findViewById(R.id.tvProductCount)).setVisibility(View.VISIBLE);
                ((TextView) findViewById(R.id.tvProductCount)).setText("Total Products : " + strProductCount);

            }
        } catch (Exception ex) {
            ex.printStackTrace();
        }

        //    subCat = (Category.SubCat) getIntent().getExtras().get(Constants.CATEGORY);
        if (subCat != null && subCat.getName() != null && subCat.getName().equalsIgnoreCase("For You")) {
            isForYou = true;
        }

        if (subCat != null && subCat.getName() != null) {
            if (subCat.getName().equalsIgnoreCase("Offers_Sale") || subCat.getName().equalsIgnoreCase("Offers_Category"))
                setUpToolBar("Home".toUpperCase());
            else
                setUpToolBar(subCat.getName().toUpperCase());
        } else
            setUpToolBar("");

        final ActionBar ab = getSupportActionBar();
        ab.setDisplayHomeAsUpEnabled(true);

        setUpRecyclerView();
        setUpSlidingPanel();
        setUpProductOptions();
    }

    private void setUpProductOptions() {
        ProductOptions options = new ProductOptions(this);
        options.setOptionClickListener(new ProductOptions.OptionClick() {
            @Override
            public void onOptionClick(ProductOptions.Option option) {
                if (option.getTag() == ProductOptions.TOGGLE) {
                    if (option.isToggleState()) {
                        resetRecyclerView(ProductsAdapter.PRODUCT_HORIZONTAL);
                        slidePanelTo(SlidingUpPanelLayout.PanelState.ANCHORED);
                    } else {
                        resetRecyclerView(ProductsAdapter.PRODUCT_VERTICAL);
                        slidePanelTo(SlidingUpPanelLayout.PanelState.COLLAPSED);
                    }
                } else if (option.getTag() == ProductOptions.FILTER) {
                    Intent intent = new Intent();
                    intent.putExtra(Constants.CATEGORY, subCat);
                    setResult(RESULT_OK, intent);
                    finish();
                   /* Intent intent = new Intent(getApplicationContext(), FilterActivity.class);
                    intent.putExtra(Constants.CATEGORY, subCat);
                    startActivity(intent);
                    finish();*/
                }
            }
        });
        options.setSortCallback(new ProductsSortFragment.Callback() {
            @Override
            public void onCallback(String sort) {
                sortBy = sort;
                productAdapter.getItems().clear();
                productAdapter.showProgressBar();
                productAdapter.notifyDataSetChanged();
                page = 1;
                getProducts(page);

                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
                    ((TextView) findViewById(R.id.tvSort)).setCompoundDrawablesWithIntrinsicBounds(getDrawable(R.drawable.selected_sort), null, null, null);
                } else
                    ((TextView) findViewById(R.id.tvSort)).setCompoundDrawablesWithIntrinsicBounds(getResources().getDrawable(R.drawable.selected_sort), null, null, null);

                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                    ((TextView) findViewById(R.id.tvSort)).setTextColor(getColor(R.color.color_accent));
                } else
                    ((TextView) findViewById(R.id.tvSort)).setTextColor(getResources().getColor(R.color.color_accent));
            }
        });
    }

    private void getProducts(int page) {
        if (sortBy != null) {
            params.put("sort", sortBy);
            // page = 1;
        }

        if (page == 1)
            params.remove("page");

        if (page > 1)
            params.put("page", page + "");

        postStringData(Urls.PRODUCTS, params, Constants.TAG_PRODUCTS, false);
        isLoading = true;
    }

    void setUpRecyclerView() {
        rv = (CustomRecyclerView) findViewById(R.id.rvProducts);
        productAdapter = new ProductsAdapter(this, products, ProductsAdapter.PRODUCT_VERTICAL, this, isForYou);
        productAdapter.setOnItemClickListener(new BaseRecyclerAdapter.OnItemClickListener() {
            @Override
            public void onItemClick(View view, int position) {
                if (view.getTag() != null) {
                    Intent intent = new Intent(getApplicationContext(), ProductViewActivity.class);
                    intent.putExtra(Constants.PRODUCT_NAME, (Product) view.getTag());
                    startActivity(intent);
                }
            }
        });
        recyclerScrollListener = new RecyclerScrollListener();
        resetRecyclerView(ProductsAdapter.PRODUCT_VERTICAL);
        rv.addOnScrollListener(recyclerScrollListener);
        //  getProducts(page);
        selectRecords(Constants.TAG_SHORTLIST);
    }

    void resetRecyclerView(int orientation) {
        LinearLayoutManager layoutManager;
        if (orientation == ProductsAdapter.PRODUCT_VERTICAL) {
            layoutManager = new GridLayoutManager(rv.getContext(), 2);
            ((GridLayoutManager) layoutManager).setSpanSizeLookup(new GridLayoutManager.SpanSizeLookup() {
                @Override
                public int getSpanSize(int position) {
                    return productAdapter.getItemWeight(position);
                }
            });
            rv.setIsHorizontal(false);
        } else {
            layoutManager = new LinearLayoutManager(rv.getContext());
            layoutManager.setOrientation(LinearLayoutManager.HORIZONTAL);
            rv.setIsHorizontal(true);
        }
        productAdapter.setType(orientation);
        rv.setLayoutManager(layoutManager);
        rv.setAdapter(productAdapter);
        recyclerScrollListener.setLayoutManager(layoutManager);
        rv.scrollToPosition(recyclerScrollListener.getPastVisibleItem());
    }

    @Override
    protected void onResume() {
        super.onResume();
        ActionItemBadge.update(this, menuCart,
                getResources().getDrawable(R.drawable.ic_cart), ActionItemBadge.BadgeStyles.RED,
                CartOperations.getInstance().getCartSize());
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_home, menu);
        menuCart = menu.findItem(R.id.action_cart);
        ActionItemBadge.update(this, menuCart,
                getResources().getDrawable(R.drawable.ic_cart), ActionItemBadge.BadgeStyles.RED,
                CartOperations.getInstance().getCartSize());
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.action_profile:
                startActivity(new Intent(this, ProfileActivity.class));
                return true;
            case R.id.action_home:
                startActivity(new Intent(this, HomeActivity.class));
                return true;
            case R.id.action_cart:
                startActivity(new Intent(this, CartActivity.class));
                return true;
            default:
                break;
        }
        return super.onOptionsItemSelected(item);
    }


    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == 1 && resultCode == Activity.RESULT_OK) {

        }
    }

    @Override
    public void addProductToShortList(Product product) {
        JSONObject jsonObject = new JSONObject();
        try {
            jsonObject.put(Constants.AUTH_TOKEN, preferences.getString(Constants.KEY_TOKEN, ""));
            if (user != null && String.valueOf(user.getUser_id()) != null){
                if (!String.valueOf(user.getUser_id()).trim().isEmpty()){
                    jsonObject.put(Constants.USER_ID, String.valueOf(user.getUser_id()));
                }
            }
            else
                jsonObject.put(Constants.USER_ID, preferences.getString(Constants.USER_ID,""));
            jsonObject.put(Constants.PRODUCT, product.getProduct_id());
        } catch (JSONException e) {
            e.printStackTrace();
        }
        postJsonData(Urls.SHORTLIST_ADD, jsonObject, Constants.TAG_SHORTLIST, false);
    }

    @Override
    public void removeProductFromShortList(Product product) {

        JSONObject jsonObject = new JSONObject();
        try {
            jsonObject.put(Constants.AUTH_TOKEN, preferences.getString(Constants.KEY_TOKEN, ""));
            if (user != null && String.valueOf(user.getUser_id()) != null)
                jsonObject.put(Constants.USER_ID, user.getUser_id());
            else
                jsonObject.put(Constants.USER_ID, preferences.getString(Constants.USER_ID,""));
//            jsonObject.put(Constants.USER_ID, user.getUser_id());

            jsonObject.put(Constants.PRODUCT, product.getProduct_id());
        } catch (JSONException e) {
            e.printStackTrace();
        }
        postJsonData(Urls.SHORTLIST_REMOVE, jsonObject, Constants.TAG_SHORTLIST_REMOVE, true);
    }

    @Override
    public void dropComplete(View view, int tag, Object object) {
        super.dropComplete(view, tag, object);
        switch (tag) {
            case Constants.TAG_SHORTLIST:
                Product product = (Product) object;
                productAdapter.changeProductState(product.getProduct_id(), FavoritesButton.IN_PROGRESS);
                addProductToShortList(product);
                break;
            default:
        }
    }

    @Override
    public void onCompleteRetrieval(int tag, ArrayList obj, String msg) {
        super.onCompleteRetrieval(tag, obj, msg);
        switch (tag) {
            case Constants.TAG_SHORTLIST:
                final ArrayList<Shortlist> shortlists = obj;
                new Thread(new Runnable() {
                    @Override
                    public void run() {
                        for (Shortlist shortlist : shortlists)
                            productAdapter.changeProductState(shortlist.getProduct_id(), FavoritesButton.PROGRESS_COMPLETE);
                    }
                }).start();
                break;
        }
    }

    @Override
    public <T> void onResponse(T obj, int tag) {
        super.onResponse(obj, tag);
        switch (tag) {
            case Constants.TAG_SHORTLIST:
                showToast(getString(R.string.toast_added_to_shortlist));
                Shortlist shortlist = (Shortlist) obj;
                productAdapter.changeProductState(shortlist.getProduct_id() + "", FavoritesButton.PROGRESS_COMPLETE);
                productAdapter.notifyDataSetChanged();
                insertRecord(obj, tag);
                break;

            case Constants.TAG_PRODUCTS:
                productAdapter.hideProgressBar();
                if (obj instanceof String && ((String) obj).equalsIgnoreCase(Constants.NO_MORE_RESULTS)) {
                    showToast(obj.toString());
                }/*else if (obj instanceof SubTip) {
                    products.addAll(((SubTip) obj).getProducts());
                    productAdapter.notifyDataSetChanged();
                    isLoading = false;
                }*/
                 else if (obj instanceof Product[]) {
                    products.addAll((Arrays.asList((Product[]) obj)));
                    productAdapter.notifyDataSetChanged();
                    isLoading = false;
                }
                break;
            case Constants.TAG_SHORTLIST_REMOVE:
                showToast(getString(R.string.toast_removed_from_shortlist));
                Shortlist shortlist1 = (Shortlist) obj;
                productAdapter.changeProductState(shortlist1.getProduct_id() + "", FavoritesButton.PROGRESS_IDLE);
                productAdapter.notifyDataSetChanged();
                deleteRecord(shortlist1, Constants.TAG_SHORTLIST_REMOVE);
                break;
        }
    }

    @Override
    public void onError(int error_code, int tag, Object object) {
        super.onError(error_code, tag, object);
        switch (tag) {
            case Constants.TAG_SHORTLIST:
                try {
                    JSONObject jsonObject = (JSONObject) object;
                    productAdapter.changeProductState(jsonObject.get("product") + "", FavoritesButton.PROGRESS_IDLE);
                    productAdapter.notifyDataSetChanged();

                } catch (JSONException e) {
                    e.printStackTrace();
                }
                break;

            case Constants.TAG_PRODUCTS:
                if (page != 2)
                    showToast(getString(R.string.no_more_products));
                break;
        }
        productAdapter.hideProgressBar();
    }


    class RecyclerScrollListener extends RecyclerView.OnScrollListener {
        private int pastVisibleItem, visibleItemCount, totalItemCount;
        private RecyclerView.LayoutManager layoutManager;

        public void setLayoutManager(RecyclerView.LayoutManager layoutManager) {
            this.layoutManager = layoutManager;
        }

        public int getPastVisibleItem() {
            return pastVisibleItem;
        }

        @Override
        public void onScrolled(RecyclerView recyclerView, int dx, int dy) {
            visibleItemCount = layoutManager.getChildCount();
            totalItemCount = layoutManager.getItemCount();
            pastVisibleItem = ((LinearLayoutManager) layoutManager).findFirstVisibleItemPosition();

            if ((visibleItemCount + pastVisibleItem) >= totalItemCount) {
                if (!isLoading) {
                    productAdapter.showProgressBar();
                    page++;
                    getProducts(page);
                }
            }
        }
    }

}
