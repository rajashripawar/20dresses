package outletwise.com.twentydresses.view.activity;

import android.Manifest;
import android.accounts.Account;
import android.accounts.AccountManager;
import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.os.Build;
import android.os.Bundle;
import android.support.v7.app.ActionBar;
import android.text.TextUtils;
import android.util.Log;
import android.view.View;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.CompoundButton;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.RadioButton;
import android.widget.Spinner;
import android.widget.TextView;

import com.andexert.expandablelayout.library.ExpandableLayout;
import com.citrus.sdk.Callback;
import com.citrus.sdk.CitrusClient;
import com.citrus.sdk.CitrusUser;
import com.citrus.sdk.Environment;
import com.citrus.sdk.TransactionResponse;
import com.citrus.sdk.classes.CitrusException;
import com.citrus.sdk.classes.Month;
import com.citrus.sdk.classes.Year;
import com.citrus.sdk.payment.CreditCardOption;
import com.citrus.sdk.payment.DebitCardOption;
import com.citrus.sdk.payment.MerchantPaymentOption;
import com.citrus.sdk.payment.NetbankingOption;
import com.citrus.sdk.payment.PaymentType;
import com.citrus.sdk.response.CitrusError;
import com.google.android.gms.analytics.HitBuilders;
import com.paytm.pgsdk.PaytmMerchant;
import com.paytm.pgsdk.PaytmOrder;
import com.paytm.pgsdk.PaytmPGService;
import com.paytm.pgsdk.PaytmPaymentTransactionCallback;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;

import outletwise.com.twentydresses.R;
import outletwise.com.twentydresses.controller.AnalyticsTracker;
import outletwise.com.twentydresses.controller.CartOperations;
import outletwise.com.twentydresses.controller.adapter.NetBankingAdapter;
import outletwise.com.twentydresses.controller.adapter.OrdersViewAdapter;
import outletwise.com.twentydresses.model.Address;
import outletwise.com.twentydresses.model.OrderItem;
import outletwise.com.twentydresses.model.PinCodeResponse;
import outletwise.com.twentydresses.model.PlaceOrder;
import outletwise.com.twentydresses.model.PlaceOrderResponse;
import outletwise.com.twentydresses.model.VisitorResponse;
import outletwise.com.twentydresses.model.database.greenbot.Cart;
import outletwise.com.twentydresses.model.database.greenbot.User;
import outletwise.com.twentydresses.utilities.Constants;
import outletwise.com.twentydresses.utilities.Urls;
import outletwise.com.twentydresses.view.custom.MonthYearPicker;

/**
 * Created by User-PC on 06-11-2015.
 * This class is use to show payment options
 */
public class PaymentOptionsActivity extends BaseActivity {

    private PlaceOrder placeOrder = new PlaceOrder();
    private ExpandableLayout elDebitCard, elCreditCard, elInternetBanking;
    private Spinner spInternetBank;
    private CitrusClient citrusClient;
    private Button bProceed;
    private TextView tvDebitCardHolderName, tvDebitCardNumber, tvDebitCardExpiry, tvDebitCardCVV;
    private TextView tvCreditCardHolderName, tvCreditCardNumber, tvCreditCardExpiry, tvCreditCardCVV;
    private PlaceOrderResponse placeOrderResponse;
    private PaytmPGService Service;
    private PaytmMerchant Merchant;
    private RadioButton radioButton;
    private String paymentMode = "";
    private boolean isNetBankSelected;
    private LinearLayout ll_mailadd;
    private EditText tvemail;
    private TextView tv_emailvalidtxt;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_payment_options);
        trackPage(getString(R.string.pgCheckout));
        setUpToolBar(getString(R.string.toolbar_payment_mode));
        final ActionBar ab = getSupportActionBar();
        ab.setDisplayHomeAsUpEnabled(true);
    }

    @Override
    public void onCompleteRetrieval(int tag, ArrayList obj, String msg) {
        super.onCompleteRetrieval(tag, obj, msg);
        switch (tag) {
            case Constants.TAG_USER:
                if (obj.size() > 0)
                    user = (User) obj.get(0);
                placeOrder.setUser(user.getUser_id());
                init();
                initCitrusSDK();
                //savePaymentOption();
                initPatym();
                checkoutReminder();
                break;
        }
    }

    /*******************************
     * CHECKOUT START
     ***************************************/

    private void checkoutReminder() {
        JSONObject jsonObject = new JSONObject();
        try {
            jsonObject.put(Constants.USER_ID, user.getUser_id());
            jsonObject.put(Constants.AUTH_TOKEN, preferences.getString(Constants.KEY_TOKEN, ""));
        } catch (JSONException e) {
            e.printStackTrace();
        }
        postJsonData(Urls.CHECKOUT_CART, jsonObject, Constants.TAG_CHECKOUT_CART, false);
    }

    void checkout(){
        switch (paymentMode) {
            case Constants.DEBIT_CARD:
                if (validateCard()) {
                    String temp[] = tvDebitCardExpiry.getText().toString().trim().split("/");
                    DebitCardOption debitCardOption = new DebitCardOption(tvDebitCardHolderName.getText().toString().trim(),
                            tvDebitCardNumber.getText().toString().trim(), tvDebitCardCVV.getText().toString().trim(),
                            Month.getMonth(temp[0]), Year.getYear(temp[1]));
                    placeOrder.setPaymentOption(debitCardOption);
                }
                break;
            case Constants.CREDIT_CARD:
                if (validateCard()) {
                    String temp[] = tvCreditCardExpiry.getText().toString().trim().split("/");
                    CreditCardOption creditCardOption = new CreditCardOption(tvCreditCardHolderName.getText().toString().trim(),
                            tvCreditCardNumber.getText().toString().trim(), tvCreditCardCVV.getText().toString().trim(),
                            Month.getMonth(temp[0]), Year.getYear(temp[1]));
                    placeOrder.setPaymentOption(creditCardOption);
                }
                break;
            case Constants.PATYM:
                payUsingPatym();
                return;
        }

        Address ad = placeOrder.getAddress();
        CitrusUser.Address citrusAddress = new CitrusUser.Address(ad.getAddress(), "", ad.getCity(), ad.getState(), "India", ad.getPincode());
        PaymentType.PGPayment pgPayment = null;
        try {
            pgPayment = new PaymentType.PGPayment(placeOrder.getAmount(), placeOrder.getBillUri(),
                    placeOrder.getPaymentOption(), new CitrusUser(user.getUser_email(), placeOrder.getAddress().getMobile(),
                    user.getUser_first_name(), user.getUser_last_name(), citrusAddress));
        } catch (CitrusException e) {
            e.printStackTrace();
        }

        citrusClient.pgPayment(pgPayment, new Callback<TransactionResponse>() {

            @Override
            public void success(TransactionResponse transactionResponse) {
                Constants.debug("Json Response :" + transactionResponse.getJsonResponse());
                Constants.debug(transactionResponse.getMessage());
                confirmPrepaid(transactionResponse);
                // orderPlaced();
            }

            @Override
            public void error(CitrusError error) {
                showToast(error.getMessage());
                Constants.error(error.getMessage());
                finish();
                startActivity(new Intent(getApplicationContext(), CartActivity.class));
            }
        });
    }

    /*******************************
     * CHECKOUT END
     ***************************************/
    /*To send transaction response to server to confirm order*/
    private void confirmPrepaid(TransactionResponse transactionResponse) {
        String json = transactionResponse.getJsonResponse();
        JSONObject obj = null;
        try {
            obj = new JSONObject(json);
            Log.d("My App", obj.toString());
        } catch (Throwable t) {
            Log.e("My App", "Could not parse malformed JSON: \"" + json + "\"");
        }
        JSONObject jsonObject = new JSONObject();
        try {
            jsonObject.put(Constants.USER_ID, placeOrder.getUser());
            jsonObject.put(Constants.AUTH_TOKEN, preferences.getString(Constants.KEY_TOKEN, ""));
            jsonObject.put(Constants.ORDER, placeOrderResponse.getOrder_id());
            jsonObject.put("response", obj);
            jsonObject.put("payment_gateway", Constants.CITRUS);

        } catch (JSONException e) {
            e.printStackTrace();
        }
        postJsonData(Urls.CONFIRM_PREPAID, jsonObject, Constants.TAG_CONFIRM_PREPAID, true);
        //  sendInvoice();
    }

    /*To send invoice to customer after order confirmed*/
    private void sendInvoice() {

        JSONObject jsonObject = new JSONObject();
        try {
            jsonObject.put(Constants.USER_ID, placeOrder.getUser());
            jsonObject.put(Constants.AUTH_TOKEN, preferences.getString(Constants.KEY_TOKEN, ""));
            jsonObject.put(Constants.ORDER, placeOrderResponse.getOrder_id());

        } catch (JSONException e) {
            e.printStackTrace();
        }
        postJsonData(Urls.SEND_INVOICE, jsonObject, Constants.TAG_SEND_INVOICE, true);
    }

  /*  void savePaymentOption() {
  // Save credit card details
        citrusClient.savePaymentOption(new CreditCardOption("Bruce Wayne", "4111111111111111", "123", Month.getMonth("12"), Year.getYear("2016")), new Callback<CitrusResponse>() {
            @Override
            public void success(CitrusResponse citrusResponse) {
                Constants.debug("Payment Option Saved");
            }

            @Override
            public void error(CitrusError error) {

            }
        });

// Save debit card details
        citrusClient.savePaymentOption(new DebitCardOption("Tom Cruise", "4111111111111111", "111", Month.getMonth("12"), Year.getYear("2016")), new Callback<CitrusResponse>() {
            @Override
            public void success(CitrusResponse citrusResponse) { }

            @Override
            public void error(CitrusError error) { }
        });

// Save internet banking details
        citrusClient.savePaymentOption(new NetbankingOption("ICICI Bank", "CID001"), new Callback<CitrusResponse>() {
            @Override
            public void success(CitrusResponse citrusResponse) { }

            @Override
            public void error(CitrusError error) { }
        });
    }*/

    boolean validateCard() {
        switch (paymentMode) {
            case Constants.CREDIT_CARD:
                if (TextUtils.isEmpty(tvCreditCardHolderName.getText().toString())) {
                    showToast(getString(R.string.toast_card_name));
                    return false;
                } else if (TextUtils.isEmpty(tvCreditCardNumber.getText().toString())) {
                    showToast(getString(R.string.toast_card_number));
                    return false;
                } else if (TextUtils.isEmpty(tvCreditCardExpiry.getText().toString())) {
                    showToast(getString(R.string.toast_card_expiry));
                    return false;
                } else if (TextUtils.isEmpty(tvCreditCardCVV.getText().toString())) {
                    showToast(getString(R.string.toast_card_cvv));
                    return false;
                }
                break;
            case Constants.DEBIT_CARD:
                if (TextUtils.isEmpty(tvDebitCardHolderName.getText().toString())) {
                    showToast(getString(R.string.toast_card_name));
                    return false;
                } else if (TextUtils.isEmpty(tvDebitCardNumber.getText().toString())) {
                    showToast(getString(R.string.toast_card_number));
                    return false;
                } else if (TextUtils.isEmpty(tvDebitCardExpiry.getText().toString())) {
                    showToast(getString(R.string.toast_card_expiry));
                    return false;
                } else if (TextUtils.isEmpty(tvDebitCardCVV.getText().toString())) {
                    showToast(getString(R.string.toast_card_cvv));
                    return false;
                }

                break;
        }
        return true;
    }

    void initPatym() {

        //     Service = PaytmPGService.getStagingService(); //for testing environment
        Service = PaytmPGService.getProductionService(); //for production environment

        Merchant = new PaytmMerchant(Urls.CHECKSUM_GENERATOR, Urls.CHECKSUM_VALIDATE);
    }

    void payUsingPatym() {
        Map<String, String> paramMap = new HashMap<>();

        //these are mandatory parameters
       /* paramMap.put("REQUEST_TYPE", "DEFAULT");
        paramMap.put("ORDER_ID", placeOrderResponse.getOrder_id() + "");
        paramMap.put("MID", "Outlet45378741173648");
        paramMap.put("CUST_ID", user.getUser_id() + "");
        paramMap.put("CHANNEL_ID", "WAP");
        paramMap.put("INDUSTRY_TYPE_ID", "Retail");
        paramMap.put("WEBSITE", "Outletwisewap");
        paramMap.put("TXN_AMOUNT", placeOrder.getAmount().getValue() + "");
        paramMap.put("THEME", "merchant");*/

        paramMap.put("REQUEST_TYPE", "DEFAULT");
        paramMap.put("ORDER_ID", placeOrderResponse.getOrder_id() + "");
        paramMap.put("MID", "Outlet20642706073467");
        paramMap.put("CUST_ID", user.getUser_id() + "");
        paramMap.put("CHANNEL_ID", "WAP");
        paramMap.put("INDUSTRY_TYPE_ID", "Retail104");
        paramMap.put("WEBSITE", "Outletwisewap");
        paramMap.put("TXN_AMOUNT", placeOrder.getAmount().getValue() + "");
        paramMap.put("THEME", "merchant");
        paramMap.put("MOBILE_NO", placeOrder.getAddress().getMobile());
        paramMap.put("EMAIL", user.getUser_email());


        PaytmOrder Order = new PaytmOrder(paramMap);

        Service.initialize(Order, Merchant, null);
        Service.startPaymentTransaction(this, false, true, new PaytmPaymentTransactionCallback() {

            @Override
            public void onTransactionSuccess(Bundle inResponse) {
                Constants.error("success patym: " + inResponse);
                confirmPrepaidPaytm(inResponse);
                // orderPlaced();
            }

            @Override
            public void onTransactionFailure(String inErrorMessage, Bundle inResponse) {
                Constants.error("error patym: " + inErrorMessage);
                finish();
                startActivity(new Intent(getApplicationContext(), CartActivity.class));
            }

            @Override
            public void networkNotAvailable() {

            }


            @Override
            public void clientAuthenticationFailed(String inErrorMessage) {
                Log.i("Error", "clientAuthenticationFailed :" + inErrorMessage);
            }

            @Override
            public void someUIErrorOccurred(String s) {
                Constants.debug("UI Error : " + s);
            }

            @Override
            public void onErrorLoadingWebPage(int i, String s, String s1) {
                Constants.debug("onErrorLoadingWebPage : " + s);
                Constants.debug("onErrorLoadingWebPage1 : " + s1);

            }

        });

    }

    private void confirmPrepaidPaytm(Bundle inResponse) {

        JSONObject json = new JSONObject();
        Set<String> keys = inResponse.keySet();
        for (String key : keys) {
            try {
                json.put(key, inResponse.get(key));
            } catch (JSONException e) {
                e.printStackTrace();
            }
        }


        JSONObject jsonObject = new JSONObject();
        try {
            jsonObject.put(Constants.USER_ID, placeOrder.getUser());
            jsonObject.put(Constants.AUTH_TOKEN, preferences.getString(Constants.KEY_TOKEN, ""));
            jsonObject.put(Constants.ORDER, placeOrderResponse.getOrder_id());
            jsonObject.put("response", json);
            jsonObject.put("payment_gateway", Constants.PATYM);

        } catch (JSONException e) {
            e.printStackTrace();
        }
        postJsonData(Urls.CONFIRM_PREPAID, jsonObject, Constants.TAG_CONFIRM_PREPAID, true);
        //   sendInvoice();
    }

    void initCitrusSDK() {
        citrusClient = CitrusClient.getInstance(this);
        citrusClient.enableLog(true);
        citrusClient.init(getString(R.string.citrus_sign_up_id), getString(R.string.citrus_secret_key_sign_up),
                getString(R.string.citrus_sign_up_id), getString(R.string.citrus_secret_key_sign_in),
                getString(R.string.citrus_sign_vanity), Environment.PRODUCTION);

/*        citrusClient.signUp("adolfdsilva@gmail.com", "8698673954", "adolfd123", new Callback<CitrusResponse>() {
            @Override
            public void success(CitrusResponse citrusResponse) {
                Constants.debug("Signed in");
            }

            @Override
            public void error(CitrusError error) {
                Constants.debug("Error Signing in");
            }
        });

        citrusClient.signIn("vipul@outletwise.com", "vipul@123", new Callback<CitrusResponse>() {
            @Override
            public void success(CitrusResponse citrusResponse) {
                Constants.debug("Signed in");
            }

            @Override
            public void error(CitrusError error) {
                Constants.debug("Error Signing in");
            }
        });*/
        getNetBanks();
    }

    void getNetBanks() {
        citrusClient.getMerchantPaymentOptions(new Callback<MerchantPaymentOption>() {
            @Override
            public void success(MerchantPaymentOption merchantPaymentOption) {
                elInternetBanking.findViewById(R.id.pbNetBank).setVisibility(View.GONE);
                elInternetBanking.findViewById(R.id.cvInternetBanking).setVisibility(View.VISIBLE);
                ArrayList<NetbankingOption> mNetBankingOptionsList = merchantPaymentOption.getNetbankingOptionList();//this will give you only bank list
                NetBankingAdapter adapter = new NetBankingAdapter(getApplicationContext(), R.layout.layout_net_banking_adapter_spinner, mNetBankingOptionsList);
                adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
                spInternetBank.setAdapter(adapter);
                spInternetBank.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
                    @Override
                    public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                        if (position != 0) {
                            isNetBankSelected = true;
                            placeOrder.setPaymentOption((NetbankingOption) view.getTag());
                        } else
                            isNetBankSelected = false;
                    }

                    @Override
                    public void onNothingSelected(AdapterView<?> parent) {

                    }
                });
            }

            @Override
            public void error(CitrusError error) {

            }
        });
    }

    void sendTxn() {
        AlertDialog.Builder builder1 = new AlertDialog.Builder(this);
        builder1.setMessage(getString(R.string.text_place_order));
        builder1.setCancelable(false);

        builder1.setPositiveButton(
                "Yes",
                new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                        placeOrder.setCurrent_app_version(getCurrentAppVersion());
                        postJsonData(Urls.ORDER_PLACE, placeOrder, Constants.TAG_ORDER_PLACE, true);
                    }
                });

        builder1.setNegativeButton(
                "No",
                new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                        dialog.cancel();
                    }
                });

        AlertDialog alert11 = builder1.create();
        alert11.show();
    }

    public final static boolean isValidEmail(CharSequence target) {
        return !TextUtils.isEmpty(target) && android.util.Patterns.EMAIL_ADDRESS.matcher(target).matches();
    }

    void init() {
        double finalPrice = getIntent().getDoubleExtra(Constants.GRAND_TOTAL, 0);
        boolean isOfferApplied = getIntent().getBooleanExtra(Constants.APPLY_OFFERS, false);
        boolean isCreditApplied = getIntent().getBooleanExtra(Constants.APPLY_CREDITS, false);
        boolean isWalletApplied = getIntent().getBooleanExtra(Constants.APPLY_WALLET, false);
        boolean isGiftWrap = getIntent().getBooleanExtra(Constants.GIFT_WRAP, false);
        Address selectedAddress = getIntent().getParcelableExtra(Constants.ADDRESS);
        ll_mailadd= (LinearLayout) findViewById(R.id.ll_mailadd);

        // Existing Address
        checkForCODAvailability(selectedAddress.getPincode(), selectedAddress.getMobile());

        placeOrder.setIsCreditApplied(isCreditApplied);
        placeOrder.setIsGiftWrap(isGiftWrap);
        placeOrder.setIsOfferApplied(isOfferApplied);
        placeOrder.setIsWalletApplied(isWalletApplied);
        placeOrder.setAddress(selectedAddress);
        placeOrder.setAuth_token(preferences.getString(Constants.KEY_TOKEN, ""));
        placeOrder.setAmount(finalPrice);
        //  placeOrder.setAmount(1);

        findViewById(R.id.llCheckout).bringToFront();
        ((TextView) findViewById(R.id.tvFinalAmount)).setText(Constants.getPaddedAmount(finalPrice, true));

        List<Cart> cartList = CartOperations.getInstance().getCartList();
        ArrayList<OrderItem.OrderItemsEntity> orderItems = new ArrayList<>();

        for (Cart cartItem : cartList) {
            OrderItem.OrderItemsEntity orderItem = new OrderItem.OrderItemsEntity();
            orderItem.setItem_image(cartItem.getProduct_img());
            orderItem.setItem_name(cartItem.getProduct_name());
            orderItem.setItem_price(Integer.valueOf(cartItem.getProduct_price()));
            orderItem.setItem_savings(Integer.valueOf(cartItem.getProduct_savings() + ""));
            orderItem.setItem_quantity(cartItem.getProduct_qty());
            orderItem.setItem_size(cartItem.getProduct_size());
            orderItem.setItem_status("");
            orderItem.setItem_mrp(cartItem.getProduct_mrp());
            orderItems.add(orderItem);

        }

        elDebitCard = (ExpandableLayout) findViewById(R.id.elDebitCard);
        tvDebitCardHolderName = (TextView) elDebitCard.findViewById(R.id.cardHolderName);
        tvDebitCardNumber = (TextView) elDebitCard.findViewById(R.id.cardNumber);
        tvDebitCardExpiry = (TextView) elDebitCard.findViewById(R.id.cardExpiry);
        tvDebitCardCVV = (TextView) elDebitCard.findViewById(R.id.cardCVV);

        elCreditCard = (ExpandableLayout) findViewById(R.id.elCreditCard);
        tvCreditCardHolderName = (TextView) elCreditCard.findViewById(R.id.cardHolderName);
        tvCreditCardNumber = (TextView) elCreditCard.findViewById(R.id.cardNumber);
        tvCreditCardExpiry = (TextView) elCreditCard.findViewById(R.id.cardExpiry);
        tvCreditCardCVV = (TextView) elCreditCard.findViewById(R.id.cardCVV);

        elInternetBanking = (ExpandableLayout) findViewById(R.id.elInternetBank);

        spInternetBank = (Spinner) elInternetBanking.findViewById(R.id.spInternetBanking);
        bProceed = (Button) findViewById(R.id.bProceed);

        bProceed.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                if (paymentMode != null && !paymentMode.equalsIgnoreCase("")) {
                    if ((paymentMode.equalsIgnoreCase(Constants.DEBIT_CARD) ||
                            paymentMode.equalsIgnoreCase(Constants.CREDIT_CARD)) && validateCard()) {
                        placeOrder.setPayment_mode(Constants.PREPAID);
                        placeOrder.setPayment_gateway(Constants.CITRUS);
                        if (ll_mailadd.isShown() && tvemail.getText().toString() != null){
                            if (isValidEmail(tvemail.getText().toString())){
                                placeOrder.setUser_email(tvemail.getText().toString());
                                sendTxn();

                            }else{
                                tvemail.requestFocus();
                                showToast("Please enter valid email");
                            }
                        }else{
                            sendTxn();
                        }

                    } else if (paymentMode.equalsIgnoreCase(Constants.INTERNET_BANKING)) {
                        if (isNetBankSelected) {
                            placeOrder.setPayment_mode(Constants.PREPAID);
                            placeOrder.setPayment_gateway(Constants.CITRUS);
                            if (ll_mailadd.isShown() && tvemail.getText().toString() != null){
                                if (isValidEmail(tvemail.getText().toString())){
                                    placeOrder.setUser_email(tvemail.getText().toString());
                                    sendTxn();

                                }else{
                                    tvemail.requestFocus();
                                    showToast("Please enter valid email");
                                }
                            }else{
                                sendTxn();
                            }
//                            sendTxn();
                        } else
                            showToast(getString(R.string.select_bank_name));
                    }
                    else if (paymentMode.equalsIgnoreCase(Constants.COD)) {
                        placeOrder.setPayment_mode(Constants.COD);
                        placeOrder.setPayment_gateway("");
                        if (ll_mailadd.isShown() && tvemail.getText().toString() != null){
                            if (isValidEmail(tvemail.getText().toString())){
                                placeOrder.setUser_email(tvemail.getText().toString());
                                sendTxn();

                            }else{
                                tvemail.requestFocus();
                                showToast("Please enter valid email");
                            }
                        }else{
                            sendTxn();
                        }
//                        sendTxn();
                    } else if (paymentMode.equalsIgnoreCase(Constants.PATYM)) {
                        placeOrder.setPayment_mode(Constants.PATYM);
                        placeOrder.setPayment_gateway(Constants.PATYM);
                        if (ll_mailadd.isShown() && tvemail.getText().toString() != null){
                            if (isValidEmail(tvemail.getText().toString())){
                                placeOrder.setUser_email(tvemail.getText().toString());
                                sendTxn();

                            }else{
                                tvemail.requestFocus();
                                showToast("Please enter valid email");
                            }
                        }else{
                            sendTxn();
                        }
//                        sendTxn();
                    }
                } else
                    showToast(getString(R.string.select_payment_option));
            }
        });

        // TODO Use checkbox text instead of separate textView
        //Checkbox Listeners
        final RadioButton radioCOD = ((RadioButton) findViewById(R.id.rbCOD));
        //Set initial mode of payment to COD
        this.radioButton = radioCOD;
        radioCOD.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                if (isChecked) {
                    paymentMode = Constants.COD;
                    resetRadioButtons(radioCOD);
                }
            }
        });

        final RadioButton radioDebit = ((RadioButton) elDebitCard.findViewById(R.id.radio));
        radioDebit.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                if (isChecked) {
                    paymentMode = Constants.DEBIT_CARD;
                    resetRadioButtons(radioDebit);
                }
                if (elDebitCard.isOpened())
                    elDebitCard.hide();
                else
                    elDebitCard.show();
            }
        });

        final RadioButton radioCredit = ((RadioButton) elCreditCard.findViewById(R.id.radio));
        radioCredit.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                if (isChecked) {
                    paymentMode = Constants.CREDIT_CARD;
                    resetRadioButtons(radioCredit);
                }
                if (elCreditCard.isOpened())
                    elCreditCard.hide();
                else
                    elCreditCard.show();
            }
        });

        final RadioButton radioNet = ((RadioButton) elInternetBanking.findViewById(R.id.radio));
        radioNet.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                if (isChecked) {
                    paymentMode = Constants.INTERNET_BANKING;
                    resetRadioButtons(radioNet);
                }
                if (elInternetBanking.isOpened())
                    elInternetBanking.hide();
                else
                    elInternetBanking.show();
            }
        });

        final RadioButton radioPatym = ((RadioButton) findViewById(R.id.rbPaytm));
        radioPatym.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                if (isChecked) {
                    paymentMode = Constants.PATYM;
                    resetRadioButtons(radioPatym);
                }
            }
        });

        TextView tvDebitHeader = ((TextView) elDebitCard.findViewById(R.id.tvHeaderText));
        tvDebitHeader.setText(getString(R.string.text_debit_card));
        tvDebitHeader.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                radioDebit.performClick();
            }
        });

        TextView tvCreditHeader = ((TextView) elCreditCard.findViewById(R.id.tvHeaderText));
        tvCreditHeader.setText(getString(R.string.text_credit_card));
        tvCreditHeader.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                radioCredit.performClick();
            }
        });

        TextView tvNet = ((TextView) elInternetBanking.findViewById(R.id.tvHeaderText));
        tvNet.setText(getString(R.string.text_internet_bank));
        tvNet.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                radioNet.performClick();
            }
        });

        findViewById(R.id.ivPatym).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                radioPatym.performClick();
            }
        });

        findViewById(R.id.tvCOD).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                radioCOD.performClick();
            }
        });


        //Show Date Picker on Expiry Click
        tvCreditCardExpiry.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                showDatePicker(tvCreditCardExpiry);
            }
        });

        tvDebitCardExpiry.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                showDatePicker(tvDebitCardExpiry);
            }
        });

        findViewById(R.id.ivPatym).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                radioPatym.performClick();
            }
        });


        new OrdersViewAdapter(this, orderItems, false, null);
    }

    public String getMailId() {
        String strGmail = "";
        try {
            //  if (ActivityCompat.checkSelfPermission(this, Manifest.permission.GET_ACCOUNTS) != PackageManager.PERMISSION_GRANTED)
            {
                Account[] accounts = AccountManager.get(this).getAccounts();
                for (Account account : accounts) {
                    String possibleEmail = account.name;
                    String type = account.type;

                    if (type.equals("com.google")) {
                        strGmail = possibleEmail;
                        Log.e("PIKLOG", "Emails: " + strGmail);
                        break;
                    }
                }
            }

        } catch (Exception e) {
            e.printStackTrace();
        }

        return strGmail;
    }
    private void checkForCODAvailability(String pincode, String mobile) {
        JSONObject jsonObject = new JSONObject();
        try {
            jsonObject.put(Constants.USER_ID, user.getUser_id());
            jsonObject.put(Constants.PINCODE, pincode);

            /*
            * Send mobile number for COD block check only on checkout page
            * */
            if (mobile != null && !mobile.equalsIgnoreCase(""))
                jsonObject.put(Constants.MOBILE, mobile);

        } catch (JSONException e) {
            e.printStackTrace();
        }
        postJsonData(Urls.CHK_DEL_OPTS, jsonObject, Constants.TAG_CHK_DEL_OPTS, true);
    }

    void resetRadioButtons(RadioButton radioButton) {
        this.radioButton.setChecked(false);
        this.radioButton = radioButton;
    }

    void showDatePicker(final TextView textView) {
      /*  CardExpiryPickerFragment datePicker = new CardExpiryPickerFragment();
        datePicker.setDatePicked(new CardExpiryPickerFragment.DatePicked() {
            @Override
            public void onDateSet(DatePicker view, int year, int month) {
                textView.setText(Constants.toCardExpiryFormat(year, month));
            }
        });
        datePicker.show(getmFragmentManager(), CardExpiryPickerFragment.class.getSimpleName());*/

        final MonthYearPicker myp = new MonthYearPicker(this);
        myp.build(new DialogInterface.OnClickListener() {

            @Override
            public void onClick(DialogInterface dialog, int which) {
                textView.setText(Constants.toCardExpiryFormat(myp.getSelectedYear(), myp.getSelectedMonth()));
            }
        }, null);

        myp.show();
    }

    void orderPlaced() {
        AnalyticsTracker.getInstance().trackRevenue(placeOrderResponse, placeOrder);

        /* Google Analytics*/
        AnalyticsTracker.getInstance().sendDataToTwoTrackers(new HitBuilders.TransactionBuilder()
                .setTransactionId(String.valueOf(placeOrderResponse.getOrder_id()))
                .setRevenue(placeOrderResponse.getOrder().getOrder_total())
                .setCurrencyCode("INR")
                .build());


        for (int i = 0; i < placeOrderResponse.getOrder_items().size(); i++) {

            AnalyticsTracker.getInstance().sendDataToTwoTrackers(new HitBuilders.ItemBuilder()
                    .setTransactionId(String.valueOf(placeOrderResponse.getOrder_id()))
                    .setName(placeOrderResponse.getOrder_items().get(i).getProduct_name())
                    .setSku(placeOrderResponse.getOrder_items().get(i).getProduct_sku())
                    .setCategory(placeOrderResponse.getOrder_items().get(i).getProduct_category_name())
                    .setPrice(Double.parseDouble(placeOrderResponse.getOrder_items().get(i).getProduct_mrp()))
                    .setQuantity(Long.parseLong(placeOrderResponse.getOrder_items().get(i).getQuantity()))
                    .setCurrencyCode("INR")
                    .build());
        }

        /*Empty cart after placing order*/
        CartOperations.getInstance().removeAll();
        user.setCart_count("0");
        insertRecord(user, Constants.TAG_USER);

        if (placeOrderResponse.isMobile_verified()) {
            Intent intent = new Intent(getApplicationContext(), OrderSuccessfulActivity.class);
            intent.putExtra(Constants.ORDER_ID, placeOrderResponse.getOrder_id());
            startActivity(intent);
        } else {
            Intent intent = new Intent(getApplicationContext(), VerifyOrderActivity.class);
            intent.putExtra(Constants.ORDER_ID, placeOrderResponse.getOrder_id());
            intent.putExtra(Constants.MOBILE, placeOrder.getAddress().getMobile());
            startActivity(intent);
        }
    }


    @Override
    public <T> void onResponse(T obj, int tag) {
        super.onResponse(obj, tag);
        switch (tag) {
            case Constants.TAG_ORDER_PLACE:
                placeOrderResponse = (PlaceOrderResponse) obj;
                //Mixpanel track revenue
                //trackRevenue(placeOrderResponse, placeOrder);
                if (placeOrderResponse.getStatus().equalsIgnoreCase(Constants.RESPONSE_SUCCESS)) {
                    if (placeOrder.getPayment_mode().equalsIgnoreCase(Constants.COD)) {
                        orderPlaced();
                        sendInvoice();
                    } else {
                        placeOrder.setBillUri(getString(R.string.citrus_bill_url) + placeOrderResponse.getOrder_id());
                        checkout();
                    }
                } else {
                    if (placeOrderResponse.getMessage().equalsIgnoreCase("Email is already exist")){
                        tvemail.setText("");
                        tv_emailvalidtxt.requestFocus();
                        showToast(placeOrderResponse.getMessage());

                    }else {
                        showToast(placeOrderResponse.getMessage());
                        finish();
                        startActivity(new Intent(getApplicationContext(), CartActivity.class));
                    }
                }

                break;
            case Constants.TAG_CHK_DEL_OPTS:
                PinCodeResponse response1 = (PinCodeResponse) obj;
                if (response1.getStatus().equalsIgnoreCase(Constants.RESPONSE_SUCCESS)) {
                    // Check for paytm option
                    if (response1.getPaytm_option())
                        findViewById(R.id.rlPatym).setVisibility(View.VISIBLE);
                    else
                        findViewById(R.id.rlPatym).setVisibility(View.GONE);
                    findViewById(R.id.rlCOD).setVisibility(View.VISIBLE);
                    if (response1.getCod_available()) {
                        if (response1.getUser_cod_block()) {
                            paymentMode = "";
                            findViewById(R.id.rbCOD).setEnabled(false);
                            findViewById(R.id.tvMsg).setVisibility(View.VISIBLE);
                            ((TextView) findViewById(R.id.tvMsg)).setText(response1.getCheckout_message());

                        } else {
                            findViewById(R.id.rbCOD).setEnabled(true);
                            findViewById(R.id.tvMsg).setVisibility(View.GONE);
                            paymentMode = Constants.COD;
                        }

                    } else {
                        paymentMode = "";
                        findViewById(R.id.rbCOD).setEnabled(false);
                        findViewById(R.id.tvMsg).setVisibility(View.VISIBLE);
                        ((TextView) findViewById(R.id.tvMsg)).setText(response1.getCheckout_message());
                    }
                    if (response1.getUser_email().equalsIgnoreCase("invalid")){
                        ll_mailadd.setVisibility(View.VISIBLE);
                        tvemail = (EditText) findViewById(R.id.tvemail);
                        tv_emailvalidtxt = (TextView)findViewById(R.id.tv_emailvalidtxt);
                        tv_emailvalidtxt.setText(response1.getUser_email_message());

                        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                            int hasReadStoragePermission = checkSelfPermission(Manifest.permission.GET_ACCOUNTS);
                            if (hasReadStoragePermission != PackageManager.PERMISSION_GRANTED) {
                                requestPermissions(new String[]{Manifest.permission.GET_ACCOUNTS}, 1);
                                return;
                            } else
                                tvemail.setText(getMailId());
                        } else
                            tvemail.setText(getMailId());
                    }
                    else {
                        ll_mailadd.setVisibility(View.GONE);
                    }
                } else
                    showToast(response1.getMsg());

                break;

            case Constants.TAG_CONFIRM_PREPAID:
                VisitorResponse confirmResponse = (VisitorResponse) obj;
                if (confirmResponse.getStatus().equalsIgnoreCase(Constants.RESPONSE_SUCCESS)) {
                    sendInvoice();
                } else
                    showToast(confirmResponse.getMessage());
                orderPlaced();
                break;
        }
    }

    public void checkExpand(View view) {
        if (elDebitCard.isOpened())
            elDebitCard.findViewById(R.id.tvHeaderText).setSelected(true);
        else
            elDebitCard.findViewById(R.id.tvHeaderText).setSelected(false);
    }


    @Override
    public void onError(int error_code, int tag, Object object) {
        checkErrorCode(error_code, object);
    }
}
