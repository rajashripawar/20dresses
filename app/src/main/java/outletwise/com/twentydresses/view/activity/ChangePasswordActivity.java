package outletwise.com.twentydresses.view.activity;

import android.os.Bundle;
import android.support.v7.app.ActionBar;
import android.view.View;
import android.widget.EditText;

import org.json.JSONException;
import org.json.JSONObject;

import outletwise.com.twentydresses.R;
import outletwise.com.twentydresses.model.VisitorResponse;
import outletwise.com.twentydresses.utilities.Constants;
import outletwise.com.twentydresses.utilities.Urls;

/**
 * Class is use to change password of account
 */
public class ChangePasswordActivity extends BaseActivity {

    private EditText edt_oldpass, edt_newpass, edt_confirmpass;
    private String strOldPass, strNewPass, strConfirmPass;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_change_password);
        setUpToolBar(getString(R.string.toolbar_change_passwd));
        final ActionBar ab = getSupportActionBar();
        ab.setDisplayHomeAsUpEnabled(true);

        initUI();


        findViewById(R.id.bSubmit).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                strOldPass = edt_oldpass.getText().toString();
                strNewPass = edt_newpass.getText().toString();
                strConfirmPass = edt_confirmpass.getText().toString();

                if (!strOldPass.isEmpty() && !strNewPass.isEmpty() && !strConfirmPass.isEmpty()) {
                    if (strNewPass.equals(strConfirmPass)) {
                        findViewById(R.id.pbWheelPassword).setVisibility(View.VISIBLE);
                        JSONObject jsonObject = new JSONObject();
                        try {
                            jsonObject.put(Constants.USER_ID, user.getUser_id());
                            jsonObject.put(Constants.AUTH_TOKEN, preferences.getString(Constants.KEY_TOKEN, ""));
                            jsonObject.put(Constants.OLD_PASSWORD, strOldPass);
                            jsonObject.put(Constants.NEW_PASSWORD, strNewPass);
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                        postJsonData(Urls.CHANGE_PASSWORD, jsonObject, Constants.TAG_CHANGE_PASSWORD, true);
                    } else
                        showToast("New password and retype password does not match.");
                } else
                    showToast("Please enter all fields.");
            }
        });


    }

    private void initUI() {
        edt_oldpass = (EditText) findViewById(R.id.edt_oldpass);
        edt_newpass = (EditText) findViewById(R.id.edt_newpass);
        edt_confirmpass = (EditText) findViewById(R.id.edt_confirmpass);
    }

    @Override
    public <T> void onResponse(T obj, int tag) {
        super.onResponse(obj, tag);
        switch (tag) {
            case Constants.TAG_CHANGE_PASSWORD:
                findViewById(R.id.pbWheelPassword).setVisibility(View.GONE);
                VisitorResponse response = (VisitorResponse) obj;
                if (response.getStatus().equalsIgnoreCase(Constants.RESPONSE_SUCCESS)) {
                    showToast(response.getMessage().toString());
                    finish();
                } else
                    showToast(response.getMessage().toString());
                break;

        }
    }


    @Override
    public void onError(int error_code, int tag, Object object) {
        checkErrorCode(error_code, object);
    }
}
