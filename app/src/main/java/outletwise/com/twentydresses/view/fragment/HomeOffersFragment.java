package outletwise.com.twentydresses.view.fragment;

import android.content.Context;
import android.content.Intent;
import android.content.res.Resources;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.TypedValue;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.GridView;
import android.widget.ListAdapter;
import android.widget.TextView;

import org.json.JSONException;
import org.json.JSONObject;

import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;

import outletwise.com.twentydresses.R;
import outletwise.com.twentydresses.controller.adapter.BaseRecyclerAdapter;
import outletwise.com.twentydresses.controller.adapter.JustSoldAdapter;
import outletwise.com.twentydresses.controller.adapter.OffersAdapter;
import outletwise.com.twentydresses.controller.adapter.ProductsAdapter;
import outletwise.com.twentydresses.model.Category;
import outletwise.com.twentydresses.model.HomeBanner;
import outletwise.com.twentydresses.model.JustSoldResponse;
import outletwise.com.twentydresses.model.Offer;
import outletwise.com.twentydresses.model.Product;
import outletwise.com.twentydresses.utilities.Constants;
import outletwise.com.twentydresses.utilities.Urls;
import outletwise.com.twentydresses.view.activity.BaseActivity;
import outletwise.com.twentydresses.view.activity.HomeActivity;
import outletwise.com.twentydresses.view.activity.ProductViewActivity;
import outletwise.com.twentydresses.view.activity.ProductsActivity;
import outletwise.com.twentydresses.view.activity.TrendsActivity;
import outletwise.com.twentydresses.view.custom.CustomRecyclerView;

/**
 * Created by User-PC on 02-12-2015.
 */
public class HomeOffersFragment extends BaseFragment implements SwipeRefreshLayout.OnRefreshListener {

    private View rootView;
    private RecyclerView rvOffers;
    private SwipeRefreshLayout mSwipeRefreshLayout;
  //  private GridView gridView;
    private ArrayList<Product> products;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        rootView = inflater.inflate(R.layout.fragment_offers, container, false);
        rootView.findViewById(R.id.pbWheel).setVisibility(View.VISIBLE);
        mSwipeRefreshLayout = (SwipeRefreshLayout) rootView.findViewById(R.id.swipeRefreshLayout);
        mSwipeRefreshLayout.setOnRefreshListener(this);

        init();
     //   getJustSoldProducts();
        //   getBanners();

        return rootView;
    }

    private void getBanners() {

        HashMap<String, String> params = new HashMap<>();
        params.put("app_key", Constants.APP_KEY);
        postJsonData(Urls.HOME_BANNERS, params, Constants.TAG_HOME_BANNERS, false);
    }


    void init() {
      //  gridView = (GridView) rootView.findViewById(R.id.gridview);

        rvOffers = (RecyclerView) rootView.findViewById(R.id.rvOffers);
        rvOffers.setLayoutManager(new LinearLayoutManager(rvOffers.getContext()));

        getJustSoldProducts();

        // getBanners();

       /* gridView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {
                Product product = (Product) view.getTag();

                Intent intent = new Intent(mActivity, ProductViewActivity.class);
                intent.putExtra(Constants.PRODUCT_NAME, product);
                intent.putExtra(Constants.CATEGORY, "");
                startActivity(intent);

            }

        });*/

      /*  rootView.findViewById(R.id.tv_see_more).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Category.SubCat subCat = new Category.SubCat();
                subCat.setId("just_sold");
                subCat.setName("JUST SOLD");
                Intent intent = new Intent(mActivity, ProductsActivity.class);
                intent.putExtra(Constants.CATEGORY, subCat);
                startActivity(intent);
            }
        });*/

        // getJustSoldProducts();
    }

    private void getJustSoldProducts() {

        HashMap<String, String> params = new HashMap<>();
        params.put("app_key", Constants.APP_KEY);
        postStringData(Urls.JUST_SOLD_PRODUCTS, params, Constants.TAG_JUST_SOLD, false);
    }

    protected int getGridHeight(int arrayListSize) {

        DecimalFormat df = new DecimalFormat("##.##");
        float a1 = arrayListSize / 3.0f;
        String a1Str = df.format(a1);
        System.out.println("a1Str:" + a1Str);
        int gridHeight = 0;

        System.out.println("float a1:" + a1);

        if (a1Str.contains(".33")) {
            gridHeight = (int) a1 + 1;
        } else if (a1Str.contains(".67")) {
            gridHeight = (int) a1 + 1;
        } else {
            gridHeight = (int) a1;
        }
        return gridHeight;

                  /*  if (arrayListSoldOut.size() % 3 == 0)
                height = (arrayListSoldOut.size() / 3) * 200;
            else
                height = ((arrayListSoldOut.size() / 3) + 1) * 200;*/
    }


    public static int convertDpToPixels(float dp, Context context) {
        Resources resources = context.getResources();
        return (int) TypedValue.applyDimension(
                TypedValue.COMPLEX_UNIT_DIP,
                dp,
                resources.getDisplayMetrics()
        );
    }

    @Override
    public <T> void onResponse(T obj, int tag) {
        super.onResponse(obj, tag);
        switch (tag) {

            case Constants.TAG_JUST_SOLD:
                JustSoldResponse justSoldResponse = (JustSoldResponse) obj;
                if (justSoldResponse.getStatus().equalsIgnoreCase(Constants.RESPONSE_SUCCESS)) {

                    products = justSoldResponse.getProducts();
                 /*   int height = getGridHeight(products.size());
                    height = height * 195;
                    ViewGroup.LayoutParams layoutParams = gridView.getLayoutParams();
                    layoutParams.height = convertDpToPixels(height, mActivity);
                    gridView.setLayoutParams(layoutParams);

                    JustSoldAdapter justSoldAdapter = new JustSoldAdapter(mActivity, products);
                    gridView.setAdapter(justSoldAdapter);*/
                    getBanners();
                } else
                    showToast(getString(R.string.no_products));
                break;

            case Constants.TAG_HOME_BANNERS:
                try{
                    rootView.findViewById(R.id.pbWheel).setVisibility(View.GONE);
                    getActivity().findViewById(R.id.rl_loader).setVisibility(View.GONE);
                    final HomeBanner bannerResponse = (HomeBanner) obj;
                    final ArrayList<HomeBanner.BannersBean> bannersBeanArrayList = bannerResponse.getBanners();

                    if (!bannerResponse.getToday_offer().get(0).getOffer_description().equalsIgnoreCase("")) {
                        rootView.findViewById(R.id.rvOfferOfDay).setVisibility(View.VISIBLE);
                        ((TextView) rootView.findViewById(R.id.tvOfferOfDay)).setText(bannerResponse.getToday_offer().get(0).getOffer_description());

                        rootView.findViewById(R.id.rvOfferOfDay).setOnClickListener(new View.OnClickListener() {
                            @Override
                            public void onClick(View view) {
                                ArrayList<HomeBanner.TodayOfferBean> todayOfferBeen = bannerResponse.getToday_offer();

                                Category.SubCat subCat = new Category.SubCat();
                                if (todayOfferBeen.get(0).getTrend_page() != null && todayOfferBeen.get(0).getTrend_page().equalsIgnoreCase("1")) {
                                    startActivity(new Intent(mActivity, TrendsActivity.class));
                                } else if (todayOfferBeen.get(0).getTrend_id() != null && !todayOfferBeen.get(0).getTrend_id().equalsIgnoreCase("0")) {
                                    subCat.setId(todayOfferBeen.get(0).getTrend_id());
                                    subCat.setName("TRENDS");
                                    Intent intent = new Intent(mActivity, ProductsActivity.class);
                                    try {
                                        Constants.TIP_NAME = todayOfferBeen.get(0).getTrend_caption();
                                        Constants.TIP_TOTAL = Integer.parseInt(todayOfferBeen.get(0).getTotal().toString());
                                    } catch (NullPointerException ex) {
                                        Constants.TIP_NAME = "";
                                        Constants.TIP_TOTAL = 0;
                                    }
                                    intent.putExtra(Constants.CATEGORY, subCat);
                                    startActivity(intent);
                                    // startActivity(new Intent(mActivity, TrendsActivity.class));
                                } else if (todayOfferBeen.get(0).getSale() != null && !todayOfferBeen.get(0).getSale().equalsIgnoreCase("0")) {
                                    subCat.setId(todayOfferBeen.get(0).getSale());
                                    subCat.setName("Offers_Sale");
                                    Intent intent = new Intent(mActivity, ProductsActivity.class);
                                    intent.putExtra(Constants.CATEGORY, subCat);
                                    startActivity(intent);
                                } else if (todayOfferBeen.get(0).getProduct_id() != null && !todayOfferBeen.get(0).getProduct_id().equalsIgnoreCase("0")) {
                                    Product product = new Product();
                                    product.setProduct_id(todayOfferBeen.get(0).getProduct_id());
                                    product.setProduct_name("");
                                    subCat.setId(todayOfferBeen.get(0).getProduct_id());
                                    subCat.setName("PRODUCT DETAIL");
                                    Intent intent = new Intent(mActivity, ProductViewActivity.class);
                                    intent.putExtra(Constants.PRODUCT_NAME, product);
                                    intent.putExtra(Constants.CATEGORY, subCat);
                                    startActivity(intent);
                                } else if (todayOfferBeen.get(0).getCategory_id() != null && !todayOfferBeen.get(0).getCategory_id().equalsIgnoreCase("0")) {
                                    subCat.setId(todayOfferBeen.get(0).getCategory_id());
                                    subCat.setName("Offers_Category");
                                    Intent intent = new Intent(mActivity, ProductsActivity.class);
                                    intent.putExtra(Constants.CATEGORY, subCat);
                                    startActivity(intent);
                                }
                            }
                        });

                    } else
                        rootView.findViewById(R.id.rvOfferOfDay).setVisibility(View.GONE);

                    OffersAdapter offersAdapter = new OffersAdapter(mActivity, bannerResponse, products);
                    rvOffers.setAdapter(offersAdapter);
                    offersAdapter.notifyDataSetChanged();
                    mSwipeRefreshLayout.setRefreshing(false);


                    offersAdapter.setOnBannerClickListener(new OffersAdapter.OnBannerClickListener() {

                        @Override
                        public void OnBannerClick(int position, int item_pos) {
                            Constants.debug("position  :" + position);
                            Constants.debug("Item position  :" + item_pos);

                            HomeBanner.BannersBean.DataBean dataBean = bannersBeanArrayList.get(position).getData().get(item_pos);
                            Category.SubCat subCat = new Category.SubCat();
                            if (dataBean.getTrend_page() != null && dataBean.getTrend_page().equalsIgnoreCase("1")) {
                                startActivity(new Intent(mActivity, TrendsActivity.class));
                            } else if (dataBean.getTrend_id() != null && !dataBean.getTrend_id().equalsIgnoreCase("0")) {
                                subCat.setId(dataBean.getTrend_id());
                                subCat.setName("TRENDS");
                                Intent intent = new Intent(mActivity, ProductsActivity.class);
                                try {
                                    Constants.TIP_NAME = dataBean.getTrend_caption();
                                    Constants.TIP_TOTAL = Integer.parseInt(dataBean.getTotal().toString());
                                } catch (NullPointerException ex) {
                                    Constants.TIP_NAME = "";
                                    Constants.TIP_TOTAL = 0;
                                }
                                intent.putExtra(Constants.CATEGORY, subCat);
                                startActivity(intent);
                                // startActivity(new Intent(mActivity, TrendsActivity.class));
                            } else if (dataBean.getSale() != null && !dataBean.getSale().equalsIgnoreCase("0")) {
                                subCat.setId(dataBean.getSale());
                                subCat.setName("Offers_Sale");
                                Intent intent = new Intent(mActivity, ProductsActivity.class);
                                intent.putExtra(Constants.CATEGORY, subCat);
                                startActivity(intent);
                            } else if (dataBean.getProduct_id() != null && !dataBean.getProduct_id().equalsIgnoreCase("0")) {
                                Product product = new Product();
                                product.setProduct_id(dataBean.getProduct_id());
                                product.setProduct_name("");
                                subCat.setId(dataBean.getProduct_id());
                                subCat.setName("PRODUCT DETAIL");
                                Intent intent = new Intent(mActivity, ProductViewActivity.class);
                                intent.putExtra(Constants.PRODUCT_NAME, product);
                                intent.putExtra(Constants.CATEGORY, subCat);
                                startActivity(intent);
                            } else if (dataBean.getCategory_id() != null && !dataBean.getCategory_id().equalsIgnoreCase("0")) {
                                subCat.setId(dataBean.getCategory_id());
                                subCat.setName("Offers_Category");
                                Intent intent = new Intent(mActivity, ProductsActivity.class);
                                intent.putExtra(Constants.CATEGORY, subCat);
                                startActivity(intent);
                            }
                        }

                        @Override
                        public void OnGridItemClick(View view) {
                            Product product = (Product) view.getTag();
                            Intent intent = new Intent(mActivity, ProductViewActivity.class);
                            intent.putExtra(Constants.PRODUCT_NAME, product);
                            intent.putExtra(Constants.CATEGORY, "");
                            startActivity(intent);
                        }

                        @Override
                        public void OnSeeMoreClick() {

                            Category.SubCat subCat = new Category.SubCat();
                            subCat.setId("just_sold");
                            subCat.setName("JUST SOLD");
                            Intent intent = new Intent(mActivity, ProductsActivity.class);
                            intent.putExtra(Constants.CATEGORY, subCat);
                            startActivity(intent);

                        }
                    });
                }catch (Exception e){
                    e.printStackTrace();
                }
                break;
        }
    }


    @Override
    public void onError(int error_code, int tag, Object object) {
        //super.onError(error_code, tag, object);
        switch (tag) {
            case Constants.TAG_OFFERS:
                showToast(getString(R.string.no_products));
                break;
            case Constants.TAG_JUST_SOLD:
                if (error_code == 99)
                    showToast(getString(R.string.no_products));
                break;
        }
        getActivity().findViewById(R.id.rl_loader).setVisibility(View.GONE);
    }

    @Override
    public void onRefresh() {
        rootView.findViewById(R.id.pbWheel).setVisibility(View.GONE);
        init();

    }
}
