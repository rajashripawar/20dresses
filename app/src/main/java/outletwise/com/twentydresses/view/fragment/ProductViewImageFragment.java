package outletwise.com.twentydresses.view.fragment;

import android.app.Dialog;
import android.content.Context;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.DialogFragment;
import android.support.v4.view.PagerAdapter;
import android.support.v4.view.ViewPager;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.view.animation.AccelerateDecelerateInterpolator;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.ImageView;
import android.widget.RelativeLayout;

import com.pnikosis.materialishprogress.ProgressWheel;
import com.squareup.picasso.Callback;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;

import outletwise.com.twentydresses.R;
import outletwise.com.twentydresses.controller.adapter.BaseRecyclerAdapter;
import outletwise.com.twentydresses.utilities.Constants;
import outletwise.com.twentydresses.view.custom.HackyViewPager;
import uk.co.senab.photoview.PhotoView;
import uk.co.senab.photoview.PhotoViewAttacher;

/**
 * Created by User-PC on 15-09-2015.
 */
public class ProductViewImageFragment extends DialogFragment {

    private View rootView;
    private HackyViewPager mViewPager;
    private RecyclerView recyclerView;
    private ImageRecyclerAdapter imageRecyclerAdapter;
    private boolean toggle;
    private ProgressWheel progressWheel;
    private boolean showProgress = true;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        rootView = inflater.inflate(R.layout.fragment_product_view_image, container, false);
        init();
        mViewPager.addOnPageChangeListener(new ViewPager.OnPageChangeListener() {
            @Override
            public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {
                imageRecyclerAdapter.setSelectedPos(position);
                imageRecyclerAdapter.notifyDataSetChanged();

            }

            @Override
            public void onPageSelected(int position) {

            }

            @Override
            public void onPageScrollStateChanged(int state) {

            }
        });
        return rootView;
    }

    void init() {
        ArrayList<String> zoomImages = getArguments().getStringArrayList(Constants.PRODUCT_ZOOM_IMAGES);
        ArrayList<String> thumbImages = getArguments().getStringArrayList(Constants.PRODUCT_THUMB_IMAGES);
        imageRecyclerAdapter = new ImageRecyclerAdapter(getActivity(), thumbImages);
        mViewPager = (HackyViewPager) rootView.findViewById(R.id.vpProductImage);
        recyclerView = (RecyclerView) rootView.findViewById(R.id.rvProductImage);
        recyclerView.setLayoutManager(new LinearLayoutManager(recyclerView.getContext(), LinearLayoutManager.HORIZONTAL, false));
        recyclerView.setAdapter(imageRecyclerAdapter);
        mViewPager.setAdapter(new ImagePagerAdapter(getActivity(), zoomImages));
        setUpProgressBar();

        rootView.findViewById(R.id.img_cancel).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dismiss();
            }
        });
    }

    private void setUpProgressBar() {
        progressWheel = (ProgressWheel) rootView.findViewById(R.id.pbWheel);
        if (progressWheel != null)
            progressWheel.setBarColor(getResources().getColor(R.color.color_accent));
    }

    void toggleRecyclerView() {
        Animation animation;
        toggle = !toggle;
        if (toggle) {
            animation = AnimationUtils.loadAnimation(getContext(),
                    R.anim.bottom_down);
            animation.setInterpolator(new AccelerateDecelerateInterpolator());
            recyclerView.startAnimation(animation);
            recyclerView.setVisibility(View.GONE);
        } else {
            animation = AnimationUtils.loadAnimation(getContext(),
                    R.anim.bottom_up);
            animation.setInterpolator(new AccelerateDecelerateInterpolator());
            recyclerView.startAnimation(animation);
            recyclerView.setVisibility(View.VISIBLE);
        }
    }

    private class ImageRecyclerAdapter extends BaseRecyclerAdapter {
        private int selectedPos = 0;

        class ImageViewHolder extends BaseViewHolder {
            public ImageView imageView;

            public ImageViewHolder(View itemView) {
                super(itemView);
                imageView = (ImageView) itemView.findViewById(R.id.ivProductImage);
            }

            @Override
            public void onClick(View v) {
                selectedPos = (int) v.getTag();
                mViewPager.setCurrentItem(selectedPos);
                notifyDataSetChanged();
            }
        }

        ImageRecyclerAdapter(Context context, ArrayList items) {
            super(context, items);
        }

        @Override
        public BaseViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
            View view = LayoutInflater.from(parent.getContext())
                    .inflate(R.layout.layout_recyclerview_product_image_zoom, parent, false);
            return new ImageViewHolder(view);
        }

        @Override
        public void onBindViewHolder(BaseViewHolder holder, int position) {
            holder.itemView.setTag(position);
            ImageView imageView = ((ImageViewHolder) holder).imageView;
            Picasso.with(context).load((String) items.get(position)).into(imageView);

            if (position == selectedPos)
                holder.itemView.setSelected(true);
            else
                holder.itemView.setSelected(false);
        }

        public void setSelectedPos(int selectedPos) {
            this.selectedPos = selectedPos;
        }
    }

    private class ImagePagerAdapter extends PagerAdapter {

        private ArrayList<String> imageThumbnails;
        private Context context;


        ImagePagerAdapter(Context context, ArrayList<String> imageThumbnails) {
            this.imageThumbnails = imageThumbnails;
            this.context = context;
        }

        private void showProgress() {
            if (!showProgress)
                return;

            if (progressWheel == null) {
                setUpProgressBar();
            }

            if (progressWheel != null) {
                progressWheel.bringToFront();
                progressWheel.setVisibility(View.VISIBLE);
            }
        }

        private void hideProgress() {
            if (progressWheel != null)
                progressWheel.setVisibility(View.GONE);
        }

        @Override
        public int getCount() {
            return imageThumbnails.size();
        }

        @Override
        public View instantiateItem(ViewGroup container, final int position) {
            //  showProgress();
            PhotoView photoView = new PhotoView(container.getContext());

            Picasso.with(context).load(imageThumbnails.get(position)).into(photoView, new Callback() {
                @Override
                public void onSuccess() {
                    //  hideProgress();
                    progressWheel.setVisibility(View.GONE);
                }

                @Override
                public void onError() {

                }


            });


            photoView.setOnViewTapListener(new PhotoViewAttacher.OnViewTapListener() {
                @Override
                public void onViewTap(View view, float v, float v1) {
                    toggleRecyclerView();
                }
            });
            container.addView(photoView, ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.MATCH_PARENT);

            return photoView;
        }

        @Override
        public void destroyItem(ViewGroup container, int position, Object object) {
            container.removeView((View) object);
        }

        @Override
        public boolean isViewFromObject(View view, Object object) {
            return view == object;
        }
    }

    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {
        final RelativeLayout root = new RelativeLayout(getActivity());
        root.setLayoutParams(new ViewGroup.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.MATCH_PARENT));

        Dialog dialog = new Dialog(getActivity());
        dialog.getWindow().requestFeature(Window.FEATURE_NO_TITLE);
        dialog.setContentView(root);
        dialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.WHITE));
        dialog.getWindow().setLayout(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.MATCH_PARENT);

        return dialog;
    }
}
