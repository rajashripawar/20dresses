package outletwise.com.twentydresses.view.activity;

import android.os.Build;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.view.ViewPager;
import android.support.v7.app.ActionBar;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.astuetz.PagerSlidingTabStrip;

import java.util.ArrayList;
import java.util.List;

import outletwise.com.twentydresses.R;
import outletwise.com.twentydresses.controller.adapter.ReferTabAdapter;
import outletwise.com.twentydresses.model.database.greenbot.User;
import outletwise.com.twentydresses.utilities.Constants;
import outletwise.com.twentydresses.view.fragment.InviteFaqFragment;
import outletwise.com.twentydresses.view.fragment.InviteFragment;
import outletwise.com.twentydresses.view.fragment.InviteRewardsFragment;

/**
 * This class is use to invite friends
 */

public class InviteActivity extends BaseActivity {
    private final List<Fragment> mFragments = new ArrayList<>();
    private LinearLayout mTabsLinearLayout;
    private PagerSlidingTabStrip pagerTabStrip;
    private ViewPager pager;
    private User user;
    private Long user_id;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_invite);
        setUpToolBar(getString(R.string.toolbar_invite));
        final ActionBar ab = getSupportActionBar();
        ab.setDisplayHomeAsUpEnabled(true);

    }

    private void setPager() {
        pagerTabStrip = (PagerSlidingTabStrip) findViewById(R.id.pager_title_strip);
        pager = (ViewPager) findViewById(R.id.pager);

        //To evenly place tabs sarika
//        pagerTabStrip.setShouldExpand(true);

        //To set indicator color of tab
       /* if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            pagerTabStrip.setIndicatorColor(getColor(R.color.color_accent));
        } else
            pagerTabStrip.setIndicatorColorResource(R.color.color_accent);
        pagerTabStrip.setIndicatorHeight(5);*/

        // Set fragments to viewpager
        Fragment f1 = new InviteFragment();
//        Fragment f2 = new InviteRewardsFragment();
//        Fragment f3 = new InviteFaqFragment();

        Bundle b = new Bundle();
        b.putLong(Constants.USER_ID, user_id);

        f1.setArguments(b);
//        f2.setArguments(b);
//        f3.setArguments(b);

        mFragments.add(f1);
//        mFragments.add(f2);
//        mFragments.add(f3);

        ReferTabAdapter adapter = new ReferTabAdapter(getSupportFragmentManager());
        adapter.addFragments(mFragments);
        pager.setAdapter(adapter);
        pager.setOffscreenPageLimit(2);
//        pagerTabStrip.setViewPager(pager);
        //To set selected color for first tab
//        setUpTabStrip();

        pager.addOnPageChangeListener(new ViewPager.OnPageChangeListener() {
            @Override
            public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {

            }

            @Override
            public void onPageSelected(int position) {

                for (int i = 0; i < mTabsLinearLayout.getChildCount(); i++) {
                    TextView tv = (TextView) mTabsLinearLayout.getChildAt(i);
                    if (i == position) {
                        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M)
                            tv.setTextColor(getColor(R.color.color_accent));
                        else
                            tv.setTextColor(getApplicationContext().getResources().getColor(R.color.color_accent));
                    } else {
                        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M)
                            tv.setTextColor(getColor(R.color.md_grey_900));
                        else
                            tv.setTextColor(getApplicationContext().getResources().getColor(R.color.md_grey_900));
                    }
                }
            }

            @Override
            public void onPageScrollStateChanged(int state) {

            }
        });
    }


    public void setUpTabStrip() {

        mTabsLinearLayout = ((LinearLayout) pagerTabStrip.getChildAt(0));
        for (int i = 0; i < mTabsLinearLayout.getChildCount(); i++) {
            TextView tv = (TextView) mTabsLinearLayout.getChildAt(i);

            if (i == 0) {
                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M)
                    tv.setTextColor(getColor(R.color.color_accent));
                else
                    tv.setTextColor(getApplicationContext().getResources().getColor(R.color.color_accent));
            } else {
                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M)
                    tv.setTextColor(getColor(R.color.md_grey_900));
                else
                    tv.setTextColor(getApplicationContext().getResources().getColor(R.color.md_grey_900));
            }
        }

    }

    @Override
    public void onCompleteRetrieval(int tag, ArrayList obj, String msg) {
        switch (tag) {
            case Constants.TAG_USER:
                if (obj != null)
                user = (User) obj.get(0);
                user_id = user.getUser_id();
                setPager();
                break;
        }
    }

    @Override
    public void onError(int error_code, int tag, Object object) {
        super.onError(error_code, tag, object);
        checkErrorCode(error_code, object);
    }
}
