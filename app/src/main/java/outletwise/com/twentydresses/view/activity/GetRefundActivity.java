package outletwise.com.twentydresses.view.activity;

import android.os.Bundle;
import android.support.v7.app.ActionBar;
import android.support.v7.widget.CardView;
import android.text.TextUtils;
import android.view.View;
import android.widget.AdapterView;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.LinearLayout;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.Spinner;
import android.widget.TextView;

import com.andexert.expandablelayout.library.ExpandableLayout;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

import info.hoang8f.android.segmented.SegmentedGroup;
import outletwise.com.twentydresses.R;
import outletwise.com.twentydresses.controller.adapter.ExistingBankWalletItemAdapter;
import outletwise.com.twentydresses.model.Refund;
import outletwise.com.twentydresses.model.VisitorResponse;
import outletwise.com.twentydresses.model.WalletResponse;
import outletwise.com.twentydresses.utilities.Constants;
import outletwise.com.twentydresses.utilities.Urls;

/**
 * This class is use to get wallet refund amount
 */
public class GetRefundActivity extends BaseActivity {
    private WalletResponse response;
    private CardView cvCredit;
    private CardView cvBank;
    private ExpandableLayout elCredit;
    private ExpandableLayout elBank;
    private SegmentedGroup sgBankDetail;
    private CheckBox cbCredit;
    private CheckBox cbBank;
    private Refund.RefundEntity refundEntity = new Refund.RefundEntity();
    private Refund refund = new Refund();
    private TextView tvBankName, tvAccHolderName, tvAccNo, tvBranchName, tvIFSCCode;
    private Spinner spAccType;
    private LinearLayout llNewBankDetails;
    private Refund.RefundEntity.NewBankDetailsEntity bankDetailsEntity = new Refund.RefundEntity.NewBankDetailsEntity();
    private int checkedId;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_get_refund);

        setUpToolBar(getString(R.string.toolbar_getrefund));
        final ActionBar ab = getSupportActionBar();
        ab.setDisplayHomeAsUpEnabled(true);


        cvCredit = (CardView) findViewById(R.id.cvCredit);
        cvBank = (CardView) findViewById(R.id.cvBank);

        elCredit = (ExpandableLayout) cvCredit.findViewById(R.id.elCredit);
        elBank = (ExpandableLayout) cvBank.findViewById(R.id.elBankAccount);

        findViewById(R.id.bProceed).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                if (checkedId == R.id.rbNew && validateNewBankDetails()) {
                    refundEntity.setBank_id(0);
                    refundEntity.setNew_bank_details(getNewBankDetails());
                    refundWallet();
                } else if (checkedId == R.id.rbExist) {
                    if (response.getRefund_types().get(1).getBank_details() != null)
                        refundWallet();

                } else if (cbCredit.isChecked()) {
                    refundEntity.setBank_id(0);
                    refundEntity.setNew_bank_details(null);
                    refundWallet();
                }
            }
        });

        findViewById(R.id.bCancel).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });

    }

    private void refundWallet() {
        refund.setRefund(refundEntity);
        refund.setUser(String.valueOf(user.getUser_id()));
        refund.setAuth_token(preferences.getString(Constants.KEY_TOKEN, ""));

        postJsonData(Urls.REFUND_WALLET, refund, Constants.TAG_REFUND_WALLET, true);
    }


    @Override
    public void onCompleteRetrieval(int tag, ArrayList obj, String msg) {
        super.onCompleteRetrieval(tag, obj, msg);

        JSONObject jsonObject = new JSONObject();
        try {
            jsonObject.put(Constants.USER_ID, user.getUser_id());
            jsonObject.put(Constants.AUTH_TOKEN, preferences.getString(Constants.KEY_TOKEN, ""));
        } catch (JSONException e) {
            e.printStackTrace();
        }

        postJsonData(Urls.USER_WALLET, jsonObject, Constants.TAG_WALLET, false);
    }

    @Override
    public <T> void onResponse(T obj, int tag) {
        super.onResponse(obj, tag);
        switch (tag) {
            case Constants.TAG_WALLET:
                findViewById(R.id.pbWheel).setVisibility(View.GONE);
                response = (WalletResponse) obj;
                initUI();

                if (response.getRefund_types().get(0).isCredit()) {
                    findViewById(R.id.cvCredit).setVisibility(View.VISIBLE);
                    TextView txtHeader = (TextView) findViewById(R.id.tvExpandableHeader);
                    txtHeader.setText("Credit");

                    TextView txtContent = (TextView) findViewById(R.id.tvExpandableContent);
                    txtContent.setText(response.getRefund_types().get(0).getCredit_text());
                }

                break;

            case Constants.TAG_REFUND_WALLET:
                VisitorResponse refundResponse = (VisitorResponse) obj;
                if (refundResponse.getStatus().equalsIgnoreCase(Constants.RESPONSE_SUCCESS)) {
                    showToast(refundResponse.getMessage());
                    finish();
                } else
                    showToast(refundResponse.getMessage());

                break;
        }
    }

    private void initUI() {

        cvCredit = (CardView) findViewById(R.id.cvCredit);
        cvBank = (CardView) findViewById(R.id.cvBank);

        elCredit = (ExpandableLayout) cvCredit.findViewById(R.id.elCredit);
        elBank = (ExpandableLayout) cvBank.findViewById(R.id.elBankAccount);

        cbCredit = (CheckBox) elCredit.findViewById(R.id.checkBox);
        cbBank = (CheckBox) elBank.findViewById(R.id.checkBox);

        if (response.getRefund_types().get(0).isCredit()) {
            elCredit.show();
            cbCredit.setChecked(true);
            refundEntity.setRefund_type("credit");
        }

        ((TextView) elBank.getHeaderLayout().findViewById(R.id.tvExpandableHeader)).setText(getString(R.string.text_bank_account));

        sgBankDetail = (SegmentedGroup) elBank.findViewById(R.id.sgBank);
        sgBankDetail.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(RadioGroup group, int checkedId) {
                toggleVisibility(checkedId);
            }
        });


        cbCredit.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                if (isChecked) {
                    elCredit.show();
                    cbBank.setChecked(false);
                    refundEntity.setRefund_type("credit");
                    refundEntity.setBank_id(0);
                    checkedId = R.id.checkBox;
                } else {
                    if (elCredit.isOpened())
                        elCredit.hide();
                    refundEntity.setRefund_type("bank_deposit");
                }

            }
        });

        cbBank.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                refundEntity.setRefund_type("bank_deposit");
                if (isChecked) {
                    elBank.show();
                    cbCredit.setChecked(false);
                    checkedId = R.id.rbExist;
                    if (response.getRefund_types().get(1).isBank()) {
                        findViewById(R.id.tvNoBanDetails).setVisibility(View.GONE);
                        refundEntity.setBank_id(response.getRefund_types().get(1).getBank_details().get(0).getBank_id());
                        refundEntity.setBank_deposit("existing");
                    } else {
                        findViewById(R.id.tvNoBanDetails).setVisibility(View.VISIBLE);
                        ((RadioButton) findViewById(R.id.rbNew)).setChecked(true);
                        toggleVisibility(R.id.rbNew);
                    }

                } else {
                    if (elBank.isOpened())
                        elBank.hide();
                }
            }
        });

        existingAddress();
        newAddress();
    }

    private void newAddress() {
        //New Address Fields
        llNewBankDetails = (LinearLayout) findViewById(R.id.llNewBankDetails);
        tvAccHolderName = (TextView) llNewBankDetails.findViewById(R.id.tvAccHolderName);
        tvAccNo = (TextView) llNewBankDetails.findViewById(R.id.tvAccNo);
        tvBankName = (TextView) llNewBankDetails.findViewById(R.id.tvBankName);
        tvBranchName = (TextView) llNewBankDetails.findViewById(R.id.tvBranchName);
        tvIFSCCode = (TextView) llNewBankDetails.findViewById(R.id.tvIFSCCode);

        spAccType = (Spinner) llNewBankDetails.findViewById(R.id.spAccType);


        spAccType.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                bankDetailsEntity.setBank_account_type((String) parent.getAdapter().getItem(position));
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });
    }

    public Refund.RefundEntity.NewBankDetailsEntity getNewBankDetails() {
        //  Refund.RefundEntity.NewBankDetailsEntity bankDetailsEntity = null;
        if (validateNewBankDetails()) {
            //  bankDetailsEntity = new Refund.RefundEntity.NewBankDetailsEntity();
            bankDetailsEntity.setBank_name(tvBankName.getText().toString().trim());
            bankDetailsEntity.setBank_account_name(tvAccHolderName.getText().toString().trim());
            bankDetailsEntity.setBank_ifsc_code(tvIFSCCode.getText().toString().trim());
            bankDetailsEntity.setBank_account_no(tvAccNo.getText().toString().trim());
            bankDetailsEntity.setBank_branch_name(tvBranchName.getText().toString().trim());
        }
        return bankDetailsEntity;
    }


    boolean validateNewBankDetails() {
        if (TextUtils.isEmpty(tvBankName.getText().toString().trim())) {
            showToast(getString(R.string.enter_bankname));
            tvBankName.requestFocus();
            return false;
        } else if (TextUtils.isEmpty(tvAccHolderName.getText().toString().trim())) {
            showToast(getString(R.string.enter_accholder));
            tvAccHolderName.requestFocus();
            return false;
        } else if (spAccType.getSelectedItemId() == 0) {
            showToast(getString(R.string.select_acctype));
            spAccType.requestFocus();
            return false;
        } else if (TextUtils.isEmpty(tvAccNo.getText().toString().trim())) {
            showToast(getString(R.string.enter_accno));
            tvAccNo.requestFocus();
            return false;
        } else if (TextUtils.isEmpty(tvIFSCCode.getText().toString().trim())) {
            showToast(getString(R.string.enter_ifsc_code));
            tvIFSCCode.requestFocus();
            return false;
        } else if (TextUtils.isEmpty(tvBranchName.getText().toString().trim())) {
            showToast(getString(R.string.enter_branchname));
            tvBranchName.requestFocus();
            return false;
        }
        return true;
    }

    private void existingAddress() {

        LinearLayout llBankDetails = (LinearLayout) elBank.findViewById(R.id.llBankDetails);
        ExistingBankWalletItemAdapter bankItemsAdapter = new ExistingBankWalletItemAdapter(getApplicationContext(),
                response.getRefund_types().get(1).getBank_details(), llBankDetails);

        bankItemsAdapter.setBankChanged(new ExistingBankWalletItemAdapter.BankChanged() {
            @Override
            public void onBankChange(WalletResponse.RefundTypesEntity.BankDetailsEntity bank) {

                refundEntity.setBank_id(bank.getBank_id());
            }

        });
    }


    void toggleVisibility(int checkedId) {
        refundEntity.setRefund_type("bank_deposit");
        if (checkedId == R.id.rbExist) {
            if (response.getRefund_types().get(1).isBank())
                findViewById(R.id.tvNoBanDetails).setVisibility(View.GONE);
            else
                findViewById(R.id.tvNoBanDetails).setVisibility(View.VISIBLE);

            findViewById(R.id.llBankDetails).setVisibility(View.VISIBLE);
            findViewById(R.id.llNewBankDetails).setVisibility(View.GONE);
            refundEntity.setBank_deposit("existing");
        } else if (checkedId == R.id.rbNew) {
            findViewById(R.id.tvNoBanDetails).setVisibility(View.GONE);
            findViewById(R.id.llBankDetails).setVisibility(View.GONE);
            findViewById(R.id.llNewBankDetails).setVisibility(View.VISIBLE);
            refundEntity.setBank_deposit("new");
        }

        this.checkedId = checkedId;
    }

}
