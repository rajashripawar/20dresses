package outletwise.com.twentydresses.view.fragment;

import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.widget.CardView;
import android.text.Editable;
import android.text.TextUtils;
import android.text.TextWatcher;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.Spinner;
import android.widget.TextView;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import info.hoang8f.android.segmented.SegmentedGroup;
import outletwise.com.twentydresses.R;
import outletwise.com.twentydresses.controller.adapter.AddressItemsAdapter;
import outletwise.com.twentydresses.model.Address;
import outletwise.com.twentydresses.model.OrderReturnResponse;
import outletwise.com.twentydresses.model.ReturnExchangeRequest;
import outletwise.com.twentydresses.model.VisitorResponse;
import outletwise.com.twentydresses.utilities.Constants;
import outletwise.com.twentydresses.utilities.Urls;
import outletwise.com.twentydresses.view.activity.BaseActivity;
import outletwise.com.twentydresses.view.activity.EditAddressActivity;
import outletwise.com.twentydresses.view.activity.LoginActivity;

/**
 * Created by User-PC on 04-11-2015.
 */
public class ReturnAddressFragment extends BaseFragment {

    private View rootView;
    private SegmentedGroup addressGroup;
    private LinearLayout llAddress;
    private OrderReturnResponse orderReturnResponse;
    private ReturnExchangeRequest returnExchangeRequest;
    private CardView cvExistingAddress;
    private CheckBox cbExisting;
    private LinearLayout llNewAddress;
    private TextView tvPinCode, tvFirstName, tvLastName, tvAddress, tvCity, tvMobile;
    private Spinner tvState;
    private ReturnExchangeRequest.AddressEntity addressEntity;
    private List<Address> addresses;
    private Address selectedAddress; //Only for Delivery order
    private AddressItemsAdapter addressItemsAdapter;
    private int checkedId;
    private OrderReturnResponse returnResponse;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        rootView = inflater.inflate(R.layout.fragment_order_select_address, container, false);
        getToolBar().setTitle(getString(R.string.toolbar_address));
        init();

        return rootView;
    }

    private void getExistingAddress() {
        JSONObject jsonObject = new JSONObject();
        try {
            jsonObject.put(Constants.USER_ID, ((BaseActivity) mActivity).user.getUser_id());
            jsonObject.put(Constants.AUTH_TOKEN, ((BaseActivity) mActivity).preferences.getString(Constants.KEY_TOKEN, ""));
            jsonObject.put(Constants.ORDER, Constants.ORDER_ID_RETURN);
        } catch (Exception e) {
            e.printStackTrace();
        }
        postJsonData(Urls.ORDER_RETURN, jsonObject, Constants.TAG_ORDER_RETURN, true);
    }

    void init() {
        this.checkedId = R.id.rbExist;
        getExistingAddress();
        initForReturnExchange();

        llAddress = (LinearLayout) rootView.findViewById(R.id.llAddress);
        addressGroup = (SegmentedGroup) rootView.findViewById(R.id.sgAddress);
        addressGroup.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener() {

            @Override
            public void onCheckedChanged(RadioGroup group, int checkedId) {

                toggleVisibility(checkedId);
            }
        });

        if (returnExchangeRequest == null) {
            addresses = new ArrayList<>();
        } else {

            if (orderReturnResponse != null && orderReturnResponse.getAddress() != null && orderReturnResponse.getAddress().size() > 0 && orderReturnResponse.getAddress().get(0).getUser_addresses() != null)
                addresses = orderReturnResponse.getAddress().get(0).getUser_addresses();
        }

        addressItemsAdapter = new AddressItemsAdapter(mActivity, addresses
                , llAddress);

        addressItemsAdapter.setAddressChanged(new AddressItemsAdapter.AddressChanged() {
            @Override
            public void onAddressChange(Address address) {
                cbExisting.setChecked(false);
                if (returnExchangeRequest != null && addressEntity != null) {
                    addressEntity.setUser_addresses(true);
                    addressEntity.setAddress_id(address.getAddress_id());
                    addressEntity.setAddressType(Constants.AddressType.previous);
                    returnExchangeRequest.setAddress(addressEntity);
                } else
                    selectedAddress = address;
            }

            @Override
            public void onEditAddress(Address address) {
                Intent intent = new Intent(mActivity, EditAddressActivity.class);
                intent.putExtra(Constants.ADDRESS, address);
                startActivityForResult(intent, 1);
            }

            @Override
            public void onDeleteAddress(final Address address) {
                AlertDialog.Builder builder1 = new AlertDialog.Builder(mActivity);
                builder1.setMessage(getString(R.string.text_delete_address));
                builder1.setCancelable(false);

                builder1.setPositiveButton(
                        "Yes",
                        new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int id) {

                                JSONObject jsonObject = new JSONObject();
                                try {

                                    jsonObject.put(Constants.USER_ID, ((BaseActivity) mActivity).user.getUser_id());
                                    jsonObject.put(Constants.AUTH_TOKEN, ((BaseActivity) mActivity).preferences.getString(Constants.KEY_TOKEN, ""));
                                    jsonObject.put(Constants.ADDRESS_ID, address.getAddress_id());
                                } catch (JSONException e) {
                                    e.printStackTrace();
                                }
                                postJsonData(Urls.ADDRESS_DELETE, jsonObject, Constants.TAG_DEL_ADDRESS, true);

                            }
                        });

                builder1.setNegativeButton(
                        "No",
                        new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int id) {
                                dialog.cancel();
                            }
                        });

                AlertDialog alert11 = builder1.create();
                alert11.show();
            }
        });

        cvExistingAddress = (CardView) rootView.findViewById(R.id.cvExisting);
        cbExisting = ((CheckBox) cvExistingAddress.findViewById(R.id.checkBox));
        cbExisting.setChecked(true);

        cbExisting.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (cbExisting.isChecked()) {
                    addressEntity.setAddressType(Constants.AddressType.same);
                    addressItemsAdapter.setFirstAddressCheck(false);
                } else {
                    if (returnExchangeRequest != null) {
                        addressEntity.setAddressType(null);
                        returnExchangeRequest.setAddress(addressEntity);
                    }
                }
            }
        });

        if (returnExchangeRequest != null) {
            if (addresses != null) {
                Address del_address = addresses.get(0);
                ((TextView) cvExistingAddress.findViewById(R.id.tvAddress)).
                        setText(del_address.getAddress());
                ((TextView) cvExistingAddress.findViewById(R.id.tvName)).
                        setText(del_address.getFirstname() + " " + del_address.getLastname());
                ((TextView) cvExistingAddress.findViewById(R.id.tvMobile)).
                        setText(del_address.getMobile());

                ((ImageView) cvExistingAddress.findViewById(R.id.iv_edit)).setVisibility(View.GONE);

                cbExisting.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
                    @Override
                    public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                        if (isChecked) {
                            addressItemsAdapter.clearCheck();
                            if (returnExchangeRequest != null && addressEntity != null) {
                                returnExchangeRequest.setAddress(addressEntity);
                                addressEntity.setDelhivery_address(true);
                            }
                        } else {
                            if (returnExchangeRequest != null) {
                                addressEntity.setAddressType(null);
                                returnExchangeRequest.setAddress(addressEntity);
                            }
                        }
                    }
                });
            }
        } else {
            cvExistingAddress.setVisibility(View.GONE);
            rootView.findViewById(R.id.tvSameAsShipping).setVisibility(View.GONE);
            rootView.findViewById(R.id.tvPreviousAddress).setVisibility(View.GONE);
            addressItemsAdapter.setFirstAddressCheck(false);
        }

        //New Address Fields
        llNewAddress = (LinearLayout) rootView.findViewById(R.id.llNewAddress);
        tvPinCode = (TextView) llNewAddress.findViewById(R.id.tvPinCode);
        tvFirstName = (TextView) llNewAddress.findViewById(R.id.tvFirstName);
        tvLastName = (TextView) llNewAddress.findViewById(R.id.tvLastName);
        // tvEmail = (TextView) llNewAddress.findViewById(R.id.tvEmail);
        tvAddress = (TextView) llNewAddress.findViewById(R.id.tvAddress);
        tvCity = (TextView) llNewAddress.findViewById(R.id.tvCity);
        tvState = (Spinner) llNewAddress.findViewById(R.id.tvState);
        tvMobile = (TextView) llNewAddress.findViewById(R.id.tvMobile);

        tvFirstName.setText(((BaseActivity) mActivity).user.getUser_first_name());
        tvLastName.setText(((BaseActivity) mActivity).user.getUser_last_name());
        if (((BaseActivity) mActivity).user.getUser_mobile() != null)
            tvMobile.setText(((BaseActivity) mActivity).user.getUser_mobile());

        rootView.findViewById(R.id.llNewAddress).setVisibility(View.GONE);

        ArrayList<String> simpleState = new ArrayList<>();
        simpleState.add("Select state");
        ArrayAdapter<String> adapter = new ArrayAdapter<String>(getContext(), R.layout.layout_spinner, simpleState);
        tvState.setAdapter(adapter);

        tvPinCode.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {
                String pincode = tvPinCode.getText().toString();
                if (pincode.length() == 6) {
                    Constants.pincodeTextchange = true;
                   /* HashMap<String, String> params = new HashMap<>();
                    params.put("pincode", pincode);
                    postStringData(Urls.CHK_DEL_OPTS, params, Constants.TAG_CHK_DEL_OPTS, false);*/

                    JSONObject jsonObject = new JSONObject();
                    try {
                        jsonObject.put(Constants.PINCODE, pincode);

                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                    postJsonData(Urls.CHK_DEL_OPTS, jsonObject, Constants.TAG_CHK_DEL_OPTS, false);
                } else {
                    showToast(getString(R.string.text_invalid_pincode));
                 /*   Drawable img = getContext().getResources().getDrawable(R.drawable.pincode_not_valid);
                    EditText edtText = (EditText) rootView.findViewById(R.id.tvPinCode);
                    edtText.setCompoundDrawablesWithIntrinsicBounds(null, null, img, null);*/
                }
            }

            @Override
            public void afterTextChanged(Editable editable) {

            }
        });


    }

    @Override
    public <T> void onResponse(T obj, int tag) {
        super.onResponse(obj, tag);
        switch (tag) {
            case Constants.TAG_ADDRESS_VIEW:
                rootView.findViewById(R.id.scrollViewAddress).setVisibility(View.VISIBLE);
                addresses = Arrays.asList((Address[]) obj);
                if (addresses.size() > 0)
                    selectedAddress = addresses.get(0);

                addressItemsAdapter.setAddressList(addresses);
                addressItemsAdapter.notifyDataSetChanged();
                addressItemsAdapter.setFirstAddressCheck(false);
                break;

            case Constants.TAG_ORDER_RETURN:
                rootView.findViewById(R.id.scrollViewAddress).setVisibility(View.VISIBLE);
                //     returnResponse = (OrderReturnResponse) obj;
                List<OrderReturnResponse.AddressEntity> userAddress = ((OrderReturnResponse) obj).getAddress();
                setDeliveryAddress(userAddress.get(0).getDelhivery_address());

                if (userAddress.get(0).getUser_addresses() != null) {
                    addresses = userAddress.get(0).getUser_addresses();
                    if (addresses.size() > 0)
                        selectedAddress = addresses.get(0);

                    addressItemsAdapter.setAddressList(addresses);
                    addressItemsAdapter.notifyDataSetChanged();
                    addressItemsAdapter.setFirstAddressCheck(false);
                } else
                    rootView.findViewById(R.id.tvPreviousAddress).setVisibility(View.GONE);
                break;
            case Constants.TAG_DEL_ADDRESS:
                VisitorResponse response = (VisitorResponse) obj;
                if (response.getStatus().equalsIgnoreCase(Constants.RESPONSE_SUCCESS))
                    init();
                break;
        }
    }

    private void setDeliveryAddress(List<Address> del_address) {

        ((TextView) cvExistingAddress.findViewById(R.id.tvAddress)).
                setText(del_address.get(0).getAddress());
        ((TextView) cvExistingAddress.findViewById(R.id.tvName)).
                setText(del_address.get(0).getFirstname() + " " + del_address.get(0).getLastname());
        ((TextView) cvExistingAddress.findViewById(R.id.tvMobile)).
                setText("Mobile No. : " + del_address.get(0).getMobile());
        cbExisting.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                if (isChecked) {
                    addressItemsAdapter.clearCheck();
                    if (returnExchangeRequest != null && addressEntity != null) {
                        returnExchangeRequest.setAddress(addressEntity);
                        addressEntity.setDelhivery_address(true);
                    }
                } else {
                    if (returnExchangeRequest != null) {
                        addressEntity.setAddressType(null);
                        returnExchangeRequest.setAddress(addressEntity);
                    }
                }
            }
        });
    }

    void initForReturnExchange() {
        try {
            addressEntity = getArguments().getParcelable(ReturnExchangeRequest.AddressEntity.class.getSimpleName());
            orderReturnResponse = getArguments().getParcelable(OrderReturnResponse.class.getSimpleName());
            returnExchangeRequest = getArguments().getParcelable(ReturnExchangeRequest.class.getSimpleName());
            addressEntity.setDelhivery_address(true);
            addressEntity.setAddressType(Constants.AddressType.same);
            returnExchangeRequest.setAddress(addressEntity);
        } catch (NullPointerException e) {
            e.printStackTrace();
        }
    }


    void toggleVisibility(int checkedId) {
        this.checkedId = checkedId;
        if (checkedId == R.id.rbExist) {
            if (addresses != null && addresses.size() > 0)
                selectedAddress = addresses.get(0);

            rootView.findViewById(R.id.llExistingAddress).setVisibility(View.VISIBLE);
            rootView.findViewById(R.id.llNewAddress).setVisibility(View.GONE);
            if (returnExchangeRequest != null) {
                returnExchangeRequest.getAddress().setNew_address(false);
                returnExchangeRequest.getAddress().setDelhivery_address(true);
            }
        } else if (checkedId == R.id.rbNew) {
            rootView.findViewById(R.id.llExistingAddress).setVisibility(View.GONE);
            rootView.findViewById(R.id.llNewAddress).setVisibility(View.VISIBLE);
            if (returnExchangeRequest != null) {
                returnExchangeRequest.getAddress().setNew_address(true);
                returnExchangeRequest.getAddress().setDelhivery_address(false);
            } else
                selectedAddress = null;
        }
    }

    public Address getNewAddress() {
        Address address = null;
        if (checkedId == R.id.rbNew) {
            if (validateAddress()) {
                address = new Address();
                address.setFirstname(tvFirstName.getText().toString().trim());
                address.setLastname(tvLastName.getText().toString().trim());
                address.setAddress(tvAddress.getText().toString().trim());
                address.setCity(tvCity.getText().toString().trim());
                address.setState(tvState.getSelectedItem().toString().trim());
                address.setPincode(tvPinCode.getText().toString().trim());
                address.setMobile(tvMobile.getText().toString().trim());


                // Set new address details
                returnExchangeRequest.getAddress().setNew_address_details(address);
                returnExchangeRequest.getAddress().setAddressType(Constants.AddressType.new_address);
            }
        } else if (checkedId == R.id.rbExist)
            showToast(getString(R.string.add_address));

        return address;
    }

    boolean validateAddress() {
        if (TextUtils.isEmpty(tvPinCode.getText().toString().trim())) {
            showToast(getString(R.string.enter_pincode));
            tvPinCode.requestFocus();
            return false;
        } else if (TextUtils.isEmpty(tvFirstName.getText().toString().trim())) {
            showToast(getString(R.string.enter_first_name));
            tvFirstName.requestFocus();
            return false;
        } else if (TextUtils.isEmpty(tvLastName.getText().toString().trim())) {
            showToast(getString(R.string.enter_last_name));
            tvLastName.requestFocus();
            return false;
        } else if (TextUtils.isEmpty(tvCity.getText().toString().trim())) {
            showToast(getString(R.string.enter_city));
            tvCity.requestFocus();
            return false;
        } else if (TextUtils.isEmpty(tvState.getSelectedItem().toString().trim()) || tvState.getSelectedItem().toString().equalsIgnoreCase("Select State")) {
            showToast(getString(R.string.select_state));
            tvState.requestFocus();
            return false;
        } else if (TextUtils.isEmpty(tvAddress.getText().toString().trim())) {
            showToast(getString(R.string.enter_address));
            tvAddress.requestFocus();
            return false;
        } else if (TextUtils.isEmpty(tvMobile.getText().toString().trim()) || tvMobile.getText().length() != 10) {
            showToast(getString(R.string.invalid_mob));
            tvMobile.requestFocus();
            return false;
        }
        return true;
    }

    private boolean isValidEmail(String target) {
        return !TextUtils.isEmpty(target) && android.util.Patterns.EMAIL_ADDRESS.matcher(target).matches();
    }

    public ReturnExchangeRequest getReturnExchangeRequest() {
        return returnExchangeRequest;
    }

    public OrderReturnResponse getOrderReturnResponse() {
        return orderReturnResponse;
    }

    public boolean validate() {
     /*   if (returnExchangeRequest.getAddress() == null) {
            showToast("Please select address");
            return false;
        } else if (returnExchangeRequest.getAddress().getNew_address()) {
            getNewAddress();
        }
        return true;*/

        if (returnExchangeRequest.getAddress().getAddressType() == null) {
            if (returnExchangeRequest.getAddress().getNew_address())
                getNewAddress();
            else
                showToast(getString(R.string.select_address));
            return false;
        } else if (returnExchangeRequest.getAddress().getNew_address()) {
            getNewAddress();
        }
        return true;
    }

    public Address getSelectedAddress() {
        if (selectedAddress != null)
            return selectedAddress;
        else
            return getNewAddress();
    }


    @Override
    public void onError(int error_code, int tag, Object object) {
        switch (tag) {
            case Constants.TAG_ADDRESS_VIEW:
                addressItemsAdapter.notifyDataSetChanged();
                rootView.findViewById(R.id.scrollViewAddress).setVisibility(View.VISIBLE);
                rootView.findViewById(R.id.tvNoAddress).setVisibility(View.VISIBLE);
                toggleVisibility(R.id.rbNew);
                ((RadioButton) rootView.findViewById(R.id.rbNew)).setChecked(true);
                break;
        }

        if (error_code == Constants.UNAUTHORIZED_CODE) {
            ((BaseActivity) mActivity).showErrorDialog(Constants.SESSION_EXPIRED_MSG, new Intent(mActivity, LoginActivity.class));
        } else if (error_code == Constants.SERVER_DOWN_CODE || error_code == Constants.SERVER_DOWN_CODE1) {
            ((BaseActivity) mActivity).showErrorDialog(Constants.SERVER_DOWN_MSG, null);
        }

    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (resultCode == 1)
            getExistingAddress();
    }
}
