package outletwise.com.twentydresses.view.fragment;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import outletwise.com.twentydresses.R;
import outletwise.com.twentydresses.controller.AnalyticsTracker;
import outletwise.com.twentydresses.controller.adapter.BaseRecyclerAdapter;
import outletwise.com.twentydresses.controller.adapter.OrdersAdapter;
import outletwise.com.twentydresses.controller.interfaces.OrderItemClick;
import outletwise.com.twentydresses.model.Order;
import outletwise.com.twentydresses.model.Reason;
import outletwise.com.twentydresses.utilities.Constants;
import outletwise.com.twentydresses.utilities.Urls;
import outletwise.com.twentydresses.view.activity.BaseActivity;
import outletwise.com.twentydresses.view.activity.LoginActivity;

/**
 * Created by User-PC on 04-11-2015.
 */
public class OrderListFragment extends BaseFragment {

    private View rootView;
    private RecyclerView rvMyOrders;
    private List<Reason.ReasonsEntity> reasons = new ArrayList<>();
    private RecyclerScrollListener recyclerScrollListener;
    private boolean isLoading;
    private boolean isCalled;
    private int page;
    private OrdersAdapter adapter;
    private ArrayList<Order> orders = new ArrayList<>();
    private String user_id;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        rootView = inflater.inflate(R.layout.fragment_order_list, container, false);
        rvMyOrders = (RecyclerView) rootView.findViewById(R.id.rvOrdersList);
        init();

        return rootView;
    }


    void init() {
        page = 1;
        user_id = getArguments().getString("user_id");
        getOrders();
    }

    void getOrders() {


        JSONObject jsonObject = new JSONObject();
        try {
           /* jsonObject.put(Constants.USER_ID, ((BaseActivity) mActivity).user.getUser_id());
            jsonObject.put(Constants.AUTH_TOKEN, ((BaseActivity) mActivity).preferences.getString(Constants.KEY_TOKEN, ""));*/
            jsonObject.put(Constants.USER_ID, user_id);
            jsonObject.put(Constants.AUTH_TOKEN, ((BaseActivity) mActivity).preferences.getString(Constants.KEY_TOKEN, ""));


        } catch (JSONException e) {
            e.printStackTrace();
        } catch (NullPointerException ex) {
            ex.printStackTrace();
        }
        postJsonDataArray(Urls.ORDER_LIST, jsonObject, Constants.TAG_ORDER_LIST, true);
        isLoading = true;
    }


    @Override
    public <T> void onResponse(T obj, int tag) {
        super.onResponse(obj, tag);
        switch (tag) {
            case Constants.TAG_ORDER_CANCEL_REASONS:
                reasons.clear();
                reasons.addAll(Arrays.asList((Reason.ReasonsEntity[]) obj));
                getOrders();
                break;

            case Constants.TAG_ORDER_LIST:

                if (page == 1) {
                    orders.clear();
                    orders.addAll((Arrays.asList((Order[]) obj)));
                } else
                    orders.addAll((Arrays.asList((Order[]) obj)));

                if (orders.get(0).getOrder_count() != null)
                    ((TextView) rootView.findViewById(R.id.tvOrderCount)).setText("Total Orders: " + orders.get(0).getOrder_count());

                adapter = new OrdersAdapter(mActivity, orders);
                recyclerScrollListener = new RecyclerScrollListener();
                LinearLayoutManager layoutManager = new LinearLayoutManager(rvMyOrders.getContext());
                rvMyOrders.setLayoutManager(layoutManager);
                adapter.setReasons(reasons);
                rvMyOrders.setAdapter(adapter);
                recyclerScrollListener.setLayoutManager(layoutManager);
                rvMyOrders.scrollToPosition(recyclerScrollListener.getPastVisibleItem());
                rvMyOrders.addOnScrollListener(recyclerScrollListener);
                isLoading = false;

                adapter.setOnItemClickListener(new BaseRecyclerAdapter.OnItemClickListener() {
                    @Override
                    public void onItemClick(View view, int position) {
                        ((OrderItemClick) mActivity).onItemClicked((Order) view.getTag(), Constants.OrderAction.VIEW, null);
                    }
                });
                adapter.setReturnExchange(new OrdersAdapter.ReturnExchange() {
                    @Override
                    public void onReturnExchange(Order order) {
                        ((OrderItemClick) mActivity).onItemClicked(order, Constants.OrderAction.RETURN_EXCHANGE, null);
                    }

                    @Override
                    public void onCancel(Order order, Reason.ReasonsEntity reason) {
                        ((OrderItemClick) mActivity).onItemClicked(order, Constants.OrderAction.CANCEL, reason);
                    }

                    @Override
                    public void onItemClicked(Order order) {
                        ((OrderItemClick) mActivity).onItemClicked(order, Constants.OrderAction.VIEW, null);
                    }
                });
                break;
        }
    }

    @Override
    public void onError(int error_code, int tag, Object object) {
        super.onError(error_code, tag, object);
        switch (tag) {
            case Constants.TAG_ORDER_LIST:
                Activity activity = getActivity();
                if (activity != null && isAdded()) {
                    if (page != 2)
                        showToast(getString(R.string.no_orders));

                    if (page == 1) {
                        rootView.findViewById(R.id.rlNoOrderItems).setVisibility(View.VISIBLE);

                        rootView.findViewById(R.id.tvStartShopping).setOnClickListener(new View.OnClickListener() {
                            @Override
                            public void onClick(View v) {
                                mActivity.finish();
                            }
                        });
                    } else
                        rootView.findViewById(R.id.rlNoOrderItems).setVisibility(View.GONE);
                }

                break;
        }

        if (error_code == Constants.UNAUTHORIZED_CODE) {
            ((BaseActivity) mActivity).showErrorDialog(Constants.SESSION_EXPIRED_MSG, new Intent(mActivity, LoginActivity.class));
        } else if (error_code == Constants.SERVER_DOWN_CODE || error_code == Constants.SERVER_DOWN_CODE1) {
            ((BaseActivity) mActivity).showErrorDialog(Constants.SERVER_DOWN_MSG, null);
        }

    }


    class RecyclerScrollListener extends RecyclerView.OnScrollListener {
        private int pastVisibleItem, visibleItemCount, totalItemCount;
        private RecyclerView.LayoutManager layoutManager;

        public void setLayoutManager(RecyclerView.LayoutManager layoutManager) {
            this.layoutManager = layoutManager;
        }

        public int getPastVisibleItem() {
            return pastVisibleItem;
        }

        @Override
        public void onScrolled(RecyclerView recyclerView, int dx, int dy) {
            visibleItemCount = layoutManager.getChildCount();
            totalItemCount = layoutManager.getItemCount();
            pastVisibleItem = ((LinearLayoutManager) layoutManager).findFirstVisibleItemPosition();

            if ((visibleItemCount + pastVisibleItem) >= totalItemCount) {
                if (!isLoading) {
                    //reset isCalled here
                    isCalled = false;
                    getOrders(++page);
                }
            }
        }
    }

    void getOrders(int page) {
        JSONObject jsonObject = new JSONObject();
        try {
            if (((BaseActivity) mActivity).user != null &&  String.valueOf(((BaseActivity) mActivity).user.getUser_id()) != null) {
                jsonObject.put(Constants.USER_ID, ((BaseActivity) mActivity).user.getUser_id());
                jsonObject.put(Constants.AUTH_TOKEN, ((BaseActivity) mActivity).preferences.getString(Constants.KEY_TOKEN, ""));
                jsonObject.put("page", page);
                postJsonDataArray(Urls.ORDER_LIST, jsonObject, Constants.TAG_ORDER_LIST, true);
                isLoading = true;
            }else{
                jsonObject.put(Constants.USER_ID, user_id);
                jsonObject.put(Constants.AUTH_TOKEN, ((BaseActivity) mActivity).preferences.getString(Constants.KEY_TOKEN, ""));
                jsonObject.put("page", page);
                postJsonDataArray(Urls.ORDER_LIST, jsonObject, Constants.TAG_ORDER_LIST, true);
                isLoading = true;
            }
        } catch (JSONException e) {
            e.printStackTrace();
        }

    }

}
