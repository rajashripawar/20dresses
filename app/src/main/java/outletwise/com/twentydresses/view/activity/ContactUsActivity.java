package outletwise.com.twentydresses.view.activity;

import android.Manifest;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.support.v7.app.ActionBar;
import android.text.Html;
import android.view.View;
import android.widget.TextView;

import org.json.JSONObject;

import outletwise.com.twentydresses.R;
import outletwise.com.twentydresses.model.ContactUs;
import outletwise.com.twentydresses.utilities.Constants;
import outletwise.com.twentydresses.utilities.Urls;

/**
 * Class is use to display contact details
 */
public class ContactUsActivity extends BaseActivity {
    private Intent intent;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_contact_us);
        setUpToolBar(getString(R.string.toolbar_contactus));
        final ActionBar ab = getSupportActionBar();
        ab.setDisplayHomeAsUpEnabled(true);

        getData();

        findViewById(R.id.txtvw_email).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                Intent emailIntent = new Intent(Intent.ACTION_SEND);

                emailIntent.setData(Uri.parse("mailto:"));
                emailIntent.setType("text/plain");
                emailIntent.putExtra(Intent.EXTRA_EMAIL, new String[]{"hello@20Dresses.com"});
                startActivity(Intent.createChooser(emailIntent, "Send mail..."));
                //finish();
            }
        });

        findViewById(R.id.txtvw_number).setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View view) {
                intent = new Intent(Intent.ACTION_CALL, Uri.parse("tel:" + "+919699822211"));

                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                    int hasCallPermission = checkSelfPermission(Manifest.permission.CALL_PHONE);
                    if (hasCallPermission != PackageManager.PERMISSION_GRANTED) {
                        requestPermissions(new String[]{Manifest.permission.CALL_PHONE}, 1);
                        return;
                    } else
                        startActivity(intent);
                } else
                    startActivity(intent);
            }
        });
    }

    private void getData() {
        postJsonData(Urls.CONTACT_US, new JSONObject(), Constants.TAG_CONTACT_US, true);
    }


    @Override
    public void onRequestPermissionsResult(int requestCode, String[] permissions, int[] grantResults) {
        // super.onRequestPermissionsResult(requestCode, permissions, grantResults);

        if (grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
            // Permission Granted
            startActivity(intent);
        }
        else
            showToast(getString(R.string.text_permission_denied));
    }

    @Override
    public <T> void onResponse(T obj, int tag) {
        super.onResponse(obj, tag);
        switch (tag) {
            case Constants.TAG_CONTACT_US:
                ContactUs response = (ContactUs) obj;
                ((TextView) findViewById(R.id.tvDetails)).setText(Html.fromHtml(response.getDetails()));
                ((TextView) findViewById(R.id.txtvw_email)).setText(response.getEmail());
                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
                    ((TextView) findViewById(R.id.txtvw_number)).setCompoundDrawablesWithIntrinsicBounds(getDrawable(R.drawable.phone), null, null, null);
                    ((TextView) findViewById(R.id.txtvw_email)).setCompoundDrawablesWithIntrinsicBounds(getDrawable(R.drawable.email), null, null, null);
                } else {
                    ((TextView) findViewById(R.id.txtvw_number)).setCompoundDrawablesWithIntrinsicBounds(getResources().getDrawable(R.drawable.phone), null, null, null);
                    ((TextView) findViewById(R.id.txtvw_email)).setCompoundDrawablesWithIntrinsicBounds(getResources().getDrawable(R.drawable.email), null, null, null);
                }


                ((TextView) findViewById(R.id.txtvw_number)).setText(response.getPhone());
                ((TextView) findViewById(R.id.tvAddress)).setText(Html.fromHtml(response.getAddress()));
                break;

        }
    }

    @Override
    public void onError(int error_code, int tag, Object object) {
        checkErrorCode(error_code, object);
    }
}
