package outletwise.com.twentydresses.view.activity;

import android.os.Bundle;
import android.support.v7.app.ActionBar;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.View;
import android.widget.EditText;
import android.widget.TextView;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;

import outletwise.com.twentydresses.R;
import outletwise.com.twentydresses.controller.adapter.TestimonialsAdapter;
import outletwise.com.twentydresses.model.Testimonial;
import outletwise.com.twentydresses.model.TestimonialSocial;
import outletwise.com.twentydresses.model.database.greenbot.User;
import outletwise.com.twentydresses.utilities.Constants;
import outletwise.com.twentydresses.utilities.Urls;
import outletwise.com.twentydresses.view.decoration.DividerItemDecoration;

/**
 * Created by User-PC on 17-11-2015.
 * This class is use to show other users testimonials.
 * Also user can add his testimonial.
 */
public class TestimonialsActivity extends BaseActivity {
    private RecyclerView rvTestimonial;
    private RecyclerScrollListener recyclerScrollListener;
    private boolean isLoading;
    private int page = 1;
    private boolean isCalled;
//    private ArrayList<Testimonial> testimonials = new ArrayList<>();
    private ArrayList<TestimonialSocial.Testimonials> data = new ArrayList<>();

    private TestimonialsAdapter testimonialsAdapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_testimonials);
        trackPage(getString(R.string.pgTestimonial));
        setUpToolBar(getString(R.string.toolbar_testimonial));
        final ActionBar ab = getSupportActionBar();
        ab.setDisplayHomeAsUpEnabled(true);
        findViewById(R.id.bAdd).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                findViewById(R.id.bAdd).setVisibility(View.GONE);
                findViewById(R.id.llContent).setVisibility(View.VISIBLE);
            }
        });

        findViewById(R.id.bSubmit).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String strTestimonial = ((EditText) findViewById(R.id.edtAddTestimonial)).getText().toString();

                if (!strTestimonial.isEmpty() && !strTestimonial.equalsIgnoreCase("")) {
                    JSONObject jsonObject = new JSONObject();
                    try {
                        if (user != null && String.valueOf(user.getUser_id()) != null)
                            jsonObject.put(Constants.USER_ID, user.getUser_id());
                        else if (preferences.getString(Constants.USER_ID, "") != null){
                            jsonObject.put(Constants.USER_ID, preferences.getString(Constants.USER_ID, ""));
                        }
//                        jsonObject.put(Constants.USER_ID, user.getUser_id() + "");
                        jsonObject.put(Constants.AUTH_TOKEN, preferences.getString(Constants.KEY_TOKEN, ""));
                        jsonObject.put(Constants.TESTIMONIAL, strTestimonial);
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                    postJsonData(Urls.USER_TESTIMONIALS, jsonObject, Constants.TAG_USER_TESTIMONIALS, true);
                } else
                    showToast(getString(R.string.add_testimonial));
            }
        });
        init();
    }

    @Override
    public void onCompleteRetrieval(int tag, ArrayList obj, String msg) {
        switch (tag) {
            case Constants.TAG_USER:
                user = (User) obj.get(0);
                checkForAddTestimonial();
                break;
        }
    }

    void init() {
        rvTestimonial = (RecyclerView) findViewById(R.id.rvTestimonials);
        rvTestimonial.addItemDecoration(new DividerItemDecoration(this, DividerItemDecoration.VERTICAL_LIST));
        rvTestimonial.setLayoutManager(new LinearLayoutManager(rvTestimonial.getContext()));
//        postJsonDataArray(Urls.TESTIMONIALS, new JSONObject(), Constants.TAG_TESTIMONIALS, true);

        HashMap<String, String> params = new HashMap<>();
        params.put("app_key", Constants.APP_KEY);
        postJsonData(Urls.USER_TESTIMONIALS_SOCIAL, params, Constants.TAG_TESTIMONIALS_SOCIAL, true);
        isLoading = true;

//        testimonialsAdapter = new TestimonialsAdapter(this, testimonials);

        recyclerScrollListener = new RecyclerScrollListener();
        LinearLayoutManager layoutManager = new LinearLayoutManager(rvTestimonial.getContext());
        rvTestimonial.setLayoutManager(layoutManager);
        recyclerScrollListener.setLayoutManager(layoutManager);

        testimonialsAdapter = new TestimonialsAdapter(this, data);
        rvTestimonial.setAdapter(testimonialsAdapter);

        rvTestimonial.scrollToPosition(recyclerScrollListener.getPastVisibleItem());
        rvTestimonial.addOnScrollListener(recyclerScrollListener);
    }

    private void checkForAddTestimonial() {

        JSONObject jsonObject = new JSONObject();
        try {
            jsonObject.put(Constants.USER_ID, user.getUser_id() + "");
            jsonObject.put(Constants.AUTH_TOKEN, preferences.getString(Constants.KEY_TOKEN, ""));
        } catch (JSONException e) {
            e.printStackTrace();
        }
        postJsonData(Urls.USER_TESTIMONIALS, jsonObject, Constants.TAG_USER_TESTIMONIALS, true);
    }

    @Override
    public <T> void onResponse(T obj, int tag) {
        super.onResponse(obj, tag);
        switch (tag) {
            case Constants.TAG_TESTIMONIALS_SOCIAL:
                TestimonialSocial social = ((TestimonialSocial)obj);
                Log.e(""," social size:::::"+social.getData().size());
                if (page == 1) {
                    data.clear();
                    data.addAll(social.getData());

                }
                else
                data.addAll(social.getData());
                testimonialsAdapter.notifyDataSetChanged();
                Log.e("","data size:::::"+data.size());
                isLoading = false;
                break;

          /*  case Constants.TAG_TESTIMONIALS:

          *//*      if (page == 1) {
                    testimonials.clear();
                    testimonials.addAll((Arrays.asList((Testimonial[]) obj)));
                } else
                    testimonials.addAll((Arrays.asList((Testimonial[]) obj)));

                testimonialsAdapter.notifyDataSetChanged();

                isLoading = false;*//*

                break;*/

            case Constants.TAG_USER_TESTIMONIALS:
                Testimonial response = (Testimonial) obj;
                if (response.getStatus().equalsIgnoreCase(Constants.RESPONSE_SUCCESS)) {
                    if (response.getAdd_testimonial())
                        findViewById(R.id.llAddTestimonial).setVisibility(View.VISIBLE);
                    else
                        findViewById(R.id.llAddTestimonial).setVisibility(View.GONE);

                    if (response.getTestimonial_added()) {
                        findViewById(R.id.llAddTestimonial).setVisibility(View.GONE);
                        findViewById(R.id.llContent).setVisibility(View.GONE);
                    }

                    ((TextView) findViewById(R.id.tvCount)).setText("(" + response.getTotal_testimonials() + ")");
                    ((TextView) findViewById(R.id.tvCount)).setVisibility(View.VISIBLE);

                   // ((TextView) findViewById(R.id.tvCust)).setText(getString(R.string.text_cust_testimonial) + " (" + response.getTotal_testimonials() + ")");

                    showToast(response.getMessage());
                }

                break;

        }
    }


    class RecyclerScrollListener extends RecyclerView.OnScrollListener {
        private int pastVisibleItem, visibleItemCount, totalItemCount;
        private RecyclerView.LayoutManager layoutManager;

        public void setLayoutManager(RecyclerView.LayoutManager layoutManager) {
            this.layoutManager = layoutManager;
        }

        public int getPastVisibleItem() {
            return pastVisibleItem;
        }

        @Override
        public void onScrolled(RecyclerView recyclerView, int dx, int dy) {
            visibleItemCount = layoutManager.getChildCount();
            totalItemCount = layoutManager.getItemCount();
            pastVisibleItem = ((LinearLayoutManager) layoutManager).findFirstVisibleItemPosition();

            if ((visibleItemCount + pastVisibleItem) >= totalItemCount) {
                if (!isLoading) {
                    //reset isCalled here
                    isCalled = false;
                    // productAdapter.showProgressBar();
                    getTestimonials(++page);
                }
            }
        }
    }

    private void getTestimonials(int page) {

        HashMap<String, String> params = new HashMap<>();
        params.put("app_key", Constants.APP_KEY);
        params.put("page",String.valueOf(page));
        postJsonData(Urls.USER_TESTIMONIALS_SOCIAL, params, Constants.TAG_TESTIMONIALS_SOCIAL, true);
        isLoading = true;

     /*   JSONObject jsonObject = new JSONObject();
        try {
            jsonObject.put("page", page);
        } catch (JSONException e) {
            e.printStackTrace();
        }
        postJsonDataArray(Urls.TESTIMONIALS, jsonObject, Constants.TAG_TESTIMONIALS, true);
        isLoading = true;*/
    }

    @Override
    public void onError(int error_code, int tag, Object object) {
        checkErrorCode(error_code, object);
    }
}
