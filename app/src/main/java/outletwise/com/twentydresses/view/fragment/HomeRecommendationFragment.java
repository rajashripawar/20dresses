package outletwise.com.twentydresses.view.fragment;

import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.support.annotation.Nullable;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.google.gson.Gson;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

import outletwise.com.twentydresses.R;
import outletwise.com.twentydresses.controller.AnalyticsTracker;
import outletwise.com.twentydresses.controller.adapter.BaseRecyclerAdapter;
import outletwise.com.twentydresses.controller.adapter.TipsAdapter;
import outletwise.com.twentydresses.model.Category;
import outletwise.com.twentydresses.model.Quiz;
import outletwise.com.twentydresses.model.Recommended;
import outletwise.com.twentydresses.model.StyleAttrEntity;
import outletwise.com.twentydresses.model.UpdateStyleProfile;
import outletwise.com.twentydresses.model.UserAttrEntity;
import outletwise.com.twentydresses.model.VisitorResponse;
import outletwise.com.twentydresses.utilities.Constants;
import outletwise.com.twentydresses.utilities.Urls;
import outletwise.com.twentydresses.view.activity.BaseActivity;
import outletwise.com.twentydresses.view.activity.ForYouQuizActivity;
import outletwise.com.twentydresses.view.activity.LoginActivity;
import outletwise.com.twentydresses.view.activity.ProductsActivity;
import outletwise.com.twentydresses.view.activity.QuizActivity;

/**
 * Created by User-PC on 06-08-2015.
 */
public class HomeRecommendationFragment extends BaseFragment implements SwipeRefreshLayout.OnRefreshListener {

    private View rootView;
    private Category.SubCat subCat;
    private ArrayList<Recommended.Tip> tips = new ArrayList<>();
    private ArrayList<Recommended.Tip> lock_data = new ArrayList<>();
    private TipsAdapter adapter;
    private String gender = "F";
    private String uid = "";
    private UserAttrEntity userAttr;
    private StyleAttrEntity styleArr;
    private SwipeRefreshLayout mSwipeRefreshLayout;
    public static SharedPreferences preferences;


    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        rootView = inflater.inflate(R.layout.fragment_recommendations, container, false);
        preferences = PreferenceManager.getDefaultSharedPreferences(getActivity());
        init();
        return rootView;
    }

    private void init() {
        mSwipeRefreshLayout = (SwipeRefreshLayout) rootView.findViewById(R.id.swipeRefreshLayout);
        mSwipeRefreshLayout.setOnRefreshListener(this);
        try {
            gender = ((BaseActivity) mActivity).user.getGender();
            subCat = getArguments().getParcelable(Constants.CATEGORY);
            uid = getArguments().getString(Constants.USER_ID);
        } catch (NullPointerException ex) {
            ex.printStackTrace();
        }

    }

    @Override
    public void onResume() {
        super.onResume();
        init();
        setUpRecyclerView();
    }

    void setUpRecyclerView() {
        RecyclerView rv = (RecyclerView) rootView.findViewById(R.id.rvRecommendations);
        rv.setLayoutManager(new GridLayoutManager(rv.getContext(), 2));
        adapter = new TipsAdapter(mActivity, tips, lock_data);
        adapter.setOnItemClickListener(new BaseRecyclerAdapter.OnItemClickListener() {
            @Override
            public void onItemClick(View view, int position) {


                if (view.getId() == R.id.ivCluster) {
                    Constants.debug("Cluster lock clicked!!");
                    AnalyticsTracker.getInstance().trackForYouClicked(lock_data.get(position).getTip_id(), lock_data.get(position).getTip_text());
                    Intent intent = new Intent(mActivity, ForYouQuizActivity.class);
                    intent.putExtra("que_id", lock_data.get(position).getQue_id());
                    intent.putExtra("tip_id", lock_data.get(position).getTip_id());
                    intent.putExtra("tip_text", lock_data.get(position).getTip_text());
                    intent.putExtra("tip_total", lock_data.get(position).getTotal());
                    startActivity(intent);
                } else {
                    Constants.debug("Normal cluster clicked!");
                    Recommended.Tip tip = (Recommended.Tip) view.getTag();

                    AnalyticsTracker.getInstance().trackForYouClicked(tip.getTip_id(), tip.getTip_text());

                    if (subCat != null)
                        subCat.setId(tip.getTip_id());
                    else
                        subCat = getArguments().getParcelable(Constants.CATEGORY);


                    Intent intent = new Intent(mActivity, ProductsActivity.class);
                    intent.putExtra(Constants.CATEGORY, subCat);
                    Constants.TIP_NAME = tip.getTip_text();
                    Constants.TIP_TOTAL = tip.getTotal();
                    startActivity(intent);
                }

            }
        });
        rv.setAdapter(adapter);

        JSONObject jsonObject = new JSONObject();
        try {
            jsonObject.put(Constants.AUTH_TOKEN, ((BaseActivity) mActivity).preferences.getString(Constants.KEY_TOKEN, ""));
            if (((BaseActivity) mActivity).user  != null && String.valueOf(((BaseActivity) mActivity).user .getUser_id()) != null){
                if (!String.valueOf(((BaseActivity) mActivity).user.getUser_id()).trim().isEmpty()){
                    jsonObject.put(Constants.USER_ID, ((BaseActivity) mActivity).user.getUser_id());
                }
            }
            else
                jsonObject.put(Constants.USER_ID, preferences.getString(Constants.USER_ID,""));

        } catch (JSONException e) {
            e.printStackTrace();
        }
        postJsonData(Urls.FOR_YOU, jsonObject, Constants.TAG_FOR_YOU, false);
    }

    @Override
    public <T> void onResponse(T obj, int tag) {
        super.onResponse(obj, tag);
        rootView.findViewById(R.id.pbWheelFrag).setVisibility(View.GONE);
        switch (tag) {
            case Constants.TAG_FOR_YOU:
                Recommended recommended = (Recommended) obj;
                Log.e("RECOMMENDATION SCREEN ",""+new Gson().toJson(recommended));

                if (recommended.getStatus().equalsIgnoreCase(Constants.RESPONSE_SUCCESS)) {
                    tips.clear();
                    tips.addAll(recommended.getTips());
                    lock_data.clear();

                    if (recommended.getLock_data() != null)
                        lock_data.addAll(recommended.getLock_data());

                    adapter.notifyDataSetChanged();
                }
                else if (recommended.getStatus().equalsIgnoreCase(Constants.RESPONSE_FAILED))
                    takeQuiz();
                mSwipeRefreshLayout.setRefreshing(false);
                break;

            case Constants.TAG_UPDATE_STYLE_PROFILE:
                VisitorResponse response = (VisitorResponse) obj;
                if (response.getStatus().equalsIgnoreCase(Constants.RESPONSE_SUCCESS)) {
                    rootView.findViewById(R.id.pbWheelFrag).setVisibility(View.GONE);
                    rootView.findViewById(R.id.txtView).setVisibility(View.GONE);
                    rootView.findViewById(R.id.tvTakeQuiz).setVisibility(View.GONE);
                    Constants.TAKE_QUIZ = false;
                    setUpRecyclerView();
                }
                showToast(response.getMessage());
                break;
        }
    }

    @Override
    public void onError(int error_code, int tag, Object object) {
        switch (tag) {
            case Constants.TAG_FOR_YOU:
                mSwipeRefreshLayout.setRefreshing(false);
                /*sarika*/
//                takeQuiz();
                break;
        }
        if (error_code == Constants.UNAUTHORIZED_CODE) {
            //startActivity(new Intent(mActivity, LoginActivity.class));
            ((BaseActivity) mActivity).showErrorDialog(Constants.SESSION_EXPIRED_MSG, new Intent(mActivity, LoginActivity.class));
        } else if (error_code == Constants.SERVER_DOWN_CODE || error_code == Constants.SERVER_DOWN_CODE1) {
            ((BaseActivity) mActivity).showErrorDialog(Constants.SERVER_DOWN_MSG, null);
        }
    }

    private void takeQuiz() {
        rootView.findViewById(R.id.pbWheelFrag).setVisibility(View.GONE);
        rootView.findViewById(R.id.txtView).setVisibility(View.VISIBLE);
        rootView.findViewById(R.id.tvTakeQuiz).setVisibility(View.VISIBLE);

        rootView.findViewById(R.id.tvTakeQuiz).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Constants.TAKE_QUIZ = true;
                startActivityForResult(new Intent(getActivity(), QuizActivity.class), 1);
            }
        });
    }


    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (resultCode == mActivity.RESULT_OK) {
            userAttr = Quiz.getInstance().getUser_attr();
            styleArr = Quiz.getInstance().getStyle_attr();
            UpdateStyleProfile styleProfile = new UpdateStyleProfile();
            styleProfile.setUser_attr(userAttr);
            styleProfile.setStyle_attr(styleArr);
            if (String.valueOf(((BaseActivity) mActivity).user.getUser_id()) != null)
                styleProfile.setUserId(String.valueOf(((BaseActivity) mActivity).user.getUser_id()));
            else if (preferences.getString(Constants.USER_ID, "") != null){
                styleProfile.setUserId(preferences.getString(Constants.USER_ID, ""));
            }
            styleProfile.setAuth_token(((BaseActivity) mActivity).preferences.getString(Constants.KEY_TOKEN, ""));

            postJsonData(Urls.UPDATE_STYLE_PROFILE, styleProfile, Constants.TAG_UPDATE_STYLE_PROFILE, true);

        }
    }

    @Override
    public void onRefresh() {
        setUpRecyclerView();
    }
}
