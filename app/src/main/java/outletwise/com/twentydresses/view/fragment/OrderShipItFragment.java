package outletwise.com.twentydresses.view.fragment;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import outletwise.com.twentydresses.R;
import outletwise.com.twentydresses.model.OrderReturnResponse;
import outletwise.com.twentydresses.model.ReturnExchangeRequest;

public class OrderShipItFragment extends BaseFragment {

    private View rootView;
    private OrderReturnResponse orderReturnResponse;
    private ReturnExchangeRequest returnExchangeRequest;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        rootView = inflater.inflate(R.layout.fragment_myorders_ship_it, container, false);
        getToolBar().setTitle(getString(R.string.toolbar_ship_it));
        orderReturnResponse = getArguments().getParcelable(OrderReturnResponse.class.getSimpleName());
        returnExchangeRequest = getArguments().getParcelable(ReturnExchangeRequest.class.getSimpleName());

        return rootView;
    }

    public OrderReturnResponse getOrderReturnResponse() {
        return orderReturnResponse;
    }

    public ReturnExchangeRequest getReturnExchangeRequest() {
        return returnExchangeRequest;
    }
}
