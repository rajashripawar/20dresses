package outletwise.com.twentydresses.view.activity;

import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Paint;
import android.net.Uri;
import android.os.Bundle;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AlertDialog;
import android.support.v7.widget.LinearLayoutManager;
import android.text.Html;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewTreeObserver;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.ScrollView;
import android.widget.TextView;

import com.dd.CircularProgressButton;
import com.mikepenz.actionitembadge.library.ActionItemBadge;
import com.sothree.slidinguppanel.SlidingUpPanelLayout;
import com.viewpagerindicator.CirclePageIndicator;

import org.json.JSONException;
import org.json.JSONObject;

import java.lang.ref.WeakReference;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;

import outletwise.com.twentydresses.R;
import outletwise.com.twentydresses.controller.AnalyticsTracker;
import outletwise.com.twentydresses.controller.CartOperations;
import outletwise.com.twentydresses.controller.MyTagHandler;
import outletwise.com.twentydresses.controller.adapter.BaseRecyclerAdapter;
import outletwise.com.twentydresses.controller.adapter.ProductsAdapter;
import outletwise.com.twentydresses.controller.adapter.SingleImageAdapter;
import outletwise.com.twentydresses.controller.adapter.SizesAdapter;
import outletwise.com.twentydresses.model.AddCartResponse;
import outletwise.com.twentydresses.model.Category;
import outletwise.com.twentydresses.model.PinCodeResponse;
import outletwise.com.twentydresses.model.Product;
import outletwise.com.twentydresses.model.SingleProductModel;
import outletwise.com.twentydresses.model.VisitorResponse;
import outletwise.com.twentydresses.model.database.greenbot.Cart;
import outletwise.com.twentydresses.model.database.greenbot.Shortlist;
import outletwise.com.twentydresses.model.database.greenbot.User;
import outletwise.com.twentydresses.utilities.Constants;
import outletwise.com.twentydresses.utilities.Urls;
import outletwise.com.twentydresses.view.custom.CustomRecyclerView;
import outletwise.com.twentydresses.view.custom.CustomViewPager;
import outletwise.com.twentydresses.view.custom.FavoritesButton;
import outletwise.com.twentydresses.view.decoration.FlipAnimation;
import outletwise.com.twentydresses.view.fragment.ImageFragment;
import outletwise.com.twentydresses.view.fragment.ProductViewImageFragment;

/**
 * Created by User-PC on 14-09-2015.
 * This class is use to view detail information about single product
 */
public class ProductViewActivity extends BaseActivity {

    private int positionMain[] = {0, 0};
    private Product product;
    private SingleImageAdapter adapter;
    private List<String> imageUrls;

    private ScrollView scrollView;
    private View viewMain, viewScroll;
    private CustomViewPager imageViewPager;
    private TextView tvProductName, tvFinalPriceMain, tvPriceMain, tvDiscountMain,
            tvFinalPriceScroll, tvPriceScroll, tvDiscountScroll, etPinCode, tvCODOptions,tvProductNOneLiner;
    private Button bScrollGotoBag, bScrollBuyNow, bBuyNow, bGotoBag;
    private CircularProgressButton bChkDelOpts;
    private LinearLayout llSizes;
    private Product.ProductSizesEntity selectedSize, tempSize, selectedOutOfStockSize;
    private View fromView, toView;
    private CustomRecyclerView rvYouMayLike;
    private MenuItem menuCart;
    private boolean isKeyboardVisible;
    private ArrayList<String> currentProductSize = new ArrayList<>();
    public FavoritesButton bFavorites;
    int favorite_state = FavoritesButton.PROGRESS_IDLE;
    private Category.SubCat subCat;
    private User user;
    private boolean isCalled;
    String id = "";

   /* bChkDelOpts.setProgress(0);   // Apply
    bChkDelOpts.setProgress(100);   //Change
    bChkDelOpts.setProgress(-1);  //Retry   */

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_product_view);
        //  init();
        findViewById(R.id.ivLeft).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                previousPage();
            }
        });
        findViewById(R.id.ivRight).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                nextPage();
            }
        });
        /* Share product to users*/
        findViewById(R.id.bShare).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                String msg = product.getProduct_name() + "\n" + "Rs." + product.getProduct_price();
                Constants.debug("Product Share :" + msg);

                try {
                     if(product.getProduct_discount() != null && !product.getProduct_discount().equalsIgnoreCase("0")){
                         String priceWithDiscount=Constants.RUPEE + Constants.getPriceWithDiscount(product.getProduct_price(), product.getProduct_discount());
                         shareMsg(priceWithDiscount);
                    }else{
                        shareMsg(msg);
                    }
                    AnalyticsTracker.getInstance().trackItemShared(product, subCat.getName());
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        });


      /*  SoftKeyboardStateWatcher softKeyboardStateWatcher = new SoftKeyboardStateWatcher(etPinCode);
        softKeyboardStateWatcher.addSoftKeyboardStateListener(new SoftKeyboardStateWatcher.SoftKeyboardStateListener() {
            @Override
            public void onSoftKeyboardOpened(int keyboardHeightInPx) {
                findViewById(R.id.rlSlidingLayout).setVisibility(View.GONE);
                // setSlidingPanelHeight(0);
                isKeyboardVisible = true;
            }

            @Override
            public void onSoftKeyboardClosed() {
                findViewById(R.id.rlSlidingLayout).setVisibility(View.VISIBLE);
                //  setSlidingPanelHeight(getResources().getDimensionPixelSize(R.dimen.umano_collapsed_height));
                isKeyboardVisible = false;
            }
        });*/

        findViewById(R.id.bSizeChart).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (product != null && product.getSizeChart_img() != null) {
                    Bundle bundle = new Bundle();
                    bundle.putString(Constants.SIZE_CHART_IMAGE, product.getSizeChart_img());
                    ImageFragment imageFragment = new ImageFragment();
                    imageFragment.setArguments(bundle);
                    imageFragment.show(getmFragmentManager(), ImageFragment.class.getSimpleName());
                }
            }
        });



    }


    public void shareMsg(String msg) {


        Uri uri = Uri
                .parse(product.getProduct_img());

        msg = msg +"\n" + product.getProduct_url();

        Constants.debug("Product share :" + msg);

        Intent shareIntent = new Intent();

        shareIntent.setAction(Intent.ACTION_SEND);

        shareIntent.putExtra(Intent.EXTRA_TEXT, msg);

        shareIntent.setType("text/plain");

        shareIntent.addFlags(Intent.FLAG_GRANT_READ_URI_PERMISSION);

        startActivity(Intent.createChooser(shareIntent, "Share Product..."));

    }

    private void previousPage() {
        if (imageViewPager != null) {
            int currentPage = imageViewPager.getCurrentItem();
            int totalPages = imageViewPager.getAdapter().getCount();

            int previousPage = currentPage - 1;
            if (previousPage < 0) {
                // We can't go back anymore.
                // Loop to the last page. If you don't want looping just
                // return here.
                previousPage = totalPages - 1;
            }

            imageViewPager.setCurrentItem(previousPage, true);
        }
    }

    private void nextPage() {
        if (imageViewPager != null) {
            int currentPage = imageViewPager.getCurrentItem();
            int totalPages = imageViewPager.getAdapter().getCount();

            int nextPage = currentPage + 1;
            if (nextPage >= totalPages) {
                // We can't go forward anymore.
                // Loop to the first page. If you don't want looping just
                // return here.
                nextPage = 0;
            }

            imageViewPager.setCurrentItem(nextPage, true);
        }
    }


    void init() {
        scrollView = (ScrollView) findViewById(R.id.scrollView);
        viewMain = findViewById(R.id.viewMain);
        viewScroll = findViewById(R.id.viewScroll);
        imageViewPager = (CustomViewPager) findViewById(R.id.vpProductImage);
        tvProductName = (TextView) findViewById(R.id.tvProductName);

        tvProductNOneLiner=(TextView) findViewById(R.id.tvProductNOneLiner);

        tvFinalPriceMain = (TextView) findViewById(R.id.tvFinalProductPriceMain);
        tvPriceMain = (TextView) findViewById(R.id.tvProductPriceMain);
        tvDiscountMain = (TextView) findViewById(R.id.tvProductDiscountMain);
        tvFinalPriceScroll = (TextView) findViewById(R.id.tvFinalProductPriceScroll);
        tvPriceScroll = (TextView) findViewById(R.id.tvProductPriceScroll);
        tvDiscountScroll = (TextView) findViewById(R.id.tvProductDiscountScroll);
        tvCODOptions = (TextView) findViewById(R.id.tvCODOptions);
        etPinCode = (EditText) findViewById(R.id.etPinCode);
        bChkDelOpts = (CircularProgressButton) findViewById(R.id.bCheckDeliveryOpts);
        llSizes = (LinearLayout) findViewById(R.id.llSizes);

        bScrollBuyNow = (Button) findViewById(R.id.bBuyNowScroll);
        bBuyNow = (Button) findViewById(R.id.bBuyNow);
        bScrollGotoBag = (Button) findViewById(R.id.bGotoBagScroll);
        bGotoBag = (Button) findViewById(R.id.bGotoBag);

        setUpShortList();
        setUpToolBar(getString(R.string.toolbar_product_detail));
        final ActionBar ab = getSupportActionBar();
        ab.setDisplayHomeAsUpEnabled(true);
        setUpSlidingPanel();
        setSlidingPanelHeight(0);//Initially hide the panel


        id = getIntent().getStringExtra(Constants.SINGLE_ITEM);

        if (id != null && !id.isEmpty()) {
            getProductSingleDetails(id);
        }
        else {
            product = getIntent().getParcelableExtra(Constants.PRODUCT_NAME);
            subCat = getIntent().getParcelableExtra(Constants.CATEGORY);
            if (CartOperations.getInstance().getCartList().size() > 0) {
                for (Cart cart : CartOperations.getInstance().getCartList()) {
                    if (cart.getProduct_id().equalsIgnoreCase(product.getProduct_id()))
                        product.setProduct_size(cart.getProduct_size());
                }
            }
            tvProductName.setText(product.getProduct_name());
            tvProductNOneLiner.setText("Additional tax may apply, charged at checkout");

            currentProductSize = product.getProduct_size();
            calculateAndSetPrice();
            setUpViewPager();
            setUpVisibilityBtn();
            //Get Product Details From Server
            getProductDetails();
        }
        setUpButtonListeners();

        /*Button to check for delivery availability*/
        bChkDelOpts.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (etPinCode.getVisibility() == View.VISIBLE) {
                    if (!TextUtils.isEmpty(etPinCode.getText().toString())) {
                        bChkDelOpts.setIndeterminateProgressMode(true);
                        bChkDelOpts.setProgress(1);
                        pincodeCheck(etPinCode.getText().toString().trim());
                    } else {
                        showToast(getString(R.string.enter_pincode));
                        etPinCode.requestFocus();
                    }
                } else if (tvCODOptions.getVisibility() == View.VISIBLE) {
                    bChkDelOpts.setProgress(0);
                    tvCODOptions.setVisibility(View.GONE);
                    etPinCode.setVisibility(View.VISIBLE);
                    etPinCode.requestFocus();
                    if (product.getPincode() != null && !product.getPincode().equalsIgnoreCase(""))
                        etPinCode.setText(product.getPincode());

                }
            }
        });

        /*Scrolling behaviour change from here only*/
        final int positionScroll[] = {0, 0};
        scrollView.getViewTreeObserver().addOnScrollChangedListener(new ViewTreeObserver.OnScrollChangedListener() {
            @Override
            public void onScrollChanged() {
                viewScroll.getLocationOnScreen(positionScroll);
                if (positionScroll[1] <= positionMain[1]) {
                    if (!isKeyboardVisible)
                        setSlidingPanelHeight(getResources().getDimensionPixelSize(R.dimen.umano_collapsed_height));
                    viewMain.setVisibility(View.GONE);
                    viewScroll.setVisibility(View.VISIBLE);

                } else {
                    viewMain.setVisibility(View.VISIBLE);
                  //viewScroll.setVisibility(View.GONE);
                    setSlidingPanelHeight(0);
                }
            }
        });

    }

    private void getProductDetails() {

        HashMap<String, String> params = new HashMap<>();
        params.put("product_id", product.getProduct_id());
        if (user != null)
            params.put(Constants.USER_ID, user.getUser_id() + "");
        postStringData(Urls.SINGLE_PRODUCT, params, Constants.TAG_SINGLE_PRODUCT, true);
    }

    private void getProductSingleDetails(String id) {

        HashMap<String, String> params = new HashMap<>();
        params.put("product_id",id);
        if (user != null)
            params.put(Constants.USER_ID, user.getUser_id() + "");
        postStringData(Urls.SINGLE_PRODUCT, params, Constants.TAG_SINGLE_PRODUCT, true);
    }

    private void pincodeCheck(String pincode) {

        JSONObject jsonObject = new JSONObject();
        try {
            jsonObject.put(Constants.PINCODE, pincode);
        } catch (JSONException e) {
            e.printStackTrace();
        }
        postJsonData(Urls.CHK_DEL_OPTS, jsonObject, Constants.TAG_CHK_DEL_OPTS, false);
    }

    /*
    * Set up shortlist products
    * */
    private void setUpShortList() {

        bFavorites = (FavoritesButton) findViewById(R.id.bFavorites);
        if (favorite_state == FavoritesButton.PROGRESS_COMPLETE)
            bFavorites.stopProgress(true);
        else if (favorite_state == FavoritesButton.PROGRESS_IDLE)
            bFavorites.stopProgress(false);
        else if (favorite_state == FavoritesButton.IN_PROGRESS)
            bFavorites.startProgress();

        bFavorites.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                if (favorite_state == FavoritesButton.PROGRESS_COMPLETE) {
                    bFavorites.startProgress();
                    removeProductFromShortList(product);

                } else {
                    bFavorites.startProgress();
                    addProductToShortList(product);
                }
            }
        });
    }


    private void setUpVisibilityBtn() {
        selectedSize = null;
        toView = null;

   /*     product = getIntent().getParcelableExtra(Constants.PRODUCT_NAME);
        ArrayList<Product.ProductSizesEntity> productSizes = product.getProduct_sizes();
        if (productSizes != null && productSizes.size() == 1)
            selectedSize = productSizes.get(0);*/

        bScrollBuyNow.setVisibility(View.VISIBLE);
        bBuyNow.setVisibility(View.VISIBLE);
        bScrollGotoBag.setVisibility(View.GONE);
        bGotoBag.setVisibility(View.GONE);

        if (CartOperations.getInstance().getCartList().size() > 0) {
            for (Cart cart : CartOperations.getInstance().getCartList()) {
                if (cart.getProduct_id().equalsIgnoreCase(product.getProduct_id())) {
                    currentProductSize = product.getProduct_size();
                    bScrollBuyNow.setVisibility(View.GONE);
                    bBuyNow.setVisibility(View.GONE);
                    bScrollGotoBag.setVisibility(View.VISIBLE);
                    bGotoBag.setVisibility(View.VISIBLE);
                }
            }
        } else {
            bScrollBuyNow.setVisibility(View.VISIBLE);
            bBuyNow.setVisibility(View.VISIBLE);
            bScrollGotoBag.setVisibility(View.GONE);
            bGotoBag.setVisibility(View.GONE);
        }
    }

    void setUpButtonListeners() {
        findViewById(R.id.bBuyNowScroll).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (selectedSize == null) {
                    showToast(getString(R.string.toast_select_size));
                    return;
                }

                if (toView == null) {
                    fromView = v;
                    toView = findViewById(R.id.bGotoBagScroll);
                    addToCart();
                    if (product.getProduct_alert_message() != null && !product.getProduct_alert_message().equalsIgnoreCase(""))
                        show_nonReturnablePopup();
                }
            }
        });

        findViewById(R.id.bBuyNow).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (selectedSize == null) {
                    showToast(getString(R.string.toast_select_size));
                    return;
                }

                if (toView == null) {
                    fromView = v;
                    toView = findViewById(R.id.bGotoBag);
                    addToCart();
                    if (product.getProduct_alert_message() != null && !product.getProduct_alert_message().equalsIgnoreCase(""))
                        show_nonReturnablePopup();
                }
            }
        });

        findViewById(R.id.bGotoBag).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(getApplicationContext(), CartActivity.class));
            }
        });

        findViewById(R.id.bGotoBagScroll).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(getApplicationContext(), CartActivity.class));
            }
        });
    }

    /*
    * Method to show dialog if product is non - returnable
    * */
    private void show_nonReturnablePopup() {
        android.app.AlertDialog.Builder builder1 = new android.app.AlertDialog.Builder(this);
        builder1.setMessage(product.getProduct_alert_message());
        builder1.setCancelable(false);

        builder1.setPositiveButton(
                "Ok",
                new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {

                    }
                });

        android.app.AlertDialog alert11 = builder1.create();
        alert11.show();
    }


    void calculateAndSetPrice() {
        if (product.getProduct_discount() != null && !product.getProduct_discount().equalsIgnoreCase("0")) {
            tvPriceMain.setVisibility(View.VISIBLE);
            tvDiscountMain.setVisibility(View.VISIBLE);
            tvFinalPriceMain.setText(Constants.RUPEE + Constants.getPriceWithDiscount(product.getProduct_price(), product.getProduct_discount()));
            tvPriceMain.setText(Constants.RUPEE + Constants.formatAmount(product.getProduct_price()));
            tvPriceMain.setPaintFlags(tvPriceMain.getPaintFlags() | Paint.STRIKE_THRU_TEXT_FLAG);
            tvDiscountMain.setText(product.getProduct_discount() + "% Off");

            //Set scroll view price
            tvPriceScroll.setVisibility(View.VISIBLE);
            tvDiscountScroll.setVisibility(View.VISIBLE);
            tvFinalPriceScroll.setText(Constants.RUPEE + Constants.getPriceWithDiscount(product.getProduct_price(), product.getProduct_discount()));
            tvPriceScroll.setText(Constants.RUPEE + Constants.formatAmount(product.getProduct_price()));
            tvPriceScroll.setPaintFlags(tvPriceScroll.getPaintFlags() | Paint.STRIKE_THRU_TEXT_FLAG);
            tvDiscountScroll.setText(product.getProduct_discount() + "% Off");
        } else {
            tvFinalPriceMain.setText(Constants.RUPEE + Constants.formatAmount(product.getProduct_price()));
            tvFinalPriceScroll.setText(Constants.RUPEE + Constants.formatAmount(product.getProduct_price()));
        }
    }

    void setUpViewPager() {
        //Initially set up only one image from ProductsActivity
        imageUrls = new ArrayList<>();
        imageUrls.add(product.getProduct_img());
        adapter = new SingleImageAdapter(this, imageUrls);
        imageViewPager.setAdapter(adapter);
        CirclePageIndicator pageIndicator = (CirclePageIndicator) findViewById(R.id.piProductImage);
        pageIndicator.setViewPager(imageViewPager);
    }

    void setUpImageViewAdapter() {

        if (product.getProduct_thumbnails().size() == 1) {
            findViewById(R.id.ivLeft).setVisibility(View.GONE);
            findViewById(R.id.ivRight).setVisibility(View.GONE);
        } else {
            findViewById(R.id.ivLeft).setVisibility(View.VISIBLE);
            findViewById(R.id.ivRight).setVisibility(View.VISIBLE);
        }

        adapter = new SingleImageAdapter(this, product.getProduct_thumbnails());
        imageViewPager.setAdapter(null);
        imageViewPager.setAdapter(adapter);
        CirclePageIndicator pageIndicator = (CirclePageIndicator) findViewById(R.id.piProductImage);
        pageIndicator.setViewPager(imageViewPager);
        adapter.setImageClick(new SingleImageAdapter.ImageClick() {
            @Override
            public void onImageClick(View v) {
                Bundle bundle = new Bundle();
                bundle.putStringArrayList(Constants.PRODUCT_THUMB_IMAGES, product.getProduct_thumbnails());
                bundle.putStringArrayList(Constants.PRODUCT_ZOOM_IMAGES, product.getProduct_images());
                ProductViewImageFragment imageFragment = new ProductViewImageFragment();
                imageFragment.setArguments(bundle);
                imageFragment.show(getmFragmentManager(), ProductViewImageFragment.class.getSimpleName());
            }
        });
    }

    void setUpSizesRecyclerView() {
        try {
            ArrayList<Product.ProductSizesEntity> productSizes = product.getProduct_sizes();
            if (productSizes.size() == 1 && productSizes.get(0).getAttr_id().equalsIgnoreCase(Constants.DEFAULT_SIZE_ID)) {
                selectedSize = productSizes.get(0);
                findViewById(R.id.llSize).setVisibility(View.GONE);
            } else {
                SizesAdapter adapter = new SizesAdapter(this, productSizes, llSizes, false);

                for (int i = 0; i < productSizes.size(); i++) {
                    if (CartOperations.getInstance().getCartList().size() > 0 && currentProductSize != null) {
                        for (int j = 0; j < currentProductSize.size(); j++)
                            if (currentProductSize.get(j).equalsIgnoreCase(productSizes.get(i).getAttr_value()))
                                adapter.setSelectedView(i);
                    }
                }

                adapter.setSizeSelected(new SizesAdapter.SizeSelected() {
                    @Override
                    public void onSizeSelected(Product.ProductSizesEntity size) {
                        selectedSize = size;
                        if (currentProductSize != null && currentProductSize.size() > 0) {
                            for (int i = 0; i < currentProductSize.size(); i++) {
                                if (currentProductSize.get(i).equalsIgnoreCase(selectedSize.getAttr_value())) {
                                    showToast(getString(R.string.text_already_added));
                                    bScrollBuyNow.setVisibility(View.GONE);
                                    bBuyNow.setVisibility(View.GONE);
                                    bScrollGotoBag.setVisibility(View.VISIBLE);
                                    bGotoBag.setVisibility(View.VISIBLE);
                                    break;
                                } else {
                                    bScrollBuyNow.setVisibility(View.VISIBLE);
                                    bBuyNow.setVisibility(View.VISIBLE);
                                    bScrollGotoBag.setVisibility(View.GONE);
                                    bGotoBag.setVisibility(View.GONE);
                                }
                            }
                        }
                    }
                });
            }
        } catch (NullPointerException ex) {
            ex.printStackTrace();
        }
        /* For added cart size */
        // llSizes.getChildAt(currentSize).setBackgroundColor(getResources().getColor(R.color.color_accent));
    }

    void setUpYouMayLike(ArrayList<Product> products) {
        rvYouMayLike = (CustomRecyclerView) findViewById(R.id.rvYouMayLike);
        if (products == null)
            rvYouMayLike.setVisibility(View.GONE);
        ProductsAdapter productAdapter = new ProductsAdapter(this, products, ProductsAdapter.PRODUCT_HORIZONTAL_SMALL, null, false);
        productAdapter.hideProgressBar();
        productAdapter.setOnItemClickListener(new BaseRecyclerAdapter.OnItemClickListener() {
            @Override
            public void onItemClick(View view, int position) {
                if (view.getTag() != null) {
                    Intent intent = new Intent(getApplicationContext(), ProductViewActivity.class);
                    intent.putExtra(Constants.PRODUCT_NAME, (Product) view.getTag());
                    startActivity(intent);
                    finish();
                }
            }
        });
        LinearLayoutManager layoutManager = new LinearLayoutManager(rvYouMayLike.getContext());
        layoutManager.setOrientation(LinearLayoutManager.HORIZONTAL);
        rvYouMayLike.setIsHorizontal(true);
        rvYouMayLike.setLayoutManager(layoutManager);
        rvYouMayLike.setAdapter(productAdapter);
    }

    void addToCart() {
        tempSize = selectedSize;
        JSONObject jsonObject = new JSONObject();
        try {
            jsonObject.put(Constants.AUTH_TOKEN, preferences.getString(Constants.KEY_TOKEN, ""));
            jsonObject.put(Constants.USER_ID, user.getUser_id());
            jsonObject.put(Constants.PRODUCT, product.getProduct_id());
            jsonObject.put("attr_id", selectedSize.getAttr_id());
            jsonObject.put("attr_value", selectedSize.getAttr_value());
            jsonObject.put("quantity", "1");
        } catch (JSONException e) {
            e.printStackTrace();
        }
        postJsonData(Urls.CART_ADD, jsonObject, Constants.TAG_CART_ADD, false);
    }

    @Override
    protected void onResume() {
        super.onResume();
        ActionItemBadge.update(this, menuCart,
                getResources().getDrawable(R.drawable.ic_cart), ActionItemBadge.BadgeStyles.RED,
                CartOperations.getInstance().getCartSize());
        isCalled = false;
        selectRecords(Constants.TAG_USER);
        // setUpVisibilityBtn();
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_home, menu);
        menuCart = menu.findItem(R.id.action_cart);
        setCartCount(menuCart);
      /*  ActionItemBadge.update(this, menuCart,
                getResources().getDrawable(R.drawable.ic_cart), ActionItemBadge.BadgeStyles.RED,
                CartOperations.getInstance().getCartSize());*/
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.action_profile:
                startActivity(new Intent(this, ProfileActivity.class));
                return true;
            case R.id.action_home:
                startActivity(new Intent(this, HomeActivity.class));
                return true;
            case R.id.action_cart:
                startActivity(new Intent(this, CartActivity.class));
                return true;
            default:
                break;
        }
        return super.onOptionsItemSelected(item);
    }


    void toggleViewVisibility() {
        findViewById(R.id.llPinCode).setVisibility(View.VISIBLE);
        findViewById(R.id.llSize).setVisibility(View.VISIBLE);
        findViewById(R.id.etPinCode).setVisibility(View.VISIBLE);
        findViewById(R.id.bCheckDeliveryOpts).setVisibility(View.VISIBLE);
        findViewById(R.id.tvColor).setVisibility(View.VISIBLE);
        findViewById(R.id.tvMaterial).setVisibility(View.VISIBLE);
        findViewById(R.id.tvDetail).setVisibility(View.VISIBLE);
        findViewById(R.id.tvStyle).setVisibility(View.VISIBLE);
        findViewById(R.id.tvCare).setVisibility(View.VISIBLE);
        findViewById(R.id.tvNote).setVisibility(View.VISIBLE);
        findViewById(R.id.tvYouMayLike).setVisibility(View.VISIBLE);
        findViewById(R.id.rvYouMayLike).setVisibility(View.VISIBLE);
        findViewById(R.id.tvStyleDesc).setVisibility(View.VISIBLE);
        findViewById(R.id.tvCareDesc).setVisibility(View.VISIBLE);
        findViewById(R.id.tvNoteDesc).setVisibility(View.VISIBLE);
        findViewById(R.id.tvColorDesc).setVisibility(View.VISIBLE);
        findViewById(R.id.tvMaterialDesc).setVisibility(View.VISIBLE);
    }


    @Override
    public <T> void onResponse(T obj, int tag) {
        super.onResponse(obj, tag);
        switch (tag) {
            case Constants.TAG_SINGLE_PRODUCT:
                product = (Product) obj;
                //trackPage(product.getProduct_name());
                if (subCat != null && subCat.getName() != null)
                    AnalyticsTracker.getInstance().trackProductView(product, subCat.getName());
                else
                    AnalyticsTracker.getInstance().trackProductView(product, "");

                setUpToolBar(product.getProduct_name());
                calculateAndSetPrice();
                toggleViewVisibility();
                setUpImageViewAdapter();
                setUpSizesRecyclerView();
                setProductDetails();
                getYouMayLikeProducts();

                // To set pincode from API
                if (product.getPincode() != null && !product.getPincode().equalsIgnoreCase("")) {
                    bChkDelOpts.setIndeterminateProgressMode(true);
                    bChkDelOpts.setProgress(1);
                    pincodeCheck(product.getPincode());
                }
                break;

            case Constants.TAG_CHK_DEL_OPTS:
                PinCodeResponse pinCodeResponse = (PinCodeResponse) obj;
                if (pinCodeResponse.getStatus().equalsIgnoreCase(Constants.RESPONSE_SUCCESS)) {
                    bChkDelOpts.setProgress(100);

                    if (pinCodeResponse.getCod_available()) {
                        etPinCode.setVisibility(View.GONE);
                        tvCODOptions.setVisibility(View.VISIBLE);
                        tvCODOptions.setText(pinCodeResponse.getMsg() + "\n" + pinCodeResponse.getExpected_delivery());

                    } else {
                        etPinCode.setVisibility(View.GONE);
                        tvCODOptions.setVisibility(View.VISIBLE);
                        tvCODOptions.setText(getString(R.string.text_only_ol_pay_avail) + "\n" + pinCodeResponse.getExpected_delivery());
                    }
                } else {
                    bChkDelOpts.setProgress(100);

                    etPinCode.setVisibility(View.GONE);
                    tvCODOptions.setVisibility(View.VISIBLE);
                    tvCODOptions.setText(pinCodeResponse.getMsg());
                }
                break;
            case Constants.TAG_CART_ADD:
                AddCartResponse addCartResponse = (AddCartResponse) obj;
                if (addCartResponse.getStatus().equalsIgnoreCase(Constants.RESPONSE_SUCCESS)) {

                    if (subCat != null)
                        AnalyticsTracker.getInstance().trackAddedToCart(addCartResponse, subCat.getName());
                    else
                        AnalyticsTracker.getInstance().trackAddedToCart(addCartResponse, "");

                    addCartResponse.getProductDetails().setProduct_attr_id(tempSize.getAttr_id());
                    addCartResponse.getProductDetails().setProduct_size(tempSize.getAttr_value());
                    addCartResponse.getProductDetails().setProduct_max_qty(tempSize.getQuantity());

                    CartOperations cartOperations = CartOperations.getInstance();
                    cartOperations.setCartUpdation(new CartOperations.CartUpdation() {
                        @Override
                        public void onCartUpdate() {
                            FlipAnimation flipAnimation = new FlipAnimation(fromView, toView);
                            if (fromView.getId() == R.id.bBuyNow)
                                findViewById(R.id.flButton).startAnimation(flipAnimation);
                            else
                                findViewById(R.id.flButtonScroll).startAnimation(flipAnimation);

                            bBuyNow.setVisibility(View.GONE);
                            bGotoBag.setVisibility(View.VISIBLE);
                            bScrollBuyNow.setVisibility(View.GONE);
                            bScrollGotoBag.setVisibility(View.VISIBLE);

                            ActionItemBadge.update(ProductViewActivity.this, menuCart,
                                    getResources().getDrawable(R.drawable.ic_cart), ActionItemBadge.BadgeStyles.RED,
                                    CartOperations.getInstance().getCartSize());
                        }
                    });
                    cartOperations.addItem(addCartResponse.getProductDetails());

                    if (CartOperations.getInstance().getCartSize() == Integer.MIN_VALUE)
                        user.setCart_count("1");
                    else {
                        int cnt = CartOperations.getInstance().getCartSize() + 1;
                        user.setCart_count(String.valueOf(cnt));
                    }
                    insertRecord(user, Constants.TAG_USER);
                } else
                    showToast(addCartResponse.getMessage());
                break;

            case Constants.TAG_YOU_MAY_LIKE:
                if (obj != null)
                    setUpYouMayLike(new ArrayList<>(Arrays.asList((Product[]) obj)));
                break;

            case Constants.TAG_SHORTLIST:
                showToast(getString(R.string.toast_added_to_shortlist));
                Shortlist shortlist = (Shortlist) obj;
                bFavorites.stopProgress(true);
                // productAdapter.changeProductState(shortlist.getProduct_id() + "", FavoritesButton.PROGRESS_COMPLETE);
                // productAdapter.notifyDataSetChanged();
                favorite_state = FavoritesButton.PROGRESS_COMPLETE;
                insertRecord(obj, tag);
                if (shortlists.size() == 0)
                    slidePanelTo(SlidingUpPanelLayout.PanelState.ANCHORED);
                break;

            case Constants.TAG_SHORTLIST_REMOVE:
                showToast(getString(R.string.toast_removed_from_shortlist));
                Shortlist shortlist1 = (Shortlist) obj;
                bFavorites.stopProgress(false);
                // productAdapter.changeProductState(shortlist1.getProduct_id() + "", FavoritesButton.PROGRESS_IDLE);
                //  productAdapter.notifyDataSetChanged();
                favorite_state = FavoritesButton.PROGRESS_IDLE;
                deleteRecord(shortlist1, Constants.TAG_SHORTLIST_REMOVE);
                break;

            case Constants.TAG_NOTIFY_PRODUCT:
                VisitorResponse notifyResponse = (VisitorResponse) obj;
                ((TextView) findViewById(R.id.tv_notify)).setText(notifyResponse.getMessage());

                break;
        }
    }

    private void getYouMayLikeProducts() {

        //For you may like
        JSONObject jsonObject = new JSONObject();
        try {
            jsonObject.put(Constants.USER_ID, user.getUser_id() + "");
            jsonObject.put(Constants.AUTH_TOKEN, preferences.getString(Constants.KEY_TOKEN, ""));
            jsonObject.put(Constants.CATEGORY, product.getCategory_id());
            jsonObject.put(Constants.PRODUCT, product.getProduct_id());

        } catch (JSONException e) {
            e.printStackTrace();
        } catch (NullPointerException ex) {
            ex.printStackTrace();
        }
        postJsonDataArray(Urls.YOU_MAY_LIKE, jsonObject, Constants.TAG_YOU_MAY_LIKE, false);
    }

    private void setProductDetails() {

        if (product.getProduct_offer() == null) {
            findViewById(R.id.rvOfferOfDay).setVisibility(View.GONE);
        } else {
            findViewById(R.id.rvOfferOfDay).setVisibility(View.VISIBLE);
            ((TextView) findViewById(R.id.tvText)).setText(product.getProduct_offer_title());
            ((TextView) findViewById(R.id.tvOffer)).setText(product.getProduct_offer());
            ((TextView) findViewById(R.id.tvOfferOfDay)).setText(product.getProduct_offer_desc());
        }

        findViewById(R.id.ivClose).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                findViewById(R.id.rvOfferOfDay).setVisibility(View.GONE);
            }
        });


        if (product.getProduct_stock().equalsIgnoreCase("0")) {
            Button btnBuy = (Button) findViewById(R.id.bBuyNow);
            btnBuy.setText(getString(R.string.text_out_of_stock_product));
            btnBuy.setClickable(false);

            Button btnBuyScroll = (Button) findViewById(R.id.bBuyNowScroll);
            btnBuyScroll.setText(getString(R.string.text_out_of_stock_product));
            btnBuyScroll.setClickable(false);
        }


        if (product.getProduct_notify_text() != null && !product.getProduct_notify_text().equalsIgnoreCase("")) {
            ((TextView) findViewById(R.id.tv_notify)).setText(product.getProduct_notify_text());
            findViewById(R.id.rlNotifyMe).setVisibility(View.VISIBLE);
            findViewById(R.id.view3).setVisibility(View.VISIBLE);
        } else {
            findViewById(R.id.rlNotifyMe).setVisibility(View.GONE);
            findViewById(R.id.view3).setVisibility(View.GONE);
        }


        findViewById(R.id.tvNotifyMe).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                ArrayList<Product.ProductSizesEntity> outOfProductSizes = product.getProduct_notify_sizes();
                if (outOfProductSizes != null && outOfProductSizes.size() > 0 && outOfProductSizes.size() == 1 && outOfProductSizes.get(0).getAttr_id().equalsIgnoreCase(Constants.DEFAULT_SIZE_ID)) {
                    selectedOutOfStockSize = outOfProductSizes.get(0);
                    notifyMe();
                } else
                    setOutofStockSizes();
            }
        });


        if (product.getProduct_color() == null) {
            findViewById(R.id.tvColor).setVisibility(View.GONE);
            findViewById(R.id.tvColorDesc).setVisibility(View.GONE);
        } else
            ((TextView) findViewById(R.id.tvColorDesc)).setText(product.getProduct_color());


        if (product.getProduct_material() == null) {
            findViewById(R.id.tvMaterial).setVisibility(View.GONE);
            findViewById(R.id.tvMaterialDesc).setVisibility(View.GONE);
        } else
            ((TextView) findViewById(R.id.tvMaterialDesc)).setText(product.getProduct_material());


        if (product.getProduct_style() == null) {
            findViewById(R.id.tvStyle).setVisibility(View.GONE);
            findViewById(R.id.tvStyleDesc).setVisibility(View.GONE);
        } else
            ((TextView) findViewById(R.id.tvStyleDesc)).setText(Html.fromHtml(product.getProduct_style(), null, new MyTagHandler()));

        if (product.getProduct_care() == null) {
            findViewById(R.id.tvCare).setVisibility(View.GONE);
            findViewById(R.id.tvCareDesc).setVisibility(View.GONE);
        } else
            ((TextView) findViewById(R.id.tvCareDesc)).setText(Html.fromHtml(product.getProduct_care(), null, new MyTagHandler()));

        if (product.getProduct_note() == null) {
            findViewById(R.id.tvNote).setVisibility(View.GONE);
            findViewById(R.id.tvNoteDesc).setVisibility(View.GONE);
        } else
            ((TextView) findViewById(R.id.tvNoteDesc)).setText(Html.fromHtml(product.getProduct_note(), null, new MyTagHandler()));


        String detailText = null;
        if (product.getDimensionAsString() == null)
            detailText = product.getProduct_details();
        else
            detailText = product.getProduct_details() + "<li> Dimensions: " + product.getDimensionAsString() + "<\\/li>";

        ((TextView) findViewById(R.id.tvDetailDesc)).setText(Html.fromHtml(detailText, null, new MyTagHandler()));

    }

    private void setOutofStockSizes() {

        ArrayList<Product.ProductSizesEntity> outOfProductSizes = product.getProduct_notify_sizes();

       /* for (int i = 0; i < productSizes.size(); i++) {
            if (productSizes.get(i).getQuantity().equalsIgnoreCase("0"))
                outOfProductSizes.add(productSizes.get(i));
        }*/
        final WeakReference<ProductViewActivity> productViewActivityWeakReference;
        productViewActivityWeakReference = new WeakReference<ProductViewActivity>(this);

        if (productViewActivityWeakReference.get() != null && !productViewActivityWeakReference.get().isFinishing()) {
            AlertDialog.Builder dialogBuilder = new AlertDialog.Builder(ProductViewActivity.this);
            LayoutInflater inflater = this.getLayoutInflater();
            final View dialogView = inflater.inflate(R.layout.custom_dialog, null);
            dialogBuilder.setView(dialogView);

            final LinearLayout llOutOfStockSize = (LinearLayout) dialogView.findViewById(R.id.llOutOfStockSizes);

            SizesAdapter outOfSizeAdapter = new SizesAdapter(this, outOfProductSizes, llOutOfStockSize, true);
            outOfSizeAdapter.setSizeSelected(new SizesAdapter.SizeSelected() {
                @Override
                public void onSizeSelected(Product.ProductSizesEntity size) {
                    Constants.debug("Size selected ::: " + size.getAttr_value());
                    selectedOutOfStockSize = size;
                }
            });

            // dialogBuilder.setTitle("Notify me");
            dialogBuilder.setMessage(getString(R.string.text_notify));
            dialogBuilder.setPositiveButton("Submit", new DialogInterface.OnClickListener() {
                public void onClick(DialogInterface dialog, int whichButton) {
                    if (selectedOutOfStockSize == null)
                        showToast(getString(R.string.select_size));
                    else
                        notifyMe();
                }
            });
            AlertDialog b = dialogBuilder.create();
            b.show();
        }
    }

    /*
    * Method to call notify
    * */
    private void notifyMe() {

        JSONObject jsonObject = new JSONObject();
        try {
            jsonObject.put(Constants.USER_ID, user.getUser_id() + "");
            jsonObject.put(Constants.AUTH_TOKEN, preferences.getString(Constants.KEY_TOKEN, ""));
            jsonObject.put(Constants.PRODUCT, product.getProduct_id());
            jsonObject.put(Constants.ATTR_ID, selectedOutOfStockSize.getAttr_id());
            jsonObject.put(Constants.ATTR_VALUE, selectedOutOfStockSize.getAttr_value());

        } catch (JSONException e) {
            e.printStackTrace();
        }
        postJsonData(Urls.NOTIFY_PRODUCT, jsonObject, Constants.TAG_NOTIFY_PRODUCT, true);
    }

    @Override
    public void onError(int error_code, int tag, Object object) {
        checkErrorCode(error_code, object);
        switch (tag) {
            case Constants.TAG_CHK_DEL_OPTS:
                bChkDelOpts.setProgress(-1);
            case Constants.TAG_YOU_MAY_LIKE:
                setUpYouMayLike(null);
                break;
            case Constants.TAG_SINGLE_PRODUCT:
                finish();
                break;
        }
    }

    @Override
    public void onWindowFocusChanged(boolean hasFocus) {
        if (viewMain != null)
            viewMain.getLocationOnScreen(positionMain);
    }

    public void addProductToShortList(Product product) {
        JSONObject jsonObject = new JSONObject();
        try {
            jsonObject.put(Constants.AUTH_TOKEN, preferences.getString(Constants.KEY_TOKEN, ""));
            jsonObject.put(Constants.USER_ID, user.getUser_id() + "");
            if (product != null)
                jsonObject.put(Constants.PRODUCT, product.getProduct_id());
            else
                jsonObject.put(Constants.PRODUCT, id);
        } catch (JSONException e) {
            e.printStackTrace();
        }
        postJsonData(Urls.SHORTLIST_ADD, jsonObject, Constants.TAG_SHORTLIST, false);

        if (subCat != null && subCat.getName() != null)
            AnalyticsTracker.getInstance().trackShortlist(product, subCat.getName());
        else
            AnalyticsTracker.getInstance().trackShortlist(product, "");
    }

    public void removeProductFromShortList(Product product) {
        JSONObject jsonObject = new JSONObject();
        try {
            jsonObject.put(Constants.AUTH_TOKEN, preferences.getString(Constants.KEY_TOKEN, ""));
            jsonObject.put(Constants.USER_ID, user.getUser_id());
            if (product != null)
                jsonObject.put(Constants.PRODUCT, product.getProduct_id());
            else
                jsonObject.put(Constants.PRODUCT, id);

        } catch (JSONException e) {
            e.printStackTrace();
        }
        postJsonData(Urls.SHORTLIST_REMOVE, jsonObject, Constants.TAG_SHORTLIST_REMOVE, false);
    }

    @Override
    public void onCompleteRetrieval(int tag, ArrayList obj, String msg) {
        super.onCompleteRetrieval(tag, obj, msg);
        switch (tag) {
            case Constants.TAG_SHORTLIST:
                final ArrayList<Shortlist> shortlists = obj;
                runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        bFavorites = (FavoritesButton) findViewById(R.id.bFavorites);
                        for (Shortlist shortlist : shortlists) {
                            if (product != null) {
                                if (product.getProduct_id().equalsIgnoreCase(shortlist.getProduct_id()))
                                    favorite_state = FavoritesButton.PROGRESS_COMPLETE;
                            }
                            else if (!id.isEmpty() && id.equalsIgnoreCase(shortlist.getProduct_id())){
                                favorite_state = FavoritesButton.PROGRESS_COMPLETE;
                            }

                        }
                        if (favorite_state == FavoritesButton.PROGRESS_COMPLETE)
                            bFavorites.stopProgress(true);
                        else if (favorite_state == FavoritesButton.PROGRESS_IDLE)
                            bFavorites.stopProgress(false);
                        else if (favorite_state == FavoritesButton.IN_PROGRESS)
                            bFavorites.startProgress();
                    }
                });
                break;

            case Constants.TAG_USER:
                if (!isCalled) {
                    if (obj.size() > 0)
                        user = (User) obj.get(0);
                    init();
                    isCalled = true;
                }
                break;
        }
    }
}
