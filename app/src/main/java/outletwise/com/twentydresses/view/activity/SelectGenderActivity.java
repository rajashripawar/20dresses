package outletwise.com.twentydresses.view.activity;

import android.content.Intent;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.squareup.picasso.Picasso;

import outletwise.com.twentydresses.R;
import outletwise.com.twentydresses.model.BackgroundImgResponse;
import outletwise.com.twentydresses.model.Registration;
import outletwise.com.twentydresses.model.database.greenbot.User;
import outletwise.com.twentydresses.utilities.Constants;

/**
 * This class is use to select gender of user
 */
public class SelectGenderActivity extends BaseActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_select_gender);
        //  setBackgroundImage(Constants.IMG_GENDER);

        User user = Registration.getInstance().getUser();

        if (user != null && user.getGender() != null && !user.getGender().equalsIgnoreCase("F")) {
            Intent intent = new Intent(getApplicationContext(), QuizActivity.class);
            startActivity(intent);
        }

        findViewById(R.id.tvMale).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                //trackPage(getString(R.string.pgGenderMale));
                Registration.getInstance().getUser().setGender("M");
                Intent intent = new Intent(getApplicationContext(), QuizActivity.class);
                startActivity(intent);
            }
        });

        findViewById(R.id.tvFemale).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                //trackPage(getString(R.string.pgGenderFemale));
                Registration.getInstance().getUser().setGender("F");
                Intent intent = new Intent(getApplicationContext(), RegistrationCompleteActivity.class);
                startActivity(intent);
            }
        });

    }

    @Override
    public <T> void onResponse(T obj, int tag) {
        switch (tag) {
            case Constants.TAG_IMG_BACKGROUND:
                findViewById(R.id.pbWheel).setVisibility(View.GONE);

                BackgroundImgResponse imgResponse = (BackgroundImgResponse) obj;
                if (imgResponse.getStatus().equalsIgnoreCase(Constants.RESPONSE_SUCCESS)) {
                    String imgUrl = imgResponse.getImage();
                    ImageView iv_background = (ImageView) findViewById(R.id.iv_background);
                    Picasso.with(getApplicationContext()).load(imgUrl).into(iv_background);
                    toggleVisibility();
                }

                break;
        }
    }

    private void toggleVisibility() {

        ((TextView) findViewById(R.id.tvMale)).setVisibility(View.VISIBLE);
        ((TextView) findViewById(R.id.tvFemale)).setVisibility(View.VISIBLE);
        ((TextView) findViewById(R.id.tvSelectGender)).setVisibility(View.VISIBLE);
    }

    @Override
    protected void onResume() {
        super.onResume();

        if (Registration.getInstance().getUser() != null && Registration.getInstance().getUser().getGender() != null) {

            if (Registration.getInstance().getUser().getGender().equalsIgnoreCase("F")) {
                ((TextView) findViewById(R.id.tvFemale)).setCompoundDrawablesWithIntrinsicBounds(0, //left
                        R.drawable.ic_female_selected, //top
                        0, //right
                        0);//bottom

                ((TextView) findViewById(R.id.tvMale)).setCompoundDrawablesWithIntrinsicBounds(0, //left
                        R.drawable.ic_male, //top
                        0, //right
                        0);//bottom
            } else if (Registration.getInstance().getUser().getGender().equalsIgnoreCase("M")) {
                ((TextView) findViewById(R.id.tvMale)).setCompoundDrawablesWithIntrinsicBounds(
                        0, //left
                        R.drawable.ic_male_selected, //top
                        0, //right
                        0);//bottom

                ((TextView) findViewById(R.id.tvFemale)).setCompoundDrawablesWithIntrinsicBounds(0, //left
                        R.drawable.ic_female, //top
                        0, //right
                        0);//bottom
            }


        } else if (Registration.getInstance().getUser() != null && Registration.getInstance().getUser().getGender() == null) {
            ((TextView) findViewById(R.id.tvFemale)).setCompoundDrawablesWithIntrinsicBounds(0, //left
                    R.drawable.ic_female, //top
                    0, //right
                    0);//bottom

            ((TextView) findViewById(R.id.tvMale)).setCompoundDrawablesWithIntrinsicBounds(0, //left
                    R.drawable.ic_male, //top
                    0, //right
                    0);//bottom
        }


    }
}
