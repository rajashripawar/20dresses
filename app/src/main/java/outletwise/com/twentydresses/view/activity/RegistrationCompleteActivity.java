package outletwise.com.twentydresses.view.activity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.ImageView;

import com.squareup.picasso.Picasso;

import outletwise.com.twentydresses.R;
import outletwise.com.twentydresses.model.BackgroundImgResponse;
import outletwise.com.twentydresses.utilities.Constants;


public class RegistrationCompleteActivity extends BaseActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_register_complete);
       // setBackgroundImage(Constants.IMG_THANK_YOU);

        findViewById(R.id.bStartQuiz).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(getApplicationContext(), QuizActivity.class);
                Constants.QUIZ = Constants.REGISTER;
                // intent.putExtra(Constants.QUIZ,Constants.REGISTER);
                startActivity(intent);
            }
        });
    }

    @Override
    public <T> void onResponse(T obj, int tag) {
        switch (tag) {
            case Constants.TAG_IMG_BACKGROUND:
                findViewById(R.id.pbWheel).setVisibility(View.GONE);

                BackgroundImgResponse imgResponse = (BackgroundImgResponse) obj;
                if (imgResponse.getStatus().equalsIgnoreCase(Constants.RESPONSE_SUCCESS)) {
                    String imgUrl = imgResponse.getImage();
                    ImageView iv_background = (ImageView) findViewById(R.id.iv_background);
                    Picasso.with(getApplicationContext()).load(imgUrl).into(iv_background);

                    toggleVisibility();
                }

                break;
        }
    }

    private void toggleVisibility() {

        findViewById(R.id.rlDetails).setVisibility(View.VISIBLE);
    }
}
