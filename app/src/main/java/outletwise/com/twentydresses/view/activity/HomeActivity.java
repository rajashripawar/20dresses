package outletwise.com.twentydresses.view.activity;

import android.Manifest;
import android.app.Activity;
import android.app.SearchManager;
import android.content.ActivityNotFoundException;
import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.BitmapFactory;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.os.StrictMode;
import android.support.design.widget.CollapsingToolbarLayout;
import android.support.design.widget.TabLayout;
import android.support.v4.app.ActivityCompat;
import android.support.v4.app.Fragment;
import android.support.v4.view.ViewPager;
import android.support.v7.widget.SearchView;
import android.util.DisplayMetrics;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.android.camera.CropImageIntentBuilder;
import com.sothree.slidinguppanel.SlidingUpPanelLayout;
import com.squareup.picasso.Picasso;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Calendar;

import it.sauronsoftware.ftp4j.FTPClient;
import it.sauronsoftware.ftp4j.FTPDataTransferListener;
import outletwise.com.twentydresses.R;
import outletwise.com.twentydresses.controller.AnalyticsTracker;
import outletwise.com.twentydresses.controller.adapter.DrawerAdapter;
import outletwise.com.twentydresses.controller.adapter.HomeTabAdapter;
import outletwise.com.twentydresses.model.Category;
import outletwise.com.twentydresses.model.LoginResponse;
import outletwise.com.twentydresses.model.Profile;
import outletwise.com.twentydresses.model.ProfilePictureResponse;
import outletwise.com.twentydresses.model.Registration;
import outletwise.com.twentydresses.model.database.greenbot.Shortlist;
import outletwise.com.twentydresses.model.database.greenbot.User;
import outletwise.com.twentydresses.utilities.Constants;
import outletwise.com.twentydresses.utilities.Urls;
import outletwise.com.twentydresses.view.fragment.HomeNewFragment;
import outletwise.com.twentydresses.view.fragment.HomeOffersFragment;
import outletwise.com.twentydresses.view.fragment.HomeRecommendationFragment;
import outletwise.com.twentydresses.view.fragment.HomeSaleFragment;
import outletwise.com.twentydresses.view.fragment.HomeTrendsFragment;

/**
 * Created by User-PC on 06-08-2015.
 * This is main home activity to show all offers, for you, new, sale products.
 * Also this activity is use to show left drawer for all menus and user profile info
 */
public class HomeActivity extends BaseActivity implements DrawerAdapter.OnDrawerItemClick {
    private static final int RESULT_LOAD_IMAGE = 1;
    private static int REQUEST_CROP_PICTURE = 2;
    private HomeTabAdapter homeTabAdapter;
    private ViewPager viewPager;
    private CollapsingToolbarLayout collapsingToolbarLayout;
    private ImageView ivProfile, ivStyleImage;
    private User user;
    private File croppedImageFile;
    private MenuItem menuCart;
    private LoginResponse loginResponse;
    private TextView tvStyle, tvUserName;
    private String strImgName;
    private ArrayList<Category> categories;
    private boolean isLoaded;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_home);

        trackPage(getString(R.string.pgHome));

        try {
            if (getIntent().getBooleanExtra(Constants.CLEAR_ACTIVITIES, false)) {
                getProfileData();
                Intent intent = new Intent(this, ProfileActivity.class);
                intent.putExtra(Constants.CLEAR_ACTIVITIES, getIntent().getBooleanExtra(Constants.CLEAR_ACTIVITIES, false));
                startActivity(intent);
            }
        } catch (Exception e) {
            e.printStackTrace();
            //Crashlytics.logException(e);
        }

        //Setup Toolbar as Action bar & navigation drawer
        setUpToolBar("");
        setToolbarLogo();
        setUpDrawer();

        viewPager = (ViewPager) findViewById(R.id.viewpager);
        selectRecords(Constants.TAG_USER);
        //If back pressed exit app
        setClearActivities(true);
    }

    private void setUpDrawer() {
        // get List from splash activity
        categories = getIntent().getParcelableArrayListExtra(Constants.CATEGORY);
        setUpNavDrawer(this, categories);
    }


    // Call login webservice to get user picture, style image etc.
    private void getProfileData() {
        Registration registration = Registration.getInstance();
        JSONObject jsonObject = new JSONObject();
        try {
            if (user != null && String.valueOf(user.getUser_id()) != null){
                if (!String.valueOf(user.getUser_id()).trim().isEmpty()){
                    jsonObject.put(Constants.USER_ID, user.getUser_id());
                }
            }
            else
            jsonObject.put(Constants.USER_ID, preferences.getString(Constants.USER_ID,""));

            jsonObject.put(Constants.AUTH_TOKEN, preferences.getString(Constants.KEY_TOKEN, ""));
        } catch (JSONException e) {
            e.printStackTrace();
        }
        postJsonData(Urls.PROFILE, jsonObject, Constants.TAG_PROFILE, false);
    }


    private void setUpTabLayout() {
        final TabLayout tabLayout = (TabLayout) findViewById(R.id.tabs);
        tabLayout.setTabTextColors(getResources().getColor(R.color.background_material_dark),
                getResources().getColor(R.color.color_accent));
        tabLayout.setupWithViewPager(viewPager);

        tabLayout.getChildAt(0);

        // Iterate over all tabs and set the custom view
        for (int i = 0; i < tabLayout.getTabCount(); i++) {
            TabLayout.Tab tab = tabLayout.getTabAt(i);
            tab.setCustomView(homeTabAdapter.getTabView(i));
        }


        tabLayout.post(new Runnable()
        {
            @Override
            public void run()
            {
                int tabLayoutWidth = tabLayout.getWidth();
                DisplayMetrics metrics = new DisplayMetrics();
                HomeActivity.this.getWindowManager().getDefaultDisplay().getMetrics(metrics);
                int deviceWidth = metrics.widthPixels;

                if (tabLayoutWidth < deviceWidth)
                {
                    tabLayout.setTabMode(TabLayout.MODE_FIXED);
                    tabLayout.setTabGravity(TabLayout.GRAVITY_FILL);
                } else
                {
                    tabLayout.setTabMode(TabLayout.MODE_SCROLLABLE);
                }

            }
        });


        tabLayout.setOnTabSelectedListener(new TabLayout.OnTabSelectedListener() {
            @Override
            public void onTabSelected(TabLayout.Tab tab) {
                /*int position = tab.getPosition();
                Constants.debug("position ::"+position);
                switch (position)
                {
                    case 2:
                        ((HomeNewFragment) homeTabAdapter.getItem(position)).refresh();
                        homeTabAdapter.notifyDataSetChanged();
                        break;
                    case 3:
                        ((HomeSaleFragment) homeTabAdapter.getItem(position)).refresh();
                        break;
                }*/    //  showToast("onclick :"+tab.getPosition());

                viewPager.setCurrentItem(tab.getPosition());
            }

            @Override
            public void onTabUnselected(TabLayout.Tab tab) {

            }

            @Override
            public void onTabReselected(TabLayout.Tab tab) {

            }
        });

    }

    private void setUpViewPager(ViewPager viewPager) {
        homeTabAdapter = new HomeTabAdapter(this, getSupportFragmentManager());

        String titles[] = getResources().getStringArray(R.array.page_titles);
        Fragment fragment = null;

        for (int i = 0; i < titles.length; i++) {
            Bundle b = new Bundle();
            Category.SubCat subCat = new Category.SubCat();
            subCat.setId(i + "");
            subCat.setName(titles[i]);
            b.putParcelable(Constants.CATEGORY, subCat);
                if (user != null && String.valueOf(user.getUser_id()) != null)
                    b.putString(Constants.USER_ID, String.valueOf(user.getUser_id()));
                else
                    b.putString(Constants.USER_ID, preferences.getString(Constants.USER_ID,""));
            switch (i) {
                case 0:
                    fragment = new HomeOffersFragment();
                    break;
                case 1:
                    fragment = new HomeRecommendationFragment();
                    break;
                case 2:
                    fragment = new HomeNewFragment();
                    break;
                case 3:
                    fragment = new HomeSaleFragment();
                    break;
                case 4:
                    /*here need to add trends activity as fragment*/
                    fragment = new HomeTrendsFragment();
                    break;
            }
            fragment.setArguments(b);
            homeTabAdapter.addFragment(fragment, titles[i]);
        }


        viewPager.setAdapter(homeTabAdapter);
          viewPager.addOnPageChangeListener(new ViewPager.OnPageChangeListener() {
            @Override
            public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {

            }

            @Override
            public void onPageSelected(int position) {
                homeTabAdapter.setNewSelectedTextView(position);
                Constants.debug("Nav menu selected ::" + String.valueOf(homeTabAdapter.getPageTitle(position)));
                AnalyticsTracker.getInstance().trackNavMenuSelected(String.valueOf(homeTabAdapter.getPageTitle(position)));
               /* switch (position) {
                    case 2:
                        ((HomeNewFragment) homeTabAdapter.getItem(position)).refresh();
                        break;
                    case 3:
                        ((HomeSaleFragment) homeTabAdapter.getItem(position)).refresh();
                        break;
                }*/
            }

            @Override
            public void onPageScrollStateChanged(int state) {

            }
        });

    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_home, menu);
        menu.findItem(R.id.action_home).setIcon(R.drawable.ic_home_selected);

     /*   if (user.getGender() != null && user.getGender().equalsIgnoreCase("Male"))
            menu.removeItem(R.id.action_profile);*/

        menuCart = menu.findItem(R.id.action_cart);
        setCartCount(menuCart);

        /*For changing actionbar color when search click, refer below link
        * http://stackoverflow.com/questions/29076628/how-to-change-action-bar-background-on-click-of-search-menu
        * */
     /*   SearchManager searchManager =
                (SearchManager) HomeActivity.this.getSystemService(Context.SEARCH_SERVICE);
        SearchView searchView =
                (SearchView) menu.findItem(R.id.action_search).getActionView();

        searchView.setSearchableInfo(searchManager.getSearchableInfo(
                new ComponentName(getApplicationContext(), ProductsActivity.class)));

        searchView.setOnQueryTextListener(new SearchView.OnQueryTextListener() {
            @Override
            public boolean onQueryTextSubmit(String query) {
                Constants.debug("Query :: " + query);
                return false;
            }

            @Override
            public boolean onQueryTextChange(String newText) {
                Constants.debug("TextChange query :: " + newText);
                return false;
            }

        });*/
        return true;
    }


    @Override
    protected void onResume() {
        super.onResume();
        isLoaded = false;
        Constants.LATESTSORT = 1;
        setCartCount(menuCart);
        if (user != null)
            setUpSlidingPanel();

        if (Constants.UPDATE_PROFILE)
            updateProfile();
        else if (user != null)
            setUpProfile(user.getUser_first_name(), user.getUser_last_name(), user.getUser_style(), user.getUser_picture(), user.getUser_background_image());

        viewPager.setOffscreenPageLimit(2);

        if (Constants.GOTO_WISHLIST) {
            setUpSlidingPanel();
            slidePanelTo(SlidingUpPanelLayout.PanelState.ANCHORED);
            Constants.GOTO_WISHLIST = false;
        }
    }


    private void updateProfile() {

        if (user != null) {
            //   setUpDrawer();
            JSONObject jsonObject = new JSONObject();
            try {
                if (user != null && String.valueOf(user.getUser_id()) != null){
                    if (!String.valueOf(user.getUser_id()).trim().isEmpty()){
                        jsonObject.put(Constants.USER_ID, user.getUser_id());
                    }
                }
                else
                    jsonObject.put(Constants.USER_ID, preferences.getString(Constants.USER_ID,""));
                jsonObject.put(Constants.AUTH_TOKEN, preferences.getString(Constants.KEY_TOKEN, ""));
            } catch (JSONException e) {
                e.printStackTrace();
            }
            postJsonData(Urls.PROFILE, jsonObject, Constants.TAG_PROFILE, false);
        }
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.action_profile:
                startActivity(new Intent(this, ProfileActivity.class));
                return true;
            case R.id.action_home:
                return true;
            case R.id.action_cart:
                startActivity(new Intent(this, CartActivity.class));
                return true;
           /* case R.id.action_search:
                startActivity(new Intent(this, SearchActivity.class));
                return true;*/
            default:
                break;
        }
        return super.onOptionsItemSelected(item);
    }


    private void setUpProfile(String userFirstName, String userLastName, String user_style, String user_picture, String user_background_image) {

        collapsingToolbarLayout = (CollapsingToolbarLayout) findViewById(R.id.collapsing_toolbar);
        ivProfile = (ImageView) collapsingToolbarLayout.findViewById(R.id.ivProfile);
        ivStyleImage = (ImageView) collapsingToolbarLayout.findViewById(R.id.imgvw_styleImg);
        tvStyle = (TextView) collapsingToolbarLayout.findViewById(R.id.tvStyle);
        tvUserName = (TextView) collapsingToolbarLayout.findViewById(R.id.tvName);
        ivProfile.setOnClickListener(this);
        //Set User name
        tvUserName.setText(userFirstName + Constants.SPACE + userLastName);
        //Set Style name
        if (!(user_style == null))
            tvStyle.setText(user_style);
        else
            tvStyle.setText("");

        if (user != null) {
            getDrawerAdapter().setUser(user);
            if (user.getUser_picture() != null && !user.getUser_picture().equalsIgnoreCase(""))
                Picasso.with(this).load(user_picture).into(ivProfile);

            if (user.getUser_background_image() != null && !user.getUser_background_image().equalsIgnoreCase(""))
                Picasso.with(this).load(user_background_image).into(ivStyleImage);
        }

        StrictMode.ThreadPolicy policy = new StrictMode.ThreadPolicy.Builder().permitAll().build();
        StrictMode.setThreadPolicy(policy);
        // Set user Image
   /*     URL url = null;
        Bitmap image;
        try {
            if (user.getUser_picture() != null && user.getUser_picture().equalsIgnoreCase("")) {
                url = new URL(user.getUser_picture());
                image = BitmapFactory.decodeStream(url.openConnection().getInputStream());
                ivProfile.setImageBitmap(image);
            }
        } catch (MalformedURLException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }*/
    }


    @Override
    public void onClick(View v) {
        super.onClick(v);
        switch (v.getId()) {
            case R.id.ivProfile:
                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                    int hasReadStoragePermission = checkSelfPermission(Manifest.permission.READ_EXTERNAL_STORAGE);
                    if (hasReadStoragePermission != PackageManager.PERMISSION_GRANTED) {
                        requestPermissions(new String[]{Manifest.permission.READ_EXTERNAL_STORAGE}, 1);
                        return;
                    } else
                        cropImage();
                } else
                    cropImage();
                break;
        }
    }


    private void cropImage() {
        strImgName = "test_" + Calendar.getInstance().getTimeInMillis() + ".jpg";
        croppedImageFile = new File(getCacheDir(), strImgName);
        try {
            croppedImageFile.createNewFile();
        } catch (IOException e) {
            e.printStackTrace();
        }

        Intent i = new Intent(
                Intent.ACTION_PICK, android.provider.MediaStore.Images.Media.EXTERNAL_CONTENT_URI);
        startActivityForResult(i, RESULT_LOAD_IMAGE);
    }


    @Override
    public void onRequestPermissionsResult(int requestCode, String[] permissions, int[] grantResults) {
        // super.onRequestPermissionsResult(requestCode, permissions, grantResults);

        if (grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
            // Permission Granted
            cropImage();
        } else
            showToast(getString(R.string.text_permission_denied));
    }


    @Override
    public <T> void onResponse(T obj, int tag) {
        super.onResponse(obj, tag);
        switch (tag) {
            case Constants.TAG_PROFILE:
                Profile profile = (Profile) obj;
                if (profile.getStatus().equalsIgnoreCase("Success")) {
                        setUpProfile(profile.getUser_first_name(), profile.getUser_last_name(), profile.getUser_style(), profile.getUser_picture(), profile.getUser_background_image());
                        if (user != null) {
                            user.setUser_first_name(profile.getUser_first_name());
                            user.setUser_last_name(profile.getUser_last_name());
                            user.setDob(profile.getDob());
                            user.setUser_mobile(profile.getUser_mobile());
//                    user.setUser_id(Long.parseLong(profile.getUser_id()));
                            user.setGender(profile.getUser_gender());
                            user.setUser_background_image(profile.getUser_background_image());
                            user.setUser_email(profile.getEmail());
                            user.setUser_picture(profile.getUser_picture());
                            user.setUser_style(profile.getUser_style());
                            insertRecord(user, Constants.TAG_USER);
                            getDrawerAdapter().setUser(user);
                        }
                }
                else{
                    signOutFromApp();
                    startActivity(new Intent(getApplicationContext(), RegistrationActivity.class));
                    finish();
                }
                break;

            case Constants.TAG_UPDATE_PROFILE_PIC:
                ProfilePictureResponse profilePictureResponse = (ProfilePictureResponse) obj;
                if (profilePictureResponse.getStatus().equalsIgnoreCase(Constants.RESPONSE_SUCCESS))
                    showToast(getString(R.string.toast_img_uploaded));
                break;
            case Constants.TAG_SHORTLIST_VIEW:
                if (obj != null) {
                    Shortlist shortlist = (Shortlist) obj;
                    insertRecord(shortlist.getShortlist_items(), Constants.TAG_SHORTLIST);
                }
                break;
        }
    }


    @Override
    public void onCompleteInsertion(int tag, String msg) {
        super.onCompleteInsertion(tag, msg);

        switch (tag) {
            case Constants.TAG_USER:
                selectRecords(tag);
                break;
            case Constants.TAG_SHORTLIST:
                setUpSlidingPanel();
                break;
        }
    }

    @Override
    public void onCompleteRetrieval(int tag, ArrayList obj, String msg) {
        super.onCompleteRetrieval(tag, obj, msg);
        switch (tag) {
            case Constants.TAG_USER:
                 /*Viewpager by default calls 2 times after initialization,
                 isLoaded is use to call viewpager once*/
                setUpSlidingPanel();

                if (!isLoaded) {
                    isLoaded = true;
                    if (obj != null && obj.size() > 0)
                        user = (User) obj.get(0);


                    getShortlistItems();
                    //CleverTap
                    AnalyticsTracker.getInstance().setRegProperties(user);

                    if (Constants.UPDATE_PROFILE || Constants.CHECK_RECOMMENTATION) {
                        updateProfile();
                        viewPager.setOffscreenPageLimit(1);
                        setUpViewPager(viewPager);
                    } else {
                        viewPager.setOffscreenPageLimit(2);
                        setUpViewPager(viewPager);
                    }

                    setUpProfile(user.getUser_first_name(), user.getUser_last_name(), user.getUser_style(), user.getUser_picture(), user.getUser_background_image());
                    setUpTabLayout();

                    if (Constants.UPDATE_PROFILE || Constants.CHECK_RECOMMENTATION) {
                        if (Constants.UPDATE_PROFILE)
                            Constants.UPDATE_PROFILE = false;
                        if (Constants.CHECK_RECOMMENTATION)
                            Constants.CHECK_RECOMMENTATION = false;
                        viewPager.setCurrentItem(1);
//                        findViewById(R.id.pbWheel).setVisibility(View.GONE);
                    }
                }
                break;
        }
    }


    public void getShortlistItems() {
        JSONObject jsonObject = new JSONObject();
        try {
            if (user != null && String.valueOf(user.getUser_id()) != null)
                jsonObject.put(Constants.USER_ID, user.getUser_id());
            else
                jsonObject.put(Constants.USER_ID, preferences.getString(Constants.USER_ID,""));

            jsonObject.put(Constants.AUTH_TOKEN, preferences.getString(Constants.KEY_TOKEN, ""));
            postJsonData(Urls.SHORTLIST_VIEW, jsonObject, Constants.TAG_SHORTLIST_VIEW, false);

        } catch (JSONException e) {
            e.printStackTrace();
        } catch (NullPointerException ex) {
            ex.printStackTrace();
        }

        /*HashMap<String, String> params = new HashMap<>();
        params.put(Constants.USER_ID, user.getUser_id() + "");
        params.put(Constants.AUTH_TOKEN, preferences.getString(Constants.KEY_TOKEN, ""));
        postStringData(Urls.SHORTLIST_VIEW, params, Constants.TAG_SHORTLIST_VIEW, false);*/
    }

    @Override
    public void onCompleteDeletion(int tag, String msg) {
        super.onCompleteDeletion(tag, msg);
        switch (tag) {
            case Constants.TAG_USER:
                // finish();
                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.JELLY_BEAN) {
                    finishAffinity();
                    Intent i  = new Intent(HomeActivity.this, RegistrationActivity.class);
                    i.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP|Intent.FLAG_ACTIVITY_SINGLE_TOP);
                    startActivity(i);
                }
                else {
                    ActivityCompat.finishAffinity(this);
                    Intent i  = new Intent(HomeActivity.this, RegistrationActivity.class);
                    i.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP|Intent.FLAG_ACTIVITY_SINGLE_TOP);
                    startActivity(i);
                }
                break;
        }
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        if (requestCode == RESULT_LOAD_IMAGE && resultCode == Activity.RESULT_OK && data != null) {

            Uri croppedImage = Uri.fromFile(croppedImageFile);
            CropImageIntentBuilder cropImage = new CropImageIntentBuilder(ivProfile.getWidth(), ivProfile.getWidth(), croppedImage);
            cropImage.setCircleCrop(true);
            cropImage.setOutlineCircleColor(getResources().getColor(R.color.color_accent));
            cropImage.setSourceImage(data.getData());

            startActivityForResult(cropImage.getIntent(this), REQUEST_CROP_PICTURE);

        } else if ((requestCode == REQUEST_CROP_PICTURE) && (resultCode == Activity.RESULT_OK)) {
            BitmapFactory.Options options = new BitmapFactory.Options();
            options.inJustDecodeBounds = true;
            BitmapFactory.decodeFile(croppedImageFile.getPath(), options);
            int width = options.outWidth;
            int height = options.outHeight;
            this.uploadFileToFtp(croppedImageFile, width, height);

            //updateProfilePic();

            user.setUser_picture(Uri.fromFile(croppedImageFile).toString());
            getDrawerAdapter().setUser(user);   //Update Drawer Profile icon
            Picasso.with(this).load(Uri.fromFile(croppedImageFile).toString())
                    .resize(ivProfile.getWidth(), ivProfile.getHeight())
                    .into(ivProfile);
            insertRecord(user, Constants.TAG_USER);
        }
    }

    //Used to upload user profile image
    public void uploadFileToFtp(final File file, final int width, final int height) {
        runOnUiThread(new Runnable() {
            @Override
            public void run() {
                FTPClient client = new FTPClient();
                try {
                    File f = Constants.getCompressedImage(getApplicationContext(), file, width, height);
                    client.connect(getString(R.string.ftp_address));
                    client.login(getString(R.string.ftp_username), getString(R.string.ftp_password));
                    client.setType(FTPClient.TYPE_BINARY);
                    client.changeDirectory("/images/");
                    // client.upload(f, new MyTransferListener());
                    client.upload(f, new FTPDataTransferListener() {
                        @Override
                        public void started() {
                            Constants.debug("Image upload started...");
                        }

                        @Override
                        public void transferred(int i) {

                        }

                        @Override
                        public void completed() {
                            Constants.debug("Image upload completed...");
                            //After completion of uploading image to ftp call update prof pic webservice
                            updateProfilePic();
                        }

                        @Override
                        public void aborted() {

                        }

                        @Override
                        public void failed() {

                        }
                    });
                } catch (Exception e) {
                    e.printStackTrace();
                    try {
                        client.disconnect(true);
                    } catch (Exception e2) {
                        e2.printStackTrace();
                    }
                }
            }
        });
    }


    public void updateProfilePic() {

        JSONObject jsonObject = new JSONObject();
        try {
            if (user != null && String.valueOf(user.getUser_id()) != null)
                jsonObject.put(Constants.USER_ID, user.getUser_id());
            else
                jsonObject.put(Constants.USER_ID, preferences.getString(Constants.USER_ID,""));

            jsonObject.put(Constants.AUTH_TOKEN, preferences.getString(Constants.KEY_TOKEN, ""));
            jsonObject.put("user_picture", strImgName);
        } catch (JSONException e) {
            e.printStackTrace();
        }
        postJsonData(Urls.UPDATE_PROFILE_PIC, jsonObject, Constants.TAG_UPDATE_PROFILE_PIC, false);
    }


    @Override
    public void onChildItemClick(Object tag) {
        if (tag != null) {
            if (tag instanceof Category.SubCat) {
                Category.SubCat subCat = (Category.SubCat) tag;
                String page = subCat.getPage();
                String parent_page = subCat.getParent_page();

                Class<?> class_name;
                if (parent_page.equalsIgnoreCase("my_account")) {

                    Constants.debug("Selected Menu ::" + page);
                    AnalyticsTracker.getInstance().trackMenuSelected(page);

                    switch (page) {
                        case "account_info":
                            class_name = MyAccount.class;
                            break;
                        case "my_wallet":
                            class_name = MyWalletActivity.class;
                            break;
                        case "my_credits":
                            class_name = MyCreditsActivity.class;
                            break;
                        case "change_password":
                            class_name = ChangePasswordActivity.class;
                            break;
                        default:
                            class_name = ProductsActivity.class;
                    }
                }
                else {
                    class_name = ProductsActivity.class;
                    Constants.debug("Selected Menu ::" + subCat.getName());
                    AnalyticsTracker.getInstance().trackMenuSelected(subCat.getName());
                }

                Intent intent = new Intent(getApplicationContext(), class_name);
                intent.putExtra(Constants.CATEGORY, subCat);
                startActivity(intent);

            } else
                drawerAction((Category) tag);

        }
    }

    private void drawerAction(Category category) {
        if (category.getSubCats().isEmpty()) {

            if (category.getPage_type() != null && category.getPage_type().equalsIgnoreCase("html")) {

                Constants.debug("Selected Menu " + category.getName());

                Intent intent = new Intent(this, TermsActivity.class);
                intent.putExtra("page_url", category.getPage_url());
                intent.putExtra("page_name", category.getName());
                startActivity(intent);
            } else {

                Constants.debug("Selected Menu " + category.getPage());

                switch (category.getPage()) {
                    case "my_orders":
                        startActivity(new Intent(this, MyOrdersActivity.class));
                        break;
                    case "my_shortlist":
                        closeDrawer();
                        slidePanelTo(SlidingUpPanelLayout.PanelState.EXPANDED);
                        break;
                    case "sale":
                        Category.SubCat subCat = new Category.SubCat();
                        subCat.setId(Constants.SALE_CAT);
                        subCat.setName("Sale");
                        Intent intent = new Intent(getApplicationContext(), ProductsActivity.class);
                        intent.putExtra(Constants.CATEGORY, subCat);
                        startActivity(intent);
                        break;
                    case "trends":
                        startActivity(new Intent(this, TrendsActivity.class));
                        break;
                    case "testimonials":
                        startActivity(new Intent(this, TestimonialsActivity.class));
                        break;
                    case "help_and_faq":
                        startActivity(new Intent(this, HelpFaqActivity.class));
                        break;
                    case "contact_us":
                        startActivity(new Intent(this, ContactUsActivity.class));
                        break;
                    case "invite_and_earn":
                        startActivity(new Intent(this, InviteActivity.class));
                        break;
                    case "about_us":
                        startActivity(new Intent(this, AboutusActivity.class));
                        break;
                    case "rate_the_app":
                        Uri uri = Uri.parse("market://details?id=" + getPackageName());
                        Intent myAppLinkToMarket = new Intent(Intent.ACTION_VIEW, uri);
                        try {
                            startActivity(myAppLinkToMarket);
                        } catch (ActivityNotFoundException e) {
                            Toast.makeText(this, " Unable to find market app", Toast.LENGTH_LONG).show();
                        }
                        break;
                    case "sign_out":
                        signOutFromApp();
                       /* logoutFromFacebook();
                        deleteAllRecords(Constants.TAG_USER);
                        preferences.edit().remove(Constants.KEY_TOKEN).commit();*/
                        break;
                    case "just_sold":
                        Category.SubCat subCat1 = new Category.SubCat();
                        subCat1.setId("just_sold");
                        subCat1.setName("JUST SOLD");
                        Intent intent1 = new Intent(getApplicationContext(), ProductsActivity.class);
                        intent1.putExtra(Constants.CATEGORY, subCat1);
                        startActivity(intent1);
                        break;
                }
            }
        }
    }

}
