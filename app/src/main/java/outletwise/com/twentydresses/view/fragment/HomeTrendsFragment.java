package outletwise.com.twentydresses.view.fragment;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.sothree.slidinguppanel.SlidingUpPanelLayout;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

import outletwise.com.twentydresses.R;
import outletwise.com.twentydresses.controller.AnalyticsTracker;
import outletwise.com.twentydresses.controller.adapter.BaseRecyclerAdapter;
import outletwise.com.twentydresses.controller.adapter.ProductsAdapter;
import outletwise.com.twentydresses.controller.adapter.TrendsAdapter;
import outletwise.com.twentydresses.model.Category;
import outletwise.com.twentydresses.model.Product;
import outletwise.com.twentydresses.model.Trends;
import outletwise.com.twentydresses.model.database.greenbot.Shortlist;
import outletwise.com.twentydresses.utilities.Constants;
import outletwise.com.twentydresses.utilities.Urls;
import outletwise.com.twentydresses.view.activity.BaseActivity;
import outletwise.com.twentydresses.view.activity.ProductsActivity;
import outletwise.com.twentydresses.view.custom.CustomRecyclerView;
import outletwise.com.twentydresses.view.custom.FavoritesButton;

/**
 * Created by webwerks on 10/7/16.
 */
public class HomeTrendsFragment extends BaseFragment implements SwipeRefreshLayout.OnRefreshListener {

    private View rootView;
    private TrendsAdapter adapter;
    private ArrayList<Trends.DataEntity> data = new ArrayList<>();
    private SwipeRefreshLayout mSwipeRefreshLayout;
    private CustomRecyclerView rv;
    private ArrayList<Product> products = new ArrayList<>();
    private boolean isLoading;
    private int page = 1;
    private ProductsAdapter productAdapter;
    private Category.SubCat subCat;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        rootView = inflater.inflate(R.layout.trends_fragment_layout, container, false);
        mSwipeRefreshLayout = (SwipeRefreshLayout) rootView.findViewById(R.id.swipeRefreshLayout);
        mSwipeRefreshLayout.setOnRefreshListener(this);
        subCat = getArguments().getParcelable(Constants.CATEGORY);
        mSwipeRefreshLayout = (SwipeRefreshLayout) rootView.findViewById(R.id.swipeRefreshLayout);
        mSwipeRefreshLayout.setOnRefreshListener(this);
        rootView.findViewById(R.id.pbWheel).setVisibility(View.VISIBLE);
        setUpRecyclerView();
        return rootView;
    }

    private void setUpRecyclerView() {
        RecyclerView rv = (RecyclerView)rootView.findViewById(R.id.rvTrends);
        // rv.setLayoutManager(new LinearLayoutManager(rv.getContext(), LinearLayoutManager.VERTICAL, false));
        rv.setLayoutManager(new GridLayoutManager(getActivity(), 1));

        adapter = new TrendsAdapter(getActivity(), data);
        rv.setAdapter(adapter);
        adapter.setOnItemClickListener(new BaseRecyclerAdapter.OnItemClickListener() {
            @Override
            public void onItemClick(View view, int position) {
                Trends.DataEntity data = (Trends.DataEntity) view.getTag();
                Constants.debug("Trend Id:: " + data.getTrend_id());

                AnalyticsTracker.getInstance().trackTrendsClicked(data.getTrend_id(), data.getTrend_caption());

                Category.SubCat subCat = new Category.SubCat();
                subCat.setId(data.getTrend_id());
                subCat.setName("TRENDS");
                Intent intent = new Intent(getActivity(), ProductsActivity.class);
                Constants.TIP_NAME = data.getTrend_caption();
                Constants.TIP_TOTAL = Integer.parseInt(data.getTotal());
                intent.putExtra(Constants.CATEGORY, subCat);
                startActivity(intent);
            }
        });

        postJsonData(Urls.TRENDS, new JSONObject(), Constants.TAG_TRENDS, false);
    }

    @Override
    public <T> void onResponse(T obj, int tag) {
        switch (tag) {
            case Constants.TAG_TRENDS:
                rootView.findViewById(R.id.pbWheel).setVisibility(View.GONE);
                Trends trends = (Trends) obj;
                if (trends.getStatus().equalsIgnoreCase(Constants.RESPONSE_SUCCESS)) {
                    data.clear();
                    data.addAll(trends.getData());
                    adapter.notifyDataSetChanged();
                } else
                    showToast(trends.getMessage());
                mSwipeRefreshLayout.setRefreshing(false);
                break;
        }
    }

    @Override
    public void onRefresh()
    {
        setUpRecyclerView();
    }

    @Override
    public void onError(int error_code, int tag, Object object) {
        super.onError(error_code, tag, object);
    }
}

