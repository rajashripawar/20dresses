package outletwise.com.twentydresses.view.activity;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.ActionBar;
import android.text.TextUtils;
import android.view.View;
import android.widget.TextView;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

import outletwise.com.twentydresses.R;
import outletwise.com.twentydresses.model.VerifyMobileResponse;
import outletwise.com.twentydresses.model.database.greenbot.User;
import outletwise.com.twentydresses.utilities.Constants;
import outletwise.com.twentydresses.utilities.Urls;

/**
 * Created by User-PC on 07-11-2015.
 * Verify mobile number after placing order (If mobile number not verified previously)
 */
public class VerifyOrderActivity extends BaseActivity {

    private String mobile;
    private int orderId;
    private TextView tvMobile, tvOtp;
    public static final String ACTION_RESP = "outletwise.com.twentydresses.verifyorderactivity";
    private User user;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_verify_order);

        setUpToolBar(getString(R.string.toolbar_verify_order));
        final ActionBar ab = getSupportActionBar();
        ab.setDisplayHomeAsUpEnabled(true);

        init();
        selectRecords(Constants.TAG_USER);
        findViewById(R.id.bVerify).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                verifyNumber();
            }
        });
    }

    private void verifyNumber() {

        String otp = tvOtp.getText().toString().trim();
        if (!TextUtils.isEmpty(otp)) {
            JSONObject jsonObject = new JSONObject();
            try {
                if (user != null)
                    jsonObject.put(Constants.USER_ID, user.getUser_id() + "");
                else
                    jsonObject.put(Constants.USER_ID, preferences.getString(Constants.USER_ID, ""));
                jsonObject.put(Constants.USER_ID, user.getUser_id());
                jsonObject.put(Constants.AUTH_TOKEN, preferences.getString(Constants.KEY_TOKEN, ""));
                jsonObject.put(Constants.MOBILE, mobile);
                //if (orderId != 0)
                jsonObject.put(Constants.ORDER_ID, orderId);
                jsonObject.put(Constants.OTP, otp);
            } catch (JSONException e) {
                e.printStackTrace();
            }
            postJsonData(Urls.USER_VERIFY_MOBILE, jsonObject, Constants.TAG_USER_CONFIRM_MOBILE, false);

        } else {
            showToast(getString(R.string.text_enter_otp));
        }
    }


    void init() {
        orderId = getIntent().getIntExtra(Constants.ORDER_ID, 0);
        mobile = getIntent().getStringExtra(Constants.MOBILE);

        tvMobile = ((TextView) findViewById(R.id.tvMobile));
        tvOtp = ((TextView) findViewById(R.id.tvOTP));
    }

    @Override
    public void onCompleteRetrieval(int tag, ArrayList obj, String msg) {
        super.onCompleteRetrieval(tag, obj, msg);
        switch (tag) {
            case Constants.TAG_USER:
                user = (User) obj.get(0);
                tvMobile.setText(mobile);
                JSONObject jsonObject = new JSONObject();
                try {
                    jsonObject.put(Constants.USER_ID, user.getUser_id());
                    jsonObject.put(Constants.AUTH_TOKEN, preferences.getString(Constants.KEY_TOKEN, ""));
                    jsonObject.put(Constants.MOBILE, mobile);
                    //  if (orderId != 0)
                    jsonObject.put(Constants.ORDER_ID, orderId);
                } catch (JSONException e) {
                    e.printStackTrace();
                }
                postJsonData(Urls.USER_VERIFY_MOBILE, jsonObject, Constants.TAG_USER_VERIFY_MOBILE, false);
                break;
        }
    }

    @Override
    public <T> void onResponse(T obj, int tag) {
        super.onResponse(obj, tag);
        switch (tag) {
            case Constants.TAG_USER_CONFIRM_MOBILE:
                VerifyMobileResponse response = (VerifyMobileResponse) obj;
                if (response.getStatus().equalsIgnoreCase(Constants.RESPONSE_SUCCESS)) {
                  /*  if (orderId == 0) {
                        Intent intent = new Intent(getApplicationContext(), HomeActivity.class);
                        intent.putExtra(Constants.CLEAR_ACTIVITIES, true);
                        intent.putExtra(Constants.START_PROFILE_ACTIVITY, true);
                        startActivity(intent);
                    } else*/
                    //   startActivity(new Intent(this, OrderSuccessfulActivity.class));
                    Intent intent = new Intent(getApplicationContext(), OrderSuccessfulActivity.class);
                    intent.putExtra(Constants.ORDER_ID, orderId);
                    startActivity(intent);
                } else
                    showToast(response.getMessage());
                findViewById(R.id.pbWheel).setVisibility(View.GONE);
                break;

            case Constants.TAG_USER_VERIFY_MOBILE:
                VerifyMobileResponse response1 = (VerifyMobileResponse) obj;
                showToast(response1.getMessage());
                /*if (orderId == 0) {
                    Intent intent = new Intent(getApplicationContext(), HomeActivity.class);
                    intent.putExtra(Constants.CLEAR_ACTIVITIES, true);
                    intent.putExtra(Constants.START_PROFILE_ACTIVITY, true);
                    startActivity(intent);
                }*/
                break;
        }
    }

    @Override
    protected void onResume() {
        super.onResume();
     /*   if (orderId == 0)
            registerReceiver(receiver, new IntentFilter(ACTION_RESP));*/
    }

    @Override
    protected void onPause() {
        super.onPause();
      /*  if (orderId == 0)
            unregisterReceiver(receiver);*/
    }


    /*private BroadcastReceiver receiver = new BroadcastReceiver() {

        @Override
        public void onReceive(Context context, Intent intent) {
            if (intent.getAction().equals(VerifyOrderActivity.ACTION_RESP)) {
                String strMsg = intent.getStringExtra(SmsListener.MESSAGE);
                updateText(strMsg);
            }
        }
    };*/

    @Override
    public void onBackPressed() {
        gotoHome();
    }

    void gotoHome() {
        Intent intent = new Intent(this, HomeActivity.class);
        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_NEW_TASK);
        startActivity(intent);
        finish();
    }
}
