package outletwise.com.twentydresses.view.activity;

import android.os.Bundle;
import android.support.v7.app.ActionBar;
import android.text.Html;
import android.widget.TextView;

import org.json.JSONObject;

import outletwise.com.twentydresses.R;
import outletwise.com.twentydresses.model.ContactUs;
import outletwise.com.twentydresses.utilities.Constants;
import outletwise.com.twentydresses.utilities.Urls;
/**
 * Class is used to display information about company
 */
public class AboutusActivity extends BaseActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_aboutus);
        setUpToolBar(getString(R.string.toolbar_aboutus));
        final ActionBar ab = getSupportActionBar();
        ab.setDisplayHomeAsUpEnabled(true);

        getData();
    }

    private void getData() {
        postJsonData(Urls.ABOUT_US, new JSONObject(), Constants.TAG_ABOUT_US, true);
    }


    @Override
    public <T> void onResponse(T obj, int tag) {
        super.onResponse(obj, tag);
        switch (tag) {
            case Constants.TAG_ABOUT_US:
                ContactUs response = (ContactUs) obj;
                ((TextView) findViewById(R.id.tvDetails)).setText(Html.fromHtml(response.getAboutus()));
                break;

        }
    }

    @Override
    public void onError(int error_code, int tag, Object object) {
        checkErrorCode(error_code, object);
    }
}
