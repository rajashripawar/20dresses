package outletwise.com.twentydresses.view.activity;

import android.app.Notification;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.net.Uri;
import android.os.Bundle;
import android.support.v7.app.ActionBar;
import android.view.KeyEvent;
import android.view.inputmethod.EditorInfo;
import android.widget.EditText;
import android.widget.RemoteViews;
import android.widget.TextView;
import android.widget.Toast;

import java.io.IOException;
import java.net.MalformedURLException;
import java.net.URL;

import outletwise.com.twentydresses.R;

public class SearchActivity extends BaseActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_search);
        setUpToolBar("");
        final ActionBar ab = getSupportActionBar();
        ab.setDisplayHomeAsUpEnabled(true);

        ((EditText) findViewById(R.id.myEditText)).setOnEditorActionListener(new TextView.OnEditorActionListener() {
            @Override
            public boolean onEditorAction(TextView v, int actionId, KeyEvent event) {

                if (actionId == EditorInfo.IME_ACTION_SEARCH) {
                    Toast.makeText(getApplicationContext(), "" + v.getText().toString(), Toast.LENGTH_SHORT).show();
                    //  customNotification();
                    //sendInboxStyleNotification();
                    String name = "http://www.20dresses.com/uploads/emailers/images/600_x_300.jpg";
                    Bitmap bigBitmap = null;
                    URL url_value = null;
                    try {
                        url_value = new URL(name);
                        bigBitmap = BitmapFactory.decodeStream(url_value.openConnection().getInputStream());
                    } catch (MalformedURLException e) {
                        e.printStackTrace();
                    } catch (IOException e) {
                        e.printStackTrace();
                    }


                    if (android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.JELLY_BEAN) {
                        Notification notif = new Notification.Builder(getApplicationContext())
                                .setContentTitle("Title")
                                .setContentText("content")
                                .setSubText("sub text")
                                .setSmallIcon(R.drawable.notification_icon)
                                .setStyle(new Notification.BigPictureStyle()
                                        .bigPicture(bigBitmap)
                                        .setBigContentTitle("big title")
                                        .setSummaryText("aojkxnck ccjlsdnskjkc zlxkclzmc.zx, .zxcmz kdjncvlskdnvdknvldkvnlkvnlskdvn dknflsdknsl,xnkclkcnvlskdnv dknfcdknvdklvnslkdnvsldkvnls,kdvn askflsdkfnslkdnvjfkhndkfjngdflkng summary text end"))
                                .build();

                        NotificationManager mNotificationManager = (NotificationManager) getSystemService(NOTIFICATION_SERVICE);
                        mNotificationManager.notify(1, notif);
                    }
                }
                return false;
            }
        });

    }


    private void customNotification() {

        int icon = R.drawable.ic_launcher;
        long when = System.currentTimeMillis();
        Notification notification = new Notification(icon, "Custom Notification", when);

        Uri uri = Uri.parse("http://www.20dresses.com/uploads/emailers/images/600_x_300.jpg");

        NotificationManager mNotificationManager = (NotificationManager) getSystemService(NOTIFICATION_SERVICE);

        RemoteViews contentView = new RemoteViews(getPackageName(), R.layout.custom_notification);
        contentView.setImageViewResource(R.id.image, R.drawable.ic_launcher);
        contentView.setTextViewText(R.id.title, "Title");
        contentView.setTextViewText(R.id.text, "This is a custom layout.");
        contentView.setTextViewText(R.id.text1, "This is summary text.");
        contentView.setImageViewUri(R.id.bigimage, uri);
        notification.contentView = contentView;

        Intent notificationIntent = new Intent(this, SearchActivity.class);
        PendingIntent contentIntent = PendingIntent.getActivity(this, 0, notificationIntent, 0);
        notification.contentIntent = contentIntent;


        notification.flags |= Notification.FLAG_AUTO_CANCEL; //Do not clear the notification
        notification.defaults |= Notification.DEFAULT_LIGHTS; // LED
        notification.defaults |= Notification.DEFAULT_VIBRATE; //Vibration
        notification.defaults |= Notification.DEFAULT_SOUND; // Sound

        mNotificationManager.notify(1, notification);
    }


    public void sendInboxStyleNotification() {

        String name = "http://www.20dresses.com/uploads/emailers/images/600_x_300.jpg";
        Bitmap bigBitmap = null;
        URL url_value = null;
        try {
            url_value = new URL(name);
            bigBitmap = BitmapFactory.decodeStream(url_value.openConnection().getInputStream());
        } catch (MalformedURLException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }

        Notification.Builder builder = null;
        if (android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.JELLY_BEAN) {
            builder = new Notification.Builder(this)
                    .setContentTitle("IS Notification")
                    .setContentText("Inbox Style notification!!")
                    .setSubText("start msdnskjnfskldj djnfvsdkvnldfknvdflkb dkjfnslkdnflkdngvdf dsknvkfnvkdofewinf dfjksdjfodkjnnvnnvnvnnveiekokk iwejekekkekkejjjnnjeihjknkf iwknfhtshhshhshs djdjkdkkdkdk end")
                    .setSmallIcon(R.drawable.ic_launcher)
                    .setStyle(new Notification.BigPictureStyle()
                            .bigPicture(bigBitmap));
        }

        Notification notification = null;
        if (android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.JELLY_BEAN) {
            notification = new Notification.InboxStyle(builder)
                    .addLine("First message").addLine("Second message")
                    .addLine("Thrid message").addLine("Fourth Message")
                    .setSummaryText("+2 more").build();
        }
        // Put the auto cancel notification flag
        notification.flags |= Notification.FLAG_AUTO_CANCEL;
        NotificationManager notificationManager = getNotificationManager();
        notificationManager.notify(0, notification);
    }


    public NotificationManager getNotificationManager() {
        return (NotificationManager) getSystemService(NOTIFICATION_SERVICE);
    }

}
