package outletwise.com.twentydresses.view.activity;

import android.os.Bundle;
import android.support.v7.app.ActionBar;
import android.text.Html;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.TextView;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

import outletwise.com.twentydresses.R;
import outletwise.com.twentydresses.controller.MyTagHandler;
import outletwise.com.twentydresses.controller.adapter.creditAdapter;
import outletwise.com.twentydresses.model.CreditResponse;
import outletwise.com.twentydresses.utilities.Constants;
import outletwise.com.twentydresses.utilities.Urls;

/**
 * Created by User-PC on 05-11-2015.
 * This class is use to show user's credits with description
 */
public class MyCreditsActivity extends BaseActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_my_credits);
        trackPage(getString(R.string.pgCredit));
        setUpToolBar(getString(R.string.toolbar_credits));
        final ActionBar ab = getSupportActionBar();
        ab.setDisplayHomeAsUpEnabled(true);

    }

    @Override
    public void onCompleteRetrieval(int tag, ArrayList obj, String msg) {
        super.onCompleteRetrieval(tag, obj, msg);
        JSONObject jsonObject = new JSONObject();
        try {
            jsonObject.put(Constants.USER_ID, user.getUser_id());
            jsonObject.put(Constants.AUTH_TOKEN, preferences.getString(Constants.KEY_TOKEN, ""));
        } catch (JSONException e) {
            e.printStackTrace();
        }

        postJsonData(Urls.CREDITS, jsonObject, Constants.TAG_CREDITS, true);
    }

    @Override
    public <T> void onResponse(T obj, int tag) {
        super.onResponse(obj, tag);
        findViewById(R.id.pbWheel).setVisibility(View.GONE);
        findViewById(R.id.rlBalance).setVisibility(View.VISIBLE);

        CreditResponse response = (CreditResponse) obj;
        ((TextView) findViewById(R.id.tvMyCreditsLarge)).setText(Constants.formatAmount(response.getCredits()));

        ((TextView) findViewById(R.id.tvHowToEarnCredits)).setText(Html.fromHtml(String.valueOf(response.getCredit_description()), null, new MyTagHandler()));
        ((TextView) findViewById(R.id.tvHowCreditsWork)).setText(Html.fromHtml(String.valueOf(response.getCredit_work()), null, new MyTagHandler()));

        if (response.getCredits() == 0) {
           /* findViewById(R.id.tvDate).setVisibility(View.GONE);
            findViewById(R.id.tvExpiry).setVisibility(View.GONE);*/
            findViewById(R.id.tvCrediSummary).setVisibility(View.GONE);
            findViewById(R.id.cvCredit).setVisibility(View.GONE);
        } else {
            findViewById(R.id.tvCrediSummary).setVisibility(View.VISIBLE);
            findViewById(R.id.cvCredit).setVisibility(View.VISIBLE);
            ArrayList<CreditResponse.CreditSummaryEntity> list = response.getCredit_summary();
            setUpRecyclerView(list);
        }

    }

    private void setUpRecyclerView(ArrayList<CreditResponse.CreditSummaryEntity> list) {
        LinearLayout llCredit = (LinearLayout) findViewById(R.id.llCredit);
        creditAdapter creditAdapter = new creditAdapter(this, list, llCredit);
      /*  for (int i = 0; i < list.size(); i++) {
            TextView msg = new TextView(this);
            msg.setText("- Your credit " + list.get(i).getCredits() + " will be expired on " + list.get(i).getExpire_date());
            msg.setPadding(10, 10, 10, 10);
            msg.setTextColor(Color.BLACK);
            LinearLayout.LayoutParams params = new LinearLayout.LayoutParams(LinearLayout.LayoutParams.WRAP_CONTENT, LinearLayout.LayoutParams.WRAP_CONTENT);
            params.setMargins(5, 15, 0, 0);
            params.gravity = Gravity.LEFT;
            msg.setLayoutParams(params);
            msg.setGravity(Gravity.CENTER);
            LinearLayout chat = (LinearLayout) findViewById(R.id.llCredit);
            chat.addView(msg);
        }*/

    }

    @Override
    public void onError(int error_code, int tag, Object object) {
        checkErrorCode(error_code, object);
    }
}
