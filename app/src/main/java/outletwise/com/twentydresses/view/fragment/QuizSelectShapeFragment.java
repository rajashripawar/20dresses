package outletwise.com.twentydresses.view.fragment;

import android.content.res.TypedArray;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.text.Html;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TableLayout;
import android.widget.TableRow;
import android.widget.TextView;

import java.util.HashMap;

import outletwise.com.twentydresses.R;
import outletwise.com.twentydresses.controller.interfaces.StyleProfileChanged;
import outletwise.com.twentydresses.model.Registration;
import outletwise.com.twentydresses.model.StyleProfile;
import outletwise.com.twentydresses.model.UserAttrEntity;
import outletwise.com.twentydresses.utilities.Constants;

/**
 * Created by User-PC on 24/07/2015.
 */
public class QuizSelectShapeFragment extends BaseFragment implements View.OnClickListener {

    private View rootView;
    private View previousView;
    private HashMap<String, View> bodyShapes = new HashMap<>();
    private UserAttrEntity userAttr;
    private TextView bSkipQuiz;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        rootView = inflater.inflate(R.layout.fragment_quiz_select_shape, container, false);
        init();

        //For Edit profile
        try {
            userAttr = getArguments().getParcelable(StyleProfile.class.getSimpleName());
        } catch (NullPointerException np) {
            Constants.warning("Style Profile null");
        }
        if (userAttr != null) {
            View view = bodyShapes.get(userAttr.getShape());
            resetViews(view);
        }
        bSkipQuiz = (TextView)rootView. findViewById(R.id.bSkipQuiz);
        bSkipQuiz.setText(Html.fromHtml(getString(R.string.button_skip)));
//        bSkipQuiz = (Button)rootView. findViewById(R.id.bSkipQuiz);
        if (Constants.EDITPROFILE ||Constants.TAKE_QUIZ){
            bSkipQuiz.setVisibility(View.GONE);
        }

        bSkipQuiz.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Constants.SKIPQUIZ = true;
                addFragment(R.id.frame_container, new QuizEnterUserDetails(), QuizEnterUserDetails.class.getSimpleName());
            }
        });
        return rootView;
    }

    private void init() {
        TypedArray bodyShapeImages = mActivity.getResources().obtainTypedArray(R.array.body_shape_images);
        String[] bodyShapeText = mActivity.getResources().getStringArray(R.array.body_shape_text);
        String[] bodyShapeTag = mActivity.getResources().getStringArray(R.array.body_shape_tag);

        TableLayout tablelayout = (TableLayout) rootView.findViewById(R.id.tlBodyShapes);
        int k = 0;
        for (int i = 0; i < tablelayout.getChildCount(); i++) {
            TableRow row = (TableRow) tablelayout.getChildAt(i);
            for (int j = 0; j < row.getChildCount(); j++) {
                View v = row.getChildAt(j);
                ((TextView) v.findViewById(R.id.tvBodyShape)).setText(bodyShapeText[k]);
                ((ImageView) v.findViewById(R.id.ivBodyShape)).setImageDrawable(bodyShapeImages.getDrawable(k));
                v.findViewById(R.id.ivBodyShape).setOnClickListener(this);
                v.setTag(bodyShapeTag[k]);
                bodyShapes.put(bodyShapeTag[k], v);
                k++;
            }
        }
        bodyShapeImages.recycle();
    }

    @Override
    public void onResume() {
        super.onResume();
        TextView textView = ((TextView) getToolBar().findViewById(R.id.tvStep));
        if (textView != null)
            textView.setText("3 of 5");
    }

    @Override
    public void onClick(View view) {
        rootView.findViewById(R.id.pbWheel).setVisibility(View.VISIBLE);
        view = (View) view.getParent();
        resetViews(view);
        if (userAttr != null) {
            userAttr.setShape((String) view.getTag());
            // if(Constants.QUIZ.equalsIgnoreCase(Constants.REGISTER))
            ((StyleProfileChanged) mActivity).onChange(userAttr);
           /* else
            {
                Quiz.getInstance().getUser_attr().setShape((String) view.getTag());
                addFragment(R.id.frame_container, new QuizSelectStyleFragment(),
                        QuizSelectStyleFragment.class.getSimpleName());
            }*/
        } else {
            Registration.getInstance().getQuiz().getUser_attr().setShape((String) view.getTag());
            addFragment(R.id.frame_container, new QuizSelectStyleFragment(),
                    QuizSelectStyleFragment.class.getSimpleName());
        }
    }

    void resetViews(View v) {
        if (v != null) {
            if (previousView != null) {
                previousView.findViewById(R.id.ivTick).setVisibility(View.INVISIBLE);
                previousView.findViewById(R.id.tvBodyShape).setSelected(false);
                previousView.findViewById(R.id.ivBodyShape).setSelected(false);
            }
            previousView = v;
            v.findViewById(R.id.ivTick).setVisibility(View.VISIBLE);
            v.findViewById(R.id.tvBodyShape).setSelected(true);
            v.findViewById(R.id.ivBodyShape).setSelected(true);
        }
    }
}
