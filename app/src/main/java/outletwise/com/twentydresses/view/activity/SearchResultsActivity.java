package outletwise.com.twentydresses.view.activity;

import android.app.SearchManager;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.View;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;

import outletwise.com.twentydresses.R;
import outletwise.com.twentydresses.model.Product;
import outletwise.com.twentydresses.model.SubTip;
import outletwise.com.twentydresses.utilities.Constants;
import outletwise.com.twentydresses.utilities.Urls;

public class SearchResultsActivity extends BaseActivity {

    private ArrayList<Product> products = new ArrayList<>();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_search_results);

        setUpToolBar("Search Result");
        final ActionBar ab = getSupportActionBar();
        ab.setDisplayHomeAsUpEnabled(true);

        handleIntent(getIntent());
    }

    private void handleIntent(Intent intent) {

        if (Intent.ACTION_SEARCH.equals(intent.getAction())) {
            String query = intent.getStringExtra(SearchManager.QUERY);
            Constants.debug("Search ::" + query);

            HashMap<String, String> params = new HashMap<>();
            params.put("keyword", query);
            postStringData(Urls.SEARCH, params, Constants.TAG_SEARCH, false);

        }
    }

    @Override
    protected void onNewIntent(Intent intent) {
        super.onNewIntent(intent);
        handleIntent(intent);
    }

    @Override
    public <T> void onResponse(T obj, int tag) {
        super.onResponse(obj, tag);
        switch (tag) {
            case Constants.TAG_PRODUCTS:
                findViewById(R.id.pbWheel).setVisibility(View.GONE);
                //  productAdapter.hideProgressBar();
               /* if (page == 1 && blnProductState) {
                    findViewById(R.id.progressbar).setVisibility(View.VISIBLE);
                    disableTouch();
                }*/

                if (obj instanceof String && ((String) obj).equalsIgnoreCase(Constants.NO_MORE_RESULTS)) {
                    showToast(obj.toString());
                }/*else if (obj instanceof SubTip) {
                    products.addAll(((SubTip) obj).getProducts());
                }*/

                else if (obj instanceof Product[]) {
                    products.addAll((Arrays.asList((Product[]) obj)));
                    //  productAdapter.notifyDataSetChanged();
                    // isLoading = false;
                }
                // mSwipeRefreshLayout.setRefreshing(false);
                break;
        }
    }
}
