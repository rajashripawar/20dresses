package outletwise.com.twentydresses.view.activity;

import android.Manifest;
import android.accounts.Account;
import android.accounts.AccountManager;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.Typeface;
import android.os.Build;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.support.v4.app.ActivityCompat;
import android.text.Editable;
import android.text.InputType;
import android.text.TextWatcher;
import android.util.Log;
import android.view.View;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.ImageView;
import android.widget.TextView;

import com.squareup.picasso.Picasso;
import com.sromku.simple.fb.entities.Profile;

import java.util.Random;

import outletwise.com.twentydresses.R;
import outletwise.com.twentydresses.controller.AnalyticsTracker;
import outletwise.com.twentydresses.model.BackgroundImgResponse;
import outletwise.com.twentydresses.model.Login;
import outletwise.com.twentydresses.model.RegisterResponse;
import outletwise.com.twentydresses.model.Registration;
import outletwise.com.twentydresses.model.database.greenbot.User;
import outletwise.com.twentydresses.utilities.Constants;
import outletwise.com.twentydresses.utilities.Urls;

/**
 * This class is use to register user via email or facebook
 */
public class RegistrationActivity2 extends BaseActivity {
    private TextView tv_email;
    private TextView tv_password;
    private TextView tv_phNo;
    private boolean isFromFacebook;
    private CheckBox checkBox;
    private String fbGender = null;
    private String fbAccessToken;
    private String strPasswordForFb;
    //  private String user_device_id;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_register2);
        //setBackgroundImage(Constants.IMG_SIGNIN);
        initFacebookConfig();
        init();

        setFacebookCallbacks(new FacebookCallbacks() {
            @Override
            public void onLoginSuccess(String accessToken) {
                getFacebookProfile();
                fbAccessToken = accessToken;
            }

            @Override
            public void onGetProfile(Profile profile) {

                Login login = new Login();
                login.setPassword(generatePassword());

                User user = new User();
                user.setUser_mobile("");
                user.setUser_first_name(profile.getFirstName());
                user.setUser_last_name(profile.getLastName());

                try {
                    // Change b'date format Y-M-D
                    String bdate = profile.getBirthday();
                    String[] arr = bdate.split("/");
                    String month = arr[0];
                    String date = arr[1];
                    String year = arr[2];
                    user.setDob(year + "-" + month + "-" + date);
                } catch (NullPointerException ex) {
                    ex.printStackTrace();
                } catch (ArrayIndexOutOfBoundsException ex) {
                    ex.printStackTrace();
                }

                user.setUser_picture(profile.getPicture());
                user.setFacebook_id(profile.getId());

                if (profile.getGender() != null) {
                    fbGender = profile.getGender();
                    user.setGender(profile.getGender());
                }

                try {
                    user.setUser_email(profile.getEmail());
                    user.setFacebook_email(profile.getEmail());
                    login.setEmail(profile.getEmail());
                } catch (NullPointerException ex) {
                    user.setUser_email(profile.getId() + "@facebook.com");
                    user.setFacebook_email(profile.getId() + "@facebook.com");
                    login.setEmail(profile.getId() + "@facebook.com");
                }
                Registration registration = Registration.getInstance();
                registration.setUser(user);
                registration.setLogin(login);
                registration.setFacebookId(profile.getId());
                registration.setFacebook_access_token(fbAccessToken);
                isFromFacebook = true;

                registration.setUser_device_id(getDeviceId());
                registration.setCurrent_app_version(getCurrentAppVersion());
                postJsonData(Urls.REGISTRATION_USER, registration, Constants.TAG_REGISTRATION, true);
            }

            @Override
            public void onLogoutSuccess() {

            }
        });


        findViewById(R.id.bRegister).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (validateFields()) {
                    Login login = new Login();
                    login.setEmail(tv_email.getText().toString());
                    login.setPassword(tv_password.getText().toString());

                    Registration registration = Registration.getInstance();
                    User user = new User();
                    user.setUser_mobile(tv_phNo.getText().toString().trim());
                    registration.setUser(user);
                    registration.setLogin(login);
                    login.setUser(user);
                    login.setUser_device_id(getDeviceId());
                    login.setCurrent_app_version(getCurrentAppVersion());
                    // postJsonData(Urls.VISITOR, login, Constants.TAG_VISITOR, true);
                    postJsonData(Urls.REGISTRATION_USER, login, Constants.TAG_REGISTRATION, true);
                }
            }
        });

        findViewById(R.id.tvSignIn).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(getApplicationContext(), LoginActivity.class));
                finish();
            }
        });

        //Facebook Login
        findViewById(R.id.bFbLogin).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                loginWithFacebook();
            }
        });

        ((CheckBox) findViewById(R.id.checkBox)).setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                if (tv_password.getText().toString().trim().equalsIgnoreCase(""))
                    return;

                if (isChecked) {
                    tv_password.setInputType(InputType.TYPE_TEXT_VARIATION_PASSWORD);
                } else {
                    tv_password.setInputType(129);
                }
            }
        });
    }

    private String generatePassword() {
        Random rnd = new Random();
        int n = 100000 + rnd.nextInt(900000);
        strPasswordForFb = String.valueOf(n);

        return strPasswordForFb;
    }

    private void init() {
        tv_email = (TextView) findViewById(R.id.etEmail);
        tv_phNo = (TextView) findViewById(R.id.etMobileNo);
        tv_password = (TextView) findViewById(R.id.etPassword);
        tv_password.setTypeface(Typeface.DEFAULT);
        checkBox = (CheckBox) findViewById(R.id.checkBox);

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            int hasReadStoragePermission = checkSelfPermission(Manifest.permission.GET_ACCOUNTS);
            if (hasReadStoragePermission != PackageManager.PERMISSION_GRANTED) {
                requestPermissions(new String[]{Manifest.permission.GET_ACCOUNTS}, 1);
                return;
            } else
                tv_email.setText(getMailId());
        } else
            tv_email.setText(getMailId());

        tv_password.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
            }

            @Override
            public void afterTextChanged(Editable s) {


                if (tv_password.getText().length() > 0)
                    checkBox.setEnabled(true);
                else
                    checkBox.setEnabled(false);
            }
        });
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, String[] permissions, int[] grantResults) {

        if (grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
            // Permission Granted
            tv_email.setText(getMailId());
        } else
            showToast(getString(R.string.text_permission_denied));
    }

    public String getMailId() {
        String strGmail = "";
        try {
            if (ActivityCompat.checkSelfPermission(this, Manifest.permission.GET_ACCOUNTS) != PackageManager.PERMISSION_GRANTED)
            {
                Account[] accounts = AccountManager.get(this).getAccounts();
                for (Account account : accounts) {
                    String possibleEmail = account.name;
                    String type = account.type;

                    if (type.equals("com.google")) {
                        strGmail = possibleEmail;
                        Log.e("PIKLOG", "Emails: " + strGmail);
                        break;
                    }
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        }

        return strGmail;
    }

    @Override
    public <T> void onResponse(T obj, int tag) {
        super.onResponse(obj, tag);
        switch (tag) {
            case Constants.TAG_IMG_BACKGROUND:
                findViewById(R.id.pbWheel).setVisibility(View.GONE);
                BackgroundImgResponse imgResponse = (BackgroundImgResponse) obj;
                if (imgResponse.getStatus().equalsIgnoreCase(Constants.RESPONSE_SUCCESS)) {
                    String imgUrl = imgResponse.getImage();
                    ImageView iv_background = (ImageView) findViewById(R.id.iv_background);
                    Picasso.with(getApplicationContext()).load(imgUrl).into(iv_background);
                    toggleVisibility();
                }

            case Constants.TAG_REGISTRATION:
                RegisterResponse registerResponse = (RegisterResponse) obj;
                if (registerResponse.getStatus().equalsIgnoreCase(Constants.RESPONSE_SUCCESS)) {
                    User user = Registration.getInstance().getUser();
                    user.setUser_id(registerResponse.getUser());

                    if (isFromFacebook) {
                        trackPage(getString(R.string.pgFbRegistrations));
                        AnalyticsTracker.getInstance().trackSignUp(Registration.getInstance().getUser(), "Facebook");
                        user.setUser_email("");
                    } else {
                        trackPage(getString(R.string.pgRegistrations));
                        AnalyticsTracker.getInstance().trackSignUp(Registration.getInstance().getUser(), "Email");
                        user.setUser_email(tv_email.getText().toString());
                    }
                    PreferenceManager.getDefaultSharedPreferences(getApplicationContext()).edit().remove(Constants.KEY_TOKEN).commit();
                    PreferenceManager.getDefaultSharedPreferences(getApplicationContext()).edit().putString(Constants.KEY_TOKEN, registerResponse.getToken()).apply();
                    Registration.getInstance().setUser(user);
                    deleteAllRecords(Constants.TAG_SHORTLIST_ALL);
                    deleteAllRecords(Constants.TAG_USER);
                    insertRecord(user, Constants.TAG_USER);
                } else if (registerResponse.getEmail_address() != null && registerResponse.getEmail_address().equalsIgnoreCase(Constants.VALID)) {
                    finish();
                    showToast(registerResponse.getMessage());
                    startActivity(new Intent(getApplicationContext(), LoginActivity.class));
                } else
                    showToast(registerResponse.getMessage());
                break;
        }
    }

    private void toggleVisibility() {
        findViewById(R.id.textView1).setVisibility(View.VISIBLE);
        findViewById(R.id.bFbLogin).setVisibility(View.VISIBLE);
        findViewById(R.id.textView2).setVisibility(View.VISIBLE);
        findViewById(R.id.rlFields).setVisibility(View.VISIBLE);
    }

    @Override
    public void onCompleteInsertion(int tag, String msg) {
        super.onCompleteInsertion(tag, msg);
        //selectRecords(Constants.TAG_USER);
        switch (tag) {
            case Constants.TAG_USER:
                if (isFromFacebook && fbGender != null && !fbGender.equalsIgnoreCase("")) {
                    if (fbGender.equalsIgnoreCase("female"))
                        Registration.getInstance().getUser().setGender("F");
                    else if (fbGender.equalsIgnoreCase("male"))
                    Registration.getInstance().getUser().setGender("M");
                    Intent intent = new Intent(getApplicationContext(), QuizActivity.class);
                    startActivity(intent);
                } else
                    startActivity(new Intent(getApplicationContext(),SelectGenderActivity.class));
                break;
        }
    }

  /*  @Override
    public void onCompleteRetrieval(int tag, ArrayList obj, String msg) {
        switch (tag) {
            case Constants.TAG_USER:
                if (obj.size() != 0) {
                    User u = (User) obj.get(0);
                   *//* if (isFromFacebook && fbGender != null && !fbGender.equalsIgnoreCase("")) {
                        if (fbGender.equalsIgnoreCase("female"))
                            Registration.getInstance().getUser().setGender("F");
                        else if (fbGender.equalsIgnoreCase("male"))
                            Registration.getInstance().getUser().setGender("M");

                        Intent intent = new Intent(getApplicationContext(), QuizActivity.class);
                        startActivity(intent);
                    } else
                        startActivity(new Intent(getApplicationContext(), SelectGenderActivity.class));*//*
                }
                break;
        }
    }*/

    boolean validateFields() {
        if (!Constants.isValidEmail(tv_email.getText().toString().trim())) {
            showToast(getString(R.string.invalid_email));
            tv_email.requestFocus();
            return false;
        } else if (tv_password.getText().toString().trim().equalsIgnoreCase("")) {
            showToast(getString(R.string.invalid_passwd));
            tv_password.requestFocus();
            return false;
        } else if (tv_phNo.getText().toString().trim().equalsIgnoreCase("") || tv_phNo.getText().length() != 10) {
            showToast(getString(R.string.invalid_mob));
            tv_phNo.requestFocus();
            return false;
        }
        return true;
    }


    @Override
    public void onError(int error_code, int tag, Object object) {
        checkErrorCode(error_code, object);
        findViewById(R.id.pbWheel).setVisibility(View.GONE);
        enableTouch();
    }
}
