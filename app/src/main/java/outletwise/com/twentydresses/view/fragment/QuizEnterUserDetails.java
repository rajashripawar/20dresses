package outletwise.com.twentydresses.view.fragment;

import android.app.Activity;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.support.annotation.Nullable;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.TypedValue;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.widget.EditText;
import android.widget.TextView;

import com.wdullaer.materialdatetimepicker.date.DatePickerDialog;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.File;
import java.util.Calendar;
import java.util.GregorianCalendar;

import outletwise.com.twentydresses.R;
import outletwise.com.twentydresses.controller.AnalyticsTracker;
import outletwise.com.twentydresses.model.RegisterResponse;
import outletwise.com.twentydresses.model.Registration;
import outletwise.com.twentydresses.model.VisitorResponse;
import outletwise.com.twentydresses.model.database.greenbot.User;
import outletwise.com.twentydresses.utilities.Constants;
import outletwise.com.twentydresses.utilities.Urls;
import outletwise.com.twentydresses.view.activity.BaseActivity;
import outletwise.com.twentydresses.view.activity.HomeActivity;

/**
 * Created by User-PC on 24/07/2015.
 */
public class QuizEnterUserDetails extends BaseFragment implements DatePickerDialog.OnDateSetListener {
    private static final int RESULT_LOAD_IMAGE = 1;
    private static int REQUEST_CROP_PICTURE = 2;
    private View rootView;
    private TextView tvName;
    private EditText edtMobNo, edtRefCode;
    private File croppedImageFile;
    private boolean isDateSet;
    private User user;
    private String strImgName;
    private RegisterResponse response;
    private Registration registration;
    public SharedPreferences preferences;
    private String referCode;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        rootView = inflater.inflate(R.layout.fragment_quiz_enter_user_detials, container, false);

        getActivity().getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_ADJUST_PAN);
        tvName = (TextView) rootView.findViewById(R.id.etName);
        edtMobNo = (EditText) rootView.findViewById(R.id.etMobileNo);
        edtRefCode = (EditText) rootView.findViewById(R.id.etRefCode);

        user = Registration.getInstance().getUser();

        if (user != null && !user.getUser_mobile().equalsIgnoreCase("")) {
            rootView.findViewById(R.id.llMobNo).setVisibility(View.GONE);
            edtMobNo.setVisibility(View.GONE);
            edtMobNo.setText(user.getUser_mobile());
        } else {
            rootView.findViewById(R.id.llMobNo).setVisibility(View.VISIBLE);
            edtMobNo.setVisibility(View.VISIBLE);
        }

        edtRefCode.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {
                referCode = edtRefCode.getText().toString();
                if (referCode.length() == 6)
                    checkValidityOfReferCode(referCode);
                else
                    ((TextView) rootView.findViewById(R.id.tvMsg)).setVisibility(View.GONE);
            }

            @Override
            public void afterTextChanged(Editable editable) {

            }
        });

      /*  ivProfile = (ImageView) rootView.findViewById(R.id.ivProfile);
        ivProfile.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                    int hasReadStoragePermission = mActivity.checkSelfPermission(Manifest.permission.READ_EXTERNAL_STORAGE);
                    if (hasReadStoragePermission != PackageManager.PERMISSION_GRANTED) {
                        requestPermissions(new String[]{Manifest.permission.READ_EXTERNAL_STORAGE}, 1);
                        return;
                    } else
                        cropImage();
                } else
                    cropImage();

            }
        });*/


        rootView.findViewById(R.id.LlDate).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Calendar calendar = new GregorianCalendar(1980, 0, 1);
                DatePickerDialog dpd = DatePickerDialog.newInstance(
                        QuizEnterUserDetails.this,
                        calendar.get(Calendar.YEAR),
                        calendar.get(Calendar.MONTH),
                        calendar.get(Calendar.DAY_OF_MONTH)
                );
                Calendar calendarInst = Calendar.getInstance();
                int year = calendarInst.get(Calendar.YEAR);
                int month = calendarInst.get(Calendar.MONTH);
                int date = calendarInst.get(Calendar.DATE);
                Calendar cMaxDate = new GregorianCalendar(year, month, date);

                dpd.setMaxDate(cMaxDate);
                dpd.showYearPickerFirst(true);
                dpd.show(mActivity.getFragmentManager(), "DatePickerDialog");
            }
        });

        rootView.findViewById(R.id.bConfirm).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                if (validateFields()) {
                    rootView.findViewById(R.id.pbWheelReg).setVisibility(View.VISIBLE);
                  /*  if (!edtRefCode.getText().equals("") && edtRefCode.getText().length() > 0)
                        checkValidityOfReferCode(edtRefCode.getText().toString());*/

                    sendQuizDetails();
                    showToast("Just a sec!");
                }
            }
        });

        initForFacebook();
        return rootView;
    }

    private void checkValidityOfReferCode(String strReferCode) {

        JSONObject jsonObject = new JSONObject();
        try {
            jsonObject.put("refercode", strReferCode);
        } catch (JSONException e) {
            e.printStackTrace();
        }
        postJsonData(Urls.CHK_REF_CODE, jsonObject, Constants.TAG_CHK_REF_CODE, false);
    }


    @Override
    public void onRequestPermissionsResult(int requestCode, String[] permissions, int[] grantResults) {
        // super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        if (grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
            // Permission Granted
            cropImage();
        } else
            showToast(getString(R.string.text_permission_denied));
    }

    private void cropImage() {

        strImgName = "test_" + Calendar.getInstance().getTimeInMillis() + ".jpg";
        croppedImageFile = new File(mActivity.getCacheDir(), "test_" + Calendar.getInstance().getTimeInMillis() + ".jpg");
        Intent i = new Intent(
                Intent.ACTION_PICK, android.provider.MediaStore.Images.Media.EXTERNAL_CONTENT_URI);
        startActivityForResult(i, RESULT_LOAD_IMAGE);
    }

    private void sendQuizDetails() {

        rootView.findViewById(R.id.pbWheelReg).setVisibility(View.VISIBLE);

        String[] name = tvName.getText().toString().trim().split(" ");
        registration = Registration.getInstance();
        registration.getUser().setUser_first_name(name[0]);
        if (name.length > 1)
            registration.getUser().setUser_last_name(name[1]);
        else
            registration.getUser().setUser_last_name("");

        registration.setReferCode(edtRefCode.getText().toString());

        //To check if reg from fb
        if (registration.getUser().getFacebook_id() != null)
            registration.getUser().setUser_picture("");

        //  postJsonData(Urls.REGISTRATION, registration, Constants.TAG_REGISTRATION, true);
        preferences = PreferenceManager.getDefaultSharedPreferences(mActivity);
        registration.setToken(preferences.getString(Constants.KEY_TOKEN, ""));


        registration.setUser_id(registration.getUser().getUser_id());

        if (registration != null && String.valueOf(registration.getUser().getUser_id()) != null){
            if (!String.valueOf(user.getUser_id()).trim().isEmpty()){
                registration.setUser_id(registration.getUser().getUser_id());
            }
        }

        registration.setCurrent_app_version(((BaseActivity) mActivity).getCurrentAppVersion());
        registration.getUser().setUser_mobile(edtMobNo.getText().toString());
        postJsonData(Urls.ADD_QUIZ, registration, Constants.TAG_ADD_QUIZ, true);
    }


    void initForFacebook() {
        try {
            String name = user.getUser_first_name() == null ? "" : user.getUser_first_name();
            name += user.getUser_last_name() == null ? "" : " " + user.getUser_last_name();
            tvName.setText(name);
        } catch (NullPointerException ex) {
            ex.printStackTrace();
        }

       /* if (user.getUser_picture() != null && !user.getUser_picture().equalsIgnoreCase(""))
            Picasso.with(mActivity).load(user.getUser_picture()).into(ivProfile);*/

        try {
            if (user != null && user.getDob() != null) {
                try {
                    String date[] = user.getDob().split("-");
                    setDate(date[2], date[1], date[0]);
                } catch (ArrayIndexOutOfBoundsException ex) {
                    ex.printStackTrace();
                    setDate("00", "00", "0000");
                }
            }
        } catch (NullPointerException ex) {
            ex.printStackTrace();
            setDate("00", "00", "0000");
        }
    }

    void setDate(String dayOfMonth, String monthOfYear, String year) {

        ((TextView) rootView.findViewById(R.id.tvDateDay)).setText(dayOfMonth);
        ((TextView) rootView.findViewById(R.id.tvDateMonth)).setText(monthOfYear);
        ((TextView) rootView.findViewById(R.id.tvDateYear)).setText(year);
        isDateSet = true;
    }


    @Override
    public void onResume() {
        super.onResume();
        if (getToolBar() != null) {
            getToolBar().setTitle("");
            ((TextView) getToolBar().findViewById(R.id.tvStep)).setTextSize(TypedValue.COMPLEX_UNIT_SP, 20);
            ((TextView) getToolBar().findViewById(R.id.tvStep)).setText("One last thing");
        }
    }

    @Override
    public <T> void onResponse(T obj, int tag) {
        switch (tag) {
            case Constants.TAG_CHK_REF_CODE:
                   rootView.findViewById(R.id.pbWheelReg).setVisibility(View.GONE);
                VisitorResponse refResponse = (VisitorResponse) obj;
                if (refResponse.getStatus().equalsIgnoreCase(Constants.RESPONSE_SUCCESS))
                    showToast(refResponse.getMessage());
                else {
                    ((TextView) rootView.findViewById(R.id.tvMsg)).setVisibility(View.VISIBLE);
                    ((TextView) rootView.findViewById(R.id.tvMsg)).setText(refResponse.getMessage());
                    showToast(refResponse.getMessage());
                }
                //    rootView.findViewById(R.id.pbWheelReg).setVisibility(View.GONE);
                break;

            case Constants.TAG_ADD_QUIZ:
                  rootView.findViewById(R.id.pbWheelReg).setVisibility(View.GONE);
                VisitorResponse addquizResponse = (VisitorResponse) obj;
                if (addquizResponse.getStatus().equalsIgnoreCase(Constants.RESPONSE_SUCCESS)) {
                    Constants.EDITPROFILE = true;
                    if (addquizResponse.getUser_registered() != null)
                        user.setUser_registered(addquizResponse.getUser_registered());
                    else
                    /* if sarika change code*/
                        user.setUser_registered("Yes");
                    user.setUser_mobile(edtMobNo.getText().toString());
                    insertRecord(user, Constants.TAG_USER);
                    AnalyticsTracker.getInstance().trackQuiz(user);

                    /*
                    * Send to OTP page only if refer code is valid
                    * */

                   /* if (addquizResponse.getRefer_code().equalsIgnoreCase(Constants.VALID))
                        startActivity(new Intent(mActivity, MobileVerifyActivity.class));
                    else*/
                        goToNextActivity();

                } else {
                    ((TextView) rootView.findViewById(R.id.tvMsg)).setVisibility(View.VISIBLE);
                    ((TextView) rootView.findViewById(R.id.tvMsg)).setText(addquizResponse.getMessage());
                    showToast(addquizResponse.getMessage());
                }
                break;
            default:
        }

    }


    private void goToNextActivity() {
        //Constants.debug("Gender : " + Registration.getInstance().getUser().getGender());
        Intent intent = new Intent(mActivity, HomeActivity.class);
        if (user.getGender() != null && user.getGender().equalsIgnoreCase("M")) {
            intent.putExtra(Constants.CLEAR_ACTIVITIES, false);
            startActivity(intent);
        }
        else {
            if (user.getGender() != null && user.getGender().equalsIgnoreCase("F")){
                if (Constants.SKIPQUIZ){
                    intent.putExtra(Constants.CLEAR_ACTIVITIES, false);
                    startActivity(intent);
                }else {
                    intent.putExtra(Constants.CLEAR_ACTIVITIES, true);
                    intent.putExtra(Constants.START_PROFILE_ACTIVITY, true);
                    startActivity(intent);
                }
            }

        }
    }

    @Override
    public void onCompleteInsertion(int tag, String msg) {
        super.onCompleteInsertion(tag, msg);
        // goToNextActivity();
    }


    @Override
    public void onDateSet(DatePickerDialog datePickerDialog, int i, int i1, int i2) {
        isDateSet = true;
        ((TextView) rootView.findViewById(R.id.tvDateDay)).setText(i + "");
        ((TextView) rootView.findViewById(R.id.tvDateMonth)).setText(++i1 + "");
        ((TextView) rootView.findViewById(R.id.tvDateYear)).setText(i2 + "");
        if (user != null)
            user.setDob(i + "-" + i1 + "-" + i2);
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        if (requestCode == RESULT_LOAD_IMAGE && resultCode == Activity.RESULT_OK && null != data) {

            /*Uri croppedImage = Uri.fromFile(croppedImageFile);
            CropImageIntentBuilder cropImage = new CropImageIntentBuilder(ivProfile.getWidth(), ivProfile.getWidth(), croppedImage);
            cropImage.setCircleCrop(true);
            cropImage.setOutlineCircleColor(mActivity.getResources().getColor(R.color.color_accent));
            cropImage.setSourceImage(data.getData());

            startActivityForResult(cropImage.getIntent(mActivity), REQUEST_CROP_PICTURE);*/

        } else if ((requestCode == REQUEST_CROP_PICTURE) && (resultCode == Activity.RESULT_OK)) {
            /*BitmapFactory.Options options = new BitmapFactory.Options();
            options.inJustDecodeBounds = true;
            BitmapFactory.decodeFile(croppedImageFile.getPath(), options);
            int width = options.outWidth;
            int height = options.outHeight;
            ((BaseActivity) mActivity).uploadFileToFtp(croppedImageFile, width, height);

            Registration.getInstance().getUser().setUser_picture(strImgName);
            // Registration.getInstance().getUser().setUser_picture(Uri.fromFile(croppedImageFile).toString());
            Picasso.with(mActivity).load(Uri.fromFile(croppedImageFile).toString())
                    .resize(ivProfile.getWidth(), ivProfile.getHeight())
                    .into(ivProfile);*/
        }

    }

    private boolean validateFields() {
        if (tvName.getText().toString().trim().equalsIgnoreCase("") || tvName.getText().toString().trim().length() < 0) {
            showToast(getString(R.string.enter_name));
            tvName.requestFocus();
            return false;
        } else if (!isDateSet) {
            showToast(getString(R.string.text_dob));
            return false;
        } else if (edtMobNo.getText().toString().trim().equalsIgnoreCase("")) {
            showToast(getString(R.string.enter_mobno));
            edtMobNo.requestFocus();
            return false;
        }
        return true;
    }

    @Override
    public void onError(int error_code, int tag, Object object) {
        ((BaseActivity) mActivity).checkErrorCode(error_code, object);
    }
}
