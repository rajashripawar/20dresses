package outletwise.com.twentydresses.view.fragment;

import android.app.Dialog;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v4.app.DialogFragment;
import android.widget.DatePicker;

import java.util.Calendar;

import outletwise.com.twentydresses.view.custom.CustomDatePicker;

/**
 * Created by User-PC on 19-11-2015.
 */
public class CardExpiryPickerFragment extends DialogFragment
        implements CustomDatePicker.OnDateSetListener {

    private DatePicked datePicked;

    @NonNull
    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {
        final Calendar c = Calendar.getInstance();
        int year = c.get(Calendar.YEAR);
        int month = c.get(Calendar.MONTH);

        return new CustomDatePicker(getActivity(), this, year, month);
    }

    public void onDateSet(DatePicker view, int year, int month, int day) {
        if (datePicked != null)
            datePicked.onDateSet(view, year, month);
    }

    public void setDatePicked(DatePicked datePicked) {
        this.datePicked = datePicked;
    }

    public interface DatePicked {
        void onDateSet(DatePicker view, int year, int month);
    }
}