package outletwise.com.twentydresses.view.fragment;

import android.graphics.Typeface;
import android.os.Build;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.widget.CardView;
import android.text.TextUtils;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.LinearLayout;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.Spinner;
import android.widget.TextView;

import com.andexert.expandablelayout.library.ExpandableLayout;

import info.hoang8f.android.segmented.SegmentedGroup;
import outletwise.com.twentydresses.R;
import outletwise.com.twentydresses.controller.adapter.BankItemsAdapter;
import outletwise.com.twentydresses.model.OrderReturnResponse;
import outletwise.com.twentydresses.model.ReturnExchangeRequest;

/**
 * Created by User-PC on 04-11-2015.
 */
public class OrderRefundFragment extends BaseFragment {

    private View rootView;
    private CardView cvWallet, cvoption,cvBank;
    private ExpandableLayout elWallet,elcvoption, elBank;
    private OrderReturnResponse orderReturnResponse;
    private ReturnExchangeRequest returnExchangeRequest;
    private SegmentedGroup sgBankDetail;
    private TextView tvBankName, tvAccHolderName, tvAccNo, tvBranchName, tvIFSCCode;
    private Spinner spAccType;
    private LinearLayout llNewBankDetails;
    private CheckBox cbWallet,cbcvoption, cbBank;
    private int checkedId;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        rootView = inflater.inflate(R.layout.fragment_order_refund, container, false);
        init();
        return rootView;
    }

    @Override
    public void onResume() {
        super.onResume();
        getToolBar().setTitle(getString(R.string.toolbar_refund_mode));
    }

    void init() {
        orderReturnResponse = getArguments().getParcelable(OrderReturnResponse.class.getSimpleName());
        returnExchangeRequest = getArguments().getParcelable(ReturnExchangeRequest.class.getSimpleName());
        final ReturnExchangeRequest.RefundEntity refundEntity = new ReturnExchangeRequest.RefundEntity();
        refundEntity.setWallet(true);
        returnExchangeRequest.setRefund(refundEntity);

        cvWallet = (CardView) rootView.findViewById(R.id.cvWallet);
        cvoption = (CardView) rootView.findViewById(R.id.cvoption);
        cvBank = (CardView) rootView.findViewById(R.id.cvBank);
        elWallet = (ExpandableLayout) cvWallet.findViewById(R.id.el20DWallet);
        elcvoption = (ExpandableLayout) cvoption.findViewById(R.id.elcvoption);
        elBank = (ExpandableLayout) cvBank.findViewById(R.id.elBankAccount);

        if (orderReturnResponse != null)
            ((TextView) elWallet.getContentLayout().findViewById(R.id.tvExpandableContent)).
                    setText(orderReturnResponse.getRefund().get(0).getWallet().replace("{amount}",
                            returnExchangeRequest.getRefundAmount()));
        elWallet.show();


        sgBankDetail = (SegmentedGroup) elBank.findViewById(R.id.sgBank);
        sgBankDetail.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(RadioGroup group, int checkedId) {
                toggleVisibility(checkedId);
            }
        });

        cbWallet = (CheckBox) elWallet.findViewById(R.id.checkBox);
        cbWallet.setChecked(true);
        cbcvoption = (CheckBox) elcvoption.findViewById(R.id.checkBox);

        if (orderReturnResponse.getRefund().get(0).isCredit() == true){
            Log.e("Credit Description---------->",""+orderReturnResponse.getRefund().get(0).getCredit_description());

            ((TextView) elcvoption.getHeaderLayout().findViewById(R.id.tvExpandableHeader)).setText(getString(R.string.text_credit_wallet));
            ((TextView) elcvoption.getContentLayout().findViewById(R.id.tvExpandableContent)).
                    setText(orderReturnResponse.getRefund().get(0).getCredit_description());
           /* ((TextView) elcvoption.getHeaderLayout().findViewById(R.id.tvExpandableHeader)).setTextSize(12);
            ((TextView) elcvoption.getHeaderLayout().findViewById(R.id.tvExpandableHeader)).setText(orderReturnResponse.getRefund().get(0).getCredit_description());
            if (Build.VERSION.SDK_INT < 23) {
                ((TextView) elcvoption.getHeaderLayout().findViewById(R.id.tvExpandableHeader)).setTextAppearance(getActivity(), R.style.TextCustomStyleSecondary);
            } else {
                ((TextView) elcvoption.getHeaderLayout().findViewById(R.id.tvExpandableHeader)).setTextAppearance(R.style.TextCustomStyleSecondary);
            }*/
        }else{
            elcvoption.hide();
        }
        cbBank = (CheckBox) elBank.findViewById(R.id.checkBox);
        ((TextView) elBank.getHeaderLayout().findViewById(R.id.tvExpandableHeader)).setText(getString(R.string.text_bank_account));

        cbWallet.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                if (isChecked) {
                    elWallet.show();
                    cbBank.setChecked(false);
                    cbcvoption.setChecked(false);
                    refundEntity.setWallet(true);
                    refundEntity.setCurrent_bank(false);
                    refundEntity.setIs_new_bank_details(false);
                    returnExchangeRequest.setRefund(refundEntity);
                } else {
                    if (elWallet.isOpened())
                        elWallet.hide();
                }
            }
        });

        cbcvoption.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                if (isChecked) {
                    elcvoption.show();
                    cbWallet.setChecked(false);
                    cbBank.setChecked(false);
                    refundEntity.setCredit(true);
                    refundEntity.setWallet(false);
                    refundEntity.setCurrent_bank(false);
                    refundEntity.setIs_new_bank_details(false);
                    returnExchangeRequest.setRefund(refundEntity);

                } else {
                    if (elcvoption.isOpened())
                        elcvoption.hide();
                }

            }
        });

        cbBank.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                if (isChecked) {
                    elBank.show();
                    cbWallet.setChecked(false);
                    cbcvoption.setChecked(false);
                    refundEntity.setWallet(false);
                    refundEntity.setCurrent_bank(true);
                    if (orderReturnResponse.getRefund().get(0).getBank_details() != null) {
                        refundEntity.setBank_details(orderReturnResponse.getRefund().get(0).getBank_details().get(0));
                        refundEntity.setBank_id(Integer.parseInt(orderReturnResponse.getRefund().get(0).getBank_details().get(0).getBank_id()));
                    }
                    returnExchangeRequest.setRefund(refundEntity);
                } else {
                    if (elBank.isOpened())
                        elBank.hide();
                }
            }
        });

        LinearLayout llBankDetails = (LinearLayout) elBank.findViewById(R.id.llBankDetails);
        if (orderReturnResponse != null) {
            if (orderReturnResponse.getRefund().get(0).isBank()) {
                rootView.findViewById(R.id.tvNoBanDetails).setVisibility(View.GONE);

                refundEntity.setBank_details(orderReturnResponse.getRefund().get(0).getBank_details().get(0));
                returnExchangeRequest.setRefund(refundEntity);
                BankItemsAdapter bankItemsAdapter = new BankItemsAdapter(mActivity,
                        orderReturnResponse.getRefund().get(0).getBank_details(), llBankDetails);
                bankItemsAdapter.setBankChanged(new BankItemsAdapter.BankChanged() {
                    @Override
                    public void onBankChange(OrderReturnResponse.RefundEntity.BankDetailsEntity bank) {
                        refundEntity.setBank_details(bank);
                        refundEntity.setBank_id(Integer.parseInt(bank.getBank_id()));
                        returnExchangeRequest.setRefund(refundEntity);
                    }
                });
            } else {
                toggleVisibility(R.id.rbNew);
                ((RadioButton) rootView.findViewById(R.id.rbNew)).setChecked(true);
            }
        }


        //New Address Fields
        llNewBankDetails = (LinearLayout) rootView.findViewById(R.id.llNewBankDetails);
        tvAccHolderName = (TextView) llNewBankDetails.findViewById(R.id.tvAccHolderName);
        tvAccNo = (TextView) llNewBankDetails.findViewById(R.id.tvAccNo);
        tvBankName = (TextView) llNewBankDetails.findViewById(R.id.tvBankName);
        tvBranchName = (TextView) llNewBankDetails.findViewById(R.id.tvBranchName);
        tvIFSCCode = (TextView) llNewBankDetails.findViewById(R.id.tvIFSCCode);

        spAccType = (Spinner) llNewBankDetails.findViewById(R.id.spAccType);
        spAccType.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                if (refundEntity.getBank_details() != null)
                    refundEntity.getBank_details().setBank_account_type((String) parent.getAdapter().getItem(position));
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });
    }

    void toggleVisibility(int checkedId) {
        this.checkedId = checkedId;
        if (checkedId == R.id.rbExist) {
            if (orderReturnResponse.getRefund().get(0).isBank())
                rootView.findViewById(R.id.tvNoBanDetails).setVisibility(View.GONE);
            else
                rootView.findViewById(R.id.tvNoBanDetails).setVisibility(View.VISIBLE);

            rootView.findViewById(R.id.llBankDetails).setVisibility(View.VISIBLE);
            rootView.findViewById(R.id.llNewBankDetails).setVisibility(View.GONE);
            returnExchangeRequest.getRefund().setCurrent_bank(true);
            returnExchangeRequest.getRefund().setIs_new_bank_details(false);
        } else if (checkedId == R.id.rbNew) {
            rootView.findViewById(R.id.tvNoBanDetails).setVisibility(View.GONE);
            rootView.findViewById(R.id.llBankDetails).setVisibility(View.GONE);
            rootView.findViewById(R.id.llNewBankDetails).setVisibility(View.VISIBLE);
            returnExchangeRequest.getRefund().setCurrent_bank(false);
            returnExchangeRequest.getRefund().setIs_new_bank_details(true);
        }
    }

    public OrderReturnResponse.RefundEntity.BankDetailsEntity getNewBankDetails() {

        OrderReturnResponse.RefundEntity.BankDetailsEntity bankDetailsEntity = null;
        if (validateNewBankDetails()) {
            bankDetailsEntity = new OrderReturnResponse.RefundEntity.BankDetailsEntity();
            bankDetailsEntity.setBank_name(tvBankName.getText().toString().trim());
            bankDetailsEntity.setBank_account_name(tvAccHolderName.getText().toString().trim());
            bankDetailsEntity.setBank_ifsc_code(tvIFSCCode.getText().toString().trim());
            bankDetailsEntity.setBank_account_no(tvAccNo.getText().toString().trim());
            bankDetailsEntity.setBank_branch_name(tvBranchName.getText().toString().trim());
            bankDetailsEntity.setBank_account_type(spAccType.getSelectedItem().toString());

        }
        return bankDetailsEntity;
    }


    boolean validateNewBankDetails() {
        if (TextUtils.isEmpty(tvBankName.getText().toString().trim())) {
            showToast("Please enter bank name");
            tvBankName.requestFocus();
            return false;
        } else if (TextUtils.isEmpty(tvAccHolderName.getText().toString().trim())) {
            showToast("Please enter account holder name");
            tvAccHolderName.requestFocus();
            return false;
        } else if (spAccType.getSelectedItemId() == 0) {
            showToast("Please select account type");
            spAccType.requestFocus();
            return false;
        } else if (TextUtils.isEmpty(tvAccNo.getText().toString().trim())) {
            showToast("Please enter account number");
            tvAccNo.requestFocus();
            return false;
        } else if (TextUtils.isEmpty(tvIFSCCode.getText().toString().trim())) {
            showToast("Please enter IFSC code");
            tvIFSCCode.requestFocus();
            return false;
        } else if (TextUtils.isEmpty(tvBranchName.getText().toString().trim())) {
            showToast("Please enter branch name");
            tvBranchName.requestFocus();
            return false;
        }

        return true;
    }

    public boolean validate() {
        if (!cbBank.isChecked() && !cbWallet.isChecked() && !cbcvoption.isChecked()) {
            showToast("Please select mode of refund");
            return false;
        }

        if (cbBank.isChecked() && checkedId == R.id.rbExist) {
            if (returnExchangeRequest.getAddress().getAddress_id() == null)
                showToast("Please select bank address");
            return false;
        }
        else if (cbBank.isChecked() && checkedId == R.id.rbNew) {
            getNewBankDetails();
        }
        return true;
    }


    public ReturnExchangeRequest getReturnExchangeRequest() {
        Log.e("Order Refund fragment ::","------------>"+returnExchangeRequest);
        return returnExchangeRequest;
    }
}
