package outletwise.com.twentydresses.view.activity;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.ActionBar;
import android.view.View;
import android.widget.TextView;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

import outletwise.com.twentydresses.R;
import outletwise.com.twentydresses.controller.CartOperations;
import outletwise.com.twentydresses.model.Address;
import outletwise.com.twentydresses.model.NewAddressResponse;
import outletwise.com.twentydresses.model.PlaceOrder;
import outletwise.com.twentydresses.model.PlaceOrderResponse;
import outletwise.com.twentydresses.utilities.Constants;
import outletwise.com.twentydresses.utilities.Urls;
import outletwise.com.twentydresses.view.fragment.OrderAddressFragment;

/**
 * Created by User-PC on 06-11-2015.
 * This class is use to show existing delivery addresses and user can also add new address
 */
public class SelectAddressActivity extends BaseActivity {

    private Address selectedAddress;
    private boolean isOfferApplied, isCreditApplied, isWalletApplied, isGiftWrap;
    private double finalPrice;
    private String address_id;
    private PlaceOrder placeOrder = new PlaceOrder();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_select_address);
        trackPage(getString(R.string.pgCheckoutAddress));
        setUpToolBar(getString(R.string.toolbar_address));
        final ActionBar ab = getSupportActionBar();
        ab.setDisplayHomeAsUpEnabled(true);

        init();
        findViewById(R.id.tvProceed).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                OrderAddressFragment orderAddressFragment = (OrderAddressFragment) getmFragmentManager().getFragments().get(getmFragmentManager().getBackStackEntryCount() - 1);
                selectedAddress = orderAddressFragment.getSelectedAddress();
                if (selectedAddress != null) {
                    if (selectedAddress.getAddress_id() != null) {

                        if (finalPrice != 0) {
                            Intent intent = new Intent(getApplicationContext(), PaymentOptionsActivity.class);
                            intent.putExtra(Constants.USER_ID, user.getUser_id());
                            intent.putExtra(Constants.GRAND_TOTAL, finalPrice);
                            intent.putExtra(Constants.APPLY_OFFERS, isOfferApplied);
                            intent.putExtra(Constants.APPLY_WALLET, isWalletApplied);
                            intent.putExtra(Constants.APPLY_CREDITS, isCreditApplied);
                            intent.putExtra(Constants.GIFT_WRAP, isGiftWrap);
                            intent.putExtra(Constants.ADDRESS, selectedAddress);
                            startActivity(intent);
                        } else
                            orderPlace();

                    } else {
                        // Add new address
                        JSONObject jsonObject = new JSONObject();
                        try {
                            jsonObject.put(Constants.USER_ID, user.getUser_id());
                            jsonObject.put(Constants.AUTH_TOKEN, preferences.getString(Constants.KEY_TOKEN, ""));
                            jsonObject.put(Constants.FIRST_NAME, selectedAddress.getFirstname());
                            jsonObject.put(Constants.LAST_NAME, selectedAddress.getLastname());
                            jsonObject.put(Constants.MOBILE, selectedAddress.getMobile());
                            jsonObject.put(Constants.COUNTRY, "India");
                            jsonObject.put(Constants.STATE, selectedAddress.getState());
                            jsonObject.put(Constants.CITY, selectedAddress.getCity());
                            jsonObject.put(Constants.ADDRESS, selectedAddress.getAddress());
                            jsonObject.put(Constants.PINCODE, selectedAddress.getPincode());

                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                        postJsonData(Urls.ADDRESS_ADD, jsonObject, Constants.TAG_ADD_ADDRESS, true);
                    }
                }
               /* else
                        showToast("Please select address");*/
            }

        });
    }

    private void orderPlace() {

        placeOrder.setIsCreditApplied(isCreditApplied);
        placeOrder.setIsGiftWrap(isGiftWrap);
        placeOrder.setIsOfferApplied(isOfferApplied);
        placeOrder.setIsWalletApplied(isWalletApplied);
        placeOrder.setAddress(selectedAddress);
        placeOrder.setAuth_token(preferences.getString(Constants.KEY_TOKEN, ""));
        placeOrder.setAmount(finalPrice);
        placeOrder.setUser(user.getUser_id());
        //If amount paid with wallet & amt is Rs.0, set payment mode to exchange
        placeOrder.setPayment_mode(Constants.EXCHANGE);
        placeOrder.setCurrent_app_version(getCurrentAppVersion());
        postJsonData(Urls.ORDER_PLACE, placeOrder, Constants.TAG_ORDER_PLACE, true);
    }


    void init() {
        finalPrice = getIntent().getDoubleExtra(Constants.GRAND_TOTAL, 0);
        isOfferApplied = getIntent().getBooleanExtra(Constants.APPLY_OFFERS, false);
        isCreditApplied = getIntent().getBooleanExtra(Constants.APPLY_CREDITS, false);
        isWalletApplied = getIntent().getBooleanExtra(Constants.APPLY_WALLET, false);
        isGiftWrap = getIntent().getBooleanExtra(Constants.GIFT_WRAP, false);
        findViewById(R.id.llCheckout).bringToFront();
        ((TextView) findViewById(R.id.tvFinalAmount)).setText(Constants.getPaddedAmount(finalPrice, true));
    }

    @Override
    public void onCompleteRetrieval(int tag, ArrayList obj, String msg) {
        super.onCompleteRetrieval(tag, obj, msg);
        switch (tag) {
            case Constants.TAG_USER:
                replaceFragment(R.id.frame_container, new OrderAddressFragment(), OrderAddressFragment.class.getSimpleName());
                break;
        }
    }


    @Override
    public <T> void onResponse(T obj, int tag) {
        super.onResponse(obj, tag);
        switch (tag) {

            case Constants.TAG_ADD_ADDRESS:
                NewAddressResponse response = (NewAddressResponse) obj;
                if (response.getStatus().equalsIgnoreCase(Constants.RESPONSE_SUCCESS))
                    address_id = response.getAddress_id();

                selectedAddress.setAddress_id(address_id);

                if (finalPrice != 0) {
                    Intent intent = new Intent(getApplicationContext(), PaymentOptionsActivity.class);
                    intent.putExtra(Constants.GRAND_TOTAL, finalPrice);
                    intent.putExtra(Constants.APPLY_OFFERS, isOfferApplied);
                    intent.putExtra(Constants.APPLY_WALLET, isWalletApplied);
                    intent.putExtra(Constants.APPLY_CREDITS, isCreditApplied);
                    intent.putExtra(Constants.GIFT_WRAP, isGiftWrap);
                    intent.putExtra(Constants.ADDRESS, selectedAddress);
                    startActivity(intent);
                } else
                    orderPlace();
                break;

            case Constants.TAG_ORDER_PLACE:
                PlaceOrderResponse placeOrderResponse = (PlaceOrderResponse) obj;
                if (placeOrderResponse.isMobile_verified()) {
                    Intent intent = new Intent(getApplicationContext(), OrderSuccessfulActivity.class);
                    intent.putExtra(Constants.ORDER_ID, placeOrderResponse.getOrder_id());
                    startActivity(intent);
                    // startActivity(new Intent(this, OrderSuccessfulActivity.class));
                } else {
                    CartOperations.getInstance().removeAll();
                    user.setCart_count("" + 0);
                    insertRecord(user, Constants.TAG_USER);
                    Intent intent1 = new Intent(getApplicationContext(), VerifyOrderActivity.class);
                    intent1.putExtra(Constants.ORDER_ID, placeOrderResponse.getOrder_id());
                    intent1.putExtra(Constants.MOBILE, placeOrder.getAddress().getMobile());
                    startActivity(intent1);
                }
             break;
        }
    }

    /*@Override
    public void onError(int error_code, int tag, Object object) {
        checkErrorCode(error_code, object);
    }*/
}
