package outletwise.com.twentydresses.view.fragment;

import android.app.Dialog;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.support.annotation.Nullable;
import android.support.v4.app.DialogFragment;
import android.support.v7.widget.RecyclerView;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.RelativeLayout;

import org.solovyev.android.views.llm.LinearLayoutManager;

import java.util.ArrayList;
import java.util.Arrays;

import outletwise.com.twentydresses.R;
import outletwise.com.twentydresses.controller.adapter.BaseRecyclerAdapter;
import outletwise.com.twentydresses.controller.adapter.SortByAdapter;
import outletwise.com.twentydresses.utilities.Constants;
import outletwise.com.twentydresses.view.decoration.DividerItemDecoration;

/**
 * Created by User-PC on 28-09-2015.
 */
public class ProductsSortFragment extends DialogFragment {

    private View rootView;
    private RecyclerView rvSortBy;
    private Callback callback;
    private int sortPos;
    private int selectedpostionstate;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        rootView = inflater.inflate(R.layout.fragment_products_sortby, container, false);
        setUpRecyclerView();
        return rootView;
    }

    public void setCallback(Callback callback) {
        this.callback = callback;
    }

    void setUpRecyclerView() {
        rvSortBy = (RecyclerView) rootView.findViewById(R.id.rvSortBy);
        rvSortBy.setLayoutManager(new LinearLayoutManager(rvSortBy.getContext()));
        rvSortBy.addItemDecoration(new DividerItemDecoration(rvSortBy.getContext(), DividerItemDecoration.VERTICAL_LIST));
        SortByAdapter adapter = new SortByAdapter(getActivity(),
                new ArrayList<>(Arrays.asList(getActivity().getResources().getStringArray(R.array.sort_items))));
        adapter.setSelectedPos(sortPos);
        adapter.setOnItemClickListener(new BaseRecyclerAdapter.OnItemClickListener() {
            @Override
            public void onItemClick(View view, int position) {
                if (callback != null)
                    callback.onCallback((String) view.getTag());
                Constants.LATESTSORT = position;
                dismiss();
            }
        });
        rvSortBy.setAdapter(adapter);
    }

    public void setSortPos(int sortPos) {
        this.sortPos = sortPos;
    }

    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {
        final RelativeLayout root = new RelativeLayout(getActivity());
        root.setLayoutParams(new ViewGroup.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT));
        Dialog dialog = new Dialog(getActivity());
        dialog.getWindow().requestFeature(Window.FEATURE_NO_TITLE);
        dialog.getWindow().setGravity(Gravity.BOTTOM);
        dialog.setContentView(root);
        dialog.getWindow().setLayout(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT);
        return dialog;
    }

    @Override
    public void onStart() {
        super.onStart();

        if (getDialog() == null) {
            return;
        }
        getDialog().getWindow().setWindowAnimations(R.style.dialog_animation_translate);
    }

    public interface Callback {
        void onCallback(String sort);
    }

}
