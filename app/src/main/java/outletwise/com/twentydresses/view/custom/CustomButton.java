package outletwise.com.twentydresses.view.custom;

import android.annotation.TargetApi;
import android.content.Context;
import android.os.Build;
import android.util.AttributeSet;
import android.widget.Button;

/**
 * Created by User-PC on 14-08-2015.
 */
public class CustomButton extends Button {
    private int oldWidth, oldHeight;
    private float transOldX, transOldY;
    private static final float VALUE_X = 2;
    private static final float VALUE_Y = 3;

    public CustomButton(Context context) {
        super(context);
    }

    public CustomButton(Context context, AttributeSet attrs) {
        super(context, attrs);
    }

    public CustomButton(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
    }

    @TargetApi(Build.VERSION_CODES.LOLLIPOP)
    public CustomButton(Context context, AttributeSet attrs, int defStyleAttr, int defStyleRes) {
        super(context, attrs, defStyleAttr, defStyleRes);
    }


//    @Override
//    public boolean onTouchEvent(MotionEvent event) {
//
//        switch (event.getAction()) {
//            case MotionEvent.ACTION_UP:
//                this.setTranslationX(transOldX);
//                this.setTranslationY(transOldY);
//                this.setWidth(oldWidth);
//                this.setHeight(oldHeight);
//                break;
//
//            case MotionEvent.ACTION_DOWN:
//                oldWidth = this.getWidth();
//                oldHeight = this.getHeight();
//                transOldX = this.getTranslationX();
//                transOldY = this.getTranslationY();
//                this.setTranslationX(transOldX + getTransXInPx());
//                this.setTranslationY(transOldY + getTransYInPx());
//                this.setWidth(oldWidth - (int) getTransXInPx());
//                this.setHeight(oldHeight - (int) getTransYInPx());
//                return true;
//        }
//        return super.onTouchEvent(event);
//    }


    float getTransXInPx() {
        return getResources().getDisplayMetrics().density * VALUE_X;
    }

    float getTransYInPx() {
        return getResources().getDisplayMetrics().density * VALUE_Y;
    }

}
