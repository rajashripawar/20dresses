package outletwise.com.twentydresses;

import android.app.Application;
import android.content.Context;
import android.os.AsyncTask;
import android.support.multidex.MultiDex;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.toolbox.Volley;
import com.clevertap.android.sdk.ActivityLifecycleCallback;
import com.clevertap.android.sdk.CleverTapAPI;
import com.clevertap.android.sdk.exceptions.CleverTapMetaDataNotFoundException;
import com.clevertap.android.sdk.exceptions.CleverTapPermissionsNotSatisfied;
import com.crashlytics.android.Crashlytics;
import com.google.android.gms.analytics.GoogleAnalytics;
import com.google.android.gms.analytics.Tracker;
import com.google.android.gms.gcm.GoogleCloudMessaging;

import java.io.IOException;

import io.fabric.sdk.android.Fabric;
import outletwise.com.twentydresses.model.database.greenbot.DaoSession;
import outletwise.com.twentydresses.utilities.database.DBHelper;
import semusi.activitysdk.Api;
import semusi.activitysdk.ContextApplication;
import semusi.activitysdk.ContextSdk;
import semusi.activitysdk.SdkConfig;
import uk.co.chrisjenx.calligraphy.CalligraphyConfig;

/**
 * Created by User-PC on 11-08-2015.
 */
public final class ApplicationUtil extends Application {

    private RequestQueue mRequestQueue;
    private static ApplicationUtil mInstance;
    private DBHelper mDbHelper;
    public Tracker mTracker;
    public Tracker ecommerceTracker;
    public static final String MIXPANEL_DISTINCT_ID_NAME = "Mixpanel$distinctid";
    public CleverTapAPI ct;

    public RequestQueue getRequestQueue() {
        if (mRequestQueue == null) {
            mRequestQueue = Volley.newRequestQueue(getApplicationContext());
        }
        return mRequestQueue;
    }

    public <T> void addToRequestQueue(Request<T> req, String tag) {
        req.setTag(tag);
        getRequestQueue().add(req);
    }

    public void cancelPendingRequest(Object tag) {
        if (mRequestQueue != null) {
            mRequestQueue.cancelAll(tag);
        }
    }

    @Override
    public void onCreate() {
        // CleverTap
        ActivityLifecycleCallback.register(this);
        super.onCreate();


        mInstance = this;
        mDbHelper = new DBHelper(mInstance);

        //Crash Reporting
        Fabric.with(this, new Crashlytics());

        //Set Default font for whole application
        CalligraphyConfig.initDefault(new CalligraphyConfig.Builder()
                .setDefaultFontPath("fonts/PrimaryFont_Proxima.ttf")
                .setFontAttrId(R.attr.fontPath)
                .build()
        );

        //CleverTap
        try {
            ct = CleverTapAPI.getInstance(getApplicationContext());
            // CleverTapAPI.setDebugLevel(1277182231);
            registerBackground();

        } catch (CleverTapMetaDataNotFoundException e) {
            // handle appropriately
        } catch (CleverTapPermissionsNotSatisfied e) {
            // handle appropriately
        }

       /* //appIce
        ContextApplication.initSdk(getApplicationContext(), this);

        boolean isSemusiSensing = ContextSdk.isSemusiSensing(getApplicationContext());
        if (isSemusiSensing == false) {
            SdkConfig config = new SdkConfig();
            // To enable analytics tracking - default enabled
            config.setAnalyticsTrackingAllowedState(true);
            config.setGcmSenderId(getResources().getString(R.string.google_sender_id));
            config.setDebuggingStateAllowed(true);
            Api.startContext(getApplicationContext(), config);  // To disable Appice comment this line of code
        }*/
    }

    private void registerBackground() {
        new AsyncTask() {
            @Override
            protected Object doInBackground(Object[] params) {
                GoogleCloudMessaging gcm = GoogleCloudMessaging.getInstance(getApplicationContext());
                String gcmRegId = null;
                try {
                    gcmRegId = gcm.register(getResources().getString(R.string.google_sender_id));
                    ct.data.pushGcmRegistrationId(gcmRegId, true);
                } catch (IOException e) {
                    e.printStackTrace();
                } catch (NullPointerException ex) {
                    ex.printStackTrace();
                }

                return null;
            }

        }.execute(null, null, null);
    }


    synchronized public Tracker getDefaultTracker() {
        if (mTracker == null) {
            GoogleAnalytics analytics = GoogleAnalytics.getInstance(this);
            // To enable debug logging use: adb shell setprop log.tag.GAv4 DEBUG
            mTracker = analytics.newTracker(getString(R.string.global_tracker));
        }
        return mTracker;
    }


    synchronized public Tracker getEcommerceTrackerTracker() {
        if (ecommerceTracker == null) {
            GoogleAnalytics analytics =  GoogleAnalytics.getInstance(this);
            // To enable debug logging use: adb shell setprop log.tag.GAv4 DEBUG
            ecommerceTracker = analytics.newTracker(getString(R.string.global_tracker));
        }
        return ecommerceTracker;
    }

    public static ApplicationUtil getInstance() {
        return mInstance;
    }

    public static DaoSession getNewSession() {
        return getInstance().mDbHelper.getSession(true);
    }

    public static DaoSession getSession() {
        return getInstance().mDbHelper.getSession(false);
    }

    protected void attachBaseContext(Context base) {
        super.attachBaseContext(base);
        MultiDex.install(this);
    }
}
